<?php

use Symfony\Component\HttpFoundation\Request;
use Core\Includes\Config;
use DebugBar\StandardDebugBar;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\RedisSessionHandler;
use Core\Includes\Database;
use Core\Includes\PHPRbac;
use XpnCMS\Model\User;
use Core\Includes\I18n;
use Core\Helpers\Time;
use Core\Helpers\Files;
use Core\Helpers\Forms;
use Core\Includes\Action;
use DebugBar\DataCollector\PDO\TraceablePDO;
use DebugBar\DataCollector\PDO\PDOCollector;
use Whoops\Run;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Core\Includes\Application;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// Report all errors except E_NOTICE
// This is the default value set in php.ini
error_reporting(E_ALL & ~E_NOTICE);

$startInit = microtime(true);

// **PREVENTING SESSION HIJACKING**
// Prevents javascript XSS attacks aimed to steal the session ID
ini_set('session.cookie_httponly', 1);

// **PREVENTING SESSION FIXATION**
// Session ID cannot be passed through URLs
ini_set('session.use_only_cookies', 1);

$paths = [

];

define('APP_NAMESPACE', 'XpnCMS\\');
define('APP_W_NAMESPACE', APP_ROOT . 'com' . DS . 'xpncms' . DS);
define('APP_CONTROLLER', APP_W_NAMESPACE . 'Controller' . DS);
define('APP_MODEL', APP_W_NAMESPACE . DS . 'Model' . DS);
define('APP_VIEW', APP_W_NAMESPACE . 'View' . DS . 'templates' . DS);
define('APP_LOCALE', APP_W_NAMESPACE . 'Locale' . DS);
define('APP_PLUGIN', APP_W_NAMESPACE . 'Plugins' . DS);
$hybridauthConfig = APP_ROOT . 'config' . DS . 'hybridauth_config.php';

$srvIp = $_SERVER['REMOTE_ADDR'];
if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $srvIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
define('IP', $srvIp);

$config_file = APP_ROOT . 'config' . DS . 'conf.php';
if (!file_exists($config_file)) {
    throw new Exception($config_file . ' cannot load!');
}
require_once $config_file;

$auto_load = APP_ROOT . 'vendor' . DS . 'autoload.php';
if (!file_exists($auto_load)) {
    throw new Exception($auto_load . ' cannot load!');
}
$loader = require_once $auto_load;

// setup Propel
require_once APP_ROOT .'config/config.php';

require_once APP_ROOT . 'config' . DS . 'database.php';

$timezone = Config::get('site.timezone');
ini_set('date.timezone', $timezone); // lai pareizi būtu visi laiki

$handler = null;
if (session_module_name() === 'redis') {
    $redis = new Redis();
    $redisHost = !empty(getenv('REDIS_HOST')) ? getenv('REDIS_HOST') : 'redis';
    $redisPort = !empty(getenv('REDIS_PORT')) ? getenv('REDIS_PORT') : 6379;
    $redis->connect($redisHost, $redisPort);
    $handler = new RedisSessionHandler($redis, []);
}
$sessionStorage = new NativeSessionStorage(
    ['cookie_domain' => getenv('SESSION_DOMAIN')],
    $handler
);
$session = new Session($sessionStorage);

$db1 = new Database;
$db1->db_host = Config::get('databases.home.host');
$db1->db_user = Config::get('databases.home.user');
$db1->db_pass = Config::get('databases.home.password');
$db1->db_name = Config::get('databases.home.name');
$db1->db_char = Config::get('databases.home.charset');
$db1->db_prefix = Config::get('databases.home.prefix');
$db1->connect();

if (Config::get('databases.remote.host')) {
    $db2 = new Database;
    $db2->db_host = Config::get('databases.remote.host');
    $db2->db_port = Config::get('databases.remote.port', 3306);
    $db2->db_user = Config::get('databases.remote.user');
    $db2->db_pass = Config::get('databases.remote.password');
    $db2->db_name = Config::get('databases.remote.name');
    $db2->db_char = Config::get('databases.remote.charset');
    $db2->db_prefix = Config::get('databases.remote.prefix');
    $db2->connect();
}

$time = new Time;
$files = new Files;
$forms = new Forms;
$action = new Action;

$smarty = new Smarty();
$smarty->setTemplateDir(APP_VIEW . 'default/');
$smarty->setCompileDir(APP_ROOT . 'cache' . DS . 'templates_c' . DS);
$smarty->setConfigDir(APP_ROOT . 'config' . DS);
$smarty->setCacheDir(APP_ROOT . 'cache' . DS . 'templates' . DS);
$smarty->addPluginsDir(APP_PLUGIN . 'smarty' . DS);

$smarty->debugging = Config::get('debug.smartydebug');
$smarty->setCaching(Config::get('cahe.smarty') ?: 0);
$smarty->setCacheLifetime(Config::get('site.environment') === 'development' ? 0 : 120);
$smarty->setCompileCheck(true);
// $smarty->compile_check = Config::get('site.environment') === 'development' ? 1 : 0;
$smarty->setForceCompile(Config::get('site.environment') === 'development' ? 1 : 0);

// production
//error_reporting(0);
//ini_set('display_errors', 0);
//ini_set('display_startup_errors', 0);

$databaseConfig = [
    'host' => Config::get('databases.home.host'),
    'user' => Config::get('databases.home.user'),
    'pass' => Config::get('databases.home.password'),
    'dbname' => Config::get('databases.home.name'),
    'adapter' => 'pdo_mysql',
    'tablePrefix' => Config::get('databases.home.prefix'),
];

/* we use this later as global :( */
$rbac = new PHPRbac(null, $databaseConfig);

/* debugging */
$debugbar = null;
$debugbar = new StandardDebugBar();
if (Config::get('debug.enabled')) {
    $mainAdminRoleId = $rbac->Roles->returnId('main_admin');
    $allowed = $rbac->Users->hasRole($mainAdminRoleId, User::authId());
    if (Config::get('site.environment') === 'development' || $allowed) {

        if (Config::get('debug.debugbar')) {
            $debugbar['messages']->info('xpnCMS new debugbar');

            $pdoHome = new TraceablePDO($db1->getDBInstance());
            $pdoCollector = new PDOCollector();
            $pdoCollector->addConnection($pdoHome, 'home-db');

            if ($db2) {
                $pdoRemote = new TraceablePDO($db2->getDBInstance());
                $pdoCollector->addConnection($pdoRemote, 'remote-db');
            }

            $debugbar->addCollector($pdoCollector);
            $debugbarRenderer = $debugbar->getJavascriptRenderer();
            $debugbarRenderer->setBaseUrl(
                Config::get('base.url') . Config::get('paths.assets') . '/php-debugbar/src/DebugBar/Resources/'
            );
        }

        if (Config::get('debug.whoopsips')) {
            $ips = Config::get('debug.whoopsips');

            if (!in_array(IP, $ips)) {
                $debugbar['messages']->info(
                    'whoops is enabled but your IP: ' . IP . ' are not in whoopsips debug array!'
                );
            } else {
                error_reporting(Config::get('debug.level'));
                ini_set('display_startup_errors', 1);
                ini_set('display_errors', 1);

                $whoops = new Run;

                $httpXRequestedWith = $_SERVER['HTTP_X_REQUESTED_WITH'] ?? null;
                if (empty($httpXRequestedWith)) {
                    $whoops->appendHandler(new PrettyPageHandler);
                }

                if (!empty($httpXRequestedWith) && strtolower($httpXRequestedWith) == 'xmlhttprequest') {
                    $jsonResponseHandler = new JsonResponseHandler;
                    // Should detailed stack trace output also be added to the JSON payload body?
                    $jsonResponseHandler->addTraceToOutput(true);
                    header('Content-Type: application/json');
                    $whoops->appendHandler($jsonResponseHandler);
                }
                $whoops->register();
            }
        }
    }
}

$request = Request::createFromGlobals();
// get route
$startPage = explode('/', Config::get('site.startpage'));

$uri = [];
$uri['controller'] = reset($startPage);
$uri['method'] = last($startPage);
$uri['var'] = null;

if ($request->getPathInfo() !== '/') {
    $array_tmp_uri = preg_split('[\\/]', $request->getPathInfo(), -1, PREG_SPLIT_NO_EMPTY);
    $uri['controller'] = @$array_tmp_uri[0];
    if (isset($array_tmp_uri[1]) && is_numeric($array_tmp_uri[1])) {
        $uri['method'] = 'index';
        $uri['var'] = @$array_tmp_uri[1];
        $uri['var_1'] = @$array_tmp_uri[2];
        $uri['var_2'] = @$array_tmp_uri[3];
        $uri['var_3'] = @$array_tmp_uri[4];
    } else {
        $uri['method'] = isset($array_tmp_uri[1]) ? @$array_tmp_uri[1] : 'index';
        $uri['var'] = @$array_tmp_uri[2];
        $uri['var_1'] = @$array_tmp_uri[3];
        $uri['var_2'] = @$array_tmp_uri[4];
        $uri['var_3'] = @$array_tmp_uri[5];
    }
}

if (empty($uri['controller'])) {
    $uri['controller'] = reset($startPage);
}

$smarty->assign(['uri' => $uri]);

define('ASSET_COMPILE_OUTPUT_DIR', APP_ROOT . 'public' . DS . 'cache' . DS . 'sacy');
define('ASSET_COMPILE_URL_ROOT', Config::get('base.url') . 'cache' . DS . 'sacy');

$smarty->assign(Config::get('base')); // deprecated
$smarty->assign(Config::get('site')); // deprecated

$smarty->assign(['base' => Config::get('base')]);
$smarty->assign(['site' => Config::get('site')]);
$smarty->assign(['d' => Config::get('debug')]);

$gradioPrefs = Config::get('plugins.gradio.prefs');

$smarty->assign(['gradioPrefs' => $gradioPrefs]);
$smarty->assign(['plugins' => Config::get('plugins')]);
$smarty->assign(['version' => Config::get('version')]);
$smarty->assign(['ajax' => false]);
$smarty->assign(['googleanalytics' => Config::get('prefs.googleanalytics', [])]);
$smarty->assign(['piwik' => Config::get('prefs.piwik', [])]);
$paths['paths'] = Config::get('paths');
$smarty->assign($paths);
$images['images'] = Config::get('images');
$smarty->assign($images);

$smarty->assign(['APP_ROOT' => APP_ROOT, 'APP_VIEW' => APP_VIEW, 'DS' => DS]);

$i18n = new I18n;

$initiator = null;
$endInit = microtime(true);

//addMeasure($label, $start, $end, $params = array(), $collector = null)
if ($debugbar) {
    $debugbar['time']->addMeasure('init', $startInit, $endInit);
}

$appStartLoad = microtime(true);

function d($vars)
{
    echo '<pre>';
    var_dump($vars);
    echo '</pre>';
}

function dd($vars)
{
    d($vars);
    exit;
}

$application = new Application($uri);
