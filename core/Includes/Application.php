<?php

namespace Core\Includes;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Throwable;
use XpnCMS\Exceptions\ApplicationControllerLoadException;
use XpnCMS\Model\Model;

class Application
{
    private const DEFAULT_METHOD = 'index';

    /**
     * for example
     * /content/:slug
     * /category/:slug
     *
     * then we need to point to index method
     */
    private const MULTI_LEVEL_ROUTE_CONTROLLERS = [
        'content',
        'category'
    ];

    public $uri;
    public $templateArr; // set values to send to view from everywhere
    protected $request;
    protected $template;

    /**
     * Application constructor.
     * @param string|null $uri
     * @throws ApplicationControllerLoadException
     * @throws Exception
     */
    public function __construct($uri = null)
    {
        $this->request = Request::createFromGlobals();
        $this->setSettings();
        $this->uri = $uri;
        $this->loadController();
    }

    /**
     * @throws ApplicationControllerLoadException
     * @throws Exception
     * @throws Throwable
     */
    public function loadController()
    {
        $controller = ucwords(mb_strtolower($this->uri['controller']));
        $methodUri = $this->determineMethod($controller);
        $var = $this->uri['var'];
        $plugin = mb_strtolower($this->uri['controller']);

        $method = $this->prepareMethodToCall($methodUri);

        $appControllerNamespace = "\XpnCMS\Controller\\$controller";
        $appPluginControllerNamespace = "\XpnCMS\Plugins\\$plugin\Controller\\$controller";
        $controllerNameSpace = null;
        if (method_exists($appControllerNamespace, $method)) {
            $controllerNameSpace = $appControllerNamespace;
        } elseif (method_exists($appPluginControllerNamespace, $method)) {
            $controllerNameSpace = $appPluginControllerNamespace;
        }

        try {
            $controllerObject = new $controllerNameSpace();
            call_user_func([$controllerObject, $method], $var);
        } catch (Throwable $throwable) {
            if (! Config::get('debug.enabled')) {
                 $response = new Response(null, Response::HTTP_NOT_FOUND);
                 return $response->send();
            }

            if (mb_strtolower($throwable->getMessage()) !== 'class name must be a valid object or a string') {
                throw $throwable;
            }

            $namespaces = "$appControllerNamespace or $appPluginControllerNamespace";
            throw new ApplicationControllerLoadException(
                "On namespace: $namespaces method: $method doesn't exist",
                Response::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * @throws Exception
     */
    private function setSettings(): void
    {
        $session = new Session();
        $model = new Model();
        if (Config::get('site.template')) {
            $this->template = Config::get('site.template');
        }
        if ($session->get('template')) {
            $this->template = $session->get('template');
        }
        if ($this->request->query->get('template')) {
            $this->template = $this->request->query->get('template');
        }
        $session->set('template', $this->template);
        $model->setTheme($this->template);

        if ($this->request->query->get('debug')) {
            $session->set('debug', $this->request->query->get('debug'));
        }
        if (Config::get('debug.jsdebug')) {
            $session->set('jsdebug', Config::get('debug.jsdebug'));
        }
        $session->save();
    }

    /**
     * if we pass for example now it will return now
     * if we pass now-playing-deck it will return nowPlayingDeck
     * @param string $methodUri
     * @return string
     */
    private function prepareMethodToCall(string $methodUri): string
    {
        $methodWords = explode('-', $methodUri);
        $methodName[] = $methodWords[0];
        unset($methodWords[0]);
        $methodUppercaseWords = array_map(function (string $item) {
            return ucfirst($item);
        }, $methodWords);

        return  implode('', array_merge($methodName, $methodUppercaseWords));
    }

    private function determineMethod(string $controller): string
    {
        return $this->shouldBeDefaultMethod($controller) ? self::DEFAULT_METHOD : mb_strtolower($this->uri['method']);
    }

    /**
     * @param string $controller
     * @return bool
     */
    private function shouldBeDefaultMethod(string $controller): bool
    {
        return (in_array(mb_strtolower($controller), self::MULTI_LEVEL_ROUTE_CONTROLLERS)
            || !isset($this->uri['method']));
    }
}
