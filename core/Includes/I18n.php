<?php

namespace Core\Includes;

use function mebb\lib\i18n\smarty\compile;
use function mebb\lib\i18n\smarty\save_individual;
use Exception;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Session\Session;
use Core\Includes\Config;

class I18n
{
    public $debugArr;

    public function __construct()
    {
        global $conf, $smarty;

        if (!function_exists("gettext")) {
            echo "php gettext is not installed\n";
        }

        $session = new Session();

        $encoding = 'UTF-8';
        $path     = APP_LOCALE;
        if (!is_dir($path)) {
            exit("$path not directory!");
        }

        $this->addDebug(['path' => $path]);

        /* TODO remove $_GET */
        if (isset($_GET['locale']) && !empty($_GET['locale'])) {
            $locale = $_GET['locale'];
            $session->set('locale', $locale);
        } elseif ($session->get('locale')) {
            $locale = $session->get('locale');
        } elseif (Config::get('site.locale')) {
            $locale = Config::get('site.locale');
            $session->set('locale', $locale);
        }

        $localewin = $locale.'.'.$encoding;

        if (!setlocale(LC_ALL, $localewin)) {
            $locale = 'en_US'; // fallback locale
            $session->set('locale', $locale);

            //setlocale(LC_ALL, $localewin);
        }

        $session->save();

        $this->addDebug(['locale' => $locale]);

        //setlocale(LC_MESSAGES, NULL);
        //$setlocale = setlocale(LC_MESSAGES, array("lv_LV", "lv_LV.UTF-8"));

        $setlocale = setlocale(LC_ALL, $localewin);

        //print_r($setlocale);
        //echo '<br>';

        $this->addDebug(['setlocale' => $setlocale]);

        $shortLang = explode("_", $locale);

        $this->addDebug(['shortLang' => $shortLang]);

        setlocale(LC_TIME, $shortLang[0]);
        Carbon::setLocale($shortLang[0]);

        //$domain = "messages";
        //$domain = "lv_LV";
        $domain = $locale;

        $this->addDebug(['domain' => $domain]);

        //print_r($localewin);
        //echo '<br>';

        $this->addDebug(['localewin' => $localewin]);

        $publicJsDir = APP_ROOT.$conf['settings']['paths']['public'].DS.$conf['settings']['paths']['js'].DS.'locale'.DS;

        if (!file_exists($publicJsDir)) {
            if (!mkdir($publicJsDir)) {
                throw new Exception("cannot make $publicJsDir directory");
            }
        }

        $outputFile = $publicJsDir.$domain.'.json';

        $this->addDebug(['output_file' => $outputFile]);

        // path to the .MO file that we should monitor
        $filename     = "$path/$locale/LC_MESSAGES/$domain.mo";
        $mtime        = filemtime($filename); // check its modification time
        // our new unique .MO file
        $filenameNew = "$path/$locale/LC_MESSAGES/{$domain}_{$mtime}.mo";

        if (file_exists($outputFile)) {
            $publicJsFile = $conf['settings']['paths']['js'].'/locale/'.$domain.'.json';
            $smarty->assign(['locale_file' => $publicJsFile]);
        } else {
            $this->po2JSON($path, $locale, $domain, $filename, $filenameNew, $outputFile);
        }

        // gettext setup
        //$T_setlocale = T_setlocale(LC_MESSAGES, $locale);
        //$this->addDebug(['T_setlocale' => $T_setlocale]);
        //var_dump($T_setlocale);

        /*
          if ( defined('LC_MESSAGES') ){
          setlocale(LC_MESSAGES, $locale); // Linux
          }else{
          //putenv("LC_ALL={$locale}"); // windows

          $putenvlang = putenv("LANG=".$locale."");
          if (!$putenvlang){
          exit ('putenvlang failed');
          }

          $putenvlcall = putenv("LC_ALL=".$locale."");
          if (!$putenvlcall){
          exit ('putenvlcall failed');
          }
          }
         */

        if (!file_exists($filenameNew)) {
            $this->po2JSON($path, $locale, $domain, $filename, $filenameNew, $outputFile);
        }

        // compute the new domain name
        $domainNew = "{$domain}_{$mtime}";
        //$domainNew = "de";

        $this->addDebug(['domain_new' => $domainNew]);

        $putenv = putenv('LANG='.$locale.'');
        $this->addDebug(['putenv' => $putenv]);

        // bind it
        $bindtextdomain = bindtextdomain($domainNew, $path);

        $this->addDebug(['bindtextdomain' => $bindtextdomain]);

        //print_r($bindtextdomain);
        //echo '<br>';
        // bind_textdomain_codeset is supported only in PHP 4.2.0+
        if (function_exists('bind_textdomain_codeset')) {
            $bindTextdomainCodeset = bind_textdomain_codeset($domainNew, $encoding);
            $this->addDebug(['bind_textdomain_codeset' => $bindTextdomainCodeset]);
        }

        //print_r($bindTextdomainCodeset);
        //echo '<br>';
        // then activate it
        $textdomain = textdomain($domainNew);

        $this->addDebug(['textdomain' => $textdomain]);

        // echo '<pre>';
        // print_r($this->debug());
        // echo '<br>';
        // echo gettext("Recently played tunes");
        // exit;
    }

    private function po2JSON($path, $locale, $domain, $filename, $filenameNew, $outputFile)
    {
        global $conf;

        $dir = scandir(dirname($filename));

        foreach ($dir as $file) {
            if (in_array($file, ['.', '..', "{$domain}.po", "{$domain}.mo"])) {
                continue;
            }
            unlink(dirname($filename).DIRECTORY_SEPARATOR.$file);
        }

        $po2json    = APP_ROOT.'core'.DS.'Includes'.DS.'po2json.php';
        $inputFile = $path.$locale.DS.'LC_MESSAGES'.DS.$domain.'.po';

        if (file_exists($outputFile)) {
            unlink($outputFile);
        }

        /*
          echo $po2json;
          echo "<br>";
          echo $inputFile;
          echo "<br>";
          echo $outputFile;
          echo "<br>";
         */

        //if (strpos(shell_exec('php -l file.php'), 'Syntax Error')) {
        //	die('An error!');
        //}


        if (function_exists('exec')) {
            $genJson = exec('php '.$po2json.' -i '.$inputFile.' -o '.$outputFile.' -n locale_data', $output);
            //print_r($output);
            //exit;
        } else {
            throw new Exception('PHP exec not working!');
        }

        //print_r($genJson);

        copy($filename, $filenameNew);
    }

    public function addDebug($data)
    {
        $this->debugArr[__CLASS__]['debug'][] = $data;
    }

    public function debug()
    {
        return $this->debugArr;
    }

    public function generate()
    {
        global $smarty;
        require_once 'mebb/mebb_functions_glob_recursive.php';
        require_once 'mebb/mebb_i18n_smarty.php';

        $compileFile = APP_VIEW.'templates/default/user';
        $compileDir  = APP_LOCALE;

        define('MEBB_IGNORE_ERRORS', true);
        define('MEBB_TEMPLATE_EXTENSION', 'tpl');

        //3.) we compile all the templates in the template directory
        //	  you can also set a custom directory/subdirectory and only consider files therein,
        //	  if you like to create multiple po/mo files
        $info    = []; //the passback array for error definitions (in case there are any)
        $sources = compile($smarty, $compileFile, $info);

        //4.) we're saving the files to the temporary directory. We've chosen to save them individually
        //	  because the po/mo files will at least contain an indication of the origin for the
        //	  message IDs, even if the line # will not be correct and the file-name will
        //	  be re-formatted; but hey, that's as good it gets. Feedback, ideas, suggestions, etc.
        //	  more than welcome
        $directory = save_individual($smarty, $sources, $compileDir);

        //DONE. The rest ist just for informational and playful purposes :
        //You can no go to the directory and use any program to extract the message-IDs
        //If you are using xgettext, use the following command
        // > cd /my/directory/with/translation/
        // > xgettext -n *.tpl --language=PHP

        print 'The following templates have been compiled into '.$directory.':'.PHP_EOL;
        foreach ($sources as $source) {
            print '	 - '.$source['file_original'].PHP_EOL;
        }

        if (count($info['errors']) > 0) {
            print PHP_EOL.PHP_EOL.'The following errors have occured:'.PHP_EOL;
            foreach ($info['errors'] as $error) {
                $exception = $error['exception'];
                $file      = $error['file'];
                print $file.' has the following error: '.$error['message'].PHP_EOL;
            }
        }
    }
}
