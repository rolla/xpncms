<?php

namespace Core\Includes;

use XpnCMS\Exceptions\ConfigException;

class Config
{
    public static $currentValue;

    private static $_instance = null;

    private function __construct()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static function set($key, $value)
    {
        global $conf;

        if (is_array($value)) {    //if value is an array update and existing variable
            $ARRAY = $conf['settings'][$key]; //save the current session array
            foreach ($value as $k => $v) { //for each key=value pair in the value passed in
                $ARRAY[$k] = $v; //push
            }
            $conf['settings'][$key] = $ARRAY;

            return;
        }
        $conf['settings'][$key] = $value;
    }

    // dot notation

    /**
     * @param $path
     * @param null $default
     * @return array|mixed
     * @throws ConfigException
     */
    public static function get($path, $default = null)
    {
        global $conf;
        $a = $conf['settings'];
        $current = $a;
        $p = strtok($path, '.');

        while ($p !== false) {
            if (!isset($current[$p])) {
                if ($default) {
                    return $default;
                } else {
                    throw new ConfigException('Config option ' . $path . ' not found');
                }
            }
            $current = $current[$p];
            $p = strtok('.');
        }

        return $current;
    }
}
