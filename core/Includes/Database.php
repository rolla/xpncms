<?php

namespace Core\Includes;

use PDO;
use PDOException;
use Exception;

class Database
{
    public $db_host = null;
    public $db_port = 3306;
    public $db_user = null;
    public $db_pass = null;
    public $db_name = null;
    public $db_char = null;
    public $db_prefix = null;

    private $dbh;
    private $error;
    private $stmt;

    public function __construct()
    {
        /*
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        );

        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
            // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
        */
    }

    public function connect()
    {

        // Set DSN
        $dsn = 'mysql:host='.$this->db_host.';port='.$this->db_port.';dbname='.$this->db_name;

        // Set options
        $options = array(
            //PDO::MYSQL_ATTR_FOUND_ROWS => true,
            //PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            //PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
            PDO::NULL_EMPTY_STRING,
            /* https://tech.michaelseiler.net/2016/07/04/dont-emulate-prepared-statements-pdo-mysql/ */
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES '.$this->db_char.'',
        );

        try {
            $this->dbh = new PDO($dsn, $this->db_user, $this->db_pass, $options);
        } // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
            $_SESSION['db_error'] = $this->error;
            throw new Exception('DB error: '.$this->error);
        }
    }

    public function query($query)
    {
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    /*
    $bindString = helper::bindParamArray("id", $_GET['ids'], $bindArray);
    $userConditions .= " AND users.id IN($bindString)";
    */

    public function bindParamArray($prefix, $values, &$bindArray)
    {
        $str = '';
        foreach ($values as $index => $value) {
            $str .= ':'.$prefix.$index.',';
            $bindArray[$prefix.$index] = $value;
        }

        return rtrim($str, ',');
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function resultset()
    {
        $this->execute();

        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function resultnum()
    {
        $this->execute();

        return $this->stmt->fetch(PDO::FETCH_NUM);
    }

    public function single()
    {
        $this->execute();

        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function rowCount()
    {
        return $this->stmt->rowCount();
    }

    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }

    public function beginTransaction()
    {
        return $this->dbh->beginTransaction();
    }

    public function endTransaction()
    {
        return $this->dbh->commit();
    }

    public function cancelTransaction()
    {
        return $this->dbh->rollBack();
    }

    public function debugDumpParams()
    {
        return $this->stmt->debugDumpParams();
    }

    public function errorInfo()
    {
        return $this->stmt->errorInfo();
    }

    public function getDBInstance()
    {
        return $this->dbh;
    }

    public function queryString()
    {
        return $this->stmt->queryString;
    }

    public function toSql()
    {
        return $this->debugDumpParams();
    }

    /*
    public function errors(){

        if($this->$stmt->errorCode() == 0) {
            while(($row = $this->$stmt->fetch()) != false) {
                $err[] = $row['country'] . "\n";
        }
        } else {
            $errors = $this->$stmt->errorInfo();
            $err[] = ($errors[2]);
        }

        return $err;
    }
    */
}
