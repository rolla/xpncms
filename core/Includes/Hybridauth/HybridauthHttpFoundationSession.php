<?php

namespace Core\Includes\Hybridauth;

use Hybridauth\Storage\StorageInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;

class HybridauthHttpFoundationSession implements StorageInterface
{

    private $session;

    public function __construct()
    {
        $this->setSession();
    }

    private function setSession()
    {
        $session = new Session();
        $myAttributeBag = new NamespacedAttributeBag('HybridauthStorage');
        $myAttributeBag->setName('Hybridauth');
        $session->registerBag($myAttributeBag);

        $this->session = $session->getBag('Hybridauth');
    }

    /**
     *
     * @param string $key
     * @return string
     */
    private function prefixedKey($key)
    {
        return $this->keyPrefix . strtolower($key);
    }

    /**
    * {@inheritdoc}
    */
    public function get($key)
    {
        return $this->session->get($this->prefixedKey($key), null);
    }

    /**
    * {@inheritdoc}
    */
    public function set($key, $value)
    {
        $this->session->set($this->prefixedKey($key), $value);
    }

    /**
    * {@inheritdoc}
    */
    public function clear()
    {
        $this->session->clear();
    }

    /**
    * {@inheritdoc}
    */
    public function delete($key)
    {
        $this->session->remove($this->prefixedKey($key));
    }

    /**
    * {@inheritdoc}
    */
    public function deleteMatch($key)
    {
        $key = $this->prefixedKey($key);

        $tmp = $this->session->all();
        foreach ($tmp as $k => $v) {
            if (strstr($k, $key)) {
                $this->session->remove($k);
            }
        }
    }
}
