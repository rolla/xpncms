<?php

namespace Core\Includes\Hybridauth;

use Hybridauth\Exception\InvalidArgumentException;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient\Util;
use Hybridauth\Exception\Exception as HybridauthException;
use Hybridauth\Adapter\AdapterInterface;
use Hybridauth\User\Profile;
use Core\Includes\Config;

class HybridauthWrapper
{

    /**
     *
     * @var Hybridauth
     */
    private static $instance;

    /**
     *
     * @var string|null
     */
    protected $redirectUrl = null;

    /**
     *
     * @var HybridauthHttpFoundationSession
     */
    protected static $storage;

    /**
     * HybridauthWrapper constructor.
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
        $this->redirectUrl = Config::get('base.url') . '?skipAuth=true';

        /* for debugging */
        // print_r($this->getInstance());
        // exit;

        $this->getInstance();
    }

    /**
     *
     * @return Hybridauth
     * @throws InvalidArgumentException
     */
    public final static function getInstance(): Hybridauth
    {
        global $hybridauthConfig;

        if(!self::$instance) {
            self::$storage = new HybridauthHttpFoundationSession;
            self::$instance = new Hybridauth($hybridauthConfig, null, self::$storage);
        }

        return self::$instance;
    }

    /**
     *
     * @param string|null $provider
     * @return AdapterInterface;
     * @throws HybridauthException
     */
    public function authenticate(string $provider = null): AdapterInterface
    {
        try {
            if (!$provider) {
                Util::redirect($this->redirectUrl);
            }

            $adapter = $this->getInstance()->authenticate($provider);
            self::$storage->delete('authWithProvider');

            return $adapter;
        } catch (HybridauthException $e) {
            throw new HybridauthException("Ooophs, we got an error: " . $e->getMessage());
        }
    }

    /**
     *
     * @param string|null $provider
     * @return Profile
     * @throws HybridauthException
     */
    public function getUserProfile(string $provider = null): Profile
    {
        if (!$provider) {
            Util::redirect($this->redirectUrl);
        }

        $adapter = $this->authenticate($provider);

        return $adapter->getUserProfile();
    }
}
