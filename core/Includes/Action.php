<?php

namespace Core\Includes;

use XpnCMS\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;
use Carbon\Carbon;
use XpnCMS\Plugins\gradio\Model\Gradio;
use XpnCMS\Model\Content;

class Action
{

    private $_opposite_actions = [
        'liked' => 'disliked',
    ];

    private $opposite_action_ids = [
        '1' => '2',
    ];

    /**
     *
     * @var User
     */
    private $userRepo;

    /**
     *
     * @var Gradio
     */
    private $gradioRepo;

    /**
     *
     * @var Content
     */
    private $contentRepo;

    public function __construct()
    {
        $this->userRepo = new User;
        $this->gradioRepo = new Gradio;
        $this->contentRepo = new Content;
    }

    private function getOppositeAction($action)
    {
        if (array_key_exists($action, $this->_opposite_actions)) {
            return $this->_opposite_actions[$action];
        }

        if ($key = array_search($action, $this->_opposite_actions)) {
            return $key;
        }
    }

    private function getOppositeActionById($actionId)
    {
        if (array_key_exists($actionId, $this->opposite_action_ids)) {
            return $this->opposite_action_ids[$actionId];
        }

        if ($key = array_search($actionId, $this->opposite_action_ids)) {
            return $key;
        }
    }

    private function getActionIDByAlias($alias)
    {
        global $db1;

        $db1->query('SELECT id FROM ' . $db1->db_prefix . 'actions WHERE action LIKE :alias');
        $db1->bind(':alias', $alias);
        $found = $db1->single();

        return $found['id'];
    }

    private function getActionCategoryIDByAlias($alias)
    {
        global $db1;

        $db1->query('SELECT id FROM ' . $db1->db_prefix . 'actions_categories WHERE category LIKE :alias');
        $db1->bind(':alias', $alias);
        $found = $db1->single();

        return $found['id'];
    }

    public function addAction($action_id = 12, $actions_category_id = 1, $item_id = null, $item_ids = null)
    {
        global $user, $actiondb;

        $user_id = $user->getUser();
        $added = $datetime = date_create($date . $time)->format('Y-m-d H:i:s');

        $actiondb->query('INSERT INTO gradio_actions (action_id, actions_category_id, user_id, item_id, item_ids, added) VALUES (:action_id, :actions_category_id, :user_id, :item_id, :item_ids, :added)');

        //to safely serialize
        //$safe_string_to_store = base64_encode(serialize($multidimensional_array));
        //to unserialize...
        //$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));

        if (isset($item_ids) && !empty($item_ids)) {
            $item_ids = base64_encode(serialize($item_ids));
        } else {
            $item_ids = null;
        }

        if (isset($item_id) && !empty($item_id)) {
            $item_id = $item_id;
        } else {
            $item_id = null;
        }

        $actiondb->bind(':action_id', $action_id);
        $actiondb->bind(':actions_category_id', $actions_category_id);
        $actiondb->bind(':user_id', $user_id['id']);
        $actiondb->bind(':item_id', $item_id);
        $actiondb->bind(':item_ids', $item_ids);
        $actiondb->bind(':added', $added);

        //$add = $dbimport->execute();

        try {
            $add = $actiondb->execute();

            $result['action']['added']['success'][] = ['id' => $actiondb->lastInsertId(), 'type' => 'success'];

            /*
              $tunes_genres_success[] = array('songID' => $songID, 'filename' => $ThisFileInfo[$i]['filenamepath'], 'genres' => $genres);

              $result['tunes']['genres']['success'][$i] = array('songID' => $songID, 'filename' => $ThisFileInfo[$i]['filenamepath'], 'genres' => $genres);
             */
        } catch (PDOException $e) {
            $result['action']['added']['error'][] = ['error' => 'adding action', 'error_msg' => $e->getMessage()];
            /*
              $result['tunes']['genres']['error'][$i] = array('songID' => $songID, 'filename' => $ThisFileInfo[$i]['filenamepath'], 'genres' => $genres);

              if ($e->errorInfo[1] == 1062) { // duplicate entry, do something else

              }
             */
        }

        //$this->debug($result);

        return $result;
    }

    /**
     * @param $actionId
     * @param $categoryId
     * @param $itemId
     * @return mixed
     */
    private function getLastRating($actionId, $categoryId, $itemId, $userId)
    {
        global $db1;
        $query = 'select * from ' . $db1->db_prefix . 'action_category ac
            where ac.item_id is not null and ac.action_id=:action_id and ac.category_id=:category_id and ac.user_id=:user_id and ac.item_id=:item_id order by created_at desc limit 0, 1';

        $db1->query($query);
        $db1->bind(':action_id', $actionId);
        $db1->bind(':category_id', $categoryId);
        $db1->bind(':user_id', $userId);
        $db1->bind(':item_id', $itemId);

        return $db1->single();
    }

    public static function addActivity($action = 'liked', $category = 'tune', $itemId = null, $itemIds = null)
    {
        $actionRepo = new Action;

        return $actionRepo->addHumanAction($action, $category, $itemId, $itemIds);
    }

    /**
     * @param string $action
     * @param string $category
     * @param integer $itemId
     * @param array|null $itemIds
     */
    public function addHumanAction($action = 'liked', $category = 'tune', $itemId = null, $itemIds = null)
    {
        global $db1;

        $session = new Session();
        $currentUserId = User::authId(); // if not real 0 - guest
        $created_at = date_create()->format('Y-m-d H:i:s');
        $result = [];

        $item_id = null;
        $item_ids = null;

        if (isset($itemId) && !empty($itemId)) {
            $item_id = $itemId;
        }

        if (isset($itemIds) && !empty($itemIds)) {
            $item_ids = base64_encode(serialize($itemIds));
        }

        $action_id = $this->getActionIDByAlias($action);
        $category_id = $this->getActionCategoryIDByAlias($category);

        if (!$action_id && !$category_id) {
            $result['action']['added']['error'][] = [
                'error_msg' => 'missing action_id or category_id',
                'action_id' => isset($action_id) ?? null,
                'category_id' => isset($category_id) ?? null,
            ];

            return $result;
        }

        $existsData = $this->existsByActionIds('1,2', $category_id, $item_id, $currentUserId);
        $exists = false;
        $oppositeExists = false;

        if (count($existsData)) {
            foreach ($existsData as $key => $existsValue) {
                if ($existsValue['action_id'] == $action_id) {
                    $exists = $existsData[$key];
                }

                if ($existsValue['action_id'] == $this->getOppositeActionById($action_id)) {
                    $oppositeExists = $existsData[$key];
                }
            }
        }

        if ($exists) {
            $db1->query('UPDATE ' . $db1->db_prefix . 'action_category
                   SET action_id=:action_id, category_id=:category_id, updated_at=:updated_at, deleted_at=:deleted_at
                   WHERE id=:id');
            $db1->bind(':id', $exists['id']);
            $db1->bind(':action_id', $action_id);
            $db1->bind(':category_id', $category_id);
            $db1->bind(':updated_at', $created_at);
            $db1->bind(':deleted_at', $created_at);

            try {
                $add = $db1->execute();
                $result['action']['updated']['success'][] = [
                    'id' => $exists['id'],
                    'prev_action' => $exists['action'],
                    'prev_action_id' => $exists['action_id']
                ];
            } catch (PDOException $e) {
                $result['action']['updated']['error'][] = ['error_msg' => $e->getMessage()];

                $error[] = [
                    'when' => date_create()->format('Y-m-d H:i:s'),
                    'line' => __LINE__,
                    'file' => __FILE__,
                    'dir' => __DIR__,
                    'function' => __FUNCTION__,
                    'class' => __CLASS__,
                    'trait' => __TRAIT__,
                    'method' => __METHOD__,
                    'namespace' => __NAMESPACE__,
                    'error_msg' => $e->getMessage(),
                ];
                $session->set('errors', $error);
            }
        }

        if ($oppositeExists) {
            $db1->query('UPDATE ' . $db1->db_prefix . 'action_category
                        SET deleted_at=:deleted_at
                        WHERE id=:id');
            $db1->bind(':id', $oppositeExists['id']);
            $db1->bind(':deleted_at', $created_at);

            try {
                $add = $db1->execute();
                $result['action']['changed']['success'][] = [
                    'id' => $oppositeExists['id'],
                    'prev_action' => $oppositeExists['action'],
                    'prev_action_id' => $oppositeExists['action_id']
                ];
            } catch (PDOException $e) {
                $result['action']['changed']['error'][] = ['error_msg' => $e->getMessage()];

                $error[] = [
                    'when' => date_create()->format('Y-m-d H:i:s'),
                    'line' => __LINE__,
                    'file' => __FILE__,
                    'dir' => __DIR__,
                    'function' => __FUNCTION__,
                    'class' => __CLASS__,
                    'trait' => __TRAIT__,
                    'method' => __METHOD__,
                    'namespace' => __NAMESPACE__,
                    'error_msg' => $e->getMessage(),
                ];
                $session->set('errors', $error);
            }
        }

        $result['sendSlackHook'] = true;
        $lastSameRating = $this->getLastRating($action_id, $category_id, $item_id, $currentUserId);
        $lastRatingDate = $lastSameRating ? Carbon::parse($lastSameRating['created_at']) : Carbon::now();
        $lastSameRatingDiffInHours = $lastRatingDate->diffInHours(Carbon::now());

        if ($exists || $lastSameRating && $lastSameRatingDiffInHours < 1) {
            $result['sendSlackHook'] = false;
        }

        $db1->query('INSERT INTO ' . $db1->db_prefix . 'action_category (action_id, category_id, user_id, item_id, item_ids, created_at) VALUES (:action_id, :category_id, :user_id, :item_id, :item_ids, :created_at)');
        $db1->bind(':action_id', $action_id);
        $db1->bind(':category_id', $category_id);
        $db1->bind(':user_id', $currentUserId > 0 ? $currentUserId : null);
        $db1->bind(':item_id', $item_id);
        $db1->bind(':item_ids', $item_ids);
        $db1->bind(':created_at', $created_at);

        $add = $db1->execute();
        $result['action']['added']['success'][] = ['id' => $db1->lastInsertId()];

        if ($category == 'tune' && ($action == 'liked' || $action == 'disliked')) {
            $this->gradioRepo->postRate($item_id, $currentUserId, $action);
        }

        return $result;
    }

    public function checkNewActions($username = 'guest')
    {
        global $db1, $session;

        //unset($_SESSION['actions']);
        //exit;

        $now = date_create()->format('Y-m-d H:i:s');

        if (!$session->get('actions', 'timestamp')) {
            $session->set('actions', ['timestamp' => $now]);

            //$username = $user->getUser();
            //$result = array('loggedin' => array('user' => $username));
        } else {
            $latest = $session->get('actions', 'timestamp');

            // now >= end >= latest

            $query = 'SELECT * FROM ' . $db1->db_prefix . 'action_category WHERE created_at <= "' . $now . '" AND created_at >= "' . $latest . '" OR updated_at <= "' . $now . '" AND updated_at >= "' . $latest . '" AND deleted_at IS NULL ORDER BY created_at DESC, updated_at DESC limit 0, 100';

            //echo $query;

            $db1->query($query);

            /*
              $db3->query("SELECT * FROM `tune_searches` WHERE start <= ':now' AND start >= ':latest' OR end <= ':now' AND end >= ':latest' LIMIT 0,1");
              $db3->bind(':now', $now);
              $db3->bind(':latest', $latest);
             */

            //echo $query;
            try {
                $checked = $db1->resultset();

                //$this->debug($checked);

                if ($checked) {
                    //$_SESSION['actions']['timestamp'] = $now;

                    /*
                      SELECT a.id, a.user_id, a.action_id, aids.action, a.actions_category_id, ac.actions_category, ac.actions_db, ac.actions_table, a.item_id, a.added, a.updated

                      FROM gradio_actions a

                      INNER JOIN gradio_actions_categories ac

                      ON a.actions_category_id = ac.actions_category_id

                      INNER JOIN gradio_action_ids aids

                      ON a.action_id = aids.action_id

                      WHERE a.id = 44
                     */

                    //foreach($checked as $key => $check){
                    $result = $this->getActionsByIds($checked);
                    //}
                } else {
                    $result = null;
                }
            } catch (PDOException $e) {

                //$_SESSION['actions']['error'][] = array('time' => $now, 'error' => $e);

                $errors[] = ['time' => $now, 'error' => $e];

                $session->set('actions', ['error' => $errors]);

                return;
            }

            //print_r($checked);
        }

        //echo '<pre>';
        //print_r($_SESSION);
        //echo json_encode($result);
        //$result = null;
        return $result;
    }

    public function getActionsByIds($ids)
    {
        global $db1;

        /*
        $ids = [
            ['id' => 704114],
            ['id' => 643745],
        ];
         */

        $checkIds = array_column($ids, 'id');

        $query = 'select'
                . ' a.id, a.user_id, aids.action, aids.icon, ac.category, ac.icon as ac_icon, ac.db, ac.db_table, a.item_id, a.item_ids, a.created_at, a.updated_at, a.deleted_at
                from ' . $db1->db_prefix . 'action_category a
                left join ' . $db1->db_prefix . 'actions_categories ac
                on a.category_id = ac.id
                left join ' . $db1->db_prefix . 'actions aids
                on a.action_id = aids.id
                where a.id in (' . implode(',', $checkIds) . ') order by a.created_at desc';
        $db1->query($query);

        try {
            $newActionArr['human'] = $db1->resultset();
        } catch (PDOException $e) {
            $errors[] = ['time' => date('Y-m-d H:i:s'), 'error' => $e];
            // $session->set('actions', ['error' => $errors]);

            return;
        }

        $newActionArr['system'] = $checkIds;

        return $this->processActions($newActionArr);
    }

    public function getActivities(int $userId, int $start, int $limit)
    {
        $actionsIds = $this->userRepo->getActionsIds($userId, $start, $limit);
        if (! count($actionsIds)) {
            return [];
        }

        return $this->getActionsByIds($actionsIds);
    }

    public function getFreshActivities(int $userId, int $lastId)
    {
        $actionsIdsByLastId = $this->userRepo->getActionsIdsByLastId($userId, $lastId);
        if (!$lastId || ! count($actionsIdsByLastId)) {
            return [];
        }

        return $this->getActionsByIds($actionsIdsByLastId);
    }

    public function exists($action = 'liked', $category = 'tune', $item_id = null)
    {
        global $db1;

        $queryCriteria = '"' . implode('","', $action) . '"'; // eg makes "liked","disliked"
        $query = 'SELECT
            a.id,
            a.action_id,
            a.user_id,
            aids.action,
            aids.icon,
            ac.category,
            ac.icon,
            ac.db,
            ac.db_table,
            a.item_id,
            a.item_ids,
            a.created_at,
            a.updated_at,
            a.deleted_at
            from ' . $db1->db_prefix . 'action_category a
            left join ' . $db1->db_prefix . 'actions_categories ac
            on a.category_id = ac.id
            left join ' . $db1->db_prefix . 'actions aids
            on a.action_id = aids.id
            where aids.action in (' . $queryCriteria . ') and ac.category=:category and a.item_id=:item_id and a.user_id=:user_id and a.deleted_at is null';

        $db1->query($query);
        $db1->bind(':category', $category);
        $db1->bind(':item_id', $item_id);
        $db1->bind(':user_id', User::authId());

        $result = $db1->resultset();

        if ($result) {
            return $result;
        }

        return [];
    }

    public function existsByActionIds($actionids, $categoryId = '1', $item_id = null, $userId)
    {
        global $db1;

        $query = 'SELECT
            a.id,
            a.action_id,
            a.user_id,
            a.item_id,
            a.item_ids,
            a.created_at,
            a.updated_at,
            a.deleted_at
            from ' . $db1->db_prefix . 'action_category a
            where a.action_id in (' . $actionids . ') and a.category_id=:category_id and a.item_id=:item_id and a.user_id=:user_id and a.deleted_at is null';

        $db1->query($query);
        $db1->bind(':category_id', $categoryId);
        $db1->bind(':item_id', $item_id);
        $db1->bind(':user_id', $userId);

        $result = $db1->resultset();

        if ($result) {
            return $result;
        }

        return [];
    }

    private function processAction(string $inputAction)
    {
        switch ($inputAction) {
            case 'listen':
                $action = _('listening');
                break;
            case 'started':
                $action = _('starts');
                break;
            case 'loggedin':
                $action = _('comes online');
                break;
            case 'loggedout':
                $action = _('goes offline');
                break;
            default:
                $action = $inputAction;
        }

        return $action;
    }

    private function processActionCategory($actionCategory)
    {
        $itemIds = [$actionCategory['item_id']];

        if (is_array($actionCategory['item_ids']) && count($actionCategory['item_ids'])) {
            $itemIds = unserialize(base64_decode($actionCategory['item_ids']));
        }

        switch ($actionCategory['category']) {
            case 'tune':
                $actionCategory['category'] = _('tune');
                if (count($itemIds) > 1) {
                   $actionCategory['category'] = _('tunes');
                }
                foreach ($itemIds as $key => $id) {
                    $actionCategory['item_data'][] = $this->gradioRepo->getSongByID($id);
                }
                break;
            case 'tune_info':
                $actionCategory['category'] = _('tune info');
                switch ($actionCategory['action']) {
                    case 'added':
                        foreach ($itemIds as $key => $id) {
                            $actionCategory['item_data'][] = $this->gradioRepo->getTuneBySongInformationId($id);
                        }
                    break;
                    case 'updated':
                        foreach ($itemIds as $key => $id) {
                            $actionCategory['item_data'][] = $this->gradioRepo->getSongByID($id);
                        }
                }
                break;
            case 'site':
                $actionCategory['category'] = '';
                break;
            case 'news':
                $actionCategory['category'] = 'news';
                foreach ($itemIds as $key => $id) {
                    $actionCategory['item_data'][] = $this->contentRepo->getContentRatings($id);
                }
                break;
        }

        return $actionCategory;
    }

    public function processActions(array $newActionArr)
    {
        global $time;

        $userIds = array_unique(array_column($newActionArr['human'], 'user_id'));
        $users = $this->userRepo->getUsers($userIds);

        $result = [];

        foreach ($newActionArr['human'] as $key => $newAction) {
            $userId = $newAction['user_id'] ?? null;
            $userInfos = array_filter($users, function ($user) use ($userId) {
                return $user['id'] === $userId;
            });
            $userInfo = $userInfos[0] ?? null;

            $result['system'][] = $newAction;

            $action = $this->processAction($newAction['action']);
            $actionCategory = $this->processActionCategory($newAction);

            $result['human'][] = [
                    'id' => $newAction['id'],
                    'datetime_relative' => $time->relativeTime($newAction['created_at']),
                    'datetime' => $newAction['created_at'],
                    'user' => $userInfo['display_name'],
                    'user_id' => $newAction['user_id'],
                    'action' => $action,
                    'icon' => $newAction['icon'],
                    'ac_icon' => $newAction['ac_icon'],
                    'for' => $actionCategory['category'],
                    'identificator' => $newAction['category'] . '_' . $newAction['action'],
                    'item_data' => isset($actionCategory['item_data']) ? $actionCategory['item_data'] : null,
            ];
        }

        return $result;
    }

    public function saveNotifies($data)
    {
        global $user;

        $data_arr = [];
        $username = $user->getUser();
        $user_id = $username['id'];

        //$data_arr[] = $data;
        //var_dump($data_arr);
        //unset($_SESSION['notifies']);

        if (!isset($_SESSION['notifies'])) {
            $_SESSION['notifies']['user'][$user_id]['notifies'][] = $data;
        } else {
            $_SESSION['notifies']['user'][$user_id]['notifies'][] = $data;
        }
    }

    // (string) $message - message to be passed to Slack
    // (string) $hookUrl - webhook endpoint from your Slack settings
    // (string) $room - room in which to write the message, too
    // (string) $icon - You can set up custom emoji icons to use with each message
    // (array) $attachments - You can set up custom attachments
    public function slackHook($message, $hookUrl, $room, $icon = null, $attachments = [])
    {
        $room = ($room) ? $room : "#random";
        $icon = ($icon) ? $icon : ":notes:";

        $dataArr = [
            "username" => APP_TITLE . ' - ' . APP_DESCRIPTION,
            "channel"       =>  "{$room}",
            "text"          =>  $message,
            "icon_emoji" => $icon,
            "unfurl_links" => "true",
            "unfurl_media" => "true",
        ];
        if (count($attachments)) {
            $dataArr['attachments'] = $attachments;
        }
        $data = json_encode($dataArr);
        $readyData = ['payload' => $data];

        $ch = curl_init($hookUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $readyData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
