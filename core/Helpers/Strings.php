<?php

namespace Core\Helpers;
use Core\Helpers\Seo;

class Strings
{
    public function getYoutubeId($url)
    {
        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

        return $matches[1];
    }

    public function getPublisherLogo($publisher) {

        $seoRepo = new Seo;

        return $publisher ? $seoRepo->generate_seo_link($publisher) . '.svg' : null;
    }
}
