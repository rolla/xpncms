<?php

namespace Core\Helpers;

class Debug
{
    public function Display($data, $return = false, $exit = false)
    {
        $res = '<pre>';
        $res .= print_r($data, true);
        $res .= '</pre>';
        if ($return) {
            return $res;
        } else {
            echo $res;
            if ($exit) {
                exit('exiting from Debug!');
            }
        }
    }

    public function Add($data)
    {
        $_SESSION['debug'][] = $data;
    }

    public function Clear()
    {
        unset($_SESSION['debug']);
    }

    public function debugConsole()
    {
        global $conf, $smarty;

        $html = '<div class="debug-console">';
        $html .= '	<div class="debug-console-inner">';

        $html .= '	<div class="debug-console-title-smarty col-md-6 col-lg-4">Smarty';
        $html .= '		<div class="debug-console-smarty-inner" style="position: relative; height: 500px; overflow: hidden;">';
        $html .= '			<pre>'.print_r($smarty->getTemplateVars(), true).'</pre>';
        $html .= '		</div>';
        $html .= '	</div>';

        $html .= '	<div class="debug-console-title-smarty col-md-6 col-lg-4">Config';
        $html .= '		<div class="debug-console-config-inner" style="position: relative; height: 500px; overflow: hidden;">';
        $html .= '			<pre>'.print_r($conf, true).'</pre>';
        $html .= '		</div>';
        $html .= '	</div>';

        $html .= '	<div class="debug-console-title-session col-md-6 col-lg-4">Session';
        $html .= '		<div class="debug-console-session-inner" style="position: relative; height: 500px; overflow: hidden;">';
        $html .= '			<pre>'.print_r($_SESSION, true).'</pre>';
        $html .= '		</div>';
        $html .= '	</div>';

        $html .= '	<div class="debug-console-title-cookies col-md-6 col-lg-4">Cookies';
        $html .= '		<div class="debug-console-cookies-inner" style="position: relative; height: 500px; overflow: hidden;">';
        $html .= '			<pre>'.print_r($_COOKIES, true).'</pre>';
        $html .= '		</div>';
        $html .= '	</div>';

        $html .= '	<div class="debug-console-title-javascript col-md-6 col-lg-4">Javascript';
        $html .= '		<div class="debug-console-javascript-inner" style="position: relative; height: 500px; overflow: hidden;">';
        $html .= '			<pre></pre>';
        $html .= '		</div>';
        $html .= '	</div>';

        $html .= '	</div>';
        $html .= '</div>';

        return $html;
    }
}
