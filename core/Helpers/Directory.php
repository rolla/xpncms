<?php

namespace Core\Helpers;

class Directory {

    /**
      Recursion !!
     */
    public function makePath($path, $chmod = 0775)
    {
        if (mkdir($path, $chmod, true)){
            return true;
        }
        
        return false;
    }

    public function mkpath($path, $chmod = 0775)
    {
        if (@mkdir($path, $chmod) or file_exists($path))
        {
            return true;
        }

        return ($this->mkpath(dirname($path)) and mkdir($path, $chmod));
    }

}
