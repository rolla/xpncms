<?php

namespace Core\Helpers;
use Carbon\Carbon;

class Time
{
    public function format_time($mytime)
    {
        //return date("h:i:s.u", $mytime / 1000);

        //$duration = round($mytime / 1000);

        /*

        if(minutes < 10){
            var curr_mins = "0"+minutes;
        }else{
            var curr_mins = minutes;
        }

        if(seconds < 10){
            var curr_secs = "0"+seconds;
        }else{
            var curr_secs = seconds;
        }
        */

        return date('i:s', $mytime / 1000);
        //return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
    }

    /**
     * Convert number of seconds into hours, minutes and seconds
     * and return an array containing those values.
     *
     * @param int $inputSeconds Number of seconds to parse
     *
     * @return array
     */
    public function secondsToTime($inputSeconds)
    {

        //$duration = round($mytime / 1000);

        $secondsInAMinute = 60;
        $secondsInAnHour = 60 * $secondsInAMinute;
        $secondsInADay = 24 * $secondsInAnHour;

        // extract days
        $days = floor($inputSeconds / $secondsInADay);

        // extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours = floor($hourSeconds / $secondsInAnHour);

        // extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes = floor($minuteSeconds / $secondsInAMinute);

        // extract the remaining seconds
        $remainingSeconds = $minuteSeconds % $secondsInAMinute;
        $seconds = ceil($remainingSeconds);

        // return the final array
        $obj = array(
            'd' => (int) $days,
            'h' => (int) $hours,
            'm' => (int) $minutes,
            's' => (int) $seconds,
        );

        if ($obj['h']) {
            $hours = $obj['h'].':';
        } else {
            $hours = '';
        }

        if ($obj['m']) {
            if ($obj['m'] < 10) {
                $mins = '0'.$obj['m'].':';
            } else {
                $mins = $obj['m'].':';
            }
        } else {
            $mins = '00:';
        }

        if ($obj['s'] < 10) {
            $secs = '0'.$obj['s'];
        } else {
            $secs = $obj['s'];
        }

        $res = $hours.$mins.$secs;

        //echo '<pre>';
        //print_r($res);
        //exit();

        return $res;
        //return $obj;
    }

    public function relativeTime($timestamp, $remove_ago = false)
    {
        return Carbon::parse($timestamp)->diffForHumans(null, $remove_ago);
    }
}
