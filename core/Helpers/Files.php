<?php namespace Core\Helpers;

use RecursiveDirectoryException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use FilesystemIterator;
use Exception;

class Files
{

    /**
     * Get IMAGE files recursively from Root and all sub-folders
     * Skip folders in our list of results
     * LEAVES_ONLY mode makes sure ONLY FILES/Leafs endpoint is returned
     * Make sure file extension is in our Images extensions array.
     */
    public function getFileCountRecursive($path, $extensions)
    {

        //$path = 'E:\Server\_ImageOptimize\img\testfiles';

        if (!file_exists($path)) {
            throw new RecursiveDirectoryException('Directory ' . $path . ' doesn\'t exist.');
        }

        $directory = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::LEAVES_ONLY);

        //$extensions = array("png", "jpg", "jpeg", "gif", "gifgif");

        foreach ($iterator as $fileinfo) {
            if (in_array($fileinfo->getExtension(), $extensions)) {
                //echo 'File path = ' .$fileinfo->getPathname(). NL;
                //echo 'File extension = ' .$fileinfo->getExtension(). NL;
                $files[] = $fileinfo->getPathname();
            }
        }

        if ($files) {
            return count($files);
        }

        return false;
    }

    public function deleteFilesRecursive($path, $extensions = '')
    {
        if (!file_exists($path)) {
            throw new RecursiveDirectoryException('Directory ' . $path . ' doesn\'t exist.');
        }

        $errors;
        $deleted = 0;
        $deleted_size = 0;

        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($iterator as $fileinfo) {
            if (in_array($fileinfo->getExtension(), $extensions)) {
                if ($fileinfo->isFile()) {
                    $deleted_size += filesize($fileinfo);
                    if (unlink((string) $fileinfo)) {
                        ++$deleted;
                    } else {
                        $errors[] = 'cannot delete file ' . (string) $fileinfo;
                    }
                }
            }
        }

        $result['deleted'] = $deleted;
        $result['size_bytes'] = $deleted_size;
        $result['errors'] = $errors;

        return $result;
    }

    public function deleteDirectoriesRecursive($path)
    {
        if (!file_exists($path)) {
            throw new RecursiveDirectoryException('Directory ' . $path . ' doesn\'t exist.');
        }

        $errors;
        $deleted = 0;
        $deleted_size = 0;
        $deleted_dirs = 0;

        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) {
                $deleted_size += filesize($fileinfo);
                if (unlink((string) $fileinfo)) {
                    ++$deleted;
                } else {
                    $errors[] = 'cannot delete file ' . (string) $fileinfo;
                }
            }

            if ($fileinfo->isDir()) {
                if (rmdir((string) $fileinfo)) {
                    ++$deleted_dirs;
                } else {
                    $errors[] = 'cannot delete directory ' . (string) $fileinfo;
                }
            }
        }

        //rmdir($path);

        $result['deleted'] = $deleted;
        $result['size_bytes'] = $deleted_size;
        $result['deleted_dirs'] = $deleted_dirs;
        $result['errors'] = $errors;

        return $result;
    }

    public function getRemoteFile($url, $destination = APP_ROOT, $rename = true)
    {
        $headers = get_headers($url);
        $strippedUrl = preg_replace('/\\?.*/', '', $url);
        foreach ($headers as $header) {
            if (strpos(strtolower($header), 'content-disposition') !== false) {
                $tmpName = explode('=', $header);
                if ($tmpName[1]) {
                    $strippedUrl = trim($tmpName[1], '";\'');
                }
            }
        }
        $extension = pathinfo($strippedUrl, PATHINFO_EXTENSION);

        if (!$extension) {
            $headersArr = get_headers($url, 1);
            $extension = $headersArr['Content-Type'][0];
            $extension = explode('/', $extension);
            $extension = $extension[1];
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        if ($rename) {
            $seo = new Seo();
            $filename = $seo->generateRandomString();
        } else {
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($strippedUrl));
            $filename = $withoutExt;
        }

        $fullDestination .= $destination . $filename . '.' . $extension;

        $file = @fopen($fullDestination, "w+");
        $write = @fwrite($file, $data);
        if (!$write) {
            throw new Exception('Cannot create file ' . $fullDestination . ' check permissons!');
        }
        fclose($file);

        $result = [
            'headers' => $headers,
            'file' => [
                'path' => $destination,
                'name' => $filename,
                'extension' => $extension,
                'filename' => $filename . '.' . $extension
            ],
            'remoteFile' => $url
        ];

        return json_decode(json_encode($result), false); //object
    }

    /**
     * Returns the size of a file without downloading it, or -1 if the file
     * size could not be determined.
     *
     * @param $url - The location of the remote file to download. Cannot
     * be null or empty.
     *
     * @return The size of the file referenced by $url, or -1 if the size
     *             could not be determined.
     */
    public function getRemoteFileSize($url)
    {
        // Assume failure.
        $result = -1;

        $curl = curl_init($url);

        // Issue a HEAD request and follow any redirects.
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt( $curl, CURLOPT_USERAGENT, get_user_agent_string() );

        $data = curl_exec($curl);
        curl_close($curl);

        if ($data) {
            $content_length = 'unknown';
            $status = 'unknown';

            if (preg_match("/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches)) {
                $status = (int) $matches[1];
            }

            if (preg_match("/Content-Length: (\d+)/", $data, $matches)) {
                $content_length = (int) $matches[1];
            }

            // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            if ($status == 200 || ($status > 300 && $status <= 308)) {
                $result = $content_length;
            }
        }

        return $result;
    }

    // Returns a file size limit in bytes based on the PHP upload_max_filesize
    // and post_max_size
    public function fileUploadMaxSize()
    {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $max_size = $this->parseSize(ini_get('post_max_size'));

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = $this->parseSize(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    protected function parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }
}
