<?php

namespace Core\Helpers;

class Forms
{
    public function checkCheckbox($name)
    {
        if (isset($name) && !empty($name) && $name == 'on') {
            return true;
        }

        return false;
    }
}
