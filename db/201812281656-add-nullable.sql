alter table gradio_sessions change clicks clicks int(11) null;
alter table gradio_ratings change nickname nickname varchar(100) character set utf8 collate utf8_unicode_ci null;
alter table gradio_ratings change comment comment varchar(255) character set utf8 collate utf8_unicode_ci null;
