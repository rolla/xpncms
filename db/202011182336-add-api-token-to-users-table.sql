ALTER TABLE `gradio_users` ADD `api_token` VARCHAR(80) NULL DEFAULT NULL AFTER `type`;
ALTER TABLE `gradio_users` ADD UNIQUE(`api_token`);
