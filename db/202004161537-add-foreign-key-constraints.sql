ALTER TABLE `gradio_action_category` DROP INDEX `id`;
ALTER TABLE `gradio_action_category` ADD CONSTRAINT `action_category_actions_id` FOREIGN KEY (`action_id`) REFERENCES `gradio_actions`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `gradio_action_category` ADD CONSTRAINT `action_category_actions_categories_id` FOREIGN KEY (`category_id`) REFERENCES `gradio_actions_categories`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `gradio_action_category` CHANGE `user_id` `user_id` INT(11) NULL DEFAULT NULL;
update gradio_action_category set user_id=null where user_id=0;
/* delete from gradio_action_category where user_id <= 109; */

delete ac.id, ac.action_id, ac.category_id, ac.user_id as ac_user_id, u.id as user_id FROM gradio_action_category ac
left join gradio_users u
on ac.user_id = u.id
where u.id is null and ac.user_id is not null and ac.user_id <= 109;

ALTER TABLE `gradio_action_category` ADD  CONSTRAINT `action_category_users_id` FOREIGN KEY (`user_id`) REFERENCES `gradio_users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
