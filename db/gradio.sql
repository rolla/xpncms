
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- songs
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `songs`;

CREATE TABLE `songs`
(
    `ID` INTEGER NOT NULL AUTO_INCREMENT,
    `path` VARCHAR(255) NOT NULL,
    `enabled` INTEGER(1) DEFAULT 0 NOT NULL,
    `date_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `artist_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `count_played` SMALLINT(9) DEFAULT 0 NOT NULL,
    `play_limit` INTEGER DEFAULT 0 NOT NULL,
    `limit_action` INTEGER(1) DEFAULT 0 NOT NULL,
    `start_date` DATETIME DEFAULT '2002-01-01 00:00:01',
    `end_date` DATETIME DEFAULT '2002-01-01 00:00:01',
    `song_type` TINYINT(2) NOT NULL,
    `id_subcat` INTEGER NOT NULL,
    `id_genre` INTEGER NOT NULL,
    `weight` DOUBLE(5,1) DEFAULT 50.0 NOT NULL,
    `duration` DOUBLE(11,5) NOT NULL,
    `cue_times` VARCHAR(255) DEFAULT '&' NOT NULL,
    `precise_cue` TINYINT(1) DEFAULT 0 NOT NULL,
    `fade_type` TINYINT(1) DEFAULT 0 NOT NULL,
    `end_type` TINYINT(1) DEFAULT 0 NOT NULL,
    `overlay` TINYINT(1) DEFAULT 0 NOT NULL,
    `artist` VARCHAR(255) NOT NULL,
    `original_artist` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `album` VARCHAR(255) NOT NULL,
    `composer` VARCHAR(255) NOT NULL,
    `year` VARCHAR(4) DEFAULT '1900' NOT NULL,
    `track_no` SMALLINT DEFAULT 0 NOT NULL,
    `disc_no` SMALLINT DEFAULT 0 NOT NULL,
    `publisher` VARCHAR(255) NOT NULL,
    `copyright` VARCHAR(255) NOT NULL,
    `isrc` VARCHAR(255) NOT NULL,
    `bpm` DOUBLE(11,1) NOT NULL,
    `comments` TEXT,
    `sweepers` VARCHAR(250),
    `album_art` VARCHAR(255) DEFAULT 'no_image.jpg' NOT NULL,
    `buy_link` VARCHAR(255) DEFAULT 'http://' NOT NULL,
    `tdate_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `tartist_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `date_added` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    `version_created_at` DATETIME,
    `version_created_by` VARCHAR(100),
    `version_comment` VARCHAR(255),
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `path` (`path`),
    INDEX `title` (`title`),
    INDEX `artist` (`artist`),
    INDEX `date_played` (`date_played`),
    INDEX `artist_played` (`artist_played`),
    INDEX `count_played` (`count_played`),
    INDEX `id_subcat` (`id_subcat`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- songs_information
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `songs_information`;

CREATE TABLE `songs_information`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `song_id` int(10) unsigned NOT NULL,
    `artist` VARCHAR(255),
    `title` VARCHAR(255),
    `remix` VARCHAR(255),
    `label` VARCHAR(255),
    `genre` INTEGER,
    `url` VARCHAR(1000),
    `beatport` VARCHAR(1000),
    `soundcloud` VARCHAR(1000),
    `youtube` VARCHAR(1000),
    `itunes` VARCHAR(1000),
    `cover` TEXT,
    `added_by` INTEGER,
    `user_id` INTEGER,
    `status` TINYINT,
    `start` DATETIME,
    `end` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `deleted_at` DATETIME,
    `version` INTEGER DEFAULT 0,
    `version_created_at` DATETIME,
    `version_created_by` VARCHAR(100),
    `version_comment` VARCHAR(255),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `song_id` (`song_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- songs_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `songs_version`;

CREATE TABLE `songs_version`
(
    `ID` INTEGER NOT NULL,
    `path` VARCHAR(255) NOT NULL,
    `enabled` INTEGER(1) DEFAULT 0 NOT NULL,
    `date_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `artist_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `count_played` SMALLINT(9) DEFAULT 0 NOT NULL,
    `play_limit` INTEGER DEFAULT 0 NOT NULL,
    `limit_action` INTEGER(1) DEFAULT 0 NOT NULL,
    `start_date` DATETIME DEFAULT '2002-01-01 00:00:01',
    `end_date` DATETIME DEFAULT '2002-01-01 00:00:01',
    `song_type` TINYINT(2) NOT NULL,
    `id_subcat` INTEGER NOT NULL,
    `id_genre` INTEGER NOT NULL,
    `weight` DOUBLE(5,1) DEFAULT 50.0 NOT NULL,
    `duration` DOUBLE(11,5) NOT NULL,
    `cue_times` VARCHAR(255) DEFAULT '&' NOT NULL,
    `precise_cue` TINYINT(1) DEFAULT 0 NOT NULL,
    `fade_type` TINYINT(1) DEFAULT 0 NOT NULL,
    `end_type` TINYINT(1) DEFAULT 0 NOT NULL,
    `overlay` TINYINT(1) DEFAULT 0 NOT NULL,
    `artist` VARCHAR(255) NOT NULL,
    `original_artist` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `album` VARCHAR(255) NOT NULL,
    `composer` VARCHAR(255) NOT NULL,
    `year` VARCHAR(4) DEFAULT '1900' NOT NULL,
    `track_no` SMALLINT DEFAULT 0 NOT NULL,
    `disc_no` SMALLINT DEFAULT 0 NOT NULL,
    `publisher` VARCHAR(255) NOT NULL,
    `copyright` VARCHAR(255) NOT NULL,
    `isrc` VARCHAR(255) NOT NULL,
    `bpm` DOUBLE(11,1) NOT NULL,
    `comments` TEXT,
    `sweepers` VARCHAR(250),
    `album_art` VARCHAR(255) DEFAULT 'no_image.jpg' NOT NULL,
    `buy_link` VARCHAR(255) DEFAULT 'http://' NOT NULL,
    `tdate_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `tartist_played` DATETIME DEFAULT '2002-01-01 00:00:01',
    `date_added` DATETIME,
    `updated_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    `version_created_at` DATETIME,
    `version_created_by` VARCHAR(100),
    `version_comment` VARCHAR(255),
    PRIMARY KEY (`ID`,`version`),
    CONSTRAINT `songs_version_fk_e666d8`
        FOREIGN KEY (`ID`)
        REFERENCES `songs` (`ID`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- songs_information_version
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `songs_information_version`;

CREATE TABLE `songs_information_version`
(
    `id` INTEGER NOT NULL,
    `song_id` int(10) unsigned NOT NULL,
    `artist` VARCHAR(255),
    `title` VARCHAR(255),
    `remix` VARCHAR(255),
    `label` VARCHAR(255),
    `genre` INTEGER,
    `url` VARCHAR(1000),
    `beatport` VARCHAR(1000),
    `soundcloud` VARCHAR(1000),
    `youtube` VARCHAR(1000),
    `itunes` VARCHAR(1000),
    `cover` TEXT,
    `added_by` INTEGER,
    `user_id` INTEGER,
    `status` TINYINT,
    `start` DATETIME,
    `end` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `deleted_at` DATETIME,
    `version` INTEGER DEFAULT 0 NOT NULL,
    `version_created_at` DATETIME,
    `version_created_by` VARCHAR(100),
    `version_comment` VARCHAR(255),
    PRIMARY KEY (`id`,`version`),
    CONSTRAINT `songs_information_version_fk_ada3a5`
        FOREIGN KEY (`id`)
        REFERENCES `songs_information` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
