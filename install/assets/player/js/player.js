	function togglePlayer(player){
		
		//console.debug(player);
		player = player || "toggle";
		var fplayer = jQuery(".full-player");
		var splayer = jQuery(".small-player");
		var saturs = jQuery("#saturs");
		var now = jQuery(".now-on-air");
		var nows = jQuery(".now-small");
		var fheight = fplayer.height();
		var sheight = splayer.height();
		var el = jQuery(this);
		
		/*
		if(player == "full"){
			//console.debug("pilnais");
			fplayer.attr("data-show", "false");
			splayer.attr("data-show", "true");
			//jQuery.cookie("player", "full", { expires : 10 });
		}else if(player == "small"){
			//console.debug("mazais");
			splayer.attr("data-show", "false");
			fplayer.attr("data-show", "true");
			//jQuery.cookie("player", "small", { expires : 10 });
		}
		*/
		//fplayer.attr("data-show", "false").slideUp();
		//splayer.attr("data-show", "true").slideDown();
		
		//var fplayer1 = jQuery(".full-player");
		//var splayer1 = jQuery(".small-player");
		
		
		if(player == "small" || (player == "toggle" && splayer.attr("data-show") == "false")){
			//console.debug("small player: "+player);
			//console.debug("fplayer: "+fplayer.attr("data-show"));
			//console.debug("splayer: "+splayer.attr("data-show"));
			//el.animate({height: sheight}, 500, function(){
				fplayer.attr("data-show", "false").slideUp(function(){
					splayer.attr("data-show", "true").slideDown();
					now.fadeOut(function(){
						nows.fadeIn();
					});
					saturs.animate({"margin-top": "35px"}, "fast");
					//sel.css({"float": "left"});
					jQuery.cookie("player", "small", { expires : 10 });
					jQuery(".player-container i.icon-arrow-up").fadeOut("fast", function(){
						jQuery(".player-container i.icon-arrow-down").fadeIn("fast");
					});
				});
				
				
			//});
			//console.debug("fplayer: "+fplayer.attr("data-show"));
			//console.debug("splayer: "+splayer.attr("data-show"));
		}else if(player == "full" || (player == "toggle" && fplayer.attr("data-show") == "false")){
			//console.debug("full player: "+player);
			//console.debug("fplayer: "+fplayer.attr("data-show"));
			//console.debug("splayer: "+splayer.attr("data-show"));
			
			//el.animate({height: fheight}, 500, function(){
			splayer.attr("data-show", "false").slideUp(function(){
				fplayer.attr("data-show", "true").slideDown();
					nows.fadeOut(function(){
						now.fadeIn();
					});
					saturs.animate({"margin-top": "100px"}, "fast");
					jQuery.cookie("player", "full", { expires : 10 });
					jQuery(".player-container i.icon-arrow-down").fadeOut("fast", function(){
						jQuery(".player-container i.icon-arrow-up").fadeIn("fast");
					});
				});
				
				
			//});
		}
	}


jQuery(document).ready(function(){

	/*
	 * jQuery UI ThemeRoller
	 *
	 * Includes code to hide GUI volume controls on mobile devices.
	 * ie., Where volume controls have no effect. See noVolume option for more info.
	 *
	 * Includes fix for Flash solution with MP4 files.
	 * ie., The timeupdates are ignored for 1000ms after changing the play-head.
	 * Alternative solution would be to use the slider option: {animate:false}
	 */
	 
	var player_status = jQuery.cookie("player");
	
	//console.debug(player_status);
	
	if(typeof player_status == 'undefined' || player_status == "" || player_status == "full"){
		togglePlayer("full");
	}else{
		togglePlayer("small");
	}
	
	var myPlayer = jQuery("#jquery_jplayer_1"),
		myPlayerData,
		fixFlash_mp4, // Flag: The m4a and m4v Flash player gives some old currentTime values when changed.
		fixFlash_mp4_id, // Timeout ID used with fixFlash_mp4
		ignore_timeupdate, // Flag used with fixFlash_mp4
		options = {
			ready: function (event) {
				// Hide the volume slider on mobile browsers. ie., They have no effect.
				if(event.jPlayer.status.noVolume) {
					// Add a class and then CSS rules deal with it.
					jQuery(".jp-gui").addClass("jp-no-volume");
				}
				// Determine if Flash is being used and the mp4 media type is supplied. BTW, Supplying both mp3 and mp4 is pointless.
				fixFlash_mp4 = event.jPlayer.flash.used && /m4a|m4v/.test(event.jPlayer.options.supplied);
				// Setup the player with media.
				jQuery(this).jPlayer("setMedia", {
					 mp3: "http://lvur.no-ip.biz:8005/;stream.nsv",
					 //mp3: "http://lvur.no-ip.biz:8000/demo-stream",
					//m4a: "http://www.jplayer.org/audio/m4a/Miaow-07-Bubble.m4a",
					//oga: "http://www.jplayer.org/audio/ogg/Miaow-07-Bubble.ogg"
				});
				//console.debug("player loaded!");
				jQuery(".player-container-inner").fadeIn();
			},
			timeupdate: function(event) {
				if(!ignore_timeupdate) {
					myControl.progress.slider("value", event.jPlayer.status.currentPercentAbsolute);
				}
			},
			volumechange: function(event) {
				if(event.jPlayer.options.muted) {
					myControl.volume.slider("value", 0);
				} else {
					myControl.volume.slider("value", event.jPlayer.options.volume);
				}
			},
			swfPath: "player/js",
			supplied: "mp3",
			//solution:"flash,html",
			cssSelectorAncestor: "#jp_container_1",
			wmode: "window",
			keyEnabled: true,
		},
		myControl = {
			progress: jQuery(options.cssSelectorAncestor + " .jp-progress-slider"),
			volume: jQuery(options.cssSelectorAncestor + " .jp-volume-slider")
		};

	// Instance jPlayer
	myPlayer.jPlayer(options);

	// A pointer to the jPlayer data object
	myPlayerData = myPlayer.data("jPlayer");

	// Define hover states of the buttons
	jQuery('.jp-gui ul li').hover(
		function() { jQuery(this).addClass('ui-state-hover'); },
		function() { jQuery(this).removeClass('ui-state-hover'); }
	);

	// Create the progress slider control
	myControl.progress.slider({
		animate: "fast",
		max: 100,
		range: "min",
		step: 0.1,
		value : 0,
		slide: function(event, ui) {
			var sp = myPlayerData.status.seekPercent;
			if(sp > 0) {
				// Apply a fix to mp4 formats when the Flash is used.
				if(fixFlash_mp4) {
					ignore_timeupdate = true;
					clearTimeout(fixFlash_mp4_id);
					fixFlash_mp4_id = setTimeout(function() {
						ignore_timeupdate = false;
					},1000);
				}
				// Move the play-head to the value and factor in the seek percent.
				myPlayer.jPlayer("playHead", ui.value * (100 / sp));
			} else {
				// Create a timeout to reset this slider to zero.
				setTimeout(function() {
					myControl.progress.slider("value", 0);
				}, 0);
			}
		}
	});

	// Create the volume slider control
	myControl.volume.slider({
		animate: "fast",
		max: 1,
		range: "min",
		step: 0.01,
		value : jQuery.jPlayer.prototype.options.volume,
		slide: function(event, ui) {
			myPlayer.jPlayer("option", "muted", false);
			myPlayer.jPlayer("option", "volume", ui.value);
		}
	});
	
	//var value = $( ".small-player.jp-volume-slider" ).slider( "option", "value" );
	
	// setter
	jQuery(".small-player div.jp-volume-slider-small").slider({
		animate: "fast",
		max: 1,
		range: "min",
		step: 0.01,
		value : jQuery.jPlayer.prototype.options.volume,
		slide: function( event, ui ){
			myPlayer.jPlayer("option", "muted", false);
			myPlayer.jPlayer("option", "volume", ui.value);
			//console.debug(event);
		}
	});
	
	jQuery(document).on("click", ".small-player li.jp-play", function(){
		//console.debug("play");
		myPlayer.jPlayer("play");
	});
	
	jQuery(document).on("click", ".small-player li.jp-pause", function(){
		//console.debug("play");
		myPlayer.jPlayer("pause");
	});
	
	jQuery(document).on("click", ".small-player li.jp-stop", function(){
		//console.debug("play");
		myPlayer.jPlayer("stop");
	});
	
	//jQuery("#jplayer_inspector").jPlayerInspector({jPlayer:jQuery("#jquery_jplayer_1")});
});

	jQuery(document).on("click", ".toggle-player", function(){
		//console.debug("toggle");
		togglePlayer();
	});
	
	jQuery(document).on("click", ".refresh-player", function(){
		update_song();
	});
	
//]]>
