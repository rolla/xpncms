	function togglePlayer(player){
		
		//console.log(player);
		player = player || "toggle";
		var fplayer = $(".full-player");
		var splayer = $(".small-player");
		var saturs = $("#saturs");
		var now = $(".now-on-air");
		var nows = $(".now-small");
		var fheight = fplayer.height();
		var sheight = splayer.height();
		var el = $(this);
		var art = $(".now-playing-art");

		var gradio_player = $('#gradio_player');
		
		/*
		if(player == "full"){
			//console.log("pilnais");
			fplayer.attr("data-show", "false");
			splayer.attr("data-show", "true");
			//$.cookie("player", "full", { expires : 10 });
		}else if(player == "small"){
			//console.log("mazais");
			splayer.attr("data-show", "false");
			fplayer.attr("data-show", "true");
			//$.cookie("player", "small", { expires : 10 });
		}
		*/
		//fplayer.attr("data-show", "false").slideUp();
		//splayer.attr("data-show", "true").slideDown();
		
		//var fplayer1 = $(".full-player");
		//var splayer1 = $(".small-player");
		
		
		if(player == "small" || (player == "toggle" && splayer.attr("data-show") == "false")){
			//console.log("small player: "+player);
			//console.log("fplayer: "+fplayer.attr("data-show"));
			//console.log("splayer: "+splayer.attr("data-show"));
			//el.animate({height: sheight}, 500, function(){
				$('.player-controls').fadeOut('fast');
				now.fadeOut();
				gradio_player.show();
				art.fadeOut();
				fplayer.attr("data-show", "false").delay(500).slideUp("fast", function(){
					splayer.attr("data-show", "true").slideDown("fast", function(){
						//now.fadeOut(function(){
							nows.fadeIn();
							$('.player-controls').fadeIn();
						//});
					});
					
					//saturs.animate({"margin-top": "35px"}, "fast");
					//sel.css({"float": "left"});
					//$.cookie("player", "small", { expires : 10 });
					/*
					var player_opts = {
						'status': 'small'
					}
					*/

					$.cookie("gplayer.status", 'small', { expires : 10 });

					$(".player-container i.icon-arrow-up").fadeOut("fast", function(){
						$(".player-container i.icon-arrow-down").fadeIn("fast");
					});
				});
				
				
			//});
			//console.log("fplayer: "+fplayer.attr("data-show"));
			//console.log("splayer: "+splayer.attr("data-show"));
		}else if(player == "full" || (player == "toggle" && fplayer.attr("data-show") == "false")){
			//console.log("full player: "+player);
			//console.log("fplayer: "+fplayer.attr("data-show"));
			//console.log("splayer: "+splayer.attr("data-show"));
			
			//el.animate({height: fheight}, 500, function(){
			$('.player-controls').fadeOut('fast');
			nows.fadeOut();
			gradio_player.show();
			art.delay(800).fadeIn();
			splayer.attr("data-show", "false").delay(500).slideUp("fast", function(){
				fplayer.attr("data-show", "true").slideDown("fast", function(){
					//nows.fadeOut(function(){
						now.fadeIn();
						$('.player-controls').fadeIn();
					//});
				});
					//saturs.animate({"margin-top": "100px"}, "fast");
					//$.cookie("player", "full", { expires : 10 });
					/*
					var player_opts = {
						'status': 'full'
					}
					*/

					$.cookie("gplayer.status", 'full', { expires : 10 });

					$(".player-container i.icon-arrow-down").fadeOut("fast", function(){
						$(".player-container i.icon-arrow-up").fadeIn("fast");
					});
				});
				
				
			//});
		}
	}


$(document).ready(function(){

	$(".player-container-inner").fadeIn();

	/*
	 * $ UI ThemeRoller
	 *
	 * Includes code to hide GUI volume controls on mobile devices.
	 * ie., Where volume controls have no effect. See noVolume option for more info.
	 *
	 * Includes fix for Flash solution with MP4 files.
	 * ie., The timeupdates are ignored for 1000ms after changing the play-head.
	 * Alternative solution would be to use the slider option: {animate:false}
	 */
	 
	/*
	var player_opts = {
		'status': 'full',
		'volume': '80'
	}
	*/

	if(!$.cookie("gplayer.volume")){
		$.cookie("gplayer.volume", 1, { expires : 10 });
	}

	//console.log( $.cookie() );
	//$.cookie("player");

	var player_status = $.cookie("gplayer.status");
	var player_volume = $.cookie("gplayer.volume");
	//var player_volume = $.cookie("player");
	
	//console.log(player_status);
	
	if(typeof player_status == 'undefined' || player_status == "" || player_status == "full"){
		togglePlayer("full");
	}else{
		togglePlayer("small");
	}
	
	var myPlayer = $("#gradio_player"),
		myPlayerData,
		fixFlash_mp4, // Flag: The m4a and m4v Flash player gives some old currentTime values when changed.
		fixFlash_mp4_id, // Timeout ID used with fixFlash_mp4
		ignore_timeupdate, // Flag used with fixFlash_mp4
		options = {
			ready: function (event) {
				// Hide the volume slider on mobile browsers. ie., They have no effect.
				if(event.jPlayer.status.noVolume) {
					// Add a class and then CSS rules deal with it.
					$(".jp-gui").addClass("jp-no-volume");
				}
				// Determine if Flash is being used and the mp4 media type is supplied. BTW, Supplying both mp3 and mp4 is pointless.
				fixFlash_mp4 = event.jPlayer.flash.used && /m4a|m4v/.test(event.jPlayer.options.supplied);
				// Setup the player with media.
				$(this).jPlayer("setMedia", {
					 mp3: "http://server.gradio.lv:8000/gradio",
					 //mp3: "http://lvur.no-ip.biz:8000/demo-stream",
					//m4a: "http://www.jplayer.org/audio/m4a/Miaow-07-Bubble.m4a",
					//oga: "http://www.jplayer.org/audio/ogg/Miaow-07-Bubble.ogg"
				});

				$(this).jPlayer("volume", player_volume);
				//console.log("player loaded!");
				$(".player-container-inner").fadeIn();
			},
			timeupdate: function(event) {
				if(!ignore_timeupdate) {
					myControl.progress.slider("value", event.jPlayer.status.currentPercentAbsolute);
				}
			},
			volumechange: function(event) {
				if(event.jPlayer.options.muted) {
					myControl.volume.slider("value", 0);
				} else {
					myControl.volume.slider("value", event.jPlayer.options.volume);
				}
			},
			swfPath: baseURL+"assets/player/js",
			supplied: "mp3",
			solution:"html, flash",
			//solution:"html",
			cssSelectorAncestor: "#gradio_player_container",
			wmode: "window",
			keyEnabled: false,
			preload: "auto",
			//preload: "none",
		},
		myControl = {
			progress: $(options.cssSelectorAncestor + " .jp-progress-slider"),
			volume: $(options.cssSelectorAncestor + " .jp-volume-slider")
		};

	// Instance jPlayer
	myPlayer.jPlayer(options);

	// A pointer to the jPlayer data object
	myPlayerData = myPlayer.data("jPlayer");

	// Define hover states of the buttons
	$('.jp-gui ul li').hover(
		function() { $(this).addClass('ui-state-hover'); },
		function() { $(this).removeClass('ui-state-hover'); }
	);

	// Create the progress slider control
	myControl.progress.slider({
		animate: "fast",
		max: 100,
		range: "min",
		step: 0.1,
		value : 0,
		slide: function(event, ui) {
			var sp = myPlayerData.status.seekPercent;
			if(sp > 0) {
				// Apply a fix to mp4 formats when the Flash is used.
				if(fixFlash_mp4) {
					ignore_timeupdate = true;
					clearTimeout(fixFlash_mp4_id);
					fixFlash_mp4_id = setTimeout(function() {
						ignore_timeupdate = false;
					},1000);
				}
				// Move the play-head to the value and factor in the seek percent.
				myPlayer.jPlayer("playHead", ui.value * (100 / sp));
			} else {
				// Create a timeout to reset this slider to zero.
				setTimeout(function() {
					myControl.progress.slider("value", 0);
				}, 0);
			}
		}
	});

	// Create the volume slider control
	myControl.volume.slider({
		animate: "fast",
		max: 1,
		range: "min",
		step: 0.01,
		value : $.jPlayer.prototype.options.volume,
		slide: function(event, ui) {
			myPlayer.jPlayer("option", "muted", false);
			myPlayer.jPlayer("option", "volume", ui.value);
			$.cookie("gplayer.volume", ui.value, { expires : 10 });
			$('.small-player div.jp-volume-slider-small').slider('value', ui.value);
		}
	});
	
	//var value = $( ".small-player.jp-volume-slider" ).slider( "option", "value" );
	
	// setter
	$(".small-player div.jp-volume-slider-small").slider({
		animate: "fast",
		max: 1,
		range: "min",
		step: 0.01,
		value : $.jPlayer.prototype.options.volume,
		slide: function( event, ui ){
			myPlayer.jPlayer("option", "muted", false);
			myPlayer.jPlayer("option", "volume", ui.value);
			$.cookie("gplayer.volume", ui.value, { expires : 10 });
			//console.log(event);
		}
	});
	
	$(document).on("click", ".small-player li.jp-play", function(event){
		//console.log("play");
		event.stopImmediatePropagation();
		myPlayer.jPlayer("play");

		setInterval(function(){
    		var isPaused = myPlayer.data().jPlayer.status.paused;

    		if(!isPaused){
    			$('.small-player .ui-icon-play').css('background-position', '-16px -160px');
    			//.ui-icon-play {background-position: -16px -160px;}
    		}
    		//$("#message").text("Is Paused: " + isPaused);
		}, 500);

		console.log( myPlayer.data() );

	});
	
	$(document).on("click", ".small-player li.jp-pause", function(event){
		//console.log("pause");
		event.stopImmediatePropagation();
		myPlayer.jPlayer("pause");
	});
	
	$(document).on("click", ".small-player li.jp-stop", function(event){
		//console.log("stop");
		event.stopImmediatePropagation();
		myPlayer.jPlayer("stop");
		setInterval(function(){
    		var isPaused = myPlayer.data().jPlayer.status.paused;

    		if(isPaused){
    			//.ui-icon-play {background-position: -16px -160px;}
    			$('.small-player .ui-icon-play').css('background-position', '0 -160px');
    		}
    		//$("#message").text("Is Paused: " + isPaused);
		}, 500);
	});
	
	//$("#jplayer_inspector").jPlayerInspector({jPlayer:$("#jquery_jplayer_1")});
});

	$(document).on("click", ".toggle-player", function(event){
		//console.log("toggle");
		event.stopImmediatePropagation();
		togglePlayer();
		setTimeout(function(){
			//nowPlaying(true);
		}, 2000);
	});
	
	$(document).on("click", ".refresh-player", function(event){
		$(this).attr("disabled", "disabled");
		event.stopImmediatePropagation();
		gradioPlayer.nowPlaying(true);
	});
	
//]]>
