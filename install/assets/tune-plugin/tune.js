var xpnCMSPlugTune = {};
var tuneLogger = Logger.get('ModuleTune');
tuneLogger.setLevel(Logger.DEBUG);

var xpnCMSPlugTuneClass = xpnCMSClass.extend(function () {

  this.constructor = function () {
    $(document).on('submit', '#iTunesSearch', function(){
      performSearch();

      return false;
    });

    $(document).on('click', '.itunes-search', function(){
      $('#query').val($(this).data('query'))
      performSearch();

      return false;
    });

    $(document).on('click', '.standart', function(){
      var imageSrc = $(this).data('href');
      $('[name="remoteFileUpload"]').val(imageSrc)

      $('.uploaded-image').fadeOut('slow', function() {
        $('.uploaded-image').attr('src', imageSrc);
        $('.uploaded-image').fadeIn('slow');
      });

      return false;
    });

    $(document).on('click', '.hires', function(){
      var imageSrc = $(this).data('href');
      $('[name="remoteFileUpload"]').val(imageSrc)

      $('.uploaded-image').fadeOut('slow', function() {
        $('.uploaded-image').attr('src', imageSrc);
        $('.uploaded-image').fadeIn('slow');
      });

      return false;
    });

    $('#edit-tune').on('ajax:complete', function(event, request, settings) {
      var responseJSON = request.responseJSON;

      if (!_.isEmpty(responseJSON.result.saveSong)) {
        $.address.update($.address.path()); // ajax reload page
      }

    });
  };

  this.initTuneEdit = function () {
    tuneLogger.debug('initTuneEdit');

    var tuneCoversURL = $('.tuneCoversURL').val();
    var relativeUploaded = $('.relativeUploaded').val();
    //var album_art = $('#edit-tune .tunePicture').val();
    //$('.uploaded-image').attr('src', album_art);
    var btn = document.getElementById('uploadBtn'),
    progressBar = document.getElementById('progressBar'),
    progressOuter = document.getElementById('progressOuter');
    var uploader = new ss.SimpleUpload({
      dropzone: 'dragbox', // ID of element to be the drop zone
      dragClass: 'dragged-over',
      button: btn,
      url: baseURL + 'tune/coverupload/',
      name: 'uploadfile',
      multipart: true,
      hoverClass: 'hover',
      focusClass: 'focus',
      responseType: 'json',
      startXHR: function() {
        progressOuter.style.display = 'block'; // make progress bar visible
        this.setProgressBar( progressBar );
      },
      onSubmit: function() {
        btn.innerHTML = t('Uploading...'); // change button text to "Uploading..."
      },
      onComplete: function( filename, response ) {
        //console.log(filename);
        //console.log(response);
        btn.innerHTML = t('Choose Another File');
        progressOuter.style.display = 'none'; // hide progress bar when upload is completed
        if ( !response ) {
          xpnCMSNotify.simpleNotify({title: t('Ouch'), 'type': 'error', alert: t('Unable to upload file')});

          return;
        }
        if ( response.success === true ) {
          var encodedPictureUrl = tuneCoversURL + relativeUploaded + response.filename;
          $('.uploaded-image').fadeOut('slow', function() {
            $('.uploaded-image').attr('src', phpThumbUrl + '?q=100&w=400&src=' + encodeURIComponent(encodedPictureUrl) + '&hash=' + phpThumbCacheHash);
            $('.uploaded-image').fadeIn('slow');
          });
          $('#edit-tune .album_art').val(relativeUploaded + response.filename);
          xpnCMSNotify.simpleNotify({title: t('Yayy'), 'type': 'success', alert: response.filename + ' ' + t('successfully uploaded.')});
        } else {
          if (response.msg)  {
            xpnCMSNotify.simpleNotify({title: t('Something happen'), 'type': 'warning', alert: response.msg});
          } else {
            xpnCMSNotify.simpleNotify({title: t('Ouchh'), 'type': 'error', alert: t('An error occurred and the upload failed.')});
          }
        }
      },
      onError: function() {
        progressOuter.style.display = 'none';
        xpnCMSNotify.simpleNotify({title: t('Ouch'), 'type': 'error', alert: t('Unable to upload file')});
      }
    });

    $('#dragbox').css('z-index', '1');
  }

  function performSearch() {
    $('#iTunes-results').html('');
    $('#iTunes-results').append('<h3>' + t('Searching...') + '</h3>');

    var query = $('#query').val();
    if (!query.length) {
        xpnCMSNotify.simpleNotify({title: t('Ooopss'), 'type': 'warning', alert: t('Please fill something')});

        return false;
    };

    var entity = ($('#entity').val()) ? $('#entity').val() : 'album';
    var country = ($('#country').val()) ? $('#country').val() : 'us';

    $.ajax({
        type: "GET",
        crossDomain: true,
        url: baseURL + 'assets/itunes-artwork-finder/api.php',
        data: {query: query, entity: entity, country: country},
        dataType: 'json'
    }).done(function(data) {
        $('#iTunes-results').html('');
        if (data.error) {
                $('#iTunes-results').append('<h3>'+data.error+'</h3>');
        } else {
            if (!data.length) {
                $('#iTunes-results').append('<h3>' + t('No results found!') + '</h3>');
            } else {
                for (var i = 0; i < data.length; i++) {
                    var result = data[i];
                    //console.log(result.title);

                    var html = '<div><h3>'+result.title+'</h3>';
                    if (entity != 'software') {
                        html += '<p><a href="#" class="standart" data-href="'+result.url+'">Standard Resolution</a> ';
                        html += '<a href="'+result.url+'" target="_blank"><i class="fa fa-external-link"></i></a> | ';
                        html += '<a href="#" class="hires" data-href="'+result.hires+'" target="_blank">High Resolution</a> ';
                        html += '<a href="'+result.hires+'" target="_blank"><i class="fa fa-external-link"></i></a> ';
                        html += '<em><small>'+result.warning+'</small></em></p>';
                    } else {
                        html += '<p><a href="./app/?url='+encodeURIComponent(result.appstore)+'" target="_blank">View screenshots / videos</a></p>';
                    }
                    html += '<a href="'+result.hires+'" class="hires" data-href="'+result.hires+'"><img src="'+result.url+'" alt="iTunes Artwork for \''+result.title+'\'" width="'+result.width+'" height="'+result.height+'"></a>';
                    html += '<br>';
                    html += '</div>';

                    $('#iTunes-results').append(html);
                };
            }
        }
        //$('#iTunes-results').append('<p>If the item you are searching for is not available on iTunes, this tool will not find it. Please do not email me asking for specific items if they are not available on iTunes! I recommend both <a href="https://code.google.com/p/subler/">Subler</a> and <a href="https://www.google.co.uk/imghp?gws_rd=ssl">Google Image Search</a> as good alternative places to find artwork.</p>');

    });
  }

});

xpnCMSPlugTune = new xpnCMSPlugTuneClass();
