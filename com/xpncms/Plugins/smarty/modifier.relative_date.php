<?php

use Carbon\Carbon;

/*
* Smarty plugin
* -------------------------------------------------------------
* Type:     modifier
* Name:     relative_date
* Version:  1.1
* Date:     November 28, 2008
* Author:   Chris Wheeler <chris@haydendigital.com>
* Purpose:  Output dates relative to the current time
* Input:    timestamp = UNIX timestamp or a date which can be converted by strtotime()
*           days = use date only and ignore the time
*           format = (optional) a php date format (for dates over 1 year)
* -------------------------------------------------------------
*/

function smarty_modifier_relative_date($timestamp, $remove_ago = false)
{
    return Carbon::parse($timestamp)->diffForHumans(null, $remove_ago);
}
