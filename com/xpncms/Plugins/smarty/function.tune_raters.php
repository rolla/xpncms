<?php

use Carbon\Carbon;

    function smarty_function_tune_raters(array $params, Smarty_Internal_Template $template)
    {
        //print_r($params);
        //exit;

        $html = '';
        foreach ($params['ratings'] as $rating) {
            if ($rating['category'] == 'tune') {
                $ratingText = _('liked tune');
                if ($rating['action'] == 'disliked') {
                    $ratingText = _('disliked tune');
                }
            }

            $html .= Carbon::parse($rating['created_at'])->diffForHumans(null, false) . ' ';
            $html .= $rating['display_name'] . ' ';
            /*
            $ratingText = '<i class="fa fa-thumbs-up"></i> ';
            if ($rating['action'] == 'disliked') {
                $ratingText = '<i class="fa fa-thumbs-down"></i> ';
            }

            $html .= $ratingText;
             *
             */
            $html .= $ratingText . ' ';
            //$html .= $ratingCategoryText . ' ';
            $html .= '<br>';
        }

        return $html;
    }
