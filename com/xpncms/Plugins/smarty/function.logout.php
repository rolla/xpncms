<?php
    function smarty_function_logout(array $params, Smarty_Internal_Template $template)
    {
        if (isset($_SESSION['user'])) {
            $res = '
		<a href="'.BASE_URL.'user/logout" class="btn btn-block btn-social btn-google-plus">
			<i class="fa fa-power-off "></i> '._('Logout').'
		</a>
		';
        }

        return $res;
    }
