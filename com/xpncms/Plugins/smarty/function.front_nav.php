<?php
    function smarty_function_front_nav(array $params, Smarty_Internal_Template $template)
    {
        $frontend = new \XpnCMS\Controller\Frontend();

        return $frontend->getNav();
    }
