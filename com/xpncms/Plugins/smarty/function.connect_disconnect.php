<?php
    function smarty_function_connect_disconnect(array $params, Smarty_Internal_Template $template)
    {
        global $conf;

        //echo '<pre>';
        //print_r($conf['settings']['base']['url']);
        //echo '</pre>';
        //exit;


        if ($template->tpl_vars['user_authentications']) {
            function search($array, $key, $value)
            {
                $results = array();
                if (is_array($array)) {
                    if (isset($array[$key]) && $array[$key] == $value) {
                        $results[] = $array;
                    }
                    foreach ($array as $subarray) {
                        $results = array_merge($results, search($subarray, $key, $value));
                    }
                }

                return $results;
            }

            $enabled_providers = array('facebook', 'twitter', 'draugiem', 'google');
            $logins = '';

            foreach ($enabled_providers as $provider) {
                //echo 'mekleju ieks: '.$provider.' <br />';
                /*
                echo '<pre>';
                print_r( search($user_authentication, 'provider', $provider) );
                echo '</pre>';
                */

                if ($provider == 'google') {
                    $icon = 'google';
                    $butt = 'google';
                } elseif ($provider == 'draugiem') {
                    $icon = 'user';
                    $butt = 'draugiem';
                } else {
                    $icon = $provider;
                    $butt = $icon;
                }

                if (search($template->tpl_vars['user_authentications']->value, 'provider', $provider)) {
                    $logins .= '<a href="'.$conf['settings']['base']['url'].'auth/disconnect/'.$provider.'" class="btn btn-social-icon btn-'.$butt.' connected-login" title="'._('Disconnect from').' '.$provider.'"><i class="fa fa-'.$icon.'"></i></a>
					';
                } else {
                    $logins .= '<a href="'.$conf['settings']['base']['url'].'auth/connect/'.$provider.'" class="btn btn-social-icon btn-'.$butt.' disconnected-login" title="'._('Connect with').' '.$provider.'"><i class="fa fa-'.$icon.'"></i></a>
					';
                }
            }

            /*
            echo '<pre>';
            print_r($connected);
            echo '</pre>';
            */

            //echo $provider;
            $html = '
			<div class="connections-container">
				<div class="connections-inner">
					<h1>'._('Your connections').'</h1>
					<fieldset>
						<legend>'._('Connect').' / '._('Disconnect').'</legend>
						'.$logins.'
					</fieldset>
				</div>
			</div>
			';

            return $html;
        }
    }
