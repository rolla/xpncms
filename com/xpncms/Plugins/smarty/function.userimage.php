<?php

use Core\Includes\Config;

function smarty_function_userimage(array $params, Smarty_Internal_Template $template)
{
    //$params = ['user_id' => null, 'photo_url' => null, 'phpthumb' => 'w=80&q=50'];
    $usr_img_path = Config::get('base.url').Config::get('paths.files').DS.'user_'.$params['user_id'].DS.'images'.DS;

    if (!preg_match('~^(?:f|ht)tps?://~i', $params['photo_url'])) {
        //$photo_url = $usr_img_path.$user['photo_url'];
        //$users[$key]['photo_url'] = $photo_url;
        $photo_url = $usr_img_path.$params['photo_url'];
    } else {
        $photo_url = $params['photo_url'];
    }

    if ($params['phpthumb']) {
        $photo_url = Config::get('base.url').'libs/phpThumb/?'.$params['phpthumb'].'&src=../..'.$photo_url;
    }

    return $photo_url;
}
