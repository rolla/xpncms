<?php
    function smarty_function_ifadmin(array $params, Smarty_Internal_Template $template)
    {
        $frontend = new \XpnCMS\Controller\Frontend();

        return $frontend->getIfAdminNav();
    }
