<?php

function smarty_modifier_emoji($inputText)
{
    //namespace Emojione;
    $client = new Emojione\Client(new Emojione\Ruleset());
    $client->imageType = 'svg'; // or png (default)
    return $client->toImage($inputText);
}
