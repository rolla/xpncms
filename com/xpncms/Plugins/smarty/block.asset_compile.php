<?php Phar::interceptFileFuncs();
    define("____SACY_BUNDLED", 1);
    Phar::mapPhar("sacy.phar");
    include("phar://sacy.phar/sacy/ext-translators.php");
    include("phar://sacy.phar/sacy/sacy.php");
    

if (!defined("____SACY_BUNDLED"))
    include_once(implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'sacy', 'sacy.php')));

if (!(defined("ASSET_COMPILE_OUTPUT_DIR") && defined("ASSET_COMPILE_URL_ROOT"))){
    throw new sacy_Exception("Failed to initialize because path configuration is not set (ASSET_COMPILE_OUTPUT_DIR and ASSET_COMPILE_URL_ROOT)");
}

function smarty_block_asset_compile($params, $content, &$smarty, &$repeat){
    if (!$repeat){
        // don't shoot me, but all tried using the dom-parser and removing elements
        // ended up with problems due to the annoying DOM API and braindead stuff
        // like no getDocumentElement() or getElementByID() not working even though
        // loadHTML clearly knows that the content is in fact HTML.
        //
        // So, let's go back to good old regexps :-)

        $cfg = new sacy_Config($params);
        if ($cfg->getDebugMode() == 1 ){
            return $content;
        }

        $tags = array('link', 'script');
        $tag_pattern = '#\s*<\s*T\s+(.*)\s*(?:/>|>(.*)</T>)\s*#Uim';
        $work = array();
        $aindex = 0;

        // first assembling all work. The idea is that, if sorted by descending
        // location offset, we can selectively remove tags.
        //
        // We'll need that to conditionally handle tags (like jTemplate's
        // <script type="text/html" that should remain)
        foreach($tags as $tag){
            $p = str_replace('T', preg_quote($tag), $tag_pattern);
            if(preg_match_all($p, $content, $ms, PREG_OFFSET_CAPTURE)){
                foreach($ms[1] as $i => $m)
                    $work[] = array($tag, $m[0], $ms[0][$i][1], $ms[0][$i][0], $ms[2][$i][0], $aindex++);
                                  // tag, attrdata, index in doc, whole tag, content, order of appearance
            }
        }

        // now sort task list by descending location offset
        // by the way: I want widespread 5.3 adoption for anonymous functions
        usort($work, create_function('$a,$b', 'if ($a[2] == $b[2]) return 0; return ($a[2] < $b[2]) ? 1 : -1;'));
        $ex = new sacy_FileExtractor($cfg);
        $files = array();

        foreach($work as $unit){
            $r = $ex->extractFile($unit[0], $unit[1], $unit[4]);
            if ($r === false) continue; // handler has declined
            $r = array_merge($r, array(
                'page_order' => $unit[5],
                'position' => $unit[2],
                'length' => strlen($unit[3]),
                'tag' => $unit[0]
            ));
            $r[] = $unit[0];
            $files[] = $r;
        }

        $renderer = new sacy_CacheRenderer($cfg, $smarty);
        $patched_content = $content;

        $render = array();
        $curr_cat = $files[0]['group'].$files[0]['tag'];

        $entry = null;
        foreach($files as $i => $entry){
            $cg = $entry['group'].$entry['tag'];

            // the moment the category changes, render all we have so far
            // this makes it IMPERATIVE to keep links of the same category
            // together.
            if ($curr_cat != $cg || ($cfg->getDebugMode() == 3 && count($render))){
                $render_order = array_reverse($render);
                $res = $renderer->renderFiles($files[$i-1]['tag'], $files[$i-1]['group'], $render_order);
                if ($res === false){
                    // rendering failed.
                    // because we don't know which one, we just enter emergency mode
                    // and return the initial content unharmed:
                    return $content;
                }
                // add rendered stuff to patched content
                $m = null;
                foreach($render as $r){
                    if ($m == null) $m = $r['position'];
                    if ($r['position'] < $m) $m = $r['position'];
                    // remove tag
                    $patched_content = substr_replace($patched_content, '', $r['position'], $r['length']);
                }
                // splice in replacement
                $patched_content = substr_replace($patched_content, $res, $m, 0);
                $curr_cat = $cg;
                $render = array($entry);
            }else{
                $render[] = $entry;
            }
        }
        $render_order = array_reverse($render);
        if ($files){
            $res = $renderer->renderFiles($entry['tag'], $entry['group'], $render_order);
            if ($res === false){
                // see last comment
                return $content;
            }
        }
        $m = null;
        foreach($render as $r){
            if ($m == null) $m = $r['position'];
            if ($r['position'] < $m) $m = $r['position'];
            // remove tag
            $patched_content = substr_replace($patched_content, '', $r['position'], $r['length']);
        }
        $patched_content = substr_replace($patched_content, $res, $m, 0);
        return $patched_content;
    }
}

__HALT_COMPILER(); ?>
�          	   sacy.phar       sacy/coffeescript.phpw   ���Sw   ����         sacy/cssmin.php�S  ���S�S  /?��         sacy/ext-translators.php`  ���S`   Q���         sacy/jsmin.php�  ���S�  ���         sacy/sacy.php�B  ���S�B  �]�1�      <?php
class CoffeeScript{

    public static function build($file){
        return CoffeeScript\compile($file);
    }
}<?php
/* Taken from minify by Ryan Grove and Steve Clay and distributed
   under the following license:
   
   Copyright (c) 2008 Ryan Grove <ryan@wonko.com>
   Copyright (c) 2008 Steve Clay <steve@mrclay.org>
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
     * Neither the name of this project nor the names of its contributors may be
       used to endorse or promote products derived from this software without
       specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*/

class Minify_CSS {
    
    public static function minify($css, $options = array()) 
    {
        if (isset($options['preserveComments']) 
            && !$options['preserveComments']) {
            $css = Minify_CSS_Compressor::process($css, $options);
        } else {
            $css = Minify_CommentPreserver::process(
                $css
                ,array('Minify_CSS_Compressor', 'process')
                ,array($options)
            );
        }
        if (! isset($options['currentDir']) && ! isset($options['prependRelativePath'])) {
            return $css;
        }
        if (isset($options['currentDir'])) {
            return Minify_CSS_UriRewriter::rewrite(
                $css
                ,$options['currentDir']
                ,isset($options['docRoot']) ? $options['docRoot'] : $_SERVER['DOCUMENT_ROOT']
                ,isset($options['symlinks']) ? $options['symlinks'] : array()
            );  
        } else {
            return Minify_CSS_UriRewriter::prepend(
                $css
                ,$options['prependRelativePath']
            );
        }
    }
}

class Minify_CSS_UriRewriter {
    
    /**
     * Defines which class to call as part of callbacks, change this
     * if you extend Minify_CSS_UriRewriter
     * @var string
     */
    protected static $className = 'Minify_CSS_UriRewriter';
    
    /**
     * rewrite() and rewriteRelative() append debugging information here
     * @var string
     */
    public static $debugText = '';
    
    /**
     * Rewrite file relative URIs as root relative in CSS files
     * 
     * @param string $css
     * 
     * @param string $currentDir The directory of the current CSS file.
     * 
     * @param string $docRoot The document root of the web site in which 
     * the CSS file resides (default = $_SERVER['DOCUMENT_ROOT']).
     * 
     * @param array $symlinks (default = array()) If the CSS file is stored in 
     * a symlink-ed directory, provide an array of link paths to
     * target paths, where the link paths are within the document root. Because 
     * paths need to be normalized for this to work, use "//" to substitute 
     * the doc root in the link paths (the array keys). E.g.:
     * <code>
     * array('//symlink' => '/real/target/path') // unix
     * array('//static' => 'D:\\staticStorage')  // Windows
     * </code>
     * 
     * @return string
     */
    public static function rewrite($css, $currentDir, $docRoot = null, $symlinks = array()) 
    {
        self::$_docRoot = self::_realpath(
            $docRoot ? $docRoot : $_SERVER['DOCUMENT_ROOT']
        );
        self::$_currentDir = self::_realpath($currentDir);
        self::$_symlinks = array();
        
        // normalize symlinks
        foreach ($symlinks as $link => $target) {
            $link = ($link === '//')
                ? self::$_docRoot
                : str_replace('//', self::$_docRoot . '/', $link);
            $link = strtr($link, '/', DIRECTORY_SEPARATOR);
            self::$_symlinks[$link] = self::_realpath($target);
        }
        
        self::$debugText .= "docRoot    : " . self::$_docRoot . "\n"
                          . "currentDir : " . self::$_currentDir . "\n";
        if (self::$_symlinks) {
            self::$debugText .= "symlinks : " . var_export(self::$_symlinks, 1) . "\n";
        }
        self::$debugText .= "\n";
        
        $css = self::_trimUrls($css);
        
        // rewrite
        $css = preg_replace_callback('/@import\\s+([\'"])(.*?)[\'"]/'
            ,array(self::$className, '_processUriCB'), $css);
        $css = preg_replace_callback('/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
            ,array(self::$className, '_processUriCB'), $css);

        return $css;
    }
    
    /**
     * Prepend a path to relative URIs in CSS files
     * 
     * @param string $css
     * 
     * @param string $path The path to prepend.
     * 
     * @return string
     */
    public static function prepend($css, $path)
    {
        self::$_prependPath = $path;
        
        $css = self::_trimUrls($css);
        
        // append
        $css = preg_replace_callback('/@import\\s+([\'"])(.*?)[\'"]/'
            ,array(self::$className, '_processUriCB'), $css);
        $css = preg_replace_callback('/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
            ,array(self::$className, '_processUriCB'), $css);

        self::$_prependPath = null;
        return $css;
    }
    
    
    /**
     * @var string directory of this stylesheet
     */
    private static $_currentDir = '';
    
    /**
     * @var string DOC_ROOT
     */
    private static $_docRoot = '';
    
    /**
     * @var array directory replacements to map symlink targets back to their
     * source (within the document root) E.g. '/var/www/symlink' => '/var/realpath'
     */
    private static $_symlinks = array();
    
    /**
     * @var string path to prepend
     */
    private static $_prependPath = null;
    
    private static function _trimUrls($css)
    {
        return preg_replace('/
            url\\(      # url(
            \\s*
            ([^\\)]+?)  # 1 = URI (assuming does not contain ")")
            \\s*
            \\)         # )
        /x', 'url($1)', $css);
    }
    
    private static function _processUriCB($m)
    {
        // $m matched either '/@import\\s+([\'"])(.*?)[\'"]/' or '/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
        $isImport = ($m[0][0] === '@');
        // determine URI and the quote character (if any)
        if ($isImport) {
            $quoteChar = $m[1];
            $uri = $m[2];
        } else {
            // $m[1] is either quoted or not
            $quoteChar = ($m[1][0] === "'" || $m[1][0] === '"')
                ? $m[1][0]
                : '';
            $uri = ($quoteChar === '')
                ? $m[1]
                : substr($m[1], 1, strlen($m[1]) - 2);
        }
        // analyze URI
        if ('/' !== $uri[0]                  // root-relative
            && false === strpos($uri, '//')  // protocol (non-data)
            && 0 !== strpos($uri, 'data:')   // data protocol
        ) {
            // URI is file-relative: rewrite depending on options
            $uri = (self::$_prependPath !== null)
                ? (self::$_prependPath . $uri)
                : self::rewriteRelative($uri, self::$_currentDir, self::$_docRoot, self::$_symlinks);
        }
        return $isImport
            ? "@import {$quoteChar}{$uri}{$quoteChar}"
            : "url({$quoteChar}{$uri}{$quoteChar})";
    }
    
    /**
     * Rewrite a file relative URI as root relative
     *
     * <code>
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       '../img/hello.gif'
     *     , '/home/user/www/css'  // path of CSS file
     *     , '/home/user/www'      // doc root
     * );
     * // returns '/img/hello.gif'
     * 
     * // example where static files are stored in a symlinked directory
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       'hello.gif'
     *     , '/var/staticFiles/theme'
     *     , '/home/user/www'
     *     , array('/home/user/www/static' => '/var/staticFiles')
     * );
     * // returns '/static/theme/hello.gif'
     * </code>
     * 
     * @param string $uri file relative URI
     * 
     * @param string $realCurrentDir realpath of the current file's directory.
     * 
     * @param string $realDocRoot realpath of the site document root.
     * 
     * @param array $symlinks (default = array()) If the file is stored in 
     * a symlink-ed directory, provide an array of link paths to
     * real target paths, where the link paths "appear" to be within the document 
     * root. E.g.:
     * <code>
     * array('/home/foo/www/not/real/path' => '/real/target/path') // unix
     * array('C:\\htdocs\\not\\real' => 'D:\\real\\target\\path')  // Windows
     * </code>
     * 
     * @return string
     */
    public static function rewriteRelative($uri, $realCurrentDir, $realDocRoot, $symlinks = array())
    {
        // prepend path with current dir separator (OS-independent)
        $path = strtr($realCurrentDir, '/', DIRECTORY_SEPARATOR)  
            . DIRECTORY_SEPARATOR . strtr($uri, '/', DIRECTORY_SEPARATOR);
        
        self::$debugText .= "file-relative URI  : {$uri}\n"
                          . "path prepended     : {$path}\n";
        
        // "unresolve" a symlink back to doc root
        foreach ($symlinks as $link => $target) {
            if (0 === strpos($path, $target)) {
                // replace $target with $link
                $path = $link . substr($path, strlen($target));
                
                self::$debugText .= "symlink unresolved : {$path}\n";
                
                break;
            }
        }
        // strip doc root
        $path = substr($path, strlen($realDocRoot));
        
        self::$debugText .= "docroot stripped   : {$path}\n";
        
        // fix to root-relative URI

        $uri = strtr($path, '/\\', '//');

        // remove /./ and /../ where possible
        $uri = str_replace('/./', '/', $uri);
        // inspired by patch from Oleg Cherniy
        do {
            $uri = preg_replace('@/[^/]+/\\.\\./@', '/', $uri, 1, $changed);
        } while ($changed);
      
        self::$debugText .= "traversals removed : {$uri}\n\n";
        
        return $uri;
    }
    
    /**
     * Get realpath with any trailing slash removed. If realpath() fails,
     * just remove the trailing slash.
     * 
     * @param string $path
     * 
     * @return mixed path with no trailing slash
     */
    protected static function _realpath($path)
    {
        $realPath = realpath($path);
        if ($realPath !== false) {
            $path = $realPath;
        }
        return rtrim($path, '/\\');
    }
}


class Minify_CommentPreserver {
    
    /**
     * String to be prepended to each preserved comment
     *
     * @var string
     */
    public static $prepend = "\n";
    
    /**
     * String to be appended to each preserved comment
     *
     * @var string
     */
    public static $append = "\n";
    
    /**
     * Process a string outside of C-style comments that begin with "/*!"
     *
     * On each non-empty string outside these comments, the given processor 
     * function will be called. The first "!" will be removed from the 
     * preserved comments, and the comments will be surrounded by 
     * Minify_CommentPreserver::$preprend and Minify_CommentPreserver::$append.
     * 
     * @param string $content
     * @param callback $processor function
     * @param array $args array of extra arguments to pass to the processor 
     * function (default = array())
     * @return string
     */
    public static function process($content, $processor, $args = array())
    {
        $ret = '';
        while (true) {
            list($beforeComment, $comment, $afterComment) = self::_nextComment($content);
            if ('' !== $beforeComment) {
                $callArgs = $args;
                array_unshift($callArgs, $beforeComment);
                $ret .= call_user_func_array($processor, $callArgs);    
            }
            if (false === $comment) {
                break;
            }
            $ret .= $comment;
            $content = $afterComment;
        }
        return $ret;
    }
    
    /**
     * Extract comments that YUI Compressor preserves.
     * 
     * @param string $in input
     * 
     * @return array 3 elements are returned. If a YUI comment is found, the
     * 2nd element is the comment and the 1st and 2nd are the surrounding
     * strings. If no comment is found, the entire string is returned as the 
     * 1st element and the other two are false.
     */
    private static function _nextComment($in)
    {
        if (
            false === ($start = strpos($in, '/*!'))
            || false === ($end = strpos($in, '*/', $start + 3))
        ) {
            return array($in, false, false);
        }
        $ret = array(
            substr($in, 0, $start)
            ,self::$prepend . '/*' . substr($in, $start + 3, $end - $start - 1) . self::$append
        );
        $endChars = (strlen($in) - $end - 2);
        $ret[] = (0 === $endChars)
            ? ''
            : substr($in, -$endChars);
        return $ret;
    }
}

class Minify_CSS_Compressor {

    /**
     * Minify a CSS string
     * 
     * @param string $css
     * 
     * @param array $options (currently ignored)
     * 
     * @return string
     */
    public static function process($css, $options = array())
    {
        $obj = new Minify_CSS_Compressor($options);
        return $obj->_process($css);
    }
    
    /**
     * @var array options
     */
    protected $_options = null;
    
    /**
     * @var bool Are we "in" a hack?
     * 
     * I.e. are some browsers targetted until the next comment?
     */
    protected $_inHack = false;
    
    
    /**
     * Constructor
     * 
     * @param array $options (currently ignored)
     * 
     * @return null
     */
    private function __construct($options) {
        $this->_options = $options;
    }
    
    /**
     * Minify a CSS string
     * 
     * @param string $css
     * 
     * @return string
     */
    protected function _process($css)
    {
        $css = str_replace("\r\n", "\n", $css);
        
        // preserve empty comment after '>'
        // http://www.webdevout.net/css-hacks#in_css-selectors
        $css = preg_replace('@>/\\*\\s*\\*/@', '>/*keep*/', $css);
        
        // preserve empty comment between property and value
        // http://css-discuss.incutio.com/?page=BoxModelHack
        $css = preg_replace('@/\\*\\s*\\*/\\s*:@', '/*keep*/:', $css);
        $css = preg_replace('@:\\s*/\\*\\s*\\*/@', ':/*keep*/', $css);
        
        // apply callback to all valid comments (and strip out surrounding ws
        $css = preg_replace_callback('@\\s*/\\*([\\s\\S]*?)\\*/\\s*@'
            ,array($this, '_commentCB'), $css);

        // remove ws around { } and last semicolon in declaration block
        $css = preg_replace('/\\s*{\\s*/', '{', $css);
        $css = preg_replace('/;?\\s*}\\s*/', '}', $css);
        
        // remove ws surrounding semicolons
        $css = preg_replace('/\\s*;\\s*/', ';', $css);
        
        // remove ws around urls
        $css = preg_replace('/
                url\\(      # url(
                \\s*
                ([^\\)]+?)  # 1 = the URL (really just a bunch of non right parenthesis)
                \\s*
                \\)         # )
            /x', 'url($1)', $css);
        
        // remove ws between rules and colons
        $css = preg_replace('/
                \\s*
                ([{;])              # 1 = beginning of block or rule separator 
                \\s*
                ([\\*_]?[\\w\\-]+)  # 2 = property (and maybe IE filter)
                \\s*
                :
                \\s*
                (\\b|[#\'"])        # 3 = first character of a value
            /x', '$1$2:$3', $css);
        
        // remove ws in selectors
        $css = preg_replace_callback('/
                (?:              # non-capture
                    \\s*
                    [^~>+,\\s]+  # selector part
                    \\s*
                    [,>+~]       # combinators
                )+
                \\s*
                [^~>+,\\s]+      # selector part
                {                # open declaration block
            /x'
            ,array($this, '_selectorsCB'), $css);
        
        // minimize hex colors
        $css = preg_replace('/([^=])#([a-f\\d])\\2([a-f\\d])\\3([a-f\\d])\\4([\\s;\\}])/i'
            , '$1#$2$3$4$5', $css);
        
        // remove spaces between font families
        $css = preg_replace_callback('/font-family:([^;}]+)([;}])/'
            ,array($this, '_fontFamilyCB'), $css);
        
        $css = preg_replace('/@import\\s+url/', '@import url', $css);
        
        // replace any ws involving newlines with a single newline
        $css = preg_replace('/[ \\t]*\\n+\\s*/', "\n", $css);
        
        // separate common descendent selectors w/ newlines (to limit line lengths)
        $css = preg_replace('/([\\w#\\.\\*]+)\\s+([\\w#\\.\\*]+){/', "$1\n$2{", $css);
        
        // Use newline after 1st numeric value (to limit line lengths).
        $css = preg_replace('/
            ((?:padding|margin|border|outline):\\d+(?:px|em)?) # 1 = prop : 1st numeric value
            \\s+
            /x'
            ,"$1\n", $css);
        
        // prevent triggering IE6 bug: http://www.crankygeek.com/ie6pebug/
        $css = preg_replace('/:first-l(etter|ine)\\{/', ':first-l$1 {', $css);
            
        return trim($css);
    }
    
    /**
     * Replace what looks like a set of selectors  
     *
     * @param array $m regex matches
     * 
     * @return string
     */
    protected function _selectorsCB($m)
    {
        // remove ws around the combinators
        return preg_replace('/\\s*([,>+~])\\s*/', '$1', $m[0]);
    }
    
    /**
     * Process a comment and return a replacement
     * 
     * @param array $m regex matches
     * 
     * @return string
     */
    protected function _commentCB($m)
    {
        $hasSurroundingWs = (trim($m[0]) !== $m[1]);
        $m = $m[1]; 
        // $m is the comment content w/o the surrounding tokens, 
        // but the return value will replace the entire comment.
        if ($m === 'keep') {
            return '/**/';
        }
        if ($m === '" "') {
            // component of http://tantek.com/CSS/Examples/midpass.html
            return '/*" "*/';
        }
        if (preg_match('@";\\}\\s*\\}/\\*\\s+@', $m)) {
            // component of http://tantek.com/CSS/Examples/midpass.html
            return '/*";}}/* */';
        }
        if ($this->_inHack) {
            // inversion: feeding only to one browser
            if (preg_match('@
                    ^/               # comment started like /*/
                    \\s*
                    (\\S[\\s\\S]+?)  # has at least some non-ws content
                    \\s*
                    /\\*             # ends like /*/ or /**/
                @x', $m, $n)) {
                // end hack mode after this comment, but preserve the hack and comment content
                $this->_inHack = false;
                return "/*/{$n[1]}/**/";
            }
        }
        if (substr($m, -1) === '\\') { // comment ends like \*/
            // begin hack mode and preserve hack
            $this->_inHack = true;
            return '/*\\*/';
        }
        if ($m !== '' && $m[0] === '/') { // comment looks like /*/ foo */
            // begin hack mode and preserve hack
            $this->_inHack = true;
            return '/*/*/';
        }
        if ($this->_inHack) {
            // a regular comment ends hack mode but should be preserved
            $this->_inHack = false;
            return '/**/';
        }
        // Issue 107: if there's any surrounding whitespace, it may be important, so 
        // replace the comment with a single space
        return $hasSurroundingWs // remove all other comments
            ? ' '
            : '';
    }
    
    /**
     * Process a font-family listing and return a replacement
     * 
     * @param array $m regex matches
     * 
     * @return string   
     */
    protected function _fontFamilyCB($m)
    {
        $m[1] = preg_replace('/
                \\s*
                (
                    "[^"]+"      # 1 = family in double qutoes
                    |\'[^\']+\'  # or 1 = family in single quotes
                    |[\\w\\-]+   # or 1 = unquoted family
                )
                \\s*
            /x', '$1', $m[1]);
        return 'font-family:' . $m[1] . $m[2];
    }
}
<?php

abstract class ExternalProcessor{
    abstract protected function getCommandLine($filename);

    function transform($in, $filename){
        $s = array(
            0 => array('pipe', 'r'),
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w')
        );
        $cmd = $this->getCommandLine($filename);
        $p = proc_open($cmd, $s, $pipes);
        if (!is_resource($p))
            throw new Exception("Failed to execute $cmd");

        fwrite($pipes[0], $in);
        fclose($pipes[0]);

        $out = stream_get_contents($pipes[1]);
        $err = stream_get_contents($pipes[2]);

        fclose($pipes[1]);
        fclose($pipes[2]);

        $r = proc_close($p);

        if ($r != 0){
            throw new Exception("Command returned $r: $err");
        }
        return $out;
    }
}

class ExternalProcessorRegistry{
    private static $transformers;
    private static $compressors;

    public static function registerTransformer($type, $cls){
        self::$transformers[$type] = $cls;
    }

    public static function registerCompressor($type, $cls){
        self::$compressors[$type] = $cls;
    }

    private static function lookup($type, $in){
        return (isset($in[$type])) ? new $in[$type]() : null;
    }

    public static function typeIsSupported($type){
        return isset(self::$transformers[$type]) ||
            isset(self::$compressors[$type]);
    }

    /**
     * @static
     * @param $type mime type of input
     * @return ExternalProcessor
     */
    public static function getTransformerForType($type){
        return self::lookup($type, self::$transformers);
    }

    /**
     * @static
     * @param $type mime type of input
     * @return ExternalProcessor
     */
    public static function getCompressorForType($type){
        return self::lookup($type, self::$compressors);
    }

}

class ProcessorUglify extends ExternalProcessor{
    protected function getCommandLine($filename){
        if (!is_executable(SACY_COMPRESSOR_UGLIFY)){
            throw new Exception('SACY_COMPRESSOR_UGLIFY defined but not executable');
        }
        return SACY_COMPRESSOR_UGLIFY;
    }
}

class ProcessorCoffee extends ExternalProcessor{
    protected function getCommandLine($filename){
        if (!is_executable(SACY_TRANSFORMER_COFFEE)){
            throw new Exception('SACY_TRANSFORMER_COFFEE defined but not executable');
        }
        return sprintf('%s -c -s', SACY_TRANSFORMER_COFFEE);
    }
}

class ProcessorSass extends ExternalProcessor{

    protected function getType(){
        return 'text/x-sass';
    }

    protected function getCommandLine($filename){
        if (!is_executable(SACY_TRANSFORMER_SASS)){
            throw new Exception('SACY_TRANSFORMER_SASS defined but not executable');
        }
        return sprintf('%s --cache-location=%s -s %s -I %s',
            SACY_TRANSFORMER_SASS,
            escapeshellarg(sys_get_temp_dir()),
            $this->getType() == 'text/x-scss' ? '--scss' : '',
            escapeshellarg(dirname($filename))
        );
    }
}

class ProcessorScss extends ProcessorSass{
    protected function getType(){
        return 'text/x-scss';
    }
}

class ProcessorLess extends ExternalProcessor{
    protected function getCommandLine($filename){
        if (!is_executable(SACY_TRANSFORMER_LESS)){
            throw new Exception('SACY_TRANSFORMER_LESS defined but not executable');
        }
        return sprintf(
            '%s -I%s -',
            SACY_TRANSFORMER_LESS,
            escapeshellarg(dirname($filename))
        );
    }
}



if (defined('SACY_COMPRESSOR_UGLIFY')){
    ExternalProcessorRegistry::registerCompressor('text/javascript', 'ProcessorUglify');
}

if (defined('SACY_TRANSFORMER_COFFEE')){
    ExternalProcessorRegistry::registerTransformer('text/coffeescript', 'ProcessorCoffee');
}

if (defined('SACY_TRANSFORMER_SASS')){
    ExternalProcessorRegistry::registerTransformer('text/x-sass', 'ProcessorSass');
    ExternalProcessorRegistry::registerTransformer('text/x-scss', 'ProcessorScss');
}

if (defined('SACY_TRANSFORMER_LESS')){
    ExternalProcessorRegistry::registerTransformer('text/x-less', 'ProcessorLess');
}
<?php
/**
 * jsmin.php - PHP implementation of Douglas Crockford's JSMin.
 *
 * This is pretty much a direct port of jsmin.c to PHP with just a few
 * PHP-specific performance tweaks. Also, whereas jsmin.c reads from stdin and
 * outputs to stdout, this library accepts a string as input and returns another
 * string as output.
 *
 * PHP 5 or higher is required.
 *
 * Permission is hereby granted to use this version of the library under the
 * same terms as jsmin.c, which has the following license:
 *
 * --
 * Copyright (c) 2002 Douglas Crockford  (www.crockford.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * --
 *
 * @package JSMin
 * @author Ryan Grove <ryan@wonko.com>
 * @copyright 2002 Douglas Crockford <douglas@crockford.com> (jsmin.c)
 * @copyright 2008 Ryan Grove <ryan@wonko.com> (PHP port)
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @version 1.1.1 (2008-03-02)
 * @link http://code.google.com/p/jsmin-php/
 */

class JSMin {
  const ORD_LF    = 10;
  const ORD_SPACE = 32;

  protected $a           = '';
  protected $b           = '';
  protected $input       = '';
  protected $inputIndex  = 0;
  protected $inputLength = 0;
  protected $lookAhead   = null;
  protected $output      = '';

  // -- Public Static Methods --------------------------------------------------

  public static function minify($js) {
    $jsmin = new JSMin($js);
    return $jsmin->min();
  }

  // -- Public Instance Methods ------------------------------------------------

  public function __construct($input) {
    $this->input       = str_replace("\r\n", "\n", $input);
    $this->inputLength = strlen($this->input);
  }

  // -- Protected Instance Methods ---------------------------------------------

  protected function action($d) {
    switch($d) {
      case 1:
        $this->output .= $this->a;

      case 2:
        $this->a = $this->b;

        if ($this->a === "'" || $this->a === '"') {
          for (;;) {
            $this->output .= $this->a;
            $this->a       = $this->get();

            if ($this->a === $this->b) {
              break;
            }

            if (ord($this->a) <= self::ORD_LF) {
              throw new JSMinException('Unterminated string literal.');
            }

            if ($this->a === '\\') {
              $this->output .= $this->a;
              $this->a       = $this->get();
            }
          }
        }

      case 3:
        $this->b = $this->next();

        if ($this->b === '/' && (
            $this->a === '(' || $this->a === ',' || $this->a === '=' ||
            $this->a === ':' || $this->a === '[' || $this->a === '!' ||
            $this->a === '&' || $this->a === '|' || $this->a === '?')) {

          $this->output .= $this->a . $this->b;

          for (;;) {
            $this->a = $this->get();

            if ($this->a === '/') {
              break;
            } elseif ($this->a === '\\') {
              $this->output .= $this->a;
              $this->a       = $this->get();
            } elseif (ord($this->a) <= self::ORD_LF) {
              throw new JSMinException('Unterminated regular expression '.
                  'literal.');
            }

            $this->output .= $this->a;
          }

          $this->b = $this->next();
        }
    }
  }

  protected function get() {
    $c = $this->lookAhead;
    $this->lookAhead = null;

    if ($c === null) {
      if ($this->inputIndex < $this->inputLength) {
        $c = $this->input[$this->inputIndex];
        $this->inputIndex += 1;
      } else {
        $c = null;
      }
    }

    if ($c === "\r") {
      return "\n";
    }

    if ($c === null || $c === "\n" || ord($c) >= self::ORD_SPACE) {
      return $c;
    }

    return ' ';
  }

  protected function isAlphaNum($c) {
    return ord($c) > 126 || $c === '\\' || preg_match('/^[\w\$]$/', $c) === 1;
  }

  protected function min() {
    $this->a = "\n";
    $this->action(3);

    while ($this->a !== null) {
      switch ($this->a) {
        case ' ':
          if ($this->isAlphaNum($this->b)) {
            $this->action(1);
          } else {
            $this->action(2);
          }
          break;

        case "\n":
          switch ($this->b) {
            case '{':
            case '[':
            case '(':
            case '+':
            case '-':
              $this->action(1);
              break;

            case ' ':
              $this->action(3);
              break;

            default:
              if ($this->isAlphaNum($this->b)) {
                $this->action(1);
              }
              else {
                $this->action(2);
              }
          }
          break;

        default:
          switch ($this->b) {
            case ' ':
              if ($this->isAlphaNum($this->a)) {
                $this->action(1);
                break;
              }

              $this->action(3);
              break;

            case "\n":
              switch ($this->a) {
                case '}':
                case ']':
                case ')':
                case '+':
                case '-':
                case '"':
                case "'":
                  $this->action(1);
                  break;

                default:
                  if ($this->isAlphaNum($this->a)) {
                    $this->action(1);
                  }
                  else {
                    $this->action(3);
                  }
              }
              break;

            default:
              $this->action(1);
              break;
          }
      }
    }

    return $this->output;
  }

  protected function next() {
    $c = $this->get();

    if ($c === '/') {
      switch($this->peek()) {
        case '/':
          for (;;) {
            $c = $this->get();

            if (ord($c) <= self::ORD_LF) {
              return $c;
            }
          }

        case '*':
          $this->get();

          for (;;) {
            switch($this->get()) {
              case '*':
                if ($this->peek() === '/') {
                  $this->get();
                  return ' ';
                }
                break;

              case null:
                throw new JSMinException('Unterminated comment.');
            }
          }

        default:
          return $c;
      }
    }

    return $c;
  }

  protected function peek() {
    $this->lookAhead = $this->get();
    return $this->lookAhead;
  }
}

// -- Exceptions ---------------------------------------------------------------
class JSMinException extends Exception {}
?><?php

if (!defined("____SACY_BUNDLED"))
    include_once(implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'ext-translators.php')));

if (!class_exists('JSMin') && !ExternalProcessorRegistry::typeIsSupported('text/javascript'))
    include_once(implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'jsmin.php')));

if (!class_exists('Minify_CSS'))
    include_once(implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'cssmin.php')));

if (!class_exists('lessc') && !ExternalProcessorRegistry::typeIsSupported('text/x-less')){
    $less = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'lessc.inc.php'));
    if (file_exists($less)){
        include_once($less);
    }
}

if(function_exists('CoffeeScript\compile')){
    include_once(implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'coffeescript.php')));
} else if (!ExternalProcessorRegistry::typeIsSupported('text/coffeescript')){
    $coffee = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), '..', 'coffeescript', 'coffeescript.php'));
    if (file_exists($coffee)){
        include_once($coffee);
        include_once(implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'coffeescript.php')));
    }
}


if (!class_exists('SassParser') && !ExternalProcessorRegistry::typeIsSupported('text/x-sass')){
    $sass = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), '..', 'sass', 'SassParser.php'));
    if (file_exists($sass)){
        include_once($sass);
    }
}

/*
 *   An earlier experiment contained a real framework for tag
 *   and parser registration. In the end, this turned out
 *   to be much too complex if we just need to support two tags
 *   for two types of resources.
 */
class sacy_FileExtractor{
    private $_cfg;

    function __construct(sacy_Config $config){
        $this->_cfg = $config;
    }

    function extractFile($tag, $attrdata, $content){
        switch($tag){
            case 'link':
                $fn = 'extract_css_file';
                break;
            case 'script':
                $fn = 'extract_js_file';
                break;
            default: throw new sacy_Exception("Cannot handle tag: $tag");
        }
        return $this->$fn($attrdata, $content);
    }

    private function urlToFile($ref){
        $u = parse_url($ref);
        if ($u === false) return false;
        if (isset($u['host']) || isset($u['scheme']))
            return false;

        if ($this->_cfg->get('query_strings') == 'ignore')
            if (isset($u['query'])) return false;

        $ref = $u['path'];
        $path = array($_SERVER['DOCUMENT_ROOT']);
        if ($ref[0] != '/')
            $path[] = $_SERVER['PHP_SELF'];
        $path[] = $ref;
        return realpath(implode(DIRECTORY_SEPARATOR, $path));

    }


    private function extract_css_file($attrdata, $content){
        // if any of these conditions are met, this handler will decline
        // handling the tag:
        //
        //  - the tag contains content (invalid markup)
        //  - the tag uses any rel beside 'stylesheet' (valid, but not supported)
        //  - the tag uses a not-supported type (
        $attrs = sacy_extract_attrs($attrdata);
        $attrs['type'] = strtolower($attrs['type']);
        if ($this->_cfg->getDebugMode() == 3 &&
                !sacy_CssRenderHandler::willTransformType($attrs['type']))
            return false;

        if (empty($content) && (strtolower($attrs['rel']) == 'stylesheet') &&
            (!isset($attrs['type']) ||
            (in_array(strtolower($attrs['type']), sacy_CssRenderHandler::supportedTransformations())))){
            if (!isset($attrs['media']))
                $attrs['media'] = "";

            $path = $this->urlToFile($attrs['href']);
            if ($path === false) return false;

            return array(
                'group' => $attrs['media'],
                'name' => $path,
                'type' => $attrs['type']
            );
        }
        return false;
    }

    private function validTag($attrs){
        $types = array_merge(array('text/javascript', 'application/javascript'), sacy_JavascriptRenderHandler::supportedTransformations());
        return  in_array(
            $attrs['type'],
            $types
        ) && !empty($attrs['src']);
    }

    private function extract_js_file($attrdata, $content){
        // don't handle non-empty tags
        if (preg_match('#\S+#', $content)) return false;

        $attrs = sacy_extract_attrs($attrdata);
        $attrs['type'] = strtolower($attrs['type']);
        if ($this->_cfg->getDebugMode() == 3 &&
                !sacy_JavaScriptRenderHandler::willTransformType($attrs['type'])){
            return false;
        }


        if ($this->validTag($attrs)) {
            $path = $this->urlToFile($attrs['src']);
            if ($path === false) return false;
            return array(
                'group' => '',
                'name' => $path,
                'type' => $attrs['type']
            );
        }
        return false;
    }

}

class sacy_Config{
    private $params;

    public function get($key){
        return $this->params[$key];
    }

    public function __construct($params = null){
        $this->params['query_strings'] = defined('SACY_QUERY_STINGS') ? SACY_QUERY_STRINGS : 'ignore';
        $this->params['write_headers'] = defined('SACY_WRITE_HEADERS') ? SACY_WRITE_HEADERS : true;
        $this->params['debug_toggle']  = defined('SACY_DEBUG_TOGGLE') ? SACY_DEBUG_TOGGLE : '_sacy_debug';

        if (is_array($params))
            $this->setParams($params);
    }

    public function getDebugMode(){
        if ($this->params['debug_toggle'] === false)
            return 0;
        if (isset($_GET[$this->params['debug_toggle']]))
            return intval($_GET[$this->params['debug_toggle']]);
        if (isset($_COOKIE[$this->params['debug_toggle']]))
            return intval($_COOKIE[$this->params['debug_toggle']]);
        return 0;

    }

    public function setParams($params){
        foreach($params as $key => $value){
            if (!in_array($key, array('query_strings', 'write_headers', 'debug_toggle')))
                throw new sacy_Exception("Invalid option: $key");
        }
        if (isset($params['query_strings']) && !in_array($params['query_strings'], array('force-handle', 'ignore')))
            throw new sacy_Exception("Invalid setting for query_strings: ".$params['query_strings']);
        if (isset($params['write_headers']) && !in_array($params['write_headers'], array(true, false), true))
            throw new sacy_Exception("Invalid setting for write_headers: ".$params['write_headers']);


        $this->params = array_merge($this->params, $params);
    }

}

class sacy_CacheRenderer {
    private $_smarty;
    private $_cfg;

    function __construct(sacy_Config $config, $smarty){
        $this->_smarty = $smarty;
        $this->_cfg = $config;
    }

    function renderFiles($tag, $cat, $files){
        switch($tag){
            case 'link':
                $fn = 'render_css_files';
                break;
            case 'script':
                $fn = 'render_js_files';
                break;
            default: throw new sacy_Exception("Cannot handle tag: $tag");
        }
        return $this->$fn($files, $cat);
    }


    private function render_css_files($files, $cat){
        $ref = sacy_generate_cache($this->_smarty, $files, new sacy_CssRenderHandler($this->_cfg, $this->_smarty));
        if (!$ref) return false;
        $cs = $cat ? sprintf(' media="%s"', htmlspecialchars($cat, ENT_QUOTES)) : '';
        return sprintf('<link rel="stylesheet" type="text/css"%s href="%s" />'."\n",
                       $cs, htmlspecialchars($ref, ENT_QUOTES)
                      );

    }

    private function render_js_files($files, $cat){
        $ref = sacy_generate_cache($this->_smarty, $files, new sacy_JavascriptRenderHandler($this->_cfg, $this->_smarty));
        if (!$ref) return false;
        return sprintf('<script type="text/javascript" src="%s"></script>'."\n", htmlspecialchars($ref, ENT_QUOTES));
    }
}

interface sacy_CacheRenderHandler{
    function __construct(sacy_Config $cfg, $smarty);
    function getFileExtension();
    static function willTransformType($type);
    function writeHeader($fh, $files);
    function processFile($fh, $file);
    function getConfig();
}

abstract class sacy_ConfiguredRenderHandler implements sacy_CacheRenderHandler{
    private $_smarty;
    private $_cfg;

    function __construct(sacy_Config $cfg, $smarty){
        $this->_smarty = $smarty;
        $this->_cfg = $cfg;
    }

    protected function getSmarty(){
        return $this->_smarty;
    }

    public function getConfig(){
        return $this->_cfg;
    }

    static public function willTransformType($type){
        return false;
    }
}

class sacy_JavaScriptRenderHandler extends sacy_ConfiguredRenderHandler{
    static function supportedTransformations(){
        if (function_exists('CoffeeScript\compile') || ExternalProcessorRegistry::typeIsSupported('text/coffeescript'))
            return array('text/coffeescript');
        return array();
    }

    static function willTransformType($type){
        // transforming everything but plain old CSS
        return in_array($type, self::supportedTransformations());
    }

    function getFileExtension() { return '.js'; }

    function writeHeader($fh, $files){
        fwrite($fh, "/*\nsacy javascript cache dump \n\n");
        fwrite($fh, "This dump has been created from the following files:\n");
        foreach($files as $file){
            fprintf($fh, "    - %s\n", str_replace($_SERVER['DOCUMENT_ROOT'], '<root>', $file['name']));
        }
        fwrite($fh, "*/\n\n");
    }

    function processFile($fh, $file){
        $debug = $this->getConfig()->getDebugMode() == 3;

        if ($this->getConfig()->get('write_headers'))
            fprintf($fh, "\n/* %s */\n", str_replace($_SERVER['DOCUMENT_ROOT'], '<root>', $file['name']));
        $js = @file_get_contents($file['name']);
        if ($js == false){
            fwrite($fh, "/* <Error accessing file> */\n");
            $this->getSmarty()->trigger_error("Error accessing JavaScript-File: ".$file['name']);
            return;
        }
        if ($file['type'] == 'text/coffeescript'){
            $js = ExternalProcessorRegistry::typeIsSupported('text/coffeescript') ?
                ExternalProcessorRegistry::getTransformerForType('text/coffeescript')->transform($js, $file['name']) :
                Coffeescript::build($js);
        }
        if ($debug){
            fwrite($fh, $js);
        }else{
            fwrite($fh, ExternalProcessorRegistry::typeIsSupported('text/javascript') ?
                ExternalProcessorRegistry::getCompressorForType('text/javascript')->transform($js, $file['name']) :
                JSMin::minify($js)
            );
        }
    }

}

class sacy_CssRenderHandler extends sacy_ConfiguredRenderHandler{
    static function supportedTransformations(){
        $res = array('', 'text/css');
        if (class_exists('lessc') || ExternalProcessorRegistry::typeIsSupported('text/x-less'))
            $res[] = 'text/x-less';
        if (class_exists('SassParser') || ExternalProcessorRegistry::typeIsSupported('text/x-sass'))
            $res = array_merge($res, array('text/x-sass', 'text/x-scss'));

        return $res;
    }

    function getFileExtension() { return '.css'; }

    static function willTransformType($type){
        // transforming everything but plain old CSS
        return !in_array($type, array('', 'text/css'));
    }

    function writeHeader($fh, $files){
        fwrite($fh, "/*\nsacy css cache dump \n\n");
        fwrite($fh, "This dump has been created from the following files:\n");
        foreach($files as $file){
            fprintf($fh, "    - %s\n", str_replace($_SERVER['DOCUMENT_ROOT'], '<root>', $file['name']));
        }
        fwrite($fh, "*/\n\n");
    }

    function processFile($fh, $file){
        $debug = $this->getConfig()->getDebugMode() == 3;
        if ($this->getConfig()->get('write_headers'))
           fprintf($fh, "\n/* %s */\n", str_replace($_SERVER['DOCUMENT_ROOT'], '<root>', $file['name']));
        $css = @file_get_contents($file['name']); //maybe stream this later to save memory?
        if ($css == false){
            fwrite($fh, "/* <Error accessing file> */\n");
            $this->getSmarty()->trigger_error("Error accessing CSS-File: ".$file['name']);
            return;
        }

        if (ExternalProcessorRegistry::typeIsSupported($file['type'])){
            $css = ExternalProcessorRegistry::getTransformerForType($file['type'])->transform($css, $file['name']);
        }else{
            if ($file['type'] == 'text/x-less'){
                $less = new lessc();
                $less->importDir = dirname($file['name']).'/'; #lessphp concatenates without a /
                $css = $less->parse($css);
            }
            if (in_array($file['type'], array('text/x-scss', 'text/x-sass'))){
                $config = array(
                    'cache' => false, // no need. WE are the cache!
                    'debug_info' => $debug,
                    'line' => $debug,
                    'load_paths' => array(dirname($file['name'])),
                    'filename' => $file['name'],
                    'quiet' => true,
                    'style' => $debug ? 'nested' : 'compressed'
                );
                $sass = new SassParser($config);
                $css = $sass->toCss($css, false); // isFile?
            }
        }

        if ($debug){
            fwrite($fh, Minify_CSS_UriRewriter::rewrite(
                $css,
                dirname($file['name']),
                $_SERVER['DOCUMENT_ROOT'],
                array()
            ));
        }else{
            fwrite($fh, Minify_CSS::minify($css, array(
                'currentDir' => dirname($file['name'])
            )));
        }
    }
}

class sacy_Exception extends Exception {}

function sacy_extract_attrs($attstr){
    $attextract = '#([a-z]+)\s*=\s*(["\'])\s*(.*?)\s*\2#';
    $res = array();
    if (!preg_match_all($attextract, $attstr, $m)) return false;
    $res = array();
    foreach($m[1] as $idx => $name){
        $res[strtolower($name)] = $m[3][$idx];
    }
    return $res;

}

function sacy_generate_cache(&$smarty, $files, sacy_CacheRenderHandler $rh){
    if (!is_dir(ASSET_COMPILE_OUTPUT_DIR))
        mkdir(ASSET_COMPILE_OUTPUT_DIR);

    $f = create_function('$f', 'return basename($f["name"], "'.$rh->getFileExtension().'");');
    $ident = implode('-', array_map($f, $files));
    if (strlen($ident) > 120)
        $ident = 'many-files-'.md5($ident);
    $max = 0;
    foreach($files as $f){
        $max = max($max, filemtime($f['name']));
    }
    // not using the actual content for quicker access
    $key = md5($max . serialize($files) . $rh->getConfig()->getDebugMode());
    $cfile = ASSET_COMPILE_OUTPUT_DIR . DIRECTORY_SEPARATOR ."$ident-$key".$rh->getFileExtension();
    $pub = ASSET_COMPILE_URL_ROOT . "/$ident-$key".$rh->getFileExtension();

    if (file_exists($cfile) && ($rh->getConfig()->getDebugMode() != 2)){
        return $pub;
    }

    if (!sacy_write_cache($smarty, $cfile, $files, $rh)){
        /* If something went wrong in here we delete the cache file

           This ensures that on reload, sacy would see that no cached file exists and
           will retry the process.

           This is helpful for example if you define() one of the external utilities
           and screw something up in the process.
        */
        if (file_exists($cfile)){
            @unlink($cfile);
        }
        return false;
    }

    return $pub;
}

function sacy_write_cache(&$smarty, $cfile, $files, sacy_CacheRenderHandler $rh){
    $lockfile = $cfile.".lock";
    $fhl = @fopen($lockfile, 'w');
    if (!$fhl){
        trigger_error("Cannot create cache-lockfile: $lockfile", E_USER_WARNING);
        return false;
    }
    $wb = false;
    if (!@flock($fhl, LOCK_EX | LOCK_NB, $wb)){
        trigger_error("Canot lock cache-lockfile: $lockfile", E_USER_WARNING);
        return false;
    }
    if ($wb){
        // another process is writing the cache. Let's just return false
        // the caller will leave the CSS unaltered
        return false;
    }
    $fhc = @fopen($cfile, 'w');
    if (!$fhc){
        trigger_error("Cannot open cache file: $cfile", E_USER_WARNING);
        fclose($fhl);
        unlink($lockfile);
        return false;
    }

    if ($rh->getConfig()->get('write_headers'))
        $rh->writeHeader($fhc, $files);

    $res = true;
    foreach($files as $file){
        try{
            $rh->processFile($fhc, $file);
        }catch(Exception $e){
            trigger_error(sprintf(
                "Exception %s while processing %s:\n\n%s",
                get_class($e),
                $file['name'],
                $e->getMessage()
            ), E_USER_WARNING);
            $res = false;
            break;
        }
    }

    fclose($fhc);
    fclose($fhl);
    unlink($lockfile);
    return $res;
}
����w	{��])�����u$   GBMB