<?php
    function smarty_function_social_icons(array $params, Smarty_Internal_Template $template)
    {
        $frontend = new \XpnCMS\Controller\Frontend();

        return $frontend->getSocialIcons();
    }
