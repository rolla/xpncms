<?php

function smarty_modifier_timer($mode = 'begin', $msg = '')
{
    global $_timer_blocks, $_timer_history;
    switch ($mode) {
        case 'begin':
            $_timer_blocks[] = array(microtime(), $msg);
            break;
        case 'end':
            $last = array_pop($_timer_blocks);
            $_start = $last[0];
            $_msg = $last[1];
            list($a_micro, $a_int) = explode(' ', $_start);
            list($b_micro, $b_int) = explode(' ', microtime());
            $elapsed = ($b_int - $a_int) + ($b_micro - $a_micro);
            $_timer_history[] = array($elapsed, $_msg, "$_msg [".round($elapsed, 4).'s '.(round(1 / $elapsed, 2)).'/s]');
            if ($msg) {
                return "$_msg: {$elapsed}s \n";
            }

            return $elapsed;
        break;
        case 'list':
            $o = '';
            foreach ($_timer_history as $mark) {
                $o .= $mark[2]." \n";
            }

            return $o;
        break;
        case 'stop':
            $result = '';
            while (!empty($_timer_blocks)) {
                $result .= util::timer('end', $msg);
            }

            return $result;
        break;
    }
}
