<?php
    function smarty_function_timeline(array $params, Smarty_Internal_Template $template)
    {

        /*
        echo '<pre>';
        print_r($_SESSION['timeline']['twitter_timeline']);
        echo '</pre>';
        */
        $provider = '';
        $timeline = '';
        if (isset($_SESSION['timeline'])) {
            $timeline = '';
            if (isset($_SESSION['timeline']['twitter'])) {
                $provider = 'twitter';
                // iterate over the user timeline
                foreach ($_SESSION['timeline']['twitter'] as $item => $val) {

                    //echo '<pre>';
                    //print_r($val);
                    //echo '</pre>';

                    //$timeline .= '<div class="twt-timeline">'.$item->user->displayName.': ' .$item->text.'<hr /></div>';
                    $timeline .= '<div class="twt-timeline" data-cnt="'.$item.'">'.$val.'</div>'.PHP_EOL;
                }
            } else {
                $provider = 'facebook';
                foreach ($_SESSION['timeline']['facebook'] as $item => $val) {
                    $timeline .= '<div class="fb-timeline" data-cnt="'.$item.'">'.$val.'</div>'.PHP_EOL;
                }
            }

            $html = '
			<div class="timeline-container">
				<div class="timeline-inner">
					<fieldset>
						<legend>'._('Your stream from').' '.$provider.' - '._('Wolla').'</legend>
						'.$timeline.'
					</fieldset>
				</div>
			</div>
			';

            return $html;
        }
    }
