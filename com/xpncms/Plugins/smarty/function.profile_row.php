<?php
use XpnCMS\Controller\Frontend;

function smarty_function_profile_row(array $params, Smarty_Internal_Template $template)
{
    $frontend = new Frontend();

    return $frontend->getProfileRow();
}
