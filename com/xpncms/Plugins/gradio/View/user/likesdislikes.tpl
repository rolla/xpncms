{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="body" append}
  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    {if isset($likes) }
      <li class="active"><a href="#likes" role="tab" data-toggle="tab"><h1>{t}Your likes{/t} ({$likes.count})</h1></a></li>
      {elseif isset($likeserror)}
      <li class="active"><a href="#likes" role="tab" data-toggle="tab"><h1>{t}Your likes{/t} (0)</h1></a></li>
    {/if}


    {if isset($dislikes) }
      <li><a href="#dislikes" role="tab" data-toggle="tab"><h1>{t}Your dislikes{/t} ({$dislikes.count})</h1></a></li>
      {elseif isset($dislikeserror)}
      <li><a href="#dislikes" role="tab" data-toggle="tab"><h1>{t}Your dislikes{/t} (0)</h1></a></li>
    {/if}
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div class="tab-pane fade in active" id="likes">
      {if isset($likes) }
        <div class="clearfix"></div>
        <div class="likes-table">
          <table class="table">
            <thead>
              <tr>
                <th>{t}ID{/t}</th>
                <th>{t}Name{/t}</th>
                <th>{t}Weight{/t}</th>
                <th>{t}Last time streamed{/t}</th>
                <th>{t}Played{/t}</th>
                <th>{t}Date added{/t}</th>
                <th>{t}Picture{/t}</th>
                <th>{t}Options{/t}</th>
              </tr>
            </thead>
            <tbody>
              {foreach $likes.tunes as $song}

                <tr>
                  <td class="sat-id"><div>{$song.ID}</div></td>
                  <td class="sat-sat"><div>{$song.artist} - {$song.title}</div></td>
                  <td class="sat-weight"><div>{$song.weight}</div></td>
                  <td class="music-date-played"><div>{$song.date_played|relative_date}</div></td>
                  <td class="sat-kat"><div>{$song.count_played}</div></td>
                  <td class="sat-dat"><div>{$song.time|relative_date}</div></td>
                  <td class="sat-bilde"><div><img src="{$song.phpThumbPictureURL}" width="200"/></div></td>
                  <td class="music-options">
                  {if !empty($song.youtube) }
                    <div class="tune-table-title" title="{t}Youtube{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.youtube}" target="_blank"><img src="{$url}themes/{$template}/img/icons/youtube-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/youtube-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($song.soundcloud) }
                    <div class="tune-table-title" title="{t}Soundcloud{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.soundcloud}" target="_blank"><img src="{$url}themes/{$template}/img/icons/soundcloud-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/soundcloud-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($song.beatport) }
                    <div class="tune-table-title" title="{t}Beatport{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.beatport}" target="_blank"><img src="{$url}themes/{$template}/img/icons/beatport-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/beatport-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($song.itunes) }
                    <div class="tune-table-title" title="{t}iTunes{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.itunes}" target="_blank"><img src="{$url}themes/{$template}/img/icons/itunes-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/itunes-logo-inactive.svg"/>
                    </div>
                  {/if}
                  </td>
                </tr>
              <div class="clearfix"></div>

            {/foreach}
            </tbody>
          </table>
        </div>
      {/if}

    </div>
    <div class="tab-pane fade" id="dislikes">

      {if isset($dislikes) }

        <div class="clearfix"></div>
        <div class="dislikes-table">
          <table class="table">
            <thead>
              <tr>
                <th>{t}ID{/t}</th>
                <th>{t}Name{/t}</th>
                <th>{t}Weight{/t}</th>
                <th>{t}Last time streamed{/t}</th>
                <th>{t}Played{/t}</th>
                <th>{t}Date added{/t}</th>
                <th>{t}Picture{/t}</th>
                <th>{t}Options{/t}</th>
              </tr>
            </thead>
            <tbody>
              {foreach $dislikes.tunes as $song}

                <tr>
                  <td class="sat-id"><div>{$song.ID}</div></td>
                  <td class="sat-sat"><div>{$song.artist} - {$song.title}</div></td>
                  <td class="sat-weight"><div>{$song.weight}</div></td>
                  <td class="music-date-played"><div>{$song.date_played|relative_date}</div></td>
                  <td class="sat-kat"><div>{$song.count_played}</div></td>
                  <td class="sat-dat"><div>{$song.time|relative_date}</div></td>
                  <td class="sat-bilde"><div><img src="{$song.phpThumbPictureURL}" width="200"/></div></td>
                  <td class="music-options">
                  {if !empty($song.youtube) }
                    <div class="tune-table-title" title="{t}Youtube{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.youtube}" target="_blank"><img src="{$url}themes/{$template}/img/icons/youtube-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/youtube-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($song.soundcloud) }
                    <div class="tune-table-title" title="{t}Soundcloud{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.soundcloud}" target="_blank"><img src="{$url}themes/{$template}/img/icons/soundcloud-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/soundcloud-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($song.beatport) }
                    <div class="tune-table-title" title="{t}Beatport{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.beatport}" target="_blank"><img src="{$url}themes/{$template}/img/icons/beatport-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/beatport-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($song.itunes) }
                    <div class="tune-table-title" title="{t}iTunes{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$song.itunes}" target="_blank"><img src="{$url}themes/{$template}/img/icons/itunes-logo-opacity-50.svg"/></a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/itunes-logo-inactive.svg"/>
                    </div>
                  {/if}
                  </td>
                </tr>
              <div class="clearfix"></div>

            {/foreach}
            </tbody>
          </table>
        </div>
      {/if}

    </div>

  </div>

{/block}