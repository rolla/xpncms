{capture name="page_title"}
{block name="title" prepend}{$pageTitle} :{/block}
{/capture}
{block name="content" append}
<div class="container-fluid">
  <section class="main-section" id="your-likes">
    <div class="content">
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs.tpl" activeTab=7}
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs-check.tpl" activeTab=1}
      <h3>{$pageTitle} ({$tunes|@count})</h3>
      <div class="clearfix"></div>
      
      {if ($userData)}
      
      <div class="recent-table">
        <table class="table">
          <thead>
            <tr>
              <th>{t}ID{/t}</th>
              <th>{t}Name{/t}</th>
              <th>{t}Ratings{/t}</th>
              <th>{t}Duration{/t}</th>
              <th>{t}Last played{/t}</th>
              <th>{t}Played{/t}</th>
              <th>{t}Date added{/t}</th>
              <th>{t}Year{/t}</th>
              <th>{t}Picture{/t}</th>
              <th>{t}Options{/t}</th>
            </tr>
          </thead>
          <tbody>
            {if isset($tunes) }
              {foreach $tunes as $tune}
                <tr>
                  <td class="music-id"><div>{$tune.idWithLink}</div></td>
                  <td class="music-artist"><div>{$tune.artist} - {$tune.title}</div></td>
                  <td class="music-ratings">
                    <div
                      class="popover-data ratings-count"
                      data-toggle="popover"
                      data-placement="left"
                      data-trigger="hover"
                      data-html="true"
                      data-content="{tune_raters ratings=$tune.ratings}">
                        <span class="badge">{$tune.ratings|@count}</span>
                    </div>
                  </td>
                  <td class="music-duration"><div>{$tune.duration}</div></td>
                  <td class="music-date-played"><div>{$tune.date_played|relative_date}</div></td>
                  <td class="music-count-played"><div>{$tune.count_played} {if ($gradioTeam && !$tune.enabled)}<span class="badge btn-warning">{t}Disabled{/t}</span>{/if}</div></td>
                  <td class="music-date-added"><div>{$tune.date_added|relative_date}</div></td>
                  <td class="music-year"><div>{$tune.year}</div></td>
                  <td class="music-img"><div><img src="{$tune.phpThumbPictureURL}" width="200"></div></td>
                  <td class="music-options">
                  {if !empty($tune.youtube) }
                    <div class="tune-table-title" title="{t}Youtube{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$tune.youtube}" target="_blank">
                      <img src="{$url}themes/{$template}/img/icons/youtube-logo-opacity-50.svg"/>
                    </a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/youtube-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($tune.soundcloud) }
                    <div class="tune-table-title" title="{t}Soundcloud{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$tune.soundcloud}" target="_blank">
                      <img src="{$url}themes/{$template}/img/icons/soundcloud-logo-opacity-50.svg"/>
                    </a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/soundcloud-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($tune.beatport) }
                    <div class="tune-table-title" title="{t}Beatport{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$tune.beatport}" target="_blank">
                      <img src="{$url}themes/{$template}/img/icons/beatport-logo-opacity-50.svg"/>
                    </a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/beatport-logo-inactive.svg"/>
                    </div>
                  {/if}
                  {if !empty($tune.itunes) }
                    <div class="tune-table-title" title="{t}iTunes{/t}"></div>
                    <div class="tune-table-content">
                    <a href="{$tune.itunes}" target="_blank">
                      <img src="{$url}themes/{$template}/img/icons/itunes-logo-opacity-50.svg"/>
                    </a>
                    </div>
                  {else}
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/itunes-logo-inactive.svg"/>
                    </div>
                  {/if}
                  </td>
                </tr>
              <div class="clearfix"></div>
            {/foreach}
            {/if}
          </tbody>
        </table>
      </div>
      
      {/if}
          
    </div>
  </section>
</div>
{/block}
