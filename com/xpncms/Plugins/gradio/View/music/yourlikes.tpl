{capture name="page_title"}
{block name="title" prepend}{$pageTitle} :{/block}
{/capture}
{block name="content" append}
<div class="container-fluid">
  <section class="main-section" id="your-likes">
    <div class="content">
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs.tpl" activeTab=4}
      <h3>{$pageTitle} ({$tunes|@count})</h3>
      <div class="clearfix"></div>
      {if ($userData)}
        <div class="recent-table">
        {if isset($tunes) }
          {foreach $tunes as $tune}
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
              {include file="{APP_PLUGIN}gradio/View/music/partial/tune-card.tpl" tune=$tune}
            </div>
          {/foreach}
        {/if}
        </div>
      {/if}
    </div>
  </section>
</div>
{/block}
