{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
<div class="clearfix"></div>
<table cellpadding="0" cellspacing="0" border="0" class="table display" id="gradio_latest_tracks">
    <thead>
        <tr>
            <!--<th width="8%">Info</th>-->
            <th width="5%">ID</th>
            <th width="20%">Artist</th>
            <th width="20%">Title</th>
            <th width="5%">Duration</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" class="dataTables_empty">Loading data from server</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <!--<th>Info</th>-->
            <th>ID</th>
            <th>Artist</th>
            <th>Title</th>
            <th>Duration</th>
    </tfoot>
</table>