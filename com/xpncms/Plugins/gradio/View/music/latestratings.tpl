{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
<div class="clearfix"></div>
<div class="ratings-table">
    <table class="table">
        <thead>
            <tr>
                <th>{t}ID{/t}</th>
                <th>{t}First name Last name{/t}</th>
                <th>{t}Rating{/t}</th>
                <th>{t}Date{/t}</th>
            </tr>
        </thead>
        <tbody>

            {if isset($ratings) }

                {foreach $ratings as $rating}

                    <tr>
                        <td class="sat-songid"><div><a href="{$url}tune/{$rating.songid}/" class="ajax-nav">{$rating.songid}</a></div></td>
                        <td class="sat-first-name"><div>{$rating.first_name} {$rating.last_name}</div></td>
                        <td class="sat-rating"><div>{$rating.rating}</div></td>
                        <td class="sat-time"><div>{$rating.time|relative_date}</div></td>

                    </tr>
                <div class="clearfix"></div>

            {/foreach}
            </tbody>
        </table>
    </div>

{/if}