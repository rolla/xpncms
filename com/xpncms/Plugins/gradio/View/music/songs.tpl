{capture name="page_title"}
  {block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="body" append}
  <div class="content">
    <section class="main-section" id="gradio-songs">
      <h3>{t}Gradio tunes DB{/t}</h3>
      <div class="clearfix"></div>

      <table cellpadding="0" cellspacing="0" border="0" class="table" id="gradio_songs">
        <thead>
          <tr>
            <!--<th width="8%">Info</th>-->
            <th width="5%">{t}ID{/t}</th>
            <th width="20%">{t}Artist{/t}</th>
            <th width="20%">{t}Title{/t}</th>
            <th width="20%">{t}Album{/t}</th>
            <th width="5%">{t}Weight{/t}</th>
            <th width="5%">{t}Duration{/t}</th>
            <th width="5%">{t}Added{/t}</th>
            <th width="5%">{t}Count played{/t}</th>
            <!--
            <th width="3%">{t}Likes{/t}</th>
            <th width="3%">{t}Dislikes{/t}</th>
            -->
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="10" class="dataTables_empty">{t}Loading data{/t}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <th>{t}ID{/t}</th>
            <th>{t}Artist{/t}</th>
            <th>{t}Title{/t}</th>
            <th>{t}Album{/t}</th>
            <th>{t}Weight{/t}</th>
            <th>{t}Duration{/t}</th>
            <th>{t}Added{/t}</th>
            <th>{t}Count played{/t}</th>
            <!--
            <th>{t}Likes{/t}</th>
            <th>{t}Dislikes{/t}</th>
            -->
          </tr>
        </tfoot>
      </table>
    </section>
  </div>
{/block}
