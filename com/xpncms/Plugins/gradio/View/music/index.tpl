{capture name="page_title"}
  {block name="title" prepend}{$pageTitle} :{/block}
{/capture}
{block name="body" append}
  <div class="content">
    <section class="main-section" id="gradio-index">
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs.tpl"}
    </section>
  </div>          
  <script>
    
    $(function(){
      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var clickedSection = $(this).attr('href');
        
        var gradioBaseUrl = baseURL + 'gradio/';
        var sectionUrl = gradioBaseUrl + clickedSection.substring(1);
        
        //xpnCMS.loadJson(sectionUrl, '.tab-content ' + clickedSection + '', -70);

        //console.debug('newly activated tab', clickedSection);
        //console.debug('previous active tab', e);
          
        //var jQueryPromise = $.ajax('/data.json');
        //var realPromise = Promise.resolve(jQueryPromise);
          
        //e.target // newly activated tab
        //e.relatedTarget // previous active tab
      });
    });
    
  </script>
{/block}
