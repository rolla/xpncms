<ul id="gradio-tunes-tabs" class="nav nav-tabs" role="tablist">
  <li role="presentation" class="{if $activeTab == 1}active{/if}">
    <router-link to="{$url}gradio/recently-played-tunes" class="ajax-nav" aria-controls="recently-played-tunes" role="tab">
      <i class="fa fa-history"></i> {t}Recently played tunes{/t}
    </router-link>
  </li>
  <li role="presentation" class="{if $activeTab == 2}active{/if}">
    <router-link to="{$url}gradio/most-played-tunes" class="ajax-nav" aria-controls="most-played-tunes" role="tab">
      <i class="fa fa-music"></i> {t}Most played tunes{/t}
    </router-link>
  </li>
  <li role="presentation" class="{if $activeTab == 3}active{/if}">
    <router-link to="{$url}gradio/latest-tunes" class="ajax-nav" aria-controls="latest-tunes" role="tab">
      <i class="fa fa-clock-o"></i> {t}Latest tunes{/t}
    </router-link>
  </li>
  <li role="presentation" class="{if $activeTab == 4}active{/if}">
    <router-link to="{$url}gradio/your-likes" class="ajax-nav" aria-controls="your-likes" role="tab">
      <i class="fa fa-thumbs-up"></i> {t}Your likes{/t}
    </router-link>
  </li>
  <li role="presentation" class="{if $activeTab == 5}active{/if}">
    <router-link to="{$url}gradio/your-dislikes" class="ajax-nav" aria-controls="your-dislikes" role="tab">
      <i class="fa fa-thumbs-down"></i> {t}Your dislikes{/t}
    </router-link>
  </li>
  {if ($gradioTeam)}
    <li role="presentation" class="{if $activeTab == 6}active{/if}">
      <router-link to="{$url}gradio/queue" class="ajax-nav" aria-controls="queue" role="tab">
        <i class="fa fa-list-ol"></i> {t}Queued tunes{/t}
      </router-link>
    </li>
    <li role="presentation" class="{if $activeTab == 7}active{/if}">
      <router-link to="{$url}gradio/check" class="ajax-nav" aria-controls="check" role="tab">
        <i class="fa fa-check-circle"></i> {t}Check tunes{/t}
      </router-link>
    </li>
  {/if}
</ul>
