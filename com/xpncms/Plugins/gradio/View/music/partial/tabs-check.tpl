      <div class="container">
        <!-- Nav tabs -->
        <ul id="tabs-check" class="nav nav-tabs" role="tablist">
          {if ($gradioTeam)}
            <li role="presentation" class="{if $activeTab == 1}active{/if}">
              <a href="{$url}gradio/check-likes" class="ajax-nav" aria-controls="check-likes" role="tab" data-toggle="tabb">
                <i class="fa fa-check-circle"></i> {t}Check likes{/t}
              </a>
            </li>
            <li role="presentation" class="{if $activeTab == 2}active{/if}">
              <a href="{$url}gradio/check-dislikes" class="ajax-nav" aria-controls="check-dislikes" role="tab" data-toggle="tabb">
                <i class="fa fa-check-circle-o"></i> {t}Check dislikes{/t}
              </a>
            </li>
          {/if}
        </ul>
      </div>
