<div class="card-container">
  <div class="card">
    <div class="front">
      <div class="cover">
        {if ($gradioTeam)}
          <span class="badge btn-warning tune-extension">{$tune.extension}</span>
        {/if}
        <div><img src="{$tune.phpThumbPictureURL}" width="200"></div>
      </div>
      <div class="card-content">
        <div class="main">
          <h3 class="artist"><div>{$tune.artist}</div></h3>
          <h2 class="title"><div>{$tune.title}</div></h2>
          <h4 class="text-left">{t}Date added{/t}: {$tune.date_added|relative_date}</h4>
          <h4 class="text-left">{t}Last played{/t}: {$tune.date_played|relative_date}</h4>
          <h4 class="text-left">{t}Count played{/t}: {$tune.count_played} {if ($gradioTeam && !$tune.enabled)}<span class="badge btn-warning">{t}Disabled{/t}</span>{/if}</h4>
          <h4 class="text-left">
          {t}Genre{/t}:
            <span class="genre-container">
              {foreach $tune.genres as $genre}
              <span class="tune-card-genre badge">
                {$genre.name}
              </span>
              {/foreach}
            </span>
          </h4>
          {if !empty($tune.publisher)}
            <img src="{$url}themes/{$frontendTheme}/img/icons/labels/{$tune.publisher_svg}" class="record-label publisher pull-right" alt="{$tune.publisher}" title="{$tune.publisher}" />
          {/if}

        </div>
      </div>
    </div><!-- end front panel -->

    <div class="back">
      <div class="front">
        <div class="cover-back">
          <div><img src="{$tune.phpThumbPictureURL}" width="500"></div>
        </div>
        <div class="card-content">
          <div class="main">
            <h4 class="text-left">{t}Year{/t}: {$tune.year}</h4>
            <h4 class="text-left">{t}Album{/t}: {$tune.album}</h4>
            <h4 class="text-left">{t}Duration{/t}: {$tune.duration}</h4>
            <h4 class="text-left">{t}Weight{/t}: {$tune.weight}</h4>
            <h4 class="text-left">{t}BPM{/t}: {$tune.bpm}</h4>
            <div class="row">
              <div class="col-xs-3">
                <div
                  class="tune-like likes rate"
                  data-item-id="{$tune.id}"
                  data-action="liked"
                  data-action-category="tune"
                  data-ot="{t}Like this tune{/t}"
                  title="{t}Like this tune{/t}">
                  <i class="fa fa-thumbs-o-up fa-4x"></i> <!--<span class="rating-count">{$tune.likes}</span>-->
                </div>
              </div>
              <div class="col-xs-3">
                <div
                  class="tune-dislike dislikes rate"
                  data-item-id="{$tune.id}"
                  data-action="disliked"
                  data-action-category="tune"
                  data-ot="{t}Dislike this tune{/t}"
                  title="{t}Dislike this tune{/t}">
                  <i class="fa fa-thumbs-o-down fa-4x"></i> <!--<span class="rating-count">{$tune.dislikes}</span>-->
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              {if !empty($tune.youtube) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}Youtube{/t}">
                  <div class="tune-table-title" title="{t}Youtube{/t}"></div>
                  <div class="tune-table-content">
                    <a href="{$tune.youtube}" target="_blank">
                      <img src="{$url}themes/{$template}/img/icons/youtube-logo-opacity-50.svg"/>
                    </a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no youtube link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-content">
                    <img src="{$url}themes/{$template}/img/icons/youtube-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}
              {if !empty($tune.soundcloud) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}Soundcloud{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-content soundcloud-content">
                    <a href="{$tune.soundcloud}" target="_blank"><img src="{$url}themes/{$frontendTheme}/img/icons/soundcloud-logo-opacity-50.svg"/></a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no soundcloud link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img src="{$url}themes/{$frontendTheme}/img/icons/soundcloud-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}

              {if !empty($tune.beatport) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}Beatport{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a href="{$tune.beatport}" target="_blank"><img src="{$url}themes/{$frontendTheme}/img/icons/beatport-logo-opacity-50.svg"/></a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no url link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img src="{$url}themes/{$frontendTheme}/img/icons/beatport-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}
              {if !empty($tune.itunes) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}iTunes{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a href="{$tune.itunes}" target="_blank"><img src="{$url}themes/{$frontendTheme}/img/icons/itunes-logo-opacity-50.svg"/></a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no iTunes link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img src="{$url}themes/{$frontendTheme}/img/icons/itunes-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}
            </div>
            <br>
            <a href="{$url}tune/{$tune.id}" class="btn btn-default btn-block ajax-nav" data-target=".content">{t}See more{/t}</a>
          </div>
        </div>
      </div>
    </div> <!-- end back panel -->

  </div> <!-- end card -->
</div> <!-- end card-container -->
