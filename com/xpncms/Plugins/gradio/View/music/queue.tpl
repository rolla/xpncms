{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="content" append}
<div class="container-fluid">
  <section class="main-section" id="queue-tunes">
    <div class="content">
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs.tpl" activeTab=6}
      <h3>{t}Next tunes{/t} ({$tunes|@count})</h3>
      <div class="clearfix"></div>
      <div class="queue-table">
      {if !empty($tunes) }
        {foreach $tunes as $tune}
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            {include file="{APP_PLUGIN}gradio/View/music/partial/tune-card.tpl" tune=$tune}
          </div>
        {/foreach}
      {/if}
      </div>
    </div>
  </section>
</div>
{/block}
