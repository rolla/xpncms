{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="content" append}
<div class="container-fluid">
  <section class="main-section" id="queue-tunes">
    <div class="content">
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs.tpl" activeTab=7}
      {include file="{APP_PLUGIN}gradio/View/music/partial/tabs-check.tpl" activeTab=1}
      <h3>{t}Check tunes{/t}</h3>
      {if !count($userData)}
        <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-warning">
              {t}Access allowed only To Gradio Team!{/t}
            </div>
        </div>
      {/if}
    </div>
  </section>
</div>
{/block}
