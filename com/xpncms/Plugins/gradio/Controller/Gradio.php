<?php

namespace XpnCMS\Plugins\gradio\Controller;

use XpnCMS\Controller\Auth;
use XpnCMS\Plugins\Plugins;
use XpnCMS\Model\User;
use stdClass;
use JasonGrimes\Paginator;
use Symfony\Component\HttpFoundation\Request;

class Gradio extends Plugins
{
    private $_maintenance = false;

    public function __construct()
    {
        global $uri, $rbac;
        //parent::__construct();

        // Get Role Id
        $roleId = $rbac->Roles->returnId('gradio');
        // Make sure User has 'forum_user' Role
        $allowed = 0;
        if (User::authId()) {
            $allowed = $rbac->Users->hasRole($roleId, User::authId());
        }

        $allowedMethods = [
            'index',
            'now',
            'rate',
            'currentshow',
            'likesdislikes',
            'recent',
            'likes',
            'm3u',
            'mp3',
            'aacp',
            'recently-played-tunes',
            'most-played-tunes',
            'latest-tunes',
            'your-likes',
            'your-dislikes',
            'check',
            'check-likes',
            'check-dislikes',
        ];

        if ($allowed) {
          return true;
        }

        if (!in_array($uri['method'], $allowedMethods)) {
            Auth::notAllowed();
        }
    }

    private function maintenance()
    {
        global $smarty;

        $var[] = 'Gradio Ratings';
        $notifyRepository = $this->loadModel('notify');
        $notify = $notifyRepository->getCode(402, $var);
        $smarty->clearAssign('notify');
        $result['notify'] = $notify;
        $json['rate'] = $result;

        echo json_encode($json);
    }


    public function index()
    {
        $this->recentlyPlayedTunes();
    }

    public function label()
    {
        $music = $this->loadPluginModel('gradio');
        $pageTitle = _('Recently played tunes');
        $tunes = $music->getRecentsongs(10);

        $this->loadPluginView(
            'gradio/music/label',
            compact('userData', 'tunes', 'pageTitle')
        );
    }

    public function recentlyPlayedTunes()
    {
        $request = Request::createFromGlobals();

        $page = intval($request->get('page', 1));

        $gradioRepo = $this->loadPluginModel('gradio');

        $totalItems = $gradioRepo->getAllRecentSongsCount();
        $urlPattern = '/gradio/recently-played-tunes?page=(:num)';

        $paginator = new Paginator($totalItems, $gradioRepo->perPage, $page, $urlPattern);

        $startAt = $gradioRepo->perPage * ($page - 1);

        $pageTitle = _('Recently played tunes');
        $tunes = $gradioRepo->getRecentsongs($startAt, $gradioRepo->perPage);

        $this->loadPluginView(
            'gradio/music/recent',
            compact('userData', 'tunes', 'pageTitle', 'paginator')
        );
    }

    public function mostPlayedTunes()
    {
        $request = Request::createFromGlobals();

        $page = intval($request->get('page', 1));

        $gradioRepo = $this->loadPluginModel('gradio');

        $totalItems = $gradioRepo->getAllMostSongsCount();
        $urlPattern = '/gradio/most-played-tunes?page=(:num)';

        $paginator = new Paginator($totalItems, $gradioRepo->perPage, $page, $urlPattern);

        $startAt = $gradioRepo->perPage * ($page - 1);

        $pageTitle = _('Most played tunes');
        $tunes = $gradioRepo->getMostPlayedTunes($startAt, $gradioRepo->perPage);

        $this->loadPluginView(
            'gradio/music/most',
            compact('userData', 'tunes', 'pageTitle', 'paginator')
        );
    }

    public function latestTunes()
    {
        $request = Request::createFromGlobals();

        $page = intval($request->get('page', 1));

        $gradioRepo = $this->loadPluginModel('gradio');

        $totalItems = $gradioRepo->getAllLatestSongsCount();
        $urlPattern = '/gradio/latest-tunes?page=(:num)';

        $paginator = new Paginator($totalItems, $gradioRepo->perPage, $page, $urlPattern);

        $startAt = $gradioRepo->perPage * ($page - 1);
        $pageTitle = _('Latest tunes');
        $tunes = $gradioRepo->getLatestTunes($startAt, $gradioRepo->perPage);

        $this->loadPluginView(
            'gradio/music/latest',
            compact('userData', 'tunes', 'pageTitle', 'paginator')
        );
    }

    public function yourLikes()
    {
        $request = Request::createFromGlobals();

        $page = intval($request->get('page', 1));

        $gradioRepo = $this->loadPluginModel('gradio');
        $notifyObj = $this->loadModel('notify');

        $userData = null;
        $tunes = [];
        $notify = null;

        $currentUserId = User::authId();
        if ($currentUserId) {
            $userRepository = $this->loadModel('user');
            $userData = $userRepository->get($currentUserId);

            $args = new stdClass;
            $args->userId = $userData['id'];
            $args->actionId = 1; // like
            $args->categoryId = 1; // tune

            $totalItems = $gradioRepo->getAllUserTuneRatingsCount($args);
            $urlPattern = '/gradio/your-likes?page=(:num)';

            $paginator = new Paginator($totalItems, $gradioRepo->perPage, $page, $urlPattern);

            $startAt = $gradioRepo->perPage * ($page - 1);
            $args->offset = $startAt;
            $args->limit = $gradioRepo->perPage;

            $tunes = $gradioRepo->getUserTuneRatings($args);
        } else {
            $notify = $notifyObj->getCode(10);
        }

        $pageTitle = _('Your likes');

        $this->loadPluginView(
            'gradio/music/yourlikes',
            compact('userData', 'tunes', 'pageTitle', 'notify')
        );
    }

    public function yourDislikes()
    {
        $request = Request::createFromGlobals();

        $page = intval($request->get('page', 1));

        $gradioRepo = $this->loadPluginModel('gradio');
        $notifyObj = $this->loadModel('notify');

        $userData = null;
        $tunes = [];
        $notify = null;

        $currentUserId = User::authId();
        if ($currentUserId) {
            $userRepository = $this->loadModel('user');
            $userData = $userRepository->get($currentUserId);

            $args = new stdClass;
            $args->userId = $userData['id'];
            $args->actionId = 2; // dislike
            $args->categoryId = 1; // tune

            $totalItems = $gradioRepo->getAllUserTuneRatingsCount($args);
            $urlPattern = '/gradio/your-likes?page=(:num)';

            $paginator = new Paginator($totalItems, $gradioRepo->perPage, $page, $urlPattern);

            $startAt = $gradioRepo->perPage * ($page - 1);
            $args->offset = $startAt;
            $args->limit = $gradioRepo->perPage;

            $tunes = $gradioRepo->getUserTuneRatings($args);
        } else {
            $notify = $notifyObj->getCode(10);
        }

        $pageTitle = _('Your dislikes');

        $this->loadPluginView(
            'gradio/music/yourdislikes',
            compact('userData', 'tunes', 'pageTitle', 'notify')
        );
    }

    public function check()
    {
        $currentUserId = User::authId();
        $userRepository = $this->loadModel('user');

        $userData = null;
        $tunes = [];

        if ($currentUserId) {
            $userData = $userRepository->get($currentUserId);
        }

        $pageTitle = _('Check');

        $this->loadPluginView(
            'gradio/music/check',
            compact('userData', 'tunes', 'pageTitle')
        );
    }

    public function checkLikes()
    {
        $gradioRepository = $this->loadPluginModel('gradio');
        $currentUserId = User::authId();
        $userRepository = $this->loadModel('user');

        $userData = null;
        $tunes = [];

        if ($currentUserId) {
            $userData = $userRepository->get($currentUserId);
            $args = new stdClass;
            $args->actionId = 1; // like
            $args->categoryId = 1; // tune
            $args->enabled = [0]; // disabled
            $args->offset = 0;
            $args->limit = 200;
            $tunes = $gradioRepository->getAllTuneRatings($args);
        }

        $pageTitle = _('Check likes');

        $this->loadPluginView(
            'gradio/music/checklikes',
            compact('userData', 'tunes', 'pageTitle')
        );
    }

    public function checkDislikes()
    {
        $gradioRepository = $this->loadPluginModel('gradio');
        $currentUserId = User::authId();
        $userRepository = $this->loadModel('user');

        $userData = null;
        $tunes = [];

        if ($currentUserId) {
            $userData = $userRepository->get($currentUserId);
            $args = new stdClass;
            $args->actionId = 2; // dislike
            $args->categoryId = 1; // tune
            $args->enabled = [1]; // enabled
            $args->offset = 0;
            $args->limit = 200;
            $tunes = $gradioRepository->getAllTuneRatings($args);
        }

        $pageTitle = _('Check dislikes');

        $this->loadPluginView(
            'gradio/music/checkdislikes',
            compact('userData', 'tunes', 'pageTitle')
        );
    }

    public function now()
    {
        $now = $this->loadPluginModel('gradio');
        $json = $now->getNow();
        header('Content-type: application/json');
        header('Cache-Control: no-cache, must-revalidate');
        echo json_encode($json);
    }

    public function rate()
    {
        global $smarty, $action;

        //$addHumanAction = $action->addHumanAction('liked', 'tune', 0, null);
        //print_r($addHumanAction);
        //exit;

        if ($this->_maintenance) {
            return $this->maintenance();
        }

        //$request_body = readfile('php://input');
        $request_body = file_get_contents('php://input');

        $master_pieces = explode('userfile=----AJAX----', $request_body);
        $master_pieces2 = explode('--', $master_pieces[1]);

        $hash = $master_pieces2[0];

        //print_r($master_pieces2);

        $request_body = urldecode($request_body);

        //$request_body = htmlentities($request_body);

        $pieces = explode($hash.'GRadio', $request_body);

        $cnt = count($pieces);

        unset($pieces[0]);
        unset($pieces[$cnt - 1]);

        foreach ($pieces as $key => $value) {
            $s_data[] = explode('=', str_replace(';', '', $value));
        }

        foreach ($s_data as $key => $value) {
            foreach ($value as $k => $v) {
                if ($k == 0) {
                    $r_data[$v];
                } else {
                    $r_data[$s_data[$key][0]] = $v;
                }
            }
        }

        //$result['error'] = '【G】Whoops!';
        //$result['error_msg'] = 'Only logged users can vote!';

        $notify = $this->loadModel('notify');

        if (User::authId()) {
            $rate = $this->loadPluginModel('gradio');
            $rating = $rate->postRate($r_data['songid'], User::authId(), $r_data['rating']);
            $json['rating'] = $rating;

            /*
            if($rating == 'like'){
                $action_rating = 'liked';
            }else if($rating == 'dislike'){
                $action = 2;
            }
            */

            if ($rating['ok']) {
                $notify = $notify->getCode(201);
                $smarty->clearAssign('notify');
                $result['notify'] = $notify;
                $json['rate'] = $result;

                $addHumanAction = $action->addHumanAction($r_data['rating'].'d', 'tune', $r_data['songid'], null);
                //print_r($addHumanAction);

                $json['addHumanAction'] = $addHumanAction;
                $json['song'] = $rate->getSongByID($r_data['songid']);
            } elseif ($rating['update']) {
                $notify = $notify->getCode(301);
                $smarty->clearAssign('notify');
                $result['notify'] = $notify;
                $json['rate'] = $result;

                $addHumanAction = $action->addHumanAction($r_data['rating'].'d', 'tune', $r_data['songid'], null);

                $json['addHumanAction'] = $addHumanAction;
                $json['song'] = $rate->getSongByID($r_data['songid']);

                //print_r($addHumanAction);
            } elseif ($rating['rated']) {
                $notify = $notify->getCode(401);
                $smarty->clearAssign('notify');
                $result['notify'] = $notify;
                $json['rate'] = $result;
                $json['song'] = $rate->getSongByID($r_data['songid']);
            }

            //print_r($rating);

            $json['rate'] = $result;
        } else {
            $notify = $notify->getCode(14);
            $smarty->clearAssign('notify');
            $result['notify'] = $notify;
            $json['rate'] = $result;
            //header("Content-type: application/json");
            //header("Cache-Control: no-cache, must-revalidate");
        }

        echo json_encode($json);
    }

    public function currentshow()
    {
        $valid_passwords = ['admin' => 'Universal9550'];
        $valid_users = array_keys($valid_passwords);

        $user = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];

        $validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

        if (!$validated) {
            header('WWW-Authenticate: Basic realm="Gradio Realm"');
            header('HTTP/1.0 401 Unauthorized');
            die('Not authorized');
        }

        // If arrives here, is a valid user.
        echo "<p>Welcome $user.</p>";
        //echo "<p>Congratulation, you are into the system.</p>";

        if (isset($_GET['showID'], $_GET['title'], $_GET['time'])) {
            if (!empty($_GET['showID']) || $_GET['showID'] == 0 && !empty($_GET['title']) && !empty($_GET['time'])) {
                //echo 'ir';

                $data['showID'] = $_GET['showID'];
                $data['title'] = $_GET['title'];
                $data['time'] = $_GET['time'];

                if (!isset($_GET['artist'])) {
                    $data['artist'] = '';
                } else {
                    $data['artist'] = $_GET['artist'];
                }

                $now = $this->loadPluginModel('gradio');

                //print_r($data);

                $now->SetCurrentShow($data);
            } else {
                echo 'check parameters!';
            }
        } else {
            echo 'error setting current show!';
        }
    }

    public function songs()
    {
        global $rbac;

        $user = $this->loadModel('user');
        // get the user data from database
        $userData = $user->get(User::authId());
        $music = $this->loadPluginModel('gradio');

        // load profile view
        $data = [
            'pageTitle' => _('Tunes'),
            'userData' => $userData
        ];
        $this->loadPluginView('gradio/music/songs', $data);
    }

    public function latesttracks()
    {
        global $rbac;

        $music = $this->loadPluginModel('gradio');
        // load profile view
        $data = ['title' => 'Latest tracks'];
        $this->loadPluginView('gradio/music/latesttracks', $data);
    }

    public function latestnow()
    {
        global $rbac;

        $music = $this->loadPluginModel('gradio');
        // load profile view
        //$data = array("pageTitle" => "Songs", 'userData' => $userData);
        $this->loadPluginView('gradio/music/latestnow');
    }

    public function latestratings()
    {
        global $rbac;

        $latestratings = $this->loadPluginModel('gradio');
        //$getlatestratings = $latestratings->getLatestRatings();
        // load profile view
        $data = ['pageTitle' => 'LatestRatings', 'ratings' => $latestratings->getLatestRatings()];
        $this->loadPluginView('gradio/music/latestratings', $data);
    }

    public function datasongs()
    {
        global $rbac;

        $user = $this->loadModel('user');
        $notify = $this->loadModel('notify');
        // get the user data from database
        $userData = $user->get(User::authId());
        //$user->currentVisit($_SESSION["user"]);
        //$user->userVisits();

        $music = $this->loadPluginModel('gradio');
        // load profile view
        //$data = array("pageTitle" => "Songs", 'userData' => $userData, "songs" => $music->getSongsNew());
        //$this->loadPluginView("gradio/music/songs", $data);

        $getSongsNew = $music->getSongsNew();
        //print_r($getSongsNew);

        if ($getSongsNew['error']) {
            $notify = $notify->getCode(101);
            //$smarty->clearAssign('notify');
            $result['notify'] = $notify;
            $json['songs'] = $result;
        } else {
            $json = $getSongsNew;
        }

        echo json_encode($json);
    }

    public function datasongslatest()
    {
        global $rbac, $debug;

        $music = $this->loadPluginModel('gradio');
        // load profile view
        //$data = array("pageTitle" => "Songs", 'userData' => $userData, "songs" => $music->getSongsNew());
        //$this->loadPluginView("gradio/music/songs", $data);

        $getSongsLatest = $music->getSongsLatest();

        //print_r($getSongsNew);

        if ($getSongsLatest['error']) {
            $notify = $notify->getCode(101);
            //$smarty->clearAssign('notify');
            $result['notify'] = $notify;
            $json['songs'] = $result;
        } else {
            $json = $getSongsLatest;
        }

        //$debug->Display($json);
        //exit();

        echo json_encode($json);
    }

    // deprecated
    public function recent()
    {
        return $this->recentlyPlayedTunes();
    }

    public function queue()
    {
        $user = $this->loadModel('user');
        $userData = $user->get(User::authId());

        $music = $this->loadPluginModel('gradio');
        // load profile view
        $data = ['pageTitle' => _('Queued tunes'), 'userData' => $userData, 'tunes' => $music->getQueuesongs()];
        $this->loadPluginView('gradio/music/queue', $data);
    }

    protected function download($fileName)
    {
        // make sure it's a file before doing anything!
        if (is_file($fileName)) {
            /*
                Do any processing you'd like here:
                1.  Increment a counter
                2.  Do something with the DB
                3.  Check user permissions
                4.  Anything you want!
            */

            // required for IE
            if (ini_get('zlib.output_compression')) {
                ini_set('zlib.output_compression', 'Off');
            }

            // get the file mime type using the file extension
            switch (strtolower(substr(strrchr($fileName, '.'), 1))) {
                case 'pdf': $mime = 'application/pdf'; break;
                case 'zip': $mime = 'application/zip'; break;
                case 'jpeg':
                case 'jpg': $mime = 'image/jpg'; break;
                default: $mime = 'application/force-download';
            }
            header('Pragma: public');   // required
            header('Expires: 0');       // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($fileName)).' GMT');
            header('Cache-Control: private', false);
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($fileName));    // provide file size
            header('Connection: close');
            readfile($fileName);       // push it out
            exit();
        }
    }

    public function m3u()
    {
        // grab the requested file's name
        $fileName = APP_ROOT . 'public' . DS . 'files' . DS . 'gradio' . DS . 'gradio.lv.m3u';

        return $this->download($fileName);
    }

    public function mp3()
    {
        // grab the requested file's name
        $fileName = APP_ROOT . 'public' . DS . 'files' . DS . 'gradio' . DS . 'mp3.gradio.lv.m3u';

        return $this->download($fileName);
    }

    public function aacp()
    {
        // grab the requested file's name
        $fileName = APP_ROOT . 'public' . DS . 'files' . DS . 'gradio' . DS . 'aacp.gradio.lv.m3u';

        return $this->download($fileName);
    }
}
