<?php

namespace XpnCMS\Plugins\gradio\Model;

use SplFileInfo;
use DateTime;
use XpnCMS\Model\Model;
use Core\Includes\Config;
use Core\Helpers\Strings;
use Core\Helpers\Time;
use XpnCMS\Model\User;
use Core\Includes\Database;
use Underscore\Types\Arrays;
use XpnCMS\Plugins\tune\Model\Tune;

/* TODO

  protected $_database more logical

 */

class Gradio extends Model
{
    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
    protected $cacheHash = 20160526151411; // hash for phpThumb
    //private $relativeUploaded = 'uploaded/'; // relative from coversMnt

    //protected $_aColumns = array( 'id', 'artist', 'title', 'weight', 'duration', 'date_added', 'count_played' );
    protected $_aColumns = null;
    /* Indexed column (used for fast and accurate table cardinality) */
    protected $_sIndexColumn = 'id';

    /* DB table to use */
    protected $_sTable = 'songs';
    protected $_database = false;
    protected $baseQuery = '';
    public $perPage = 24;

    public function __construct()
    {
        global $db1, $db2;
        $this->_database = $db2;
        $this->cacheHash = Config::get('site.phpThumbCacheHash');

        $this->baseQuery = "select si.remix, si.label, si.url, si.beatport, si.soundcloud, si.youtube, si.itunes, si.cover, si.added_by, si.user_id, si.updated_at as si_updated_at, s.*, ua.ampache_id "
                . "from songs s "
                . "left join songs_information si on s.ID = si.song_id "
                . "left join " . Config::get('databases.utils.name') . ".ampache ua on s.ID = ua.song_id ";
    }

    protected function getPlayDb()
    {
        if (Config::get('databases.play.host')) {
            $playDb = new Database;

            $playDb->db_host = Config::get('databases.play.host');
            $playDb->db_port = Config::get('databases.play.port', 3306);
            $playDb->db_user = Config::get('databases.play.user');
            $playDb->db_pass = Config::get('databases.play.password');
            $playDb->db_name = Config::get('databases.play.name');
            $playDb->db_char = Config::get('databases.play.charset');
            $playDb->db_prefix = Config::get('databases.play.prefix');
            $playDb->connect();

            return $playDb;
        }

        return;
    }

    protected function getGenres($ids)
    {
      $genres = [];

      if (!count($ids)) {
          return $genres;
      }

      $playDb = $this->getPlayDb();
      $playDb->query('select tm.object_id, t.id, t.name from tag_map tm '
           . 'left join tag t on tm.tag_id = t.id '
           . 'where tm.object_id in (' . implode(',', $ids) . ')');

      $genresData = $playDb->resultset();

      $genresGroup = Arrays::group($genresData, 'object_id');

      return $genresGroup;
    }

    protected function getRatings($ids)
    {
        global $db1;
        // select item_id, user_id, action_id, created_at, updated_at from gradio_ratings where category_id=1 and item_id in(9789, 6348) and deleted_at is null group by user_id, item_id

        $ratings = [];

        if (!count($ids)) {
            return $ratings;
        }

        $db1->query('select item_id, user_id, action_id, created_at, updated_at '
                . 'from ' . $db1->db_prefix . 'ratings '
                . 'where category_id=:category_id and '
                . 'item_id in (' . implode(',', $ids) . ') and '
                . 'deleted_at is null group by user_id, item_id');

        $db1->bind(':category_id', 1);
        $ratingsData = $db1->resultset();

        $ratingsGroup = Arrays::group($ratingsData, 'item_id');

        foreach ($ratingsGroup as $key => $value) {
            $ratings[$key]['likes'] = Arrays::filter($value, function($value) {
                return $value['action_id'] == 1;
            });

            $ratings[$key]['dislikes'] = Arrays::filter($value, function($value) {
                return $value['action_id'] == 2;
            });

            $ratings[$key]['users'] = Arrays::group($value, 'user_id');
        }

        /* Reset array keys to start from zero */
        if (isset($ratings[$key]['likes'])) {
            $ratings[$key]['likes'] = array_values($ratings[$key]['likes']);
        }

        if (isset($ratings[$key]['dislikes'])) {
            $ratings[$key]['dislikes'] = array_values($ratings[$key]['dislikes']);
        }

        return $ratings;
    }

    protected function getRaters($ids)
    {
        global $db1;

        $raters = [];
        $db1->query('select id, username, first_name, last_name, display_name, description, photo_url '
                . 'from ' . $db1->db_prefix . 'users '
                . 'where id in (' . implode(',', $ids) . ') ');

        $usersData = $db1->resultset();

        $raters = $usersData;

        return $raters;
    }

    protected function addUserToRatings($ratings)
    {
        foreach ($ratings['likes'] as $likeKey => $like) {
            $ratings['likes'][$likeKey]['user'] = last($ratings['raters'][$like['user_id']]);
        }

        foreach ($ratings['dislikes'] as $dislikeKey => $dislike) {
            $ratings['dislikes'][$dislikeKey]['user'] = last($ratings['raters'][$dislike['user_id']]);
        }

        return $ratings;
    }

    protected function getExtendedData($tunes)
    {
        $ids = array_column($tunes, 'ID');
        $ratings = $this->getRatings($ids);

        // fix if empty ampache_id
        foreach ($tunes as $key => $tune) {
            if (empty($tune['ampache_id'])) {
                $tunes[$key]['ampache_id'] = 0;
            }
        }

        $ampacheIds = array_column($tunes, 'ampache_id');

        $genres = $this->getGenres($ampacheIds);

        foreach ($tunes as $key => $tune) {
            $tunes[$key]['genres'] = [];
            if (array_key_exists($tune['ampache_id'], $genres)) {
                $tunes[$key]['genres'] = $genres[$tune['ampache_id']];
            }

            $tunes[$key]['ratings']['likes'] = [];
            $tunes[$key]['ratings']['dislikes'] = [];
            if (array_key_exists($tune['ID'], $ratings)) {
                $userIds = array_keys($ratings[$tune['ID']]['users']);
                $raters = Arrays::group($this->getRaters($userIds), 'id');

                $tunes[$key]['ratings'] = $ratings[$tune['ID']];
                $tunes[$key]['ratings']['raters'] = $raters;

                $ratingsWithUser = $this->addUserToRatings($tunes[$key]['ratings']);

                $tunes[$key]['ratings']['likes'] = $ratingsWithUser['likes'];
                $tunes[$key]['ratings']['dislikes'] = $ratingsWithUser['dislikes'];

                unset($tunes[$key]['ratings']['users']);
                unset($tunes[$key]['ratings']['raters']);
            }
        }

        return $tunes;
    }

    public function getSongByID($id)
    {
        global $db2;

        $baseQuery = $this->baseQuery;
        $baseQuery .= 'where s.ID=:id';
        $db2->query($baseQuery);
        $db2->bind(':id', $id);
        $tunes = $this->getExtendedData($db2->resultset());

        return $this->formatTunes($tunes)[0];
    }

    public function getRecentsongs($start = 0, $limit = 100)
    {
        global $db2;

        $baseQuery = $this->baseQuery;
        $baseQuery .= 'where s.enabled IN (' . implode(',', [0,1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'order by s.date_played desc ';
        $baseQuery .= 'limit :start, :limit';

        $db2->query($baseQuery);

        $db2->bind(':start', $start);
        $db2->bind(':limit', $limit);

        $tunes = $this->getExtendedData($db2->resultset());

        return $this->formatTunes($tunes);
    }

    public function getAllRecentSongsCount()
    {
        global $db2;

        $baseQuery = "select count(s.ID) from songs s ";
        $baseQuery .= 'where s.enabled IN (' . implode(',', [0,1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'order by s.date_played desc ';
        $db2->query($baseQuery);

        return last($db2->single());
    }

    public function getAllMostSongsCount()
    {
        global $db2;

        $baseQuery = "select count(s.ID) from songs s ";
        $baseQuery .= 'where s.enabled IN (' . implode(',', [1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'order by s.count_played desc ';
        $db2->query($baseQuery);

        return last($db2->single());
    }

    public function getAllLatestSongsCount()
    {
        global $db2;

        $baseQuery = "select count(s.ID) from songs s ";
        $baseQuery .= 'where s.enabled IN (' . implode(',', [1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'order by s.date_added desc ';
        $db2->query($baseQuery);

        return last($db2->single());
    }

    public function getMostPlayedTunes($start = 0, $limit = 100)
    {
        global $db2;

        $baseQuery = $this->baseQuery;
        $baseQuery .= 'where s.enabled IN (' . implode(',', [1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'and year(s.date_added) = '. date('Y') . ' ';
        $baseQuery .= 'order by s.count_played desc ';
        $baseQuery .= 'limit :start, :limit';

        $db2->query($baseQuery);

        $db2->bind(':start', $start);
        $db2->bind(':limit', $limit);

        $tunes = $this->getExtendedData($db2->resultset());

        return $this->formatTunes($tunes);
    }

    public function getLatestTunes($start = 0, $limit = 100, $random = false)
    {
        global $db2;

        $baseQuery = $this->baseQuery;
        $baseQuery .= 'where s.enabled IN (' . implode(',', [1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';

        if (!$random) {
            $baseQuery .= 'order by s.date_added desc ';
        }

        if ($random) {
            // $subMonths = 24;
            $baseQuery .= 'order by rand() ';
        }

        $baseQuery .= 'limit :start, :limit';

        $db2->query($baseQuery);

        $db2->bind(':start', $start);
        $db2->bind(':limit', $limit);

        $latestTunes = $db2->resultset();

        $tunes = $this->getExtendedData($latestTunes);

        return $this->formatTunes($tunes);
    }

    public function getLatestVideos($limit, $random = false)
    {
        global $db2;

        $baseQuery = $this->baseQuery;
        $baseQuery .= 'where s.enabled IN (' . implode(',', [1]) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'and (si.youtube <> "" or si.youtube is not null) ';
        //$baseQuery .= 'and s.date_added > :date_added ';

        //$subMonths = 1;
        if (!$random) {
            $baseQuery .= ' order by si.updated_at desc, si.created_at desc, s.updated_at desc, s.date_added desc limit :limit';
        }

        if ($random) {
            //$subMonths = 24;
            $baseQuery .= ' order by rand() limit :limit';
        }

        //exit(Carbon::now()->subMonths($subMonths)->toDateTimeString());

        $db2->query($baseQuery);

        $db2->bind(':limit', $limit);
        //$db2->bind(':date_added', Carbon::now()->subMonths($subMonths)->toDateTimeString());
        $tunes = $this->getExtendedData($db2->resultset());

        return $this->formatTunes($tunes);
    }

    public function getPowerVideo($random = true)
    {
        global $db2;

        $utilsMusicSearchesTable = Config::get('databases.utils.name') . ".music_searches ms";

        $baseQuery = "select * from $utilsMusicSearchesTable ";
        $baseQuery .= 'where (ms.youtube <> "" or ms.youtube is not null) ';

        if (!$random) {
            $baseQuery .= 'and year(ms.created_at) = '. date('Y') . ' ';
            $baseQuery .= ' order by ms.updated_at desc, ms.created_at desc';
        }

        if ($random) {
            /* change to current year every 5 seconds */
            if (date('s') % 5 === 0) {
                $baseQuery .= 'and year(ms.created_at) = '. date('Y') . ' ';
            }

            $baseQuery .= ' order by rand()';
        }

        $db2->query($baseQuery);
        $powerVideo = $db2->single();

        if ($powerVideo) {
            $stringsHelper = new Strings();
            $powerVideo['youtube_id'] = $stringsHelper->getYoutubeId($powerVideo['youtube']);
        }

        return $powerVideo;
    }

    public function getAllUserTuneRatingsCount($args)
    {
        global $db1;

        $db1->query('select count(item_id) from ' . $db1->db_prefix . 'ratings'
                . ' where action_id=:action_id and user_id=:user_id and category_id=:category_id ');

        $db1->bind(':user_id', $args->userId);
        $db1->bind(':action_id', $args->actionId);
        $db1->bind(':category_id', $args->categoryId);

        return last($db1->single());
    }

    /**
     *
     * @param  integer $start
     * @param  integer $limit
     * @param  boolean $random
     * @return
     */
    public function getUserLikedTunes($start = 0, $limit = 1, $random = false)
    {
        $args = (object)[
            'userId' => User::auth()->id,
            'actionId' => 1,
            'categoryId' => 1,
            'offset' => $start,
            'limit' => $limit
        ];

        return $this->getUserTuneRatings($args);
    }

    /**
     *
     * @param  integer $start
     * @param  integer $limit
     * @param  boolean $random
     * @return
     */
    public function getUserDislikedTunes($start = 0, $limit = 1, $random = false)
    {
        $args = (object)[
            'userId' => User::auth()->id,
            'actionId' => 2,
            'categoryId' => 1,
            'offset' => $start,
            'limit' => $limit
        ];

        return $this->getUserTuneRatings($args);
    }

    public function getUserTuneRatings($args)
    {
        global $db1;

        $db1->query('select '
                . ' item_id as songid, created_at as time from ' . $db1->db_prefix . 'ratings'
                . ' where action_id=:action_id and user_id=:user_id and category_id=:category_id '
                . ' order by created_at desc limit :offset, :limit');

        $db1->bind(':user_id', $args->userId);
        $db1->bind(':action_id', $args->actionId);
        $db1->bind(':category_id', $args->categoryId);
        $db1->bind(':offset', $args->offset);
        $db1->bind(':limit', $args->limit);

        $ids = array_column($db1->resultset(), 'songid');

        return $this->formatTunes($this->getTunesByIds($ids));
    }

    public function getAllTuneRatings($args)
    {
        global $db1;

        $db1->query('select '
                . ' item_id, created_at from ' . $db1->db_prefix . 'ratings'
                . ' where action_id=:action_id and category_id=:category_id '
                . ' order by :order_by :order_dir limit :offset, :limit');

        $db1->bind(':action_id', $args->actionId);
        $db1->bind(':category_id', $args->categoryId);
        $db1->bind(':order_by', 'created_at');
        $db1->bind(':order_dir', 'desc');
        $db1->bind(':offset', $args->offset);
        $db1->bind(':limit', $args->limit);

        $ids = array_column($db1->resultset(), 'item_id');

        //$ids = [1823,1824];

        $tunesByIds = $this->getTunesByIds($ids, $args->enabled);

        $args->ids = array_column($tunesByIds, 'ID');
        $allTuneUserRatings = $this->getAllTuneUserRatings($args);

        foreach ($tunesByIds as $key => $tune) {
            $tunesByIds[$key]['ratings'] = $allTuneUserRatings[$tune['ID']];
        }

        // sort by ratings count
        $rates = [];
        foreach ($tunesByIds as $key => $row) {
            $rates[$key] = count($row['ratings']);
        }
        array_multisort($rates, SORT_DESC, $tunesByIds);

        return $this->formatTunes($tunesByIds);
    }

    public function getAllTuneUserRatings($args)
    {
        global $db1;

        $db1->query('select'
                . ' gr.item_id, gr.user_id, gu.display_name, ga.action, gac.category, gr.created_at from ' . $db1->db_prefix . 'ratings gr'
                . ' left join ' . $db1->db_prefix . 'users gu'
                . ' on gr.user_id = gu.id'
                . ' left join ' . $db1->db_prefix . 'actions ga'
                . ' on gr.action_id = ga.id'
                . ' left join ' . $db1->db_prefix . 'actions_categories gac'
                . ' on gr.category_id = gac.id'
                . ' where item_id in (' . implode(',', $args->ids) . ')'
                . ' and action_id=:action_id and category_id=:category_id'
                . ' group by item_id, user_id'
                . ' order by gr.created_at desc limit :offset, :limit');

        $db1->bind(':action_id', $args->actionId);
        $db1->bind(':category_id', $args->categoryId);
        $db1->bind(':offset', $args->offset);
        $db1->bind(':limit', $args->limit);

        $tunes = $db1->resultset();

        // for sorting by ids input array
        // TODO need optimize
        foreach ($tunes as $key => $tune) {
            $tmpTunes[$tune['item_id']][] = $tune;
        }

        foreach ($args->ids as $key => $id) {
            $tmpIds[$id] = $id;
        }

        $arr2ordered = [];
        foreach (array_keys($tmpIds) as $key) {
            $arr2ordered[$key] = $tmpTunes[$key] ;
        }
        // end for sorting

        return $arr2ordered;
    }

    public function getTunesByIds($ids, $enabled = [0,1])
    {
        global $db2;

        if (!count($ids)) {
            return [];
        }

        $baseQuery = $this->baseQuery;

        $baseQuery .= 'where s.enabled IN (' . implode(',', $enabled) . ') and s.song_type IN (' . implode(',', [0]) . ') ';
        $baseQuery .= 'and s.ID IN (' . implode(',', $ids) . ') ';

        $db2->query($baseQuery);

        $tunes = $this->getExtendedData($db2->resultset());

        if (!count($tunes)) {
            http_response_code(404);
            exit;
        }

        // for sorting by ids input array
        // TODO need optimize

        $tmpTunes = [];
        foreach ($tunes as $key => $tune) {
            $tmpTunes[$tune['ID']] = $tune;
        }

        $tmpIds = [];
        foreach ($ids as $key => $id) {
            if (isset($tmpTunes[$id]) && count($tmpTunes[$id])) { // check if id comes out from results
                $tmpIds[$id] = $id;
            }
        }

        $arr2ordered = [];
        foreach (array_keys($tmpIds) as $key) {
            $arr2ordered[$key] = $tmpTunes[$key] ;
        }
        // end for sorting

        return array_values($arr2ordered); // reindex
    }

    public function formatTunes($tunes)
    {
        if (!count($tunes)) {
            return [];
        }

        $stringsHelper = new Strings();
        $time = new Time;

        foreach ($tunes as $song) {
            $song['id'] = $song['ID'];

            $song['youtube_id'] = $stringsHelper->getYoutubeId($song['youtube']);
            $song['publisher_svg'] = $stringsHelper->getPublisherLogo($song['publisher']);

            $song['hash'] = time();
            $song['durationInSec'] = $song['duration'];
            $duration = $time->secondsToTime(round($song['duration']));

            $song['duration'] = $duration;
            // $song['iso8601Duration'] = CarbonInterval::milliseconds($song['durationInSec'] * 1000)->spec();

            $picture = Config::get('base.url') . Config::get('paths.images') . '/' . Config::get('images.defaultpath');
            $song['resizeCover'] = Config::get('plugins.gradio.prefs.covers') . 'resize/big/not-found.png';
            if (!empty($song['album_art'])) {
                $picture = Config::get('plugins.gradio.prefs.covers') . $song['album_art'];
                $resizeCover = Config::get('plugins.gradio.prefs.covers') . 'resize/big/' . $song['album_art'];
                $song['resizeCover'] = $resizeCover;
            }
            $song['pictureEncodedFullURL'] = urlencode($picture);
            $song['phpThumbPictureURL'] = Config::get('site.phpThumbUrl') . '?q=100&w=700&src=' . $song['pictureEncodedFullURL'] . '&hash=' . $this->cacheHash;

            //$song['tuneCoversURL'] = Config::get('plugins.gradio.prefs.covers');
            //$song['relativeUploaded'] = $this->relativeUploaded;

            if ($song['count_played'] <= 0) {
                $song['date_played'] = '';
            }

            $extension = new SplFileInfo($song['path']);
            $song['extension'] = $extension->getExtension();

            /* Reset array keys to start from zero */
            if (isset($song['ratings']['likes'])) {
                $song['ratings']['likes'] = array_values($song['ratings']['likes']);
            }

            if (isset($song['ratings']['dislikes'])) {
                $song['ratings']['dislikes'] = array_values($song['ratings']['dislikes']);
            }

            $tunesArr[] = $song;
        }

        return $tunesArr;
    }

    public function getQueuesongs()
    {
        global $db2;
        $time = new Time;

        $db2->query('
            SELECT s.*, si.label, si.url, si.beatport, si.soundcloud, si.youtube, si.itunes, si.added_by, si.created_at, si.updated_at FROM ' . $db2->db_prefix . 'queuelist ql
            LEFT JOIN ' . $db2->db_prefix . 'songs s
            ON ql.songID = s.ID
            LEFT JOIN ' . $db2->db_prefix . 'songs_information si
            ON s.ID = si.song_id
            ORDER BY ql.ID ASC LIMIT 0, 200');
        $tunes = $db2->resultset();

        return $this->formatTunes($tunes);
    }

    public function updateWeight($tuneId, $actionName)
    {
        global $db2, $action;
        $result = null;

        if ($actionName == 'liked') {
            $db2->query('update ' . $db2->db_prefix . 'songs set weight = weight+1 where ID = :ID');
            $db2->bind(':ID', $tuneId);
            $result['like'] = $db2->execute();
        }

        if ($actionName == 'disliked') {
            $db2->query('update ' . $db2->db_prefix . 'songs set weight = weight-1 where ID = :ID');
            $db2->bind(':ID', $tuneId);
            $result['dislike'] = $db2->execute();
        }

        return $result;
    }

    public function getTuneBySongInformationId($id)
    {
        global $db2;

        $db2->query('
            SELECT s.*, g.name AS genre, si.label, si.url, si.beatport, si.soundcloud, si.youtube, si.itunes, si.added_by, si.created_at, si.updated_at FROM songs s
            LEFT JOIN ' . $db2->db_prefix . 'genre g
            ON g.id = s.id_genre
            LEFT JOIN ' . $db2->db_prefix . 'songs_information si
            ON s.ID = si.song_id
            WHERE si.id = :id');
        $db2->bind(':id', $id);

        $tunes = $db2->resultset();

        return $this->formatTunes($tunes)[0];
    }

    private function userLiked($song_id)
    {
        global $db1;

        $user_id = User::authId();

        $db1->query('SELECT created_at FROM ' . $db1->db_prefix . "ratings WHERE item_id='$song_id' AND action_id=1 AND category_id=1 AND user_id='$user_id'");
        $liked = $db1->single();
        if ($liked) {
            return $liked['created_at'];
        }

        return false;
    }

    private function userDisliked($song_id)
    {
        global $db1;

        $user_id = User::authId();

        $db1->query('SELECT created_at FROM ' . $db1->db_prefix . "ratings WHERE item_id='$song_id' AND action_id=2 AND category_id=1 AND user_id='$user_id'");
        $disliked = $db1->single();
        if ($disliked) {
            return $disliked['created_at'];
        }

        return false;
    }

    public function getNow()
    {
        global $db1, $db2, $debugbar;

        $baseQuery = $this->baseQuery;
        $baseQuery .= 'where s.song_type IN (' . implode(',', [0]) . ') ';

        $db2->query($baseQuery . " ORDER BY s.date_played DESC LIMIT 0, 2"); // skip jingles

        if (Config::get('site.environment') === 'development') {
            $db2->query($baseQuery . " ORDER BY rand() LIMIT 0, 2");
            $debugbar['messages']->warning("Enabled debug query in gradio plugin model!");
            $debugbar->sendDataInHeaders();
        }

        // fetch
        $songs = $this->getExtendedData($db2->resultset());

        $now_h_song = $this->formatTunes($songs)[0];
        $result = $now_h_song;

        $prev_h_song = $songs[1];

        $songid = $now_h_song['ID'];

        $prev_result = [];

        $result['song_id'] = $songid;
        ($now_h_song['artist']) ? $result['artist'] = $now_h_song['artist'] : $result['artist'] = '';
        ($now_h_song['title']) ? $result['title'] = $now_h_song['title'] : $result['title'] = '';
        ($now_h_song['album']) ? $result['album'] = $now_h_song['album'] : '';
        ($now_h_song['album_art']) ? $result['picture'] = $now_h_song['album_art'] : '';
        $result['year'] = $now_h_song['year'] ?: '';
        $result['weight'] = $now_h_song['weight'] ?: '';
        $result['bpm'] = $now_h_song['bpm'] ?: '';
        $result['count_played'] = $now_h_song['count_played'] ?: '';
        $result['date_played'] = $now_h_song['date_played'] ?: '';

        $result['genre'] = $now_h_song['genre'] ?: '';
        $result['date_added'] = $now_h_song['date_added'] ?: '';

        $prev_result['song_id'] = $prev_h_song['ID'];
        ($prev_h_song['artist']) ? $prev_result['artist'] = $prev_h_song['artist'] : $prev_result['artist'] = '';
        ($prev_h_song['title']) ? $prev_result['title'] = $prev_h_song['title'] : $prev_result['title'] = '';
        ($prev_h_song['album']) ? $prev_result['album'] = $prev_h_song['album'] : '';
        ($prev_h_song['album_art']) ? $prev_result['picture'] = $prev_h_song['album_art'] : '';
        ($prev_h_song['date_played']) ? $prev_result['date_played'] = $prev_h_song['date_played'] : '';

        $dt_played = $now_h_song['date_played'];
        $dt_duration = $now_h_song['duration'];

        $start_time = strtotime($dt_played);
        $curr_time = time();

        //$start_time = new \DateTime($dt_played);
        //$curr_time =  new \DateTime();
        //$offset = $curr_time->diff($start_time);
        //print_r($offset);
        //$time_left = $start_time + (round($dt_duration / 1000) - $curr_time);

        $time_left = $start_time + round($dt_duration) - $curr_time;

        $secs_remain = round($dt_duration) - ($curr_time - $start_time) + 8; // + crossfade seconds
        $secs_played = $curr_time - $start_time;

        $duration_in_sec = round($dt_duration);
        $secs_offset = $duration_in_sec - $secs_remain;

        // parbauda vai nav nulle sekundes savādāk met divison by zero
        if ($duration_in_sec <= 0) {
            //echo 'error';
            $percents_played = 1;
        } else {
            $percents_played = round(($secs_offset / $duration_in_sec) * 100);
        }

        /*
          $dt_played = $array1['date_played'];
          $dt_duration = $array1['duration'];

          $starttime = strtotime($dt_played);
          $curtime = time();
          $timeleft = $starttime+round($dt_duration/1000)-$curtime;

          $secsRemain = round($dt_duration / 1000)-($curtime-$starttime)+1;
         */

        $result['date_played'] = $dt_played;
        $result['duration'] = $dt_duration;
        $result['secs_remaining'] = $secs_remain;
        $result['secs_played'] = $secs_played;
        $result['percents_played'] = $percents_played;

        $date = new DateTime(date('Y-m-d H:i:s'));
        $result['now_time'] = $date->format('Y-m-d H:i:s');

        $now_date = new DateTime($now_h_song['date_played']);
        //$now_date = new \DateTime("2014-05-21 20:10:06");
        $prev_date = new DateTime($prev_h_song['date_played']);
        $interval = $prev_date->diff($now_date);

        $result['time_offset'] = $interval->format('%h:%i:%s');

        $db1->query('SELECT * FROM ' . $db1->db_prefix . "ratings WHERE item_id = '$songid' AND category_id = 1 LIMIT 0, 30 ");
        $ratings = $db2->resultset();

        // $db1->query('SELECT * FROM ' . $db1->db_prefix . 'currshow ORDER BY id DESC LIMIT 0, 2');
        // $curr_show = $db1->resultset();
        //
        // $result['curr_show'] = $curr_show[0];

        if ($songid) {
            $tuneRepo = new Tune;
            $result = $tuneRepo->appendRatings($result, $songid);

            if ($liked = $this->userLiked($songid)) {
                $result['liked'] = $liked;
            }

            if ($disliked = $this->userDisliked($songid)) {
                $result['disliked'] = $disliked;
            }

        } else {
            $result['error'] = 'invalidID';
        }

        unset($result['path']);

        $json['now'] = $result;
        $json['prev'] = $prev_result;

        return $json;
    }

    public function postRate($songId, $vuserid, $rating)
    {
        global $db1;

        $time = date('Y-m-d H:i:s');
        $result = [];

        $rating_type = ['liked', 'disliked'];
        $extrarating = ['new_system' => 'true'];

        if (in_array($rating, $rating_type)) {
            $category_id = 1; // tune

            if ($rating == 'liked') {
                $action_id = 1;
            } elseif ($rating == 'disliked') {
                $action_id = 2;
            }

            //check if exist
            $sql = 'select * from ' . $db1->db_prefix . 'ratings '
                . 'where item_id=' . $songId . ' and user_id=' . $vuserid . ' and category_id=' . $category_id . ' limit 0, 1';
            $db1->query($sql);
            $exists = $db1->single();

            if (empty($exists)) {
                $voted1 = 1; // jo pirmo reizi kad balso bāzē rakstās nulle
                $db1->query('INSERT INTO ' . $db1->db_prefix . 'ratings (user_id, item_id, action_id, category_id, model, ip, cepumi, extrarating, created_at) VALUES(:user_id, :item_id, :action_id, :category_id, :model, :ip, :cepumi, :extrarating, :created_at)'); //INSERTS INITIAL RATING

                $db1->bind(':user_id', $vuserid);
                $db1->bind(':item_id', $songId);
                $db1->bind(':action_id', $action_id);
                $db1->bind(':category_id', $category_id); // tune
                $db1->bind(':model', 'Gradio');
                $db1->bind(':ip', IP);
                $db1->bind(':cepumi', $voted1);
                $db1->bind(':extrarating', serialize($extrarating));
                $db1->bind(':created_at', $time);

                $post = $db1->execute();

                if ($post) {
                    $result = [
                        'ok' => 'add',
                        'ratings' => $this->updateWeight($songId, $rating),
                    ];
                }
            } else {
                $id = $exists['id'];

                if ($exists['user_id'] == $vuserid && $exists['item_id'] == $songId && $exists['action_id'] == $action_id) { // same rating
                    $sql = 'UPDATE ' . $db1->db_prefix . 'ratings SET model=:model, updated_at=:updated_at WHERE id=:id';
                    $db1->query($sql);

                    $db1->bind(':id', $id);
                    $db1->bind(':model', 'Gradio');
                    $db1->bind(':updated_at', $time);

                    $update = $db1->execute();

                    if ($update) {
                        $result = ['rated' => $rating];
                    }
                } elseif ($exists['user_id'] == $vuserid && $exists['item_id'] == $songId) { // change rating
                  //elseif ($exists['user_id'] == $vuserid && $exists['item_id'] == $songId && $exists['ip'] == IP) { // change rating
                    $sql = 'UPDATE ' . $db1->db_prefix . 'ratings SET action_id=:action_id, model=:model, updated_at=:updated_at WHERE id=:id';
                    $db1->query($sql);

                    $db1->bind(':id', $id);
                    $db1->bind(':action_id', $action_id);
                    $db1->bind(':model', 'Gradio');
                    $db1->bind(':updated_at', $time);

                    $update = $db1->execute();

                    if ($update) {
                        $result = [
                            'update' => $rating,
                            'ratings' => $this->updateWeight($songId, $rating),
                        ];
                    }
                }
            }
        } else {
            $result = ['error' => 'reitinga kļūda'];
        }

        return $result;
    }

    // below old methods

    public function getSongsNew()
    {
        global $debug;
        /*
         * DataTables example server-side processing script.
         *
         * Please note that this script is intentionally extremely simply to show how
         * server-side processing can be implemented, and probably shouldn't be used as
         * the basis for a large complex system. It is suitable for simple use cases as
         * for learning.
         *
         * See http://datatables.net/usage/server-side for full details on the server-
         * side processing requirements of DataTables.
         *
         * @license MIT - http://datatables.net/license_mit
         */

        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */

        // DB table to use
        $table = 'songs';

        // Table's primary key
        $primaryKey = 'ID';

        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        /*
          $output['aaData'][$i]['id'] = $disliked['output'][$key]['songid'];
          $output['aaData'][$i]['artist'] = 'not found';
          $output['aaData'][$i]['title'] = 'in db';
          $output['aaData'][$i]['weight'] = '';
          $output['aaData'][$i]['duration'] = '';
          $output['aaData'][$i]['date_added'] = '';
          $output['aaData'][$i]['count_played'] = '';
          $output['aaData'][$i]['likes'] = '';
          $output['aaData'][$i]['dislikes'] = $disliked['output'][$key]['dislikes'];
         */

        $time = new Time;
        $columns = [
            ['db' => 'album_art', 'dt' => 'album_art'],
            ['db' => 'ID',
                'dt' => 'id',
                'formatter' => function ($d, $song) {
                    $tune_url = Config::get('base.url') . 'tune/' . $d . '/';
                    $this->cacheHash = '';
                    $picture
                        = Config::get('base.url')
                        . Config::get('paths.images') . '/'
                        . Config::get('images.defaultpath');
                    if (!empty($song['album_art'])) {
                        $picture = Config::get('plugins.gradio.prefs.covers') . $song['album_art'];
                    }
                    $pictureEncodedFullURL = urlencode($picture);
                    $phpThumbPictureURL = Config::get('site.phpThumbUrl') . '?q=100&w=600&src=' . $pictureEncodedFullURL . '&hash=' . $this->cacheHash;

                    return '<a href="' . $tune_url . '" class="ajax-nav tune-link"><img src="' . $phpThumbPictureURL . '" height="60"><span class="tune-id">' . $d . '</span></a>';
                },
            ],
            ['db' => 'artist', 'dt' => 'artist'],
            ['db' => 'title', 'dt' => 'title'],
            ['db' => 'album', 'dt' => 'album'],
            ['db' => 'weight', 'dt' => 'weight'],
            [
                'db' => 'duration',
                'dt' => 'duration',
                'formatter' => function ($d, $row) {
                    $time = new Time;
                    return $time->secondsToTime(round($d));
                },
            ],
            [
                'db' => 'date_added',
                'dt' => 'date_added',
                'formatter' => function ($d, $row) {
                    //global $time;
                    $time = new Time;
                    return $time->relativeTime($d);
                },
            ],
            [
                'db' => 'enabled',
                'dt' => 'enabled',
            ],
            [
                'db' => 'count_played',
                'dt' => 'count_played',
                'formatter' => function ($d, $row) {
                    $html = $d;
                    if (!$row['enabled']) {
                        $html = $d . ' <span class="badge btn-warning">' . _("Disabled") . '</span>';
                    }

                    return $html;
                },
            ],
            /*
              array(
              'db' => 'likes',
              'dt' => 'likes',
              'formatter' => function ($d, $row) {
              //global $debug;
              //$debug->Display($d, 0, 1);
              },
              ),
              array(
              'db' => 'dislikes',
              'dt' => 'dislikes',
              'formatter' => function ($d, $row) {
              //global $debug;
              //$debug->Display($d, 0, 1);
              },
              ),
              array(
              'db' => 'comments',
              'dt' => 'comments',
              'formatter' => function ($d, $row) {
              //global $debug;
              //$debug->Display($d, 0, 1);
              },
              ),
             *
             */
            //array( 'db' => 'likes',     'dt' => 7 ),
            //array( 'db' => 'dislikes',     'dt' => 8 ),
            //array( 'db' => 'comments',     'dt' => 9 )

            /*
              array(
              'db'        => 'salary',
              'dt'        => 5,
              'formatter' => function( $d, $row ) {
              return '$'.number_format($d);
              }
              )
             */
        ];

        // SQL server connection information
        $sql_details = [
            'host' => Config::get('databases.remote.host'),
            'port' => Config::get('databases.remote.port'),
            'db' => Config::get('databases.remote.name'),
            'user' => Config::get('databases.remote.user'),
            'pass' => Config::get('databases.remote.password'),
        ];

        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */

        require 'SSP.php';

        //$debug->Display($_GET);
        header("Content-type: application/json");

        $array = SSP::complex($_GET, $sql_details, $table, $primaryKey, $columns);

        return $array;
    }

    public function getSongsLatest()
    {
        $this->_aColumns = ['id', 'artist', 'title', 'duration'];
        $time = new Time;
        $output = $this->regularLatestProcess();

        return $output;
    }

    public function getLatestRatings()
    {
        global $db1;

        $db1->query('SELECT r.item_id as songid, r.action_id, r.user_id as vuserid, u.id, u.first_name, u.last_name, r.created_at as time, a.action as rating
                    FROM ' . $db1->db_prefix . 'ratings r
                    LEFT JOIN ' . $db1->db_prefix . 'users u
                        ON u.id = r.user_id
                    LEFT JOIN ' . $db1->db_prefix . 'actions a
                        ON a.id = r.action_id
                    WHERE r.category_id=1
                    ORDER BY r.created_at DESC LIMIT 0, 10'
        );
        $latestratings = $db1->resultset();

        if ($latestratings != null) {
            return $latestratings;
        }
    }

    public function getDataLikes($resIn)
    {
        global $db1;

        $likes = [];

        $db1->query('SELECT * FROM ' . $db1->db_prefix
            . 'ratings WHERE songid IN (' . $resIn . ') AND rating LIKE \'like\' ORDER BY songid DESC');
        $likesArr = $db1->resultset();
        if ($likesArr != null) {
            foreach ($likesArr as $like) {
                $l['songs']['song'][$like['songid']][] = $like;
            }
            foreach ($l['songs']['song'] as $key => $val) {
                $likes[$key]['likes'] = count($val);
            }
        }

        return $likes;
    }

    public function getDataDislikes($resIn)
    {
        global $db1;

        $dislikes = [];

        $db1->query('SELECT * FROM ' . $db1->db_prefix
            . 'ratings WHERE songid IN ('
            . $resIn . ') AND rating LIKE \'dislike\' ORDER BY songid DESC');
        $dislikesArr = $db1->resultset();
        if ($dislikesArr != null) {
            foreach ($dislikesArr as $dislike) {
                $d['songs']['song'][$dislike['songid']][] = $dislike;
            }
            foreach ($d['songs']['song'] as $key => $val) {
                $dislikes[$key]['dislikes'] = count($val);
            }
        }

        return $dislikes;
    }

    public function getDataMostLiked($order, $limitstart, $limitend)
    {
        global $db1;
        //SELECT id, songid, COUNT(rating) as likes FROM `gradio_ratings` WHERE rating LIKE 'like' GROUP BY songid ORDER BY COUNT(rating) DESC LIMIT 0, 500
        $db1->beginTransaction();
        $db1->query('SELECT id, songid, COUNT(rating) as likes FROM '
            . $db1->db_prefix
            . 'ratings WHERE rating LIKE \'like\' GROUP BY songid ORDER BY COUNT(rating) '
            . $order . ' LIMIT ' . $limitstart . ', ' . $limitend . '');
        $likesArr = $db1->resultset();

        $db1->query('SELECT COUNT(*) FROM (SELECT id FROM '
            . $db1->db_prefix . 'gradio_ratings_old WHERE rating LIKE \'like\' GROUP BY songid) as count');
        $iFilteredTotal = $db1->resultnum();

        $db1->query('SELECT COUNT(id) as id FROM ' . $db1->db_prefix . 'ratings');
        $aResultTotal = $db1->resultnum();
        $iTotal = $aResultTotal;
        $db1->endTransaction();

        if ($likesArr != null) {
            foreach ($likesArr as $like) {
                //$likes['songs']['song'][$like['songid']] = $like;
                $likes[$like['songid']] = $like;
            }

            /*
              foreach($l['songs']['song'] as $key => $val){
              $likes[$key]['likes'] = count($val);
              }
             */
        } else {
            $likes = [];
        }

        $output = $likes;

        $arr_c = count($output);
        $resIn = false;
        if (!empty($arr_c)) {
            $i = 0;
            foreach ($output as $key => $val) {
                if ($arr_c == $i + 1) {
                    $sep = '';
                } else {
                    $sep = ', ';
                }

                $resIn .= $key . $sep;
                ++$i;
            }
        } else {
            $resIn = 'error';
        }

        $likes['resIn'] = $resIn;
        $likes['output'] = $output;
        $likes['iFilteredTotal'] = $iFilteredTotal;
        $likes['iTotal'] = $iTotal;

        return $likes;
    }

    public function getDataMostDisliked($order, $limitstart, $limitend)
    {
        global $db1;

        //SELECT id, songid, COUNT(rating) as likes FROM `gradio_ratings` WHERE rating LIKE 'like' GROUP BY songid ORDER BY COUNT(rating) DESC LIMIT 0, 500
        $db1->beginTransaction();
        $db1->query('SELECT id, songid, COUNT(rating) as dislikes FROM '
            . $db1->db_prefix . 'gradio_ratings_old WHERE rating LIKE \'dislike\' GROUP BY songid ORDER BY COUNT(rating) '
            . $order . ' LIMIT ' . $limitstart . ', ' . $limitend . '');
        $dislikesArr = $db1->resultset();

        /*
          SELECT COUNT(*) FROM
          (SELECT id FROM gradio_ratings WHERE rating LIKE 'dislike' GROUP BY songid)
          as count
         */

        $db1->query('SELECT COUNT(*) FROM (SELECT id FROM '
            . $db1->db_prefix . 'ratings WHERE rating LIKE \'dislike\' GROUP BY songid) as count');

        /*
          "SELECT COUNT(*)
          FROM   $this->_sTable
          $sWhere";
         */

        $iFilteredTotal = $db1->resultnum();

        /* Total data set length */
        /*
          $sQuery = "
          SELECT COUNT(`".$this->_sIndexColumn."`)
          FROM   $this->_sTable";
         */

        $db1->query('SELECT COUNT(id) as id FROM ' . $db1->db_prefix . 'ratings');
        $aResultTotal = $db1->resultnum();
        $iTotal = $aResultTotal;
        $db1->endTransaction();

        //print_r($iFilteredTotal);

        if ($dislikesArr != null) {
            foreach ($dislikesArr as $dislike) {
                //$likes['songs']['song'][$like['songid']] = $like;
                $dislikes[$dislike['songid']] = $dislike;
            }

            /*
              foreach($l['songs']['song'] as $key => $val){
              $likes[$key]['likes'] = count($val);
              }
             */
        } else {
            $dislikes = [];
        }

        $output = $dislikes;

        $arr_c = count($output);
        $resIn = false;
        if (!empty($arr_c)) {
            $i = 0;
            foreach ($output as $key => $val) {
                if ($arr_c == $i + 1) {
                    $sep = '';
                } else {
                    $sep = ', ';
                }

                $resIn .= $key . $sep;
                ++$i;
            }
        } else {
            $resIn = 'error';
        }

        $dislikes['resIn'] = $resIn;
        $dislikes['output'] = $output;
        $dislikes['iFilteredTotal'] = $iFilteredTotal;
        $dislikes['iTotal'] = $iTotal;

        return $dislikes;
    }

    public function regularLatestProcess()
    {

        /*
         * Paging
         */
        $sLimit = '';
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = 'LIMIT ' . intval($_GET['iDisplayStart']) . ', ' .
                intval($_GET['iDisplayLength']);
        }

        /*
         * Ordering
         */
        $sOrder = '';
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = 'ORDER BY  ';
            for ($i = 0; $i < intval($_GET['iSortingCols']); ++$i) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == 'true') {
                    $sOrder .= '`' . $this->_aColumns[intval($_GET['iSortCol_' . $i])] . '` ' .
                        ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ', ';
                }
            }

            $sOrder = substr_replace($sOrder, '', -2);
            if ($sOrder == 'ORDER BY') {
                $sOrder = '';
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = '';
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
            $search = rtrim($_GET['sSearch']);
            $search = ltrim($search);

            $words = preg_split('/((^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $w_cnt = count($words);

            if ($w_cnt == 1) {
                $search_ready = $words[0];
            } elseif ($w_cnt == 2) {
                $search_space = implode(' ', $words);
                $search_ready = $search_space;
            } elseif ($w_cnt == 3) {
                $pre_search = $words[0] . ' ' . $words[1];
                $post_search = ' ' . $words[2];
            } else {
                $pre_search = $words[0] . ' ' . $words[1];
                unset($words[0]);
                unset($words[1]);
                $search_pipeline = implode('|', $words);
                $post_search = $search_pipeline;
            }

            $sWhere = 'WHERE (';
            //$sWhere = "WHERE ";
            for ($i = 0; $i < count($this->_aColumns); ++$i) {
                if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == 'true') {
                    //$sWhere .= "`".$this->_aColumns[$i]."` REGEXP '^".$search_ready."$' OR ";
                    if ($w_cnt == 1) {
                        $sWhere .= '`' . $this->_aColumns[$i] . "` REGEXP '" . $search_ready . "' OR ";
                    } elseif ($w_cnt == 2) {
                        $sWhere .= '`'
                            . $this->_aColumns[$i] . "` REGEXP '("
                            . $search_ready . "|\\\("
                            . $search_ready . ")' OR ";
                    } elseif ($w_cnt == 3) {
                        $sWhere .= '`'
                            . $this->_aColumns[$i] . "` REGEXP '("
                            . $pre_search . "|\\\("
                            . $post_search . ")' OR ";
                    } else {
                        $sWhere .= '`'
                            . $this->_aColumns[$i] . "` REGEXP '("
                            . $pre_search . '|'
                            . $post_search . ")' OR ";
                    }
                }
            }
            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
            //$sWhere .= '';
        }

        //echo $sWhere;
        //exit();

        /* Individual column filtering */
        for ($i = 0; $i < count($this->_aColumns); ++$i) {
            if (isset($_GET['bSearchable_'
            . $i]) && $_GET['bSearchable_'
            . $i] == 'true' && $_GET['sSearch_'
            . $i] != '') {
                if ($sWhere == '') {
                    $sWhere = 'WHERE ';
                } else {
                    $sWhere .= ' AND ';
                }
                $sWhere .= '`' . $this->_aColumns[$i] . "` LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
            }
        }

        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = '
            SELECT SQL_CALC_FOUND_ROWS `' . str_replace(' , ', ' ', implode('`, `', $this->_aColumns)) . '`
            FROM   ' . $this->_database->db_prefix . $this->_sTable . "
            $sWhere
            $sOrder
            $sLimit
            ";

        $this->_database->beginTransaction();
        $this->_database->query($sQuery);
        $rResult = $this->_database->resultset();

        if ($rResult) {
            //echo '<pre>';
            //print_r($rResult);
            //exit();

            /* Data set length after filtering */
            /*
              $sQuery = "
              SELECT FOUND_ROWS()
              ";
             */

            /*
              $sQuery = "
              SELECT COUNT(*)
              FROM   $this->_sTable
              $sWhere
              ";
             */

            $this->_database->query('SELECT COUNT(*) as count FROM '
                . $this->_database->db_prefix
                . $this->_sTable . ' '
                . $sWhere . '');
            $iFilteredTotal = $this->_database->resultnum();

            /* Total data set length */
            /*
              $sQuery = "
              SELECT COUNT(`".$this->_sIndexColumn."`)
              FROM   $this->_sTable
              ";
             */
            //$rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );
            //$aResultTotal = mysql_fetch_array($rResultTotal);

            $this->_database->query('SELECT COUNT(' . $this->_sIndexColumn . ') as count FROM ' . $this->_database->db_prefix . $this->_sTable . '');
            //$rResultTotal = $this->_database->single();
            $aResultTotal = $this->_database->resultnum();
            $iTotal = $aResultTotal;
            $this->_database->endTransaction();

            /*
              while ( $aRow = ( $rResult ) )
              {
              $row = array();
              for ( $i=0 ; $i<count($this->_aColumns) ; $i++ )
              {
              if ( $this->_aColumns[$i] == "version" )
              {
              # Special output formatting for 'version' column
              $row[] = ($aRow[ $this->_aColumns[$i] ]=="0") ? '-' : $aRow[ $this->_aColumns[$i] ];
              }
              else if ( $this->_aColumns[$i] != ' ' )
              {
              # General output
              $row[] = $aRow[ $this->_aColumns[$i] ];
              }
              }
              $output['aaData'][] = $row;
              } */

            /*
             * Output
             */
            $output = [
                'sEcho' => intval($_GET['sEcho']),
                'iTotalRecords' => $iTotal,
                'iTotalDisplayRecords' => $iFilteredTotal,
                'aaData' => [],
            ];

            foreach ($rResult as $aRow) {
                $row = [];

                //$row[] = '<img src="../../examples_support/details_open.png">';

                for ($i = 0; $i < count($this->_aColumns); ++$i) {
                    if ($this->_aColumns[$i] == 'version') {
                        # Special output formatting for 'version' column
                        $row[] = ($aRow[$this->_aColumns[$i]] == '0') ? '-' : $aRow[$this->_aColumns[$i]];
                    } elseif ($this->_aColumns[$i] != ' ') {
                        # General output
                        $row[] = $aRow[$this->_aColumns[$i]];
                    }
                }
                //$row['extra'] = 'hrmll';
                $output['aaData'][] = $row;
            }

            $arr_c = count($output['aaData']);
            $resIn = false;
            foreach ($output['aaData'] as $key => $val) {
                if ($arr_c == $key + 1) {
                    $sep = '';
                } else {
                    $sep = ', ';
                }

                $resIn .= $output['aaData'][$key][0] . $sep;
            }

            $likes = $this->getDataLikes($resIn);
            $dislikes = $this->getDataDislikes($resIn);
            //$comments = $this->getDataComments($resIn);

            $time = new Time;

            foreach ($output['aaData'] as $key => $val) {
                $output['aaData'][$key]['id'] = '<a href="' . BASE_URL . 'tune/' . $val[0] . '/" class="ajax-nav">' . $val[0] . '</a>';
                unset($output['aaData'][$key][0]);
                $output['aaData'][$key]['artist'] = $val[1];
                unset($output['aaData'][$key][1]);
                $output['aaData'][$key]['title'] = $val[2];
                unset($output['aaData'][$key][2]);

                //$output['aaData'][$key]['date_added'] = $val[2];
                //unset($output['aaData'][$key][2]);

                $duration = $time->secondsToTime(round($val[3] / 1000));
                unset($output['aaData'][$key][3]);
                $output['aaData'][$key]['duration'] = $duration;
            }
        } else {
            $output['error'] = 'nothing find';
        }

        return $output;
    }

    public function extendProcess($data)
    {
        return $data;
    }

    public function voted()
    {
        global $domain, $path, $expired;
        $voted = 0;

        if (isset($_COOKIE['voted'])) {
            if ($_COOKIE['voted'] == 0) {
                return false;
            } elseif ($_COOKIE['voted'] == 1) {
                return true;
            }
        } else {
            setcookie('voted', $voted, time() + $expired, $path, $domain, false, true);
        }
    }

    public function SetCurrentShow($data)
    {
        global $db1;

        $shows = [
            0 => 'Test ShoW!!!',
            1 => 'Prepare for Morning',
            2 => 'Trance Edition!',
            3 => 'Rave Nation!',
            4 => 'Eurodance Members',
            5 => 'Dance Selection!',
            6 => 'Wake Up Power!',
            7 => 'Prepare for Power!',
            8 => 'You want Harder?',
            9 => 'Techno Time',
            10 => 'Request Feature',
            11 => 'Slow Emotions',
            12 => 'Party feeling with URDT',
            13 => 'Prepare for Lives',
            14 => 'Take The Chance Of Dance with Universal Radio',
            15 => 'Saturday Warm Up',
            16 => 'URDT Live Sets',
            17 => 'Live Sets',
            18 => 'Universal Radio Classics',
            19 => 'Xpand Spectrum with DJ Beater',
            20 => 'Special Connect with DJ Special',
            21 => 'Digital Age - On Stage',
            22 => 'Karlito House Selection',
            23 => 'Energy Flow with Girts L.',
            24 => 'Gatis G. selection',
            25 => 'Junior G. selection',
            26 => 'ΕΔΩ selection',
            27 => '',
            28 => '',
            29 => '',
            30 => 'Test show!!!',
        ];

        $djs = [
            0 => '',
            1 => 'Beater',
            2 => 'Rolla',
            3 => 'Edo',
            4 => 'Girts L.',
            5 => 'Digital Age',
            6 => 'Gatis G.',
            7 => 'Special',
            8 => 'Junior G.',
            9 => 'Karlito',
        ];

        // data OK insert in db
        $db1->query('INSERT INTO ' . $db1->db_prefix . 'currshow (currshow_showid, currshow_artist, currshow_title, currshow_send) VALUES (:id, :artist, :title, :time)');
        $db1->bind(':id', $data['showID']);
        $db1->bind(':artist', $data['artist']);
        $db1->bind(':title', $data['title']);
        $db1->bind(':time', $data['time']);
        $result = $db1->execute();
        if ($result) {
            echo 'Show with id: '
            . $data['showID'] . ' and name '
                . $data['artist'] . ' - '
                . $data['title'] . ', added at '
                . $data['time'];
        }
    }
}
