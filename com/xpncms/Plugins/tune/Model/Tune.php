<?php

namespace XpnCMS\Plugins\tune\Model;

use XpnCMS\Model\Model;
use Core\Includes\Config;
use XpnCMS\Model\User;
use XpnCMS\Controller\Controller;
use Model\Song\SongsQuery;
use Model\Song\SongsInformationQuery;
use Core\Helpers\Files;
use Model\Main\DisableReasons;
use Core\Includes\Action as ActionRepo;
use XpnCMS\Model\Notify;

class Tune extends Model
{
    protected $cacheHash = 20160526151411; // hash for phpThumb

    //private $relativeUploaded = 'uploaded/'; // relative from coversMnt

    protected $songTrackingFields = [
        'artist',
        'title',
        'album',
        'publisher',
        'year',
        'album_art',
        'enabled',
        'weight'
    ];

    protected $songInformationTrackingFields = [
        'url',
        'beatport',
        'soundcloud',
        'youtube',
        'itunes'
    ];

    public function __construct()
    {
        $this->cacheHash = Config::get('site.phpThumbCacheHash');
    }

    public function getTune($trID)
    {
        global $db2;

        $controllerRepo = new Controller;
        $gradioRepo = $controllerRepo->loadPluginModel('gradio');
        $song = last($gradioRepo->formatTunes($gradioRepo->getTunesByIds([intval($trID)])));
        $song = $this->appendRatings($song, $trID);

        return $song;
    }

    public function appendRatings(array $song, int $trID)
    {
        $song['likes'] = 0;
        $song['dislikes'] = 0;

        if (! $trID) {
            return $song;
        }

        $ratings = $this->getDataRatings($trID);

        if ($ratings) {
            $song['likes'] = $ratings['likes'];
            $song['dislikes'] = $ratings['dislikes'];
            $song['comments'] = null;

            $song['likespercent']    = 100;
            $song['dislikespercent'] = 0;

            if ($ratings['likes'] > $ratings['dislikes']) {
                $song['likespercent'] = round((1 - $ratings['dislikes'] / $ratings['likes']) * 100);
                $song['dislikespercent'] = round(($ratings['dislikes'] / $ratings['likes']) * 100);
            } elseif ($ratings['likes'] < $ratings['dislikes']) {
                $song['likespercent']    = round(($ratings['likes'] / $ratings['dislikes']) * 100);
                $song['dislikespercent'] = round((1 - $ratings['likes'] / $ratings['dislikes']) * 100);
            } elseif ($ratings['likes'] == $ratings['dislikes']) {
                $song['likespercent']    = 50;
                $song['dislikespercent'] = 50;
            }
        }

        return $song;
    }

    public function getTuneOnly($tuneId)
    {
        global $db2;
        $db2->query('SELECT * FROM '.$db2->db_prefix.'songs WHERE ID = :song_id');
        $db2->bind(':song_id', $tuneId);

        return $db2->single();
    }

    public function getTuneInformation($tuneId)
    {
        global $db2;
        $db2->query('SELECT * FROM '.$db2->db_prefix.'songs_information WHERE song_id = :song_id');
        $db2->bind(':song_id', $tuneId);

        return $db2->single();
    }

    public function getTuneMeta($tune)
    {
        $id = $tune['ID'];
        $pageTitle = _('Tune') . ' ' . $tune['artist'] . ' - ' . $tune['title'];
        $contentText = '';
        if (!empty($tune['weight'])) {
            $contentText .= _('Weight') . ': ' . $tune['weight'] . '%, ';
        }
        if (!empty($tune['duration'])) {
            $contentText .= _('Duration') . ': ' . $tune['duration'] . ', ';
        }
        if (!empty(intval($tune['bpm']))) {
            $contentText .= 'BPM: ' . $tune['bpm'] . ', ';
        }
        if (!empty($tune['album'])) {
            $contentText .= _('Album') . ': ' . $tune['album'] . ', ';
        }
        if (!empty($tune['genres'])) {
            $genresArray = array_map(function ($genre) {
              return $genre['name'];
            }, $tune['genres']);
            $contentText .= _('Genre') . ': ' . implode(', ', $genresArray) . ', ';
        }
        if (!empty($tune['year'])) {
            $contentText .= _('Year') . ': ' . $tune['year'] . ', ';
        }
        if (!empty($tune['likes'])) {
            $contentText .= _('Likes') . ': ' . intval($tune['likes']) . ', ';
        }
        if (!empty($tune['dislikes'])) {
            $contentText .= _('Dislikes') . ': ' . intval($tune['dislikes']) . ', ';
        }
        if (!empty($tune['count_played'])) {
            $contentText .= _('Play count') . ' ' . $tune['count_played'];
        }

        $coverImageDecoded = urldecode($tune['resizeCover']);
        $tuneUrl = Config::get('base.fbsiteurl') . '/tune/' . $id;
        $tuneDescription = strip_tags(html_entity_decode($contentText));

        $meta = [
            'pageTitle' => $pageTitle,
            'content' =>$tuneDescription,
            'ogtype' => 'website',
            'ogdescription' => $tuneDescription,
            // 'musicduration' => round($tune['durationInSec']),
            'ogurl' => str_replace('https', 'http', Config::get('base.fbsiteurl')) . '/tune/' . $id . '/', // without https to work fb comments
            'ogimage' => $coverImageDecoded,
            'ogtitle' => $pageTitle,
            'twittercard' => 'summary_large_image',
            'twittertitle' => $pageTitle,
            'twitterdescription' => $tuneDescription,
            'twitterimage' => $coverImageDecoded,
            'twittersite' => Config::get('site.meta.twittersite'),
            'twittercreator' => Config::get('site.meta.twittercreator'),
            'twitterurl' => $tuneUrl,
            'name' => $pageTitle,
            'description' => $tuneDescription,
            'image' => $coverImageDecoded,
            'canonical' => $tuneUrl,
        ];

        return $meta;
    }

    public function getDataRatings($songid)
    {
        global $db1;
        // select action_id, count(*) as quantity from gradio_ratings where item_id=31924 and action_id in(1,2) group by action_id

        $db1->query(
            'select count(IF(action_id = 1, 1, null)) as likes, count(IF(action_id = 2, 1, null)) as dislikes
            FROM '.$db1->db_prefix.'ratings WHERE item_id=:songid AND category_id=:category_id LIMIT 0, 1'
        );
        $db1->bind(':songid', $songid);
        $db1->bind(':category_id', 1);

        return $db1->single();
    }

    public function saveSong($songData)
    {
        $song = (object) $songData;

        $userRepo = new User;
        $userData = (object) $userRepo->get(User::authId());

        $songRepo = new SongsQuery;

        $saveSong = $songRepo->findPk($song->ID);
        $saveSong->setArtist($song->artist);
        $saveSong->setTitle($song->title);
        $saveSong->setAlbum($song->album);
        $saveSong->setPublisher($song->publisher);
        $saveSong->setYear($song->year);
        $saveSong->setAlbumArt($song->album_art);
        $saveSong->setEnabled($song->enabled);
        $saveSong->setWeight($song->weight);
        $saveSong->setBpm($song->bpm);

        $saveSong->setVersionCreatedBy($userData->username);
        $saveSong->setVersionComment('Update');

        return $saveSong->save();
    }

    public function checkSongInformation($song_id)
    {
        global $db2;

        $db2->query('SELECT song_id FROM '.$db2->db_prefix.'songs_information WHERE song_id = :song_id');
        $db2->bind(':song_id', $song_id);

        return $db2->single();
    }

    protected function getSongInformationDiff($songData, $saveSong)
    {
        $formData = [];
        foreach ($this->songInformationTrackingFields as $val) {
            $formData[$val] = $songData[$val];
        }

        $current = [
            'url' => $saveSong->getUrl(),
            'beatport' => $saveSong->getBeatport(),
            'soundcloud' => $saveSong->getSoundcloud(),
            'youtube' => $saveSong->getYoutube(),
            'itunes' => $saveSong->getItunes()
        ];

        $someDiff = [];
        $additionDiff = array_diff($formData, $current);
        $removalDiff = array_diff($current, $formData);

        $someDiff = count($additionDiff) ? $additionDiff : (count($removalDiff) ? $removalDiff : []);

        return $someDiff; // getting diff
    }

    public function saveSongInformation($songData)
    {
        $songsInformation = (object) $songData;

        $userRepo = new User;
        $userData = (object) $userRepo->get(User::authId());

        $songsInformationRepo = new SongsInformationQuery;

        $saveSong = $songsInformationRepo::create()
            ->filterBySongId($songsInformation->song_id)
            ->findOneOrCreate();

        $diff = $this->getSongInformationDiff($songData, $saveSong);

        if (count($diff)) {
            $saveSong->setUrl(!empty($songsInformation->url) ? $songsInformation->url : null);
            $saveSong->setBeatport(!empty($songsInformation->beatport) ? $songsInformation->beatport : null);
            $saveSong->setSoundcloud(!empty($songsInformation->soundcloud) ? $songsInformation->soundcloud : null);
            $saveSong->setYoutube(!empty($songsInformation->youtube)? $songsInformation->youtube : null);
            $saveSong->setItunes(!empty($songsInformation->itunes) ? $songsInformation->itunes : null);
            $saveSong->setAddedBy($userData->id);
            $saveSong->setCreatedAt($saveSong->getCreatedAt()?: date('Y-m-d H:i:s'));
            $saveSong->setUpdatedAt(date('Y-m-d H:i:s'));

            $saveSong->setVersionCreatedBy($userData->username);
            $saveSong->setVersionComment('Update');
            $saveSong->save();

            return $saveSong->getId();
        }

        return 0;
    }

    protected function iterateChanges($songChanges)
    {
        $changes = '';
        unset($songChanges['CreateddAt']);
        unset($songChanges['UpdatedAt']);

        foreach ($songChanges as $key => $val) {
            if (!in_array($key, array_merge($this->songTrackingFields, $this->songInformationTrackingFields))) {
                continue;
            }

            $changes .= $key . ' ';
            $changes .= _('from') . ' <b>' . reset($val) . '</b> ';
            $changes .= _('to') . ' <b>' . end($val) . '</b> ';
            $changes .= '<br>';
        }

        return $changes;
    }

    public function getSongLatestChanges($initialSong)
    {
        $songRepo = new SongsQuery;
        $foundSong = $songRepo::create()->findPk($initialSong['Id']);
        $songLastVersion = $foundSong->getLastVersionNumber();

        $changes = '';
        if (!$songLastVersion) {
            return $changes;
        }

        if ($songLastVersion === 1) {
            $songChanges = [];
            $firstVersion = $foundSong->getOneVersion($songLastVersion);

            //print_r($initialSong);
            //print_r($firstVersion->toArray());
            //exit;

            foreach ($this->songTrackingFields as $val) {
                $methodArray = explode('_', $val);
                $methodArray = array_map('ucfirst', $methodArray);
                $method = 'get' . implode('', $methodArray);
                $firstVersionVal = $firstVersion->{$method}(); // call method from string
                $initialVal = $initialSong[implode('', $methodArray)];

                if ($initialVal != $firstVersionVal) {
                    $songChanges[$val] = [0 => $initialVal, 1 => $firstVersionVal];
                }
            }

            $changes .= $this->iterateChanges($songChanges);

            return $changes;
        }

        $songChanges = $foundSong->compareVersions($songLastVersion-1, $songLastVersion);

        $changes .= $this->iterateChanges($songChanges);

        return $changes;
    }

    public function getSongInformationLatestChanges($songData)
    {
        $songsInformationRepo = new SongsInformationQuery;
        $foundSongInformation = $songsInformationRepo->filterBySongId($songData['song_id'])->findOne();
        $songInformationLastVersion = $foundSongInformation->getLastVersionNumber();

        $changes = '';
        if (!$songInformationLastVersion) {
            return $changes;
        }

        if ($songInformationLastVersion === 1) {
            $songInformationChanges = [];
            foreach ($this->songInformationTrackingFields as $val) {
                if (!empty($songData[$val])) {
                    $songInformationChanges[$val] = [0 => '', 1 => $songData[$val]];
                }
            }

            $changes .= $this->iterateChanges($songInformationChanges);

            return $changes;
        }

        $songInformationChanges = $foundSongInformation->compareVersions(
            $songInformationLastVersion-1,
            $songInformationLastVersion
        );

        $changes .= $this->iterateChanges($songInformationChanges);

        return $changes;
    }

    /**
     *
     * @param  array  $payload
     * @return array
     */
    public function saveTuneV2(array $payload)
    {
        if (isset($payload['remoteFileUpload']) &&
            !empty($payload['remoteFileUpload'])) {
            $filesHelper = new Files();
            $remoteFile = $filesHelper->getRemoteFile(
                $payload['remoteFileUpload'],
                Config::get('plugins.gradio.prefs.uploaded')
            );
            $relativeUpload = Config::get('plugins.gradio.prefs.relativeUploaded');
            $payload['songs']['album_art'] = $relativeUpload . $remoteFile->file->filename;
        }

        $disableReasonReason = null;
        $savedDisableReason = null;
        if (isset($payload['disableReason']) &&
            !empty($payload['disableReason'])) {
            $disableReasonReason = $payload['disableReason'];

            $disableReason = new DisableReasons;
            $disableReason->setSongId($payload['songs']['ID']);
            $disableReason->setUserid(User::authId());
            $disableReason->setReason($disableReasonReason);
            $savedDisableReason = $disableReason->save();
        }

        $enabled = $payload['songs']['enabled'];

        $payload['songs']['enabled'] = $enabled === 'true' || $enabled === '1';
        $tuneId = $payload['songs']['ID'];

        $songRepo = new SongsQuery;
        $initialSong = $songRepo::create()->findPk($tuneId)->toArray();

        $savedSong = $this->saveSong($payload['songs']);
        $savedSongInformationId = $this->saveSongInformation(
            $payload['songs_information']
        );

        $result = [];
        $simpleNotify = (object)[];
        $slackHook = [];
        if ($savedSong || $savedSongInformationId) {
            $actionRepo = new ActionRepo;
            $addHumanAction = $actionRepo->addHumanAction(
                'updated',
                'tune_info',
                $tuneId,
                null
            );
            $result['addHumanAction'] = $addHumanAction;

            $changes = '<br>';
            if ($savedSong) {
                $changes .= $this->getSongLatestChanges($initialSong);
            }

            if ($savedSongInformationId) {
                $changes .= $this->getSongInformationLatestChanges(
                    $payload['songs_information']
                );
            }

            $simpleNotify = (new Notify)->getCode(303, [
                'tuneTitle' => $payload['songs']['artist'] . ' - ' . $payload['songs']['title'],
                'changes' => $changes,
            ]);

            $slackHook = [
                'title' => '',
                'type' => 'info',
                'message' => '',
                'alert' => $changes,
                //'item' => $item,
                'itemId' => $tuneId,
            ];
        }

        return compact('simpleNotify', 'result', 'slackHook');
    }
}
