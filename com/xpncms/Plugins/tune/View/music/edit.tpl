{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="content"}
<div class="container-fluid">
  <section class="main-section" id="edit-tune">
    <div class="content">
      {if isset($tune_error) }
        <h3><div class="alert alert-danger">{t}Tune not found{/t}</div></h3>
      {elseif isset($tune) }
        <h3>{t}Edit tune{/t} {$tune.artist} - {$tune.title} <a href="{$url}tune/{$tune.id}" class="ajax-nav pull-right btn btn-default">{t}To tune{/t}</a></h3>
        <div class="tune-table-edit" style="margin-bottom: 10px;">
            <div class="row" style="padding-top:10px;">
              <div class="col-xs-4">
                {t}Search by:{/t} {t}Artist{/t} - {t}Title{/t}
                <a href="//google.com/search?q={urlencode($tune.artist)}+{urlencode($tune.title)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Artist{/t} - {t}Title{/t}
                <a href="//google.com/search?as_st=y&tbm=isch&as_q={urlencode($tune.artist)}+{urlencode($tune.title)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Artist{/t} - {t}Title{/t}
                <a href="#" class="itunes-search" data-query="{$tune.artist} {$tune.title}">
                  <i class="fa fa-apple pull-right"></i>
                </a>
                <br>
                {t}Search by:{/t} {t}Artist{/t}
                <a href="//google.com/search?q={urlencode($tune.artist)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Artist{/t}
                <a href="//google.com/search?as_st=y&tbm=isch&as_q={urlencode($tune.artist)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Artist{/t}
                <a href="#" class="itunes-search" data-query="{$tune.artist}">
                  <i class="fa fa-apple pull-right"></i>
                </a>
                <br>
                {t}Search by:{/t} {t}Title{/t}
                <a href="//google.com/search?q={urlencode($tune.title)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Title{/t}
                <a href="//google.com/search?as_st=y&tbm=isch&as_q={urlencode($tune.title)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Title{/t}
                <a href="#" class="itunes-search" data-query="{$tune.title}">
                  <i class="fa fa-apple pull-right"></i>
                </a>
                <br>
                {t}Search by:{/t} {t}Album{/t}
                <a href="//google.com/search?q={urlencode($tune.album)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Album{/t}
                <a href="//google.com/search?as_st=y&tbm=isch&as_q={urlencode($tune.album)}" target="_blank">
                  <i class="fa fa-google pull-right"></i>
                </a>
                <br>
                {t}Search image by:{/t} {t}Album{/t}
                <a href="#" class="itunes-search" data-query="{$tune.album}">
                  <i class="fa fa-apple pull-right"></i>
                </a>
                <br>
              </div>
              <div class="col-xs-6 col-md-5 col-lg-3">
                <button id="uploadBtn" class="btn btn-sm btn-primary">{t}Choose file{/t}</button> {t}Or Drop here{/t}
                <br><br>
                <div id="dragbox">
                  <div class="progress-container">
                    <div id="progressOuter" class="progress progress-striped active" style="display:none;">
                      <div
                        id="progressBar"
                        class="progress-bar progress-bar-success"
                        role="progressbar"
                        aria-valuenow="45"
                        aria-valuemin="0"
                        aria-valuemax="100"
                        style="width: 0%">
                      </div>
                    </div>
                  </div>
                  <img src="{$tune.phpThumbPictureURL}" class="uploaded-image">
                </div>
              </div>
              <div class="col-xs-4 col-md-3">
                <br>
                <a href="{$plugins.gradio.prefs.utilsurl}admin/tunes?filter=id:{$tune.id}" class="btn btn-warning" target="_blank">
                  {t}Edit in Gradio utils{/t}
                </a>
                <br><br>
                {t}Check in{/t} {t}Facebook{/t}
                <a href="//developers.facebook.com/tools/debug/sharing/?q={urlencode($fbsiteurl)}/tune/{$tune.id}" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
                <br>
                {t}Check in{/t}
                <a href="//oscarotero.com/embed2/demo/index.php?url={$fbsiteurl}/tune/{$tune.id}" target="_blank">
                  {t}Embed{/t}
                </a>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-7">
                <form
                  id="edit-tune"
                  data-remote="true"
                  data-type="json"
                  method="post"
                  action="{$url}tune/save/"
                  accept-charset="UTF-8"
                  class="edit-tune-form">
                  <input name="songs[ID]" type="hidden" value="{$tune.id}">
                  <input name="songs_information[song_id]" type="hidden" value="{$tune.id}">
                  <input class="phpThumbPictureURL" type="hidden" name="phpThumbPictureURL" value="{$tune.phpThumbPictureURL}">
                  <input class="tuneCoversURL" type="hidden" name="tuneCoversURL" value="{$tune.tuneCoversURL}">
                  <input class="relativeUploaded" type="hidden" name="relativeUploaded" value="{$tune.relativeUploaded}">
                  <div class="form-group tune-form-container">
                    <div class="input-group">
                      <span class="input-group-addon">{t}Enabled{/t}</span>
                      <input type="checkbox" name="songs[enabled]" class="form-control" {if $tune.enabled}checked{/if}>
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Image location{/t}</span>
                      <input type="hidden" name="songs[album_art]" class="album_art" value="{$tune.album_art}">
                      <input type="text" name="songs[album_art]" class="form-control album_art" value="{$tune.album_art}" disabled>
                    </div>

                    <div class="input-group">
                      <span class="input-group-addon">{t}Artist{/t} <i class="fa fa-asterisk"></i></span>
                      <input class="form-control" autofocus="autofocus" name="songs[artist]" type="text" value="{$tune.artist}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Title{/t} <i class="fa fa-asterisk"></i></span>
                      <input class="form-control" name="songs[title]" type="text" value="{$tune.title}">
                    </div>
                    <!--
                    <div class="input-group">
                      <span class="input-group-addon">Remix</span>
                      <input class="form-control" name="remix" type="text">
                    </div>
                    -->
                    <div class="input-group">
                      <span class="input-group-addon">{t}Album{/t} <i class="fa fa-asterisk"></i></span>
                      <input class="form-control" name="songs[album]" type="text" value="{$tune.album}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Year{/t}</span>
                      <input class="form-control" name="songs[year]" type="text" value="{$tune.year}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Weight{/t}</span>
                      <input class="form-control" name="songs[weight]" type="text" value="{$tune.weight}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Record label{/t}</span>
                      <input class="form-control" name="songs[publisher]" type="text" value="{$tune.publisher}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}URL{/t}</span>
                      <input class="form-control" name="songs_information[url]" type="text" value="{$tune.url}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Beatport{/t}</span>
                      <input class="form-control" name="songs_information[beatport]" type="text" value="{$tune.beatport}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Soundcloud{/t}</span>
                      <input class="form-control" name="songs_information[soundcloud]" type="text" value="{$tune.soundcloud}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Youtube{/t}</span>
                      <input class="form-control" name="songs_information[youtube]" type="text" value="{$tune.youtube}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}iTunes{/t}</span>
                      <input class="form-control" name="songs_information[itunes]" type="text" value="{$tune.itunes}">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon">{t}Remote file{/t}</span>
                      <input type="text" class="form-control" name="remoteFileUpload" placeholder="{t}http://myserver.com/myfile.jpg{/t}">
                    </div>
                    <br>
                      <button type="submit" class="btn btn-block btn-success pull-right" data-disable-with="{t}Saving...{/t}">{t}Edit{/t}</button>
                  </div>
                </form>
              </div>
              <div class="col-xs-5">
                <form action="" method="get" class="form" accept-charset="utf-8" id="iTunesSearch">
                  <input class="form-control" type="hidden" name="entity" value="album" />
                  <input class="form-control text" type="text" name="query" id="query" placeholder="{t}Type here{/t}" />
                  <input class="form-control" type="hidden" name="country" value="us" />
                  <br>
		              <input class="form-control submit" type="submit" value="{t}Find in iTunes{/t}" />
		            </form>
                <br>
                <div id="iTunes-results"></div>
              </div>
            </div>
        </div>

      {/if}
    </div>
  </section>
</div>
{/block}
