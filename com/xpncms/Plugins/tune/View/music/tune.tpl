{capture name="page_title"}
{block name="title" prepend}{$pageTitle} :{/block}
{/capture}

{block name="twitter_card_extend" append}

<meta name="twitter:label1" content="{$meta.twitterlabel1}">
<meta name="twitter:data1" content="{$meta.twitterdata1}">
<meta name="twitter:label2" content="{$meta.twitterlabel2}">
<meta name="twitter:data2" content="{$meta.twitterdata2}">

{/block}

{block name="content"}
  <div class="content">
    <section class="main-section" id="tune">
      {if isset($tune_error) }
        <h3><div class="alert alert-danger">{t}Tune not found{/t}</div></h3>
        {t}Only logged in user can see ratings!{/t}
      {elseif isset($tune) }
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4">
            <div class="tune-image-100">
              {if ($gradioTeam)}
                <a href="{$url}tune/edit/{$tune.id}" class="ajax-nav btn btn-info edit-tune-btn pull-left"><i class="fa fa-edit "></i> {t}Edit Tune{/t}</a>
                <span class="tune-extension badge btn-warning pull-right">{$tune.extension} </span>
              {/if}
              <img src="{$tune.phpThumbPictureURL}" />
            </div>
          </div>
          <div class="col-xs-12 col-sm-8 col-md-5">
            <div class="row">
              <div class="col-xs-12">
                <h3>{t}Artist{/t}</h3>
                <h2 class="text-left">{$tune.artist}</h2>
                <h3>{t}Title{/t}</h3>
                <h2 class="text-left">{$tune.title}</h2>
              </div>
              <div class="col-xs-6 col-md-6">
                <h3>{t}Album{/t}</h3>
                <h4>{$tune.album}</h4>
                <h3>{t}Genre{/t}</h3>
                <h4>{$tune.genre}</h4>
                <h3>{t}Weight{/t}</h3>
                <h4>{$tune.weight}</h4>
                <h3>{t}Duration{/t}</h3>
                <h4>{$tune.duration}</h4>
                <div class="col-xs-4 col-md-6">
                  <div
                  class="tune-like likes rate"
                  data-item-id="{$tune.id}"
                  data-action="liked"
                  data-action-category="tune"
                  data-ot="{t}Like this tune{/t}"
                  title="{t}Like this tune{/t}">
                  <i class="fa fa-thumbs-o-up fa-3x"></i> <span class="rating-count">{$tune.likes}</span>
                  </div>
                </div>
                <div class="col-xs-4 col-md-6">
                  <div
                  class="tune-dislike dislikes rate"
                  data-item-id="{$tune.id}"
                  data-action="disliked"
                  data-action-category="tune"
                  data-ot="{t}Dislike this tune{/t}"
                  title="{t}Dislike this tune{/t}">
                  <i class="fa fa-thumbs-o-down fa-3x"></i> <span class="rating-count">{$tune.dislikes}</span>
                  </div>
                </div>
              </div>
              <div class="col-xs-6 col-md-6">
                <h3>{t}Last time played{/t}</h3>
                <h4>{$tune.date_played|relative_date}</h4>
                <h3>{t}Play count{/t} {if ($gradioTeam && !$tune.enabled)}<span class="badge btn-warning">{t}Disabled{/t}</span>{/if}</h3>
                <h4>{$tune.count_played}</h4>
                <h3>{t}Date added{/t}</h3>
                <h4>{$tune.date_added|relative_date}</h4>
                <h3>{t}Record label{/t}</h3>
                <h4>
                {if !empty($tune.label)}
                <img src="{$url}themes/{$frontendTheme}/img/icons/labels/{$tune.publisher_svg}" alt="{$tune.label}" title="{$tune.label}" class="tune-label-logo"/>
                {/if}
                </h4>
              </div>
            </div>
            <div class="row">
              <div id="fb-comment-content" class="col-md-12">
                <div
                  class="fb-comments"
                  data-href="{$fbsiteurl}/tune/{$tune.id}"
                  data-numposts="5"
                  data-width="100%">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-3">

            {if !empty($tune.soundcloud) }
            <h3>{t}Other interesting tunes{/t}</h3>
            <div class="row hidden-xs ">
            {block name="latest-tunes"}
              {foreach $latestTunes as $tune}
                <div class="col-xs-12 col-sm-2 col-md-6 wow fadeInRight delay-1s album-thumbnails " title="{$tune.artist} - {$tune.title}">
                  <a href="{$url}tune/{$tune.id}" class="ajax-nav " data-target=".content">
                    <img src="{$tune.phpThumbPictureURL}" class="portrait">

                  </a>
                  <br>
                </div>
              {/foreach}
            {/block}
            </div>
            <br>
            {else}
            <h3>{t}Other interesting tunes{/t}</h3>
            <div class="row ">
            {block name="latest-tunes"}
              {foreach $latestTunes as $tune}
                <div class="col-xs-4 col-md-6 wow fadeInRight delay-1s album-thumbnails " title="{$tune.artist} - {$tune.title}">
                  <a href="{$url}tune/{$tune.id}" class="ajax-nav " data-target=".content">
                    <img src="{$tune.phpThumbPictureURL}" class="portrait">

                  </a>
                  <br>
                </div>
              {/foreach}
            {/block}
            </div>
           {/if}
            <div class="row">
              {if !empty($tune.soundcloud) }
                <div class="row">
                  <div class="col-xs-12 wow fadeInRight delay-2s" title="{t}Soundcloud{/t}">
                    <div class="tune-table-title"></div>
                    <div class="tune-table-content soundcloud-content">
                      <a href="{$tune.soundcloud}" target="_blank"><img src="{$url}themes/{$frontendTheme}/img/icons/soundcloud-logo-opacity-50.svg"/></a>
                    </div>
                  </div>
                </div>
                <br>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no soundcloud link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img src="{$url}themes/{$frontendTheme}/img/icons/soundcloud-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}
              {if !empty($tune.url) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}URL{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a href="{$tune.url}" target="_blank"><span class="fa fa-2x fa-external-link" aria-hidden="true"></span></a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no url link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <span class="fa fa-2x fa-external-link url-search-disabled" aria-hidden="true" style="color: #D6D2D2"></span>
                  </div>
                </div>
              {/if}
              {if !empty($tune.beatport) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}Beatport{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a href="{$tune.beatport}" target="_blank"><img src="{$url}themes/{$frontendTheme}/img/icons/beatport-logo-opacity-50.svg"/></a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no url link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img src="{$url}themes/{$frontendTheme}/img/icons/beatport-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}
              {if !empty($tune.itunes) }
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}iTunes{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a href="{$tune.itunes}" target="_blank"><img src="{$url}themes/{$frontendTheme}/img/icons/itunes-logo-opacity-50.svg"/></a>
                  </div>
                </div>
              {else}
                <div class="col-xs-3 wow fadeInRight delay-2s" title="{t}There is no iTunes link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img src="{$url}themes/{$frontendTheme}/img/icons/itunes-logo-inactive.svg"/>
                  </div>
                </div>
              {/if}
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          {if !empty($tune.youtube_id) }
            <div class="col-xs-12 col-sm-6 col-md-8">
              <div class="responsive-video-container">
                <iframe src="https://www.youtube.com/embed/{$tune.youtube_id}?&hd=1" frameborder="0" allowfullscreen class="responsive-video"></iframe>
              </div>
              <br>
            </div>
            <div class="col-sm-6 col-md-4">
              <h3>{t}Other cool videos{/t}</h3>
              {assign var=tune1 value=0.2}
              {foreach $latestTuneVideos as $video}
                <div class="col-xs-4 col-sm-6 wow fadeInRight album-thumbnails" data-wow-delay="{$tune1}s" title="{$video.artist} - {$video.title}">
                  <a href="{$url}tune/{$video.song_id}" class="ajax-nav album-thumbnails" data-target=".content">
                    <img src="https://img.youtube.com/vi/{$video.youtube_id}/hqdefault.jpg" class="portrait">
                  </a>
                  <br>
                </div>
                {assign var=tune1 value=$tune1+0.2}
              {/foreach}
            </div>
          {else}
            <div class="col-xs-12 ">
              <h3>{t}Other cool videos{/t}</h3>
              {assign var=tune1 value=0.3}
              {foreach $latestTuneVideos as $video}
                <div class="col-xs-4 col-lg-2 wow fadeInRight album-thumbnails" data-wow-delay="{$tune1}s" title="{$video.artist} - {$video.title}">
                  <a href="{$url}tune/{$video.song_id}" class=" ajax-nav album-thumbnails" data-target=".content">
                    <img src="https://img.youtube.com/vi/{$video.youtube_id}/hqdefault.jpg" class="portrait">
                  </a>
                  <br>
                </div>
                {assign var=tune1 value=$tune1+0.3}
              {/foreach}
            </div>
          {/if}
        </div>
      </div>
    <script>
    var soundCloudUrl = "{$tune.soundcloud}";
    {literal}
      function initTuneIndex()
      {
        if (soundCloudUrl) {
          $.ajax({
            url: '//soundcloud.com/oembed?callback=?',
            data: {format: 'json', url: soundCloudUrl, iframe: true, maxheight: 150},
            success: function (data, status) {
              $('.soundcloud-content').html(data['html']);
              soundCloudUrl = null; // reset to work next tune from dynamic load
            },
            error: function (xOptions, textStatus) {
              xpnCMSNotify.simpleNotify({title: t('Ouch'), 'type': 'error', alert: t('Unable to load soundcloud tune')});
              $('.soundcloud-content').fadeOut('fast');
            }
          });
        }
      }

      document.addEventListener("DOMContentLoaded", function(event) {

  var addImageOrientationClass = function(img) {
    if(img.naturalHeight > img.naturalWidth) {
      img.classList.add("portrait");
    }
  }

  // Add "portrait" class to thumbnail images that are portrait orientation
  var images = document.querySelectorAll(".album-thumbnails img");
  for(var i=0; i<images.length; i++) {
    if(images[i].complete) {
      addImageOrientationClass(images[i]);
    } else {
      images[i].addEventListener("load", function(evt) {
        addImageOrientationClass(evt.target);
      });
    }
  }

});
     {/literal}
      </script>
      {/if}
    </section>
  </div>
{/block}
