<?php

namespace XpnCMS\Plugins\tune\Controller;

use XpnCMS\Controller\Auth;
use XpnCMS\Plugins\Plugins;
use Core\Includes\Config;
use Core\Helpers\Seo;
use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;
use Core\Includes\Upload\FileUpload;
use Core\Helpers\Files;
use XpnCMS\Controller;

class Tune extends Plugins
{
    protected $request;

    public function __construct()
    {
        global $uri, $rbac;
        $session = new Session();

        // Get Role Id
        $roleId = $rbac->Roles->returnId('gradio');
        // Make sure User has 'forum_user' Role
        $allowed = 0;
        if (User::authId()) {
          $allowed = $rbac->Users->hasRole($roleId, User::authId());
        }

        $allowedMethods = [
          'index',
        ];

        if ($allowed) {
            return true;
        }

        if (!in_array($uri['method'], $allowedMethods)) {
          Auth::notAllowed();
        }
    }

    public function index($id)
    {
        $user = $this->loadModel('user');
        $userData = User::authId() ? $user->get(User::authId()) : null;
        $tuneRepo = $this->loadPluginModel('tune');
        $getTune = $tuneRepo->getTune($id);

        // $gradioRepo = $this->loadPluginModel('gradio');
        // $latestTunes = $gradioRepo->getLatestTunes(0, 6, true);
        // $latestTuneVideos = $gradioRepo->getLatestVideos(6, true);

        if (!empty($id) && is_numeric($id) && $getTune) {
            $meta = $tuneRepo->getTuneMeta($getTune);
            $pageTitle = $meta['pageTitle'];

            $data = [
                'pageTitle' => $pageTitle,
                'userData' => $userData,
                'tune' => $getTune,
                'meta' => $meta,
                // 'latestTunes' => $latestTunes,
                // 'latestTuneVideos' => $latestTuneVideos,
            ];
        } else {
            $data = [
                'pageTitle' => _('Tune not found'),
                'userData' => $userData,
                'tune_error' => 'error'
            ];
        }

        $this->loadPluginView('tune/music/tune', $data);
    }

    public function edit($id)
    {
        $user = $this->loadModel('user');
        // get the user data from database
        $userData = $user->get(User::authId());
        $tune = $this->loadPluginModel('tune');

        if (!empty($id) && is_numeric($id)) {
            $getTune = $tune->getTune($id);
            if ($getTune['error']) {
                $data = ['pageTitle' => _('Tune not found'), 'userData' => $userData, 'tune_error' => $getTune];
            } else {
                $data = [
                    'pageTitle' => _('Edit tune') . ' ' . $getTune['artist'] . ' - ' . $getTune['title'],
                    'userData' => $userData,
                    'tune' => $getTune
                ];
            }
        } else {
            $data = ['pageTitle' => _('Tune not found'), 'userData' => $userData, 'tune_error' => 'error'];
        }

        $this->loadPluginView('tune/music/edit', $data);
    }

    public function coverupload()
    {
        $user = $this->loadModel('user');

        $uploadDir = Config::get('plugins.gradio.prefs.uploaded');

        $filesHelper = new Files;
        $uploader = new FileUpload('uploadfile');
        $uploader->sizeLimit = $filesHelper->fileUploadMaxSize();

        $seo = new Seo();
        $filename = $seo->generateRandomString();
        $uploader->newFileName = $filename;

        // Handle the upload
        $result = $uploader->handleUpload($uploadDir);
        if (!$result) {
            exit(json_encode(['success' => false, 'msg' => $uploader->getErrorMsg()]));
        }
        echo json_encode(['success' => true, 'filename' => $result]);
    }

    /*
    public function cover($filename)
    {
        $file = dirname(__FILE__) . '/upload/' . $filename;
        $type = 'image/jpeg';
        header('Content-Type:'.$type);
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }
    */

    // public function save()
    // {
    //     global $action;
    //
    //     $this->request = Request::createFromGlobals();
    //     $post = $this->request->request;
    //     $postAll = $post->all();
    //     $tuneRepo = $this->loadPluginModel('tune');
    //
    //     if ($post->get('remoteFileUpload')) {
    //         $filesHelper = new Files();
    //         $remoteFile = $filesHelper->getRemoteFile(
    //             $post->get('remoteFileUpload'),
    //             Config::get('plugins.gradio.prefs.uploaded')
    //         );
    //
    //         $postAll['songs']['album_art'] = Config::get('plugins.gradio.prefs.relativeUploaded') . $remoteFile->file->filename;
    //     }
    //
    //     $enabled = 1;
    //     if (isset($post->get('songs')['enabled'])) {
    //         if ($post->get('songs')['enabled'] == 'false') {
    //             $enabled = 0;
    //         }
    //     }
    //
    //     $disableReasonReason = null;
    //     $savedDisableReason = null;
    //     if ($post->get('disableReason')) {
    //         $disableReasonReason = $post->get('disableReason');
    //
    //         $disableReason = new DisableReasons;
    //         $disableReason->setSongId($postAll['songs']['ID']);
    //         $disableReason->setUserid(User::authId());
    //         $disableReason->setReason($disableReasonReason);
    //         $savedDisableReason = $disableReason->save();
    //     }
    //
    //     $postAll['songs']['enabled'] = $enabled;
    //
    //     $songRepo = new SongsQuery;
    //     $initialSong = $songRepo::create()->findPk($postAll['songs']['ID'])->toArray();
    //
    //     $savedSong = $tuneRepo->saveSong($postAll['songs']);
    //     $savedSongInformationId = $tuneRepo->saveSongInformation($postAll['songs_information']);
    //
    //     $notifyRepository = $this->loadModel('notify');
    //
    //     $result = [];
    //     $simpleNotify = [];
    //     $slackHook = [];
    //     if ($savedSong || $savedSongInformationId) {
    //         $addHumanAction = $action->addHumanAction('updated', 'tune_info', $postAll['songs']['ID'], null);
    //         $result['addHumanAction'] = $addHumanAction;
    //
    //         $changes = '<br>';
    //         if ($savedSong) {
    //             $changes .= $tuneRepo->getSongLatestChanges($initialSong);
    //         }
    //
    //         if ($savedSongInformationId) {
    //             $changes .= $tuneRepo->getSongInformationLatestChanges($postAll['songs_information']);
    //         }
    //
    //         $simpleNotify = $notifyRepository->getCode(303, [
    //             'tuneTitle' => $postAll['songs']['artist'] . ' - ' . $postAll['songs']['title'],
    //             'changes' => $changes,
    //         ]);
    //
    //         //$item = $tuneRepo->getTune($postAll['songs']['ID']);
    //         $slackHook = [
    //             'title' => '',
    //             'type' => 'info',
    //             'message' => '',
    //             'alert' => $changes,
    //             //'item' => $item,
    //             'itemId' => $postAll['songs']['ID'],
    //         ];
    //     }
    //
    //     $response = new JsonResponse();
    //     $response->setData(compact('simpleNotify', 'result', 'slackHook'));
    //     $response->send();
    // }
}
