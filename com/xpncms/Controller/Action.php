<?php namespace XpnCMS\Controller;

use XpnCMS\Model\User;
use Core\Includes\Action as ActionRepo;
use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Request;
use stdClass;

class Action extends Controller
{

    public function check()
    {
        global $action;

        $latest_actions = $action->checkNewActions();

        //echo '<pre>';
        //print_r($latest_actions);

        header('Content-Type: application/json');
        echo json_encode($latest_actions);
    }

    public function addhumanaction()
    {
        global $action;

        $request = Request::createFromGlobals();

        $url = str_replace('v4.', '', $request->server->get('HTTP_HOST'));
        $json['rate']['notify'] = [
            'notice' => '',
            'type' => 'notice',
            'title' => _('Notice'),
            'text' => "Cannot rate anymore on the legacy system because we are moving to the new one! <a href='https://$url?utm_source=rating&utm_medium=v4&utm_campaign=migrate-to-v5'>click here to visit new system</a>",
            'salert' => 'this section currently is not available'
        ];

        echo json_encode($json);
        return;

        $postAction = $request->get('action'); // liked, added, changed
        $postActionCategory = $request->get('action_category'); // tune
        $postItemId = $request->get('item_id');

        $notifyRepository = $this->loadModel('notify');

        if (!User::authId()) {
            $notify = $notifyRepository->getCode(10);
            $json['rate']['notify'] = $notify;

            echo json_encode($json);

            return;
        }

        $addHumanAction = $action->addHumanAction($postAction, $postActionCategory, $postItemId, null);

        if ($addHumanAction['action']['added']['success']) {
            $ids = $addHumanAction['action']['added']['success'];
            $json['data'] = $action->getActionsByIds($ids);
            $notify = $notifyRepository->getCode(201);
        }

        if ($addHumanAction['action']['updated']['success']) {
            $ids = $addHumanAction['action']['updated']['success'];
            $json['data'] = $action->getActionsByIds($ids);
            $notify = $notifyRepository->getCode(301);
        }

        if ($postActionCategory == 'tune') {
            $tuneRepo = $this->loadPluginModel('tune');
            $item = (object) $tuneRepo->getTune($postItemId);
            if ($postAction == 'liked') {
                $notify = $notifyRepository->getCode(207, [
                    'tuneTitle' => $item->artist . ' - ' . $item->title,
                ]);
            }

            if ($postAction == 'disliked') {
                $notify = $notifyRepository->getCode(208, [
                    'tuneTitle' => $item->artist . ' - ' . $item->title,
                ]);
            }
        }

        $json['addhumanaction'] = $addHumanAction;
        $json['rate']['notify'] = $notify;

        echo json_encode($json);
    }

    public function ratetune()
    {
        /*
        $notify = $this->loadModel('notify');
        $actionRepo = new ActionRepo;
        $request = Request::createFromGlobals();

        $itemId = $request->get('item_id');
        $action = $request->get('action');
        $status = $request->get('status'); // added, changed

        $vars = [
            'liked' => ['color' => '#228B22', 'icon' => ':thumbsup:'],
            'disliked' => ['color' => '#D00000', 'icon' => ':thumbsdown:'],
            'same' => ['color' => '#1E90FF', 'icon' => ':eyes:'],
        ];

        //if (User::authId()) {
            $userRepo = $this->loadModel('user');
            $user = $userRepo->get(User::authId());

            $gradioRepo = $this->loadPluginModel('gradio');
            $tune = $gradioRepo->getSongByID($itemId);
            //$artist = $this->transliterateString(html_entity_decode(htmlspecialchars_decode($tune['artist'])));
            //$title = $this->transliterateString(html_entity_decode(htmlspecialchars_decode($tune['title'])));

            if ($status == 'added') {
                $notify = $notify->getCode(201);
            } elseif ($status == 'changed') {
                $notify = $notify->getCode(301);
            } else {
                $notify = $notify->getCode(401);
            }
        //} else {
            //$notify = $notify->getCode(14);
        //}
        */

        /*
        $message = '';
        $message .= $notify['alert'] . ' ' . $notify['title'];
        $message .= ' no @' . $user['username'];

        $attachments[] = [
            'fallback' => $message,
            'color' => $vars[$action]['color'],
            'author_name' => $user['username'],
            'author_icon' => Config::get('base.siteurl') . $user['image'],
            'title' => $artist . ' - ' . $title,
            'title_link' => Config::get('base.siteurl') . '/'
            . Config::get('plugins.gradio.prefs.tuneurl') . '/'
            . $itemId . '?utm_campaign=slackratingbot',
            'fields' => [
                [
                    'title' => 'Rating',
                    'value' => $action,
                    'short' => false
                ]
            ],
            'footer' => Config::get('site.title') . ' ' . _('rating bot for Slack'),
            'footer_icon' => Config::get('base.siteurl') . '/'
            . Config::get('paths.assets')
            . '/gradio-player/img/no-now-playing-art.png',
            'ts' => time(),
        ];
        */

        /*
        if ($status !== 'updated') {
            $actionRepo->slackHook(
                $message,
                Config::get('prefs.services.slack.webhook'),
                '#gradio',
                $vars[$action]['icon'],
                $attachments
            );
        }
        */
    }

    private function transliterateString($txt)
    {
        $transliterationTable = ['&' => 'and', '?' => ' '];

        return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
    }

    private function prepareToGlobalObject($itemId, $itemType = 'tune', $itemAction = 'update', $itemActionValue = null)
    {
        $userRepo = $this->loadModel('user');
        $user = $userRepo->get(User::authId());
        $notifyRepository = $this->loadModel('notify');
        $itemObject = new stdClass;

        $utm = '';
        if ($itemType == 'tune') {
            //$gradioRepo = $this->loadPluginModel('gradio');
            $tuneRepo = $this->loadPluginModel('tune');

            //$item = (object) $gradioRepo->getSongByID($itemId);
            $item = (object) $tuneRepo->getTune($itemId);

            $itemObject = $item;
            $itemObject->name = $item->artist . ' - ' . $item->title;
            $itemObject->url = Config::get('base.siteurl') . '/'
            . Config::get('plugins.gradio.prefs.tuneurl') . '/'
            . $item->ID;
            //$itemObject->imageUrl = $item->resizeCover;
            $itemObject->thumbUrl = $item->resizeCover;

            if ($itemAction == 'update') {
                $simpleNotify = $notifyRepository->getCode(304, [
                    'username' => $user['username'],
                    'tuneTitle' => $itemObject->name,
                ]);
                $itemObject->notifyTitle = _('Updated values');
                $itemObject->icon = ':pencil:';

                $utm = '?utm_campaign=slackupdatebot';
            }

            if ($itemAction == 'rate') {
                $itemObject->notifyTitle = _('Rating');
                if ($itemActionValue == 'liked') {
                    $simpleNotify = $notifyRepository->getCode(305, [
                        'username' => $user['username'],
                        'tuneTitle' => $itemObject->name,
                    ]);
                    $itemObject->icon = ':thumbsup:';
                    $itemObject->color = '#228B22';
                    $itemObject->notifyAlert = _('Liked');
                }

                if ($itemActionValue == 'disliked') {
                    $simpleNotify = $notifyRepository->getCode(306, [
                        'username' => $user['username'],
                        'tuneTitle' => $itemObject->name,
                    ]);
                    $itemObject->icon = ':thumbsdown:';
                    $itemObject->color = '#D00000';
                    $itemObject->notifyAlert = _('Disliked');
                }

                if ($itemActionValue == 'same') {
                    $simpleNotify = $notifyRepository->getCode(307, [
                        'username' => $user['username'],
                        'tuneTitle' => $itemObject->name,
                    ]);
                    $itemObject->icon = ':eyes:';
                    $itemObject->color = '#1E90FF';
                    $itemObject->notifyAlert = _('Same');
                }

                $utm = '?utm_campaign=slackratingbot';
            }

            $itemObject->message = $simpleNotify['text'];
        }

        if ($itemType == 'post') {

        }

        $itemObject->url .= $utm;
        $itemObject->authorName = $user['username'];
        $itemObject->authorIcon = Config::get('base.siteurl') . $user['image'];

        return $itemObject;
    }

    private function prepareSlackMsg($itemObject)
    {
        $slackMessage = new stdClass;

        $slackMessage->message = $itemObject->message;
        $slackMessage->title = $itemObject->name;
        $slackMessage->titleLink = $itemObject->url;
        $slackMessage->fallback = $slackMessage->message . ' - ' . $slackMessage->titleLink;
        $slackMessage->authorName = $itemObject->authorName;
        $slackMessage->authorIcon = $itemObject->authorIcon;
        //$slackMessage->imageUrl = $itemObject->imageUrl;
        $slackMessage->thumbUrl = $itemObject->thumbUrl;
        $slackMessage->icon = $itemObject->icon;
        $slackMessage->color = $itemObject->color;

        $fields = [];
        if (!empty($itemObject->notifyAlert)) {
            $fields[] = [
                'title' => $itemObject->notifyTitle ? $itemObject->notifyTitle : _('Updated values'),
                'value' => $itemObject->notifyAlert,
                'short' => false,
            ];
        }

        if (isset($itemObject->likes)) {
            $fields[] = [
                'title' => _('Likes'),
                'value' => $itemObject->likes,
                'short' => true,
            ];
        }

        if (isset($itemObject->dislikes)) {
            $fields[] = [
                'title' => _('Dislikes'),
                'value' => $itemObject->dislikes,
                'short' => true,
            ];
        }

        if (isset($itemObject->weight)) {
            $fields[] = [
                'title' => _('Weight'),
                'value' => $itemObject->weight,
                'short' => true,
            ];
        }

        $slackMessage->fields = $fields;

        return $slackMessage;
    }

    public function slackHook()
    {
        if (!User::authId()) {

            return 'only auth user can access!';
        }

        $request = Request::createFromGlobals();
        $userRepo = $this->loadModel('user');
        $user = $userRepo->get(User::authId());

        $preText = strip_tags($request->request->get('preText'));
        $title = strip_tags($request->request->get('title'));
        $titleLink = $request->request->get('titleLink');

        $notifyType = $request->request->get('notifyType'); // info, success etc
        $notifyMessage = strip_tags($request->request->get('notifyMessage')); //
        $notifyTitle = strip_tags($request->request->get('notifyTitle')); // Updated fields
        $notifyAlert = strip_tags($request->request->get('notifyAlert')); // Weight from 50 to 55

        $icon = $request->request->get('icon');

        $itemId = $request->request->get('itemId');
        $itemType = $request->request->get('itemType'); //tune, post etc
        $itemAction = $request->request->get('itemAction'); //rate, update etc
        $itemActionValue = $request->request->get('itemActionValue'); // liked, disliked

        //$item = (object) json_decode($request->request->get('item'));

        $itemObject = $this->prepareToGlobalObject($itemId, $itemType, $itemAction, $itemActionValue);
        $itemObject->notifyAlert = $notifyAlert ? $notifyAlert : $itemObject->notifyAlert;
        $itemObject->icon = $icon ? $icon : $itemObject->icon;
        //$itemObject->notifyTitle = $notifyTitle ? $notifyTitle : null;
        $slackMessage = $this->prepareSlackMsg($itemObject);

        $this->sendSlackHook($slackMessage);
    }

    private function sendSlackHook($slackMessage)
    {
        $actionRepo = new ActionRepo;

        $attachments[] = [
            'fallback' => $slackMessage->fallback,
            'pretext' => isset($slackMessage->preText) ? $slackMessage->preText : _('Building better G-radio!') . ' - ' . Config::get('base.siteurl'),
            'color' => isset($slackMessage->color) ? $slackMessage->color : '#5bc0de',
            'author_name' => $slackMessage->authorName,
            'author_icon' => $slackMessage->authorIcon,
            'title' => $slackMessage->title,
            'title_link' => $slackMessage->titleLink,
            'fields' => $slackMessage->fields,
            'image_url' => isset($slackMessage->imageUrl) ? $slackMessage->imageUrl : null,
            'thumb_url' => $slackMessage->thumbUrl,
            'footer' => Config::get('site.title') . ' ' . _('messager bot for Slack'),
            'footer_icon' => Config::get('base.siteurl') . '/' . Config::get('paths.assets') . '/gradio-player/img/no-now-playing-art.png',
            'ts' => time(),
        ];

        /*
        [0] => Array
            (
                [fallback] => @rolla Atjaunotā dziesma John O'Callaghan feat. Sarah Howells - Find Yourself (Cosmic Gate remix)
                [color] => #5bc0de
                [author_name] => rolla
                [author_icon] => http://dev.gradio.lv/files/user_100/images/twitter/O8i1Fv2pdD.jpeg
                [title] =>
                [title_link] => http://dev.gradio.lv/tune/14460?utm_campaign=slackratingbot
                [fields] => Array
                    (
                        [0] => Array
                            (
                                [title] => John O'Callaghan feat. Sarah Howells - Find Yourself (Cosmic Gate remix)
                                [value] => DatePlayed no 2018-02-24T00:04:29+02:00 uz 2018-03-03T14:53:25+02:00 ArtistPlayed no 2018-02-24T00:04:29+02:00 uz 2018-03-03T14:53:25+02:00 CountPlayed no 28 uz 38 Weight no 72 uz 66 Publisher no Armada Various uz Armada Varioussss TdatePlayed no 2018-02-24T00:04:29+02:00 uz 2018-03-03T14:53:25+02:00 TartistPlayed no 2018-02-24T00:04:29+02:00 uz 2018-03-03T14:53:25+02:00
                                [short] => 1
                            )

                    )

                [footer] => Džī reidijō robots ziņotājs priekš Slack
                [footer_icon] => http://dev.gradio.lv/assets/gradio-player/img/no-now-playing-art.png
                [ts] => 1520085208
            )

            print_r($attachments);
            // exit;
        */

        $channel = Config::get('debug.enabled') ?
            Config::get('prefs.services.slack.devChannel') :
            Config::get('prefs.services.slack.channel');

        $icon = $slackMessage->icon ? $slackMessage->icon :
            Config::get('prefs.services.slack.icon');

        $actionRepo->slackHook(
            $slackMessage->message,
            Config::get('prefs.services.slack.webhook'),
            $channel,
            $icon,
            $attachments
        );
    }
}
