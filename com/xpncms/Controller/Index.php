<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use XpnCMS\View\View;

class Index extends Controller
{

    public function index()
    {
        $data = [
            'pageTitle' => _('Home'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

}
