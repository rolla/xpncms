<?php

namespace XpnCMS\Controller;

use JasonGrimes\Paginator;
use Core\Includes\Config;
use XpnCMS\Model\User;
use XpnCMS\Model\Content as ContentModel;
use XpnCMS\View\View;

class Content extends Controller
{
    public function index($id, $start = 0, $limit = 1)
    {
        global $uri;

        if (isset($uri['var']) && !empty($uri['var']) && is_numeric($uri['var'])) {
            $start = $uri['var'];
        }

        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $limit = $uri['var_1'];
        }

        if (!is_numeric($uri['method']) && $uri['method'] != 'index') {
            return $this->indexV2($uri['method']);
        }

        $content = $this->loadModel('content');
        $user = $this->loadModel('user');
        $userData = $user->get(User::authId());

        $pageTitle = _('All Content');

        if (is_numeric($id) && $id != 0) {
            $getContent = $content->getContent($id);
            $content = $getContent['content'][0];
            $pageTitle = _('Post') . ' : ' . $content['cont_name'];
            $contentText = strip_tags(html_entity_decode($content['cont_content']));
            $ogimage = urldecode(Config::get('base.fbsiteurl')
                . '/' . Config::get('paths.images') . '/'
                . $content['cont_imgs'])
            ;
            $contents = $getContent['content'];
        } elseif ($id == 'byid') {
            $getContentById = $content->getContentById($start, $limit);
            $pageTitle = _('Post') . ' : ' . $getContent['content'][0]['cont_name'];
            $contents = $getContentById['content'];
        } elseif (isset($uri['method']) && !empty($uri['method']) && $uri['method'] != 'index') {
            $getContentByAlias = $content->getContentByAlias($uri['method']);
            $pageTitle = _('Post') . ' : ' . $getContentByAlias['content'][0]['cont_name'];
            $contentText = strip_tags(html_entity_decode($getContentByAlias['content'][0]['cont_content']));
            $contents = $getContentByAlias['content'];
            $ogimage = urldecode(Config::get('base.fbsiteurl')
                . '/' . Config::get('paths.images') . '/' . $getContentByAlias['content'][0]['cont_imgs']);
        } else {
            $getAllContent = $content->getAllContent(0, 25);
            $pageTitle = _('All Content');
            $contents = $getAllContent['content'];
        }

        $gradioRepo = $this->loadPluginModel('gradio');

        $latestTuneVideos = $gradioRepo->getLatestVideos(6, true);
        $latestTunes = $gradioRepo->getLatestTunes(0, 6, true);
        $recentTunes = $gradioRepo->getRecentsongs(0, 3);
        $powerVideo = $gradioRepo->getPowerVideo();

        $data = [
            'pageTitle' => $pageTitle,
            'userData' => $userData,
            'contents' => $contents,
            'meta' => [
                'content' => $contentText,
                'ogurl' => str_replace('https', 'http', Config::get('base.fbsiteurl')) . '/content/' . $getContentByAlias['content'][0]['cont_alias'] . '/', // without https to work fb comments
                'ogimage' => $ogimage,
                'ogtitle' => $pageTitle,
                'ogtype' => 'article',
                'twittertitle' => $pageTitle,
                'twitterdescription' => $contentText,
                'twitterimage' => $ogimage,
            ],
            'latestTuneVideos' => $latestTuneVideos,
            'latestTunes' => $latestTunes,
            'recentTunes' => $recentTunes,
            'powerVideo' => $powerVideo,
        ];

        $this->loadView('content/index', $data);
    }

    private function indexV2($id)
    {
        if (FRONTEND_V2) {
            $viewModel = new View;
            $contentModel = new ContentModel;
            $post = $contentModel->getContentByAliasV2($id);
            $postMeta = $contentModel->getPostMeta($post);

            return $viewModel->loadViewV2('meta', ['meta' => $postMeta]);
        }
    }

    public function pageplain()
    {
        global $uri;

        $page = isset($uri['var']) ? (int)$uri['var'] : 1;

        $user = $this->loadModel('user');
        $userData = $user->get(User::authId());

        $content = $this->loadModel('content');

        $totalItems = $content->getAllCount();
        $urlPattern = '/content/page/(:num)';

        $paginator = new Paginator($totalItems, $content->perPage, $page, $urlPattern);

        $startAt = $content->perPage * ($page - 1);
        $getAllContent = $content->getAllContent($startAt, $content->perPage);

        $data = [
            'pageTitle' => 'Content',
            'userData' => $userData,
            'contents' => $getAllContent['content'],
            'page' => $page
        ];

        $this->loadView('content/plain', $data);
    }
}
