<?php

namespace XpnCMS\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;
use Exception;
use elFinder as EF;
use elFinderConnector;

define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', realpath('./' . DS) . DS);

// exit(APP_ROOT);

set_time_limit(0); // just in case it too long, not recommended for production
// error_reporting(0);
ini_set('max_file_uploads', 50); // allow uploading up to 50 files at once
// needed for case insensitive search to work, due to broken UTF-8 support in PHP
//ini_set('mbstring.internal_encoding', 'UTF-8');
ini_set('mbstring.func_overload', 2);

$conf = [];
$config_file = APP_ROOT . 'config' . DS . 'conf.php';
if (!file_exists($config_file)) {
    throw new Exception($config_file . ' cannot load!');
}
require_once $config_file;

$elfinder_path = APP_ROOT . $conf['settings']['paths']['public'] . DS . $conf['settings']['paths']['assets'] . DS . 'elfinder' . DS;

require_once $elfinder_path . 'php' . DS . 'elFinderConnector.class.php';
require_once $elfinder_path . 'php' . DS . 'elFinder.class.php';
require_once $elfinder_path . 'php' . DS . 'elFinderVolumeDriver.class.php';
require_once $elfinder_path . 'php' . DS . 'elFinderVolumeLocalFileSystem.class.php';
require_once $elfinder_path . 'php' . DS . 'elFinderVolumeMySQL.class.php';
require_once $elfinder_path . 'php' . DS . 'elFinderVolumeFTP.class.php';

class Elfinder extends Controller
{

    private $_opts = [];
    private $_path;
    private $_url;
    private $_alias;

    public function index()
    {
        global $debug, $conf;

        $session = new Session();

        $acl = new elFinderTestACL();
        $logger = new elFinderSimpleLogger(APP_ROOT . 'log' . DS . 'elfinder.log');

        if (isset($_REQUEST['user_id'])) {
            $user_id = 'user_' . $_REQUEST['user_id'];

            $this->_path = APP_ROOT . $conf['settings']['paths']['public'] . DS . $conf['settings']['paths']['files'] . DS . $user_id;
            $this->_url = $conf['settings']['base']['url'] . $conf['settings']['paths']['files'] . '/' . $user_id . '/';

            if (!file_exists($this->_path)) {
                $mkdir = mkdir($this->_path, 0777, true);

                if ($mkdir) {
                    $user_paths = $conf['settings']['paths']['user'];

                    foreach ($user_paths as $upath) {
                        $tmp_path = $this->_path . DS . $upath;
                        if (!file_exists($tmp_path)) {
                            mkdir($tmp_path, 0777, true);
                        }
                    }
                } else {
                    die('error creating directory');
                }
            }

            $this->_alias = 'files';

            if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'add_content_imgs') {
                $this->_path = APP_ROOT . $conf['settings']['paths']['public'] . DS . $conf['settings']['paths']['images'] . DS . 'content';

                if (!file_exists($this->_path)) {
                    $mkdir = mkdir($this->_path, 0775, true);
                }

                $this->_url = $conf['settings']['base']['url'] . $conf['settings']['paths']['images'] . '/' . 'content' . '/';
                $this->_alias = 'content';
            } elseif (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'add_category_imgs') {
                $this->_path = APP_ROOT . $conf['settings']['paths']['public'] . DS . $conf['settings']['paths']['images'] . DS . 'categories';

                if (!file_exists($this->_path)) {
                    $mkdir = mkdir($this->_path, 0775, true);
                }

                $this->_url = $conf['settings']['base']['url'] . $conf['settings']['paths']['images'] . '/' . 'categories' . '/';
                $this->_alias = 'categories';

                /*
                  if( !is_writable($path) ){


                  $html = '
                  <script>
                  var notify_msg = {code: 33, type: "error", title: "Error", alert: "Directory '.$path.' not writable", salert: "Directory not writable"};

                  xpnCMSNotify.Notify(notify_msg);

                  </script>';

                  echo $html;
                  exit;

                  }
                 */
            }

            //$res['path'] = $path;
            //$res['alias'] = $alias;
            //echo json_encode($res);
        } else {
            $user_id = null;
        }

        $this->_opts = [
            'locale' => 'en_US.UTF-8',
            'bind' => [
                // '*' => 'logger',
                'mkdir mkfile rename duplicate upload rm paste' => [$logger, 'log'],
            ],
            'debug' => true,
            'roots' => [
                [
                    'driver' => 'LocalFileSystem',
                    //'path'	   => '../../../files/user_1/',
                    //'startPath'  => '../files/test/',
                    //'URL'		 => dirname($_SERVER['PHP_SELF']) . '/../files/',
                    'URL' => $this->_url,
                    'path' => $this->_path,
                    //'path'		 => '../files',
                    //'startPath'  => $path,
                    //'URL'		   => dirname($_SERVER['PHP_SELF']).'/../../../files/'.$user_id,
                    //'URL'		   => $path,
                    // 'treeDeep'	=> 3,
                    //'separator' => DIRECTORY_SEPARATOR,
                    'alias' => $this->_alias,
                    'mimeDetect' => 'internal',
                    'tmbPath' => '.tmb',
                    'utf8fix' => true,
                    'tmbCrop' => false,
                    'tmbBgColor' => 'transparent',
                    'accessControl' => [$this, 'access'],
                    'acceptedName' => '/^[^\.].*$/',
                    // 'disabled' => array('extract', 'archive'),
                    // 'tmbSize' => 128,
                    'attributes' => [
                        [
                            'pattern' => '/\.js$/',
                            'read' => true,
                            'write' => false,
                        ],
                        [
                            'pattern' => '/^\/icons$/',
                            'read' => true,
                            'write' => false,
                        ],
                        [
                            'pattern' => '/^\/images$/',
                            'read' => true,
                            'write' => true,
                            'locked' => true,
                        ],
                        [
                            'pattern' => '/^\/music$/',
                            'read' => true,
                            'write' => true,
                            'locked' => true,
                        ],
                        [
                            'pattern' => '/^\/video$/',
                            'read' => true,
                            'write' => true,
                            'locked' => true,
                        ],
                        [
                            'pattern' => '/^\/private$/',
                            'read' => true,
                            'write' => true,
                            'locked' => true,
                        ],
                    ],
                // 'uploadDeny' => array('application', 'text/xml')
                ],
            // array(
            //	'driver'	 => 'LocalFileSystem',
            //	'path'		 => '../files2/',
            //	// 'URL'		=> dirname($_SERVER['PHP_SELF']) . '/../files2/',
            //	'alias'		 => 'File system',
            //	'mimeDetect' => 'internal',
            //	'tmbPath'	 => '.tmb',
            //	'utf8fix'	 => true,
            //	'tmbCrop'	 => false,
            //	'startPath'	 => '../files/test',
            //	// 'separator' => ':',
            //	'attributes' => array(
            //		array(
            //			'pattern' => '~/\.~',
            //			// 'pattern' => '/^\/\./',
            //			'read' => false,
            //			'write' => false,
            //			'hidden' => true,
            //			'locked' => false
            //		),
            //		array(
            //			'pattern' => '~/replace/.+png$~',
            //			// 'pattern' => '/^\/\./',
            //			'read' => false,
            //			'write' => false,
            //			// 'hidden' => true,
            //			'locked' => true
            //		)
            //	),
            //	// 'defaults' => array('read' => false, 'write' => true)
            // ),
            // array(
            //	'driver' => 'FTP',
            //	'host' => '192.168.1.38',
            //	'user' => 'dio',
            //	'pass' => 'hane',
            //	'path' => '/Users/dio/Documents',
            //	'tmpPath' => '../files/ftp',
            //	'utf8fix' => true,
            //	'attributes' => array(
            //		array(
            //			'pattern' => '~/\.~',
            //			'read' => false,
            //			'write' => false,
            //			'hidden' => true,
            //			'locked' => false
            //		),
            //
                //	)
            // ),
            /*
              array(
              'driver' => 'FTP',
              'host' => 'work.std42.ru',
              'user' => 'dio',
              'pass' => 'wallrus',
              'path' => '/',
              'tmpPath' => '../files/ftp',
              ),
             */
            // array(
            //	'driver' => 'FTP',
            //	'host' => '10.0.1.3',
            //	'user' => 'frontrow',
            //	'pass' => 'frontrow',
            //	'path' => '/',
            //	'tmpPath' => '../files/ftp',
            // ),
            // array(
            //	'driver'	 => 'LocalFileSystem',
            //	'path'		 => '../files2/',
            //	'URL'		 => dirname($_SERVER['PHP_SELF']) . '/../files2/',
            //	'alias'		 => 'Files',
            //	'mimeDetect' => 'internal',
            //	'tmbPath'	 => '.tmb',
            //	// 'copyOverwrite' => false,
            //	'utf8fix'	 => true,
            //	'attributes' => array(
            //		array(
            //			'pattern' => '~/\.~',
            //			// 'pattern' => '/^\/\./',
            //			// 'read' => false,
            //			// 'write' => false,
            //			'hidden' => true,
            //			'locked' => false
            //		),
            //	)
            // ),
            // array(
            //	'driver' => 'MySQL',
            //	'path' => 1,
            //	// 'treeDeep' => 2,
            //	// 'socket'			 => '/opt/local/var/run/mysql5/mysqld.sock',
            //	'user' => 'root',
            //	'pass' => 'hane',
            //	'db' => 'elfinder',
            //	'user_id' => 1,
            //	// 'accessControl' => 'access',
            //	// 'separator' => ':',
            //	'tmbCrop'		  => true,
            //	// thumbnails background color (hex #rrggbb or 'transparent')
            //	'tmbBgColor'	  => '#000000',
            //	'files_table' => 'elfinder_file',
            //	// 'imgLib' => 'imagick',
            //	// 'uploadOverwrite' => false,
            //	// 'copyTo' => false,
            //	// 'URL'	=> 'http://localhost/git/elfinder',
            //	'tmpPath' => '../filesdb/tmp',
            //	'tmbPath' => '../filesdb/tmb',
            //	'tmbURL' => dirname($_SERVER['PHP_SELF']) . '/../filesdb/tmb/',
            //	// 'attributes' => array(
            //	//	array(),
            //	//	array(
            //	//		'pattern' => '/\.jpg$/',
            //	//		'read' => false,
            //	//		'write' => false,
            //	//		'locked' => true,
            //	//		'hidden' => true
            //	//	)
            //	// )
            //
                // )
            ],
        ];

        //$debug->Display($this->_opts, 0, 1);

        if (User::authId() == $_REQUEST['user_id']) {
            $connector = new elFinderConnector(new EF($this->_opts), true);

            //$debug->Display($connector, 0, 1);

            $connector->run();
        } else {
            echo json_encode(['error' => 'access denied!']);
        }
    }

    /**
     * Simple function to demonstrate how to control file access using "accessControl" callback.
     * This method will disable accessing files/folders starting from  '.' (dot).
     *
     * @param string $attr attribute name (read|write|locked|hidden)
     * @param string $path file path relative to volume root directory started with directory separator
     *
     * @return bool|null
     * */
    public function access($attr, $path, $data, $volume)
    {
        return strpos(basename($path), '.') === 0        // if file/folder begins with '.' (dot)
            ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
            : null;                                    // else elFinder decide it itself
    }

    public function validName($name)
    {
        return strpos($name, '.') !== 0;
    }
}

/**
 * Simple logger function.
 * Demonstrate how to work with elFinder event api.
 *
 * @author Dmitry (dio) Levashov
 * */
class elFinderSimpleLogger
{

    /**
     * Log file path.
     *
     * @var string
     * */
    protected $file = '';

    /**
     * constructor.
     *
     * @author Dmitry (dio) Levashov
     * */
    public function __construct($path)
    {
        $this->file = $path;
        $dir = dirname($path);
        if (!is_dir($dir)) {
            mkdir($dir);
        }
    }

    /**
     * Create log record.
     *
     * @param string   $cmd      command name
     * @param array    $result   command result
     * @param array    $args     command arguments from client
     * @param elFinder $elfinder elFinder instance
     *
     * @return void|true
     *
     * @author Dmitry (dio) Levashov
     * */
    public function log($cmd, $result, $args, $elfinder)
    {
        $log = $cmd . ' [' . date('d.m H:s') . "]\n";

        if (!empty($result['error'])) {
            $log .= "\tERROR: " . implode(' ', $result['error']) . "\n";
        }

        if (!empty($result['warning'])) {
            $log .= "\tWARNING: " . implode(' ', $result['warning']) . "\n";
        }

        if (!empty($result['removed'])) {
            foreach ($result['removed'] as $file) {
                // removed file contain additional field "realpath"
                $log .= "\tREMOVED: " . $file['realpath'] . "\n";
            }
        }

        if (!empty($result['added'])) {
            foreach ($result['added'] as $file) {
                $log .= "\tADDED: " . $elfinder->realpath($file['hash']) . "\n";
            }
        }

        if (!empty($result['changed'])) {
            foreach ($result['changed'] as $file) {
                $log .= "\tCHANGED: " . $elfinder->realpath($file['hash']) . "\n";
            }
        }

        $this->write($log);
    }

    /**
     * Write log into file.
     *
     * @param string $log log record
     *
     * @author Dmitry (dio) Levashov
     * */
    protected function write($log)
    {
        if (($fp = @fopen($this->file, 'a'))) {
            fwrite($fp, $log . "\n");
            fclose($fp);
        }
    }
}

// END class

/**
 * Access control example class.
 *
 * @author Dmitry (dio) Levashov
 * */
class elFinderTestACL
{

    /**
     * make dotfiles not readable, not writable, hidden and locked.
     *
     * @param string               $attr   attribute name (read|write|locked|hidden)
     * @param string               $path   file path. Attention! This is path relative to volume root directory started with directory separator.
     * @param mixed                $data   data which seted in 'accessControlData' elFinder option
     * @param elFinderVolumeDriver $volume volume driver
     *
     * @return bool
     *
     * @author Dmitry (dio) Levashov
     * */
    public function fsAccess($attr, $path, $data, $volume)
    {
        if ($volume->name() == 'localfilesystem') {
            return strpos(basename($path), '.') === 0 ? !($attr == 'read' || $attr == 'write') : $attr == 'read' || $attr == 'write';
        }

        return true;
    }
}

// END class
