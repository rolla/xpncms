<?php

namespace XpnCMS\Controller;

use Core\Includes\Application;
use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use XpnCMS\View\View;

class Controller extends Application
{

    public function __construct()
    {
        $session = new Session();

        if (!$session->has('csrfToken')) {
            $csrfToken = bin2hex(openssl_random_pseudo_bytes(24));
            $session->set('csrfToken', $csrfToken);
        }
        $session->save();
    }

    public function loadModel($model)
    {
        $modelWithNameSpace = 'XpnCMS\Model\\' . ucwords(strtolower($model));

        return new $modelWithNameSpace();
    }

    public function loadPluginModel($model)
    {
        $modelWithNameSpace = 'XpnCMS\Plugins\\' . $model . '\\Model\\' . ucwords(strtolower($model));

        return new $modelWithNameSpace();
    }

    public function redirect($uri)
    {
        $request = Request::createFromGlobals();
        $furi = Config::get('base.url') . $uri;
        $ajax = '';
        if ($request->query->has('ajax')) {
            $ajax = '?ajax=true';
        }
        $furi = $furi . $ajax;
        header("Location: $furi");
    }

    /**
     *
     * @param  string $path
     * @return
     */
    protected function goTo($path)
    {
        $response = new RedirectResponse($path, 302);
        $response->send();
        return;
    }

    protected function loadView($view = false, $vars = '', $template = null, $ajax = false) {
        (new View)->loadView($view, $vars, $template, $ajax);
    }

    protected function loadPluginView($view = false, $vars = '', $template = null, $ajax = false) {
        (new View)->loadPluginView($view, $vars, $template, $ajax);
    }
}
