<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use XpnCMS\View\View;

class Categories extends Controller
{

    public function index()
    {
        $pageTitle = _('Categories');
        $data = [
            'pageTitle' => $pageTitle,
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }
}
