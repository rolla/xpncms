<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use SmartyException;
use XpnCMS\Exceptions\ConfigException;
use XpnCMS\View\View;

class Tunes extends Controller
{
    /**
     * @throws SmartyException
     * @throws ConfigException
     */
    public function index()
    {
        $data = [
            'pageTitle' => _('Tunes'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

    /**
     * @throws SmartyException
     * @throws ConfigException
     */
    public function recent()
    {
        $data = [
            'pageTitle' => _('Recent tunes'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

    /**
     * @throws SmartyException
     * @throws ConfigException
     */
    public function mostPlayed()
    {
        $data = [
            'pageTitle' => _('Most played tunes'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

    /**
     * @throws SmartyException
     * @throws ConfigException
     */
    public function latest()
    {
        $data = [
            'pageTitle' => _('Latest tunes'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

    /**
     * @throws SmartyException
     * @throws ConfigException
     */
    public function yourLikes()
    {
        $data = [
            'pageTitle' => _('Your likes'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

    /**
     * @throws SmartyException
     * @throws ConfigException
     */
    public function yourDislikes()
    {
        $data = [
            'pageTitle' => _('Your dislikes'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }
}
