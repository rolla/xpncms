<?php

namespace XpnCMS\Controller;

use Hybrid_authId;
use Hybrid_Endpoint;
use Core\Includes\Config;
use Hybridauth\HttpClient\Util;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;
use XpnCMS\Model\Notify;
use Exception;
use Hybridauth\Hybridauth;
use Hybridauth\Exception\Exception as HybridauthException;
use Core\Includes\Hybridauth\HybridauthWrapper;

class Auth extends Controller
{
    protected $userPhoto;

    private $blackListRefererPath = [
        'auth',
        'authv2',
    ];

    public static function notAllowed()
    {
        $notify = new Notify;
        $notifyCode = $notify->getCode(11);
        $controller = new Controller;
        $homeController = new Home;
        $indexData = [];
        $indexData = $homeController->indexData();
        $data = [
            'isAdmin' => false,
            'notify' => $notifyCode
        ];
        $finalData = array_merge($indexData, $data);
        $controller->loadView(Config::get('site.startpage'), $finalData);
        exit(); // important to stop other code execution
    }

    private function savePreviousReferer()
    {
        $request = Request::createFromGlobals();

        $referer = $request->headers->get('referer');
        $url = parse_url($referer);

        $blackListReferers = implode('|', $this->blackListRefererPath);
        $blackListRegex = "/$blackListReferers/";
        if (isset($url['path'])) {
            preg_match($blackListRegex, $url['path'], $blacklistFound);
        }

        if (!isset($blacklistFound[0])) {
            $session = new Session();
            $session->set('previousReferer', $referer);
            $session->save();
        }

        // if ($blacklistFound[0] === $this->blackListRefererPath) {
        //
        // }
    }

    public function connect($provider)
    {
        global $hybridauthConfig, $rbac;

        $this->savePreviousReferer();
        $provider = mb_strtolower($provider);

        try {
            $session = new Session();
            $userId = $session->get('userId');
            $previousReferer = $session->get('previousReferer');
            if ($userId && $previousReferer) {
                Util::redirect($previousReferer);
            }
            $session->set('authWithProvider', $provider);
            $session->save();

            (new HybridauthWrapper)->authenticate($provider);

        } catch (HybridauthException $e) {
            // if ($e->getCode() === 5 && strpos($e->getMessage(), 'denied') !== false) {
            //     $utmA = [
            //         'utm_source' => $_REQUEST['hauth_done'],
            //         'utm_medium' => 'social',
            //         'utm_campaign' => 'socialConnectAccessDenied',
            //     ];
            //     $this->redirect('?' . http_build_query($utmA));
            // }

            throw new HybridauthException("Ooophs, we got an error: " . $e->getMessage());
        }
    }

    public function disconnect($provider)
    {
        global $smarty;

        $provider = mb_strtolower($provider);

        $utm = '?';
        $utm .= 'utm_source=' . $provider;
        $utm .= '&utm_medium=social';
        $utm .= '&utm_campaign=socialDisconnect';

        $session = new Session();

        if (User::authId()) {
            $auth = $this->loadModel('auth');
            $userAuthentications = $auth->auths(User::authId());

            if (count($userAuthentications) <= 1) {
                $notifyObj = $this->loadModel('notify');
                $notify = $notifyObj->getCode(13);

                $data['notify'] = $notify;

                $session->set('smartyExtData', $data);
                $this->redirect('user/settings/' . $utm);
            } else {
                global $hybridauthConfig;
                $hybridauth = new Hybrid_authId($hybridauthConfig);
                $adapter = $hybridauth->getAdapter($provider);
                $adapter->logout($provider);
                //Hybrid_Provider_Adapter::logout();
                $auth = $this->loadModel('auth');
                $to_archive = $auth->to_archive(User::authId(), $provider);

                if ($to_archive) {
                    $this->redirect('user/settings/' . $utm);
                }
            }
        }
    }

    private function showErrorGoToHome()
    {
        $utmA = [
            'utm_source' => $_REQUEST['hauth_done'],
            'utm_medium' => 'social',
            'utm_campaign' => 'socialConnectError',
        ];
        $utm = '?' . http_build_query($utmA);

        if (array_key_exists('error_code', $_REQUEST) && !empty($_REQUEST['error_code'])) {
            echo '<h4>Sorry Error happen! ' . $_REQUEST['error_message'] . '</h4> <br><h2>go to home page <a href="/' . $utm . '">Click here</a></h2>';
        }
    }

    // public function hybrid()
    // {
    //     $hybridPath
    //         = APP_ROOT
    //         . 'vendor' . DS
    //         . 'hybridauth' . DS
    //         . 'hybridauth' . DS
    //         . 'hybridauth' . DS
    //         . 'Hybrid' . DS
    //     ;
    //     require_once $hybridPath . 'Auth.php';
    //     require_once $hybridPath . 'Endpoint.php';
    //
    //     $this->showErrorGoToHome();
    //
    //     try {
    //         Hybrid_Endpoint::process();
    //     } catch (Exception $e) {
    //         throw new Exception("Ooophs, we got an error: " . $e->getMessage());
    //     }
    //
    // }
}
