<?php

namespace XpnCMS\Controller;

use Emojione\Client;
use Emojione\Ruleset;
use XpnCMS\Controller\Controller;
use Core\Includes\Config;
use Emojione;

class Frontend extends Controller
{
    //public $database;

    public function getNav()
    {
        global $db1;

        $client = new Client(new Ruleset());
        $client->imageType = 'svg';
        //echo 'DEBUG!!';

        $db1->query('SELECT * FROM '.$db1->db_prefix.'categories '
            . 'WHERE cat_parent = :cat_parent ORDER BY cat_order LIMIT 0, 15');
        $db1->bind(':cat_parent', 0);
        $nav = $db1->resultset();

        //echo '<pre>';
        //print_r($nav);
        //echo '</pre>';
        $res = '';
        //$i = 0;
        if ($nav) {
            foreach ($nav as $k => $v) {
                //echo '<pre>';
                //print_r($v['cat_name']);
                //echo '</pre>';
                $res .= '<li data-catid="'.$v['cat_id'].'">'.PHP_EOL;
                if ($k == 0) {
                    $res .= '<a '
                        . 'href="'. BASE_URL . 'category/' . $v['cat_alias']
                        . 'data-catid="' . $v['cat_id'] . '" '
                        . 'class="ajax-nav home-nav">'
                        . '<img src="' . BASE_URL . 'img/logos/only-g-without-gloss-web-200.png" width="50%" />';
                } else {
                    $res .= '<a '
                        . 'href="' . BASE_URL . 'category/' . $v['cat_alias']
                        . 'data-catid="' . $v['cat_id'] . '" '
                        . 'class="ajax-nav">';
                }
                $res .= $client->toImage($v['cat_name']);
                $res .= '</a>'.PHP_EOL;
                $res .= '</li>'.PHP_EOL;
            }
        } else {
            $res['error'] = 'true';
            $res['reason'] = 'invalid ID';
        }

        return $res;
    }

    public function getIfAdminNav()
    {
        global $conf;

        $user = $this->loadModel('user');
        $base_url = $conf['settings']['plugins']['gradio']['prefs']['baseurl'];

        if ($user->checkClass(250)) {
            $header = '
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sys Tools <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="http://admin:Universal9550@home.urdt.lv/radio/gradio/utils/addmusic/" class="ext-saite" target="_blank">Add Music</a>
                    </li>
                    <li>
                        <a href="http://admin:Universal9550@'.$base_url.':8005/admin.cgi" class="ext-saite" target="_blank">Shoutcast</a>
                    </li>
                    <li>
                        <a href="http://admin:Universal9550@'.$base_url.':8000/admin.cgi" class="ext-saite" target="_blank">Shoutcast v2.0</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="http://admin:Universal9550@home.urdt.lv/radio/gradio/utils/sync/" class="ext-saite" target="_blank">Sync Covers</a>
                    </li>
                    <!--
                    <li>
                        <a href="http://server.gradio.lv/gradio/dev/plugins/DataTables/process/server_side/pipeline.html" class="ext-saite" target="_blank">Database</a>
                    </li>
                    -->
                    <li>
                        <a href="http://'.$base_url.'/phpmyadmin/" class="ext-saite" target="_blank">phpmyadmin</a>
                    </li>
                    <li>
                        <a href="https://'.$base_url.':10000" class="ext-saite" target="_blank">Webmin</a>
                    </li>
                    <li class="divider"></li>
                    <!--
                    <li>
                        <a href="http://urdt:Universal9550@'.$base_url.':8080" class="ext-saite" target="_blank">RR Moe\'s Tavern</a>
                    </li>
                    -->
                    <li>
                        <a href="http://airlive:Universal9550@home.urdt.lv:8080" class="ext-saite" target="_blank">RR URDT</a>
                    </li>

                </ul>
            </li>
            <li><a href="javascript:xpnCMS.openConsole();" class="">Console</a></li>
            ';

            return $header;
        }
    }

    public function getProfileRow()
    {
        global $rbac, $debug;

        $user = $this->loadModel('user');

        if ($user->checkUsr()) {
            $client = new Client(new Ruleset());
            $client->imageType = 'svg';

            //$debug->Display($user->userData, 0, 1);

            //echo $user->userData['photo_url'];
            //exit;

            $usr_img_path = Config::get('base.url') . Config::get('paths.files') . '/user_'.$user->userData['id'] . '/';

            //exit($usr_img_path);

            if (!preg_match('~^(?:f|ht)tps?://~i', $user->userData['photo_url'])) {
                //$photo_url = $usr_img_path.$user['photo_url'];
                //$users[$key]['photo_url'] = $photo_url;

                $photo_url = $usr_img_path.$user->userData['photo_url'];
            } else {
                $photo_url = $user->userData['photo_url'];
            }

            //$usr_img_path.$user->userData['photo_url']

            $html = '<div class="container-fluid profile-row-inner">';
            $html .= '<div class="col-md-4"></div>';
            $html .= '<div class="col-md-4"></div>';
            $html .= '<div class="col-md-4">
                        <div class="fast-user-settings">
                            <div class="user-small-pic pull-left"><img src="'.$photo_url.'" /></div>
            ';
            $html .= '      <div class="user-name pull-left">' . $client->toImage($user->userData['display_name']) . '</div>';

            $html .= '      <span class="fa fa-chevron-down user-chevron pull-left"></span>
                        </div>';

            $html .= '<div class="clearfix col-md-4 fast-user-settings-expand" style="display: none">
                <br />

                <a href="'.Config::get('base.url').'user/notifications/" class="btn btn-block btn-default ajax-nav">
                    <i class="fa fa-bullhorn"></i> '._('Notifications').'
                </a>

                <a href="'.Config::get('base.url').'user/settings/" class="btn btn-block btn-default ajax-nav">
                    <i class="fa fa-wrench"></i> '._('Settings').'
                </a>

                <a href="'.Config::get('base.url').'user/logout/" class="btn btn-block btn-danger">
                    <i class="fa fa-power-off"></i> '._('Logout').'
                </a>
            </div>';

            $html .= '</div>';
            $html .= '</div>';
        } else {
            $html = '<div class="container-fluid profile-row-inner">';
            $html .= '  <div class="col-md-4"></div>';
            $html .= '  <div class="col-md-4"></div>';
            $html .= '  <div class="col-md-2">';
            $html .= '      <div style="margin-top: 2px"><a href="'.Config::get('base.url').'user/login/" class="btn btn-default btn-block ajax-nav">'._('Login').'</a></div>';
            $html .= '  </div>';
            $html .= '</div>';
        }

        return $html;
    }

    public function getSocialIcons()
    {
        global $conf, $debug;

        $baseurl = $conf['settings']['base']['url'];
        $img = $conf['settings']['paths']['images'];
        $social = $conf['settings']['site']['social'];

        $html = '<div class="row">';
        $html .= '   <div class="social-icons pull-right">';

        $schema = '<span itemscope itemtype="http://schema.org/Organization" style="display:none">
                    <link itemprop="url" href="' . $baseurl . '">';

        if ($social['facebook']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://facebook.com/'.$social['facebook']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/facebook.png" alt="facebook"></a>';
            $schema .= '<a itemprop="sameAs" href="http://facebook.com/' . $social['facebook']['profile_url'] . '">Facebook</a>';
            $html .= '</div>';
        }

        if ($social['twitter']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://twitter.com/'.$social['twitter']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/twitter.png" alt="twitter"></a>';
            $schema .= '<a itemprop="sameAs" href="http://twitter.com/' . $social['twitter']['profile_url'] . '">Twitter</a>';
            $html .= '</div>';
        }

        if ($social['google']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://google.lv/'.$social['google']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/google.png" alt="google"></a>';
            $schema .= '<a itemprop="sameAs" href="http://google.com/' . $social['google']['profile_url'] . '">Google</a>';
            $html .= '</div>';
        }

        if ($social['googleplus']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://plus.google.com/'.$social['googleplus']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/googleplus.png" alt="googleplus"></a>';
            $schema .= '<a itemprop="sameAs" href="http://plus.google.com/' . $social['googleplus']['profile_url'] . '">Google Plus</a>';
            $html .= '</div>';
        }

        //$html .= '<div class="fa fa-google"></div>';

        if ($social['draugiem']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://draugiem.lv/'.$social['draugiem']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/draugiem.png" alt="draugiem"></a>';
            $schema .= '<a itemprop="sameAs" href="http://draugiem.lv/' . $social['draugiem']['profile_url'] . '">Draugiem</a>';
            $html .= '</div>';
        }

        if ($social['youtube']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://youtube.com/'.$social['youtube']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/youtube.png" alt="youtube"></a>';
            $schema .= '<a itemprop="sameAs" href="http://youtube.com/' . $social['youtube']['profile_url'] . '">Youtube</a>';
            $html .= '</div>';
        }

        if ($social['mixcloud']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://mixcloud.com/'.$social['mixcloud']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/mixcloud.png" alt="mixcloud"></a>';
            $schema .= '<a itemprop="sameAs" href="http://mixcloud.com/' . $social['mixcloud']['profile_url'] . '">Mixcloud</a>';
            $html .= '</div>';
        }

        if ($social['soundcloud']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://soundcloud.com/'.$social['soundcloud']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/soundcloud.png" alt="soundcloud"></a>';
            $schema .= '<a itemprop="sameAs" href="http://soundcloud.com/' . $social['soundcloud']['profile_url'] . '">Soundcloud</a>';
            $html .= '</div>';
        }

        if ($social['lastfm']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://lastfm.com/user/'.$social['lastfm']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/lastfm.png" alt="lastfm"></a>';
            $schema .= '<a itemprop="sameAs" href="http://lastfm.com/user/' . $social['lastfm']['profile_url'] . '">Last.fm</a>';
            $html .= '</div>';
        }

        if ($social['foursquare']['show']) {
            $html .= '<div class="pull-left">';
            $html .= '  <a href="http://foursquare.com/'.$social['foursquare']['profile_url'].'" target="_blank"><img src="'.$baseurl.$img.'/icons/foursquare.png" alt="foursquare"></a>';
            $html .= '</div>';
        }

        $schema .= '</span>';
        $html .= $schema;

        $html .= '  </div>';
        $html .= '</div>';

        return $html;
    }

    public function prepareToHTML($data = [])
    {
        //return $res;
    }
}
