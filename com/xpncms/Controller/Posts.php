<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use XpnCMS\View\View;

class Posts extends Controller
{

    public function index()
    {
        $data = [
            'pageTitle' => _('All posts'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

}
