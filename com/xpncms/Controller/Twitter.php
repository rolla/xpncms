<?php


namespace XpnCMS\Controller;

use Emojione\Client;
use Emojione\Ruleset;
use TwitterAPIExchange;
use Core\Helpers\Time;
use XpnCMS\Controller\Controller;
use Emojione;

class Twitter extends Controller
{
    public function __construct()
    {
        //parent::__construct();
        //$this->index();
        //$user = $this->loadModel("user");
    }

    public function index()
    {
        //$this->redirect('music/songs');
    }

    public function search($search)
    {
        global $debug;
        require_once APP_ROOT . 'vendor' . DS . 'j7mbo' . DS . 'twitter-api-php' . DS . 'TwitterAPIExchange.php';
        $client = new Client(new Ruleset());
        $client->imageType = 'svg';

        //$blocked = array('spoken44');

        $settings = [
            'oauth_access_token' => '480426949-IkvNf2bigCcMsxWX2RbeIWCxXOgy0u6j3BS8I0FF',
            'oauth_access_token_secret' => '4irS1Ai0zGTuhnXbosT2C1vVbQtNoc25ruBojPUrUQbyb',
            'consumer_key' => 'a6W06J1MjSD6vgqFTFavoIB8w',
            'consumer_secret' => 'PZFPEGhuHp228FY0GLGpFqptSCrGMUzBZBCt5srPf7JiORkq4Z',
        ];

        //$debug->Display($search, 0 ,1);
        //$def_search = '?q=@gradiolv OR #gradio OR gradio.lv OR cmpl OR gradio&result_type=mixed&count=50';
        $def_search = '@gradiolv OR #gradio OR gradio.lv OR cmpl OR gradio';

        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        if (!isset($search)) {
            //$getfield = '?q=%40gradiolv%20OR%20%23gradio%20OR%20gradio%20OR%20gradio.lv&result_type=mixed&count=20';
            //$getfield = '?q=%40gradiolv%20OR%20%23gradio%20OR%20gradio.lv%20OR%20gradio%20OR%20cmpl&result_type=mixed&count=50&geocode=56.8366269,25.1495716,5000km';
            $getfield = '?q='.urlencode($def_search).'';
        } else {
            //$search = $search.'&result_type=mixed&count=15';
            $search = $search;
            $getfield = '?q='.urlencode($search).'';
        }
        //$getfield = '?q=#tiesto&count=100';
        $requestMethod = 'GET';

        $twitter = new TwitterAPIExchange($settings);
        $response = $twitter->setGetfield($getfield)
                    ->buildOauthId($url, $requestMethod)
                   ->performRequest();

        //$debug->Display($response, 0 ,1);

        if ($response) {
            $time = new Time();

            $data = json_decode($response);

            foreach ($data->statuses as $key => $value) {
                $tweets[$key]['tweet']['id'] = $value->id_str;
                $tweets[$key]['tweet']['created_at'] = $time->relativeTime($value->created_at);
                $tweets[$key]['tweet']['text'] = $this->makeLinks($client->toImage($value->text));
                $tweets[$key]['tweet']['screen_name'] = $value->user->screen_name;
                $tweets[$key]['tweet']['time_zone'] = $value->user->time_zone;
            }

            //echo '<pre>';
          //print_r($tweets);

            $json['tweets'] = $tweets;

            header('Cache-Control: no-cache, must-revalidate');

            echo json_encode($json);
        }
    }

    public function makeLinks($text)
    {
        $text = preg_replace('%(((f|ht){1}tp://)[-a-zA-^Z0-9@:\%_\+.~#?&//=]+)%i',
    '<a href="\\1" target="_blank">\\1</a>', $text);
        $text = preg_replace('%([[:space:]()[{}])(www.[-a-zA-Z0-9@:\%_\+.~#?&//=]+)%i',
    '\\1<a href="http://\\2" target="_blank">\\2</a>', $text);

        return $text;
    }
}

    //$get = new Get;
    //$Songs = new Songs;

