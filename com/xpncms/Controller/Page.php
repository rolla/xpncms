<?php

namespace XpnCMS\Controller;

class Page extends Controller
{
    public function help()
    {
        $this->loadView('pages/help');
    }

    public function error()
    {
        $this->loadView('pages/error');
    }
}
