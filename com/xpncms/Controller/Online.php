<?php

namespace XpnCMS\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;

class Online extends Controller
{
    public function __construct()
    {
        global $uri, $rbac, $debug;

        $currentuserId = User::authId();

        // Get Role Id
        $roleId = $rbac->Roles->returnId('gradio');

        // Make sure User has 'forum_user' Role
        $allowed = 0;
        $allowed = $rbac->Users->hasRole($roleId, $currentuserId);
        if ($allowed) {
            return true;
        }

        Auth::notAllowed();
    }

    public function index()
    {
        $currentuserId = User::authId();

        $user = $this->loadModel('user');
        $userData = $user->get($currentuserId);

        $online = $this->loadModel('online');
        $users = $online->getOnlineUsers();
        $guests = $online->getOnlineGuests();

        $pageTitle = _('Users online');
        $data =  compact(
            'pageTitle',
            'userData',
            'users',
            'guests'
        );

        $this->loadView('user/online', $data);
    }
}
