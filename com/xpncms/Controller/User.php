<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use XpnCMS\Model\User as UserModel;
use XpnCMS\Model\Auth;
use Core\Includes\Hybridauth\HybridauthWrapper;

class User extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
    }

    public function login()
    {
        $session = new Session();
        $user = $this->loadModel('user');
        $currentUserId = $user->authId();

        if ($currentUserId) {
            $this->redirect('user/settings');
        } else {
            $session->remove('user');
            //session_destroy();
        }

        $data = ['pageTitle' => _('User login')];

        $this->loadView('user/login', $data);
    }

    private function deleteUserSession()
    {
        $session = new Session();

        $session->remove('user');
        $session->remove('userId'); // deprecated
        $session->remove('online');
        $session->remove('currentActivity');

        $session->remove('timeline');
        $session->remove('csrfToken');
        $session->remove('provider');
        $session->remove('providerUid');
        $session->remove('authWithProvider');

        //$session->remove('lastActivity');
    }

    public function logout()
    {
        global $action;

        $hybridauth = HybridauthWrapper::getInstance();
        $providers = $hybridauth->getConnectedProviders();
        $provider = mb_strtolower($providers[0]);

        $utm = '?';
        $utm .= 'utm_source=' . $provider;
        $utm .= '&utm_medium=social';
        $utm .= '&utm_campaign=socialLogout';

        // Disconnect the adapter
        $hybridauth->disconnectAllAdapters();

        $addHumanAction = $action->addHumanAction('loggedout', 'site', null, null);
        $result['addHumanAction'] = $addHumanAction;
        //$user->lastVisit(true); // update lastvisit timestamp

        $this->deleteUserSession();

        // go back home
//        $this->redirect('user/login' . $utm);

        /* redirect to utils to logout */
        $utilsUrl = Config::get('plugins.gradio.prefs.utilsurl');
        $url = $utilsUrl . 'auth/logout?' . $utm;
        header("Location: $url");
    }

    public function settings()
    {
        global $rbac, $uri, $debug;

        $user = $this->loadModel('user');
        $gradioRepo = $this->loadPluginModel('gradio');
        $powerVideo = $gradioRepo->getPowerVideo();
        $currentUserId = $user->authId();

        if ($uri['var'] == 'edit') {
            return $this->editSettings();
        }

        if (!$currentUserId) {
            $this->redirect('user/login');
        }

        // load user and authentication models
        $auth = $this->loadModel('auth');

        // get the user data from database
        $userData = $user->get($currentUserId);

        $userRoles = $rbac->Users->allRoles($currentUserId);

        $uroles = '';
        if ($userRoles) {
            foreach ($userRoles as $role) {
                $uroles .= $role['Description'].', ';
            }
        }

        $userData['roles'] = $uroles;
        //echo $debug->display($userData);

        $usr_img_path = Config::get('base.url') . Config::get('paths.files') . '/user_'.$userData['id'] . '/';

        if (!preg_match('~^(?:f|ht)tps?://~i', $userData['photo_url'])) {
            $photo_url = $usr_img_path.$userData['photo_url'];
            $userData['photo_url'] = $photo_url;
        }

        // provider like twitter, linkedin, do not provide the user email
        // in this case, we should ask them to complete their profile before continuing
        //if( ! $userData["email"] ){
            //$this->redirect( "users/complete_registration" );
        //}

        // get the user authentication info from db, if any
        $user_authentications = $auth->auths($currentUserId);

        //$user->currentVisit($_SESSION["user"]);
        //$user->userVisits();

        // load profile view
        $data = [
            'pageTitle' => _('User settings'),
            'userData' => $userData,
            'user_authentications' => $user_authentications,
            'powerVideo' => $powerVideo,
        ];
        $this->loadView('user/settings', $data);
    }

    public function profile()
    {
        global $rbac, $uri, $action;

        $user = $this->loadModel('user');
        $currentUserId = $user->authId();

        if ($uri['var'] == 'edit') {
            return $this->editSettings();
        }

        if (!$currentUserId) {
            $this->redirect('user/login');
        }

        // load user and authentication models
        $auth = $this->loadModel('auth');

        // get the user data from database
        $userData = $user->get($currentUserId);

        $userRoles = $rbac->Users->allRoles($currentUserId);

        $uroles = '';
        if ($userRoles) {
            foreach ($userRoles as $role) {
                $uroles .= $role['Description'].', ';
            }
        }

        $userData['roles'] = $uroles;
        $usr_img_path = Config::get('base.url') . Config::get('paths.files') . '/user_'.$userData['id'] . '/';

        if (!preg_match('~^(?:f|ht)tps?://~i', $userData['photo_url'])) {
            $photo_url = $usr_img_path.$userData['photo_url'];
            $userData['photo_url'] = $photo_url;
        }

        // get the user authentication info from db, if any
        $user_authentications = $auth->auths($currentUserId);

        $userActions = $user->getActionsIds($currentUserId, 0, 200);

        //echo count($userActions);
        //exit;

        $humanActions = $action->getActionsByIds($userActions);

        // load profile view
        $data = [
            'pageTitle' => _('User settings'),
            'userData' => $userData,
            'user_authentications' => $user_authentications,
            'actions' => $humanActions
        ];

        /*
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        exit;
         *
         */

        $this->loadView('user/profile', $data);
    }

    public function editSettings()
    {
        $user = $this->loadModel('user');
        $currentUserId = $user->authId();

        $auth = $this->loadModel('auth');

        $userData = $user->get($currentUserId);
        $user_authentications = $auth->auths($currentUserId);

        $data = [
            'pageTitle' => _('Edit user settings'),
            'userData' => $userData,
            'user_authentications' => $user_authentications
        ];

        $this->loadView('user/settings_edit', $data);
    }

    public function updateuser()
    {
        $session = new Session();
        $user = $this->loadModel('user');
        $currentUserId = $user->authId();
        $providerUid = $session->get('providerUid');

        $data = $_POST;

        if (isset($data['pk']) && !empty($data['pk'])) {
            $id = $data['pk'];
            unset($data['pk']);

            $data[$_POST['name']] = $_POST['value'];
            unset($data['name']);
            unset($data['value']);

            $data['updated'] = date('Y-m-d H:i:s');

            if ($id == $currentUserId) {
                $user = $this->loadModel('user');
                $update = $user->updateUser($data, $id, $providerUid);

                if ($update) {
                    $result['type'] = 'success';
                    $result['id'] = $id;
                }
            } else {
                $result['type'] = 'error';
                $result['msg'] = 'cannot allowed to edit!';
            }

            echo json_encode($result);
        }
    }

    public function setNotifyAboutNewProfilePic()
    {
        $session = new Session();
        $response = new Response();
        $request = Request::createFromGlobals();

        if ($request->getMethod() != 'POST') {

            return;
        }

        $choose = $request->request->get('choose');
        $newProfilePicAvailable = $request->request->get('newProfilePicAvailable');
        $authRepo = $this->loadModel('auth');
        $userRepo = $this->loadModel('user');

        $currentUserId = $userRepo->authId();
        $provider = $authRepo->findByProviderUid($session->get('providerUid'));

        if ($choose == 'cancel') {
            $userPhoto = $authRepo->saveImage($currentUserId, $provider['provider'], $newProfilePicAvailable);
            $cookie = new Cookie('notifyAboutProfilePic', $choose, time()+2592000); // 1 month
            $response->headers->setCookie($cookie);
            $response->send();
        }

        if ($choose == 'confirm') {
            $userPhoto = $authRepo->saveImage($currentUserId, $provider['provider'], $newProfilePicAvailable);
            $userRepo->updatePhoto($currentUserId, $userPhoto['storename']);

            $authRepo->updateProvider(['picture' => $newProfilePicAvailable], $provider['id']);
            $response->headers->clearCookie('notifyAboutProfilePic');
            $response->setContent(json_encode(['updated' => true]));
            $response->headers->set('Content-Type', 'application/json');
            $response->send();
        }
    }

    public function pd36aNUQNxUNRhYSnT0()
    {
        $request = Request::createFromGlobals();
        if ($request->getMethod() != 'POST') {

            return;
        }

        $userContent = json_decode($request->getContent(), true);

        $session = new Session();

        $session->set('userId', $userContent['id']); // deprecated
//        $user = [
//            'id' => 102,
//            'latest_provider' => NULL,
//            'username' => "edphoto",
//            'display_name' => "EdgarsLV",
//            'description' => "Need Photo?",
//            'gender' => "male",
//            'language' => "",
//            'country' => "",
//            'region' => "Liepaja",
//            'city' => "",
//            'zip' => "",
//            'website_url' => "",
//            'profile_url' => "https://www.facebook.com/edphoto.lv",
//            'photo_url' => "images/facebook/SwMUU2ijgR.jpeg",
//            'ip' => "46.109.129.165",
//            'visits' => 313,
//            'currentvisit' => "2022-11-18 18:25:20",
//            'lastvisit' => "2022-11-18 18:21:48",
//            'updated' => "2022-11-18 18:25:35",
//            'added' => "2014-04-04 18:38:16",
//            'ban' => 0,
//            'api_token' => "1|plHkRbIdZbJl27o10cr59gCNpuboq0QIrAxn5r4C",
//            'exists' => true,
//            'filesPath' => "/files/user_102/",
//            'image' => "/files/user_102/images/facebook/SwMUU2ijgR.jpeg",
//        ];
        $session->set('user', $userContent);

//        dd($userContent);
    }
}
