<?php

namespace XpnCMS\Controller;

use Core\Includes\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use XpnCMS\Model\User;

class Alive extends Application
{

    public function __construct()
    {
        // we need empty to avoid parent::__construct
    }

    public function index()
    {
        $user = new User();
        $request = Request::createFromGlobals();

        if (!$request->query->get('initiator') ||
            $request->query->get('initiator') == 'user') { // only user clicks register
            $user->currentVisit();
        }

        $result = [
            'status' => 'success',
            'response' => [
                'alive' => true
            ]
        ];

        $response = new JsonResponse();
        $response->setData($result);
        $response->send();
    }
}
