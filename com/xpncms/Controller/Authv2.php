<?php

namespace XpnCMS\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\Auth;
use XpnCMS\Model\User;
use Core\Includes\Config;
use Core\Includes\Hybridauth\HybridauthWrapper;
use PhpRbac\Rbac;
use Exception;

class Authv2 extends Controller
{

    /* referer location */
    private $location;

    public function callback()
    {
        $session = new Session();
        $provider = $session->get('authWithProvider');
        $session->save();

        try {
            $userProfile = (new HybridauthWrapper)->getUserProfile($provider);

            $this->logInOrCreate($provider, $userProfile);
        } catch (Exception $e) {
            throw new Exception("Ooophs, we got an error: " . $e->getMessage());
        }
    }

    private function logInOrCreate($provider, $userProfile)
    {
        $session = new Session();
        $request = Request::createFromGlobals();
        $cookies = $request->cookies;

        $authModel = new Auth;
        $userModel = new User;
        $userId = $userModel->authId();

        $utm = [
            'utm_source' => $provider,
            'utm_medium' => 'social',
            'utm_campaign' => 'socialConnect',
        ];

        $referer = $session->get('previousReferer');
        $session->save();

        $this->location = $referer . '?' . http_build_query($utm);

        # check if user already have authenticated using this provider before
        $authInfoFound = $authModel->findByProviderUid($provider, $userProfile->identifier);

        # if auth exists in the database, then we set the user as connected and redirect him to his profile page
        if ($authInfoFound) {
            $this->createFoldersAndSetOnline($authInfoFound['user_id'], $userProfile, $provider);

            /*
            $this->createUserFoldersAndSaveImage($authInfoFound['user_id'], $userProfile, $provider);
            $this->setOnline($authInfoFound['user_id'], $userProfile->identifier, $provider);
            */

            $this->goTo($this->location);
        }

        /* if user logged in add to connections, else create new user and add connection */
        $newUserId = $userId ?: $userModel->saveNewUser($userProfile);
        $authModel->addProvider($newUserId, $provider, $userProfile);

        $this->createFoldersAndSetOnline($newUserId, $userProfile, $provider);

        // $newUser = $userModel->get($newUserId);
        // $this->createUserFoldersAndSaveImage($newUser, $userProfile, $provider);
        // $this->setOnline($newUser, $userProfile->identifier, $provider);
        // $userModel->lastVisit(true);

        $this->goTo($this->location);
    }

    private function createFoldersAndSetOnline($userId, $userProfile, $provider)
    {
        $userModel = new User;
        $newUser = $userModel->get($userId);
        $this->createUserFoldersAndSaveImage($newUser, $userProfile, $provider);

        // after setting new image we need refresh user infor for session
        $newUser = $userModel->get($userId);
        $this->setOnline($newUser, $userProfile->identifier, $provider);
        $userModel->lastVisit(true);
    }

    private function setOnline($newUser, $identifier, $provider)
    {
        $session = new Session();
        $session->set('userId', intval($newUser['id'])); // deprecated
        $session->set('user', $newUser);
        $session->set('providerUid', $identifier);
        $session->set('provider', $provider);
        $session->set('online', 1);
        $session->set('lastActivity', time());
        $session->save();
    }

    private function createUserFoldersAndSaveImage($newUser, $userProfile, $provider)
    {
        $request = Request::createFromGlobals();
        $cookies = $request->cookies;

        $userModel = new User;
        $authModel = new Auth;

        $userId = $newUser['id'];
        $createUserFolders = $authModel->createUserFolders($userId, Config::get('userFolders'));

        $userFilesPath = APP_ROOT . Config::get('paths.public') . DS .
            Config::get('paths.files') . DS . 'user_' . $userId . DS;

        if (!is_file($userFilesPath . $newUser['photo_url'])) {
            $userPhoto = $authModel->saveImage($userId, $provider, $userProfile->photoURL);
            $userModel->updatePhoto($userId, $userPhoto['storename'], $userProfile->identifier);
        }

        $matchUserPictures = $authModel->matchUserPictures(
            $userId,
            $provider,
            $newUser['photo_url'],
            $userProfile->photoURL
        );

        if (!$cookies->get('notifyAboutProfilePic', null) && !$matchUserPictures) {
            $notifyObj = $this->loadModel('notify');
            $modalNotify = $notifyObj->getCode(308, [
                'username' => $newUser['username'],
            ]);
            $newProfilePicAvailable = $userProfile->photoURL;
            $session = new Session();
            $session->set('flashNotify', compact('modalNotify', 'newProfilePicAvailable'));
            $session->save();
        }
    }
}
