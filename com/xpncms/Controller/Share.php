<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;

class Share extends Controller
{
    public function index()
    {
        global $uri;
        $request = $_GET;

        $utm = '?';
        $utm .= 'utm_source=' . $uri['method'];
        $utm .= '&utm_medium=social';
        $utm .= '&utm_campaign=2016';
        
        // remove old query params of specified, maybe need clever solution
        $fullURL = strtok($request['linkurl'], '?') . $utm;

        echo json_encode(['longURL' => $fullURL, compact('request')]);
    }
}
