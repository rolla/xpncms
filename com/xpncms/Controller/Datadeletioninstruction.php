<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use XpnCMS\View\View;

class Datadeletioninstruction extends Controller
{

    public function index()
    {
        $pageTitle = _('Data deletion instruction');
        $data = [
            'pageTitle' => $pageTitle,
            'meta' => [
                'ogtitle' => $pageTitle,
                'twittertitle' => $pageTitle,
            ]
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }
}
