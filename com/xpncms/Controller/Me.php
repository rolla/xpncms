<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;
use XpnCMS\View\View;

class Me extends Controller
{

    public function index()
    {
        $data = [
            'pageTitle' => _('Me'),
        ];

        (new View)->loadViewV2(Config::get('site.startpage'), $data);
    }

}
