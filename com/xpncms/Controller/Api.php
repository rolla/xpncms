<?php

namespace XpnCMS\Controller;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use XpnCMS\Model\Content;
use XpnCMS\Model\Category;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use XpnCMS\Model\Auth;
use XpnCMS\Model\Notify;
use XpnCMS\Model\User;
use XpnCMS\Plugins\gradio\Model\Gradio;
use XpnCMS\Plugins\tune\Model\Tune;
use Core\Includes\Action;

class Api extends Controller
{

    /**
     *
     * @var User
     */
    private $userRepo;

    /**
     *
     * @var Action
     */
    private $actionRepo;

    /**
     *
     * @var Content
     */
    private $contentRepo;

    /**
     *
     * @var Category
     */
    private $categoryRepo;

    /**
     *
     * @var Gradio
     */
    private $gradioRepo;

    public function __construct()
    {
        $this->userRepo = new User;
        $this->actionRepo = new Action;
        $this->contentRepo = new Content;
        $this->categoryRepo = new Category;
        $this->gradioRepo = new Gradio;
    }

    public function index()
    {
        exit('woila');
    }

    /**
     * @param $var
     */
    public function v2($var)
    {
        if (method_exists($this, $var)) {
            return $this->{$var}();
        }

        return $this->index();
    }

    /**
     * Limit api to not be able to fetch unlimited rows
     * @param  int $limit
     * @return int
     */
    private function maxLimit(int $limit): int
    {
        $maxLimit = 500;

        return $limit > $maxLimit ? 100 : $limit;
    }

    /**
     * @throws Exception
     */
    private function post(): Response
    {
        global $uri;

        $uri = (object)$uri;

        $post = $this->contentRepo->getContentByAliasV2($uri->var_1);
        $meta = $this->contentRepo->getPostMeta($post);
        $results = compact('post', 'meta');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     *
     */
    private function posts(): Response
    {
        $request = Request::createFromGlobals();

        $start = intval($request->get('start', 0));
        $limit = $this->maxLimit(intval($request->get('limit', 6)));
        $random = $request->get('random', false) === 'true';

        $posts = $this->contentRepo->getAllContentV2($start, $limit, $random);
        $results = compact('posts');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     *
     */
    private function me(): Response
    {
        global $uri;

        $request = Request::createFromGlobals();

        $start = intval($request->get('start', 0));
        $limit = $this->maxLimit(intval($request->get('limit', 6)));
        $lastId = intval($request->get('lastId', null));
        $uri = (object)$uri;

        $user = $this->userRepo->auth();

        $results = null;
        switch($uri->var_1) {
            case 'activities':
                $activities = isset($user->id) ? $this->actionRepo->getActivities($user->id, $start, $limit) : null;
                $results['activities'] = $activities;
            break;

            case 'freshactivities':
                $activities = isset($user->id) ? $this->actionRepo->getFreshActivities($user->id, $lastId) : null;
                $results['activities'] = $activities;
            break;

            default:
                $authModel = new Auth;
                $session = new Session();
                if (!$user) {
                    $user = (object)[];
                }
                $results['session'] = $session->all();
                $session->save();

                $user->auths = $authModel->auths($user->id);
                $user->roles = $this->userRepo->roles();
                $results['user'] = $user;
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    private function meupdate(): Response
    {
        $request = Request::createFromGlobals();

        $this->userRepo->updateUserV2($request->request->all());

        return $this->me();
    }

    /**
     *
     */
    private function tune(): Response
    {
        global $uri;

        $uri = (object)$uri;

        $tuneModel = new Tune;
        $tune = $tuneModel->getTune(intval($uri->var_1));
        $meta = $tuneModel->getTuneMeta($tune);

        $results = compact('tune', 'meta');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    private function tunesave(): Response
    {
        $request = Request::createFromGlobals();
        $payload = $request->request->all();
        $tuneModel = new Tune;
        $savedTune = $tuneModel->saveTuneV2($payload);

        $results = compact('savedTune');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     * @return bool|JsonResponse
     */
    private function checkAuth()
    {
        if (! User::auth()) {
            $notify = (new Notify)->getCode(11);
            $results = compact('notify');

            $jsonResponse = new JsonResponse();
            $jsonResponse->setData(compact('results'));
            $jsonResponse->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $jsonResponse->send();
        }

        return true;
    }

    /**
     *
     */
    private function tunes(): Response
    {
        global $uri;

        $request = Request::createFromGlobals();

        $start = intval($request->get('start', 0));
        $limit = $this->maxLimit(intval($request->get('limit', 6)));
        $random = $request->get('random', false) === 'true';
        $uri = (object)$uri;

        $results = null;
        switch($uri->var_1) {
            case 'recent':
                $recentTunes = $this->gradioRepo->getRecentsongs($start, $limit);
                $results = compact('recentTunes');
                break;

            case 'most':
                $mostPlayedTunes = $this->gradioRepo->getMostPlayedTunes($start, $limit);
                $results = compact('mostPlayedTunes');
                break;

            case 'liked':
                $this->checkAuth();
                $likedTunes = $this->gradioRepo->getUserLikedTunes($start, $limit, $random);
                $results = compact('likedTunes');
                break;

            case 'disliked':
                $this->checkAuth();
                $dislikedTunes = $this->gradioRepo->getUserDislikedTunes($start, $limit, $random);
                $results = compact('dislikedTunes');
                break;

            default:
                $tunes = $this->gradioRepo->getLatestTunes($start, $limit, $random);
                $results = compact('tunes');
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     *
     */
    private function videos(): Response
    {
        $request = Request::createFromGlobals();

        $start = intval($request->get('start', 0));
        $limit = $this->maxLimit(intval($request->get('limit', 6)));
        $random = $request->get('random', false) === 'true';

        $videos = $this->gradioRepo->getLatestVideos($limit, $random);

        $results = compact('videos');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     *
     */
    private function video(): Response
    {
        global $uri;

        $request = Request::createFromGlobals();
        $random = $request->get('random', false) === 'true';

        $uri = (object)$uri;

        $results = null;
        switch($uri->var_1) {
            case 'power':
                $video = $this->gradioRepo->getPowerVideo($random);
                $results = compact('video');
                break;
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     * @throws Exception
     */
    private function category(): Response
    {
        global $uri;

        $uri = (object)$uri;

        $category = $this->categoryRepo->getCategoryByAliasV2($uri->var_1);
        $meta = $this->categoryRepo->getCategoryMeta($category);
        $results = compact('category', 'meta');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     *
     */
    private function categories(): Response
    {
        $request = Request::createFromGlobals();

        $start = intval($request->get('start', 0));
        $limit = $this->maxLimit(intval($request->get('limit', 18)));
        $random = $request->get('random', false) === 'true';

        $categories = $this->categoryRepo->getAllCategoriesV2($start, $limit, $random);
        $meta = ['pageTitle' => _('Categories')];
        $results = compact('categories', 'meta');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

    /**
     *
     */
    private function contents(): Response
    {
        global $uri;

        $uri = (object)$uri;

        $request = Request::createFromGlobals();

        $start = intval($request->get('start', 0));
        $limit = $this->maxLimit(intval($request->get('limit', 6)));
        $random = $request->get('random', false) === 'true';

        $contents = $this->contentRepo->getContentByCategorySlug($uri->var_1, $start, $limit, $random);

        $results = compact('contents');

        $jsonResponse = new JsonResponse();
        $jsonResponse->setData(compact('results'));
        return $jsonResponse->send();
    }

}
