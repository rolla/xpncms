<?php namespace XpnCMS\Controller;

use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use XpnCMS\Model\User;
use Core\Includes\Upload\FileUpload;
use Core\Helpers\Seo;
use Core\Helpers\Directory;
use Core\Helpers\Files;

class Admin extends Controller
{

    public function __construct()
    {
        global $uri, $rbac;

        $currentUserId = User::authId();

        // Get Role Id
        $roleId = $rbac->Roles->returnId('gradio');

        // Make sure User has 'forum_user' Role
        $allowed = 0;
        $allowed = $rbac->Users->hasRole($roleId, $currentUserId);

        if ($allowed) {
          return true;
        }

        if ($uri['method'] != 'version') {
          Auth::notAllowed();
        }
    }

    public function isAdmin($class)
    {
        global $rbac;

        $user = $this->loadModel('user');
        // $userId = User::authId();
        // $userData = $user->get($userId);
        //
        // $role_id = $rbac->Roles->returnId('main_admin');
        // $allowed = $rbac->Users->hasRole($role_id, $userId);

        if ($user->checkUsr()) {
            if ($user->checkClass($class)) { // 250 main site admin
                return true;
            }

        } else {
            return false;
        }
    }

    public function index()
    {
        global $debug;

        $session = new Session();

        $user = $this->loadModel('user');
        //$user->checkUsr();
        //$debug->Display($user);
        // get the user data from database
        $userData = $user->get(User::authId());
        $admin = $this->loadModel('admin');
        $getLatestContent = $admin->getLatestContent();

        $data = [
            'pageTitle' => 'Admin home',
            'userData' => $userData,
            'isAdmin' => true,
            'contents' => $getLatestContent['content']
        ];
        $this->loadView('admin/index', $data);
    }

    public function latesttracks()
    {
        global $debug;

        $user = $this->loadModel('user');

        $this->loadView('admin/latesttracks');
    }

    public function latestcontent()
    {
        global $debug;

        $user = $this->loadModel('user');

        $admin = $this->loadModel('admin');
        $getLatestContent = $admin->getLatestContent();
        $data = ['pageTitle' => 'Latest content', 'contents' => $getLatestContent['content']];
        $this->loadView('admin/latestcontent', $data);
    }

    public function contents($id, $start = 0, $limit = 1)
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');

        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $start = $uri['var_1'];
        }

        if (isset($uri['var_2']) && !empty($uri['var_2']) && is_numeric($uri['var_2'])) {
            $limit = $uri['var_2'];
        }

        //$debug->Display($id);
        //$debug->Display($start);
        //$debug->Display($limit, 0, 0);

        $admin = $this->loadModel('admin');
        //$user = $this->loadModel("user");
        // get the user data from database
        $userData = $user->get(User::authId());

        if (is_numeric($id) && $id != 0) {
            $getContent = $admin->getContent($id);
            $data = ['pageTitle' => 'Content', 'userData' => $userData, 'contents' => $getContent['content']];
            $this->loadView('admin/contents', $data);
        } elseif ($id == 'all') {
            $getAllContent = $admin->getAllContent($start, $limit);
            $data = ['pageTitle' => 'All content', 'userData' => $userData, 'contents' => $getAllContent['content']];
            $this->loadView('admin/contents', $data);
        } elseif (isset($id)) {
            $getContentByAlias = $admin->getContentByAlias($id);
            $data = [
                'pageTitle' => 'All content',
                'userData' => $userData,
                'contents' => $getContentByAlias['content']
            ];
            $this->loadView('admin/contents', $data);
        } else {
            $getAllContent = $admin->getAllContent(0, 30);
            $data = ['pageTitle' => 'All content', 'userData' => $userData, 'contents' => $getAllContent['content']];
            $this->loadView('admin/contents', $data);
        }
    }

    // alias
    public function contentRaw($id, $start = 0, $limit = 1)
    {
        return $this->ContentsRaw($id, $start, $limit);
    }

    public function contentsRaw($id, $start = 0, $limit = 1)
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');

        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $start = $uri['var_1'];
        }

        if (isset($uri['var_2']) && !empty($uri['var_2']) && is_numeric($uri['var_2'])) {
            $limit = $uri['var_2'];
        }

        //$debug->Display($id);
        //$debug->Display($start);
        //$debug->Display($limit, 0, 0);

        $admin = $this->loadModel('admin');
        //$user = $this->loadModel("user");
        // get the user data from database
        $userData = $user->get(User::authId());
        $categories_parent = $admin->getCategoryTreeForParentId();

        if (is_numeric($id) && $id != 0) {
            $getContent = $admin->getContent($id);
            $data = ['pageTitle' => 'Content', 'userData' => $userData, 'contents' => $getContent['content']];
            $this->loadView('admin/contentraw', $data);
        } elseif ($id == 'all') {
            $getAllContent = $admin->getAllContent($start, $limit);
            $data = ['pageTitle' => 'All content', 'userData' => $userData, 'contents' => $getAllContent['content']];
            $this->loadView('admin/contentraw', $data);
        } elseif ($id == 'raw') {
            $getAllContent = $admin->getAllContent(0, 30);
            $data = ['pageTitle' => 'All content', 'userData' => $userData, 'contents' => $getAllContent['content']];
            $this->loadView('admin/contentraw', $data);
        } elseif (isset($id)) {
            $getContentByAlias = $admin->getContentByAlias($id);
            $data = [
                'pageTitle' => 'All content',
                'userData' => $userData,
                'contents' => $getContentByAlias['content']
            ];
            $this->loadView('admin/contentraw', $data);
        } else {
            $getAllContent = $admin->getAllContent(0, 30);
            $getAllCategories = $admin->getAllCategories(0, 30);

            $data = [
                'pageTitle' => 'All content',
                'userData' => $userData,
                'contents' => $getAllContent['content'],
                'categories' => $getAllCategories['categories'],
                'categories_parent' => $categories_parent
            ];
            $this->loadView('admin/contentraw', $data);
        }
    }

    public function addContent($id)
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');

        $admin = $this->loadModel('admin');

        // get the user data from database
        $userData = $user->get(User::authId());

        $categories = $admin->getCategoryTreeForParentId();
        $getLatestContent = $admin->getLatestContent();

        $data = [
            'pageTitle' => 'Add content',
            'userData' => $userData,
            'contents' => $getLatestContent['content'],
            'categories' => $categories
        ];
        $this->loadView('admin/addcontent', $data);
    }

    public function editContent($id)
    {
        $user = $this->loadModel('user');

        $admin = $this->loadModel('admin');
        // get the user data from database
        $userData = $user->get(User::authId());

        $categories = $admin->getCategoryTreeForParentId();
        $getLatestContent = $admin->getLatestContent();

        $data = [
            'pageTitle' => 'Edit Content',
            'userData' => $userData,
            'contents' => $getLatestContent['content'],
            'categories' => $categories
        ];

        if (is_numeric($id) && $id != 0) {
            $getContent = $admin->getContent($id);
            $data['content'] = $getContent['content'];
        } else {
            $getContentByAlias = $admin->getContentByAlias($id);
            $data['content'] = $getContentByAlias['content'];
        }

        //echo '<pre>';
        //print_r($data['content']);
        //exit;

        $this->loadView('admin/editcontent', $data);
    }

    public function saveContent()
    {
        global $smarty;

        $adminRepository = $this->loadModel('admin');
        $notifyRepository = $this->loadModel('notify');

        $request = Request::createFromGlobals();
        $addContent = $adminRepository->addContent(
            $request->request->get('content_type'),
            $request->request->all()
        );

        $result = [];
        $code = 15;
        if ($addContent['ok']) {
            $id = $addContent['ok'];
            $code = 202;
            $result['saveContent'] = $adminRepository->getContent($id)['content'];
        }

        $simpleNotify = $notifyRepository->getCode($code, [
            'name' => $request->request->get('cont_name'),
            'id' => $id
        ]);

        $response = new JsonResponse();
        $response->setData(compact('simpleNotify', 'result'));
        $response->send();
    }

    public function updateContent()
    {
        global $smarty;

        $adminRepository = $this->loadModel('admin');
        $notifyRepository = $this->loadModel('notify');

        $request = Request::createFromGlobals();
        $id = $request->request->get('cont_id');
        $updateContent = $adminRepository->updateContent($request->request->all());

        $result = [];
        $code = 104;
        if ($updateContent) {
            $code = 302;
            $result['updateContent'] = $adminRepository->getContent($id)['content'];
        }

        $simpleNotify = $notifyRepository->getCode($code, [
            'name' => $request->request->get('cont_name'),
            'id' => $id
        ]);

        $response = new JsonResponse();
        $response->setData(compact('simpleNotify', 'result'));
        $response->send();
    }

    public function deleteContent($content_type = 'content', $id = 0)
    {
        global $uri, $debug;

        $session = new Session();

        //$debug->Display($uri, 0, 1);

        $user = $this->loadModel('user');

        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $id = $uri['var_1'];
        }

        $admin = $this->loadModel('admin');
        // get the user data from database
        $userData = $user->get(User::authId());

        $deleteContent = $admin->deleteContent($content_type, $id);

        /*
          if( is_numeric($id) && $id!=0 ){
          $deleteContent = $admin->deleteContent($id);
          $data = array( "pageTitle" => "Edit Content", "userData" => $userData, "contents" => $deleteContent['content'] );
          $this->loadView("admin/editcontent", $data);
          }else{
          $deleteContentByAlias = $admin->deleteContentByAlias($id);
          $data = array( "pageTitle" => "Edit content", "userData" => $userData, "contents" => $deleteContentByAlias['content'] );
          $this->loadView("admin/editcontent", $data);
          }
         */
    }

    public function category($id, $start = 0, $limit = 1)
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');
        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $start = $uri['var_1'];
        }
        if (isset($uri['var_2']) && !empty($uri['var_2']) && is_numeric($uri['var_2'])) {
            $limit = $uri['var_2'];
        }
        $admin = $this->loadModel('admin');

        // get the user data from database
        $userData = $user->get(User::authId());
        $categories_parent = $admin->getCategoryTreeForParentId();

        if (is_numeric($id) && $id != 0) {
            $getCategory = $admin->getCategory($id);
            $data = [
                'pageTitle' => 'admin_category',
                'userData' => $userData,
                'categories' => $getCategory['category'],
                'categories_parent' => $categories_parent
            ];
            $this->loadView('admin/categories', $data);
        } elseif (isset($id)) {
            $getCategoryByAlias = $admin->getCategoryByAlias($id);
            $data = [
                'pageTitle' => 'admin_category',
                'userData' => $userData,
                'categories' => $getCategoryByAlias['category'],
                'categories_parent' => $categories_parent
            ];
            $this->loadView('admin/categories', $data);
        }
    }

    public function categories($id, $start = 0, $limit = 1)
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');

        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $start = $uri['var_1'];
        }

        if (isset($uri['var_2']) && !empty($uri['var_2']) && is_numeric($uri['var_2'])) {
            $limit = $uri['var_2'];
        }

        //$debug->Display($id);
        //$debug->Display($start);
        //$debug->Display($limit, 0, 0);

        $admin = $this->loadModel('admin');
        //$user = $this->loadModel("user");
        // get the user data from database
        $userData = $user->get(User::authId());
        $categories_parent = $admin->getCategoryTreeForParentId();

        if ($id == 'all') {
            $getAllCategories = $admin->getAllCategories($start, $limit);
            $data = [
                'pageTitle' => 'admin_categories',
                'userData' => $userData,
                'categories' => $getAllCategories['categories'],
                'categories_parent' => $categories_parent
            ];
            $this->loadView('admin/categories', $data);
        } elseif ($id == 'raw') {
            $getAllCategories = $admin->getAllCategories(0, 30);
            $data = [
                'pageTitle' => 'admin_categories',
                'userData' => $userData,
                'categories' => $getAllCategories['categories'],
                'categories_parent' => $categories_parent
            ];
            $this->loadView('admin/categories', $data);
        } else {
            $getAllCategories = $admin->getAllCategories(0, 30);
            $data = [
                'pageTitle' => 'admin_categories',
                'userData' => $userData,
                'categories' => $getAllCategories['categories'],
                'categories_parent' => $categories_parent
            ];

            $this->loadView('admin/categories', $data);
        }
    }

    public function addCategory()
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');
        $admin = $this->loadModel('admin');

        // get the user data from database
        $userData = $user->get(User::authId());
        $categories = $admin->getCategoryTreeForParentId();
        //$debug->Display($categories, 0, 1);
        $data = ['pageTitle' => 'admin_add_category', 'userData' => $userData, 'categories' => $categories];
        $this->loadView('admin/addcategory', $data);
    }

    public function editCategory($id)
    {
        global $uri, $debug;

        $session = new Session();

        $user = $this->loadModel('user');
        $admin = $this->loadModel('admin');
        $userData = $user->get(User::authId());
        $categories = $admin->getCategoryTreeForParentId();

        if (is_numeric($id) && $id != 0) {
            $getCategory = $admin->getCategory($id);
            $data = [
            'pageTitle' => 'admin_edit_category',
            'userData' => $userData,
            'category' => $getCategory['category'],
            'categories' => $categories
            ];
            $this->loadView('admin/editcategory', $data);
        } else {
            $getCategoryByAlias = $admin->getCategoryByAlias($id);
            $data = [
            'pageTitle' => 'admin_edit_category',
            'userData' => $userData,
            'category' => $getCategoryByAlias['category'],
            'categories' => $categories
            ];
            $this->loadView('admin/editcategory', $data);
        }
    }

    public function saveCategory()
    {
        global $uri, $debug, $smarty;

        $session = new Session();

        $user = $this->loadModel('user');
        $admin = $this->loadModel('admin');
        $userData = $user->get(User::authId());
        $data = $_POST;

        if (isset($data['add_content'])) { // ********************************* pievienosana
            if (isset($data['content_type'])) {
                $content_type = $data['content_type'];
                $addContent = $admin->addContent($content_type, $data);
                $notify = $this->loadModel('notify');
                if ($addContent['ok']) {
                    $notify = $notify->getCode(202);
                    $tmpa = $notify['alert'];
                    $notify['alert'] = $tmpa . $addContent['ok'];
                    $smarty->clearAssign('notify');
                    $result['notify'] = $notify;
                    $json['rate'] = $result;
                } elseif ($addContent['error']) {
                    $notify = $notify->getCode(15);
                    $smarty->clearAssign('notify');
                    $result['notify'] = $notify;
                    $json['rate'] = $result;
                }
            }
        } elseif (isset($data['save_content'])) { // ********************************* sagalbāšana / apstrādāšana
            if (isset($data['content_type'])) {
                $content_type = $data['content_type'];
                $saveContent = $admin->saveContent($content_type, $data);
                $notify = $this->loadModel('notify');
                if ($saveContent['ok']) {
                    $notify = $notify->getCode(202);
                    $tmpa = $notify['alert'];
                    $notify['alert'] = $tmpa . $saveContent['ok'];
                    $smarty->clearAssign('notify');
                    $result['notify'] = $notify;
                    $json['rate'] = $result;
                } elseif ($saveContent['error']) {
                    $notify = $notify->getCode(15);
                    $smarty->clearAssign('notify');
                    $result['notify'] = $notify;
                    $json['rate'] = $result;
                }
            }
        }
        echo json_encode($json);
    }

    public function getCache()
    {
        global $files, $debug;

        //$debug->Display($uri, 0, 1);
        $user = $this->loadModel('user');
        $admin = $this->loadModel('admin');
        // get the user data from database
        $userData = $user->get($_SESSION['user']);

        $smarty_extensions = ['php', ''];
        $sacy_extensions = ['css', 'js'];
        $phpthumb_extensions = ['jpg', 'jpeg', 'gif', 'png'];

        $result['smarty'] =
            $files->getFileCountRecursive(
                APP_ROOT . Config::get('paths.cache') . DS . 'templates_c',
                $smarty_extensions
            );

        $publicPathCache = APP_ROOT . Config::get('paths.public') . DS . Config::get('paths.cache') . DS;
        $result['sacy'] =
            $files->getFileCountRecursive(
                $publicPathCache . 'sacy',
                $sacy_extensions
            );
        $result['phpthumb'] =
            $files->getFileCountRecursive(
                $publicPathCache . 'phpthumb',
                $phpthumb_extensions
            );
        $result['total'] = (int) $result['smarty'] + (int) $result['sacy'] + (int) $result['phpthumb'];

        echo json_encode($result);
    }

    public function clearCache()
    {
        global $forms, $files, $debug;

        //$debug->Display($uri, 0, 1);
        $user = $this->loadModel('user');
        $admin = $this->loadModel('admin');
        $notify = $this->loadModel('notify');
        // get the user data from database
        $userData = $user->get($_SESSION['user']);

        $smarty_extensions = ['php', ''];
        $sacy_extensions = ['css', 'js'];
        $phpthumb_extensions = ['jpg', 'jpeg', 'gif', 'png'];

        $path['smarty'] = APP_ROOT . Config::get('paths.cache') . DS . 'templates_c';
        $path['sacy'] = APP_ROOT . Config::get('paths.public') . DS . Config::get('paths.cache') . DS . 'sacy';
        $path['phpthumb'] = APP_ROOT . Config::get('paths.public') . DS . Config::get('paths.cache') . DS . 'phpthumb';

        if (isset($_POST['formid']) && $_POST['formid'] == 'clearcacheform') {
            $results['smarty'] = $forms->checkCheckbox($_POST['smarty']) ? $_POST['smarty'] : false;
            $results['sacy'] = $forms->checkCheckbox($_POST['sacy']) ? $_POST['sacy'] : false;
            $results['phpthumb'] = $forms->checkCheckbox($_POST['phpthumb']) ? $_POST['phpthumb'] : false;
        }

        foreach ($results as $type => $val) {
            if ($val == 'on') {
                if ($type == 'phpthumb') {
                    $result[$type] = $files->deleteDirectoriesRecursive($path[$type]);
                    $result[$type]['path'] = $path[$type];
                } elseif ($type == 'smarty') {
                    $result[$type] = $files->deleteFilesRecursive($path[$type], $smarty_extensions);
                    $result[$type]['path'] = $path[$type];
                } elseif ($type == 'sacy') {
                    $result[$type] = $files->deleteFilesRecursive($path[$type], $sacy_extensions);
                    $result[$type]['path'] = $path[$type];
                }
            }
        }

        //$debug->Display($result, 0, 1);

        if ($result['smarty']['deleted'] > 0) {
            $result['notifies'][] = $notify->getCode(204, [$result['smarty']['deleted']]);
        }

        if ($result['phpthumb']['deleted'] > 0) {
            $result['notifies'][] = $notify->getCode(205, [$result['phpthumb']['deleted']]);
        }

        if ($result['sacy']['deleted'] > 0) {
            $result['notifies'][] = $notify->getCode(206, [$result['sacy']['deleted']]);
        }

        $json['cleared'] = $result;

        echo json_encode($json);
    }

    public function users()
    {
        global $debug, $rbac;

        //echo 'test';
        // Create a Permission
        //$perm_id = $rbac->Permissions->add('delete_posts', 'Can delete forum posts');
        // Create a Role
        //$role_id = $rbac->Roles->add('forum_moderator', 'User can moderate forums');

        /*
          $perm_descriptions = array(
          'Can delete users',
          'Can edit user profiles',
          'Can view users'
          );

          $rbac->Permissions->addPath('/delete_users/edit_users/view_users', $perm_descriptions);

          $role_descriptions = array(
          'Forum Administrator',
          'Forum Moderator',
          'Registered Forum Member'
          );

          $rbac->Roles->addPath('/admin/forum_moderator/forum_member', $role_descriptions);
         */

        // Create Role and Permission
        //$perm_id = $rbac->Permissions->add('delete_posts', 'Can delete forum posts');
        //$role_id = $rbac->Roles->add('forum_moderator', 'User can moderate forums');
        // Assign Permission to Role
        //$rbac->Roles->assign($role_id, $perm_id);
        // Assign Role to User (The UserID is provided by the application's User Management System)
        //$rbac->Users->assign($role_id, 5);
        // install steps
        // 1
        /*
          $role_id = $rbac->Roles->add('main_admin', 'Main Administrator');
          $role_id = $rbac->Roles->add('admin', 'Administrator');
          $role_id = $rbac->Roles->add('gradio', 'Gradio Team');
          $role_id = $rbac->Roles->add('everyone', 'Public visible');
          $role_id = $rbac->Roles->add('user', 'Registered user');
          $role_id = $rbac->Roles->add('guest', 'Guest user');
          $role_id = $rbac->Roles->add('no_one', 'Inactive');
         */

        // 2
        /*
          $perm_id = $rbac->Permissions->add('grant_as_madmin', 'Grant as Main Admin');
          $role_id = $rbac->Roles->returnId('main_admin');
          // Assign Permission to Role
          $rbac->Roles->assign($role_id, $perm_id);

          $perm_id = $rbac->Permissions->add('grant_as_admin', 'Grant as Admin');
          $perm_id = $rbac->Permissions->add('can_edit_user', 'Can edit user');
          $perm_id = $rbac->Permissions->add('can_see_user', 'Can see full user info');
          $perm_id = $rbac->Permissions->add('can_delete_user', 'Can delete user');
          $perm_id = $rbac->Permissions->add('can_edit_content', 'Can edit content');
          $perm_id = $rbac->Permissions->add('can_delete_content', 'Can delete content');
          $perm_id = $rbac->Permissions->add('can_edit_categories', 'Can edit categories');
          $perm_id = $rbac->Permissions->add('can_delete_categories', 'Can delete categories');
         */

        // Assign Role to User (The UserID is provided by the application's User Management System)
        //$rbac->Users->assign(1, 100);
        //$rbac->Users->assign(2, 102);
        //$rbac->Users->assign(3, 100);
        //$rbac->Users->assign(3, 102);
        //$rbac->Users->assign(5, 100);
        //$rbac->Users->assign(5, 102);
        // Unassign 'forum_user' Role assigned to a User using the Role's Path
        //$rbac->Users->unassign('user', 100);
        //$rbac->Users->unassign('user', 102);

        $debug->display($rbac->Permissions);
        $debug->display($rbac->Roles);
        $debug->display($rbac->Users->allRoles(100));
    }

    public function imageupload()
    {
        $request = Request::createFromGlobals();
        $imgPath = Config::get('paths.images');
        $type = $request->headers->get('type');
        $category = $request->headers->get('category');

        $filesHelper = new Files;
        $uploader = new FileUpload('uploadfile');
        $uploader->sizeLimit = $filesHelper->fileUploadMaxSize();

        $seo = new Seo();
        $filename = $seo->generateRandomString();
        $uploader->newFileName = $filename;

        $uploadDir = $imgPath . DS . $type . DS . $category . DS;

        //exit($uploadDir);

        $directory = new Directory();
        $directory->makePath($uploadDir);

        $uploaded = $uploader->handleUpload($uploadDir);

        $notifyModel = $this->loadModel('notify');

        $code = 17;
        if ($uploaded) {
            $code = 302;
        }

        $notify = $notifyModel->getCode($code, [
            'errorMsg' => $uploader->getErrorMsg(),
            'id' => $request->request->get('cont_id')
        ]);

        /*
        $notify = [
            'success' => false,
            'msg' => $uploader->getErrorMsg()
        ];
         *
         */

        /*
        if (!$result) {
            //exit(json_encode());
        }
         *
         */

        /*
        $notify = ['success' => true, 'filename' => $result];
         *
         */

        $response = new JsonResponse();
        $response->setData($notify);
        $response->send();
    }

    public function debug()
    {
        global $debug;
        //$conf_debug = $debug->Display($smarty->getTemplateVars(), 1, 0);
        //$conf_debug .= $debug->Display($conf, 1, 0);
        $data = ['pageTitle' => 'admin_debugconsole'];
        $this->loadDebugView('admin/debug', $data);
    }

    public function phpinfo()
    {
        phpinfo();
    }

    public function version()
    {
        $version = file_get_contents(APP_ROOT . 'app' . DS . 'version.json');
        echo $version;
    }
}
