<?php

namespace XpnCMS\Controller;

use StringTemplate\Engine;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class GetText extends Controller
{
    public function index()
    {
        $this->translate();
    }

    public function translate()
    {
        $engine = new Engine(':', '');
        $request = Request::createFromGlobals();

        //echo '<pre>';
        //print_r($request->query->all());
        //exit;

        echo $engine->render(gettext($request->query->get('string')), $request->query->get('data'));
    }

    /*
    public function translateplural()
    {
        $engine = new Engine(':', '');

        $request = Request::createFromGlobals();

        echo '<pre>';
        print_r($request->query->all());
        //exit;

        echo $engine->render(
            ngettext($request->query->get('string'), $request->query->get('string'), $request->query->get('count')), $request->query->get('var')
        );
    }
     *
     */
}
