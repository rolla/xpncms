<?php

namespace XpnCMS\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;

class Newplayer extends Controller
{
    public function index()
    {
        $session = new Session();

        $user = $this->loadModel('user');
        //$user->checkUsr();

        //$debug->Display($user);

        // get the user data from database
        $userData = $user->get(User::authId());

        $data = ['pageTitle' => 'New Player tests', 'userData' => $userData, 'newPlayer' => true];
        $this->loadView('home/newplayer', $data);
    }
}
