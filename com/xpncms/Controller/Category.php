<?php namespace XpnCMS\Controller;

use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Session\Session;
use XpnCMS\Model\User;

class Category extends Controller
{
    public function index($id, $start = 0, $limit = 1)
    {
        global $uri;

        if (isset($uri['var_1']) && !empty($uri['var_1']) && is_numeric($uri['var_1'])) {
            $start = $uri['var_1'];
        }

        if (isset($uri['var_2']) && !empty($uri['var_2']) && is_numeric($uri['var_2'])) {
            $limit = $uri['var_2'];
        }

        $category = $this->loadModel('category');
        $user = $this->loadModel('user');
        $content = $this->loadModel('content');

        // get the user data from database
        $userData = $user->get(User::authId());

        if (is_numeric($id) && $id != 0) {
            $getCategory = $category->getCategory($id);
            $currentCategory = $getCategory['categories'][0];

            $categories_parent = $category->getCategoryTreeForParentId($id);
            $getContentByCategory = $content->getContentByCategory($id);
            $pageTitle = _('Category') . ' : ' . $currentCategory['cat_name'];
            $name = $currentCategory['cat_name'];
            $contentText = strip_tags(html_entity_decode($currentCategory['cat_content']));
            $ogimage = urldecode(Config::get('base.fbsiteurl') . '/'
                . Config::get('paths.images') . '/'
                . $currentCategory['cat_imgs'])
            ;
            $categories = $getCategory['categories'];
        } elseif ($id == 'all') {
            $getAllCategories = $category->getAllCategories($start, $limit);
            $currentCategory = $getAllCategories['categories'][0];

            $categories_parent = $category->getCategoryTreeForParentId();
            $pageTitle = _('Categories');
            $name = $currentCategory['cat_name'];
            $contentText = strip_tags(html_entity_decode($currentCategory['cat_content']));
            $ogimage = urldecode(Config::get('base.fbsiteurl') . '/'
                . Config::get('paths.images') . '/'
                . $currentCategory['cat_imgs'])
            ;
            $categories = $getAllCategories['categories'];
        } elseif (isset($uri['method']) && !empty($uri['method']) && $uri['method'] != 'index') {
            $getCategoryByAlias = false;
            $getContentByCategory = false;
            $getCategoryByAlias = $category->getCategoryByAlias($uri['method']);
            if (count($getCategoryByAlias['categories'])) {
                $currentCategory = $getCategoryByAlias['categories'][0];

                $pageTitle = _('Category') . ' : ' . $currentCategory['cat_name'];
                $name = $currentCategory['cat_name'];
                $contentText = strip_tags(html_entity_decode($currentCategory['cat_content']));
                $categories = $getCategoryByAlias['categories'];
                $ogimage = urldecode(Config::get('base.fbsiteurl') . '/'
                    . Config::get('paths.images') . '/'
                    . $currentCategory['cat_imgs'])
                ;

                $getContentByCategory = false;
                $getContentByCategory = $content->getContentByCategory($currentCategory['cat_id']);
                $categories_parent = $category->getCategoryTreeForParentId($currentCategory['cat_id']);
                if ($getContentByCategory['content']) {
                    $contents = $getContentByCategory['content'];
                }
            }
        } else {
            $getAllCategories = $category->getAllCategories(0, 25);
            $pageTitle = _('Categories');
            $categories = $getAllCategories['categories'];
        }

        $gradioRepo = $this->loadPluginModel('gradio');

        $latestTuneVideos = $gradioRepo->getLatestVideos(6, true);
        $latestTunes = $gradioRepo->getLatestTunes(0, 6, true);
        $recentTunes = $gradioRepo->getRecentsongs(0, 3);
        $powerVideo = $gradioRepo->getPowerVideo();

        $data = [
            'pageTitle' => $pageTitle,
            'userData' => $userData,
            'categories' => $categories,
            'categories_parent' => $categories_parent,
            'contents' => $contents,
            'meta' => [
                'content' => $contentText,
                'ogimage' => $ogimage,
                'ogtitle' => $pageTitle,
                'ogtype' => 'article',
                'ogarticlesection' => $name,
                'twittertitle' => $pageTitle,
                'twitterdescription' => $contentText,
                'twitterimage' => $ogimage,
            ],
            'latestTuneVideos' => $latestTuneVideos,
            'latestTunes' => $latestTunes,
            'recentTunes' => $recentTunes,
            'powerVideo' => $powerVideo,
        ];

        $this->loadView('category/index', $data);
    }
}
