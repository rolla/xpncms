<?php

namespace XpnCMS\Controller;

use Core\Includes\Config;

class Home extends Controller
{
    public function indexData()
    {
      $user = $this->loadModel('user');
      $currentUserId = $user->authId();
      $auth = $this->loadModel('auth');
      $gradioRepo = $this->loadPluginModel('gradio');
      $latestTuneVideos = $gradioRepo->getLatestVideos(6);
      $latestTunes = $gradioRepo->getLatestTunes(0, 6);
      $recentTunes = $gradioRepo->getRecentsongs(0, 3);
      $powerVideo = $gradioRepo->getPowerVideo();

      $userData = null;
      $userAuthentications = null;

      if ($currentUserId) {
          $userData = $user->get($currentUserId);
          $userAuthentications = $auth->auths($currentUserId);

          $usr_img_path = Config::get('base.url') . Config::get('paths.files') . '/user_'.$userData['id'] . '/';

          if (!preg_match('~^(?:f|ht)tps?://~i', $userData['photo_url'])) {
              $photo_url = $usr_img_path.$userData['photo_url'];
              $userData['photo_url'] = $photo_url;
          }

      }

      $content = $this->loadModel('content');
      $getAllContent = $content->getAllContent(0, 6);

      $data = [
          'pageTitle' => _('Home'),
          'userData' => $userData,
          'user_authentications' => $userAuthentications,
          'contents' => $getAllContent['content'],
          'latestTuneVideos' => $latestTuneVideos,
          'latestTunes' => $latestTunes,
          'recentTunes' => $recentTunes,
          'powerVideo' => $powerVideo,
      ];

      return $data;
    }

    public function index()
    {
        //$this->redirect( $conf['settings']['site']['startpage'] );
        $this->loadView(Config::get('site.startpage'), $this->indexData());
    }
}
