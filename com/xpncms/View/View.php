<?php

namespace XpnCMS\View;

use Exception;
use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use XpnCMS\Model\User;

class View
{

    public $templateArr; // set values to send to view from everywhere

    /**
     * @var User
     */
    private $user;

    private $userId;

    public function __construct()
    {
        $userModel = new User;
        $this->user = $userModel->auth();
        $this->userId = $this->user->id ?? null;
    }

    private function checkCurrentTemplate($view, $template, $plugin = false)
    {
        global $smarty;

        $session = new Session();

        $currentTemplate = $template ? $template : Config::get('site.template');
        if ($session->get('template')) {
            $currentTemplate = $session->get('template');
            $session->save();
        }
        $smarty->setTemplateDir(['current' => APP_VIEW . $currentTemplate . '/']);

        $viewFound = true;
        $currentView = $view . '.tpl';

        if (!$plugin && !file_exists($smarty->getTemplateDir('current') . $currentView)) {
            $currentTemplate = 'default';
            $viewFound = false;
            $smarty->setTemplateDir(['current' => APP_VIEW . $currentTemplate . '/']);
        }

        // check if layout exists in current view scope
        $scopeArr = explode('/', $view);
        $scope = $scopeArr[0];
        $layout = false;
        if (file_exists($smarty->getTemplateDir('current') . $scope . '/layout.tpl')) {
            $layout = $scope . '/layout.tpl';
        }

        return [
            'template' => $currentTemplate,
            'currentView' => $currentView,
            'layout' => $layout,
            'viewFound' => $viewFound,
        ];
    }

    public function loadView($view = false, $vars = '', $template = null, $ajax = false)
    {
        global $smarty, $rbac;

        if (FRONTEND_V2) {
            return $this->loadViewV2('meta', $vars);
        }

        $request = Request::createFromGlobals();
        $session = new Session();

        $isAjax = $ajax;
        if ($request->query->has('ajax')) {
            $isAjax = true;
        }

        if (is_array($vars['meta']) && count($vars['meta'])) {
            $meta = array_merge($smarty->getTemplateVars()['meta'], $vars['meta']);
            $vars['meta'] = $meta;
        }

        if (is_array($vars) && count($vars) > 0) {
            extract($vars, EXTR_PREFIX_SAME, 'wddx');
            $this->_vars = $vars;
            $smarty->assign($this->_vars);
        }

        $currentTemplate = $this->checkCurrentTemplate($view, $template);

        $pageId = str_replace('/', '_', $view);
        $smarty->assign(compact('pageId')); // deprecated

        if ($session->get('smartyExtData')) {
            $smartyExtData = $session->get('smartyExtData');
            $smarty->assign(compact('smartyExtData'));
            $session->remove('smartyExtData');
            $session->save();
        }

        $allowed = 0;
        $roleId = $rbac->Roles->returnId('gradio');
        if ($this->userId) {
            $allowed = $rbac->Users->hasRole($roleId, $this->userId);
        }
        $smarty->assign(['gradioTeam' => intval($allowed)]);

        $currentView = $currentTemplate['currentView'];
        $layout = $currentTemplate['layout'];

        $loadTemplate = 'extends:' . $currentView;

        if (!$isAjax && Config::get('site.template') != 'gradio-material') {
            $loadTemplate = 'extends:header.tpl';
            $loadTemplate .= '|' . $currentView;
            $loadTemplate .= '|footer.tpl';
            $loadTemplate .= '|notify/notify.tpl';

            if ($layout) {
                $loadTemplate = 'extends:' . $layout;
                $loadTemplate .= '|' . $currentView;
                $loadTemplate .= '|notify/notify.tpl';
            }
        }

        $this->templateArr = [
            'pageId' => $pageId,
            'viewFound' => $currentTemplate['viewFound'],
            'view' => $view,
            'template' => $currentTemplate['template'],
            'allViews' => $loadTemplate,
        ];

        //echo '<pre>';
        //print_r($this->templateArr);
        //exit;

        return $this->render($isAjax);
    }

    /**
     * @param string $view
     * @param array $vars
     * @throws \SmartyException
     */
    public function loadViewV2(string $view, array $vars = [])
    {
        global $smarty;

        $templatePath = APP_VIEW . Config::get('site.template') . '/';

        /* old meta fix */
        if (isset($vars['meta']['pageTitle'])) {
            $vars['pageTitle'] = $vars['meta']['pageTitle'];
        }

        if (is_array($vars['meta']) && count($vars['meta'])) {
            $meta = array_merge($smarty->getTemplateVars()['meta'], $vars['meta']);
            $vars['meta'] = $meta;
        }

        if (is_array($vars) && count($vars) > 0) {
            extract($vars, EXTR_PREFIX_SAME, 'wddx');
            $this->_vars = $vars;
            $smarty->assign($this->_vars);
        }

        $smarty->assign(['user' => $this->user]);
        $smarty->display('extends:' . $templatePath . 'index.tpl|' . $templatePath . 'meta.tpl');
    }

    public function loadPluginView($view = false, $vars = '', $template = null, $ajax = false)
    {
        global $smarty, $rbac;

        if (FRONTEND_V2) {
            return $this->loadViewV2('meta', $vars);
        }

        $request = Request::createFromGlobals();

        $plugin = explode('/', $view);

        $isAjax = $ajax;
        if ($request->query->has('ajax')) {
            $isAjax = true;
        }

        if (is_array($vars['meta']) && count($vars['meta'])) {
            $meta = array_merge($smarty->getTemplateVars()['meta'], $vars['meta']);
            $vars['meta'] = $meta;
        }

        if (is_array($vars) && count($vars) > 0) {
            extract($vars, EXTR_PREFIX_SAME, 'wddx');
            $smarty->assign($vars);
        }

        $currentTemplate = $this->checkCurrentTemplate($view, $template, true);

        $pageId = str_replace('/', '_', $view);
        $smarty->assign(compact('pageId')); // deprecated

        $session = new Session();
        if ($session->get('smartyExtData')) {
            $smartyExtData = $session->get('smartyExtData');
            $smarty->assign(compact('smartyExtData'));
            $session->remove('smartyExtData');
            $session->save();
        }

        $viewFound = true;
        $currentView = str_replace('\\', '/', APP_PLUGIN) . strtolower($plugin[0]) .
                '/' . 'View' . '/' . $plugin[1] . '/' . $plugin[2] . '.tpl';

        if (!file_exists($currentView)) {
            //$template = 'default';
            $viewFound = false;
        }

        $roleId = $rbac->Roles->returnId('gradio');
        // Make sure User has 'forum_user' Role
        if ($this->userId) {
            $allowed = $rbac->Users->hasRole($roleId, $this->userId);
        }

        $smarty->assign(['gradioTeam' => intval($allowed)]);

        $loadTemplate = 'extends:' . $currentView;

        if (!$isAjax && Config::get('site.template') != 'gradio-material') {
            $loadTemplate = 'extends:header.tpl';
            $loadTemplate .= '|' . $currentView;
            $loadTemplate .= '|footer.tpl';
            $loadTemplate .= '|notify/notify.tpl';
        }

        if (Config::get('site.template') === 'gradio-material') {
            $loadTemplate = 'extends:index.tpl|' . $currentView;
        }

        $this->templateArr = [
            'pageId' => $pageId,
            'viewFound' => $viewFound,
            'view' => $view,
            'template' => $currentTemplate['template'],
            'allViews' => $loadTemplate
        ];

        return $this->render($isAjax);
    }

    private function render($isAjax)
    {
        global $smarty, $appStartLoad, $debugbar, $debugbarRenderer;

        $smarty->assign('loadedTemplate', $this->templateArr);

        if (!$isAjax) {
            $isWritable = file_put_contents($smarty->getCompileDir() . 'dummy.txt', 'hello');
            if ($isWritable > 0) {
                unlink($smarty->getCompileDir() . 'dummy.txt');
                $appEndLoad = microtime(true);
                if ($debugbar) {
                    $debugbar['time']->addMeasure('load', $appStartLoad, $appEndLoad);
                }
                if ($debugbarRenderer) {
                    $smarty->assign(['debugbarRendererHead' => $debugbarRenderer->renderHead()]);
                    $smarty->assign(['debugbarRender' => $debugbarRenderer->render()]);
                }
                $smarty->display($this->templateArr['allViews']);
                $session = new Session();
                $session->set('flashNotify', []);
                $session->save();
            } else {
                throw new Exception('Smarty compile DIR ' . $smarty->getCompileDir() . ' not writable!');
            }
        } else {
            if ($debugbar) {
                $debugbar->sendDataInHeaders(true);
            }
            $appEndLoad = microtime(true);
            if ($debugbar) {
                $debugbar['time']->addMeasure('load', $appStartLoad, $appEndLoad);
            }
            if ($debugbarRenderer) {
                $smarty->assign(['debugbarRendererHead' => $debugbarRenderer->renderHead()]);
                $smarty->assign(['debugbarRender' => $debugbarRenderer->render()]);
            }

            //echo '<pre>';
            //print_r($this->templateArr['allViews']);
            //exit;

            $html['html'] = $smarty->fetch($this->templateArr['allViews']);
            $vars = $smarty->getTemplateVars();
            $html['title'] = $vars['title'];
            $html['pageId'] = $this->templateArr['pageId']; // bug! $pageId used wrong twice!
            $html['vars'] = $vars;
            $html['templateArr'] = $this->templateArr;

            if (isset($vars['simpleNotify'])) {
                $html['simpleNotify'] = $vars['simpleNotify'];
                unset($vars['simpleNotify']);
            }
            $session = new Session();
            $session->set('flashNotify', []);
            $session->save();

            header('Content-Type: application/json');
            echo json_encode($html);
        }
    }
}
