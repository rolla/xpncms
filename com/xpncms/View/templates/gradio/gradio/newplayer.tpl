{block name="gradio_player"}
	<div class="gradio-player-container-new container">
	
			<div class="art-container full-width-overlay">
				<div class="art">
					<div class="black-footer"></div>
				</div>
			</div>

		<div id="gradio_player" class="jp-jplayer gradio-player"></div>
		
			<div class="player-container-inner" style="display: none;">
			
				<div class="full-player" data-show="true">
				
					<div class="row">
						<div class="col-xs-12 debug" style="height: 50px">
							<div class="component-current-show">
								<div class="component-current-show-inner">
									<h1>Current show: Test new player!</h1>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-2 debug" style="height: 100px; padding: 0; margin: 0;">
							<div class="component-current-tune-image">
								<div class="component-current-tune-image-inner">
									<div id="gradio-player-art" class="now-playing-art"><div class="now-playing-art-container"></div></div>
								</div>
							</div>
						</div>
						<div class="col-xs-9 debug" style="height: 100px; padding: 0; margin: 0;">
						
						<div class="component-player">
							<div class="component-player-inner">
								<div class="component-player-controls">
									<div class="component-player-controls-inner col-xs-4">
										<div class="jp-gui">
											<ul>
												<li class="jp-play ui-state-default ui-corner-all"><a href="javascript:;" class="jp-play fa fa-play fa-4x" tabindex="1" title="play"></a></li>
												
												<li class="jp-stop ui-state-default ui-corner-all"><a href="javascript:;" class="jp-stop fa fa-stop fa-4x" tabindex="1" title="stop"></a></li>
												
												<li class="ui-state-default ui-corner-all"><a href="javascript:;" class="jp-volume-change fa fa-volume-up fa-4x" tabindex="1" title="stop"></a></li>
											</ul>
									
											<!--<div class="jp-progress-slider"></div>-->
											
											<div class="jp-volume-slider"></div>
											
											<div class="jp-current-time" style="display:none"></div>
											<!--<div class="jp-duration"></div>-->
											<!--<div class="jp-clearboth"></div>-->
										</div>
									</div>
								</div>
							</div>
							
							<div id="now-on-air" class="now-on-air col-xs-8" style="display: none;">
								<div class="now-on-air-inner" style="display: none">
									<a href="http://www.google.lv/search?q=lv universal radio" id="google-search" target="_blank" title="Search about:">
									<div id="now-artist" class="pull-left"></div> <div class="a-t-seperator pull-left"> - </div> <div id="now-title" class="pull-left"></div>
									</a>
									<div class="clearfix"></div>
									<div class="current-time pull-left"></div><div id="now-rem-time" class="pull-right remain-time"></div>
									<div class="big-progress">
										<div class="percent"></div>
										<div class="pbar"></div>
										<div class="elapsed"></div>
									</div>
									
									
									<div class="ratings" data-songid="0">
										<div class="likes" data-rating="like"></div>
										<div class="dislikes" data-rating="dislike"></div>
										<div class="comments"></div>
									</div>
								</div>
							</div>
							
						</div>
						
						</div>
						<div class="col-xs-1 debug" style="height: 100px">
					
							<div class="component-settings">
								<div class="component-settings-inner">Settings
									<button class="btn btn-xs btn-danger toggle-timers" data-action="stop" title="Stop/Start all timers"><i class="fa fa-stop"></i></button>
									<div class="timer-status">Status: Start</div>
									
									<div class="toggle-player" data-ot-title="Toggle player" data-ot="">
										<i class="fa fa-arrow-up"></i>
										<i class="fa fa-arrow-down" style="display: none;"></i>
									</div>
									<div class="refresh-player" data-ot-title="Refresh" data-ot="">
										<i class="fa fa-refresh"></i>
									</div>
									
								</div>
							</div>
						
						</div>
					</div>
	
						<div class="clearfix"></div>
	
					<div class="row">
						
	
						<!--<div id="gradio-player-art" class="now-playing-art"><div class="now-playing-art-container"></div></div>-->
						<!--<div id="gradio-player-bio" class="now-playing-bio"><div class="now-playing-bio-container"></div></div>-->
					</div>
				</div>
				
				<div class="small-player col-xs-10 col-xs-push-2 col-sm-4 col-sm-push-1 col-md-4 col-md-push-1 col-lg-3 col-lg-push-1" data-show="false">
					<ul>
					<li class="jp-play ui-state-default ui-corner-all"><a href="javascript:;" class="jp-play ui-icon ui-icon-play" tabindex="1" title="play">play</a></li>
					
					<li class="jp-pause ui-state-default ui-corner-all"><a href="javascript:;" class="jp-pause ui-icon ui-icon-pause" tabindex="1" title="pause">pause</a></li>
					
					<li class="jp-stop ui-state-default ui-corner-all"><a href="javascript:;" class="jp-stop ui-icon ui-icon-stop" tabindex="1" title="stop">stop</a></li>
					</ul>
					<div class="jp-volume-slider-small"></div>
				</div>
				
				
			</div>
			
			
			
			<div id="now-on-air-s" class="now-small col-xs-8 col-xs-push-2 col-sm-5 col-sm-push-0 col-md-6 col-md-push-1 col-lg-7 col-lg-push-0" style="display: none;">
				<div id="now-artist-s"></div><div> | </div><div id="now-title-s"></div><div> | </div><div id="now-rem-time-s" class="remain-time"></div>
			</div>
			
			<div class="jp-no-solution">
				<span>Update Required</span>
				To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
			</div>
			
			<!--
			<div class="player-controls col-xs-2 col-xs-offset-1 col-sm-2 col-md-1 col-md-offset-0 col-md-push-0 col-lg-1 col-lg-offset-0 col-lg-push-1 debu">
				<div class="toggle-player" data-ot-title="Toggle player" data-ot="">
					<i class="fa fa-arrow-up"></i>
					<i class="fa fa-arrow-down" style="display: none;"></i>
				</div>
				<div class="refresh-player" data-ot-title="Refresh" data-ot="">
					<i class="fa fa-refresh"></i>
				</div>
			</div>
			-->
		
	</div>
{/block}