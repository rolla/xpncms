{block name="gradio_player"}
  <div class="gradio-player-container container">

    <div class="art-container full-width-overlay">
      <!--<div class="art" style="background-image: url('http://userserve-ak.last.fm/serve/252/97723421.png');">-->
      <div class="art">
        <div class="black-footer"></div>
      </div>
    </div>

    <div id="gradio_player" class="jp-jplayer gradio-player"></div>
    <div class="row">
      <div class="player-container-inner" style="display: none;">

        <div class="small-player col-xs-10 col-xs-push-2 col-sm-4 col-sm-push-1 col-md-4 col-md-push-1 col-lg-3 col-lg-push-1" data-show="false">
          <ul>
            <li class="jp-play ui-state-default ui-corner-all"><a href="javascript:;" class="jp-play ui-icon ui-icon-play" tabindex="1" title="play">play</a></li>

            <li class="jp-pause ui-state-default ui-corner-all"><a href="javascript:;" class="jp-pause ui-icon ui-icon-pause" tabindex="1" title="pause">pause</a></li>

            <li class="jp-stop ui-state-default ui-corner-all"><a href="javascript:;" class="jp-stop ui-icon ui-icon-stop" tabindex="1" title="stop">stop</a></li>
          </ul>
          <div class="jp-volume-slider-small"></div>
        </div>

        <div class="full-player col-xs-push-5 col-xs-7 col-sm-push-0 col-sm-7 col-md-7 col-lg-6 debu" data-show="true">

          <div id="gradio-player-art" class="now-playing-art"><div class="now-playing-art-container"></div></div>

          <div id="gradio_player_container">
            <div class="jp-gui ui-widget ui-widget-content ui-corner-all">
              <ul>
                <li class="jp-play ui-state-default ui-corner-all"><a href="javascript:;" class="jp-play ui-icon ui-icon-play" tabindex="1" title="{t}Play{/t}">{t}Play{/t}</a></li>
                <li class="jp-pause ui-state-default ui-corner-all"><a href="javascript:;" class="jp-pause ui-icon ui-icon-pause" tabindex="1" title="{t}Pause{/t}">{t}Pause{/t}</a></li>
                <li class="jp-stop ui-state-default ui-corner-all"><a href="javascript:;" class="jp-stop ui-icon ui-icon-stop" tabindex="1" title="{t}Stop{/t}">{t}Stop{/t}</a></li>

                <!--
                <li class="jp-repeat ui-state-default ui-corner-all"><a href="javascript:;" class="jp-repeat ui-icon ui-icon-refresh" tabindex="1" title="repeat">repeat</a></li>


                <li class="jp-repeat-off ui-state-default ui-state-active ui-corner-all"><a href="javascript:;" class="jp-repeat-off ui-icon ui-icon-refresh" tabindex="1" title="repeat off">repeat off</a></li>
                -->

                <li class="jp-mute ui-state-default ui-corner-all"><a href="javascript:;" class="jp-mute ui-icon ui-icon-volume-off" tabindex="1" title="{t}Mute{/t}">{t}Mute{/t}</a></li>
                <li class="jp-unmute ui-state-default ui-state-active ui-corner-all"><a href="javascript:;" class="jp-unmute ui-icon ui-icon-volume-off" tabindex="1" title="{t}Unmute{/t}">{t}Unmute{/t}</a></li>
                <li class="jp-volume-max ui-state-default ui-corner-all"><a href="javascript:;" class="jp-volume-max ui-icon ui-icon-volume-on" tabindex="1" title="{t}Max volume{/t}">{t}Max volume{/t}</a></li>
              </ul>
              <!--<div class="jp-progress-slider"></div>-->
              <div class="jp-volume-slider"></div>
              <div class="jp-current-time" style="display:none"></div>
              <!--<div class="jp-duration"></div>-->
              <!--<div class="jp-clearboth"></div>-->
            </div>
            <div class="clearfix"></div>
            <div class="listen-link-buttons">
              <a href="{$url}gradio/mp3" target="_blank" data-ot="{t}Direct mp3 stream link{/t}">
                <img src="{$url}themes/gradio/img/icons/mp3-logo-white.svg" alt="{t}Direct mp3 stream link{/t}">
              </a>
              <a href="{$url}gradio/aacp" target="_blank" data-ot="{t}Direct aac+ stream link{/t}">
                <img src="{$url}themes/gradio/img/icons/aac-logo-white.svg" alt="{t}Direct aac+ stream link{/t}">
              </a>
              <a href="{$url}gradio/m3u" target="_blank" data-ot="{t}Direct stream links{/t}">
                <img src="{$url}themes/gradio/img/icons/m3u-logo-white.svg" alt="{t}Direct stream links{/t}">
              </a>
            <!--
            <a href="{$url}gradio/m3u"
               class="external-listen-link"
               target="_blank"
               data-ot="{t}Direct stream links{/t}">
              <i class="fa fa-2x fa-external-link"></i>
            </a>
            -->
            </div>
          </div>

          <!--<div id="gradio-player-art" class="now-playing-art"><div class="now-playing-art-container"></div></div>-->
          <!--<div id="gradio-player-bio" class="now-playing-bio"><div class="now-playing-bio-container"></div></div>-->
        </div>
      </div>
      <div id="now-on-air" class="now-on-air col-xs-12 col-sm-5 col-sm-offset-0 col-sm-push-1 col-md-4 col-lg-4 col-lg-push-0 debu" style="display: none;">
        <div class="now-on-air-inner" style="display: none">
          <a href="http://www.google.lv/search?q=gradio.lv" id="google-search" target="_blank" title="Search about:">
            <div id="now-artist"></div>
            <div id="now-title"></div>
          </a>
          <div class="current-time"></div>
          <div class="big-progress">
            <div class="percent"></div>
            <div class="pbar"></div>
            <div class="elapsed"></div>
          </div>
          <div id="now-rem-time" class="remain-time"></div>
          <!-- AddToAny BEGIN -->
          <div class="a2a_kit a2a_kit_size_32 a2a_default_style col-xs-5">
            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
          </div>
          <!-- AddToAny END -->
          <div class="ratings">
            <div
              class="rate likes"
              data-item-id="0"
              data-action="liked"
              data-action-category="tune"
              data-ot="{t}Like this tune{/t}              ">
              <i class="fa fa-2x fa-thumbs-o-up"></i> <div class="pull-right like-count"></div>
            </div>
            <div
              class="rate dislikes"
              data-item-id="0"
              data-action="disliked"
              data-action-category="tune"
              data-ot="{t}Dislike this tune{/t}">
              <i class="fa fa-2x fa-thumbs-o-down"></i> <div class="pull-right dislike-count"></div>
            </div>
            <div class="comments pull-right">
              <a href="{$url}tune/0/" class="ajax-nav" data-ot="{t}Comment this tune{/t}">
                <i class="fa fa-2x fa-comments"></i>
                <span class="fb-comments-count like-count" data-href="{$fbsiteurl}/tune/0/">0</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id="now-on-air-s" class="now-small col-xs-8 col-xs-push-2 col-sm-5 col-sm-push-0 col-md-6 col-md-push-1 col-lg-7 col-lg-push-0" style="display: none;">
        <div id="now-artist-s"></div><div> | </div><div id="now-title-s"></div><div> | </div><div id="now-rem-time-s" class="remain-time"></div>
      </div>

      <!--<div class="jp-clearboth"></div>-->
      <div class="jp-no-solution">
        <span>Update Required</span>
        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
      </div>

      <!--<div class="toggle-player btn">Toggle</div>-->
      <div class="player-controls col-xs-2 col-xs-offset-1 col-sm-2 col-md-1 col-md-offset-0 col-md-push-0 col-lg-1 col-lg-offset-0 debu">
        <!--
        <div class="toggle-player" data-ot-title="Toggle player" data-ot="">
            <i class="fa fa-arrow-up"></i>
            <i class="fa fa-arrow-down" style="display: none;"></i>
        </div>
        -->
        <div class="refresh-player" data-ot="{t}Refresh{/t}">
          <i class="fa fa-refresh"></i>
        </div>
      </div>
    </div>
  </div>
{/block}
