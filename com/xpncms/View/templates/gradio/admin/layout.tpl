{"begin"|timer:"template head"}
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>{block name="title"}{$title}{/block}</title>

  {block name="meta-tags"}

  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

  {/block}
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- global css -->
  {block name="global-css"}

  <link href="{$url}{$paths.themes}/{$backendTheme}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- font Awesome -->
  <link href="{$url}{$paths.themes}/{$backendTheme}/vendors/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="{$url}{$paths.themes}/{$backendTheme}/css/styles/black.css" rel="stylesheet" type="text/css" id="colorscheme" />
  <link href="{$url}{$paths.themes}/{$backendTheme}/css/panel.css" rel="stylesheet" type="text/css"/>
  <link href="{$url}{$paths.themes}/{$backendTheme}/css/metisMenu.css" rel="stylesheet" type="text/css"/>
  
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.brighttheme.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.buttons.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.history.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.mobile.css"/>

  {/block}
  <!-- end of global css -->
    
  <!--page level css -->
  {block name="page-css"}

  <link href="{$url}{$paths.themes}/{$backendTheme}/vendors/fullcalendar/css/fullcalendar.css" rel="stylesheet" type="text/css" />
  <link href="{$url}{$paths.themes}/{$backendTheme}/css/pages/calendar_custom.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" media="all" href="{$url}{$paths.themes}/{$backendTheme}/vendors/jvectormap/jquery-jvectormap.css" />
  
  
  <link rel="stylesheet" href="{$url}{$paths.themes}/{$backendTheme}/vendors/tags/dist/bootstrap-tagsinput.css" />
  <link rel="stylesheet" href="{$url}{$paths.themes}/{$backendTheme}/vendors/tags/assets/app.css" />

  <link rel="stylesheet" href="{$url}{$paths.themes}/{$backendTheme}/vendors/animate/animate.min.css">
  <link rel="stylesheet" href="{$url}{$paths.themes}/{$backendTheme}/css/only_dashboard.css" />

  {/block}
  <!--end of page level css-->
  
  <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/elfinder.min.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/theme.css"/>
  
  {block name="global-header-js"}

  <script type="text/javascript" src="{$url}{$paths.themes}/{$backendTheme}/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/js/bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{$url}assets/underscore/underscore-min.js"></script>

  <script>
    var baseURL = "{$url}";
    var siteURL = "{$siteurl}";
    var fbsiteURL = "{$fbsiteurl}";
    var pageId = 'index';
    {if isset($userData.id)}

    var userID = {$userData.id};
    var userName = "{$userData.username}";
    {else}

    var userID = "guest";
    var userName = "guest";
    {/if}

    var appTitle = "{$title}";
    var appShortTitle = "{$shortTitle}";
    var appVersion = "{$version.number}";
    var now, Notify, simpleNotify, confirmNotify;
    var locale = "{$smarty.session._sf2_attributes.locale}";

    var headerRenderTime = '{"end"|timer}';
    var controller = '{$uri.controller}';
    var method = '{$uri.method}';
    var var_1 = '{$uri.var}';

    {literal}
    var templateRenderTimes = {
      header: headerRenderTime,
      body: false,
      footer: false
    };

    var requestData = {
      controller: controller,
      method: method,
      var : var_1
    };
    {/literal}
  </script>

  {if isset($debugbarRendererHead)}
  {$debugbarRendererHead}
  {/if}

  {/block}
</head>

{if isset($googleanalytics.id)}
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
{/literal}
  ga('create', '{$googleanalytics.id}', 'auto');
  ga('send', 'pageview');
</script>
{/if}

{if isset($piwik.id)}
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['setUserId', userName]);
  _paq.push(["setDocumentTitle", document.title]);
  _paq.push(["setDomains", ["*.gradio.lv"]]);
  // you can set up to 5 custom variables for each visitor
  _paq.push(["setDoNotTrack", true]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  _paq.push(['setSiteId', {$piwik.id}]);
  var u="{$piwik.url}";
  {literal}
  (function() {
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
  {/literal}
</script>
<!--
<noscript><p><img src="{$piwik.url}piwik.php?idsite={$piwik.id}" style="border:0;" alt=""></p></noscript>
-->
<!-- End Piwik Code -->
{/if}
<script type="text/javascript" src="{$url}js/errorlogger.js"></script>

<body class="skin-josh">
  <header class="header">
    <a href="{$url}admin" class="logo">
      <img src="{$url}{$paths.themes}/{$backendTheme}/img/g-radio-logo-v4-web.png" alt="Logo">
    </a>
    {include './nav.tpl'}
  </header>
  <div class="wrapper row-offcanvas row-offcanvas-left">
    {include './left.tpl'}
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
      <!-- Main content -->
      {block name="body"}
      {"begin"|timer:"template body"}
      
      <script>
        var bodyRenderTime = '{"end"|timer}';
        {literal}
        _.extend(templateRenderTimes, {body: bodyRenderTime});
        {/literal}
      </script>
      {/block}
    </aside>
    <!-- right-side -->
  </div>
  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
  </a>
  <!-- global js -->
  {block name="global-footer-js"}

  <!--livicons-->
  <script src="{$url}{$paths.themes}/{$backendTheme}/vendors/livicons/minified/raphael-min.js" type="text/javascript"></script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/vendors/livicons/minified/livicons-1.4.min.js" type="text/javascript"></script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/js/josh.js" type="text/javascript"></script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/js/metisMenu.js" type="text/javascript"> </script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/vendors/holder-master/holder.js" type="text/javascript"></script>
  
  <script type="text/javascript" src="{$url}assets/jquery-ujs/src/rails.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-address-old/jquery.address-1.6-mod.js"></script>
  <script type="text/javascript" src="{$url}assets/js-logger/src/logger.js"></script>
  <script type="text/javascript" src="{$url}assets/extend.js/src/extend.js"></script>
  <script type="text/javascript" src="{$url}assets/jsmart/jsmart.min.js"></script>
  
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.animate.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.buttons.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.confirm.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.nonblock.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.desktop.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.history.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.callbacks.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.mobile.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.desktop.js"></script>
  
  <script type="text/javascript" src="{$url}js/google/google.js"></script>
  <script src="https://apis.google.com/js/client.js?onload=gInit"></script>
  <script type="text/javascript" src="{$url}js/addtoany.js"></script>
  
  <script type="text/javascript">
  {if isset($smarty.session.debug) && $smarty.session.debug == "true" }
  // Only log WARN and ERROR messages.
  //Logger.setLevel(Logger.WARN);
  Logger.useDefaults();
  //Logger.debug({$smarty.session._sf2_attributes.debug});
  //Logger.debug({$smarty.session._sf2_attributes.debug});
  Logger.debug("Debug is set to: {$smarty.session._sf2_attributes.debug}");
  {else}

  Logger.setLevel(Logger.OFF);
  Logger.debug("Debug is set to: off");
  {/if}

  </script>
  
  <script src="{$url}js/translate.js"></script>
  {if $locale_file}
  <script src="{$url}{$locale_file}"></script>
  {/if}
  
  
  <script type="text/javascript" src="{$url}js/init.js"></script>
  <script type="text/javascript" src="{$url}js/pages.js"></script>
  <script type="text/javascript" src="{$url}js/tabs.js"></script>
  <script type="text/javascript" src="{$url}js/notify.js"></script>
  
  {if isset($ajax) && $ajax == false}
  <script type="text/javascript">
    pageId = "{$pageId}";
    xpnCMS.loadPage(pageId);
  </script>
  {/if}

  {/block}
  <!-- end of global js -->
  
  <!-- begining of page level js -->
  {block name="page-footer-js"}

  {/block}
  <!-- end of page level js -->
</body>
</html>
