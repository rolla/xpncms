<h3>{t}Latest content{/t}</h3>
<a href="{$url}admin/addcontent" class="btn btn-default btn-sm btn-responsive" role="button">
  <span class="glyphicon glyphicon-pencil"></span><br> {t}Add{/t}
</a>

{if isset($contents) }
  {foreach $contents as $content name=contents}
    {if $smarty.foreach.contents.index == $count}
      {break}
    {/if}
    <div class="news-blocks">
        <h3>
            <a href="{$url}admin/editcontent/{$content.cont_id}/">{$content.cont_name}</a>
        </h3>
        <div class="news-block-tags">
            <strong><a href="{$url}content/{$content.cat_alias}/">{$content.cont_category}</a></strong>
            <em>{$content.cont_added|relative_date}</em>
        </div>
        <p>
            <img
              class="news-block-img pull-right"
              alt=""
              src="{$url}libs/phpThumb/phpThumb.php?q=80&w=40&h=40&zc=1&src=../../{$paths.images}/{$content.cont_imgs}">
            {$content.cont_content|strip_tags|truncate:400}
        </p>
        <a href="{$url}content/{$content.cont_alias}/" class="news-block-btn">
            {t}View{/t}
            <i class="m-icon-swapright m-icon-black"></i>
        </a>
    </div>
  {/foreach}
{/if}
