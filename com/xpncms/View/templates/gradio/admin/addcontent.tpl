{block name="body" append}

{function name=menu level=0}
  {foreach $data as $entry}
    <option value="{$entry.id}">{str_repeat("--", $level)} {$entry.name}</option>
    {if is_array($entry.sub_category)}
      {menu data=$entry.sub_category level=$level+1}
    {else}
    {/if}
  {/foreach}
{/function}

<section class="content-header">
<h1>{$title} {t}Admin panel{/t}</h1>
<ol class="breadcrumb">
  <li>
  <a href="{$url}admin">
    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
    {t}Home{/t}
  </a>
  </li>
  <li>
    <a href="{$url}admin/content">{t}Content{/t}</a>
  </li>
  <li class="active"><a href="{$url}admin/addcontent">{t}Add{/t}</a></li>
</ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-3">
      {include './latestcontent.tpl' count=7}
    </div>
    <div class="col-md-6">
      <h3>{t}Add content{/t}</h3>
      <fieldset>
        <form id="save-form" action="{$url}admin/savecontent/" method="post" class="form-horizontal" role="form">
          <input type="hidden" name="add_content" value="1">
          <input type="hidden" name="content_type" value="content">
          
          <a href="{$url}admin/#/tab/content" class="btn btn-danger ajax-nav">
            <i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
          </a>
          <button type="submit" class="btn btn-success adm-save">
            <i class="fa fa-save"></i> {t}Save{/t}
          </button>
          
          <br /><br />
          
          <div class="form-group">
            <label class="col-sm-1 control-label" for="inputnosaukums">{t}Name{/t}</label>
            <div class="col-sm-11">
              <input class="form-control" type="text" name="cont_name"  id="cont_name" value="" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label" for="cont_alias">{t}Slug{/t}</label>
            <div class="col-sm-11">
              <input class="form-control" type="text" name="cont_alias" id="cont_alias" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label" for="inputsadala">{t}Category{/t}</label>
            <div class="col-sm-11">
              <select class="form-control" name="cont_category">
                <option value="0">Parent</option>
              {if isset($categories) }
                {* run the array through the function *}
                {menu data=$categories}
              </select>
              {else}
              {/if}
            </div>
          </div>
          <div class="edit-sat">
            <textarea id="saturs" class="ckeditor" name="cont_content" rows="15" cols="100" style="width: 100%"></textarea>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label" for="inputbild">{t}Images{/t}</label>
            <div class="col-sm-11">
              <img class="cont_imgs_add" src="{$url}themes/{$template}/img/default/{$images.defaddcontent}" width="100%">
              <input class="form-control cont_imgs_added" type="hidden" name="cont_imgs" id="cont_imgs" value="" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label" for="cont_tags">{t}Tags{/t}</label>
            <div class="col-sm-11">
              <input class="form-control" type="text" name="cont_tags" id="cont_tags" value="" data-role="tagsinput" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label" for="inputbild">{t}Order{/t}</label>
            <div class="col-sm-11">
              <input class="form-control" type="text" name="cont_order" id="cont_order" value="" />
            </div>
          </div>
          <a href="{$url}admin/content" class="btn btn-danger ajax-nav">
            <i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
          </a>
          <button type="submit" class="btn btn-success adm-save">
            <i class="fa fa-save"></i> {t}Save{/t}
          </button>
        </form>
      </fieldset> 
      </div>
    </div>
  </div>
</section>
{/block}

{block name="global-footer-js" prepend}
  <script type="text/javascript" src="{$url}assets/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="{$url}assets/ckeditor/adapters/jquery.js"></script>
{/block}

{block name="page-footer-js" append}
  <script type="text/javascript" src="{$url}assets/elfinder/js/elfinder.full.js"></script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/vendors/tags/dist/bootstrap-tagsinput.js"></script>
{/block}
