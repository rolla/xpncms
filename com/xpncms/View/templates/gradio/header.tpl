{"begin"|timer:"template head"}
<!DOCTYPE HTML>
<html>
  <head>
  <meta http-equiv="content-type" content="{$meta.contenttype}">
  <meta http-equiv="content-style-type" content="{$meta.contentstyletype}">
  <meta http-equiv="content-language" content="{$meta.contentlanguage}">
  <title>{block name="title"}{$title}{/block}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="description" content="{$meta.content}">
  <meta name="keywords" content="{$meta.keywords}">
  <meta name="copyright" content="{$meta.copyright}">
  <meta name="author" content="{$meta.author}">
  <meta name="robots" content="{$meta.robots}">
  <meta name="abstract" content="{$meta.abstract}">
  <meta name="distribution" content="{$meta.distribution}">
  <meta name="web_author" content="{$meta.webauthor}">
  <meta name="google-site-verification" content="{$meta.googlesitev}">

  <!-- facebook -->
  <meta property="fb:app_id" content="{$meta.fbappid}">
  <meta property="fb:admins" content="{$meta.fbadmins}">
  <meta property="og:title" content="{$meta.ogtitle}">
  <meta property="og:type" content="{$meta.ogtype}">
  <meta property="og:url" content="{$siteurl}{$smarty.server.REQUEST_URI}">
  <meta property="og:description" content="{$meta.content}">
  <meta property="og:image" content="{$meta.ogimage}">
  {if $meta.ogmusicduration}
  <meta property="music:duration" content="{$meta.ogmusicduration}">
  {/if}
  {if $meta.ogarticlesection}
  <meta property="article:section" content="{$meta.ogarticlesection}">
  {/if}
  <!-- /facebook -->

  <!-- twitter -->
  <meta name="twitter:card" content="{$meta.twittercard}">
  <meta name="twitter:site" content="{$meta.twittersite}">
  <meta name="twitter:creator" content="{$meta.twittercreator}">
  <meta name="twitter:title" content="{$meta.twittertitle}">
  <meta name="twitter:description" content="{$meta.twitterdescription}">
  <meta name="twitter:image" content="{$meta.twitterimage}">
  {block name="twitter_card_extend"}{/block}
  <!-- /twitter -->

  <link rel="icon" href="{$url}img/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="{$url}img/favicon.ico" type="image/xicon">
  {asset_compile}
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300&subset=latin,latin-ext">
  <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.10.3/themes/trontastic/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap-theme.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap-social/bootstrap-social.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/fontawesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.brighttheme.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.buttons.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.history.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.mobile.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}themes/gradio/css/opentip-xpn-cms.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/player/css/gplayer.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/nprogress/nprogress.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/VerticalIconMenu/css/component.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/main.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/normalize.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/DataTables/media/css/jquery.dataTables_themeroller.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/datatables-bootstrap3/BS3/assets/css/datatables.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/elfinder.min.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/theme.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"/>
  {/asset_compile}
  {asset_compile}
  <link rel="stylesheet" type="text/css" href="{$url}css/default/style.css"/>
  <link rel="stylesheet" type="text/css" href="{$url}themes/gradio/css/gradio.css"/>
  {/asset_compile}
  <!-- Core -->
  <script>
    var baseURL = "{$url}";
    var siteURL = "{$siteurl}";
    var fbsiteURL = "{$fbsiteurl}";
    var coversURL = "{$plugins.gradio.prefs.covers}";
    {if isset($userData.id)}

    var userID = {$userData.id};
    var userName = "{$userData.username}";
    {else}

    var userID = "guest";
    var userName = "guest";
    {/if}

    var appTitle = "{$title}";
    var appShortTitle = "{$shortTitle}";
    var appVersion = "{$version.number}";
    var now, Notify, simpleNotify, confirmNotify;
    var locale = "{$smarty.session._sf2_attributes.locale}";

    var headerRenderTime = '{"end"|timer}';
    var controller = '{$uri.controller}';
    var method = '{$uri.method}';
    var var_1 = '{$uri.var}';

    {literal}
    var templateRenderTimes = {
      header: headerRenderTime,
      body: false,
      footer: false
    };

    var requestData = {
      controller: controller,
      method: method,
      var : var_1
    };
    {/literal}
  </script>
  {asset_compile}
  <script type="text/javascript" src="{$url}assets/jquery/jquery.min.js"></script>
  <script type="text/javascript" src="{$url}assets/underscore/underscore-min.js"></script>
  {/asset_compile}
  {asset_compile}
  {if isset($debugbarRendererHead)}
  {$debugbarRendererHead}
  {/if}
  {/asset_compile}

</head>

{if isset($googleanalytics.id)}
<script>
{literal}
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
{/literal}
  ga('create', '{$googleanalytics.id}', 'auto');
  ga('send', 'pageview');
</script>
{/if}

{if isset($piwik.id)}
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['setUserId', userName]);
  _paq.push(["setDocumentTitle", document.title]);
  _paq.push(["setDomains", ["*.gradio.lv"]]);
  // you can set up to 5 custom variables for each visitor
  _paq.push(["setDoNotTrack", true]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  _paq.push(['setSiteId', {$piwik.id}]);
  var u="{$piwik.url}";
  {literal}
  (function() {
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
  {/literal}
</script>
<!--
<noscript><p><img src="{$piwik.url}piwik.php?idsite={$piwik.id}" style="border:0;" alt=""></p></noscript>
-->
<!-- End Piwik Code -->
{/if}
<script type="text/javascript" src="{$url}js/errorlogger.js"></script>

{block name="body"}
  {"begin"|timer:"template body"}
  <body>
  <div class="bg-eff"></div>
  <div class="container-fluid profile-row">
    {profile_row}
  </div>

  <div class="grid">
    <div class="container saturs">
    {if isset($newPlayer)}
      {include './gradio/newplayer.tpl'}
    {else}
      {include './gradio/player.tpl'}
    {/if}
    <!--
    <br>
    <br>
    <div class="alert alert-warning">Tehnisku iemeslu dēļ radio šodien nestrādā, atvainojamies par sagādātajām neertībām :(</div>
    -->

    <nav class="navbar navbar-default navbar-fixed-top gradio-menu">
      <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar">-</span>
        <span class="icon-bar">-</span>
        <span class="icon-bar">-</span>
      </button>
      </div>

      <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li>
        <a href="{$url}" class="sakums ajax-nav">{t}Start{/t}</a>
        </li>
        {if ($gradioTeam)}
        <li>
        <a href="{$url}online/" class="ajax-nav">{t}Online{/t}</a>
        </li>
        {/if}

        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{t}Categories{/t} <b class="caret"></b></a>
        <ul class="dropdown-menu">
          {front_nav}
        </ul>
        </li>
        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{t}Tunes{/t} <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li class="nav-header">{t}Gradio{/t}</li>
          {if ($gradioTeam)}
          <li>
          <a href="{$url}gradio/queue/" class="ajax-nav">{t}Queue tunes{/t}</a>
          </li>
          {/if}
          <li>
          <a href="{$url}gradio/likesdislikes/" class="ajax-nav">{t}Likes{/t} / {t}Dislikes{/t}</a>
          </li>
          <li>
          <a href="{$url}gradio/recent/" class="ajax-nav">{t}Recently played tunes{/t}</a>
          </li>
          {if ($gradioTeam)}
          <li>
          <a href="{$url}gradio/songs/" class="ajax-nav">{t}Tunes{/t}</a>
          </li>
          {/if}
          <li class="divider"></li>
        </ul>
        </li>
        {ifadmin}
        {if ($gradioTeam)}
        <li>
        <a href="{$url}admin/" class="ajax-nav">{t}Admin{/t}</a>
        </li>
        <li>
        <a href="javascript:location.reload(true)">{t}Reload{/t}</a>
        </li>
        {/if}
      </ul>
      </div>
    </nav>

    <ul class="cbp-vimenu">
      <li>
      <a href="{$url}" class="sakums ajax-nav">{t}Start{/t}</a>
      </li>
      {if ($gradioTeam)}
      <li>
      <a href="{$url}online/" class="ajax-nav">{t}Online{/t}</a>
      </li>
      {/if}

      <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">{t}Categories{/t} <b class="caret"></b></a>
      <ul class="dropdown-menu">
        {front_nav}
      </ul>
      </li>
      <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">{t}Tunes{/t} <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <!--<li class="nav-header">{t}Gradio{/t}</li>-->
        {if ($gradioTeam)}
        <li>
        <a href="{$url}gradio/queue/" class="ajax-nav">{t}Queue tunes{/t}</a>
        </li>
        {/if}
        <li>
        <a href="{$url}gradio/likesdislikes/" class="ajax-nav">{t}Likes{/t} / {t}Dislikes{/t}</a>
        </li>
        <li>
        <a href="{$url}gradio/recent/" class="ajax-nav">{t}Recently played tunes{/t}</a>
        </li>
        {if ($gradioTeam)}
        <li>
        <a href="{$url}gradio/songs/" class="ajax-nav">{t}Tunes{/t}</a>
        </li>
        {/if}
        <!--<li class="divider"></li>-->
      </ul>
      </li>
      {ifadmin}
      {if ($gradioTeam)}
      <li>
      <a href="{$url}admin/" class="ajax-nav">{t}Admin{/t}</a>
      </li>
      <li>
      <a href="javascript:location.reload(true)">{t}Reload{/t}</a>
      </li>
      {/if}
    </ul>

    <div class="loading-effect">
      <div class="dyn-content">
      <div class="row">
        <div class="genres-test row" style="margin-top: 100px; background: #fff; display: none;">Testing G! genres
        <!--
        <p>
          <input type="text" id="loadURL" size="40" value="{$url}img/logos/svg/kvadrats-apkart-300-2.svg">
          <input type="checkbox" id="addTo" value="yes"> <label for="addTo">Add to</label>
          <button type="button" id="loadExternal">Load</button>
        </p>
        -->

        <ul id="g-genres">
          <li class="btn g-genre" data-genre="g">g žanrs</li>
          <li class="btn g-genre" data-genre="live">live</li>
          <li class="btn g-genre" data-genre="house">house</li>
          <li class="btn g-genre" data-genre="electro">electro</li>
          <li class="btn g-genre" data-genre="dance">dance</li>
          <li class="btn g-genre" data-genre="hard">hard</li>
          <li class="btn g-genre" data-genre="slow">slow</li>
        </ul>

        <div id="svgContainer">
          <!--
          <div id="svgload" class="svgdiv hasSVG" style="width: 100%; height: 300px;">
          <svg version="1.1"><svg x="0" y="0" width="0" height="0" class="svg-graph"></svg>
          <svg x="0" y="0" width="0" height="0" class="svg-plot"></svg>
          </svg>
          </div>
          -->
        </div>
        </div>
      </div>

      <script>
        var bodyRenderTime = '{"end"|timer}';
        {literal}
        _.extend(templateRenderTimes, {body: bodyRenderTime});
        {/literal}
      </script>

      {/block}
