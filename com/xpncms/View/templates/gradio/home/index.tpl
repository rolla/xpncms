{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="body" append}
  <div class="row">

    <div class="col-xs-5 col-sm-5 col-md-3 col-lg-3">
      <div id="recently-played-block" class="recently-played-block">
        <span class="text-center recently-played-block-title">{t}Recently played tunes{/t}</span>
        <a href="{$url}tune/0" class="ajax-nav recently-played-block-songid">
          <div class="recently-played-block-image" style="">
            <div class="recently-played-block-artist text-center"></div>
            <div class="recently-played-block-songtitle text-center"></div>
            <div
              class="recently-played-block-mask"
              style="background-image: url({$url}themes/{$template}/img/default/recently-played-mask.svg)">
            </div>
          </div>
        </a>
      </div>
      <!--
      <div class="google-calendar-block">
        <iframe src="https://www.google.com/calendar/embed?showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=350&amp;wkst=2&amp;hl=lv&amp;bgcolor=%23ffffff&amp;src=u3nta5c23m14ss5ek5uss1rft4%40group.calendar.google.com&amp;color=%23853104&amp;ctz=Europe%2FRiga" style=" border-width:0 " width="277" height="350" frameborder="0" scrolling="no"></iframe>
      </div>
      -->
    </div>

    <div class="col-xs-7 col-sm-7 col-md-5 col-lg-5 infinite-scroll">
      {include file="content/plain.tpl"}
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="music-stream-block">
        <div id="gradio-player-bio" class="now-playing-bio"><div class="now-playing-bio-container"></div></div>
      </div>
    </div>
  </div>
{/block}
