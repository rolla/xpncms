{block name="body" append}
  {"begin"|timer:"template footer"}
  <div class="clearfix"></div>
  <br>

  <!--
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
      <img src="http://fc06.deviantart.net/fs71/f/2012/310/8/1/untitled_drawing_by_northdakotaoc-d5k7xci.png" class="suncic">
    </div>
  </div>
  -->

</div>

<div id="fb-root"></div>
<div class="clearfix"></div>

<div class="row soc-pogas center">
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div id="tweetBtn"></div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div id="draugiemLike"></div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div
      class="fb-share-button"
      data-href="{$siteurl}{$smarty.server.REQUEST_URI}"
      data-send="false"
      data-type="button_count"
      data-width="450"
      data-show-faces="true"
      data-colorscheme="light">
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div id="toTop" class="btn btn-large btn-primary">{t}To top{/t}</div>

<div class="clearfix"></div>
<!-- FOOTER -->
<footer class="footer">
  {social_icons}
  <div class="footer-content">
    <a class="footer-img" href="//urdt.lv/?bannerclient=gradio.lv" target="_blank" ><img src="{$url}img/logos/urdt-cr-logo.png"/></a>
    <p class="footer-cr" style="display:none"><a href="//urdt.lv/?bannerclient=gradio" target="_blank" >&copy; 2016 Universal Radio DJ Team v4.0</a></p>
  </div>
</footer>
<div class="clearfix"></div>
</div> <!-- /loading-effect -->
</div><!-- /container -->

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

{asset_compile}
<script type="text/javascript" src="{$url}js/twitter.js"></script>
<script type="text/javascript" src="{$url}js/facebook.js"></script>
<script type="text/javascript" src="{$url}js/draugiem.js"></script>
{/asset_compile}

{asset_compile}
<script type="text/javascript" src="{$url}assets/jquery/jquery-migrate.js"></script>
<script type="text/javascript" src="{$url}assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-lastfm/lastfm.jquery.js"></script>
<script type="text/javascript" src="{$url}assets/jplayer/dist/jplayer/jquery.jplayer.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-address-old/jquery.address-1.6-mod.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-easing-original/jquery.easing.compatibility.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.animate.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.buttons.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.confirm.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.nonblock.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.desktop.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.history.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.callbacks.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.mobile.js"></script>
<script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.desktop.js"></script>
<script type="text/javascript" src="{$url}assets/player/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="{$url}assets/player/js/jplayer.playlist.min.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-resizeimagetoparent/jquery.resizeimagetoparent.js"></script>
<script type="text/javascript" src="{$url}assets/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="{$url}assets/opentip/downloads/opentip-jquery-excanvas.min.js"></script>
<script type="text/javascript" src="{$url}assets/VerticalIconMenu/js/modernizr.custom.js"></script>
<script type="text/javascript" src="{$url}assets/swfobject/swfobject/swfobject.js"></script>
<script type="text/javascript" src="{$url}assets/html5-boilerplate/js/plugins.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="{$url}assets/player/js/gplayer.js"></script>
<script type="text/javascript" src="{$url}assets/player/js/progress.js"></script>
<script type="text/javascript" src="{$url}assets/nprogress/nprogress.js"></script>
<script type="text/javascript" src="{$url}assets/CanvasLoader/js/heartcode-canvasloader-min.js"></script>
<script type="text/javascript" src="{$url}assets/DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="{$url}assets/datatables-bootstrap3/BS3/assets/js/datatables.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-form/jquery.form.js"></script>
<script type="text/javascript" src="{$url}assets/js-logger/src/logger.js"></script>
<script type="text/javascript" src="{$url}assets/extend.js/src/extend.js"></script>
{/asset_compile}

<script type="text/javascript" src="{$url}assets/ckeditor/ckeditor.js"></script>
{asset_compile}
<script type="text/javascript" src="{$url}assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
{/asset_compile}
<script type="text/javascript" src="{$url}assets/elfinder/js/elfinder.full.js"></script>

{asset_compile}
<script type="text/javascript" src="{$url}assets/jquery-seo-url/jquery.seourl.min.js"></script>
<script type="text/javascript" src="{$url}assets/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="{$url}assets/jquery-infinite-scroll/jquery.infinitescroll.min.js"></script>
<script type="text/javascript" src="{$url}assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{$url}assets/bootstrap-datepicker/dist/locales/bootstrap-datepicker.lv.min.js"></script>
<script type="text/javascript" src="{$url}assets/emojione/lib/js/emojione.min.js"></script>
<script type="text/javascript" src="{$url}assets/jsmart/jsmart.min.js"></script>
<script type="text/javascript" src="{$url}js/google/google.js"></script>
{/asset_compile}
<script src="https://apis.google.com/js/client.js?onload=gInit"></script>
<script type="text/javascript" src="{$url}js/addtoany.js"></script>

{asset_compile}
<!-- gradio js -->
<script type="text/javascript">
  {if isset($smarty.session.debug) && $smarty.session.debug == "true" }
  // Only log WARN and ERROR messages.
  //Logger.setLevel(Logger.WARN);
  Logger.useDefaults();
  //Logger.debug({$smarty.session._sf2_attributes.debug});
  //Logger.debug({$smarty.session._sf2_attributes.debug});
  Logger.debug("Debug is set to: {$smarty.session._sf2_attributes.debug}");
  {else}

  Logger.setLevel(Logger.OFF);
  Logger.debug("Debug is set to: off");
  {/if}

</script>

<script type="text/javascript">
  {literal}
    Opentip.styles.xpncms = {
      extends: "dark",
      delay: 0.3,
      stemLength: 10,
      stemBase: 10,
      borderWidth: 3,
      borderRadius: 5,
      borderColor: "#fff",
      color: "#000",
      background: [[0, "#C4F83E"], [1, "#D2F870"]],
      closeButtonCrossColor: "rgba(255, 255, 255, 1)",
      tipJoint: "bottom left",
      removeElementsOnHide: true
    };
    Opentip.debug = false;
    Opentip.defaultStyle = "xpncms";
  {/literal}

    $(function ($) {
      $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
          $("#toTop").fadeIn();
        } else {
          $("#toTop").fadeOut();
        }
      });

  {literal}
      $("#toTop").click(function () {
        $("body,html").animate({scrollTop: 0}, 800);
      });

      $(".suncic").delay(1000).slideUp(2000, function () {
        $(".masivs").animate({"margin-top": "70px"}, 2000);
      });
  {/literal}

      $(".footer-content").hover(function () {
        $(".footer-img").fadeOut();
        $(".footer-cr").fadeIn("1000");
      },
      function () {
        $(".footer-cr").fadeOut("1000");
        $(".footer-img").fadeIn();
      });

      $(document).on("click", ".fast-user-settings", function () {
        var exp_elem = $(this).parent().find(".fast-user-settings-expand");
        exp_elem.fadeToggle(function () {
          if (exp_elem.is(":visible")) {
            $(this).parent().find(".user-chevron").removeClass("fa-chevron-down").addClass("fa-chevron-up");
          } else {
            $(this).parent().find(".user-chevron").removeClass("fa-chevron-up").addClass("fa-chevron-down");
          }
        });
      });

    });
</script>
{/asset_compile}

<script src="{$url}js/translate.js"></script>
{if $locale_file}
  <script src="{$url}{$locale_file}"></script>
{/if}

{asset_compile}
<script type="text/javascript" src="{$url}js/init.js"></script>
<script type="text/javascript" src="{$url}js/gradio/gradio.js"></script>
<script type="text/javascript" src="{$url}js/gradio/gradio.player.js"></script>
<script type="text/javascript" src="{$url}js/gradio/gradio.lastfm.js"></script>
<script type="text/javascript" src="{$url}js/gradio/gradio.twitter.js"></script>
<script type="text/javascript" src="{$url}js/gradio/gradio.datatables.js"></script>

<script type="text/javascript" src="{$url}js/pages.js"></script>
<script type="text/javascript" src="{$url}js/tabs.js"></script>
<script type="text/javascript" src="{$url}js/notify.js"></script>
{/asset_compile}

{asset_compile}
<script type="text/javascript">
    $(document).ajaxStart(function () {
      xpnCMS.NProgressStart();
    });

    $(document).ajaxStop(function () {
      xpnCMS.NProgressStop();
    });

    $(document).on("click", ".adm-del", function () {
      var href = $(this).attr("data-href");
      var alert = $(this).attr('data-confirm');
      var notify = {
        title: t("Please confirm"),
        alert: alert,
        type: "error"
      }

      xpnCMSNotify.confirmNotify(notify, href);
      return false;
    });
</script>
{/asset_compile}

</div><!-- /.grid -->

{if isset($ajax) && $ajax == false}
  <script type="text/javascript">
    xpnCMS.loadPage("{$pageId}");
  </script>
{/if}

<!-- Modal -->
<div id="dataConfirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="dataConfirmModallabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="dataConfirmModal">{t}Please confirm{/t}</h3>
  </div>
  <div class="modal-body"></div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">{t}Cancel{/t}</button>
    <a href="" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dataConfirmOK">{t}Delete{/t}</a>
  </div>
</div>

<script>
  var footerRenderTime = '{"end"|timer}';
  {literal}
    _.extend(templateRenderTimes, {footer: footerRenderTime});
  {/literal}
</script>

{if isset($debugbarRender)}
  {$debugbarRender}
{/if}

<!-- {"list"|timer:true} -->

{block name="footerjs"}
{/block}

</body>
</html>
{/block}
