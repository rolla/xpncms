{block name="body" append}
<table width="100%" border="0" cellpadding="2" cellspacing="2" class="table">
  <tr>
    <td valign="top">
		<fieldset>
        <legend>{t}Your profile information{/t}</legend>
		<table width="100%" cellspacing="0" cellpadding="3" border="0">
		<tbody>
		  <tr>
			<td width="10%"><b>{t}User ID{/t}</b></td>
			<td width="83%">&nbsp; {$userData.id}</td>
		  </tr> 
		  <tr>
			<td width="10%"><b>{t}E-mail{/t}</b></td>
			<td width="83%">&nbsp; {$userData.email}</td>
		  </tr> 
		  <tr>
			<td width="10%"><b>{t}First name{/t}</b></td>
			<td width="83%">&nbsp; {$userData.first_name}</td>
		  </tr> 
		  <tr>
			<td width="10%"><b>{t}Last name{/t}</b></td>
			<td width="83%">&nbsp; {$userData.last_name}</td>
		  </tr> 
		  <tr>
			<td width="10%"><b>{t}Password{/t}</b> {t}Generated{/t}</td>
			<td width="83%">&nbsp; <b style="color:green">{$userData.password}</b></td>
		  </tr> 
		</tbody>
		</table>
      </fieldset>
	</td>
  </tr>
</table>

{connect_disconnect}

{*
<table width="100%" border="0" cellpadding="2" cellspacing="2">
  <tr>
	<td valign="top">
		<fieldset>
		<legend>{t}Associated authentications{/t}</legend> 
			<table width="100%" cellspacing="0" cellpadding="3" border="0">
			<tbody>
			<?php foreach($user_authentication as $key => $val){ ?>
				<tr>
				<td><b>---------</b></td>
				<td>---------</td>
			  </tr>
			  <tr>
				<td width="15%"><b>{t}Provider{/t}</b></td>
				<td width="85%">&nbsp; <?php echo $val["provider"]; ?></td>
			  </tr> 
			  <tr>
				<td><b>{t}Provider UID{/t}</b></td>
				<td>&nbsp; <?php echo $val["provider_uid"]; ?></td>
			  </tr>
			  <tr>
				<td><b>{t}Show as{/t}</b></td>
				<td>&nbsp; <?php echo $val["display_name"]; ?></td>
			  </tr>  
			  <tr>
				<td><b>{t}Profile URL{/t}</b></td>
				<td>&nbsp; <?php echo $val["profile_url"]; ?></td>
			  </tr>
			  <?php } ?>
			</tbody>
			</table> 
	  </fieldset>
	</td>
  </tr>
</table>
<br />
&nbsp; <b style="color:red">{t}Note{/t}</b>: {t}You can login with your{/t} <b>{t}E-mail{/t}</b> {t}and{/t} <b>{t}Password{/t}</b> {t}or{/t} {t}using your{/t} <b><?php echo $user_authentication[0]["provider"]; ?> {t}account{/t}</b>.
<?php } else { ?> 
&nbsp; <b style="color:red">{t}Note{/t}/b>: {t}This user do not have any provider{/t}
<?php } ?>
*}
{/block}