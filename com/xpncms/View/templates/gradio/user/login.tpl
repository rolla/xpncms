{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="body" append}
  <div class="row">
    <div class="col-xs-12 col-md-6 col-lg-5">
      <fieldset>
        <legend>{t}Sign in with social network{/t}</legend>
        <a href="{$url}auth/connect/facebook/" class="btn btn-block btn-social btn-facebook auth">
          <i class="fa fa-facebook"></i> {t}Sign in with{/t} {t}Facebook{/t}
        </a>
        <a href="{$url}auth/connect/twitter/" class="btn btn-block btn-social btn-twitter auth">
          <i class="fa fa-twitter"></i> {t}Sign in with{/t} {t}Twitter{/t}
        </a>
        <a href="{$url}auth/connect/draugiem/" class="btn btn-block btn-social btn-draugiem auth">
          <i class="fa fa-user"></i> {t}Sign in with{/t} {t}Draugiem{/t}
        </a>
        <a href="{$url}auth/connect/google/" class="btn btn-block btn-social btn-google auth">
          <i class="fa fa-google-plus"></i> {t}Sign in with{/t} {t}Google{/t}
        </a>
      </fieldset>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-offset-2 col-lg-5">
      <img src="{$url}img/logos/only-g-big-without-gloss-web.jpg" style="width: 100%" alt="{t}Only G{/t}">
    </div>
  </div>
{/block}
