<div class="row content-item-likes">
    <span class="col-md-5 likes-count count">{count($content.likes)}</span> 
    <i
      class="col-md-7 fa fa-5x fa-thumbs-up rate"
      data-action="liked"
      data-action-category="news"
      data-item-id="{$content.cont_id}"
      data-page-id="{$pageId}"
      data-partial="_part_ratings">
    </i>
    {if $content.likes}
        <div class="likers-container">
        {foreach $content.likes as $likes}
            <span class="liker pull-left">
              <!--
              <img src="{userimage user_id=$likes.user_id photo_url=$likes.photo_url phpthumb='w=50&q=80'}">
              -->
              <span class="liker name clearfix">{$likes.display_name|emoji}</span></span>
        {/foreach}
        </div>
    {/if}
</div>
<div class="row content-item-dislikes">
    <span class="col-md-5 dislikes-count count">{count($content.dislikes)}</span> 
    <i
      class="col-md-7 fa fa-5x fa-thumbs-down rate"
      data-action="disliked"
      data-action-category="news"
      data-item-id="{$content.cont_id}"
      data-page-id="{$pageId}"
      data-partial="_part_ratings">
    </i>
    {if $content.dislikes}
        <div class="dislikers-container">
        {foreach $content.dislikes as $dislikes}
            <span class="disliker pull-left">
            <!--
            <img src="{userimage user_id=$dislikes.user_id photo_url=$dislikes.photo_url phpthumb='w=50&q=80'}">
            -->
            <span class="disliker name clearfix">{$dislikes.display_name|emoji}</span></span>
        {/foreach}
        </div>
    {/if}
</div>
