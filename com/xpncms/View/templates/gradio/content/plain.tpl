{if !count($contents) }
  <h1 class="alert alert-danger">{t}Content not found{/t}</h1>
{else}
  {foreach $contents as $content}
    <div class="recent-news-block">
      <a
        href="{$url}content/{$content.cont_alias}/"
        class="ajax-nav"
        data-href="{$url}content/{$content.cont_alias}/" >
        <span class="text-center recent-news-block-title">{$content.cont_name}</span>
      </a>
      <div class="recent-news-block-inner">
        <div
          class="recent-news-block-image"
          style="background-image: url({$url}libs/phpThumb/?q=100&h=200&zc=1&src={$url}{$paths.images}/{$content.cont_imgs|escape:'url'})">
          <div class="recent-news-block-content">
            {$content.cont_content|emoji|strip_tags:"<br><br /><br/><p><div><h1>"}
          </div>
          <div
            class="recent-news-block-mask"
            style="background-image: url({$url}themes/{$template}/img/default/recent-news-mask.svg)">
          </div>
          <div
            class="recent-news-block-date">
            <i class="fa fa-clock-o"></i> {$content.cont_added|relative_date}
          </div>
          <div class="recent-news-block-comments"><i class="fa fa-comments"></i>
            <a href="{$url}content/{$content.cont_alias}/" class="ajax-nav">
              <span class="fb-comments-count" data-href="{$fbsiteurl}/content/{$content.cont_alias}/">0</span> {t}Comments{/t}
            </a>
          </div>
          <div class="recent-news-block-tag">
            <i class="fa fa-tag"></i>
            <a href="{$url}category/{$content.cat_alias}/" class="ajax-nav">
              {$content.cont_category|emoji}
            </a>
          </div>
        </div>
      </div>
    </div>
  {/foreach}
    <div class="infinitescroll-nav">
      <a
        href="{$url}content/pageplain/2/?ajax=true"
        id="next"
        class="btn btn-default btn-block"
        rel="nofollow">
        {t}More{/t}
      </a>
    </div>
{/if}
