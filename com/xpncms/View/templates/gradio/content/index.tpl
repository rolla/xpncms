{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="body" append}
  <h3>{t}All news{/t} / {t count=count($contents) 1=count($contents) plural="%1 news items"}%1 news item{/t}</h3>
  <div class="row">
    <div class="col-md-12">
      <div class="content-container">
        <div class="content-container-inner">

          {if isset($contents) }
            {foreach $contents as $content}
              <div class="content-item">
                <div class="row content-item-header">
                  <div class="content-item-title"><h1>{$content.cont_name}</h1></div>
                  <div class="content-item-subtitle"></div>
                </div>

                <div class="row content-item-body">

                  <div class="col-md-3 content-item-body-left">

                    <div class="content-item-image">
                      <img src="{$url}libs/phpThumb/?q=80&w=350&zc=1&src={$url}{$paths.images}/{$content.cont_imgs}"/>
                    </div>
                    <div class="content-item-added" title="{$content.cont_added}"><i class="fa fa-clock-o"></i> {$content.cont_added|relative_date}
                    </div>
                    <div class="content-item-tag"><i class="fa fa-tag"></i> <a href="{$url}category/{$content.cat_alias}/" class="ajax-nav"> {$content.cont_category|emoji}</a>
                    </div>

                    <script id="{$pageId}_part_ratings" type="text/x-jsmart-tmpl">
                      {fetch file="`$APP_VIEW`gradio/content/part_ratings.tpl"}
                    </script>

                    <div class="{$pageId}_part_ratings">
                      {include file='gradio/content/part_ratings.tpl'}
                    </div>

                  </div>
                  <div class="col-md-9 content-item-body-center">
                    <div class="content-item-text always-visible">
                      {$content.cont_content|emoji}
                    </div>
                  </div>
                  <div class="content-item-body-right">

                  </div>
                </div>

                <div class="row content-item-footer">

                  <div class="content-item-comments">

                    <div id="fb-comment-content">
                      <div style="margin-left: 10px">
                        <fb:comments href="{$fbsiteurl}/content/{$content.cont_alias}/" numposts="5" colorscheme="light" style=""></fb:comments>
                      </div>
                    </div>

                  </div>

                </div>

              </div>
            {/foreach}
          {else}
            <h1 class="alert alert-danger">{t}Content not found{/t}</h1>
          {/if}


        </div>
      </div>

      <!--
      <div class="content-table">
      <table class="table">
        <thead>
          <tr>
            <th>{t}ID{/t}</th>
            <th>{t}Name{/t}</th>
            <th>{t}Content{/t}</th>
            <th>{t}Slug{/t}</th>
            <th>{t}Added{/t}</th>
            <th>{t}Updated{/t}</th>
            <th>{t}Content class{/t}</th>
            <th>{t}Category{/t}</th>
            <th>{t}Images{/t}</th>
            <th>{t}Order{/t}</th>
            <th>{t}Options{/t}</th>
          </tr>
        </thead>
        <tbody>

      {if isset($contents) }
        {foreach $contents as $content}

            <tr>
            <td class="cont-id"><div><a href="{$url}content/{$content.cont_id}/" class="ajax-nav">{$content.cont_id}</a></div></td>
            <td class="cont-name"><div>{$content.cont_name}</div></td>
            <td class="cont-content"><div>{$content.cont_content}</div></td>
            <td class="cont-alias"><div>{$content.cont_alias}</div></td>
            <td class="cont-added"><div>{$content.cont_added|relative_date}</div></td>
            <td class="cont-updated"><div>{$content.cont_updated|relative_date}</div></td>
            <td class="cont-class"><div>{$content.cont_class}</div></td>
            <td class="cont-category"><div>{$content.cont_category}</div></td>
            <td class="cont-imgs"><div><img src="{$url}libs/phpThumb/?q=80&w=100&h=100&zc=1&src={$url}{$paths.images}/{$content.cont_imgs}"/></div></td>
            <td class="cont-ordere"><div>{$content.cont_order}</div></td>
            <td class='cont-editing-opt'><div>
              <a href="#" class="btn btn-block"><i class="fa fa-thumbs-up"></i> {t}Like{/t}</a>
              <a href="#" class="btn btn-block"><i class="fa fa-thumbs-down"></i> {t}Dislike{/t}</a>
            </div>
            </td>

            </tr>
          <div class="clearfix"></div>

        {/foreach}
      {else}
        <tr>
          <td class="sat-cont-not-found" colspan="11"><h1 class="alert alert-danger">{t}Content not found{/t}</h1></td>
        </tr>
      {/if}
        </tbody>
        </table>
        </div>
      -->

    </div>
  </div>

{/block}