{"begin"|timer:"template head"}
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="content-type" content="{$meta.contenttype}" />
    <meta http-equiv="content-style-type" content="{$meta.contentstyletype}" />
    <meta http-equiv="content-language" content="{$meta.contentlanguage}" />
    <title>{block name="title"}{$title}{/block}</title>
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="{$meta.content}" />
    <meta name="keywords" content="{$meta.keywords}" />
    <meta name="copyright" content="{$meta.copyright}" />
    <meta name="author" content="{$meta.author}" />
    <meta name="robots" content="{$meta.robots}" />
    <meta name="abstract" content="{$meta.abstract}" />
    <meta name="distribution" content="{$meta.distribution}" />
    <meta name="web_author" content="{$meta.webauthor}" />
    <meta name="google-site-verification" content="{$meta.googlesitev}" />

    <!-- facebook -->
    <meta property="fb:app_id" content="{$meta.fbappid}" />
    <meta property="fb:admins" content="{$meta.fbadmins}" />
    <meta property="og:url" content="{$meta.ogurl}" />
    <meta property="og:image" content="{$meta.ogimage}" />
    <meta property="og:type" content="{$meta.ogtype}" />
    <meta property="og:title" content="{$meta.ogtitle}" />
    <meta property="og:description" content="{$meta.ogdescription}" />
    <!-- /facebook -->

    <!-- twitter -->
    <meta name="twitter:card" content="{$meta.twittercard}">
    <meta name="twitter:site" content="{$meta.twittersite}">
    <meta name="twitter:creator" content="{$meta.twittercreator}">
    <meta name="twitter:title" content="{$meta.twittertitle}">
    <meta name="twitter:description" content="{$meta.twitterdescription}">
    {block name="twitter_card_extend"}{/block}
    <!-- /twitter -->

    <link rel="icon" href="{$url}{$paths.themes}/{$frontendTheme}/img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="{$url}{$paths.themes}/{$frontendTheme}/img/favicon.ico" type="image/xicon" />

    {block name="css"}
    <!-- Knight theme -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600">
    <!-- end Knight theme -->
    {asset_compile}
    <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap-theme.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap-social/bootstrap-social.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.brighttheme.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.buttons.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.history.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/dist/pnotify.mobile.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}themes/gradio/css/opentip-xpn-cms.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/player/css/gplayer.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/nprogress/nprogress.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/VerticalIconMenu/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/DataTables/media/css/jquery.dataTables_themeroller.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/datatables-bootstrap3/BS3/assets/css/datatables.css"/>

    <link rel="stylesheet" type="text/css" href="{$url}assets/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" type="text/css" href="{$url}assets/font-awesome-animation/dist/font-awesome-animation.min.css"/>

    <!-- Knight theme -->
    <link rel="stylesheet" type="text/css" href="{$url}themes/Knight/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}themes/Knight/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="{$url}themes/Knight/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{$url}themes/Knight/css/animate.css"/>
    <!-- end Knight theme -->

    <!-- gdlr player -->
    <link rel="stylesheet" type="text/css" href="{$url}assets/gdlr-player/assets/mediaelement/mediaelementplayer.css" />
    <link rel="stylesheet" type="text/css" href="{$url}assets/gdlr-player/assets/mediaelement/mejs-skins.css" />
    <link rel="stylesheet" type="text/css" href="{$url}assets/gdlr-player/css/player.css" />
    <!-- end gdlr player -->

    <link rel="stylesheet" type="text/css" href="{$url}assets/jquery-ui/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/elfinder.min.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/theme.css"/>

    {/asset_compile}

    {*
    <!-- 360-player
    <link rel="stylesheet" type="text/css" href="{$url}assets/360-player/css/flashblock.css" />
    <link rel="stylesheet" type="text/css" href="{$url}assets/360-player/css/360player.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/360-player/css/360player-visualization.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}assets/360-player/css/player-fixed-bottom.css"/>
    end 360-player -->
    *}

    <!-- gradio-mobile theme -->
    <link rel="stylesheet" type="text/css" href="{$url}themes/gradio-mobile/css/particles.css"/>
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}themes/gradio-mobile/css/nowplaying.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}themes/gradio-mobile/css/tune-card.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}themes/gradio-mobile/css/profile.css"/>
    <link rel="stylesheet" type="text/css" href="{$url}themes/gradio-mobile/css/style.css"/>
    <!-- end gradio-mobile -->
    {/block}

    {if isset($debugbarRendererHead)}
      {$debugbarRendererHead}
    {/if}

    <!-- Core -->
    <script>
      var baseURL = "{$url}";
      var siteURL = "{$siteurl}";
      var fbsiteURL = "{$fbsiteurl}";
      var coversURL = "{$plugins.gradio.prefs.covers}";
      var phpThumbUrl = "{$phpThumbUrl}";
      var phpThumbCacheHash = "{$phpThumbCacheHash}";
      var frontendTheme = "{$frontendTheme}";

      {if isset($userData.id)}

      var userID = {$userData.id};
      var userName = "{$userData.username}";
      {else}

      var userID = "guest";
      var userName = "guest";
      {/if}

      var appTitle = "{$title}";
      var appShortTitle = "{$shortTitle}";
      var appVersion = "{$version.number}";
      var now, Notify, simpleNotify, confirmNotify;
      var locale = "{$smarty.session._sf2_attributes.locale}";

      var headerRenderTime = '{"end"|timer}';
      var controller = '{$uri.controller}';
      var method = '{$uri.method}';
      var var_1 = '{$uri.var}';

      {literal}
        var templateRenderTimes = {
          header: headerRenderTime,
          body: false,
          footer: false
        };

        var requestData = {
          controller: controller,
          method: method,
          var : var_1
        };
      {/literal}
    </script>
    {asset_compile}
    <script type="text/javascript" src="{$url}assets/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="{$url}assets/jquery/jquery-migrate.js"></script>
    <!-- TODO need to update to newest bower dependecies to avoid double packages -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <script type="text/javascript" src="{$url}assets/underscore/underscore-min.js"></script>
    <script type="text/javascript" src="{$url}assets/js-logger/src/logger.js"></script>
    {/asset_compile}

    {if isset($googleanalytics.id)}
    <script>
      {literal}
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      {/literal}
        ga('create', '{$googleanalytics.id}', 'auto');
        ga('send', 'pageview');
    </script>
    {/if}

    {if isset($piwik.id)}
    <!-- Piwik -->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(['setUserId', userName]);
      _paq.push(["setDocumentTitle", document.title]);
      _paq.push(["setDomains", ["*.gradio.lv"]]);
      // you can set up to 5 custom variables for each visitor
      _paq.push(["setDoNotTrack", true]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      _paq.push(['setSiteId', {$piwik.id}]);
      var u = "{$piwik.url}";
      {literal}
        (function () {
          _paq.push(['setTrackerUrl', u + 'piwik.php']);
          var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
          g.type = 'text/javascript';
          g.async = true;
          g.defer = true;
          g.src = u + 'piwik.js';
          s.parentNode.insertBefore(g, s);
        })();
      {/literal}
    </script>
    <!--
    <noscript><p><img src="{$piwik.url}piwik.php?idsite={$piwik.id}" style="border:0;" alt=""></p></noscript>
    -->
    <!-- End Piwik Code -->
    {/if}

    <script type="text/javascript" src="{$url}js/errorlogger.js"></script>
    <script type="text/x-template" id="nowPlayingFlipCard-template">
      {include file="home/partial/tune-card.tpl"}
    </script>
  </head>

  {block name="body"}
  {"begin"|timer:"template body"}
  <body>
    {block name="header"}
      <div style="overflow:hidden;">
        <header class="header" id="header"><!--header-start-->
          <div class="animation-wrapper">
            <div class="particle particle-1"></div>
            <div class="particle particle-2"></div>
            <div class="particle particle-3"></div>
            <div class="particle particle-4"></div>
          </div>
          <div id="full-screen-bg"></div>
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-6">
                <div class="brand-logo">
                  <figure class="logo animated fadeInDown delay-07s">
                    <a href="{$url}"><img src="{$url}img/logos/only-g-without-gloss-web-200.png" alt=""></a>
                  </figure>
                  <h1 class="animated fadeInDown delay-07s">{t}Welcome To gradio.lv{/t}</h1>
                  <ul class="we-create animated fadeInUp delay-1s">
                    <li>{t}Feel the drive!{/t}</li>
                  </ul>
                  <!--<a class="link animated fadeInUp delay-1s" href="#">{t}Get Started{/t}</a>-->
                </div>
              </div>
              <div class="col-xs-1 hidden-xs hidden-sm hidden-md"></div>
              <div class="col-xs-3">
                <div class="now-playing-card-container animated fadeInRightBig delay-09s">
                  <div class="col-xs-12">
                    <div id="nowPlayingFlipCard-container"></div>
                  </div>
                </div>
              </div>
              <div class="col-xs-1"></div>
            </div>
          </div>
        </header><!--header-end-->
      </div>
    {/block}

    {block name="navigation"}
      {include "navigation/navigation.tpl"}
    {/block}

    {block name="content"}{/block}

    {block name="footer"}{/block}

  {/block}
  </body>
</html>
