{capture name="page_title"}
  {block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="content" append}
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <h3 class="panel-title">{t}Content{/t}</h3>
        {if isset($categories) }
          {foreach $categories as $category}
            <a href="{$url}category/{$category.cat_alias}" class="ajax-nav">
              <div class="col-sm-3 col-md-3">
                <div class="thumb" style="background-image: url('{$phpThumbUrl}?q=100&h=400&zc=1&src={$url}{$paths.images}/{$category.cat_imgs}')">
                  <h1 class="subcategory-title"><div class="">{$category.cat_name|emoji}</div></h1>
                  <h2 class="subcategory-title"><div class="">{$category.cat_content|emoji}</div></h2>
                </div>
              </div>
            </a>
          {/foreach}
        {/if}

        {if isset($categories_parent) }
          {foreach $categories_parent as $category}
            <a href="{$url}category/{$category.alias}" class="ajax-nav">
              <div class="col-sm-3 col-md-3">
                <div class="thumb" style="background-image: url('{$phpThumbUrl}?q=100&h=400&zc=1&src={$url}{$paths.images}/{$category.img}')">
                  <h1 class="subcategory-title"><div class="">{$category.name|emoji}</div></h1>
                </div>
              </div>
            </a>
          {/foreach}
        {/if}

        <div class="clearfix"></div>
        {if isset($contents) }
          <br>
          {foreach $contents as $content}
            <div class="recent-categories-block col-xs-12 col-sm-6 col-md-4 col-lg-3">
              <a
                href="{$url}content/{$content.cont_alias}"
                data-href="{$url}content/{$content.cont_alias}"
                class="ajax-nav">
                <span class="text-center recent-categories-block-title">
                  {$content.cont_name|truncate:50:"...":true|emoji}
                </span>
              </a>
              <div class="recent-categories-block-inner">
                <div
                  class="col-xs-12 recent-categories-block-image"
                  style="background-image: url({$phpThumbUrl}?q=100&h=400&zc=1&src={$url}{$paths.images}/{$content.cont_imgs|escape:'url'})">
                </div>

                <div class="col-xs-12">
                  <div class="recent-categories-block-content">
                    {$content.cont_content|truncate:600:"...":true|emoji|strip_tags:"<p><div><h1>"}

                    <div class="row">
                      <div class="recent-categories-block-info col-xs-12">
                        <div class="recent-categories-block-date">
                          <i class="fa fa-clock-o"></i>
                          {$content.cont_added|relative_date}
                        </div>
                        <div class="recent-categories-block-comments">
                          <i class="fa fa-comments"></i>
                          <a href="{$url}content/{$content.cont_alias}" class="ajax-nav">
                            <span
                              class="fb-comments-count"
                              data-href="{$fbsiteoldurl}/content/{$content.cont_alias}/">
                              0
                            </span> {t}Comments{/t}
                          </a>
                        </div>
                        <div class="recent-categories-block-tag">
                          <i class="fa fa-tag"></i>
                          <a href="{$url}category/{$content.cat_alias}" class="ajax-nav">
                            {$content.cont_category|emoji}
                          </a>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          {/foreach}
        {/if}
      </div>
    </div>
  </div>
{/block}
