{block name="body" append}
    
    <section class="content-header">
    <h1>{$title} {t}Admin panel{/t}</h1>
    <ol class="breadcrumb">
      <li class="active">
      <a href="#">
        <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
        Home
      </a>
      </li>
    </ol>
    </section>
    <section class="content">
    <div class="row">
      <div class="col-md-3">
        {include './latestcontent.tpl' count=6}
      </div>
    </div>
    </section>
    
    <!--
    <div class="row">

      <h3>{$title} {t}Admin panel{/t}</h3>

      <ul class="nav nav-tabs nav-justified" role="tablist">

        <li class=""><a href="#dashboard" aria-controls="dashboard" role="tab" data-toggle="tab"><h1><i class="fa fa-home"></i>
        {t}Dashboard{/t}</h1></a></li>

        <li class=""><a href="#content" aria-controls="content" role="tab" data-toggle="tab"><h1><i class="fa fa-desktop"></i> {t}Content{/t}</h1></a></li>
        <li class=""><a href="#users"  aria-controls="users" role="tab" data-toggle="tab"><h1><i class="fa fa-group"></i> {t}Users{/t}</h1></a></li>
        <li class=""><a href="#plugins"  aria-controls="plugins" role="tab" data-toggle="tab"><h1><i class="fa fa-plug"></i> {t}Plugins{/t}</h1></a></li>
        <li class=""><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><h1><i class="fa fa-cogs"></i> {t}Settings{/t}</h1></a></li>
      </ul>

      <div class="tab-content">
        {block name="Content"}
        <div class="tab-pane fade" role="tabpanel" id="dashboard">
          <div class="col-md-3 col-sm-3 col-xs-6 latest-tracks">
            <h1 class="center"><a href="{$url}gradio/songs" class="ajax-nav">{t}Latest tunes{/t}</a></h1>
            {if isset($isAdmin) && $isAdmin == true}
              <div id="admin-latest-tracks"></div>
            {/if}
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 latest-likes">
            <h1 class="center"><a href="{$url}gradio/ratings" class="ajax-nav">{t}Latest ratings{/t}</a></h1>
            {if isset($isAdmin) && $isAdmin == true}
              <div id="admin-latest-ratings"></div>
            {/if}
          </div>
          <!--
          <div class="col-md-3 col-sm-3 col-xs-6 now-on-air">
            <h1 class="center"><a href="{$url}gradio/onair" class="ajax-nav">{t}Now on air{/t}</a></h1>
            {if isset($isAdmin) && $isAdmin == true}
              <div id="admin-latest-now"></div>
            {/if}
          </div>
          -->
          
          <!--
          <div class="col-md-3 col-sm-3 col-xs-6 latest-version">
            <h1 class="center"><a href="{$url}admin" class="ajax-nav">{t}Latest version{/t}</a></h1>
            {if isset($isAdmin) && $isAdmin == true}
              <div id="admin-latest-version"></div>
            {/if}
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6 admin-content">
            <h1 class="center"><a href="{$url}admin/content" class="ajax-nav">{t}Content{/t}</a></h1>
            {if isset($isAdmin) && $isAdmin == true}
              <div id="admin-latest-content-block"></div>
            {/if}
          </div>
        </div>

        <div class="tab-pane fade" role="tabpanel" id="content">

          <div class="admin-content">
            {if isset($isAdmin) && $isAdmin == true}
              <div id="admin-latest-content"></div>
            {/if}
          </div>

        </div>

        <div class="tab-pane fade" role="tabpanel" id="users">

        </div>

        <div class="tab-pane fade" role="tabpanel" id="settings">
          <div class="panel-group" id="settingsAccordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingDashboard">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#settingsAccordion" href="#collapseDashboard" aria-expanded="true" aria-controls="collapseDashboard">
                    <i class="fa fa-dashboard"></i> {t}Dashboard{/t}
                  </a>
                </h4>
              </div>
              <div id="collapseDashboard" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingDashboard">
                <div class="panel-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingCache">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#settingsAccordion" href="#collapseCache" aria-expanded="false" aria-controls="collapseCache">
                    <i class="fa fa-cahe"></i> {t}Cache{/t} {t}Current cache{/t} <span class="cache-count badge" data-val="0"></span> {t}items{/t}
                  </a>
                </h4>
              </div>
              <div id="collapseCache" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCache">
                <div class="panel-body">
                  <form id="clearcacheform" action="{$url}admin/clearcache/" method="post">
                    <input type="hidden" name="formid" value="clearcacheform" />
                    <div class="smarty-cache"><div class="col-md-6">{t}Smarty cache{/t} <span class="smarty-cache-count badge" data-val="0"></span></div><div   class="col-md-6"><input type="checkbox" class="cache-checkbox" name="smarty" checked="checked"></div></div>
                    <div class="sacy-cache"><div class="col-md-6">{t}Sacy cache{/t} <span class="sacy-cache-count badge" data-val="0"></span></div><div class="col-md-6"><input type="checkbox" class="cache-checkbox" name="sacy"></div></div>
                    <div class="phpthumb-cache"><div class="col-md-6">{t}phpThumb cache{/t} <span class="phpthumb-cache-count badge" data-val="0"></span></div><div class="col-md-6"><input type="checkbox" class="cache-checkbox" name="phpthumb"></div></div>
                    <div class="clearfix"><br /></div>
                    <button type="submit" class="btn btn-danger adm-cache-clear">
                      <i class="fa fa-eraser"></i> {t}Clear cache{/t}
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/block}
      </div>

    </div>
    -->

{/block}
