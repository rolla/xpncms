{block name="body" append}

    {function name=menu selected=0 level=0}
        {foreach $data as $entry}
            {if $entry.id == $selected}
                {str_repeat("--", $level)} {$entry.name}
            {else}

            {/if}
            {if is_array($entry.sub_category)}
                {menu data=$entry.sub_category selected=$selected level=$level+1}
            {else}

            {/if}
        {/foreach}
    {/function}

    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li class=""><a href="#subcontent" aria-controls="subcontent" role="tab" data-toggle="tab" data-level="2"><h1><i class="fa fa-desktop"></i> {t}Content{/t}</h1></a></li>
        <li class=""><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" data-level="2"><h1><i class="fa fa-list-alt"></i> {t}Categories{/t}</h1></a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade" role="tabpanel" id="subcontent">
            <br />

            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/" class="btn btn-block btn-danger" data-action="back-content"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/addcontent/" class="btn btn-block btn-primary" data-action="add-content"><i class="fa fa-file"></i> {t}Add{/t}</a>
            </div>

            <br /><br />
            <div class="clearfix"></div>
            <div class="content-table">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{t}ID{/t}</th>
                            <th>{t}Name{/t}</th>
                            <th>{t}Content{/t}</th>
                            <th>{t}Slug{/t}</th>
                            <th>{t}Added{/t}</th>
                            <th>{t}Updated{/t}</th>
                            <th>{t}Content class{/t}</th>
                            <th>{t}Category{/t}</th>
                            <th>{t}Images{/t}</th>
                            <th>{t}Order{/t}</th>
                            <th>{t}Options{/t}</th>
                        </tr>
                    </thead>
                    <tbody>

                        {if isset($contents) }
                            {foreach $contents as $content}

                                <tr>
                                    <td class="content-id"><div><a href="{$url}content/{$content.cont_id}" class="ajax-nav">{$content.cont_id}</a></div></td>
                                    <td class="content-name"><div><a href="{$url}content/{$content.cont_alias}" class="ajax-nav">{$content.cont_name}</a></div></td>
                                    <td class="content-content"><div>{$content.cont_content|strip_tags:"<br><br /><br/><p><div><h1>"|truncate:400:"...":true}</div></td>
                                    <td class="content-alias"><div>{$content.cont_alias}</div></td>
                                    <td class="content-added"><div>{$content.cont_added|relative_date}</div></td>
                                    <td class="content-updated"><div>{$content.cont_updated|relative_date}</div></td>
                                    <td class="content-class"><div>{$content.cont_class}</div></td>
                                    <td class="content-category"><div>{$content.cont_category}</div></td>
                                    <td class="content-imgs"><div><img src="{$phpThumbUrl}?q=80&w=100&h=100&zc=1&src=../../{$paths.images}/{$content.cont_imgs}"/></div></td>
                                    <td class="content-order"><div>{$content.cont_order}</div></td>
                                    <td class='content-options'><div>
                                            <a href="{$url}admin/editcontent/{$content.cont_alias}" class="btn btn-block ajax-nav"><i class="fa fa-edit "></i> {t}Edit{/t}</a>
                                            <a href="#" data-href="{$url}admin/deletecontent/content/{$content.cont_id}" class="btn btn-block adm-del" data-confirm="Vai tiešām vēlies dzēst? saturu ar ID: {$content.cont_id} un Nosaukumu: {$content.cont_name}"><i class="fa fa-trash-o"></i> {t}Delete{/t}</a>
                                        </div>
                                    </td>

                                </tr>
                            <div class="clearfix"></div>

                        {/foreach}
                    {else}
                        <tr>
                            <td class="content-not-found" colspan="11"><h1 class="alert alert-danger">{t}Content not found{/t}</h1></td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>

            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/" class="btn btn-block btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/addcontent/" class="btn btn-block btn-primary" data-action="add-content"><i class="fa fa-file"></i> {t}Add{/t}</a>
            </div>

        </div>

        <div class="tab-pane fade" role="tabpanel" id="categories">
            <br />
            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/" class="btn btn-block btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/addcategory/" class="btn btn-block btn-primary ajax-nav"><i class="fa fa-file"></i> {t}Add{/t}</a>
            </div>
            <br /><br />
            <div class="clearfix"></div>


            <div class="category-table">
                <table class="table">
                    <thead>
                        <tr>
                            <th>{t}ID{/t}</th>
                            <th>{t}Name{/t}</th>
                            <th>{t}Content{/t}</th>
                            <th>{t}Slug{/t}</th>
                            <th>{t}Parent category{/t}</th>
                            <th>{t}Added{/t}</th>
                            <th>{t}Updated{/t}</th>
                            <th>{t}Category class{/t}</th>
                            <th>{t}Images{/t}</th>
                            <th>{t}Order{/t}</th>
                            <th>{t}Options{/t}</th>
                        </tr>
                    </thead>
                    <tbody>

                        {if isset($categories) }
                            {foreach $categories as $category}

                                <tr>
                                    <td class="category-id"><div><a href="{$url}category/{$category.cat_id}" class="ajax-nav">{$category.cat_id}</a></div></td>
                                    <td class="category-name"><div><a href="{$url}category/{$category.cat_alias}" class="ajax-nav">{$category.cat_name}</a></div></td>
                                    <td class="category-category"><div>{$category.cat_content}</div></td>
                                    <td class="category-alias"><div>{$category.cat_alias}</div></td>

                                    {if isset($categories_parent) }
                                        {* run the array through the function *}
                                        <td class="category-parent"><div>{menu data=$categories_parent selected=$category.cat_parent}</div></td>
                                            {else}

                                    {/if}
                                    <td class="category-added"><div>{$category.cat_added|relative_date}</div></td>
                                    <td class="category-updated"><div>{$category.cat_updated|relative_date}</div></td>
                                    <td class="category-class"><div>{$category.cat_class}</div></td>
                                    <td class="category-imgs"><div><img src="{$phpThumbUrl}?q=80&w=100&h=100&zc=1&src=../../{$paths.images}/{$category.cat_imgs}"/></div></td>
                                    <td class="category-order"><div>{$category.cat_order}</div></td>
                                    <td class='category-options'><div>
                                            <a href="{$url}admin/editcategory/{$category.cat_alias}/" class="btn btn-block ajax-nav"><i class="fa fa-edit "></i> {t}Edit{/t}</a>
                                            <a href="#" data-href="{$url}admin/deletecontent/category/{$category.cat_id}" class="btn btn-block adm-del" data-confirm="Vai tiešām vēlies dzēst? saturu ar ID: {$category.cat_id} un Nosaukumu: {$category.cat_name}"><i class="fa fa-trash-o"></i> {t}Delete{/t}</a>
                                        </div>
                                    </td>

                                </tr>
                            <div class="clearfix"></div>

                        {/foreach}
                    {else}
                        <tr>
                            <td class="category-not-found" colspan="11"><h1 class="alert alert-danger">{t}Category not found{/t}</h1></td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>

            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/" class="btn btn-block btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                <a href="{$url}admin/addcategory/" class="btn btn-block btn-primary ajax-nav"><i class="fa fa-file"></i> {t}Add{/t}</a>
            </div>

        </div>

    </div>
{/block}