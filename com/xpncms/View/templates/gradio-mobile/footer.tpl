{block name="footer" append}
  {"begin"|timer:"template footer"}

  <section class="main-section team" id="team">
    <div class="container">
      <h2>{t}Team{/t}</h2>
      <h6>{t}Music enthusiasts{/t}</h6>
      <div class="team-leader-block clearfix">
        <div class="team-leader-box">
          <div class="team-leader wow fadeInDown delay-03s">
            <div class="team-leader-shadow"><a href="#"></a></div>
            <img src="{$url}img/team/rolla.jpg" alt="rolla">
            <ul>
              <li><a href="//www.facebook.com/djbeatermusic" class="fa-facebook" target="_blank"></a></li>
              <li><a href="//twitter.com/BeaterDJ" class="fa-twitter" target="_blank"></a></li>
            </ul>
          </div>
          <h3 class="wow fadeInDown delay-03s">rolla</h3>
          <span class="wow fadeInDown delay-03s">{t}Founder / Owner{/t}</span>
          <p class="wow fadeInDown delay-03s"></p>
        </div>

        <div class="team-leader-box">
          <div class="team-leader  wow fadeInDown delay-06s">
            <div class="team-leader-shadow"><a href="#"></a></div>
            <img src="{$url}img/team/edphoto.jpg" alt="edphoto">
            <ul>
              <li><a href="//www.facebook.com/edphoto.lv" class="fa-facebook" target="_blank"></a></li>
              <li><a href="//twitter.com/edphotolv" class="fa-twitter" target="_blank"></a></li>
            </ul>
          </div>
          <h3 class="wow fadeInDown delay-06s">edphoto</h3>
          <span class="wow fadeInDown delay-06s">{t}Songs Manager{/t}</span>
          <p class="wow fadeInDown delay-06s"></p>
        </div>
        <!--
        <div class="team-leader-box">
          <div class="team-leader wow fadeInDown delay-09s">
            <div class="team-leader-shadow"><a href="#"></a></div>
            <img src="{$url}themes/Knight/img/team-leader-pic3.jpg" alt="">
            <ul>
              <li><a href="#" class="fa-twitter"></a></li>
              <li><a href="#" class="fa-facebook"></a></li>
              <li><a href="#" class="fa-pinterest"></a></li>
              <li><a href="#" class="fa-google-plus"></a></li>
            </ul>
          </div>
          <h3 class="wow fadeInDown delay-09s">Skyler white</h3>
          <span class="wow fadeInDown delay-09s">Accountant</span>
          <p class="wow fadeInDown delay-09s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.</p>
        </div>
        -->
      </div>
    </div>
  </section>

  <section class="business-talking"><!--business-talking-start-->
    <div class="container">
      <h2>{t}Send us greetings.{/t}</h2>
    </div>
  </section><!--business-talking-end-->

  <div class="container">
    <section class="main-section contact" id="contact">
      <div class="row">
        <div class="col-lg-6 col-sm-7 wow fadeInLeft">
          <!--
          <div class="contact-info-box address clearfix">
            <h3><i class=" icon-map-marker"></i>Address:</h3>
            <span>308 Negra Arroyo Lane<br>Albuquerque, New Mexico, 87111.</span>
          </div>
          <div class="contact-info-box phone clearfix">
                <h3><i class="fa-phone"></i>Phone:</h3>
                <span>1-800-BOO-YAHH</span>
          </div>
          -->
          <div class="contact-info-box email clearfix">
            <h3><i class="fa-pencil"></i>{t}e-mail:{/t}</h3>
            <span>info@gradio.lv</span>
          </div>
          <!--
          <div class="contact-info-box hours clearfix">
            <h3><i class="fa-clock-o"></i>Hours:</h3>
            <span><strong>Monday - Thursday:</strong> 10am - 6pm<br><strong>Friday:</strong> People work on Fridays now?<br><strong>Saturday - Sunday:</strong> Best not to ask.</span>
          </div>
          -->
          <ul class="social-link">
            <li class="facebook">
              <a href="//facebook.com/gradiolv" target="_blank"><i class="fa-facebook"></i></a>
            </li>
            <li class="twitter">
              <a href="//twitter.com/gradiolv" target="_blank"><i class="fa-twitter"></i></a>
            </li>
            <li class="gplus">
              <a href="//plus.google.com/+GradioLvPage" target="_blank"><i class="fa-google-plus"></i></a>
            </li>
          </ul>

          <span itemscope="" itemtype="http://schema.org/Organization" style="display:none">
            <link itemprop="url" href="/">
            <a itemprop="sameAs" href="http://facebook.com/gradiolv">Facebook</a>
            <a itemprop="sameAs" href="http://twitter.com/gradiolv">Twitter</a>
            <a itemprop="sameAs" href="http://plus.google.com/+GradioLvPage">Google Plus</a>
            <a itemprop="sameAs" href="http://draugiem.lv/gradiolv">Draugiem</a>
            <a itemprop="sameAs" href="http://youtube.com/gradiolv">Youtube</a>
            <a itemprop="sameAs" href="http://mixcloud.com/gradiolv">Mixcloud</a>
            <a itemprop="sameAs" href="http://soundcloud.com/gradiolv">Soundcloud</a>
            <a itemprop="sameAs" href="http://lastfm.com/user/gradiolv">Last.fm</a>
          </span>

        </div>
        <div class="col-lg-6 col-sm-5 wow fadeInUp delay-05s">
          {t}Form currently not available{/t}
          <div class="form" style="opacity: 0.4;">
            <input class="input-text disabled" type="text" name="" value="Your Name *" onfocus="if (this.value == this.defaultValue)
                  this.value = '';" onblur="if (this.value == '')
                        this.value = this.defaultValue;">
            <input class="input-text disabled" type="text" name="" value="Your E-mail *" onfocus="if (this.value == this.defaultValue)
                  this.value = '';" onblur="if (this.value == '')
                        this.value = this.defaultValue;">
            <textarea class="input-text text-area disabled" cols="0" rows="0" onfocus="if (this.value == this.defaultValue)
                  this.value = '';" onblur="if (this.value == '')
                        this.value = this.defaultValue;">Your Message *</textarea>
            <input class="input-btn disabled" type="submit" value="send message">
          </div>
        </div>
      </div>
    </section>
  </div>

  <footer class="footer">
    <div class="container">
      <!--
      <div class="footer-logo"><a href="#"><img src="{$url}img/no-now-playing-art.png" alt=""></a></div>
      <span class="copyright">Copyright © 2015 | <a href="http://bootstraptaste.com/">Bootstrap Themes</a> by BootstrapTaste</span>
      -->
    </div>
    <!--
        All links in the footer should remain intact.
        Licenseing information is available at: http://bootstraptaste.com/license/
        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Knight
    -->
  </footer>

  {*
  <!-- 360-player
  <div class="fixed-bottom-player">
  <div class="ui360 ui360-vis">
  <a href="http://stream.gradio.lv:8000/gradio;stream.mp3">http://stream.gradio.lv:8000/gradio;stream.mp3</a>
  </div>
  </div>
  <!-- end 360-player -->
  *}

  <!-- Modal -->
  <div id="dataConfirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="dataConfirmModallabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="dataConfirmModal">{t}Please confirm{/t}</h3>
    </div>
    <div class="modal-body"></div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">{t}Cancel{/t}</button>
      <a href="" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dataConfirmOK">{t}Delete{/t}</a>
    </div>
  </div>

  {block name="jsscripts"}
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  {asset_compile}
  <script type="text/javascript" src="{$url}assets/bootstrap/dist/js/bootstrap.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-lastfm/lastfm.jquery.js"></script>
  <script type="text/javascript" src="{$url}assets/jplayer/dist/jplayer/jquery.jplayer.js"></script>

  <script type="text/javascript" src="{$url}assets/jquery-address-old/jquery.address-1.6-mod.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-easing-original/jquery.easing.compatibility.js"></script>

  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.animate.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.buttons.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.confirm.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.nonblock.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.desktop.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.history.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.callbacks.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.mobile.js"></script>
  <script type="text/javascript" src="{$url}assets/pnotify/dist/pnotify.desktop.js"></script>

  <script type="text/javascript" src="{$url}assets/player/js/jquery.jplayer.min.js"></script>
  <script type="text/javascript" src="{$url}assets/player/js/jplayer.playlist.min.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-resizeimagetoparent/jquery.resizeimagetoparent.js"></script>
  <script type="text/javascript" src="{$url}assets/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
  <script type="text/javascript" src="{$url}assets/opentip/downloads/opentip-jquery-excanvas.min.js"></script>
  <script type="text/javascript" src="{$url}assets/VerticalIconMenu/js/modernizr.custom.js"></script>
  <script type="text/javascript" src="{$url}assets/swfobject/swfobject/swfobject.js"></script>
  <script type="text/javascript" src="{$url}assets/html5-boilerplate/js/plugins.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-cookie/jquery.cookie.js"></script>
  <script type="text/javascript" src="{$url}assets/player/js/gplayer.js"></script>
  <script type="text/javascript" src="{$url}assets/player/js/progress.js"></script>
  <script type="text/javascript" src="{$url}assets/nprogress/nprogress.js"></script>
  <script type="text/javascript" src="{$url}assets/DataTables/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="{$url}assets/datatables-bootstrap3/BS3/assets/js/datatables.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-form/jquery.form.js"></script>
  <script type="text/javascript" src="{$url}assets/extend.js/src/extend.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-infinite-scroll/jquery.infinitescroll.min.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-infinite-scroll/behaviors/manual-trigger.js"></script>

  <script type="text/javascript" src="{$url}assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="{$url}assets/bootstrap-datepicker/dist/locales/bootstrap-datepicker.lv.min.js"></script>
  <script type="text/javascript" src="{$url}assets/jsmart/jsmart.min.js"></script>
  <script type="text/javascript" src="{$url}assets/simple-ajax-uploader/SimpleAjaxUploader.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-ujs/src/rails.js"></script>

  <!-- Knight theme -->
  <script type="text/javascript" src="{$url}themes/Knight/js/jquery-scrolltofixed.js"></script>
  <script type="text/javascript" src="{$url}themes/Knight/js/jquery.isotope.js"></script>
  <script type="text/javascript" src="{$url}themes/Knight/js/wow.js"></script>
  <script type="text/javascript" src="{$url}themes/Knight/js/classie.js"></script>
  <script type="text/javascript" src="{$url}assets/gdlr-player/assets/mediaelement/mediaelement-and-player.js"></script>

  <!--[if lt IE 9]>
    <script src="{$url}themes/Knight/js/respond-1.1.0.min.js"></script>
    <script src="{$url}themes/Knight/js/html5shiv.js"></script>
    <script src="{$url}themes/Knight/js/html5element.js"></script>
  <![endif]-->
  <!-- end Knight theme -->
  {/asset_compile}

  <script type="text/javascript" src="{$url}assets/vue/dist/vue.min.js"></script>
  <script type="text/javascript" src="{$url}assets/vue-echarts/dist/vue-echarts.js"></script>
  <script type="text/javascript" src="{$url}assets/axios/dist/axios.min.js"></script>
  <script type="text/javascript" src="{$url}assets/moment/min/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-ui/jquery-ui.min.js"></script>

  <script src="{$url}assets/ckeditor/ckeditor.js"></script>
  <script src="{$url}assets/ckeditor/adapters/jquery.js"></script>
  <script type="text/javascript" src="{$url}assets/CanvasLoader/js/heartcode-canvasloader-min.js"></script>
  <script type="text/javascript" src="{$url}assets/jquery-seo-url/jquery.seourl.min.js"></script>
  <script type="text/javascript" src="{$url}assets/elfinder/js/elfinder.min.js"></script>
  <script type="text/javascript" src="{$url}assets/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>

  <script type="text/javascript" src="{$url}assets/velocity/velocity.min.js"></script>
  <script type="text/javascript" src="{$url}assets/velocity/velocity.ui.min.js"></script>
  <script type="text/javascript" src="{$url}{$paths.themes}/{$frontendTheme}/js/now-playing-flipcard.js"></script>

  {/block}


  <script>

  {literal}
    /*
    Vue.component('my-component', {
      template: '#nowPlayingFlipCard-template',
      data: {
        tune: {artist: "test"},

        //currentUrl: function(){
          //return baseURL + 'tune/' + this.tune.song_id;
        //}
      },
      methods: {
        loadData: function(){
          var vm = this;
          axios.get(baseURL + 'gradio/now').then(function(response){
            //console.debug(response.data.now);
            vm.tune = response.data.now;
            //vm.tune.currentUrl = baseURL + 'tune/' + this.tune.song_id;
          });
        }
      },
      ready: function () {
        this.loadData();
        setInterval(function () {
          this.loadData();
        }.bind(this), 2000);
      }
    });
  */
    moment.locale('lv'); //todo need update from variable

    // register component to use
    Vue.component('chart', VueECharts);

    // create a root instance
    var myVue = new Vue({
      delimiters: ['%%', '%%'],
      el: '#nowPlayingFlipCard-container',
      template: '#nowPlayingFlipCard-template',
      data: {
        tune: {},
        site: {},
        active: false,
        pie: {
          /*
          title: {
            text: 'Like Dislike ratio',
            x: 'center'
          },
          */
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
          },

          /*
          legend: {
            orient: 'vertical',
            left: 'left',
            data: ['Likes', 'Dislikes']
         },
         */

         series: [
           {
             //name: 'test',
             type: 'pie',
             radius: '60%',
             //center: ['10%', '20%'],
             data: [
               {value: 310, name: 'Dislikes',},
               {value: 335, name: 'Likes'},
             ],
             itemStyle: {
               emphasis: {
                 shadowBlur: 10,
                 shadowOffsetX: 0,
                 shadowColor: 'rgba(0, 0, 0, 0.5)'
               }
             }
           }
         ]

        }
      },
      methods: {
        loadData: function(){
          var vm = this;
          axios.get(baseURL + 'gradio/now').then(function(response){
            //console.debug(response.data.now);
            vm.tune = response.data.now;
            vm.site = {
              baseURL: baseURL,
              frontendTheme: frontendTheme,
            }
            vm.pie.series[0].data = [
               {value: vm.tune.dislikes, name: 'Dislikes', color: "#d0648a"},
               {value: vm.tune.likes, name: 'Likes'},
            ];
          });
        },
        /*
        mouseOver: function() {
          this.active = !this.active;
          console.log("flag " + this.active);
        }
        */
      },
      mounted: function () {
        this.loadData();
        setInterval(function () {
          this.loadData();
        }.bind(this), 15000);
      }
    });

    {/literal}

 /*
 // register
Vue.component('my-component', {
  template: '#nowPlayingFlipCard-template'
})
// create a root instance
new Vue({
  delimiters: ['%%', '%%'],
  el: '#example'
})
*/

  </script>

  {block name="player"}
    {include "player/player.tpl"}
  {/block}

  {block name="footerjs"}
  <!-- footerjs -->
  <script>

  $(document).ready(function (e) {
      var tillEleHeight = $('.main-nav-outer').offset().top;
      var headerEl = $('#header');
      $(window).resize(function() {
        headerEl.css('height', tillEleHeight);
        //$(window).height();
      });
    $(window).trigger('resize');
  });

  </script>

  {*
  <!-- 360-player
  <script src="{$url}assets/360-player/js/berniecode-animator.js"></script>
  <script src="{$url}assets/360-player/js/soundmanager2.js"></script>
  <script src="{$url}assets/360-player/js/360player.js"></script>
  <script type="text/javascript">
  soundManager.setup({
  // path to directory containing SM2 SWF
  url: '{$url}themes/Knight/assets/360player/swf/',
  //preferFlash: false,
  });

  threeSixtyPlayer.config.scaleFont = (navigator.userAgent.match(/msie/i)?false:true);
  threeSixtyPlayer.config.showHMSTime = true;

  // enable some spectrum stuffs

  threeSixtyPlayer.config.useWaveformData = true;
  threeSixtyPlayer.config.useEQData = true;

  // enable this in SM2 as well, as needed

  if (threeSixtyPlayer.config.useWaveformData) {
  soundManager.flash9Options.useWaveformData = true;
  }
  if (threeSixtyPlayer.config.useEQData) {
  soundManager.flash9Options.useEQData = true;
  }
  if (threeSixtyPlayer.config.usePeakData) {
  soundManager.flash9Options.usePeakData = true;
  }

  if (threeSixtyPlayer.config.useWaveformData || threeSixtyPlayer.flash9Options.useEQData || threeSixtyPlayer.flash9Options.usePeakData) {
  // even if HTML5 supports MP3, prefer flash so the visualization features can be used.
  soundManager.preferFlash = true;
  }

  // favicon is expensive CPU-wise, but can be used.
  if (window.location.href.match(/hifi/i)) {
  threeSixtyPlayer.config.useFavIcon = true;
  }

  if (window.location.href.match(/html5/i)) {
  // for testing IE 9, etc.
  soundManager.useHTML5Audio = true;
  }

  </script>
  -->
  <!-- end 360-player -->
  *}

  <!-- Knight theme -->
  <script type="text/javascript">
    $(document).ready(function (e) {
      $('#nav-fixed').scrollToFixed();
      $('.res-nav_click').click(function () {
        $('.main-nav').slideToggle();
        return false
      });
    });
  </script>

  <script>
    wow = new WOW(
            {
              animateClass: 'animated',
              offset: 100
            }
    );
    wow.init();
  </script>

  <!--
  <script type="text/javascript">
    $(window).load(function(){
        $('.main-nav li a').bind('click',function(event){
            var $anchor = $(this);

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 102
            }, 1500,'easeInOutExpo');
            /*
            if you don't want to use the easing effects:
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1000);
            */
            event.preventDefault();
        });
    })
  </script>
  -->

  <script type="text/javascript">

    $(window).load(function () {

      var $container = $('.portfolioContainer'),
              $body = $('body'),
              colW = 375,
              columns = null;


      $container.isotope({
        // disable window resizing
        resizable: true,
        masonry: {
          columnWidth: colW
        }
      });

      $(window).smartresize(function () {
        // check if columns has changed
        var currentColumns = Math.floor(($body.width() - 30) / colW);
        if (currentColumns !== columns) {
          // set new column count
          columns = currentColumns;
          // apply width to container manually, then trigger relayout
          $container.width(columns * colW)
                  .isotope('reLayout');
        }

      }).smartresize(); // trigger resize to set container width

      $('.portfolioFilter a').click(function () {
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');

        var selector = $(this).attr('data-filter');
        $container.isotope({

          filter: selector,
        });
        return false;
      });

    });

  </script>
  <!-- end Knight theme -->

  <script>
    //core
    {if isset($smarty.session._sf2_attributes.jsdebug) && $smarty.session._sf2_attributes.jsdebug == "true" }
    // Only log WARN and ERROR messages.
    //Logger.setLevel(Logger.WARN);
    Logger.useDefaults();
    Logger.debug("Debug is set to: {$smarty.session._sf2_attributes.jsdebug}");

    {else}

    Logger.setLevel(Logger.OFF);
    if (typeof console === 'object') {
      console.debug('JS debugging disabled, to enable change jsdebug setting in conf.php');
    }

    {/if}

    {literal}


    {/literal}

  </script>

  <script type="text/javascript">
    //core

    var URL = "http://gradio.lv";
    var title = "【G】radio";
    var simple_title = "gradio";
    var title_prefix = "【G】radio";
    var result = new Array();

    {literal}
      Opentip.styles.xpncms = {
        extends: "dark",
        delay: 0.3,
        stemLength: 10,
        stemBase: 10,
        borderWidth: 3,
        borderRadius: 5,
        borderColor: "#fff",
        color: "#000",
        background: [[0, "#C4F83E"], [1, "#D2F870"]],
        closeButtonCrossColor: "rgba(255, 255, 255, 1)",
        tipJoint: "bottom left",
        removeElementsOnHide: true
      };
      Opentip.debug = false;
      Opentip.defaultStyle = "xpncms";

      //setTimeout(function(){
      //}, 1000);

    {/literal}

      //$.noConflict();
      $(function ($) {

        //nowPlaying();

        //bindAddress('.ajax-nav');

        //iegutTweets();

        /*
         $(".ajax-nav").click(function(){
         $(".ajax-nav").each(function(){
         $(this).removeClass("aktiva");
         });
         $(this).addClass("aktiva");
         });
         */

        //iegutBildes();
        //iegutSaturu("sakums");

        setTimeout(function () {
          //createOTips();
          //ieladetSupersized(result);
        }, 2000);

        /*
         if(window.location.href.indexOf("bannerclient") > -1){
         //$("#video").modal("show");
         $(".modal,.modal-footer").css("background-color","#000");
         $(".modal-body").css("max-height","500px");

         $("#video").modal({
         show: true,
         backdrop: true,
         keyboard: true
         }).css({
         "width": "890px",
         "margin-left": function(){
         return -($(this).width() / 2);
         }
         });
         //console.log("ir adrese");
         }
         */

        //iegutdatus();
        //startRemain();

        $(window).scroll(function () {
          if ($(this).scrollTop() != 0) {
            $("#toTop").fadeIn();
          } else {
            $("#toTop").fadeOut();
          }
        });

    {literal}
        $("#toTop").click(function () {
          $("body,html").animate({scrollTop: 0}, 800);
        });

        $(".suncic").delay(1000).slideUp(2000, function () {
          $(".masivs").animate({"margin-top": "70px"}, 2000);
        });

    {/literal}

        $(".footer-content").hover(function () {
          $(".footer-img").fadeOut();
          $(".footer-cr").fadeIn("1000");
        },
                function () {
                  $(".footer-cr").fadeOut("1000");
                  $(".footer-img").fadeIn();
                });


        $(document).on("click", ".fast-user-settings", function () {
          var exp_elem = $(this).parent().find(".fast-user-settings-expand");

          exp_elem.fadeToggle(function () {

            if (exp_elem.is(":visible")) {
              $(this).parent().find(".user-chevron").removeClass("fa-chevron-down").addClass("fa-chevron-up");
            } else {
              $(this).parent().find(".user-chevron").removeClass("fa-chevron-up").addClass("fa-chevron-down");
            }

          });



        });

      });

  </script>

  <script src="{$url}js/translate.js"></script>
  {if $locale_file}
    <script src="{$url}{$locale_file}"></script>
  {/if}
  {asset_compile}
  <script type="text/javascript" src="{$url}js/twitter.js"></script>
  <script type="text/javascript" src="{$url}js/facebook.js"></script>
  <script type="text/javascript" src="{$url}js/draugiem.js"></script>

  <script type="text/javascript" src="{$url}js/init.js"></script>
  <script type="text/javascript" src="{$url}js/pages.js"></script>
  <script type="text/javascript" src="{$url}js/tabs.js"></script>
  <script type="text/javascript" src="{$url}js/notify.js"></script>
  <script type="text/javascript" src="{$url}js/gradio/gradio.js"></script>
  <script type="text/javascript" src="{$url}js/gradio/gradio.datatables.js"></script>

  <script type="text/javascript" src="{$url}assets/tune-plugin/tune.js"></script>
 {/asset_compile}

  <script type="text/javascript">

      //console.debug( t("plug_gradio_loading_data_from_serverr") );

      // global ajax options
      $(document).ajaxStart(function () {
        xpnCMS.NProgressStart();
      });

      $(document).ajaxStop(function () {
        xpnCMS.NProgressStop();
      });

      $(document).ajaxComplete(function(event, request, settings) {
        var responseJSON = request.responseJSON;

        if (typeof responseJSON !== 'object') {
          return;
        }

        if (typeof responseJSON.notify === 'object' && !_.isEmpty(responseJSON.notify)) {
          xpnCMSNotify.Notify(responseJSON.notify);
        }

        if (typeof responseJSON.simpleNotify === 'object' && !_.isEmpty(responseJSON.simpleNotify)) {
          xpnCMSNotify.simpleNotify(responseJSON.simpleNotify);
        }

        if (typeof responseJSON.slackHook === 'object' && !_.isEmpty(responseJSON.slackHook)) {
          $.post('/action/slackhook', responseJSON.slackHook);
        }

      });

      $(document).on("click", ".adm-del", function () {
        var href = $(this).attr("data-href");
        var alert = $(this).attr('data-confirm');

        var notify = {
          title: t("Please confirm"),
          alert: alert,
          type: "error"
        }

        //$("#dataConfirmModal").find(".modal-body").text($(this).attr("data-confirm"));
        //$("#dataConfirmOK").attr("href", href);
        //$("#dataConfirmModal").modal("show");
        xpnCMSNotify.confirmNotify(notify, href);
        return false;
      });

  </script>

  {if isset($ajax) && $ajax == false}
    <script type="text/javascript">
      xpnCMS.loadPage("{$pageId}");
    </script>
  {/if}

  <script>
    var footerRenderTime = '{"end"|timer}';
    {literal}
      _.extend(templateRenderTimes, {footer: footerRenderTime});
    {/literal}
  </script>

  {if isset($debugbarRender)}
    {$debugbarRender}
  {/if}

  <!-- {"list"|timer:true} -->

  {/block}

{/block}
