                
                  <div class="now-playing-card white bg">
                    <div class="status-bar">{t}Now Playing{/t}</div>
                    <div class="beat__column-1"></div>
                    <div class="beat__column-2"></div>
                    <div class="beat__column-3"></div>
                    <div class="artist-img-container">
                      <i class="fa fa-heart faa-pulse animated fa-3x already-voted"></i>
                      <img
                        class="artist-img"
                        src="{$url}themes/gradio-mobile/img/default/not-found.png">
                      <img class="curved-path" src="{$url}themes/gradio-mobile/img/bg/Path_2_2.svg">
                    </div>
                    <div class="song-meta">
                      <div class="artist">{t}Artist{/t}</div>
                      <div class="title">{t}Title{/t}</div>
                    </div>
                    <div class="controls">
                      <div class="extra pull-left">
                        <div class="heart control">
                          <div class="actionBox">
                            <span class="showEmotions">
                              <a class="">
                                <div class="button-love rate"
                                     data-item-id="{$tune.id}"
                                     data-action="liked"
                                     data-action-category="tune">
                                </div></a>
                              <div class="emoji-reactions">
                                <span>
                                  <div class="reaction rate"
                                       data-item-id="{$tune.id}"
                                       data-action="liked"
                                       data-action-category="tune">
                                    <i class="react like"></i>
                                  </div>
                                  <div class="text">
                                    <div>{t}Like{/t}</div>
                                  </div>
                                </span>
                  
                                {*
                                <!--<span>
                                    <div class="reaction">
                                      <i class="react love"></i>
                                    </div>
                                    <div class="text">
                                      <div>Love</div>
                                    </div>
                                  </span>-->
                  
                                <!-- <span>
                                    <div class="reaction">
                                      <i class="react haha"></i>
                                    </div>
                                    <div class="text">
                                      <div>Haha</div>
                                    </div>
                                  </span>-->
                  
                                <!--<span>
                                    <div class="reaction">
                                      <i class="react sorry"></i>
                                    </div>
                                    <div class="text">
                                      <div>Sorry</div>
                                    </div>
                                  </span>-->
                  
                                <!--<span>
                                    <div class="reaction">
                                      <i class="react anger"></i>
                                    </div>
                                    <div class="text">
                                      <div>Angry</div>
                                    </div>
                                  </span>-->
                                  *}
                  
                                <span>
                                  <div class="reaction rate"
                                       data-item-id="{$tune.id}"
                                       data-action="disliked"
                                       data-action-category="tune">
                                    <i class="react dislike"></i>
                                  </div>
                                  <div class="text">
                                    <div>{t}Dislike{/t}</div>
                                  </div>
                                </span>
                              </div>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--
                    <div class="progress">
                      <div class="progress__bar red bg"></div>
                    </div>
                    -->
                  </div>
