<!-- gdlr-player -->
<!-- small play button -->
<div class="gdlr-open-float-player" id="gdlr-open-float-player">
  <i class="fa fa-play"></i>
</div>

<div class="gdlr-float-player" id="gdlr-float-player">
  <div class="container">
    <div class="gdlr-float-top-player gdlr-item">

      <div class="gdlr-top-player-thumbnail">
        <div class="now-playing-img">
          <a href="#header">
            <img src="{$url}{$paths.assets}/gradio-player/img/no-now-playing-art.png" alt="{t}Now playing picture{/t}"/>
          </a>
        </div>
      </div>

      <div class="gdlr-top-player-title">
        <span class="gdlr-song-title-info">{$plugins.gradio.prefs.streams.aacp.name}</span>
      </div>
      <div class="gdlr-top-player-download">
        <a class="top-player-list" href="#">
          <i class="fa fa-list-ul fa-2x"></i>
        </a>
        {*
        <a class="top-player-download gdlr-download" target="_blank" href="http://audiojungle.net/item/smash-it-up/2304628">
        <img src="http://demo.goodlayers.com/musicclub/wp-content/themes/musicclub/images/icon-download.png"
        alt="fa fa-download" />
        </a>
        <a class="top-player-apple" target="_blank" href="http://audiojungle.net/item/smash-it-up/2304628">
        <img src="http://demo.goodlayers.com/musicclub/wp-content/themes/musicclub/images/icon-apple.png"
        alt="fa fa-download" />
        </a>
        <a class="top-player-amazon" target="_blank" href="http://audiojungle.net/item/smash-it-up/2304628">
        <img src="http://demo.goodlayers.com/musicclub/wp-content/themes/musicclub/images/icon-amazon.png"
        alt="fa fa-download" />
        </a>
        *}
      </div>
      <audio class="gdlr-audio-player" preload="none" style="width: 100%; height: 70px;">
        <source type="audio/mpeg" src="{$plugins.gradio.prefs.streams.aacp.url}" />
      </audio>
    </div>
    <ol class="gdlr-player-list">
      <li
        data-download="{$plugins.gradio.prefs.streams.aacp.url}"
        data-apple="{$plugins.gradio.prefs.streams.aacp.url}"
        data-amazon="{$plugins.gradio.prefs.streams.aacp.url}"
        data-mp3="{$plugins.gradio.prefs.streams.aacp.url}">
        <span class="gdlr-song-title-info">{$plugins.gradio.prefs.streams.aacp.name}</span>
      </li>

      <!--
      <li
        class="active"
        data-download="{$plugins.gradio.prefs.streams.opus.url}"
        data-apple="{$plugins.gradio.prefs.streams.opus.url}"
        data-amazon="{$plugins.gradio.prefs.streams.opus.url}"
        data-mp3="{$plugins.gradio.prefs.streams.opus.url}">
        <span class="gdlr-song-title-info">{$plugins.gradio.prefs.streams.opus.name}</span>
      </li>

      <li
        data-download="{$plugins.gradio.prefs.streams.mp3.url}"
        data-apple="{$plugins.gradio.prefs.streams.mp3.url}"
        data-amazon="{$plugins.gradio.prefs.streams.mp3.url}"
        data-mp3="{$plugins.gradio.prefs.streams.mp3.url}">
        <span class="gdlr-song-title-info">{$plugins.gradio.prefs.streams.mp3.name}</span>
      </li>
      -->
    </ol>
  </div>
</div>

<script src="{$url}assets/gdlr-player/js/player.js"></script>
