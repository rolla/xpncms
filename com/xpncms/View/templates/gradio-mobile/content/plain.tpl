{block name="news" append}
  {if !isset($contents)}
    <h1 class="alert alert-danger">{t}Content not found{/t}</h1>
  {else}
    {assign var=i3 value=1}
    {foreach $contents as $key => $content}
      <div class="row recent-news-block wow fadeInLeft" data-wow-delay="{$i3}s">
        <div class="col-xs-11">
          <a
            href="{$url}content/{$content.cont_alias}"
            class="ajax-nav"
            data-href="{$url}content/{$content.cont_alias}"
            data-target=".content">
            <div
              class="recent-news-block-image thumb"
              style="background-image: url({$phpThumbUrl}?q=100&h=500&zc=1&src=/{$paths.images}/{$content.cont_imgs|escape:'url'})">
            </div>
            <h3 class="text-center recent-news-block-title">
              {$content.cont_name}
            </h3>
          </a>
        </div>
      </div>
      {assign var=i3 value=$i3+0.2}
    {/foreach}
  {/if}
{/block}

        <!--
        <div class="col-xs-4 wow fadeInRight delay-12s">
  
          <!--
          <div class="row">
            <div class="col-xs-3 recent-news-block-date">
              <i class="fa fa-clock-o"></i> {$content.cont_added|relative_date}
              <br>
              <i class="fa fa-comments"></i>
              <a href="{$url}content/{$content.cont_alias}/" class="ajax-nav">
                <span class="fb-comments-count" data-href="{$fbsiteurl}/content/{$content.cont_alias}/">0</span> {t}Comments{/t}
              </a>
              <br>
              <i class="fa fa-tag"></i> 
              <a href="{$url}category/{$content.cat_alias}" class="ajax-nav"> {$content.cont_category|emoji}</a>
            </div>
            <div class="col-xs-9 recent-news-block-content">
              {$content.cont_content|truncate:450:"...":true|emoji|strip_tags:"<br><p><div><h1>&nbsp;"}
              <br>
              <a href="{$url}content/{$content.cont_alias}" data-href="{$url}content/{$content.cont_alias}" class="ajax-nav btn btn-default pull-right">Lasīt visu</a>
            </div>
            <!--
            <div class="col-xs-4 recent-news-block-comments">
  
            </div>
            <div class="col-xs-4 recent-news-block-tag">
  
            </div>
          </div>
          
        </div>
        -->
