{capture name="page_title"}
  {block name="title" prepend}{$page_title} :{/block}
{/capture}

{block name="content"}
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <h3>{t}Content{/t}</h3>
        <div class="col-md-12">
          <div class="content-container">
            <div class="content-container-inner">

              {if !isset($contents)}
                <h1 class="alert alert-danger">{t}Content not found{/t}</h1>
              {else isset($contents) }
                {foreach $contents as $content}
                  <div class="content-item">
                    <div class="row content-item-header">
                      <div class="content-item-title"><h1>{$content.cont_name}</h1></div>
                      <div class="content-item-subtitle"></div>
                    </div>

                    <div class="row content-item-body">

                      <div class="col-md-3 content-item-body-left">

                        <div class="content-item-image">
                          <img src="{$phpThumbUrl}?q=80&w=350&zc=1&src=/{$paths.images}/{$content.cont_imgs}"/>
                        </div>
                        <div class="content-item-added" title="{$content.cont_added}"><i class="fa fa-clock-o"></i> {$content.cont_added|relative_date}
                        </div>
                        <div class="content-item-tag"><i class="fa fa-tag"></i> <a href="{$url}category/{$content.cat_alias}" class="ajax-nav"> {$content.cont_category|emoji}</a>
                        </div>

                        <script id="{$pageId}_{$content.cont_id}_part_ratings" type="text/x-jsmart-tmpl">
                          {include file="content/part_ratings.tpl"}
                        </script>

                        <div class="{$pageId}_{$content.cont_id}_part_ratings">
                          {include file='content/part_ratings.tpl'}
                        </div>

                      </div>
                      <div class="col-md-9 content-item-body-center">
                        <div class="content-item-text always-visible">
                          {$content.cont_content|emoji}
                        </div>
                      </div>
                      <div class="content-item-body-right">

                      </div>
                    </div>

                    <div class="row content-item-footer">

                      <div class="content-item-comments">

                        <div id="fb-comment-content">
                          <div style="margin-left: 10px">
                            <fb:comments href="{$fbsiteoldurl}/content/{$content.cont_alias}/" numposts="5" colorscheme="light" style=""></fb:comments>
                          </div>
                        </div>

                      </div>

                    </div>

                  </div>
                {/foreach}

              {/if}


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
{/block}
