<div class="row">
  <span class="col-xs-3 likes-count count">
    {count($content.likes)}
    <i
      class="fa fa-5x fa-thumbs-up rate"
      data-action="liked"
      data-action-category="news"
      data-item-id="{$content.cont_id}"
      data-page-id="{$pageId}"
      data-partial="_part_ratings" >
    </i>
    <div class="row content-item-likes">
      {if $content.likes}
        <div class="likers-container">
          {foreach $content.likes as $likes}
            <span class="col-xs-1 liker pull-left">
              <!--
              <img src="{userimage user_id=$likes.user_id photo_url=$likes.photo_url phpthumb='w=50&q=80'}">
              -->
              <span class="liker name clearfix">{$likes.display_name|emoji}</span></span>
            {/foreach}
        </div>
      {/if}
    </div>
  </span>
  <span class="col-xs-3 dislikes-count count">
    {count($content.dislikes)}
    <i
      class="fa fa-5x fa-thumbs-down rate"
      data-action="disliked"
      data-action-category="news"
      data-item-id="{$content.cont_id}"
      data-page-id="{$pageId}"
      data-partial="_part_ratings">
    </i>
    <div class="row content-item-dislikes">
      {if $content.dislikes}
        <div class="dislikers-container">
          {foreach $content.dislikes as $dislikes}
            <span class="col-xs-1 disliker pull-left">
              <!--
              <img src="{userimage user_id=$dislikes.user_id photo_url=$dislikes.photo_url phpthumb='w=50&q=80'}">
              -->
              <span class="disliker name clearfix">{$dislikes.display_name|emoji}</span></span>
            {/foreach}
        </div>
      {/if}
    </div>
  </span>
</div>
