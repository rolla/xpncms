{block name="latest-videos"}
  {assign var=i1 value=2}
  {foreach $latestTuneVideos as $video}
    <div class="row wow fadeInRight delay-{$i1}s">
      <iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/{$video.youtube_id}" style="width: 100%;height: 100%;"></iframe>
      <br>
      <a href="{$url}tune/{$video.song_id}" class="btn btn-default btn-block ajax-nav" data-target=".content">Info</a>
    </div>
    <br>
    {assign var=i1 value=$i1+1}
  {/foreach}
{/block}
