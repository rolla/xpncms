{block name="content" append}
  <div class="content">
    <section class="main-section" id="user-profile">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-3">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="media">
                  <div align="center">
                    <img class="thumbnail img-responsive" src="{$userData.photo_url}" width="300px" height="300px">
                  </div>
                  <div class="media-body">
                    <hr>
                    <h3><strong>{t}Roles{/t}</strong></h3>
                    <p>{$userData.roles}</p>
                    <hr>
                    <h3><strong>{t}Location{/t}</strong></h3>
                    <p>{$userData.region}</p>
                    <hr>
                    <h3><strong>{t}Gender{/t}</strong></h3>
                    <p>{$userData.gender}</p>
                    <hr>
                    <h3><strong>{t}Birthday{/t}</strong></h3>
                    <p>{$userData.birthday}</p>
                    <hr>
                    <h3><strong>{t}Visits{/t}</strong></h3>
                    <p>{$userData.visits}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-9">
            {t}Latest user actions{/t}  ({$actions.human|count})
            <br><br>

            <div class="timeline">

              <!-- Line component -->
              <div class="line text-muted"></div>

              {foreach array_reverse($actions.human) as $action}

                {if $action.for == 'tune' && $action.action == 'updatedfake'}
                  {continue}
                {/if}

                <!-- Separator -->
                <div class="separator text-muted">
                  <time
                    title="{$action.action} {$action.for}
                    {if preg_match('/tune/', $action.identificator)}
                        {$action.item_data.0.artist} - {$action.item_data.0.title}
                    {/if}">
                    {$action.datetime|relative_date}
                  </time>
                </div>
                <!-- /Separator -->

                {if $action.for == 'tune info' && ($action.action == 'added' or $action.action == 'updated') }
                  <!-- Panel -->
                  <article class="panel panel-primary">

                    <!-- Icon -->
                    <div class="panel-heading icon">
                      <i class="{$action.icon} fa-5x"></i>
                    </div>
                    <!-- /Icon -->

                    <!-- Heading -->
                    <div class="panel-heading">
                      <h2 class="panel-title">{$action.action} {$action.for}</h2>
                    </div>
                    <!-- /Heading -->

                    <div class="panel-body">

                      <div class="col-xs-12 col-sm-6">
                        {include file="{APP_PLUGIN}gradio/View/music/partial/tune-card.tpl" tune=$action.item_data.0}
                      </div>
                      <div class="col-xs-12 col-sm-6">
                        {if !empty($action.item_data.0.youtube)}
                          <iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/{$action.item_data.0.youtube_id}" style="width: 100%;height: 100%;"></iframe>
                        {/if}
                      </div>

                    </div>
                  </article>
                  <!-- /Panel -->
                {/if}

                {if $action.for == 'tune' && $action.action == 'updated' }
                  <!-- Panel -->
                  <article class="panel panel-primary">

                    <!-- Icon -->
                    <div class="panel-heading icon">
                      <i class="{$action.icon} fa-5x"></i>
                    </div>
                    <!-- /Icon -->

                    <!-- Heading -->
                    <div class="panel-heading">
                      <h2 class="panel-title">{$action.action} {$action.for}</h2>
                    </div>
                    <!-- /Heading -->

                    <div class="panel-body">

                      <div class="col-xs-12 col-sm-6">
                        {include file="{APP_PLUGIN}gradio/View/music/partial/tune-card.tpl" tune=$action.item_data.0}
                      </div>


                    </div>
                  </article>
                  <!-- /Panel -->
                {/if}


                {if $action.for == 'tune' && $action.action == 'liked' }
                  <!-- Panel -->
                  <article class="panel panel-success">

                    <!-- Icon -->
                    <div class="panel-heading icon">
                      <i class="{$action.icon} fa-5x"></i>
                    </div>
                    <!-- /Icon -->

                    <!-- Heading -->
                    <div class="panel-heading">
                      <h2 class="panel-title">{$action.action} {$action.for}</h2>
                    </div>
                    <!-- /Heading -->

                    {assign var="tune" value=$action.item_data.0}
                    <div class="panel-body">
                      <div class="col-xs-12 col-sm-6">
                        {include file="{APP_PLUGIN}gradio/View/music/partial/tune-card.tpl" tune=$action.item_data.0}
                      </div>
                  </article>
                  <!-- /Panel -->
                {/if}

                {if $action.for == 'tune' && $action.action == 'disliked' }
                  <!-- Panel -->
                  <article class="panel panel-danger">
                    <!-- Icon -->
                    <div class="panel-heading icon">
                      <i class="{$action.icon} fa-5x"></i>
                    </div>
                    <!-- /Icon -->
                    <!-- Heading -->
                    <div class="panel-heading">
                      <h2 class="panel-title">{$action.action} {$action.for}</h2>
                    </div>
                    <!-- /Heading -->
                    {assign var="tune" value=$action.item_data.0}
                    <div class="panel-body">
                      <div class="col-xs-12 col-sm-6">
                        {include file="{APP_PLUGIN}gradio/View/music/partial/tune-card.tpl" tune=$action.item_data.0}
                      </div>
                  </article>
                  <!-- /Panel -->
                {/if}

                {if $action.action == 'comes online' }
                  <!-- Panel -->
                  <article class="panel panel-info panel-outline">
                    <!-- Icon -->
                    <div class="panel-heading icon">
                      <i class="{$action.icon} fa-5x"></i>
                    </div>
                    <!-- /Icon -->
                    <!-- Heading -->
                    <div class="panel-heading">

                    </div>
                    <!-- /Heading -->

                    <!-- Body -->
                    <div class="panel-body">
                      <i class="{$action.ac_icon}"></i> {$action.action} {$action.for}
                    </div>
                    <!-- /Body -->

                  </article>
                  <!-- /Panel -->
                {/if}

                {if $action.action == 'goes offline' }
                  <!-- Panel -->
                  <article class="panel panel-info panel-outline">
                    <!-- Icon -->
                    <div class="panel-heading icon">
                      <i class="{$action.icon} fa-5x"></i>
                    </div>
                    <!-- /Icon -->
                    <!-- Heading -->
                    <div class="panel-heading">

                    </div>
                    <!-- /Heading -->

                    <!-- Body -->
                    <div class="panel-body">
                      <i class="{$action.ac_icon}"></i> {$action.action} {$action.for}
                    </div>
                    <!-- /Body -->

                  </article>
                  <!-- /Panel -->
                {/if}

              {/foreach}
            </div>

            {*{foreach array_reverse($actions.human) as $action}
            <div>
            {if preg_match('/tune/', $action.identificator)}
            <img src="{$action.item_data.0.resizeCover}" width="50px" />
            {else}
            <img src="{$url}img/logos/only-g-without-gloss-web-200.png" width="50px" />
            {/if}

            <i class="fa fa-clock-o"></i> {$action.datetime|relative_date}
            {if !empty($action.icon)}
            <i class="{$action.icon}" title="{$action.action}"></i>{$action.for}
            {else}
            {$action.action} {$action.for}
            {/if}

            {if preg_match('/tune/', $action.identificator)}
            <a href="{$url}tune/{$action.item_data.0.id}" class="ajax-nav" data-target=".content">
            {$action.item_data.0.artist} - {$action.item_data.0.title}
            </a>
            {/if}

            </div>
            <br>
            {/foreach}*}

          </div>
        </div>
      </div>
    </section>
  </div>

{/block}