{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="content" append}
<div class="content">
  <section class="main-section" id="user-settings">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h1 class="slim-text center">{$title} {t}{$page_title}{/t}</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-6 ">
          <div class="user-image-container">
            <div class="user-image-container-inner">
              <div class="user-image-controls">
                <button class="btn btn-info your-files"><i class="fa fa-files-o "></i> {t}Change picture{/t}</button>
              </div>
              <img src="{$userData.photo_url}" alt="{$userData.first_name} {$userData.last_name}" class="user-image" style="width: 100%" />
            </div>
          </div>
        </div>
        <div class="col-md-5 col-sm-3 col-xs-6 ">
          <h2>{$userData.first_name} {$userData.last_name}</h2>
          <u>{t}Your profile information{/t}</u><br><br>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <thead>
              <tr>
                <th width="40%"></th>
                <th width="60%"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>{t}User ID{/t}</b></td>
                <td>{$userData.id}</td>
              </tr>
              <tr>
                <td><b>{t}User roles{/t}</b></td>
                <td>{$userData.roles}</td>
              </tr>
              <tr>
                <td><b>{t}First name{/t}</b></td>
                <td><a href="#" id="first_name" data-type="text" data-pk="{$userData.id}" data-title="{t}First name{/t}">{$userData.first_name}</a></td>
              </tr>
              <tr>
                <td><b>{t}Last name{/t}</b></td>
                <td><a href="#" id="last_name" data-type="text" data-pk="{$userData.id}" data-title="{t}Last name{/t}">{$userData.last_name}</a></td>
              </tr>
              <tr>
                <td><b>{t}Show as{/t}</b></td>
                <td><a href="#" id="display_name" data-type="text" data-pk="{$userData.id}" data-title="{t}Show as{/t}"><b>{$userData.display_name|emoji}</b></a></td>
              </tr>
              <tr>
                <td><b>{t}E-mail{/t}</b></td>
                <td><a href="#" id="email" data-type="text" data-pk="{$userData.id}" data-title="{t}E-mail{/t}">{$userData.email}</a></td>
              </tr>
              <tr>
                <td><b>{t}Description{/t}</b></td>
                <td><a href="#" id="description" data-type="textarea" data-pk="{$userData.id}" data-title="{t}Description{/t}"><b>{$userData.description}</b></a></td>
              </tr>
              <tr>
                <td><b>{t}Gender{/t}</b></td>
                <td><a href="#" id="gender" data-type="select" data-pk="{$userData.id}" data-title="{t}Gender{/t}"><b>{$userData.gender}</b></a></td>
              </tr>
              <tr>
                <td><b>{t}Birthday{/t}</b></td>
                <td><a href="#" id="birthday" data-type="date" data-pk="{$userData.id}" data-title="{t}Birthday{/t}"><b>{$userData.birthday}</b></a></td>
              </tr>
              <tr>
                <td><b>{t}Last visit{/t}</b></td>
                <td><b>{$userData.lastvisit|relative_date}</b></td>
              </tr>
              {if isset($userData.updated)}
              <tr>
                <td><b>{t}Updated{/t}</b></td>
                <td><b>{$userData.updated|relative_date}</b></td>
              </tr>
              {/if}
              <tr>
                <td><b>{t}Registered{/t}</b></td>
                <td><b>{$userData.added|relative_date}</b></td>
              </tr>
              </tbody>
          </table>
          <!--<button class="btn btn-info your-files"><i class="fa fa-files-o "></i> {t}Your files{/t}</button>-->
          {timeline}
        </div>
        <div class="col-md-4 col-sm-3 col-xs-6 ">
        </div>
        <div id="elfinder"></div>
      </div>
      {connect_disconnect}
    </div>
  </section>
</div>

{/block}
