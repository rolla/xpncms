{capture name="page_title"}
{block name="title" prepend}{$page_title} :{/block}
{/capture}
{block name="body" append}
  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li class="active">
      <a
        href="#users"
        role="tab"
        data-toggle="tab">
        <h1>{t}Users{/t} {t}Online{/t} <span class="badge badge-success" style="font-size: 20px">{$users.online}</span></h1>
      </a>
    </li>
    <li>
      <a
        href="#guests"
        role="tab"
        data-toggle="tab">
        <h1>{t}Guests{/t} {t}Online{/t} <span class="badge badge-success" style="font-size: 20px">{$guests.online}</span></h1>
      </a>
    </li>
  </ul>
  <br>
  <div class="row">
    <div class="online-users-container">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="users">
        {if count($users.items) }
          {foreach $users.items as $user}
            <div class="col-md-2 col-sm-3 col-xs-6 ">
              <div class="online-user-container">
                <div class="online-user-inner">
                  <h2 title="{$user.display_name|emoji}">{$user.display_name|emoji|truncate:17}
                    {if isset($user.online) }
                      <i class="fa fa-circle" style="color: green" title="{t}Online{/t}"></i>
                    {/if}
                    {if isset($user.latest_provider) }
                      <i class="fa fa-{$user.latest_provider}" alt="{$user.latest_provider}" title="{$user.latest_provider}"></i>
                    {/if}
                  </h2>
                  <div
                    class="online-user-pic"
                    style="background-image: url('{$user.image}');
                    background-size: cover;
                    width: 100%;
                    height: 100%;
                    margin-top: -50px;
                    position: absolute;
                    z-index: 1;">
                  </div>
                  <!--<img src="{$user.photo_url}" alt="{$user.display_name|emoji}" class="online-user-pic"/>-->
                  <h3>{$user.lastvisit|relative_date}</h3>
                </div>
              </div>
            </div>
          {/foreach}
        {else}
          <h1 class="alert alert-danger">{t}No user visits{/t}</h1>
        {/if}
        </div>
        <div class="tab-pane fade" id="guests">
        {if count($guests.items) }
          {foreach $guests.items as $user}
            <div class="col-md-2 col-sm-3 col-xs-6 ">
              <div class="online-user-container">
                <div class="online-user-inner">
                  <h2>{t}Guest{/t}
                    {if isset($user.online) }
                      <i class="fa fa-circle" style="color: green" title="{t}Online{/t}"></i>
                    {/if}
                  </h2>
                  <div
                    class="online-user-pic"
                    style="background-image: url('{$user.image}');
                    background-size: cover;
                    width: 100%;
                    height: 100%;
                    margin-top: -50px;
                    position: absolute;
                    z-index: 1;">
                  </div>
                  <h3>{$user.lastvisit|relative_date}</h3>
                </div>
              </div>
            </div>
          {/foreach}
        {else}
          <h1 class="alert alert-danger">{t}No guest visits{/t}</h1>
        {/if}
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div id="widgetIframe">
      <iframe
        width="100%"
        height="900"
        src="{$piwik.url}index.php?module=Widgetize&action=iframe&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=index&idSite={$piwik.id}&period=day&date=today&disableLink=1&widget=1&token_auth={$piwik.token}"
        scrolling="no"
        frameborder="0"
        marginheight="0"
        marginwidth="0">
      </iframe>
    </div>
  </div>
{/block}
