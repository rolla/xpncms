{capture name="page_title"}{block name="title" prepend}{$page_title} :{/block}{/capture}

{block name="content" append}
  <div class="content">
    <section class="main-section" id="news">
      <div class="container">
        <h2>{t}Newest{/t}</h2>
        <h6>{t}Interesting about anything.{/t}</h6>
        <div class="row">
          <div class="col-md-7">
            <!--
            <div class="content-item">
              <div class="dyn-content">
              {block name="content-item"}{/block}
              </div>
            </div>
            -->
            <a href="{$url}content" class="ajax-nav" data-target=".content"><h3>{t}Latest news{/t}</h3></a>
            <div class="news-content infinite-scroll">
              {include file='content/plain.tpl'}
            </div>
            <!--<a href="{$url}content/page/2/" id="next" class="btn btn-default btn-block ajax-nav">{t}More news{/t}</a>-->
          </div> 
          <div class="col-md-5">
            <h3>{t}Latest videos{/t}</h3>
            {include file='content/latest-videos.tpl'}
          </div>
        </div>
      </div>
    </section>

    <section class="main-section latest-tunes-section" id="latest-tunes">  
      <div class="container-fluid">
        <a href="{$url}gradio/latest-tunes" class="ajax-nav" data-target=".content"><h2>{t}Newest Tunes{/t}</h2></a>
        <div class="row">
          <div class="col-xs-12">
          {include file='content/latest-tunes.tpl'}
          </div>
          <div class="col-xs-4 col-xs-offset-4"><a href="{$url}gradio/latest-tunes" class="ajax-nav btn btn-block btn-default" data-target=".content">{t}See more{/t}</a>
        </div>
      </div>
    </section>
  
  <!--
  <section class="main-section" id="service">
    <div class="container">
      <h2>{t}Services{/t}</h2>
      <h6>{t}We offer exceptional service with complimentary hugs.{/t}</h6>
      <div class="row">
        <div class="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
          <div class="service-list">
            <div class="service-list-col1">
              <i class="fa-paw"></i>
            </div>
            <div class="service-list-col2">
              <h3>{t}branding &amp; identity{/t}</h3>
              <p>{t}Proin iaculis purus digni consequat sem digni ssim. Donec entum digni ssim.{/t}</p>
            </div>
          </div>
          <div class="service-list">
            <div class="service-list-col1">
              <i class="fa-gear"></i>
            </div>
            <div class="service-list-col2">
              <h3>{t}web development{/t}</h3>
              <p>{t}Proin iaculis purus consequat sem digni ssim. Digni ssim porttitora.{/t}</p>
            </div>
          </div>
          <div class="service-list">
            <div class="service-list-col1">
              <i class="fa-apple"></i>
            </div>
            <div class="service-list-col2">
              <h3>{t}mobile design{/t}</h3>
              <p>{t}Proin iaculis purus consequat digni sem digni ssim. Purus donec porttitora entum.{/t}</p>
            </div>
          </div>
          <div class="service-list">
            <div class="service-list-col1">
              <i class="fa-medkit"></i>
            </div>
            <div class="service-list-col2">
              <h3>{t}24/7 Support{/t}</h3>
              <p>{t}Proin iaculis purus consequat sem digni ssim. Sem porttitora entum.{/t}</p>
            </div>
          </div>
        </div>
        <figure class="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
          <img src="{$url}themes/Knight/img/macbook-pro.png" alt="">
        </figure>

      </div>
    </div>
  </section>

  <section class="main-section alabaster">
    <div class="container">
      <div class="row">
        <figure class="col-lg-5 col-sm-4 wow fadeInLeft">
          <img  src="{$url}themes/Knight/img/iphone.png" alt="">
        </figure>
        <div class="col-lg-7 col-sm-8 featured-work">
          <h2>{t}featured work{/t}</h2>
          <P class="padding-b">{t}Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt. Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit.{/t}</P>
          <div class="featured-box">
            <div class="featured-box-col1 wow fadeInRight delay-02s">
              <i class="fa-magic"></i>
            </div>    
            <div class="featured-box-col2 wow fadeInRight delay-02s">
              <h3>{t}magic of theme development{/t}</h3>
              <p>{t}Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.{/t}</p>
            </div>  
          </div>
          <div class="featured-box">
            <div class="featured-box-col1 wow fadeInRight delay-04s">
              <i class="fa-gift"></i>
            </div>    
            <div class="featured-box-col2 wow fadeInRight delay-04s">
              <h3>{t}neatly packaged{/t}</h3>
              <p>{t}Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.{/t}</p>
            </div>  
          </div>
          <div class="featured-box">
            <div class="featured-box-col1 wow fadeInRight delay-06s">
              <i class="fa-dashboard"></i>
            </div>    
            <div class="featured-box-col2 wow fadeInRight delay-06s">
              <h3>{t}SEO optimized{/t}</h3>
              <p>{t}Proin iaculis purus consequat sem cure digni ssim. Donec porttitora entum suscipit aenean rhoncus posuere odio in tincidunt.{/t}</p>
            </div>  
          </div>
          <a class="Learn-More" href="#">{t}Learn More{/t}</a>
        </div>
      </div>
    </div>
  </section>

  <section class="main-section paddind" id="Portfolio">
    <div class="container">
      <h2>{t}Portfolio{/t}</h2>
      <h6>{t}Fresh portfolio of designs that will keep you wanting more.{/t}</h6>
      <div class="portfolioFilter">  
        <ul class="Portfolio-nav wow fadeIn delay-02s">
          <li><a href="#" data-filter="*" class="current" >{t}All{/t}</a></li>
          <li><a href="#" data-filter=".branding" >{t}Branding{/t}</a></li>
          <li><a href="#" data-filter=".webdesign" >{t}Web design{/t}</a></li>
          <li><a href="#" data-filter=".printdesign" >{t}Print design{/t}</a></li>
          <li><a href="#" data-filter=".photography" >{t}Photography{/t}</a></li>
        </ul>
      </div> 

    </div>
    <div class="portfolioContainer wow fadeInUp delay-04s">
      <div class=" Portfolio-box printdesign">
        <a href="#"><img src="{$url}themes/Knight/img/Portfolio-pic1.jpg" alt=""></a>   
        <h3>{t}Foto Album{/t}</h3>
        <p>{t}Print Design{/t}</p>
      </div>
      <div class="Portfolio-box webdesign">
        <a href="#"><img src="{$url}themes/Knight/img/Portfolio-pic2.jpg" alt=""></a>   
        <h3>{t}Luca Theme{/t}</h3>
        <p>{t}Web Design{/t}</p>
      </div>
      <div class=" Portfolio-box branding">
        <a href="#"><img src="{$url}themes/Knight/img/Portfolio-pic3.jpg" alt=""></a>   
        <h3>{t}Uni Sans{/t}</h3>
        <p>{t}Branding{/t}</p>
      </div>
      <div class=" Portfolio-box photography" >
        <a href="#"><img src="{$url}themes/Knight/img/Portfolio-pic4.jpg" alt=""></a>   
        <h3>{t}Vinyl Record{/t}</h3>
        <p>{t}Photography{/t}</p>
      </div>
      <div class=" Portfolio-box branding">
        <a href="#"><img src="{$url}themes/Knight/img/Portfolio-pic5.jpg" alt=""></a>   
        <h3>{t}Hipster{/t}</h3>
        <p>{t}Branding{/t}</p>
      </div>
      <div class=" Portfolio-box photography">
        <a href="#"><img src="{$url}themes/Knight/img/Portfolio-pic6.jpg" alt=""></a>   
        <h3>{t}Windmills{/t}</h3>
        <p>{t}Photography{/t}</p>
      </div>
    </div>
  </section>

  <section class="main-section client-part" id="client">
    <div class="container">
      <b class="quote-right wow fadeInDown delay-03"><i class="fa-quote-right"></i></b>
      <div class="row">
        <div class="col-lg-12">
          <p class="client-part-haead wow fadeInDown delay-05">It was a pleasure to work with the guys at Knight Studio. They made sure 
            we were well fed and drunk all the time!</p>
        </div>
      </div>
      <ul class="client wow fadeIn delay-05s">
        <li><a href="#">
            <img src="{$url}themes/Knight/img/client-pic1.jpg" alt="">
            <h3>James Bond</h3>
            <span>License To Drink Inc.</span>
          </a></li>
      </ul>
    </div>
  </section>
  <div class="c-logo-part">
    <div class="container">
      <ul>
        <li><a href="#"><img src="{$url}themes/Knight/img/c-liogo1.png" alt=""></a></li>
        <li><a href="#"><img src="{$url}themes/Knight/img/c-liogo2.png" alt=""></a></li>
        <li><a href="#"><img src="{$url}themes/Knight/img/c-liogo3.png" alt=""></a></li>
        <li><a href="#"><img src="{$url}themes/Knight/img/c-liogo4.png" alt=""></a></li>
        <li><a href="#"><img src="{$url}themes/Knight/img/c-liogo5.png" alt=""></a></li>
      </ul>
    </div>
  </div>
  -->
    <!--
    <div class="google-calendar-block">
      <iframe src="https://www.google.com/calendar/embed?showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=350&amp;wkst=2&amp;hl=lv&amp;bgcolor=%23ffffff&amp;src=u3nta5c23m14ss5ek5uss1rft4%40group.calendar.google.com&amp;color=%23853104&amp;ctz=Europe%2FRiga" style=" border-width:0 " width="277" height="350" frameborder="0" scrolling="no"></iframe>
    </div>
    -->
  </div> <!-- .content end -->
{/block}


{block name="music-stream" append}
  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div class="music-stream-block">
      <div id="gradio-player-bio" class="now-playing-bio"><div class="now-playing-bio-container"></div></div>
    </div>
  </div>
{/block}

<!--
{block name="recently-played" append}
  <div class="main-section recently-played-block" id="recently-played-block">
    <span class="text-center recently-played-block-title">{t}Recently played tunes{/t}</span>
    <a href="{$url}tune/0" class="ajax-nav recently-played-block-songid">
      <div class="recently-played-block-image" style="">
        <div class="recently-played-block-artist text-center"></div>
        <div class="recently-played-block-songtitle text-center"></div>
        <div class="recently-played-block-mask" style="background-image: url({$url}themes/{$template}/img/default/recently-played-mask.svg)"></div>
      </div>
    </a>
  </div>
 {/block}
-->

  <!--
<section class="recently-played-section" id="recently-played">
<div class="container">
  <h2>Recently Played</h2>
  <div class="row">
    <div class="col-xs-5 col-sm-5 col-md-3 col-lg-3">
  {* {block name="recently-played"}{/block} *}
  </div>
</div>
</div>
</section>
  -->
