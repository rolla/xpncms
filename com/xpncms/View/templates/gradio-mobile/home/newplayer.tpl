{block name="body" append}
			<!--<h1 class="slim-text center">{$title}</h1>-->
			
			<div class="row">

				<div class="col-xs-5 col-sm-5 col-md-3 col-lg-3">
					<div id="recently-played-block" class="recently-played-block">
						<span class="text-center recently-played-block-title">{t}Recently played tunes{/t}</span>
						<a href="{$url}tune/0" class="ajax-nav recently-played-block-songid">
						<div class="recently-played-block-image" style="">
							<div class="recently-played-block-artist text-center"></div>
							<div class="recently-played-block-songtitle text-center"></div>
							<div class="recently-played-block-mask" style="background-image: url({$url}themes/{$template}/img/default/recently-played-mask.svg)"></div>
						</div>
						</a>
					</div>

					<div class="google-calendar-block">
					<iframe src="https://www.google.com/calendar/embed?showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=350&amp;wkst=2&amp;hl=lv&amp;bgcolor=%23ffffff&amp;src=u3nta5c23m14ss5ek5uss1rft4%40group.calendar.google.com&amp;color=%23853104&amp;ctz=Europe%2FRiga" style=" border-width:0 " width="277" height="350" frameborder="0" scrolling="no"></iframe>
					</div>
				</div>

				<div class="col-xs-7 col-sm-7 col-md-5 col-lg-5">
					<!--<img src="{$url}img/logos/only-g-big-without-gloss-web.png" style="width: 100%" alt="{t}Only G{/t}">-->

					{if isset($contents) }
					{$comments = 1}
					{foreach $contents as $content}

						
							<div class="recent-news-block">
								<a href="{$url}content/{$content.cont_alias}/" data-href="{$url}content/{$content.cont_alias}" class="ajax-nav"><span class="text-center recent-news-block-title">{$content.cont_name}</span></a>
								<div class="recent-news-block-inner">
									<div class="recent-news-block-image" style="background-image: url({$phpThumbUrl}?q=100&h=200&zc=1&src=../../{$paths.images}/{$content.cont_imgs})">
										<div class="recent-news-block-content">{$content.cont_content}</div>
										<div class="recent-news-block-mask" style="background-image: url({$url}themes/{$template}/img/default/recent-news-mask.svg)"></div>
										<div class="recent-news-block-date"><i class="fa fa-clock-o"></i> {$content.cont_added|relative_date}</div>
										{if $comments > 1}
										<div class="recent-news-block-comments"><i class="fa fa-comments"></i> {$comments}</div>
										{else}
										<div class="recent-news-block-comments"><i class="fa fa-comment"></i> {$comments}</div>
										{/if}
										<div class="recent-news-block-tag"><i class="fa fa-tag"></i> <a href="{$url}category/{$content.cat_alias}" class="ajax-nav"> {$content.cont_category}</a></div>
									</div>
								</div>
							</div>
					{$comments = $comments+1}
					{/foreach}
       	 			{/if}

       	 			<!--
					<div class="recent-news-block">
						<p class="text-center recent-news-block-title">Jaunums 1</p>
						<div class="recent-news-block-inner">
							<div class="recent-news-block-image" style="background-image: url({$url}img/scooter.jpg)">
								<div class="recent-news-block-content">Jaunumu ievad teksts, Jaunumu ievad teksts, Jaunumu ievad teksts</div>
								<div class="recent-news-block-mask" style="background-image: url({$url}themes/{$template}/img/default/recent-news-mask.png)"></div>
								<div class="recent-news-block-date"><i class="fa fa-clock-o"></i> {"2015-02-01 02:45:34"|relative_date}</div>
								<div class="recent-news-block-comments"><i class="fa fa-comment"></i> 1</div>
								<div class="recent-news-block-tag"><i class="fa fa-tag"></i> Aktuāli</div>
							</div>
						</div>
					</div>

					<div class="recent-news-block">
						<p class="text-center recent-news-block-title">Jaunums 2</p>
						<div class="recent-news-block-inner">
							<div class="recent-news-block-image" style="background-image: url({$url}img/scooter.jpg)">
								<div class="recent-news-block-content">Jaunumu ievad teksts, Jaunumu ievad teksts, Jaunumu ievad teksts, Jaunumu ievad teksts, Jaunumu ievad teksts</div>
								<div class="recent-news-block-mask" style="background-image: url({$url}themes/{$template}/img/default/recent-news-mask.png)"></div>
								<div class="recent-news-block-date"><i class="fa fa-clock-o"></i> {"2015-01-31 23:45:34"|relative_date}</div>
								<div class="recent-news-block-comments"><i class="fa fa-comments"></i> 12</div>
								<div class="recent-news-block-tag"><i class="fa fa-tag"></i> Mūzikas Jaunumi</div>
							</div>
						</div>
					</div>
					-->

				</div>

				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

					<div class="music-stream-block">
						
						
						<div id="gradio-player-bio" class="now-playing-bio"><div class="now-playing-bio-container"></div></div>
					</div>

					<!--
					<div class="login-block">
						<fieldset>
							<legend>{t}Sign in with social network{/t}</legend>
							<a href="{$url}auth/connect/facebook" class="btn btn-block btn-social btn-facebook auth">
								<i class="fa fa-facebook"></i> {t}Sign in with{/t} {t}Facebook{/t}
							</a>
							<a href="{$url}auth/connect/twitter" class="btn btn-block btn-social btn-twitter auth">
								<i class="fa fa-twitter"></i> {t}Sign in with{/t} {t}Twitter{/t}
							</a>
							<a href="{$url}auth/connect/draugiem" class="btn btn-block btn-social btn-draugiem auth">
								<i class="fa fa-user"></i> {t}Sign in with{/t} {t}Draugiem{/t}
							</a>
							<a href="{$url}auth/connect/google" class="btn btn-block btn-social btn-google-plus auth">
								<i class="fa fa-google-plus"></i> {t}Sign in with{/t} {t}Google{/t}
							</a>
						</fieldset>
					</div>
					-->

				</div>
			</div>
			
			<!--
			<table width="00%" border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td valign="top"><fieldset>
				
					<legend>Sign-in form</legend>
					<form action="" method="post">
					<table width="300" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td><div align="right"><strong>{t}E-mail{/t}</strong></div></td>
						<td><input type="text" name="email" /></td>
					</tr>
					<tr>
						<td><div align="right"><strong>{t}Password{/t}</strong></div></td>
						<td><input type="text" name="password" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td align="right"><input type="submit" value="{t}Sign in{/t}" /> </td>
					</tr>
					</table>
					</form>
				</fieldset></td>
				<td valign="top" align="left"> 
					
					<fieldset>
						<legend>{t}Dont have account{/t}</legend>
						&nbsp;&nbsp;<a href="?route=users/register">{t}New account signup{/t}</a><br />
					</fieldset>
				</td>
			</tr>
			</table>
			-->
{/block}