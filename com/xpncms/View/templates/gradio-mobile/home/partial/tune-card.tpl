<div class="header-card-container card-container">
  <div class="card">
    <div class="front">
      <div class="cover">
        {if ($gradioTeam)}
            <span class="tune-extension badge btn-warning wow animated fadeIn"
            data-wow-delay="4s">
              %% tune.format %%
             </span>
        {/if}
        <div><img v-bind:src="tune.phpThumbPictureURL" class="tune-cover" width="200"></div>
      </div>
      <div class="card-content">
        <div class="main">
          <h2 class="artist"><div>%% tune.artist %%</div></h2>
          <h3 class="title"><div>%% tune.title %%</div></h3>
          <h4 class="text-left">
            {t}Last played{/t}:
            <span
              class="wow animated fadeIn date-played"
              data-wow-delay="5s">
              %% moment(tune.date_played, "YYYY-MM-DD HH:mm:ss").fromNow() %%
            </span>
          </h4>
          <h4 class="text-left">{t}Count played{/t}: <span class="count-played">%% tune.count_played %%</span></h4>
          <h4 class="text-left">
          {t}Genre{/t}:
            <span class="genre">
              <span
                class="tune-card-genre badge wow animated bounceInDown"
                data-wow-delay="2s"
                v-for="genre in tune.genres">
                %% genre.name %%
              </span>
            </span>
          </h4>
        </div>
      </div>
          <img
            v-if="tune.publisher_svg"
            v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/labels/' + tune.publisher_svg"
            class="publisher pull-right wow animated fadeIn"
            data-wow-delay="6s"/>
    </div><!-- end front panel -->
    <div class="back">
      <div class="front">
        <div class="cover-back">
          <div>
            <img
              v-bind:src="tune.phpThumbPictureURL"
              class="tune-cover" width="500">
          </div>
        </div>
        <div class="card-content">
          <div class="main">
            <h4 class="text-left">{t}Year{/t}: <span class="year">%% tune.year %%</span></h4>
            <h4 class="text-left">{t}Album{/t}: <span class="album">%% tune.album %%</span></h4>
            <h4 class="text-left">{t}Date added{/t}: <span class="date-added">%% moment(tune.date_added, "YYYY-MM-DD HH:mm:ss").fromNow() %%</span></h4>
            <h4 class="text-left">{t}Weight{/t}: <span class="weight">%% tune.weight %%</span></h4>
            <h4 class="text-left">{t}BPM{/t}: <span class="bpm">%% tune.bpm %%</span></h4>

            <div class="row">
              <div class="col-xs-3">
                <div
                  class="tune-like likes rate"
                  v-bind:data-item-id="tune.id"
                  data-action="liked"
                  data-action-category="tune"
                  data-ot="{t}Like this tune{/t}"
                  title="{t}Like this tune{/t}">
                  <i class="fa fa-thumbs-o-up fa-4x"></i> <span class="rating-count">%% tune.likes %%</span>
                </div>
              </div>
              <div class="col-xs-3">
                <div
                  class="tune-dislike dislikes rate"
                  v-bind:data-item-id="tune.id"
                  data-action="disliked"
                  data-action-category="tune"
                  data-ot="{t}Dislike this tune{/t}"
                  title="{t}Dislike this tune{/t}">
                  <i class="fa fa-thumbs-o-down fa-4x"></i> <span class="rating-count">%% tune.dislikes %%</span>
                </div>
              </div>
              <div class="col-xs-6">
                <figure><chart :options="pie" ref="pie" theme="ovilia-green" auto-resize></chart></figure>
              </div>
            </div>
            <div class="row">
              <div
                v-if="tune.youtube"
                class="col-xs-3 wow fadeInRight delay-2s"
                title="{t}Youtube{/t}">
                <div class="tune-table-title" title="{t}Youtube{/t}"></div>
                <div class="tune-table-content">
                  <a v-bind:href="tune.youtube" target="_blank">
                    <img
                      v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/youtube-logo.svg'"
                      class="nowPlayingFlipCard-icon"/>
                  </a>
                </div>
              </div>
              <div
                v-else
                class="col-xs-3 wow fadeInRight delay-2s"
                title="{t}There is no youtube link added.{/t}">
                <div class="tune-table-title"></div>
                <div class="tune-table-content">
                  <img
                    v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/youtube-logo-inactive.svg'"/>
                </div>
              </div>


                <div
                  v-if="tune.soundcloud"
                  class="col-xs-3 wow fadeInRight delay-2s"
                  title="{t}Soundcloud{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-content soundcloud-content">
                    <a v-bind:href="tune.soundcloud" target="_blank">
                      <img
                        v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/soundcloud-logo.svg'"
                        class="nowPlayingFlipCard-icon"/>
                    </a>
                  </div>
                </div>

                <div
                  v-else
                  class="col-xs-3 wow fadeInRight delay-2s"
                  title="{t}There is no soundcloud link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img
                      v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/soundcloud-logo-inactive.svg'"/>
                  </div>
                </div>



                <div
                  v-if="tune.beatport"
                  class="col-xs-3 wow fadeInRight delay-2s"
                  title="{t}Beatport{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a v-bind:href="tune.beatport" target="_blank">
                      <img
                        v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/beatport-logo.svg'"
                        class="nowPlayingFlipCard-icon"/>
                    </a>
                  </div>
                </div>

                <div
                  v-else
                  class="col-xs-3 wow fadeInRight delay-2s"
                  title="{t}There is no beatport link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img
                      v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/beatport-logo-inactive.svg'"/>
                  </div>
                </div>


                <div
                  v-if="tune.itunes"
                  class="col-xs-3 wow fadeInRight delay-2s"
                  title="{t}iTunes{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <a v-bind:href="tune.itunes" target="_blank">
                      <img
                        v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/itunes-logo.svg'"
                        class="nowPlayingFlipCard-icon"/>
                    </a>
                  </div>
                </div>

                <div
                  v-else
                  class="col-xs-3 wow fadeInRight delay-2s"
                  title="{t}There is no iTunes link added.{/t}">
                  <div class="tune-table-title"></div>
                  <div class="tune-table-platform-content">
                    <img
                      v-bind:src="site.baseURL + 'themes/' + site.frontendTheme + '/img/icons/itunes-logo-inactive.svg'"/>
                  </div>
                </div>

            </div>
            <br>
            <a v-bind:href="'/tune/' + tune.song_id" class="see-more btn btn-default btn-block ajax-nav" data-target=".content">{t}See more{/t}</a>
          </div>
        </div>
      </div>
    </div> <!-- end back panel -->
  </div> <!-- end card -->
</div> <!-- end card-container -->
