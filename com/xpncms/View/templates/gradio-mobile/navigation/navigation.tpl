{block name="navigation"}
  <nav id="nav-fixed"><!--main-nav-start-->
    <div class="container-fluid">
      <div class="row ">
        <div class="col-xs-12 ">
          <div class="navbar-wrapper">
            <div class="navbar navbar-default main-nav-outer" role="navigation">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">{t}Toggle navigation{/t}</span> <span class="icon-bar"></span><span
                    class="icon-bar"></span><span class="icon-bar"></span>
                </button>
              </div>
              <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav main-navigation">
                  <li class="active">
                    <a href="{$url}" class="ajax-nav" data-target=".content">{t}Home{/t}</a>
                  </li>
                  {if ($gradioTeam)}

                   <li>
                     <a href="{$url}gradio/songs" class="ajax-nav">{t}Songs DB{/t}</a>
                   </li>
                  {else}

                   <li>
                     <a href="#header" class="">{t}Now Playing{/t}</a>
                   </li>
                  {/if}

                  <li>
                    <a href="{$url}category" class="ajax-nav" data-target=".content">{t}Categories{/t}</a>
                  </li>
                  <li>
                    <a href="{$url}gradio/recent" class="ajax-nav" data-target=".content">{t}Songs{/t}</a>
                  </li>
                  <li class="small-logo">
                    <a href="#header" class="small-logo">
                      <img src="{$url}img/logos/only-g-without-gloss-web-55.png" alt="" />
                    </a>
                  </li>
                  <li>
                    <a href="#team">{t}Team{/t}</a>
                  </li>
                  <li>
                    <a href="#contact">{t}Contact{/t}</a>
                  </li>
                </ul>
                {if ($userData)}

                <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <img
                          src="{$userData.image}"
                          class="img-responsive img-circle"
                          title="{$userData.first_name} {$userData.last_name}"
                          alt="{$userData.first_name} {$userData.last_name}"
                          width="30px"
                          height="30px" />
                      </span>
                      <span class="user-name">
                        {$userData.display_name}
                      </span>
                      <b class="caret"></b>
                    </a>

                    <ul class="dropdown-menu">
                      <li>
                        <div class="navbar-login">
                          <div class="row">
                            <div class="col-xs-4">
                              <p class="text-center">
                                <span><img src="{$userData.image}" /></span>
                              </p>
                            </div>
                            <div class="col-xs-8">
                              <p class="text-left"><strong>{$userData.first_name} {$userData.last_name}</strong></p>
                              <p class="text-left small">{$userData.email}</p>
                              <p class="text-left">
                                <a href="{$url}user/profile" class="btn btn-primary btn-block btn-sm ajax-nav">{t}Profile{/t}</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="divider navbar-login-session-bg"></li>
                      <li><a href="{$url}user/settings" class="ajax-nav">{t}User Settings{/t}<span class="glyphicon glyphicon-cog pull-right"></span></a></li>
                      {*
                      <!--
                      <li class="divider"></li>
                      <li><a href="#">User stats <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                      <li class="divider"></li>
                      <li><a href="#">Messages <span class="badge pull-right"> 42 </span></a></li>
                      <li class="divider"></li>
                      <li><a href="#">Favourites Snippets <span class="glyphicon glyphicon-heart pull-right"></span></a></li>
                      -->
                      *}
                      <li class="divider"></li>
                      <li><a href="{$url}user/logout">{t}Log out{/t} <span class="glyphicon glyphicon-log-out pull-right"></span></a></li>
                    </ul>

                    </li>
                  </ul>
                {else}

                  <ul class="nav navbar-nav navbar-right">
                    <li>
                      <a href="#login" class="social-login-button">
                        <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                          <i class="fa fa-user fa-2x"></i>
                        </span>
                        <span class="user-name">
                          {t}Login{/t}
                        </span>
                      </a>
                    </li>
                  </ul>
                  <div class="social-logins-container ontop" style="display: none;">
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 col-xl-1 pull-right social-logins-inner">
                      <fieldset>
                        <legend>{t}Sign in with social network{/t}</legend>
                        <a href="{$url}auth/connect/facebook/" class="btn btn-block btn-social btn-facebook auth">
                          <i class="fa fa-facebook"></i> {t}Facebook{/t}
                        </a>
                        <a href="{$url}auth/connect/twitter/" class="btn btn-block btn-social btn-twitter auth">
                          <i class="fa fa-twitter"></i> {t}Twitter{/t}
                        </a>
                        <a href="{$url}auth/connect/draugiem/" class="btn btn-block btn-social btn-draugiem auth">
                          <i class="fa fa-user"></i> {t}Draugiem{/t}
                        </a>
                        <a href="{$url}auth/connect/google/" class="btn btn-block btn-social btn-google auth">
                          <i class="fa fa-google-plus"></i> {t}Google{/t}
                        </a>
                      </fieldset>
                    </div>
                  </div>
                {/if}
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
  <script>
    $(document).ready(function () {
      $(document).on('click', '.social-login-button', function () {
        $('.social-logins-container').fadeToggle();
      });
    });
  </script>

{/block}
