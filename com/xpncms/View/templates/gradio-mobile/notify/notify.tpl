{block name="footerjs" append}
  <script>
    window.onload = function () {
      if (window.$) {
        {if isset($notify) && !empty($notify) }
          //Notify('{$notify.code}', '{$notify.type}', '{$notify.title}', '{$notify.alert}', '{$notify.salert}');
          xpnCMSNotify.Notify({$notify|json_encode});
          //Logger.info({$notify|json_encode});
          //console.debug('{$notify}');
        {/if}
        {if isset($simpleNotify) && !empty($simpleNotify) }
          xpnCMSNotify.simpleNotify({$simpleNotify|json_encode});
        {/if}
        //notify js template
      }
    }
  </script>

{/block}
