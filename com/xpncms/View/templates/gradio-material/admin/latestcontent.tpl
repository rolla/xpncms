<h3>{t}Latest content{/t}</h3>
<a href="{$url}admin/addcontent" class="btn btn-default btn-sm btn-responsive" role="button">
  <span class="glyphicon glyphicon-pencil"></span><br> {t}Add{/t}
</a>

{if isset($contents) }
  {foreach $contents as $content name=contents}
    {if $smarty.foreach.contents.index == $count}
      {break}
    {/if}
    <div class="news-blocks">
        <h3>
          {$content.cont_name}
        </h3>
        <div class="news-block-tags">
            <strong><a href="{$url}category/{$content.cat_alias}">{$content.cont_category}</a></strong>
            <em>{$content.cont_added|relative_date}</em>
        </div>
        <p>
            <img
              class="news-block-img pull-right col-xs-6"
              alt=""
              src="{$phpThumbUrl}?q=80&w=400&h=400&zc=1&src=/{$paths.images}/{$content.cont_imgs}"
              onerror="this.onerror=null;this.src='https://placeimg.com/300/300/any';">
            {$content.cont_content|strip_tags|truncate:400}
        </p>
        <a href="{$url}content/{$content.cont_alias}" class="news-block-btn btn btn-default" target="_blank">
          {t}View{/t}
          <i class="fa fa-eye"></i>
        </a>
        <a href="{$url}admin/editcontent/{$content.cont_id}" class="news-block-btn btn btn-default">
          {t}Edit{/t}
          <i class="fa fa-pencil"></i>
        </a>
    </div>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
      <li class="divider"></li>
    </ul>
  {/foreach}
{/if}
