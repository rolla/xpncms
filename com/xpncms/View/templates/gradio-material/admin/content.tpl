{block name="body" append}
<h1 class="slim-text center">{$title}</h1>
<br />
<div class="row">
	<div class="col-md-2 col-sm-3 col-xs-6 ">
		<img src="{$userData.photo_url}" alt="{$userData.first_name} {$userData.last_name}" />
	</div>
	 <div class="col-md-5 col-sm-3 col-xs-6 ">
		<h2>{t}Hello{/t} {$userData.first_name} {$userData.last_name}</h2>
		
		
	</div>
	<div class="col-md-2 col-md-offset-3 col-sm-3 col-xs-6 ">
		{logout}
	</div>
</div>
<br /><br />
			<h3>{t}Content{/t}</h3>
		<div class="row">
			<div class="col-md-2 col-sm-3 col-xs-6">
				<a href="{$url}admin/addcontent" class="btn btn-primary ajax-nav "><i class="fa fa-file"></i> {t}Add{/t}</a>
			</div>

			<br /><br />
			<div class="clearfix"></div>
			<div class="content-table">
			<table class="table">
				<thead>
					<tr>
						<th>{t}ID{/t}</th>
						<th>{t}Name{/t}</th>
						<th>{t}Content{/t}</th>
						<th>{t}Slug{/t}</th>
						<th>{t}Added{/t}</th>
						<th>{t}Updated{/t}</th>
						<th>{t}Content class{/t}</th>
						<th>{t}Category{/t}</th>
						<th>{t}Images{/t}</th>
						<th>{t}Order{/t}</th>
						<th>{t}Options{/t}</th>
					</tr>
				</thead>
				<tbody>
		
		{if isset($contents) }
			{foreach $contents as $content}
			
					<tr>
					<td class="cont-id"><div><a href="{$url}admin/content/{$content.cont_alias}" class="ajax-nav">{$content.cont_id}</a></div></td>
					<td class="cont-name"><div>{$content.cont_name}</div></td>
					<td class="cont-content"><div>{$content.cont_content}</div></td>
					<td class="cont-alias"><div>{$content.cont_alias}</div></td>
					<td class="cont-added"><div>{$content.cont_added|relative_date}</div></td>
					<td class="cont-updated"><div>{$content.cont_updated|relative_date}</div></td>
					<td class="cont-class"><div>{$content.cont_class}</div></td>
					<td class="cont-category"><div>{$content.cont_category}</div></td>
					<td class="cont-imgs"><div><img src="{$phpThumbUrl}?q=80&w=100&h=100&zc=1&src=../../{$paths.images}/{$content.cont_imgs}"/></div></td>
					<td class="cont-ordere"><div>{$content.cont_order}</div></td>
					<td class='cont-editing-opt'><div>
						<a href="{$url}admin/editcontent/{$content.cont_alias}" class="btn btn-block ajax-nav"><i class="fa fa-edit "></i> {t}Edit{/t}</a>
						<a href="#" data-href="{$url}admin/deletecontent/content/{$content.cont_id}" class="btn btn-block adm-del" data-confirm="Vai tiešām vēlies dzēst? saturu ar ID: {$content.cont_id} un Nosaukumu: {$content.cont_name}"><i class="fa fa-trash-o"></i> {t}Delete{/t}</a>
					</div>
					</td>
					
					</tr>
				<div class="clearfix"></div>
				
			{/foreach}
		{else}
			<tr>
				<td class="sat-cont-not-found" colspan="11"><h1 class="alert alert-danger">{t}Content not found{/t}</h1></td>
			</tr>
		{/if}
			</tbody>
			</table>
			</div>


		
		</div>
{/block}