<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="{$meta.contenttype}" />
	<meta http-equiv="content-style-type" content="{$meta.contentstyletype}" />
	<meta http-equiv="content-language" content="{$meta.contentlanguage}" />
	<title>{block name="title"}{$title}{/block}:{t}{$pageTitle}{/t}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="description" content="{$meta.content}" />
	<meta name="keywords" content="{$meta.keywords}" />
	<meta name="copyright" content="{$meta.copyright}" />
	<meta name="author" content="{$meta.author}" />
	<meta name="robots" content="noindex, nofollow" />
	<meta name="abstract" content="{$meta.abstract}" />
	<meta name="distribution" content="{$meta.distribution}" />
	<meta name="web_author" content="{$meta.webauthor}" />
	<meta name="google-site-verification" content="{$meta.googlesitev}" />

	<!-- facebook -->
	<meta property="fb:app_id" content="{$meta.fbappid}" />
	<meta property="fb:admins" content="{$meta.fbadmins}" />
	<meta property="og:url" content="{$meta.ogurl}" />
	<meta property="og:image" content="{$meta.ogimage}" />
	<meta property="og:type" content="{$meta.ogtype}" />
	<meta property="og:title" content="{$meta.ogtitle}" />
	<!-- /facebook -->

	<!-- twitter -->
	<meta name="twitter:card" content="{$meta.twittercard}">
	<meta name="twitter:site" content="{$meta.twittersite}">
	<meta name="twitter:creator" content="{$meta.twittercreator}">
	<meta name="twitter:title" content="{$meta.twittertitle}">
	<meta name="twitter:description" content="{$meta.twitterdescription}">
	{block name="twitter_card_extend"}{/block}
	<!-- /twitter -->

	{* <base href="{$url}"> *}

	<!-- *FAV-ICONS* -->
	<link rel="icon" href="{$url}img/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{$url}img/favicon.ico" type="image/xicon" />

	<!-- css stili -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300&subset=latin,latin-ext&display=swap">
	<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.10.3/themes/trontastic/jquery-ui.css">

	<link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap-theme.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap-social/bootstrap-social.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/fontawesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/perfect-scrollbar/src/perfect-scrollbar.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/normalize.css"/>

	<link rel="stylesheet" type="text/css" href="{$url}css/default/debug.css"/>

	<!--
	<link rel="stylesheet" href="{$url}css/supersized.css" type="text/css" media="screen,projection"  />
	<link rel="stylesheet" href="{$url}css/supersized.shutter.css" type="text/css" media="screen,projection"  />
	-->
	<!-- VerticalIconMenu -->

	<!--<link href="{$url}css/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />-->
	<!-- Core -->

	</head>

{block name="body"}
<body>
	<div class="container-fluid">
		{$debugconsole}
	</div>

{/block}


{block name="body" append}

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script>
		//var xpnCMS;
		var baseURL = "{$url}";
		{if isset($userData.id)}
		var userID = {$userData.id};
		{else}
		var userID = "guest";
		{/if}

		var appTitle = "{$title}";
		var now;
		var Notify, simpleNotify, confirmNotify;
		var core_do_u_wanna_reload = "{t}Do you wanna reload?{/t}";
		var core_error_loading_data= "{t}Error loading data{/t}";
		var core_please_confirm = "{t}Please confirm{/t}";

	</script>

	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

	<script type="text/javascript" src="{$url}assets/jquery/jquery-migrate.js"></script>
	<script type="text/javascript" src="{$url}assets/js-logger/src/logger.js"></script>
	<script type="text/javascript" src="{$url}assets/extend.js/src/extend.js"></script>

	<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript" src="{$url}assets/perfect-scrollbar/min/perfect-scrollbar-0.4.10.with-mousewheel.min.js"></script>
	<script type="text/javascript">


		$(function($){

			$('.debug-console-smarty-inner').perfectScrollbar();
			$('.debug-console-config-inner').perfectScrollbar();
			$('.debug-console-session-inner').perfectScrollbar();
			$('.debug-console-cookies-inner').perfectScrollbar();
			$('.debug-console-javascript-inner').perfectScrollbar();

		});

	</script>

	<script type="text/javascript" src="{$url}assets/bootstrap/dist/js/bootstrap.js"></script>

	{if isset($ajax) && $ajax == false}
	<script type="text/javascript">
		//xpnCMS.loadPage("{$pageId}");
	</script>
	{/if}

</body>
{/block}
</html>
