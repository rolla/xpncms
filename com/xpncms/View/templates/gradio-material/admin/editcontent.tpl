{block name="body" append}

{function name=menu selected=0 level=0}
  {foreach $data as $entry}
    {if $entry.name == $selected}
      <option value="{$entry.id}" selected="selected" data-slug="{$entry.slug}">{str_repeat("--", $level)} {$entry.name}</option>
    {else}
      <option value="{$entry.id}" data-slug="{$entry.slug}">{str_repeat("--", $level)} {$entry.name}</option>
    {/if}
    {if is_array($entry.sub_category)}
      {menu data=$entry.sub_category selected=$selected level=$level+1}
    {else}
    {/if}
  {/foreach}
{/function}

<section class="content-header">
<h1>{$title} {t}Admin panel{/t}</h1>
<ol class="breadcrumb">
  <li>
  <a href="{$url}admin">
    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
    {t}Home{/t}
  </a>
  </li>
  <li>
    <a href="{$url}admin/content">{t}Content{/t}</a>
  </li>
  <li class="active"><a href="{$url}admin/editcontent/{$content.cont_id}">{t}Edit{/t}</a></li>
</ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-3">
      {include './latestcontent.tpl' count=7}
    </div>
    <div class="col-md-6">
      <h3>{t}Edit content{/t}</h3>
      <div class="row">
      {if !count($content) }
        <h1 class="alert alert-danger">{t}Content not found{/t}</h1>
      {else}
        <fieldset>
          <form
            id="content-form"
            action="{$url}admin/updatecontent/"
            method="post"
            class="form-horizontal"
            data-remote="true"
            data-type="json"
            accept-charset="UTF-8"
            role="form">
            <input type="hidden" name="cont_id" value="{$content.cont_id}">

            <a href="{$url}admin/" class="btn btn-danger ajax-nav">
              <i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
            </a>
            <button type="submit" class="btn btn-success adm-save">
              <i class="fa fa-save"></i> {t}Save{/t}
            </button>

            <br /><br />
            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_id">{t}ID{/t}</label>
              <div class="col-xs-10">
                <input class="form-control" type="text" value="{$content.cont_id}" disabled="disabled" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_name">{t}Name{/t}</label>
              <div class="col-xs-10">
                <input class="form-control" type="text" name="cont_name" id="cont_name" value="{$content.cont_name}" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_alias">{t}Slug{/t}</label>
              <div class="col-xs-8">
                <input
                  class="form-control disabled"
                  type="text"
                  name="cont_alias"
                  id="cont_alias"
                  value="{$content.cont_alias}"
                  disabled>
              </div>
              <div class="col-xs-1">
                <div class="checkbox">
                  <label><input type="checkbox" class="alias-lock" value="1" checked>{t}Lock{/t}</label>
		            </div>
             </div>
            </div>
            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_category">{t}Category{/t}</label>
              <div class="col-xs-10">
                <select class="form-control" name="cont_category">
                  <option value="0">Parent</option>
                {if isset($categories) }
                  {* run the array through the function *}
                  {menu data=$categories selected=$content.cont_category}
                </select>
                {else}
                {/if}
              </div>
            </div>
            <div class="form-group">
              <textarea id="saturs" class="ckeditor" name="cont_content" rows="15" cols="100" style="width: 100%">{$content.cont_content}</textarea>
            </div>
            <br>
            <div class="form-group">
              <label class="col-xs-2 control-label" for="inputbild">{t}Images{/t}</label>
              <div class="col-xs-10">
                <img
                  class="cont_imgs_add"
                  src="{$url}{$paths.images}/{$content.cont_imgs}"
                  onerror="this.onerror=null;this.src='https://placeimg.com/200/200/any';">
                <input class="form-control cont_imgs_added" type="hidden" name="cont_imgs" id="cont_imgs" value="{$content.cont_imgs}" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_tags">{t}Tags{/t}</label>
              <div class="col-xs-10">
                <input class="form-control" type="text" name="cont_tags" id="cont_tags" value="{$content.cont_tags}" data-role="tagsinput" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_order">{t}Order{/t}</label>
              <div class="col-xs-10">
                <input class="form-control" type="text" name="cont_order" id="cont_order" value="{$content.cont_order}" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-xs-2 control-label" for="cont_added">{t}Added{/t}</label>
              <div class="col-xs-10">
                <input class="form-control" type="text" value="{$content.cont_added}" disabled="disabled" />
              </div>
            </div>

            <a href="{$url}admin/content" class="btn btn-danger ajax-nav">
              <i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
            </a>
            <button type="submit" class="btn btn-success adm-save">
              <i class="fa fa-save"></i> {t}Save{/t}
            </button>
          </form>
        </fieldset>
      {/if}
      </div>
    </div>
  </div>
</section>
{/block}

{block name="global-footer-js" prepend}
  <script type="text/javascript" src="{$url}assets/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="{$url}assets/ckeditor/adapters/jquery.js"></script>
{/block}

{block name="page-footer-js" append}
  <script type="text/javascript" src="{$url}assets/elfinder/js/elfinder.full.js"></script>
  <script src="{$url}{$paths.themes}/{$backendTheme}/vendors/tags/dist/bootstrap-tagsinput.js"></script>
  <script type="text/javascript" src="{$url}assets/simple-ajax-uploader/SimpleAjaxUploader.js"></script>

  {literal}

    <script>
      var currentCatSlug = 'upload';
      var uploader;
      function initTuneEdit()
      {
        var tuneCoversURL = $('.tuneCoversURL').val();
        var relativeUploaded = $('.relativeUploaded').val();
        //var album_art = $('#edit-tune .tunePicture').val();
        //$('.uploaded-image').attr('src', album_art);
        var btn = $('#uploadBtn'),
        progressBar = $('#progressBar'),
        progressOuter = $('#progressOuter');
        uploader = new ss.SimpleUpload({
          dropzone: 'dragbox', // ID of element to be the drop zone
          customHeaders: {type: 'content', category: getCurrentCatSlug()},
          button: btn,
          url: baseURL + 'admin/imageupload/',
          name: 'adminImageUpload',
          multipart: true,
          hoverClass: 'hover',
          focusClass: 'focus',
          responseType: 'json',
          startXHR: function() {
            progressOuter.show() // make progress bar visible
            this.setProgressBar(progressBar);
          },
          onSubmit: function() {
            btn.html('Uploading...'); // change button text to "Uploading..."
          },
          onComplete: function(filename, response) {
            console.log(filename);
            console.log(response);

            if (!response) {
              var promises = [];
              promises.push(gettext('Ouch'));
              promises.push(gettext('Unable to upload file'));
              promises.push(gettext('Choose Another File'));
              //console.log(promises);

              $.when.apply($, promises)
                      .done(function() {

                          xpnCMSNotify.simpleNotify({title: promises[0].responseText, 'type': 'error', alert: promises[1].responseText});
                          btn.html(promises[2].responseText); // change button text to "Uploading..."
                      })
                      .fail(function() {console.log('something fail');});

              progressOuter.hide(); // hide progress bar when upload is completed
              return;
            }
            /*
            if ( response.success === true ) {
              $('.uploaded-image').attr('src', tuneCoversURL + relativeUploaded + response.filename);
              $('#edit-tune .album_art').val(relativeUploaded + response.filename);
              xpnCMSNotify.simpleNotify({title: gettext('Yayy'), 'type': 'success', alert: gettext(':filename successfully uploaded.', {filename: response.filename})});
            } else {
              if (response.msg)  {
                xpnCMSNotify.simpleNotify({title: gettext('Something happen'), 'type': 'warning', alert: response.msg});
              } else {
                xpnCMSNotify.simpleNotify({title: gettext('Ouchh'), 'type': 'error', alert: gettext('An error occurred and the upload failed.')});
              }
            }
            */
          },
          onError: function() {
            progressOuter.hide();
            xpnCMSNotify.simpleNotify({title: gettext('Ouch'), 'type': 'error', alert: gettext('Unable to upload file')});
          }
        });
      }

      initTuneEdit();

      $(document).on('change', 'select[name=cont_category]', function() {
        var selectedSlug = $(this).find(':selected').data('slug');
        currentCatSlug = selectedSlug;

        uploader._opts.customHeaders.category = currentCatSlug;
      });

      function getCurrentCatSlug(){
          return currentCatSlug;
      }

    </script>
    {/literal}

{/block}
