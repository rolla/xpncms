import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/es5/util/colors';
import { preset } from 'vue-cli-plugin-vuetify-preset-rally/preset'

Vue.use(Vuetify);

const opts = {
  preset,
  theme: {
    dark: false,
    themes: {
      light: {
        primary: colors.blue.darken3,
        secondary: colors.blue.lighten5,
        accent: colors.deepPurple.base,
        error: colors.red.base,
        warning: colors.orange.base,
        info: colors.lightBlue.base,
        success: colors.green.base
      },
    }
  },
  icons: {
    iconfont: 'mdi',
  },
};

export default new Vuetify(opts)
