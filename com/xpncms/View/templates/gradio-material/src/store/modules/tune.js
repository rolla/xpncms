import axios from 'axios';
import { get, pick, take } from 'lodash';
import Vue from 'vue';

const state = {
  currentTune: undefined,
  previousTune: undefined,
  selectedTune: undefined,
  latestTunes: [],
  recentTunes: [],
  mostPlayedTunes: [],
  randomTunes: [],
  likedTunes: [],
  dislikedTunes: [],
  isSavingTune: false,
  tuneEditCoverRemoteFile: undefined,
  resetStackSize: 3, // reset stacks when visit home page
};

const executeOnIndex = (collection, tune, callback) => {
  const foundIndex = collection.findIndex(foundTune => foundTune.ID === tune.ID);
  if (foundIndex !== -1) {
    callback(foundIndex)
  }
}

const getters = {
  getCurrentTune: state => state.currentTune,

  getPreviousTune: state => state.previousTune,

  getSelectedTune: state => state.selectedTune,

  getLatestTunes: state => state.latestTunes,

  getRecentTunes: state => state.recentTunes,

  getMostPlayedTunes: state => state.mostPlayedTunes,

  getRandomTunes: state => state.randomTunes,

  getLikedTunes: state => state.likedTunes,

  getDislikedTunes: state => state.dislikedTunes,

  getIsSavingTune: state => state.isSavingTune,

  getTuneEditCoverRemoteFile: state => state.tuneEditCoverRemoteFile,
};

const actions = {
  async fetchTune({ rootGetters }, tuneId) {
    const response = await axios.get(`${rootGetters.apiUrl}tune/${tuneId}`);

    const tune = get(response, 'data.results.tune', undefined);
    const tuneMeta = get(response, 'data.results.meta', undefined);
    if (tuneMeta) {
      tune.meta = tuneMeta;
    }

    return tune;
  },

  async updateTune({getters, commit, dispatch, rootGetters}, tune) {
    if (get(getters, 'getCurrentTune.ID', 0) === tune.ID) {
      commit('UPDATE_CURRENT_TUNE', tune);
    }

    if (get(getters, 'getPreviousTune.ID', 0) === tune.ID) {
      commit('UPDATE_PREVIOUS_TUNE', tune);
    }

    if (get(getters, 'getSelectedTune.ID', 0) === tune.ID) {
      dispatch('updateSelectedTune', tune);
    }

    await dispatch('updateAllTuneArrays', tune);

    const activityLastId = rootGetters['user/getActivityLastId'];
    /* we need to fetch fresh only if we have before loaded activities */
    if (activityLastId) {
      const freshactivities = await dispatch(
        'user/fetchFreshActivities',
        { lastId: activityLastId },
        { root: true }
      );
      await commit(
        'user/UNSHIFT_USER_ACTIVITIES',
        get(freshactivities, 'human'),
        { root: true }
      );
      await commit(
        'user/UPDATE_USER_ACTIVITY_LAST_ID',
        get(freshactivities, 'human[0].id'),
        { root: true }
      );
    }
  },

  async updateCurrentTune({state, commit, rootGetters}) {
    const response = await axios.get(`${rootGetters.baseUrl}gradio/now`);
    const tune = get(response, 'data.now', undefined);
    if (tune) {
      commit('UPDATE_CURRENT_TUNE', tune);
    }

    if (get(state, 'previousTune.ID', undefined) !== tune.ID) {
      commit('UNSHIFT_RECENT_TUNES', tune);
      commit('UPDATE_PREVIOUS_TUNE', pick(tune, ['ID', 'album_art']));
    }

    return tune;
  },

  updateSelectedTune({ commit }, tune) {
    commit('UPDATE_SELECTED_TUNE', tune);
    commit('UPDATE_TUNE_EDIT_COVER_REMOTE_FILE', undefined);
  },

  async reloadSelectedTune({commit, getters, dispatch}) {
    const tune = await dispatch('fetchTune', getters.getSelectedTune.ID);
    commit('UPDATE_SELECTED_TUNE', tune);

    dispatch('updateTune', tune);

    return tune;
  },

  async fetchLatestTunes({rootGetters}, params) {
    const response = await axios.get(`${rootGetters.apiUrl}tunes`,
      {
        params: params
      });

    return response.data.results.tunes;
  },

  async fetchRecentTunes({rootGetters}, params) {
    const response = await axios.get(`${rootGetters.apiUrl}tunes/recent`,
      {
        params: params
      });

    return response.data.results.recentTunes;
  },

  async fetchMostPlayedTunes({rootGetters}, params) {
    const response = await axios.get(`${rootGetters.apiUrl}tunes/most`,
      {
        params: params
      });

    return response.data.results.mostPlayedTunes;
  },

  async fetchRandomTunes({rootGetters}, params) {
    const response = await axios.get(`${rootGetters.apiUrl}tunes`,
      {
        params: {...{random: true, limit: 6}, ...params}
      });

    return response.data.results.tunes;
  },

  async fetchLikedTunes({rootGetters}, params) {
    const response = await axios.get(`${rootGetters.apiUrl}tunes/liked`,
      {
        params: params
      });

    return response.data.results.likedTunes;
  },

  async fetchDislikedTunes({rootGetters}, params) {
    const response = await axios.get(`${rootGetters.apiUrl}tunes/disliked`,
      {
        params: params
      });

    return response.data.results.dislikedTunes;
  },

  async saveTune ({commit, rootGetters, dispatch}, formData) {
    commit('UPDATE_IS_SAVING_TUNE', true);
    try {
      const response = await axios.post(`${rootGetters.apiUrl}tunesave`, formData);
      const tune = await dispatch('fetchTune', formData.get('songs[ID]'));
      if (tune) {
        dispatch('updateTune', tune);
        dispatch('tuneUpdatedEvent', {tuneId: tune.ID, formData : formData})
      }

      return response.data;
    } catch (error) {
      console.error(error);
    } finally {
      commit('UPDATE_IS_SAVING_TUNE', false);
    }
  },

  /* update all tune arrays with updated tune object */
  updateAllTuneArrays ({state, commit}, tune) {
    executeOnIndex(
      state.latestTunes,
      tune,
      (foundIndex) => commit('UPDATE_LATEST_TUNE', {foundIndex, tune})
    );
    executeOnIndex(
      state.randomTunes,
      tune,
      (foundIndex) => commit('UPDATE_RANDOM_TUNE', {foundIndex, tune})
    );
    executeOnIndex(
      state.recentTunes,
      tune,
      (foundIndex) => commit('UPDATE_RECENT_TUNE', {foundIndex, tune})
    );
    executeOnIndex(
      state.mostPlayedTunes,
      tune,
      (foundIndex) => commit('UPDATE_MOST_PLAYED_TUNE', {foundIndex, tune})
    );
    executeOnIndex(
      state.likedTunes,
      tune,
      (foundIndex) => commit('UPDATE_LIKED_TUNE', {foundIndex, tune})
    );
    executeOnIndex(
      state.dislikedTunes,
      tune,
      (foundIndex) => commit('UPDATE_DISLIKED_TUNE', {foundIndex, tune})
    );
  },

  /**
   * send put request to utils api
   * after utils broadcast event to reload tune for all connected clients via websocket
   * @param rootGetters
   * @param payload
   * @return {Promise<AxiosResponse<any>>}
   */
  async tuneUpdatedEvent({rootGetters}, payload ) {
    const user = rootGetters['user/getUser'];
    // if (user?.api_token) {
    //   return await axios.put(
    //     `${rootGetters.utilsUrl}api/tunes/${payload.tuneId}/from-v4`,
    //       payload.formData,
    //     {
    //       withCredentials: true,
    //       headers: {
    //         Authorization: `Bearer ${user.api_token}`
    //       }
    //     });
    // }

    const payloadToSend = Object.fromEntries(payload.formData)

    return await axios.put(
        `${rootGetters.utilsUrl}api/guest/tunes/${payload.tuneId}/from-v4`,
        { tune: payloadToSend, user: user }
    );
  }
};

// mutations
const mutations = {
  UPDATE_CURRENT_TUNE(state, tune) {
    state.currentTune = tune;
  },

  UPDATE_PREVIOUS_TUNE(state, tune) {
    state.previousTune = tune;
  },

  UPDATE_SELECTED_TUNE(state, tune) {
    state.selectedTune = tune;
  },

  UPDATE_LATEST_TUNES(state, tunes) {
    state.latestTunes.push(...tunes);
  },

  UPDATE_LATEST_TUNE(state, {foundIndex, tune}) {
    Vue.set(state.latestTunes, foundIndex, tune);
  },

  UPDATE_RANDOM_TUNES(state, tunes) {
    state.randomTunes = tunes;
  },

  UPDATE_RANDOM_TUNE(state, {foundIndex, tune}) {
    Vue.set(state.randomTunes, foundIndex, tune);
  },

  UPDATE_RECENT_TUNES(state, tunes) {
    state.recentTunes.push(...tunes);
  },

  UPDATE_RECENT_TUNE(state, {foundIndex, tune}) {
    Vue.set(state.recentTunes, foundIndex, tune);
  },

  UPDATE_MOST_PLAYED_TUNES(state, tunes) {
    state.mostPlayedTunes.push(...tunes);
  },

  UPDATE_MOST_PLAYED_TUNE(state, {foundIndex, tune}) {
    Vue.set(state.mostPlayedTunes, foundIndex, tune);
  },

  UNSHIFT_RECENT_TUNES(state, tune) {
    /* prevent unshift if first element is with the same ID */
    /* this can happen if another endpoint loaded state.recentTunes */
    if (get(state.recentTunes, '[0].ID') !== tune.ID) {
      state.recentTunes.unshift(tune);
    }
  },

  UPDATE_LIKED_TUNES(state, tunes) {
    state.likedTunes.push(...tunes);
  },

  UPDATE_LIKED_TUNE(state, {foundIndex, tune}) {
    Vue.set(state.likedTunes, foundIndex, tune);
  },

  UPDATE_DISLIKED_TUNES(state, tunes) {
    state.dislikedTunes.push(...tunes);
  },

  UPDATE_DISLIKED_TUNE(state, {foundIndex, tune}) {
    Vue.set(state.dislikedTunes, foundIndex, tune);
  },

  UPDATE_IS_SAVING_TUNE(state, isSavingTune) {
    state.isSavingTune = isSavingTune;
  },

  UPDATE_TUNE_EDIT_COVER_REMOTE_FILE(state, tuneEditCoverRemoteFile) {
    state.tuneEditCoverRemoteFile = tuneEditCoverRemoteFile;
  },

  RESET_TUNE_STACKS(state) {
    state.recentTunes = take(state.recentTunes, state.resetStackSize);
    state.latestTunes = take(state.latestTunes, state.resetStackSize);
    state.mostPlayedTunes = take(state.mostPlayedTunes, state.resetStackSize);
    state.randomTunes = take(state.randomTunes, state.resetStackSize);
    state.likedTunes = take(state.likedTunes, state.resetStackSize);
    state.dislikedTunes = take(state.dislikedTunes, state.resetStackSize);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
