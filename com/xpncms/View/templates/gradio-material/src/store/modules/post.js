
import axios from 'axios';

// initial state
const state = {
  post: undefined,
  postIsLoading: true,
  posts: undefined,
  postsIsLoading: true,
  relatedPosts: undefined,
  relatedPostsIsLoading: true,
};

// getters
const getters = {
  getPostIsLoading(state) {
    return state.postIsLoading;
  },

  getPost(state) {
    return state.post;
  },

  getPostsIsLoading(state) {
    return state.postsIsLoading;
  },

  getPosts(state) {
    return state.posts;
  },

  getRelatedPost(state) {
    return state.relatedPosts;
  },

  getRelatedPostIsLoading(state) {
    return state.relatedPostsIsLoading;
  },
};

// actions
const actions = {
  async fetchPost({commit, rootGetters}, slug) {
    // commit("UPDATE_POST", undefined);
    commit("UPDATE_POST_IS_LOADING", true);
    const response = await axios.get(`${rootGetters.apiUrl}post/${slug}`);
    commit("UPDATE_POST_IS_LOADING", false);

    return commit("UPDATE_POST", response.data.results.post);
  },

  async fetchPosts({commit, rootGetters}, params) {
    commit("UPDATE_POSTS", undefined);
    commit("UPDATE_POSTS_IS_LOADING", true);
    const response = await axios.get(`${rootGetters.apiUrl}posts`,
      {
        params: params
      }
    );
    commit("UPDATE_POSTS_IS_LOADING", false);

    return commit("UPDATE_POSTS", response.data.results.posts);
  },

  async fetchContents({commit, rootGetters}, params) {
    // console.log('fetchContents', params);
    // commit("UPDATE_RELATED_POSTS", undefined);
    commit("UPDATE_RELATED_POSTS_IS_LOADING", true);

    const response = await axios.get(`${rootGetters.apiUrl}contents/${params.slug}`,
      {
        params: params
      }
    );
    commit("UPDATE_RELATED_POSTS_IS_LOADING", false);

    return commit("UPDATE_RELATED_POSTS", response.data.results.contents);
  },
};

// mutations
const mutations = {
  UPDATE_POST(state, post) {
    state.post = post;
  },

  UPDATE_POST_IS_LOADING(state, postIsLoading) {
    state.postIsLoading = postIsLoading;
  },

  UPDATE_POSTS(state, posts) {
    state.posts = posts;
  },

  UPDATE_POSTS_IS_LOADING(state, postsIsLoading) {
    state.postsIsLoading = postsIsLoading;
  },

  UPDATE_RELATED_POSTS_IS_LOADING(state, relatedPostsIsLoading) {
    state.relatedPostsIsLoading = relatedPostsIsLoading;
  },

  UPDATE_RELATED_POSTS(state, relatedPosts) {
    state.relatedPosts = relatedPosts;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
