
import axios from 'axios';

// initial state
const state = {
  category: undefined,
  categories: undefined,
  meta: undefined,
};

// getters
const getters = {
  getCategory(state) {
    return state.category;
  },

  getCategories(state) {
    return state.categories;
  },

  getCategoriesMeta(state) {
    return state.meta;
  },
};

// actions
const actions = {
  async fetchCategory({commit, rootGetters}, slug) {
    // console.log('fetchCategory', slug);
    const response = await axios.get(`${rootGetters.apiUrl}category/${slug}`);

    return commit("UPDATE_CATEGORY", response.data.results.category);
  },

  async fetchCategories({commit, rootGetters}) {
    const response = await axios.get(`${rootGetters.apiUrl}categories`);

    commit("UPDATE_CATEGORIES_META", response.data.results.meta);

    return commit("UPDATE_CATEGORIES", response.data.results.categories);
  },
};

// mutations
const mutations = {
  UPDATE_CATEGORY(state, category) {
    state.category = category;
  },

  UPDATE_CATEGORIES(state, categories) {
    state.categories = categories;
  },

  UPDATE_CATEGORIES_META(state, meta) {
    state.meta = meta;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
