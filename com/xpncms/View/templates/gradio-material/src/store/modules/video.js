
import axios from 'axios';
import {pick, get} from 'lodash';

// initial state
const state = {
  latestVideos: [],
  randomVideos: [],
  powerVideo: undefined,
}

// getters
const getters = {
  getLatestVideos(state) {
    return state.latestVideos;
  },
  getRandomVideos(state) {
    return state.randomVideos;
  },
  getPowerVideo(state) {
    return state.powerVideo;
  },
}

// actions
const actions = {
  async fetchLatestVideos({commit, rootGetters}) {
    const response = await axios.get(`${rootGetters.apiUrl}videos`);

      return commit("UPDATE_LATEST_VIDEOS", response.data.results.videos);
  },
  async fetchRandomVideos({commit, rootGetters}) {
    const response = await axios.get(`${rootGetters.apiUrl}videos`,
      {
        params: {
          random: true,
        }
      })

    return commit("UPDATE_RANDOM_VIDEOS", response.data.results.videos);
  },
  async fetchPowerVideo({commit, rootGetters}) {
    const response = await axios.get(`${rootGetters.apiUrl}video/power`,
      {
        params: {
          random: true,
        }
      });

    return commit("UPDATE_POWER_VIDEO", response.data.results.video);
  },
}

// mutations
const mutations = {
  UPDATE_LATEST_VIDEOS(state, videos) {
    state.latestVideos = videos;
  },
  UPDATE_RANDOM_VIDEOS(state, videos) {
    state.randomVideos = videos;
  },
  UPDATE_POWER_VIDEO(state, video) {
    state.powerVideo = video;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
