
import axios from 'axios';
import {USER_ROLE} from '../../enums/userEnums'
import {HelperMixin} from '../../mixins/helper/helperMixin'

// initial state
const state = {
  user: undefined,
  activities: [],
  activityLastId: undefined,
}

// getters
const getters = {
  /**
   * @param state
   * @return {{image:string,display_name:string,api_token:?string}}
   */
  getUser(state) {
    return state.user;
  },

  isLoggedIn(state) {
    return state.user && state.user.exists;
  },

  isMainAdmin(state) {
    return HelperMixin.methods.getOr(state.user, 'roles', [])
      .some(role => Number(role.ID) === USER_ROLE.MAIN_ADMIN);
  },

  isAdmin(state) {
    return HelperMixin.methods.getOr(state.user, 'roles', [])
      .some(role => Number(role.ID) === USER_ROLE.ADMIN);
  },

  isGradioTeam(state) {
    return HelperMixin.methods.getOr(state.user, 'roles', [])
      .some(role => Number(role.ID) === USER_ROLE.GRADIO_TEAM);
  },

  getActivities(state) {
    return state.activities;
  },

  getActivityLastId(state) {
    return state.activityLastId;
  },
}

// actions
const actions = {
  async fetchUser({commit, rootGetters}) {
    const response = await axios.get(`${rootGetters.apiUrl}me`)

    return commit("UPDATE_USER", response.data.results.user);
  },

  async updateUser({commit, rootGetters}, params) {
      const response = await axios.post(`${rootGetters.apiUrl}meupdate`, params);

      return commit("UPDATE_USER", response.data.results.user);
  },

  async fetchActivities({rootGetters}, params) {
    // console.log('fetchActivities', params);

    const response = await axios.get(`${rootGetters.apiUrl}me/activities`,
      {
        params: params
      });

    return response.data.results.activities;
  },

  async fetchFreshActivities({rootGetters}, params) {
    // console.log('fetchActivities', params);

    const response = await axios.get(`${rootGetters.apiUrl}me/freshactivities`,
      {
        params: params
      });

    return response.data.results.activities;
  },
}

// mutations
const mutations = {
  UPDATE_USER(state, user) {
    state.user = user;
  },

  UPDATE_USER_ACTIVITIES(state, activities) {
    state.activities.push(...activities);
  },

  UPDATE_USER_ACTIVITY_LAST_ID(state, activityLastId) {
    state.activityLastId = activityLastId;
  },

  UNSHIFT_USER_ACTIVITIES(state, activities) {
    state.activities.unshift(...activities);
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
