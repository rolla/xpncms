
import Vue from 'vue';
import Vuex from 'vuex';
import tune from './modules/tune';
import video from './modules/video';
import user from './modules/user';
import post from './modules/post';
import category from './modules/category';
import {get} from 'lodash';

Vue.use(Vuex);

const getters = {
  baseUrl: state => {
    return state.base.url === '' ? '/' : state.base.url;
  },

  apiUrl: state => {
    if (state.base.apiUrl === '/') {
      state.base.apiUrl = '';
    }
    return state.base.apiUrl;
  },

  utilsUrl: state => state.gradioPrefs.utilsurl,

  loginDialog: state => state.showLoginDialog,

  phpThumbUrl: state => state.site.phpThumbUrl,

  imagePath: state => state.paths.images,

  frontendTheme: state => state.site.frontendTheme,

  frontendThemeImages: (state, getters) => {
    return `${getters.baseUrl}${state.paths.themes}/${getters.frontendTheme}/${getters.imagePath}/`;
  },

  defaultNotFoundImage: (state, getters) => {
    return `${getters.frontendThemeImages}default/not-found.png`;
  },

  siteSocial: state => state.site.social,

  fbSiteUrl: state => state.base.fbsiteurl,

  siteTitle: state => state.site.title,
};

const actions = {
  processNotifyError({commit}, notify) {
    const errorCode = get(notify, 'error', undefined);

    switch (errorCode) {
      case 10:
        commit('SHOW_LOGIN_DIALOG', true);
        break;
      case 11:
        console.log('errorCode', errorCode);
        break;
      default:
    }
  },
};

const mutations = {
  SHOW_LOGIN_DIALOG(state, showLoginDialog) {
    state.showLoginDialog = showLoginDialog;
  },
};

export const store = new Vuex.Store({
  //strict: process.env.NODE_ENV !== 'production',
  state: {
    base: base,
    site: site,
    paths: paths,
    gradioPrefs: gradioPrefs,
    debug: d,
    showLoginDialog: false,
  },
  mutations,
  actions,
  getters,
  modules: {
    category,
    post,
    tune,
    user,
    video,
  },
});
