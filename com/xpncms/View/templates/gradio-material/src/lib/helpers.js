import $ from 'jquery';
import PNotify from 'pnotify/dist/es/PNotify';

const consoleFunction = () => {
  if (!window.console) {
    window.console = {
      debug: () => {
      },
      error: () => {
      },
      info: () => {
      },
      log: () => {
      },
      warn: () => {
      },
    };
  }
};

const hotModuleDev = () => {
  if (module.hot) {
    module.hot.accept();
  }

  if (module.hot) {
    const hotEmitter = require('webpack/hot/emitter');
    const DEAD_CSS_TIMEOUT = 2000;

    hotEmitter.on('webpackHotUpdate', function (currentHash) {
      document.querySelectorAll('link[href][rel=stylesheet]')
        .forEach((link) => {
          const nextStyleHref = link.href.replace(/(\?\d+)?$/, `?${Date.now()}`);
          const newLink = link.cloneNode();
          newLink.href = nextStyleHref;

          link.parentNode.appendChild(newLink);
          setTimeout(() => {
            link.parentNode.removeChild(link);
          }, DEAD_CSS_TIMEOUT);
        });
    });
  }
};

const trackMatamo = () => {
  if (typeof _paq !== 'undefined') {
    _paq.push(['setDocumentTitle', document.title]);
    _paq.push(['setCustomUrl', document.location]);
    _paq.push(['trackPageView']);
  }
};

const trackEvent = (category, action, name = undefined, value = undefined) => {
  if (typeof _paq !== 'undefined') {
    _paq.push(['trackEvent', category, action, name, value]);
  }
};

const trackGoogleAnalytics = () => {
  if (typeof ga !== 'undefined') {
    ga('set', { page: decodeURI(document.location.pathname) });
    ga('send', 'pageview');
  }
};

const initSmoothScroll = () => {
  // https://css-tricks.com/snippets/jquery/smooth-scrolling/
  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $('html, body')
            .animate({
              scrollTop: target.offset().top
            }, 1000, function () {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(':focus')) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
            });
        }
      }
    });
};

function makeAjaxCall(url, data, methodType, callback) {
  return $.ajax({
    url: url,
    data: data,
    method: methodType,
    dataType: 'json'
  });
}

var modalNotifyObj = {};

const modalNotify = (notify, newProfilePicAvailable) => {
  modalNotifyObj =
    new PNotify({
      title: notify.title,
      text: notify.text,
      type: notify.type,
      icon: 'glyphicon glyphicon-question-sign',
      hide: false,
      confirm: {
        confirm: true
      },
      buttons: {
        closer: false,
        sticker: false
      },
      history: {
        history: false
      },
      addclass: 'stack-modal',
      stack: {
        'dir1': 'down',
        'dir2': 'right',
        'modal': true
      }
    }).get();

  modalNotifyObj.on('pnotify.confirm', () => {
    makeAjaxCall(
      `${store.getters.baseUrl}/user/setnotifyaboutnewprofilepic`,
      {
        choose: 'confirm',
        newProfilePicAvailable: newProfilePicAvailable
      },
      'POST'
    )
      .then(function (respJson) {
        location.reload();
      }, function (reason) {
        //console.log("error in processing your request", reason);
      });

  });

  modalNotifyObj.on('pnotify.cancel', () => {
    makeAjaxCall(
      `${store.getters.baseUrl}/user/setnotifyaboutnewprofilepic`,
      {
        choose: 'cancel',
        newProfilePicAvailable: newProfilePicAvailable
      },
      'POST'
    )
      .then(function (respJson) {

      }, function (reason) {
        //console.log("error in processing your request", reason);
      });
  });
};

const isAppleDevice = () => {
  const userAgent = window.navigator.userAgent;
  return (userAgent && (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)));
};

export {
  consoleFunction,
  hotModuleDev,
  trackMatamo,
  trackEvent,
  trackGoogleAnalytics,
  initSmoothScroll,
  modalNotify,
  isAppleDevice
};
