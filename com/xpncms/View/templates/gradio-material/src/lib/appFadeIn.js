
/* use https://github.com/matteobad/vanilla-fade */

let loaderElement = undefined;
let appElement = undefined;

function getElements() {
  loaderElement = document.getElementById('loader');
  appElement = document.getElementById('gradio-material-v2');
}

function fadeInApp() {
  appElement.fadeIn(300, 'linear', finishFade());
}

function fadeOutLoader() {
  loaderElement.fadeOut(300, 'linear', fadeInApp())
}

function finishFade () {
  loaderElement.style.display = 'none';
  appElement.style.display = 'block';
}

const appFadeIn = () => {
  setTimeout(() => {
    getElements();
    fadeOutLoader();
  }, 650);
}

export {appFadeIn};
