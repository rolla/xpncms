
import axios from 'axios';
import {isNil} from 'lodash';

// facebook
var fbLoginEvent = function (response) {

}

var fbLogoutEvent = function (response) {

}

var fbCommentCreateEvent = function (response) {

  var commentData = {
    commentID: response.commentID,
    message: response.message,
    href: response.href,
    userID: userID,
    userName: userName,
    type: 'info',
    icon: ':neckbeard:',
  };

  var formData = new FormData(); // Currently empty
  formData.append('message', `@${commentData.userName} posted FB comment`);
  formData.append('preText', `${currentTune.artist} - ${currentTune.title}`);
  formData.append('title', 'Comment');
  formData.append('title_link', `${commentData.href}?utm_campaign=slackcommentbot`);
  //formData.append('item', currentTune);
  formData.append('notifyAlert', commentData.message);
  formData.append('notifyType', commentData.type);
  formData.append('icon', commentData.icon);

  if (isNil(commentData.message)) {
    return;
  }
  axios.post('/action/slackhook', formData).then(function(response){

  }).catch(function (error) {
    console.error(error);
  });
}

var fbCommentRemoveEvent = function (response) {

}

var fbPageLikeEvent = function (url, html_element) {

}

var fbPageDislikeEvent = function (url, html_element) {

}

function facebookSubscribe() {
  window.FB.Event.subscribe('auth.login', fbLoginEvent);
  window.FB.Event.subscribe('auth.logout', fbLogoutEvent);

  window.FB.Event.subscribe('comment.create', fbCommentCreateEvent);
  window.FB.Event.subscribe('comment.remove', fbCommentRemoveEvent);

  window.FB.Event.subscribe('edge.create', fbPageLikeEvent);
  window.FB.Event.subscribe('edge.remove', fbPageDislikeEvent);
}

export function loadFacebook() {
  window.FB.init({
    appId: '548634151901705',
    autoLogAppEvents: true,
    xfbml: false,
    version: 'v14.0'
  });

  facebookSubscribe();
}

// end facebook
