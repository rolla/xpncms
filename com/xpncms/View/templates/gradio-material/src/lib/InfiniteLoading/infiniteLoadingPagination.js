class InfiniteLoadingPagination {

  constructor() {
    this.start = 0;
    this.limit = 4;
    this.items = [];
    this.loadedItems = [];
  }

  getStart() {
    return this.start;
  }

  setStart(start) {
    this.start = start;
  }

  getLimit() {
    return this.limit;
  }

  setLimit(limit) {
    this.limit = limit;
  }

  getItems() {
    return this.items;
  }

  setItems(items) {
    this.items.push(...items);
  }

  getLoadedItems() {
    return this.loadedItems;
  }

  setLoadedItems(items) {
    this.loadedItems = items;
    this.setItems(items);
    this.setStart(this.start += this.limit);
  }

  getStartAndLimit() {
    return {start: this.start, limit: this.limit};
  }
}

export {InfiniteLoadingPagination};
