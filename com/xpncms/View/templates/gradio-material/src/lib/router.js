
// scrollBehavior:
// - only available in html5 history mode
// - defaults to no scroll behavior
// - return false to prevent scroll
export const scrollBehavior = function (to, from, savedPosition) {

  // console.log('scrollBehavior to', to);
  // console.log('scrollBehavior from', from);

  if (savedPosition) {
    // Returning the savedPosition will result in a native-like behavior when navigating with back/forward buttons
    return savedPosition
  }

  if (to.hash) {
    return {
      selector: to.hash
    }
  }

  return {selector: '#navigation'}

  // const position = {}

  // scroll to anchor by returning the selector
  /*
  if (to.hash) {
    position.selector = to.hash

    // specify offset of the element
    if (to.hash === '#anchor2') {
      position.offset = { y: 100 }
    }

    if (document.querySelector(to.hash)) {
      return position
    }

    // if the returned position is falsy or an empty object,
    // will retain current scroll position.
    return false
  }
  */

  /*
  console.debug('to', to);

  if (to.hash === '') {
    console.debug('default anchor');
    //const position1 =  {selector: '#nav-fixed', offset: { y: 0 }};
    const position1 =  {selector: defaultTopElement};

    console.debug('position1', position1);

    return position1;

    //return false;
  }
  */

  /*
  return new Promise(resolve => {
    // check if any matched route config has meta that requires scrolling to top
    if (to.matched.some(m => m.meta.scrollToTop)) {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      position.x = 0
      position.y = 0
    }

    // wait for the out transition to complete (if necessary)
    this.app.$root.$once('triggerScroll', () => {
      // if the resolved position is falsy or an empty object,
      // will retain current scroll position.
      resolve(position)
    })
  })*/

}
