
import {store} from './store/store';

const Home = () => import('./pages/Home.vue');

const Tunes = () => import('./pages/Tunes/Tunes.vue');
const RecentTunes = () => import('./pages/Tunes/RecentTunes.vue');
const MostPlayedTunes = () => import('./pages/Tunes/MostPlayedTunes.vue');
const LatestTunes = () => import('./pages/Tunes/LatestTunes.vue');
const YourLikes = () => import('./pages/Tunes/YourLikes.vue');
const YourDislikes = () => import('./pages/Tunes/YourDislikes.vue');

const Tune = () => import('./pages/Tune.vue');
const Post = () => import('./pages/Content.vue');
const Categories = () => import('./pages/Categories.vue');
const Category = () => import('./pages/Category.vue');
const UserSettings = () => import('./pages/UserSettings.vue');
const TuneEdit = () => import('./pages/TuneEdit.vue');
const Privacy = () => import('./pages/Privacy.vue');
const DataDeletionInstruction = () => import('./pages/DataDeletionInstruction.vue');
const Me = () => import('./pages/Me.vue');
const Posts = () => import('./pages/Posts.vue');

/**
 * if not logged in user we fetch again
 * we need this when we full reload page, because then we dont have logged in user
 * @return {Promise<void>}
 */
async function checkAndFetchUser() {
  if (!store.getters['user/isLoggedIn']) {
    await store.dispatch('user/fetchUser');
  }
}

const routes = [
  { path: '/home', redirect: '/' },
  { path: '/', component: Home, props: true},
  { path: '/gradio/*', redirect: '/tunes' },

  { path: '/tunes', redirect: '/tunes/recent' },
  {
    path: '/tunes',
    component: Tunes,
    children: [
      { path: 'recent', name: 'recentTunes', component: RecentTunes },
      { path: 'most-played', name: 'mostPlayedTunes', component: MostPlayedTunes },
      { path: 'latest', name: 'latestTunes', component: LatestTunes },
      { path: 'your-likes', name: 'yourLikes', component: YourLikes },
      { path: 'your-dislikes', name: 'yourDislikes', component: YourDislikes },
    ]
  },

  {
    path: '/tune/:tuneId',
    component: Tune,
    name: 'tune',
  },

  {
    path: '/tune/:tuneId/edit',
    name: 'tuneEdit',
    component: TuneEdit,
    beforeEnter: async (to, from, next) => {
      await checkAndFetchUser();
      if (store.getters['user/isGradioTeam']) {
        next();
        return;
      }

      console.error('Route middleware 403 Forbidden');
      history.back();
    }
  },

  { path: '/content/:slug', component: Post },
  { path: '/categories', component: Categories },
  { path: '/category/:slug', component: Category },
  {
    path: '/user/settings',
    component: UserSettings,
    beforeEnter: async (to, from, next) => {
      await checkAndFetchUser();
      if (store.getters['user/isLoggedIn']) {
        next();
        return;
      }

      console.error('Route middleware 403 Forbidden');
      history.back();
    }
  },
  { path: '/user/*', redirect: '/' },
  { path: '/user/logout', component: Home },
  { path: '/privacy', component: Privacy },
  { path: '/datadeletioninstruction', component: DataDeletionInstruction },
  {
    path: '/me',
    component: Me,
    beforeEnter: async (to, from, next) => {
      await checkAndFetchUser();
      if (store.getters['user/isLoggedIn']) {
        next();
        return;
      }

      console.error('Route middleware 403 Forbidden');
      history.back();
    }
  },
  { path: '/posts', component: Posts },
];

export {routes};
