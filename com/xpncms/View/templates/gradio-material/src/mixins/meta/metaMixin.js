
import {HelperMixin as hm} from '../helper/helperMixin'

const MetaMixin = {
  data: () => ({
    name: 'MetaMixin',
    meta: undefined,
    pageTitle: '',
  }),

  head: {
    title () {
      return {
        inner: this.pageTitle,
        separator: ' : ',
        complement: this.$store.state.site.title,
      }
    },

    meta () {
      return [
        { property: 'og:url', content: hm.methods.getOr(this.meta, 'ogurl', '') },
        { property: 'og:title', content: hm.methods.getOr(this.meta, 'ogtitle', '') },
        { property: 'og:image', content: hm.methods.getOr(this.meta, 'ogimage', '') },
        { property: 'og:type', content: hm.methods.getOr(this.meta, 'ogtype', '') },
        // { property: 'music:duration', content: hm.methods.getOr(this.meta, 'musicduration', '') },
        { property: 'og:description', content: hm.methods.getOr(this.meta, 'ogdescription', '') },

        { name: 'twitter:card"', content: hm.methods.getOr(this.meta, 'twittercard', '') },
        { name: 'twitter:url', content: hm.methods.getOr(this.meta, 'twitterurl', '') },
        { name: 'twitter:title', content: hm.methods.getOr(this.meta, 'twittertitle', '') },
        { name: 'twitter:description', content: hm.methods.getOr(this.meta, 'twitterdescription', '') },
        { name: 'twitter:image', content: hm.methods.getOr(this.meta, 'twitterimage', '') },
        { name: 'twitter:site', content: hm.methods.getOr(this.meta, 'twittersite', '') },
        { name: 'twitter:creator', content: hm.methods.getOr(this.meta, 'twittercreator', '') },

        { itemprop: 'name', content: hm.methods.getOr(this.meta, 'name', '') },
        { itemprop: 'description', content: hm.methods.getOr(this.meta, 'description', '') },
        { itemprop: 'image', content: hm.methods.getOr(this.meta, 'image', '') },
      ]
    }
  },

  methods: {
    setMeta (meta) {
      this.meta = meta;
      this.setPageTitle(hm.methods.getOr(this.meta, 'pageTitle', ''));
    },

    setPageTitle (pageTitle) {
      this.pageTitle = pageTitle;
    }
  },
};

export {MetaMixin};
