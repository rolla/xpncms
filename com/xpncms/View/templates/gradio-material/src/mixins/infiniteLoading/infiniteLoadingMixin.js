
import {InfiniteLoadingPagination} from '../../lib/InfiniteLoading/infiniteLoadingPagination';

const InfiniteLoadingMixin = {
  data: () => ({
    name: 'InfiniteLoadingMixin',
    pagination: undefined,
  }),

  created () {
    this.pagination = new InfiniteLoadingPagination;
  },

  methods: {
    async infiniteHandler($state) {
      try {
        await this.load();
        if (this.pagination.getLoadedItems().length) {
          $state.loaded();
        } else {
          $state.complete();
        }
      } catch (error) {
        console.error(error);
        $state.error();
      }
    },
  }
};

export {InfiniteLoadingMixin};
