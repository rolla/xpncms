import {mapGetters} from 'vuex';

const TuneLoadMixin = {
  data: () => ({
    name: 'TuneLoadMixin',
  }),

  computed: {
    ...mapGetters([
      'baseUrl'
    ]),
  },

  methods: {
    tuneLink (tune) {
      return `${this.baseUrl}tune/${tune.id}`;
    },

    openTune(tune) {
      this.$store.dispatch('tune/updateSelectedTune', tune);
      this.$router.push({path: this.tuneLink(tune)});
    },
  }
};

export {TuneLoadMixin};
