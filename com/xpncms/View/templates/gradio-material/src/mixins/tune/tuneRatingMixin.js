
import {get} from 'lodash';
import moment from 'moment';

const TuneRatingMixin = {
  data: () => ({
    name: 'TuneRatingMixin',
  }),

  computed: {
    likesCount () {
      return get(this.tune, 'ratings.likes.length', 0);
    },

    dislikesCount () {
      return get(this.tune, 'ratings.dislikes.length', 0);
    },
  },

  methods: {

    hasUserLike (tune) {
      return Boolean(this.userLike(tune));
    },

    hasUserDislike (tune) {
      return Boolean(this.userDislike(tune));
    },

    userLike (tune) {
      return get(tune, 'ratings.likes', [])
        .find(like => Number(like.user_id) === Number(userID));
    },

    userDislike (tune) {
      return get(tune, 'ratings.dislikes', [])
        .find(dislike => Number(dislike.user_id) === Number(userID));
    },

    likeText (tune) {
      return this.hasUserLike(tune) ?
          moment(this.userLike(tune).created_at, 'YYYY-MM-DD HH:mm:ss').fromNow() + ' ' + this.$gettext('You liked this tune') :
          this.$gettext('Like this tune');
    },

    dislikeText (tune) {
      return this.hasUserDislike(tune) ?
          moment(this.userDislike(tune).created_at, 'YYYY-MM-DD HH:mm:ss').fromNow() + ' ' + this.$gettext('You disliked this tune') :
          this.$gettext('Dislike this tune');
    },

    likedAt (like) {
      return `${moment(like.created_at, 'YYYY-MM-DD HH:mm:ss').fromNow()} ${like.user.display_name} ${this.$gettext('liked this tune')}`;
    },

    dislikedAt (dislike) {
      return `${moment(dislike.created_at, 'YYYY-MM-DD HH:mm:ss').fromNow()} ${dislike.user.display_name} ${this.$gettext('disliked this tune')}`;
    },
  }
};

export {TuneRatingMixin};
