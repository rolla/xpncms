
import axios from 'axios'
import {get} from 'lodash';

const TuneRateMixin = {
  data: () => ({
    name: 'TuneRateMixin',
    componentAction: 'blank',
    isLoadingRating: false,
    ratingAction: undefined,
    showLikeLoader: false,
    showDislikeLoader: false,
    notifyObj: undefined,
  }),

  created () {

  },

  computed: {
    notify () {
      return this.notifyObj;
    },

    phpThumbPictureURL () {
      return get(this, 'ratedtune.phpThumbPictureURL', undefined);
    },

  },

  methods: {

    /**
     * @param tune
     * @param rating
     * @return {Promise<never>|Promise<AxiosResponse<any> | void>}
     */
    rateTune (tune, rating) {
      if (this.isLoadingRating) {
        return Promise.reject('isLoadingRating');
      }

      this.isLoadingRating = true;
      this.ratingAction = rating.action;

      this.showLikeLoader = this.isLoadingRating && this.ratingAction === 'liked';
      this.showDislikeLoader = this.isLoadingRating && this.ratingAction === 'disliked';

      const formData = new FormData();
      formData.append('action', rating.action);
      formData.append('action_category', rating.actionCategory);
      formData.append('item_id', rating.itemId);

      return axios.post(`${this.$store.getters.baseUrl}action/addhumanaction/`, formData).then(response => {
        this.isLoadingRating = false;
        this.ratingAction = undefined;
        this.showLikeLoader = false;
        this.showDislikeLoader = false;

        const result = get(response, 'data', undefined);
        const firstSystemResult = get(result, 'data.system[0]', undefined);

        this.shouldProcessError(result);

        if(get(firstSystemResult, 'category', undefined) === 'tune') {
              // trackEvent(this.name, this.componentAction, `${this.tune.artist} - ${this.tune.title}`);

              // var tuneData = _.head(result.data.human).item_data[0];
              // bus.$emit('currentTune', this.tune);
              // bus.$emit('rateTune', result);

              const selectedTuneId = get(
                this.$store.getters['tune/getSelectedTune'],
                'ID',
                undefined
              );

              if (selectedTuneId === tune.ID) {
                this.$store.dispatch('tune/reloadSelectedTune');
              }
        }

        this.$store.dispatch('tune/tuneUpdatedEvent',  {tuneId: tune.ID, formData : formData});

        return result;
      }).catch(error => {
        // this.error = error.toString();
        console.error(error);
      });
    },

    shouldProcessError (result) {
      const notify = get(result, 'rate.notify', undefined);
      if (get(notify, 'type', undefined) === 'error') {
        this.$store.dispatch('processNotifyError', notify);
      }
    },

    pushNotify (result) {
      const notify = get(result, 'rate.notify', undefined);
      if (notify) {
        this.notifyObj = notify;
      }

      /*
       * hack to emmit notification and be ready to next one
       * @TODO need better solution
       */
      setTimeout(() => {
        this.notifyObj = undefined;
      }, 1000);
    }
  }
};

export {TuneRateMixin};
