
import {get, isEmpty} from 'lodash';

const HelperMixin = {
  data: () => ({
    name: 'HelperMixin',
  }),
  methods: {
    getOr (obj, path, def) {
      const val = get(obj, path);
      return isEmpty(val) ? def : val
    }
  }
};

const ImageMixin = {
  data: () => ({
    name: 'ImageMixin',
  }),

  methods: {
    image(post) {
      const isFullUrl = new RegExp('https?://').test(post.image);
      const src = isFullUrl ? post.image : `${this.baseUrl}${this.imagePath}/${post.image}`;

      return `${this.phpThumbUrl}?q=100&h=500&zc=1&src=${src}`;
    },
  }
};

export {HelperMixin, ImageMixin};
