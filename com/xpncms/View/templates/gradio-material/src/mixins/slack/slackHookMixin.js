
import axios from 'axios'
import {get} from 'lodash';

const SlackHookMixin = {
  data: () => ({
    name: 'SlackHookMixin',
    componentAction: 'blank',
    isLoadingRating: false,
  }),

  created () {

  },

  methods: {
    postSlackHook (result) {
      // console.log('postSlackHook', result);

      const sendSlackHook = get(result, 'addhumanaction.sendSlackHook', false);

      if (get(result, 'data.system', undefined) && sendSlackHook) {
        const firstSystemResult = get(result, 'data.system[0]', undefined);
        const formData = new FormData();

        const actionValue = get(firstSystemResult, 'action', undefined);
        let itemAction = undefined;

        switch (actionValue) {
          case 'liked':
          case 'disliked':
            itemAction = 'rate';
            break;
          case 'updated':
            itemAction = 'update';
            break;
          default:
            itemAction = undefined;
        }

        const notifyAlert = get(result, 'slackHook.alert', undefined);

        // because in backend notifyAlert overrides itemActionValue
        if (notifyAlert) {
          formData.append('notifyAlert', notifyAlert);
        }

        formData.append('itemId', get(firstSystemResult, 'item_id', undefined));
        formData.append('itemType', get(firstSystemResult, 'category', undefined));
        formData.append('itemAction', itemAction);
        formData.append('itemActionValue', actionValue);

        // console.log(this.name, formData);

        axios.post(`${this.$store.state.base.url}action/slackhook`, formData).then(response => {

        }).catch(error => {
          // this.error = error.toString();
          console.error(error);
        });
      }
    },
  }
};

export {SlackHookMixin};
