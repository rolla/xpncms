
import axios from 'axios'
import {get} from 'lodash';

import PNotify from 'pnotify/dist/es/PNotify';
import PNotifyStyleMaterial from 'pnotify/dist/es/PNotifyStyleMaterial';
import PNotifyButtons from 'pnotify/dist/es/PNotifyButtons';
import PNotifyDesktop from 'pnotify/dist/es/PNotifyDesktop';
import PNotifyNonBlock from 'pnotify/dist/es/PNotifyNonBlock';
import PNotifyMobile from 'pnotify/dist/es/PNotifyMobile';
import PNotifyCallbacks from 'pnotify/dist/es/PNotifyCallbacks';
import PNotifyConfirm from 'pnotify/dist/es/PNotifyConfirm';

// Set default styling.
PNotify.defaults.styling = 'material';
// This icon setting requires the Material Icons font. (See below.)
PNotify.defaults.icons = 'material';

const NotifyMixin = {
  data: () => ({
    name: 'NotifyMixin',
    tuneEditdisableReason: undefined,
  }),

  created () {

  },

  methods: {
    notify (notifyObj, coverIcon) {
      if (notifyObj.error === 10) {
        notifyObj.loginDialog = true;
      }

      // console.log('NotifyMixin notifyObj', notifyObj);

      PNotify.modules.Desktop.permission();

      const notice = PNotify.alert({
        title: notifyObj.title,
        text: notifyObj.text,
        textTrusted: true,
        type: notifyObj.type,
        // delay: 80000,
        modules: {
          Buttons: {
            closer: true,
            closerHover: false,
            sticker: false,
          },
          Desktop: {
            desktop: true,
          },
          NonBlock: {
            nonblock: false
          },
          Callbacks: {
            afterClose (notice, timerHide) {
              notifyObj = undefined;
            }
          },
          // Mobile: {
          //   swipeDismiss: true,
          //   styling: true
          // }
        }
      });

      if (coverIcon) {
        notice.refs.iconContainer.appendChild(coverIcon);
      }
    },

    confirmNotify (cb) {
      let vm = this;

      const pnotify = PNotify.info({
        title: 'Esi tik laipns',
        text: 'Kāds iemesls atspējošanai?',
        //icon: 'glyphicon glyphicon-question-sign',
        type: 'info',
        hide: false,
        width: 'auto',
        stack: {
          // dir1: 'down',
          // dir2: 'down',
          // push: 'down',
          modal: true
        },
        modules: {
          Confirm: {
            confirm: true,
            // align: 'justify',
            buttons: [
              {
                text: 'Neatbilst ikdienas rotācijai',
                addClass: 'btn',
                click: function(notice) {
                  vm.tuneEditdisableReason = 'Does not match daily rotation';
                  notice.remove();
                }
              },
              {
                text: 'Nepatīk',
                addClass: 'btn',
                click: function(notice) {
                  vm.tuneEditdisableReason = 'Dislike';
                  notice.remove();
                }
              },
              {
                text: 'Par garu',
                addClass: 'btn',
                click: function(notice) {
                  vm.tuneEditdisableReason = 'Too long';
                  notice.remove();
                }
              },
              {
                text: 'Slikta kvalitāte',
                addClass: 'btn',
                click: function(notice) {
                  vm.tuneEditdisableReason = 'Poor quality';
                  notice.remove();
                }
              },
              {
                text: 'Tematiska dziesma',
                addClass: 'btn',
                click: function(notice) {
                  vm.tuneEditdisableReason = 'Thematic song';
                  notice.remove();
                }
              },
              {
                text: 'Cits iemesls',
                addClass: 'btn',
                click: function(notice) {
                  notice.remove();
                  PNotify.info({
                    title: 'Ok, ok',
                    text: 'Kāds tad ir iemesls?',
                    // icon: 'glyphicon glyphicon-question-sign',
                    type: 'info',
                    hide: false,
                    modules: {
                      Confirm: {
                        confirm: true,
                        prompt: true,
                        prompt_class: 'form-control',
                        buttons: [
                          {
                            text: "Ok",
                            addClass: "",
                            promptTrigger: true,
                            click: function (notice, value) {
                              vm.tuneEditdisableReason = value;
                              notice.remove();
                              // notice.get().trigger(
                              //   "pnotify.confirm",
                              //   [notice, value]
                              // );
                            }
                          },
                          {
                            text: "Cancel",
                            addClass: "",
                            click: function (notice) {
                              vm.tuneEditdisableReason = undefined;
                              notice.remove();
                              //notice.get().trigger("pnotify.cancel", notice);
                            }
                          }
                        ]
                      },
                      Buttons: {
                        closer: false,
                        sticker: false
                      },
                      History: {
                        history: false
                      },
                      Callbacks: {
                        afterClose: function(notice, timerHide) {
                          cb(vm.tuneEditdisableReason);
                        }
                      }
                    }
                  });
                }
              }
            ]
          },
          Buttons: {
            closer: false,
            sticker: false
          },
          History: {
            history: false
          },
          Callbacks: {
            afterClose: function(notice, timerHide) {
              cb(vm.tuneEditdisableReason);
            }
          }
        }
      });

      // return pnotify;

      // .get()
      //   .css({
      //     /*right: "calc(50% - 150px) !important;",
      //     "min-width":"800px",
      //     "width":"auto",
      //     "left": "-800px",*/
      //    });
    }
  }
};

export {NotifyMixin};
