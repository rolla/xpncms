const isDev = process.env.NODE_ENV === 'development';

import {
  consoleFunction,
  hotModuleDev,
  trackMatamo,
  trackGoogleAnalytics,
} from './lib/helpers';

consoleFunction();
hotModuleDev();

require('html5-boilerplate/dist/css/main.css');
require('html5-boilerplate/dist/css/normalize.css');

require('animate.css/animate.css');
require('../css/now-playing-flipcard.css');

require('../css/profile.css');
require('../css/gradio-tunes.css');
require('../css/style.css');

import (/* webpackPreload: true */ './scss/style.scss');

const moment = require('moment');
require('moment/locale/lv');

import PNotify from 'pnotify/dist/es/PNotify';

import Vue from 'vue';
import VueHead from 'vue-head';
import VueRouter from 'vue-router';
import vuetify from './plugins/vuetify';
import VueSocketIOExt from 'vue-socket.io-extended';

import Vue2Filters from 'vue2-filters';
import GetTextPlugin from 'vue-gettext';
import VueMoment from 'vue-moment';
import App from './App.vue'

import {scrollBehavior} from './lib/router';
import {routes} from './routes';
import translations from '../src/translations.json';

import 'vanilla-fade/dist/esm/fadeIn'; // single animation
import 'vanilla-fade/dist/esm/fadeOut'; // single animation
import {appFadeIn} from './lib/appFadeIn';
import {loadFacebook} from './lib/facebook';
import WOW from 'wowjs'
import ScrollReveal from 'scrollreveal'

import {store} from './store/store';
import { io } from 'socket.io-client';

Vue.use(VueHead);
Vue.use(VueRouter);
Vue.use(Vue2Filters);

Vue.config.productionTip = !isDev;
Vue.config.devtools = isDev;
Vue.config.silent = !isDev;
Vue.config.performance = isDev;

Vue.prototype.locale = window.locale;
Vue.config.language = window.locale; // from index.ejs
Vue.use(GetTextPlugin, {
  availableLanguages: {
    lv_LV: 'Latvian',
    en_US: 'English',
  },
  defaultLanguage: Vue.config.language,
  languageVmMixin: {
    computed: {
      currentKebabCase: function () {
        return this.current.toLowerCase().replace('_', '-')
      },
    },
  },
  translations: translations,
  silent: !isDev,
});

moment.locale(Vue.config.language);
Vue.use(VueMoment, {
  moment
});

const socket = io(process.env.SOCKET_URL, {
  debug: isDev,
  withCredentials: true,
});
Vue.use(
    VueSocketIOExt,
    socket,
    {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    }
);

const router = new VueRouter({
  // base: store.state.base.apiUrl,
  routes,
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior
});

const app = new Vue({
  el: '#app',
  vuetify,
  router,
  store,
  render: h => h(App),
  data () {
    return {
      loginDialog: false,
    }
  },

  created () {
    router.afterEach(() => {
      setTimeout(() => {
        trackMatamo();
        trackGoogleAnalytics();
      }, 1000);


    });
  },

  mounted () {
    // if (!_.isNil(flashNotify.modalNotify)) {
    //   modalNotify(flashNotify.modalNotify, flashNotify.newProfilePicAvailable);
    // }

    this.$nextTick(() => this.parseFacebook());
  },

  methods: {
    afterLeave () {
      this.$root.$emit('triggerScroll');
      // this.$vuetify.goTo('#navigation');
    },

    parseFacebook() {
      setTimeout(() => loadFacebook(), 3000);
      setTimeout(() => {
        if (typeof window.FB !== 'undefined') {
          window.FB.XFBML.parse();
        }
      }, 4000);
    },

    setActive () {
      /*
      var currEl = $(this.event.currentTarget);

      $('.navbar-nav li').removeClass('active');
      currEl.parent().addClass('active');
      */
    },

    // sendRateTune (result) {
    //   this.sendRateTuneSlackHook(result);
    // },

    // sendRateTuneSlackHook (result) {
    //   let vm = this;
    //
    //   if (isNil(result.addhumanaction)) {
    //     this.loginDialog = true;
    //
    //     return;
    //   }
    //
    //   const sendSlackHook = result.addhumanaction && result.addhumanaction.sendSlackHook ? true : false;
    //
    //   if (!isNil(head(result.data.system)) && sendSlackHook) {
    //     var rating = head(result.data.system);
    //     var formData = new FormData();
    //
    //     formData.append('itemId', rating.item_id);
    //     formData.append('itemType', 'tune');
    //     formData.append('itemAction', 'rate');
    //     formData.append('itemActionValue', rating.action);
    //     axios.post(`${store.state.base.apiUrl}/action/slackhook`, formData).then(function(response) {
    //
    //     });
    //   }
    // },

    // sendNotification (notify) {
    //   if (notify.error === 10) {
    //     this.loginDialog = true;
    //   }
    //
    //   PNotify.desktop.permission();
    //   new PNotify({
    //     title: notify.title,
    //     text: notify.text,
    //     type: notify.type,
    //     desktop: {
    //       desktop: true,
    //     },
    //     nonblock: {
    //       nonblock: false
    //     }
    //   });
    // },

    // sendEditTuneSlackHook (result) {
    //   let vm = this;
    //   //console.debug(result);
    //
    //   if (!isNil(result)) {
    //     var formData = new FormData();
    //     //formData.append('message', 'message');
    //     //formData.append('preText', 'Pretext');
    //     //formData.append('title', 'title');
    //     //formData.append('title_link', 'http://test.dev');
    //     formData.append('notifyAlert', result.alert);
    //     //formData.append('icon', ':musical_note:');
    //     formData.append('itemId', result.itemId);
    //     formData.append('itemType', 'tune');
    //     formData.append('itemAction', 'update');
    //     axios.post(`${store.state.base.apiUrl}/action/slackhook`, formData)
    //       .then(function(response) {
    //
    //       });
    //   }
    // },
  },
});

new WOW.WOW().init();
window.sr = ScrollReveal();

appFadeIn();

// Ensure required globals are available
// window.moment = moment;
// window.Vue = Vue;
window.WOW = WOW;
