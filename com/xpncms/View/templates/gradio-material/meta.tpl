{capture name="page_title"}
{block name="title" prepend}{$pageTitle} :{/block}
{/capture}

{block name="twitter_card_extend" append}

<meta name="twitter:label1" content="{$meta.twitterlabel1}">
<meta name="twitter:data1" content="{$meta.twitterdata1}">
<meta name="twitter:label2" content="{$meta.twitterlabel2}">
<meta name="twitter:data2" content="{$meta.twitterdata2}">

{/block}

{block name="schemaorg" append}
{if isset($tune) && !empty($tune) && $tune.likes + $tune.dislikes >= 1}
<!-- schema.org -->
<script type="application/ld+json">
   {
     "@context": "https://schema.org/",
     "@type": "AggregateRating",
     "ratingCount": "{$tune.likes + $tune.dislikes}",
     "ratingValue": "{$tune.likespercent}%",
     "itemReviewed": {
       "@type": "MusicRecording",
       "image": "{$meta.ogimage}",
       "name": "{$tune.artist} - {$tune.title}",
       "isrcCode": "{$tune.isrc}",
       "byArtist": {
         "@type": "MusicGroup",
         "name": "{$tune.artist}"
       },
       "inAlbum": {
         "@type": "MusicAlbum",
         "name": "{$tune.album}"
       },
       "recordLabel": {
         "@type": "Organization",
         "name": "{$tune.publisher}"
       },
       "interactionStatistic": [
         {
           "@type": "InteractionCounter",
           "interactionType": "http://schema.org/LikeAction",
           "userInteractionCount": "{$tune.likes}"
         },
         {
           "@type": "InteractionCounter",
           "interactionType": "http://schema.org/DislikeAction",
           "userInteractionCount": "{$tune.dislikes}"
         }
       ]
     }
   }
</script>
{/if}
{/block}

{block name="body" append}

{/block}
