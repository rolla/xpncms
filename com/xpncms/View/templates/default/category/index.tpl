{block name="body" append}

{function name=menu selected=0 level=0}
	{foreach $data as $entry}
		{if $entry.id == $selected}
			{str_repeat("--", $level)} {$entry.name}
		{else}
			
		{/if}
		{if is_array($entry.sub_category)}
			{menu data=$entry.sub_category selected=$selected level=$level+1}
		{else}
		
		{/if}
	{/foreach}
{/function}

					<br />
					<br />
					
					<br /><br />
					<div class="clearfix"></div>
					<div class="category-table">
					<table class="table">
						<thead>
							<tr>
								<th>{t}ID{/t}</th>
								<th>{t}Name{/t}</th>
								<th>{t}Content{/t}</th>
								<th>{t}Slug{/t}</th>
								<th>{t}Parent category{/t}</th>
								<th>{t}Added{/t}</th>
								<th>{t}Updated{/t}</th>
								<th>{t}Category class{/t}</th>
								<th>{t}Images{/t}</th>
								<th>{t}Order{/t}</th>
								<th>{t}Options{/t}</th>
							</tr>
						</thead>
						<tbody>
				
				{if isset($categories) }
					{foreach $categories as $category}
					
							<tr>
							<td class="category-id"><div><a href="{$url}category/{$category.cat_id}/" class="ajax-nav">{$category.cat_id}</a></div></td>
							<td class="category-name"><div>{$category.cat_name}</div></td>
							<td class="category-category"><div>{$category.cat_content}</div></td>
							<td class="category-alias"><div>{$category.cat_alias}</div></td>
							
							{if isset($categories_parent) }
									{* run the array through the function *}
									<td class="category-parent"><div>{menu data=$categories_parent selected=$category.cat_parent}</div></td>
							{else}

							{/if}
							<td class="category-added"><div>{$category.cat_added|relative_date}</div></td>
							<td class="category-updated"><div>{$category.cat_updated|relative_date}</div></td>
							<td class="category-class"><div>{$category.cat_class}</div></td>
							<td class="category-imgs"><div><img src="{$url}libs/phpThumb/?q=80&w=100&h=100&zc=1&src=../../{$paths.images}/{$category.cat_imgs}"/></div></td>
							<td class="category-order"><div>{$category.cat_order}</div></td>
							<td class='category-options'><div>
								<a href="#" class="btn btn-block"><i class="fa fa-thumbs-up"></i> {t}Like{/t}</a>
								<a href="#" class="btn btn-block"><i class="fa fa-thumbs-down"></i> {t}Dislike{/t}</a>
							</div>
							</td>
							
							</tr>
						<div class="clearfix"></div>
						
					{/foreach}
				{else}
					<tr>
						<td class="category-not-found" colspan="11"><h1 class="alert alert-danger">{t}Category not found{/t}</h1></td>
					</tr>
				{/if}
					</tbody>
					</table>
					</div>
					
{/block}