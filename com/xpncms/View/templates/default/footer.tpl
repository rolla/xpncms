{block name="body" append}
			<div class="clearfix"></div>
			<br />

			<div class="row">
				 <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
					<img src="http://fc06.deviantart.net/fs71/f/2012/310/8/1/untitled_drawing_by_northdakotaoc-d5k7xci.png" class="suncic" />
				</div>
			</div>

			</div>

			<div id="fb-root"></div>
			{asset_compile}
			{/asset_compile}
			<script type="text/javascript" src="{$url}js/twitter.js"></script>
			<script type="text/javascript" src="{$url}js/facebook.js"></script>
			<script type="text/javascript" src="//www.draugiem.lv/api/api.js"></script>

			<div class="clearfix"></div>

			<div class="row soc-pogas center">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div id="tweetBtn"></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div id="draugiemLike"></div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="fb-share-button" data-href="'+url+'" data-send="false" data-type="button_count" data-width="450" data-show-faces="true" data-colorscheme="light"></div>
				</div>
			</div>
			<div class="clearfix"></div>
			{social_icons}
			<div class="clearfix"></div>
			<br />
			<div id="toTop" class="btn btn-large btn-primary">{t}To top{/t}</div>

			<div class="clearfix"></div>
			<!-- FOOTER -->
			<footer class="footer">
				<div class="footer-content">
					<a class="footer-img" href="//urdt.lv/?bannerclient=sr" target="_blank" ><img src="{$url}img/logos/urdt-cr-logo.png"/></a>
					<p class="footer-cr" style="display:none"><a href="//urdt.lv/?bannerclient=gradio" target="_blank" >&copy; 2014 Universal Radio DJ Team v4.0</a></p>
				</div>
			</footer>
			<div class="clearfix"></div>
		</div> <!-- /loading-effect -->
	</div><!-- /container -->

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script>
		//var xpnCMS;
		var baseURL = "{$url}";
		{if isset($userData.id)}
		var userID = {$userData.id};
		{else}
		var userID = "guest";
		{/if}

		var appTitle = "{$title}";
		var now;
		//var Notify, simpleNotify, confirmNotify;
		var core_do_u_wanna_reload = "{t}Do you wanna reload?{/t}";
		var core_error_loading_data= "{t}Error loading data{/t}";
		var core_please_confirm = "{t}Please confirm{/t}";

	</script>

	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

	{asset_compile}
	{/asset_compile}
	<script type="text/javascript" src="{$url}assets/jquery/jquery-migrate.js"></script>

	<script type="text/javascript" src="{$url}assets/jplayer/jquery.jplayer/jquery.jplayer.js"></script>
	<!--<script type="text/javascript" src="{$url}assets/jquery-address/src/jquery.address.js"></script>-->
	<script type="text/javascript" src="{$url}assets/jquery-address-old/jquery.address-1.6-mod.js"></script>
	<script type="text/javascript" src="{$url}assets/jquery.TubePlayer/jQuery.tubeplayer.min.js"></script>

	<script type="text/javascript" src="{$url}assets/jquery.easing/js/jquery.easing.compatibility.js"></script>
	<!--<script type="text/javascript" src="{$url}assets/pnotify-old/jquery.pnotify.js"></script>-->
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.core.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.buttons.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.confirm.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.nonblock.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.desktop.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.history.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.callbacks.js"></script>
	<script type="text/javascript" src="{$url}assets/pnotify/pnotify.reference.js"></script>

	<!--
	<script type="text/javascript" src="{$url}assets/jquery.svg/jquery.svg.js"></script>
	<script type="text/javascript" src="{$url}assets/jquery.svg/jquery.svganim.js"></script>
	<script type="text/javascript" src="{$url}js/gradio/gradio.svg.js"></script>
	-->

	<script type="text/javascript" src="{$url}assets/jquery-resizeimagetoparent/jquery.resizeimagetoparent.js"></script>
	<script type="text/javascript" src="{$url}assets/perfect-scrollbar/min/perfect-scrollbar.min.js"></script>

	<script type="text/javascript" src="{$url}assets/opentip/downloads/opentip-jquery-excanvas.min.js"></script>


	<script type="text/javascript" src="{$url}assets/swfobject/swfobject/swfobject.js"></script>

	<script type="text/javascript" src="{$url}assets/html5-boilerplate/js/plugins.js"></script>
	<script type="text/javascript" src="{$url}assets/html5-boilerplate/js/vendor/modernizr-2.6.2.min.js"></script>

	<script type="text/javascript" src="{$url}assets/jquery-cookie/jquery.cookie.js"></script>

	<script type="text/javascript" src="{$url}assets/nprogress/nprogress.js"></script>
	<script type="text/javascript" src="{$url}assets/CanvasLoader/js/heartcode-canvasloader-min.js"></script>
	<script type="text/javascript" src="{$url}assets/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="{$url}assets/datatables-bootstrap3/BS3/assets/js/datatables.js"></script>
	<script type="text/javascript" src="{$url}assets/jquery-form/jquery.form.js"></script>
	<script type="text/javascript" src="{$url}assets/js-logger/src/logger.js"></script>
	<script type="text/javascript" src="{$url}assets/extend.js/src/extend.js"></script>

	<!-- elfinder core -->
	<script src="{$url}assets/elfinder/js/elFinder.js"></script>
	<script src="{$url}assets/elfinder/js/elFinder.version.js"></script>
	<script src="{$url}assets/elfinder/js/jquery.elfinder.js"></script>
	<script src="{$url}assets/elfinder/js/elFinder.resources.js"></script>
	<script src="{$url}assets/elfinder/js/elFinder.options.js"></script>
	<script src="{$url}assets/elfinder/js/elFinder.history.js"></script>
	<script src="{$url}assets/elfinder/js/elFinder.command.js"></script>

	<!-- elfinder ui -->
	<script src="{$url}assets/elfinder/js/ui/overlay.js"></script>
	<script src="{$url}assets/elfinder/js/ui/workzone.js"></script>
	<script src="{$url}assets/elfinder/js/ui/navbar.js"></script>
	<script src="{$url}assets/elfinder/js/ui/dialog.js"></script>
	<script src="{$url}assets/elfinder/js/ui/tree.js"></script>
	<script src="{$url}assets/elfinder/js/ui/cwd.js"></script>
	<script src="{$url}assets/elfinder/js/ui/toolbar.js"></script>
	<script src="{$url}assets/elfinder/js/ui/button.js"></script>
	<script src="{$url}assets/elfinder/js/ui/uploadButton.js"></script>
	<script src="{$url}assets/elfinder/js/ui/viewbutton.js"></script>
	<script src="{$url}assets/elfinder/js/ui/searchbutton.js"></script>
	<script src="{$url}assets/elfinder/js/ui/sortbutton.js"></script>
	<script src="{$url}assets/elfinder/js/ui/panel.js"></script>
	<script src="{$url}assets/elfinder/js/ui/contextmenu.js"></script>
	<script src="{$url}assets/elfinder/js/ui/path.js"></script>
	<script src="{$url}assets/elfinder/js/ui/stat.js"></script>
	<script src="{$url}assets/elfinder/js/ui/places.js"></script>

	<!-- elfinder commands -->
	<script src="{$url}assets/elfinder/js/commands/back.js"></script>
	<script src="{$url}assets/elfinder/js/commands/forward.js"></script>
	<script src="{$url}assets/elfinder/js/commands/reload.js"></script>
	<script src="{$url}assets/elfinder/js/commands/up.js"></script>
	<script src="{$url}assets/elfinder/js/commands/home.js"></script>
	<script src="{$url}assets/elfinder/js/commands/copy.js"></script>
	<script src="{$url}assets/elfinder/js/commands/cut.js"></script>
	<script src="{$url}assets/elfinder/js/commands/paste.js"></script>
	<script src="{$url}assets/elfinder/js/commands/open.js"></script>
	<script src="{$url}assets/elfinder/js/commands/rm.js"></script>
	<script src="{$url}assets/elfinder/js/commands/info.js"></script>
	<script src="{$url}assets/elfinder/js/commands/duplicate.js"></script>
	<script src="{$url}assets/elfinder/js/commands/rename.js"></script>
	<script src="{$url}assets/elfinder/js/commands/help.js"></script>
	<script src="{$url}assets/elfinder/js/commands/getfile.js"></script>
	<script src="{$url}assets/elfinder/js/commands/mkdir.js"></script>
	<script src="{$url}assets/elfinder/js/commands/mkfile.js"></script>
	<script src="{$url}assets/elfinder/js/commands/upload.js"></script>
	<script src="{$url}assets/elfinder/js/commands/download.js"></script>
	<script src="{$url}assets/elfinder/js/commands/edit.js"></script>
	<script src="{$url}assets/elfinder/js/commands/quicklook.js"></script>
	<script src="{$url}assets/elfinder/js/commands/quicklook.plugins.js"></script>
	<script src="{$url}assets/elfinder/js/commands/extract.js"></script>
	<script src="{$url}assets/elfinder/js/commands/archive.js"></script>
	<script src="{$url}assets/elfinder/js/commands/search.js"></script>
	<script src="{$url}assets/elfinder/js/commands/view.js"></script>
	<script src="{$url}assets/elfinder/js/commands/resize.js"></script>
	<script src="{$url}assets/elfinder/js/commands/sort.js"></script>
	<script src="{$url}assets/elfinder/js/commands/netmount.js"></script>

	<!-- elfinder languages -->
	<script src="{$url}assets/elfinder/js/i18n/elfinder.en.js"></script>

	<!-- elfinder dialog -->
	<script src="{$url}assets/elfinder/js/jquery.dialogelfinder.js"></script>

	<!-- elfinder 1.x connector API support -->
	<script src="{$url}assets/elfinder/js/proxy/elFinderSupportVer1.js"></script>

	<!-- elfinder custom extenstions
	<script src="{$url}assets/elfinder/extensions/jplayer/elfinder.quicklook.jplayer.js"></script>
	-->


	<script type="text/javascript" src="{$url}assets/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="{$url}assets/ckeditor/adapters/jquery.js"></script>

	<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

	{asset_compile}
	{/asset_compile}

	<script type="text/javascript" src="{$url}assets/shufflejs/dist/jquery.shuffle.min.js"></script>

	<!-- *JS* -->
	<!--
	<script src="{$url}assets/prototype/prototype.js"></script>
	<script src="{$url}assets/prototype/scriptaculous/scriptaculous.js"></script>
	<script src="{$url}assets/prototype-opentip/opentip-prototype.js"></script>
	-->

	<!-- gradio js -->

	<!--
	<script src="http://cmpl.it-studenti.liepu.edu.lv/media/jui/js/jquery.min.js" type="text/javascript"></script>
	<script src="{$url}assets/jquery.center.js"></script>
	<script src="{$url}assets/jquery.scrollTo-min.js"></script>
	-->

	<!--
	<script src="{$url}assets/supersized/supersized.3.2.7.js" type="text/javascript"></script>
	<script src="{$url}assets/supersized/supersized.shutter.js" type="text/javascript"></script>
	-->


	<!--<script src="{$url}assets/jquery-ui-1.10.2.custom.js"></script>-->

	<!--<script src="{$url}assets/jquery.svg/jquery.svgdom.js"></script>-->

	<!--
	<script src="{$url}assets/jquery.svg/jquery.svgfilter.js"></script>
	<script src="{$url}assets/jquery.svg/jquery.svggraph.js"></script>
	-->

	<script type="text/javascript">
		//core
		{if isset($smarty.session.debug) && $smarty.session.debug == "true" }
			// Only log WARN and ERROR messages.
			//Logger.setLevel(Logger.WARN);
			Logger.useDefaults();
			//Logger.debug({$smarty.session._sf2_attributes.debug});
			//Logger.debug({$smarty.session._sf2_attributes.debug});
			Logger.debug("Debug is set to: {$smarty.session._sf2_attributes.debug}");
		{else}
			Logger.setLevel(Logger.OFF);

			Logger.debug("Debug is set to: {$smarty.session._sf2_attributes.debug}");
		{/if}
	</script>

	{asset_compile}
	{/asset_compile}
	<script type="text/javascript">
		//core

		var URL = "http://gradio.lv";
		var title = "【G】radio";
		var simple_title = "gradio";
		var title_prefix = "【G】radio";
		var result = new Array();

		{literal}
		Opentip.styles.xpncms = {
				extends: "dark",
				delay: 0.3,
				stemLength: 10,
				stemBase: 10,
				borderWidth: 3,
				borderRadius: 5,
				borderColor: "#fff",
				color: "#000",
				background: [ [ 0, "#C4F83E" ], [ 1, "#D2F870" ] ],
				closeButtonCrossColor: "rgba(255, 255, 255, 1)",
				tipJoint: "bottom left",
				removeElementsOnHide: true
			};
		Opentip.debug = false;
		Opentip.defaultStyle = "xpncms";

		//setTimeout(function(){
		//}, 1000);


		{/literal}

		//$.noConflict();
		$(function($){

			//nowPlaying();

			//bindAddress('.ajax-nav');

			//iegutTweets();

			/*
			$(".ajax-nav").click(function(){
				$(".ajax-nav").each(function(){
					$(this).removeClass("aktiva");
				});
				$(this).addClass("aktiva");
			});
			*/

			//iegutBildes();
			//iegutSaturu("sakums");

			setTimeout(function(){
				//createOTips();
				//ieladetSupersized(result);
			}, 2000);

			/*
			if(window.location.href.indexOf("bannerclient") > -1){
				//$("#video").modal("show");
				$(".modal,.modal-footer").css("background-color","#000");
				$(".modal-body").css("max-height","500px");

				$("#video").modal({
				show: true,
				backdrop: true,
				keyboard: true
				}).css({
					"width": "890px",
					"margin-left": function(){
					return -($(this).width() / 2);
					}
				});
				//console.log("ir adrese");
			}
			*/

			//iegutdatus();
			//startRemain();

			$(window).scroll(function(){
				if($(this).scrollTop() != 0){
					$("#toTop").fadeIn();
				}else{
					$("#toTop").fadeOut();
				}
			});

			{literal}
			$("#toTop").click(function() {
				$("body,html").animate({scrollTop:0},800);
			});

			$(".suncic").delay(1000).slideUp(2000, function(){
				$(".masivs").animate({"margin-top":"70px"}, 2000);
			});

			{/literal}

			$(".footer-content").hover(function(){
				$(".footer-img").fadeOut();
				$(".footer-cr").fadeIn("1000");
			},
			function(){
				$(".footer-cr").fadeOut("1000");
				$(".footer-img").fadeIn();
			});


			$(document).on("click", ".fast-user-settings", function(){
				var exp_elem = $(this).parent().find(".fast-user-settings-expand");

				exp_elem.fadeToggle(function(){

					if(exp_elem.is(":visible")){
						$(this).parent().find(".user-chevron").removeClass("fa-chevron-down").addClass("fa-chevron-up");
					}else{
						$(this).parent().find(".user-chevron").removeClass("fa-chevron-up").addClass("fa-chevron-down");
					}

				});



			});


		});

	</script>

	{asset_compile}
	{/asset_compile}
	<script type="text/javascript" src="{$url}assets/bootstrap/dist/js/bootstrap.js"></script>

	<script type="text/javascript" src="{$url}js/init.js"></script>
	<script type="text/javascript" src="{$url}js/pages.js"></script>
	<script type="text/javascript" src="{$url}js/tabs.js"></script>
	<script type="text/javascript" src="{$url}js/notify.js"></script>

	{asset_compile}
	{/asset_compile}
	<script type="text/javascript">

	//console.debug(xpnCMS);

	$(document).ajaxStart(function(){
          xpnCMS.NProgressStart();
	});

	$(document).ajaxStop(function(){
	  xpnCMS.NProgressStop();
	});

	{literal}

	//xpnCMS.init({baseURL: baseURL});

	{/literal}

		$(document).on("click", ".adm-del", function(){
			var href = $(this).attr("data-href");
			var alert = $(this).attr('data-confirm');


			var notify = {
				title: t("Please confirm"),
				alert: alert,
				type: "error"
			}

			//$("#dataConfirmModal").find(".modal-body").text($(this).attr("data-confirm"));
			//$("#dataConfirmOK").attr("href", href);
			//$("#dataConfirmModal").modal("show");
			xpnCMSNotify.confirmNotify(notify, href);
			return false;
		});

		</script>

</div><!-- /.grid -->

{if isset($ajax) && $ajax == false}
	<script type="text/javascript">
		xpnCMS.loadPage("{$pageId}");
	</script>
{/if}

	<!-- Modal -->
	<div id="dataConfirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="dataConfirmModallabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="dataConfirmModal">{t}Please confirm{/t}</h3>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">{t}Cancel{/t}</button>
		<a href="" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="dataConfirmOK">{t}Delete{/t}</a>
	</div>
	</div>

	<script type="text/javascript">
		//console.log( console.logArray() );
	</script>

	{block name="footer"}{/block}

</body>
</html>
{/block}
