{block name="title" append}
:{$page_title}
{/block}
{block name="body" append}
<h1 class="slim-text center">{$title}</h1>
<br />
<div class="row">

	{if isset($users) }
		{foreach $users as $user}

	<div class="col-md-2 col-sm-3 col-xs-6 ">
		<h2>{$user.first_name} {$user.last_name}</h2>
		<img src="{$user.photo_url}" alt="{$user.first_name} {$user.last_name}" />
	</div>
	
		{/foreach}
	{else}
		<h1 class="alert alert-danger">{t}Content not found{/t}</h1>
	{/if}
	
</div>
<br /><br />

{/block}