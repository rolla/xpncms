{block name="title" append}
:{$page_title}
{/block}
{block name="body" append}
<h1 class="slim-text center">{$title}</h1>
<br />

		<div class="row">
		 <div class="col-md-7 col-sm-3 col-xs-6 "><img src="{$base_url}img/logos/only-g-big-without-gloss-web.png" alt="{t}Only G{/t}"></div>
		<div class="col-md-4 col-md-offset-1 col-sm-3 col-xs-6 ">
			<br />
			<fieldset>
				<legend>{t}Sign in with social network{/t}</legend>
				<a href="../auth/connect/facebook" class="btn btn-block btn-social btn-facebook auth">
					<i class="fa fa-facebook"></i> {t}Sign in with{/t} {t}Facebook{/t}
				</a>
				<a href="../auth/connect/twitter" class="btn btn-block btn-social btn-twitter auth">
					<i class="fa fa-twitter"></i> {t}Sign in with{/t} {t}Twitter{/t}
				</a>
				<a href="../auth/connect/draugiem" class="btn btn-block btn-social btn-draugiem auth">
					<i class="fa fa-user"></i> {t}Sign in with{/t} {t}Draugiem{/t}
				</a>
				<a href="../auth/connect/google" class="btn btn-block btn-social btn-google-plus auth">
					<i class="fa fa-google-plus"></i> {t}Sign in with{/t} {t}Google{/t}
				</a>
			</fieldset>
		</div>

<!--
<table width="00%" border="0" cellpadding="2" cellspacing="2">
  <tr>
    <td valign="top"><fieldset>
       
		<legend>Sign-in form</legend>
        <form action="" method="post">
		<table width="300" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td><div align="right"><strong>{t}E-mail{/t}</strong></div></td>
            <td><input type="text" name="email" /></td>
          </tr>
          <tr>
            <td><div align="right"><strong>{t}Password{/t}</strong></div></td>
            <td><input type="text" name="password" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="right"><input type="submit" value="{t}Sign in{/t}" /> </td>
          </tr>
        </table>
		</form>
      </fieldset></td>
    <td valign="top" align="left"> 
		
		<fieldset>
			<legend>{t}Dont have account{/t}</legend>
			&nbsp;&nbsp;<a href="?route=users/register">{t}New account signup{/t}</a><br />
		</fieldset>
	  </td>
  </tr>
</table>
-->
{/block}