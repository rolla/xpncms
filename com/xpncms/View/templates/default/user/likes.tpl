{block name="title" append}
:{$page_title}
{/block}
{block name="body" append}
<h1 class="slim-text center">{$title}</h1>
<br />
<div class="row">
	<div class="col-md-2 col-sm-3 col-xs-6 ">
		<img src="{$userData.photo_url}" alt="{$userData.first_name} {$userData.last_name}" />
	</div>
	 <div class="col-md-5 col-sm-3 col-xs-6 ">
		<h2>{t}Hello{/t} {$userData.first_name} {$userData.last_name}</h2>
		
		
	</div>
	<div class="col-md-2 col-md-offset-3 col-sm-3 col-xs-6 ">
		<a href="../users/logout" class="btn btn-block btn-social btn-google-plus">
			<i class="fa fa-power-off "></i> {t}Logout{/t}
		</a>
	</div>
</div>
<br /><br />

<h1>{t}Your likes{/t}</h1>
{/block}