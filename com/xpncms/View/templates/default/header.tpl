{block name="header"}
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="{$meta.contenttype}" />
	<meta http-equiv="content-style-type" content="{$meta.contentstyletype}" />
	<meta http-equiv="content-language" content="{$meta.contentlanguage}" />
	<title>{block name="title"}{$title}{/block}:{t}{$page_title}{/t}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="description" content="{$meta.content}" />
	<meta name="keywords" content="{$meta.keywords}" />
	<meta name="copyright" content="{$meta.copyright}" />
	<meta name="author" content="{$meta.author}" />
	<meta name="robots" content="{$meta.robots}" />
	<meta name="abstract" content="{$meta.abstract}" />
	<meta name="distribution" content="{$meta.distribution}" />
	<meta name="web_author" content="{$meta.webauthor}" />
	<meta name="google-site-verification" content="{$meta.googlesitev}" />
	
	<!-- facebook -->
	<meta property="fb:app_id" content="{$meta.fbappid}" />
	<meta property="fb:admins" content="{$meta.fbadmins}" />
	<meta property="og:url" content="{$meta.ogurl}" />
	<meta property="og:image" content="{$meta.ogimage}" />
	<meta property="og:type" content="{$meta.ogtype}" />
	<meta property="og:title" content="{$meta.ogtitle}" />
	<!-- /facebook -->
	
	<!-- twitter -->
	<meta name="twitter:card" content="{$meta.twittercard}">
	<meta name="twitter:site" content="{$meta.twittersite}">
	<meta name="twitter:creator" content="{$meta.twittercreator}">
	<meta name="twitter:title" content="{$meta.twittertitle}">
	<meta name="twitter:description" content="{$meta.twitterdescription}">
	{block name="twitter_card_extend"}{/block}
	<!-- /twitter -->
	
	{* <base href="{$url}"> *}
	
	<!-- *FAV-ICONS* -->
	<link rel="icon" href="{$url}img/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="{$url}img/favicon.ico" type="image/xicon" />
	
	<!-- css stili -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300&subset=latin,latin-ext">
	<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.10.3/themes/trontastic/jquery-ui.css">

	{asset_compile}
	{/asset_compile}
	<link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap/dist/css/bootstrap-theme.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/bootstrap-social/bootstrap-social.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/fontawesome/css/font-awesome.min.css"/>
	
	<link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/pnotify.core.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/pnotify.picon.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/pnotify.history.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/pnotify/pnotify.buttons.css"/>
	
	<link rel="stylesheet" type="text/css" href="{$url}assets/nprogress/nprogress.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/perfect-scrollbar/min/perfect-scrollbar.min.css"/>
	
	<link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/html5-boilerplate/css/normalize.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/jquery.svg/jquery.svg.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/DataTables/media/css/jquery.dataTables_themeroller.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/datatables-bootstrap3/BS3/assets/css/datatables.css"/>

	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/common.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/dialog.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/toolbar.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/navbar.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/statusbar.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/contextmenu.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/cwd.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/quicklook.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/commands.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/fonts.css"/>
	<link rel="stylesheet" type="text/css" href="{$url}assets/elfinder/css/theme.css"/>
	
	<link rel="stylesheet" type="text/css" href="{$url}css/default/style.css"/>
	{block name="css"}{/block}
	
	</head>
{/block}

{block name="body"}
<body>
	<div class="bg-eff"></div>
		<div class="container-fluid profile-row">
				{profile_row}
		</div>

<div class="grid">
	<div class="container-fluid saturs">
	
		<!-- Modal -->
		<div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modallabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="modallabel"></h3>
		</div>
		<div class="modal-body">
			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Aizvērt</button>
		</div>
		</div>
		
		<nav class="navbar navbar-default navbar-fixed-top gradio-menu">
		
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar">-</span>
			<span class="icon-bar">-</span>
			<span class="icon-bar">-</span>
		  </button>
		</div>
		
		<div id="navbar" class="navbar-collapse collapse">
		  <ul class="nav navbar-nav">
			<li>
				<a href="{$url}" class="sakums ajax-nav">{t}Start{/t}</a>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">{t}Categories{/t} <b class="caret"></b></a>
				<ul class="dropdown-menu">
				 {front_nav} 
				</ul>
			</li>
			{ifadmin}
			<li>
				<a href="{$url}admin/" class="ajax-nav">{t}Admin{/t}</a>
			</li>
			<li>
				<a href="javascript:location.reload(true)">{t}Reload{/t}</a>
			</li>
		  </ul>
		</div>
		</nav>
		
		<div class="loading-effect">
			
			<div class="dyn-content">
{/block}