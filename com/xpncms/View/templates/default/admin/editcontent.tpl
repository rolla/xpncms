
{block name="body" append}

{function name=menu selected=0 level=0}
	{foreach $data as $entry}
		{if $entry.name == $selected}
			<option value="{$entry.id}" selected="selected">{str_repeat("--", $level)} {$entry.name}</option>
		{else}
			<option value="{$entry.id}">{str_repeat("--", $level)} {$entry.name}</option>
		{/if}
	
		{if is_array($entry.sub_category)}
			{menu data=$entry.sub_category selected=$selected level=$level+1}
		{else}

		{/if}
	{/foreach}
{/function}


<br /><br />
	<h3>{t}Edit content{/t}</h3>
	
	
		<div class="row">
		<div class="col-md-12">
		{if isset($contents) }
			{foreach $contents as $content}

				
					<form id="save-form" action="{$url}admin/savecontent/" method="post" class="form-horizontal" role="form">
						<input type="hidden" name="save_content" value="1">
						<input type="hidden" name="content_type" value="content">
						<input type="hidden" name="cont_id" value="{$content.cont_id}">
						
						<a href="{$url}admin/contents/" class="btn btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
						<button type="submit" class="btn btn-success adm-save"><i class="fa fa-save"></i> {t}Save{/t}</button>
						
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputsaturaid">{t}ID{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" value="{$content.cont_id}" disabled="disabled" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputnosaukums">{t}Name{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_name"  id="nosaukums" value="{$content.cont_name}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_alias">{t}Slug{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_alias" id="cont_alias" value="{$content.cont_alias}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputsadala">{t}Category{/t}</label>
							<div class="col-sm-11">
								<select class="form-control" name="cont_category">
									<option value="0">Parent</option>
								{if isset($categories) }
									{* run the array through the function *}

									{menu data=$categories selected=$content.cont_category}

								</select>
								{else}


								{/if}
								<!--<input class="form-control" type="text" name="cont_category" id="cont_category" value="{$content.cont_category}">-->
							</div>
						</div>
						<div class="edit-sat">
							<textarea id="saturs" class="ckeditor" name="cont_content" rows="15" cols="100" style="width: 100%">{$content.cont_content}</textarea>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputdatums">{t}Added{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" value="{$content.cont_added}" disabled="disabled" />
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputbild">{t}Images{/t}</label>
							<div class="col-sm-11">

								<img class="cont_imgs_add" src="{$url}{$paths.images}/{$content.cont_imgs}">

								<input class="form-control cont_imgs_added" type="hidden" name="cont_imgs" id="cont_imgs" value="{$content.cont_imgs}" />

								<!--<input class="form-control" type="text" name="cont_imgs" id="bilde" value="{$content.cont_imgs}" />-->

							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_tags">{t}Tags{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_tags" id="cont_tags" value="{$content.cont_tags}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputbild">{t}Order{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_order" id="bilde" value="{$content.cont_order}" />
							</div>
						</div>
						
						<a href="{$url}admin/contents/" class="btn btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
						<button type="submit" class="btn btn-success adm-save"><i class="fa fa-save"></i> {t}Save{/t}</button>
						
					</form>
					

			{/foreach}
		{else}
			<h1 class="alert alert-danger">{t}Content not found{/t}</h1>
		{/if}
		</div>
		</div>
		
{/block}