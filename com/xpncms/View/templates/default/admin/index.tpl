{block name="body" append}
<h1 class="slim-text center">{$title}</h1>
<br />
<div class="row">
	<div class="col-md-2 col-sm-3 col-xs-6 ">
		<img src="{$userData.photo_url}" alt="{$userData.first_name} {$userData.last_name}" />
	</div>
	 <div class="col-md-5 col-sm-3 col-xs-6 ">
		<h2>{t}Hello{/t} {$userData.first_name} {$userData.last_name}</h2>
		
		
	</div>
	<div class="col-md-2 col-md-offset-3 col-sm-3 col-xs-6 ">
		{logout}
	</div>
</div>
		<div class="row">
		
		
	<section>
	<ul class="nav nav-tabs">
		<li><a href="#course" data-level="1" data-toggle="tab">Course</a></li>
		<li><a href="#module1" data-level="1" data-toggle="tab">Module1</a></li>
		<li><a href="#module2" data-level="1" data-toggle="tab">Module2</a></li>	 
	</ul>
	<ul class="tab-content">
		<li class="tab-pane" id="course">
			<p>I'm in a course.</p>
		</li>
		<li class="tab-pane" id="module1">
			<ul class="nav nav-tabs">
				<li><a href="#module1-step1" data-level="2" data-toggle="tab">Step 1</a></li>
				<li><a href="#module1-step2" data-level="2" data-toggle="tab">Step 2</a></li>
			</ul>
			<ul class="tab-content">
				<li class="tab-pane" id="module1-step1">
					<p>I'm Module 1 Step 1.</p>
				</li>
				<li class="tab-pane" id="module1-step2">
					<p>Howdy, I'm Module 1 Step 2</p>
				</li>
			</ul>
		</li>
		<li class="tab-pane" id="module2">
			<ul class="nav nav-tabs">
				<li><a href="#module2-step1" data-level="2" data-toggle="tab">Step 1</a></li>
				<li><a href="#module2-step2" data-level="2" data-toggle="tab">Step 2</a></li>
			</ul>
			<ul class="tab-content">
			
				<li class="tab-pane" id="module2-step1">
					<p>I'm Module 2 Step 1.</p>
				</li>
				
				<li class="tab-pane" id="module2-step2">
					<p>Howdy, I'm Module 2 Step 2</p>
					
					<ul class="nav nav-tabs">
						<li><a href="#step2-subtab1" data-level="3" data-toggle="tab">step2-subtab1</a></li>
						<li><a href="#step2-subtab2" data-level="3" data-toggle="tab">step2-subtab2</a></li>
					</ul>
					
					<ul class="tab-content">
						<li class="tab-pane" id="step2-subtab1">
							<p>step2-subtab1</p>
							
							<ul class="nav nav-tabs">
								<li><a href="#step3-subtab1" data-level="4" data-toggle="tab">step3-subtab1</a></li>
								<li><a href="#step3-subtab2" data-level="4" data-toggle="tab">step3-subtab2</a></li>
							</ul>
							
							<ul class="tab-content">
								<li class="tab-pane" id="step3-subtab1">
									<p>step3-subtab1</p>
								</li>
							</ul>
							
							
						</li>
						<li class="tab-pane" id="step2-subtab2">
							<p>step2-subtab2</p>
						</li>
					</ul>
					
				</li>
				
			</ul>
		</li>
	</ul>
	</section>
		
		
		
	<section>
	<ul class="nav nav-tabs">
		<li><a href="#course" data-toggle="tab">Course</a></li>
		<li><a href="#module1" data-toggle="tab">Module1</a></li>
		<li><a href="#module2" data-toggle="tab">Module2</a></li>	 
	</ul>
	<ul class="tab-content">
		<li class="tab-pane active" id="course">
			<p>I'm in a course.</p>
		</li>
		<li class="tab-pane" id="module1">
			<ul class="nav nav-tabs">
				<li><a href="#module1-step1" data-toggle="tab">Step 1</a></li>
				<li><a href="#module1-step2" data-toggle="tab">Step 2</a></li>
			</ul>
			<ul class="tab-content">
				<li class="tab-pane active" id="module1-step1">
					<p>I'm Module 1 Step 1.</p>
				</li>
				<li class="tab-pane" id="module1-step2">
					<p>Howdy, I'm Module 1 Step 2</p>
				</li>
			</ul>
		</li>
		<li class="tab-pane" id="module2">
			<ul class="nav nav-tabs">
				<li><a href="#module2-step1" data-toggle="tab">Step 1</a></li>
				<li><a href="#module2-step2" data-toggle="tab">Step 2</a></li>
			</ul>
			<ul class="tab-content">
			
				<li class="tab-pane active" id="module2-step1">
					<p>I'm Module 2 Step 1.</p>
				</li>
				
				<li class="tab-pane" id="module2-step2">
					<p>Howdy, I'm Module 2 Step 2</p>
					
					<ul class="nav nav-tabs">
						<li><a href="#step2-subtab1" data-toggle="tab">step2-subtab1</a></li>
						<li><a href="#step2-subtab2" data-toggle="tab">step2-subtab2</a></li>
					</ul>
					
					<ul class="tab-content">
						<li class="tab-pane active" id="step2-subtab1">
							<p>step2-subtab1</p>
						</li>
						<li class="tab-pane" id="step2-subtab2">
							<p>step2-subtab2</p>
						</li>
					</ul>
					
				</li>
				
			</ul>
		</li>
	</ul>
	</section>

			<div id="tabs">

			<ul class="nav nav-tabs nav-justified" role="tablist">
				<li class=""><a href="#dashboard" aria-controls="dashboard" role="tab" data-toggle="tab"><h1><i class="fa fa-home"></i> {t}Dashboard{/t}</h1></a></li>
				<li class=""><a href="#content" aria-controls="content" role="tab" data-toggle="tab"><h1><i class="fa fa-desktop"></i> {t}Content{/t}</h1></a></li>
				<li class=""><a href="#users"  aria-controls="users" role="tab" data-toggle="tab"><h1><i class="fa fa-group"></i> {t}Users{/t}</h1></a></li>
				<li class=""><a href="#plugins"	 aria-controls="plugins" role="tab" data-toggle="tab"><h1><i class="fa fa-plug"></i> {t}Plugins{/t}</h1></a></li>
				<li class=""><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><h1><i class="fa fa-cogs"></i> {t}Settings{/t}</h1></a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane fade" role="tabpanel" id="dashboard">
					<div class="col-md-3 col-sm-3 col-xs-6 latest-tracks">
						<h1 class="center"><a href="{$url}gradio/songs" class="ajax-nav">{t}Latest tunes{/t}</a></h1>
						{if isset($isAdmin) && $isAdmin == true}
							<div id="admin-latest-tracks"></div>
						{/if}
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 latest-likes">
						<h1 class="center"><a href="{$url}gradio/ratings" class="ajax-nav">{t}Latest ratings{/t}</a></h1>
						{if isset($isAdmin) && $isAdmin == true}
							<div id="admin-latest-ratings"></div>
						{/if}
					</div>
					<!--
					<div class="col-md-3 col-sm-3 col-xs-6 now-on-air">
						<h1 class="center"><a href="{$url}gradio/onair" class="ajax-nav">{t}Now on air{/t}</a></h1>
						{if isset($isAdmin) && $isAdmin == true}
							<div id="admin-latest-now"></div>
						{/if}
					</div>
					-->
					<div class="col-md-3 col-sm-3 col-xs-6 latest-version">
						<h1 class="center"><a href="{$url}admin" class="ajax-nav">{t}Latest version{/t}</a></h1>
						{if isset($isAdmin) && $isAdmin == true}
							<div id="admin-latest-version"></div>
						{/if}
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 admin-content">
						<h1 class="center"><a href="{$url}admin/content" class="ajax-nav">{t}Content{/t}</a></h1>
						{if isset($isAdmin) && $isAdmin == true}
							<div id="admin-latest-content-block"></div>
						{/if}
					</div>
				</div>

				<div class="tab-pane fade" role="tabpanel" id="content">

					<div class="admin-content">
						{if isset($isAdmin) && $isAdmin == true}
							<div id="admin-latest-content"></div>
						{/if}
					</div>

				</div>

				<div class="tab-pane fade" role="tabpanel" id="users">
					
				</div>

				<div class="tab-pane fade" role="tabpanel" id="settings">

					<div class="accordion" id="left-settings-menu">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#left-settings-menu" href="#">
									  <i class="fa fa-dashboard"></i> Dashboard
								</a>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#left-settings-menu" href="#collapseCache">
									<i class="fa fa-cahe"></i> Cache
								</a>
							</div>
							<div id="collapseCache" class="accordion-body collapse" style="height: 0px; ">
								<div class="accordion-inner">
									<div id="cache" class="cache">
										<div class="panel panel-default">
											<div class="panel-heading">{t}Current cache{/t} <span class="cache-count badge" data-val="0"></span> {t}items{/t}</div>
											<div class="panel-body">
												<form id="clearcacheform" action="{$url}admin/clearcache/" method="post">
													<input type="hidden" name="formid" value="clearcacheform" />
													<div class="smarty-cache"><div class="col-md-6">{t}Smarty cache{/t} <span class="smarty-cache-count badge" data-val="0"></span></div><div		class="col-md-6"><input type="checkbox" class="cache-checkbox" name="smarty" checked="checked"></div></div>
													<div class="sacy-cache"><div class="col-md-6">{t}Sacy cache{/t} <span class="sacy-cache-count badge" data-val="0"></span></div><div class="col-md-6"><input type="checkbox" class="cache-checkbox" name="sacy"></div></div>
													<div class="phpthumb-cache"><div class="col-md-6">{t}phpThumb cache{/t} <span class="phpthumb-cache-count badge" data-val="0"></span></div><div class="col-md-6"><input type="checkbox" class="cache-checkbox" name="phpthumb"></div></div>
													<div class="clearfix"><br /></div>
													<button type="submit" class="btn btn-danger adm-cache-clear">
														<i class="fa fa-eraser"></i> {t}Clear cache{/t}
													</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			
			</div>

		</div>

{/block}

