{block name="body" append}

{function name=menu level=0}
	{foreach $data as $entry}
	<option value="{$entry.id}">{str_repeat("--", $level)} {$entry.name}</option>
	{if is_array($entry.sub_category)}
		{menu data=$entry.sub_category level=$level+1}
	{else}

	{/if}
	{/foreach}
{/function}

<br /><br />
	<h3>{t}Add category{/t}</h3>
			
		<div class="row">
		
				<fieldset>
					<form id="save-form" action="{$url}admin/savecategory/" method="post" class="form-horizontal" role="form">
						<input type="hidden" name="add_content" value="1">
						<input type="hidden" name="content_type" value="category">
						<input type="hidden" name="cat_class" value="0">
						
						<a href="{$url}admin/categories/" class="btn btn-danger ajax-nav">
							<i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
						</a>
						<button type="submit" class="btn btn-success adm-save">
							<i class="fa fa-save"></i> {t}Save{/t}
						</button>
						<br /><br />
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_name">{t}Name{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_name"  id="cat_name" value="" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_alias">{t}Slug{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_alias" id="cat_alias" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputsadala">{t}Category{/t}</label>
							<div class="col-sm-11">
								<select class="form-control" name="cat_parent">
									<option value="0">Parent</option>
								{if isset($categories) }
									{* run the array through the function *}

									{menu data=$categories}

								</select>
								{else}


								{/if}
								<!--<input class="form-control" type="text" name="cat_category" id="cat_category" value="">-->
							</div>
						</div>
						<div class="edit-sat">
							<textarea id="saturs" class="ckeditor" name="cat_content" rows="15" cols="100" style="width: 100%"></textarea>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_imgs">{t}Images{/t}</label>
							<div class="col-sm-11">

								<img class="cat_imgs_add" src="{$url}themes/{$template}/img/default/{$images.defaddcontent}">
								<input class="form-control cat_imgs_added" type="hidden" name="cat_imgs" id="cat_imgs" value="" />
							</div>
						</div>
						
						<!--
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_tags">{t}Tags{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_tags" id="cat_tags" value="" />
							</div>
						</div>
						-->
						
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_order">{t}Order{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_order" id="cat_order" value="" />
							</div>
						</div>
						<a href="{$url}admin/content/" class="btn btn-danger ajax-nav">
							<i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
						</a>
						<button type="submit" class="btn btn-success adm-save">
							<i class="fa fa-save"></i> {t}Save{/t}
						</button>
					</form>
				</fieldset>	

		</div>
{/block}