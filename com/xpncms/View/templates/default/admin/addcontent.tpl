{block name="body" append}

{function name=menu level=0}
	{foreach $data as $entry}
		<option value="{$entry.id}">{str_repeat("--", $level)} {$entry.name}</option>
		{if is_array($entry.sub_category)}
		{menu data=$entry.sub_category level=$level+1}
		{else}

		{/if}
	{/foreach}
{/function}

<br /><br />
	<h3>{t}Add content{/t}</h3>
	
				<div class="row">
					<div class="col-md-12">
					
					<form id="save-form" action="{$url}admin/savecontent/" method="post" class="form-horizontal " role="form">
						<input type="hidden" name="add_content" value="1">
						<input type="hidden" name="content_type" value="content">
						
						<a href="{$url}admin/" class="btn btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
						<button type="submit" class="btn btn-success adm-save"><i class="fa fa-save"></i> {t}Save{/t}</button>
						
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_name">{t}Name{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_name"  id="cont_name" value="" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_alias">{t}Slug{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_alias" id="cont_alias" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputsadala">{t}Category{/t}</label>
							<div class="col-sm-11">
								<select class="form-control" name="cont_category">
									<option value="0">Parent</option>
								{if isset($categories) }
									{* run the array through the function *}

									{menu data=$categories}

								</select>
								{else}


								{/if}
								<!--<input class="form-control" type="text" name="cont_category" id="cont_category" value="">-->
							</div>
						</div>
						<div class="edit-sat">
							<textarea id="saturs" class="ckeditor" name="cont_content" rows="15" cols="100" style="width: 100%"></textarea>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_imgs">{t}Images{/t}</label>
							<div class="col-sm-11">

								<img class="cont_imgs_add" src="{$url}themes/{$template}/img/default/{$images.defaddcontent}">
								<input class="form-control cont_imgs_added" type="hidden" name="cont_imgs" id="cont_imgs" value="" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_tags">{t}Tags{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_tags" id="cont_tags" value="" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cont_order">{t}Order{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cont_order" id="cont_order" value="" />
							</div>
						</div>
						
						<a href="{$url}admin/" class="btn btn-danger ajax-nav"><i class="fa fa-arrow-circle-left"></i> {t}Back{/t}</a>
						<button type="submit" class="btn btn-success adm-save"><i class="fa fa-save"></i> {t}Save{/t}</button>
						
					</form>
					</div>
				</div>
				
{/block}