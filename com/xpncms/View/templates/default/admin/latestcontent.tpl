			<div class="clearfix"></div>
			<div class="content-table">
			<table class="table">
				<thead>
					<tr>
						<th>{t}ID{/t}</th>
						<th>{t}Name{/t}</th>
						<th>{t}Category{/t}</th>
						<th>{t}Images{/t}</th>
					</tr>
				</thead>
				<tbody>
		
		{if isset($contents) }

			{foreach $contents as $content}
			
					<tr>
					<td class="content-contid"><div><a href="{$url}admin/content/{$content.cont_alias}" class="ajax-nav">{$content.cont_id}</a></div></td>
					<td class="content-name"><div>{$content.cont_name}</div></td>
					<td class="content-category"><div>{$content.cont_category}</div></td>
					<td class="content-imgs"><div><img src="{$url}libs/phpThumb/phpThumb.php?q=80&w=40&h=40&zc=1&src=../../{$paths.images}/{$content.cont_imgs}"/></div></td>
					</tr>
				<div class="clearfix"></div>
				
			{/foreach}
			</tbody>
			</table>
			</div>

		{/if}