
{block name="body" append}

{function name=menu selected=0 level=0}
	{foreach $data as $entry}
		{if $entry.id == $selected}
			<option value="{$entry.id}" selected="selected">{str_repeat("--", $level)} {$entry.name}</option>
		{else}
			<option value="{$entry.id}">{str_repeat("--", $level)} {$entry.name}</option>
		{/if}
		{if is_array($entry.sub_category)}
			{menu data=$entry.sub_category selected=$selected level=$level+1}
		{else}
		
		{/if}
	{/foreach}
{/function}

<br /><br />
	<h3>{t}Edit category{/t}</h3>
			
		<div class="row">
		{if isset($category) }
			{foreach $category as $content}
			
				<fieldset>
					<form id="save-form" action="{$url}admin/savecontent/" method="post" class="form-horizontal" role="form">
						<input type="hidden" name="save_content" value="1">
						<input type="hidden" name="content_type" value="category">
						<input type="hidden" name="cat_id" value="{$content.cat_id}">
						
						<a href="{$url}admin/categories/" class="btn btn-danger ajax-nav">
							<i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
						</a>
						<button type="submit" class="btn btn-success adm-save">
							<i class="fa fa-save"></i> {t}Save{/t}
						</button>
						
						<br /><br />
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputsaturaid">{t}ID{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" value="{$content.cat_id}" disabled="disabled" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputnosaukums">{t}Name{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_name"  id="nosaukums" value="{$content.cat_name}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_alias">{t}Slug{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_alias" id="cat_alias" value="{$content.cat_alias}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputsadala">{t}Parent category{/t}</label>
							<div class="col-sm-11">
								<select class="form-control" name="cat_parent">
									<option value="0">Parent</option>
								{if isset($categories) }
									{* run the array through the function *}

									{menu data=$categories selected=$content.cat_parent}

								</select>
								{else}

								{/if}
							</div>
						</div>
						<div class="edit-sat">
							<textarea id="saturs" class="ckeditor" name="cat_content" rows="15" cols="100" style="width: 100%">{$content.cat_content}</textarea>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputdatums">{t}Added{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" value="{$content.cat_added}" disabled="disabled" />
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputbild">{t}Images{/t}</label>
							<div class="col-sm-11">

								<img class="cat_imgs_add" src="{$url}{$paths.images}/{$content.cat_imgs}">

								<input class="form-control cat_imgs_added" type="hidden" name="cat_imgs" id="cat_imgs" value="{$content.cat_imgs}" />

								<!--<input class="form-control" type="text" name="cat_imgs" id="bilde" value="{$content.cat_imgs}" />-->

							</div>
						</div>
						<!--
						<div class="form-group">
							<label class="col-sm-1 control-label" for="cat_tags">{t}Tags{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_tags" id="cat_tags" value="{$content.cat_tags}" />
							</div>
						</div>
						-->
						<div class="form-group">
							<label class="col-sm-1 control-label" for="inputbild">{t}Order{/t}</label>
							<div class="col-sm-11">
								<input class="form-control" type="text" name="cat_order" id="bilde" value="{$content.cat_order}" />
							</div>
						</div>
						<a href="{$url}admin/categories/" class="btn btn-danger ajax-nav">
							<i class="fa fa-arrow-circle-left"></i> {t}Back{/t}
						</a>
						<button type="submit" class="btn btn-success adm-save">
							<i class="fa fa-save"></i> {t}Save{/t}
						</button>
					</form>
				</fieldset>	

			{/foreach}
		{else}
			<h1 class="alert alert-danger">{t}Category not found{/t}</h1>
		{/if}

		</div>
{/block}