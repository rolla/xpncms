{block name="body" append}
			<!--<h1 class="slim-text center">{$title}</h1>-->
			<div id="flash">
				<!--
				<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="100%" height="100%">
				<param name="movie" value="{$url}{$paths.images}/bg/bg.swf" />
				<param name="quality" value="high" />
				<param name="wmode" value="transparent">
				<embed src="{$url}{$paths.images}/bg/bg.swf" quality="high" type="application/x-shockwave-flash" width="100%" height="100%" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" />
				</object>
				-->

			<div class="clearfix"></div>
			<br />
			<div class="row" style="margin-top: 50px;">
				<div class="col-md-6">
					<select class="form-control sort-options">
						<option value="">Noklusētā</option>
						<option value="title">Nosaukums</option>
						<option value="date-created">Izveidots</option>
						<option value="date-updated">Atjaunots</option>
					</select>
				</div>
			</div>
			
			<div class="row grid-container" style="display: block">

				<div class="">
				
					<div id="shuffle-loader"></div>
					<!--<img src="{$url}img/logos/only-g-big-without-gloss-web.png" style="width: 100%" alt="{t}Only G{/t}">-->
					
					<br />
					<div id="shuffle-grid" style="display: none">
					{if isset($contents) }
					{$comments = 1}
					{foreach $contents as $content}
					
					<figure class="content-item col-md-4" data-groups='["{$content.cont_category}"]' data-title="{$content.cont_name}" data-date-created="{$content.cont_added}" data-date-updated="{$content.cont_updated}">
					<a class="ajax-nav" href="{$url}content/{$content.cont_alias}/">
						<div class="content-item__container">
							<img src="{$url}{$paths.images}/{$content.cont_imgs}" class="grayscale" width="100%" height="100%" />
							<div class="content-item__details">
							<figcaption class="content-item__title">{$content.cont_name}</figcaption>
							<p class="content-item__tags"><a class="ajax-nav" href="{$url}category/{$content.cat_alias}/">{$content.cont_category}</a></p>
							</div>
						</div>
					</a>
					</figure>
					
							
							<!--
							<div class="recent-news-block" data-groups="["{$content.cont_category}"]" data-date-created="{$content.cont_added}" >
								<a href="{$url}content/{$content.cont_alias}" data-href="{$url}content/{$content.cont_alias}" class="ajax-nav"><span class="text-center recent-news-block-title">{$content.cont_name}</span></a>
								<div class="recent-news-block-inner">
									<div class="recent-news-block-image" style="background-image: url({$url}libs/phpThumb/?q=100&h=200&zc=1&src=../../{$paths.images}/{$content.cont_imgs})">
										<div class="recent-news-block-content">{$content.cont_content}</div>
										<div class="recent-news-block-mask" style="background-image: url({$url}themes/{$template}/img/default/recent-news-mask.svg)"></div>
										<div class="recent-news-block-date"><i class="fa fa-clock-o"></i> {$content.cont_added|relative_date}</div>
										{if $comments > 1}
										<div class="recent-news-block-comments"><i class="fa fa-comments"></i> {$comments}</div>
										{else}
										<div class="recent-news-block-comments"><i class="fa fa-comment"></i> {$comments}</div>
										{/if}
										<div class="recent-news-block-tag"><i class="fa fa-tag"></i> <a href="{$url}category/{$content.cat_alias}" class="ajax-nav"> {$content.cont_category}</a></div>
									</div>
								</div>
							</div>
							-->
							
					{$comments = $comments+1}
					{/foreach}
					</div>
					{/if}
					
					
				</div>
				
			</div>
			
			</div>
{/block}