{block name="title" append}
:{$page_title}
{/block}

{block name="body" append}
			<!--<a href="admin/get.php?get_content=1&content_type=content" class="btn btn-primary adm-saite">{t}To content{/t}</a>
			<a href="admin/add.php?add_content=1&content_type=category" class="btn btn-success adm-saite">{t}Add site content{/t}</a>-->
			<h3>{t}Next tunes{/t}</h3>
			<div class="notirit"></div>
			<div class="satura-tabula">
			<table class="satura-tabula">
				<thead>
					<tr>
						<th>{t}ID{/t}</th>
						<th>{t}Name{/t}</th>
						<th>{t}Duration{/t}</th>
						<th>{t}Last played{/t}</th>
						<th>{t}Played{/t}</th>
						<th>{t}Picture{/t}</th>
						<th>{t}Options{/t}</th>
					</tr>
				</thead>
				<tbody>
		
		{if isset($songs) }

			{foreach $songs as $song}
			
					<tr>
					<td class="sat-id"><div>{$song.ID}</div></td>
					<td class="sat-sat"><div>{$song.artist} - {$song.title}</div></td>
					<td class="sat-weight"><div>{$song.duration}</div></td>
					<td class="sat-dat"><div>{$song.date_added}</div></td>
					<td class="sat-kat"><div>{$song.count_played}</div></td>
					<td class="sat-bilde"><div><img src="{$base_url}libs/phpThumb/?q=80&w=200&src={$song.picture}&hash={$song.hash}"/></div></td>
					<td class="editing-opt">
					<!--
					<div>
						<a rel="address:admin/edit.php?edit_content='.$song['ID'].'&content_type=category" class="btn adm-saite">{t}Edit{/t}</a> 
						<button href="admin/delete.php?delete_content='.$song['ID'].'&content_type=category" data-confirm="{t}Are you sure you want to delete?{/t} {t}Content with ID{/t}: '.$song['ID'].' {t}and{/t} {t}Name{/t}: '.$song['artist'].' - '.$song['title'].'" class="btn btn-danger adm-del">{t}Delete{/t}</button>
					</div>
					-->
					</td>
					</tr>
				<div class="notirit"></div>
				
			{/foreach}
			</tbody>
			</table>
			</div>

		{/if}

{/block}