{block name="title" append}
:{$page_title}
{/block}

{block name="body" append}
			
			<h3>{t}Recent tunes{/t}</h3>
			<div class="notirit"></div>
			<div class="satura-tabula">
			<table class="satura-tabula">
				<thead>
					<tr>
						<th>{t}ID{/t}</th>
						<th>{t}Name{/t}</th>
						<th>{t}Duration{/t}</th>
						<th>{t}Date{/t}</th>
						<th>{t}Listeners{/t}</th>
						<th>{t}Picture{/t}</th>
						<th>{t}Options{/t}</th>
					</tr>
				</thead>
				<tbody>
		
		{if isset($songs.songs) }

			{foreach $songs.songs as $song}
			
					<tr>
					<td class="sat-id"><div>{$song.ID}</div></td>
					<td class="sat-sat"><div>{$song.artist} - {$song.title}</div></td>
					<td class="sat-weight"><div>{$song.duration}</div></td>
					<td class="sat-dat"><div>{$song.date_played}</div></td>
					<td class="sat-kat"><div>{$song.listeners}</div></td>
					<td class="sat-bilde"><div><img src="{$base_url}libs/phpThumb/phpThumb.php?q=80&w=200&src={$song.picture}&hash={$song.hash}"/></div></td>
					<td class="editing-opt">
					
					</td>
					</tr>
				<div class="notirit"></div>
				
			{/foreach}
			</tbody>
			</table>
			</div>

		{/if}

{/block}