{include file="header.tpl"}
<?php
	// if we got an error then we display it here
	if( $error ){
		echo '<p><h3 style="color:red">Authentication error!</h3>' . $error . '</p>';
		echo "<pre>Session:<br />" . print_r( $_SESSION, true ) . "</pre><hr />";
	}
?> 
{include file="footer.tpl"}