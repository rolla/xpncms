<?php

namespace XpnCMS\Model;

use Exception;
use Core\Helpers\Directory;
use Core\Helpers\Seo;
use Core\Includes\Config;
use Core\Helpers\Files;
use XpnCMS\Model\User;
use XpnCMS\Model\Content;
use XpnCMS\Model\Notify;

class Auth extends Model
{
    private $userId;
    private $provider;

    private $auths;

    const HIDDEN_FIELDS = [
        'full_user_data',
        'first_name',
        'last_name',
        'email',
        'profile_url',
        'user_id',
        'website_url',
    ];

    private function getuserFilesPath()
    {
        return APP_ROOT . Config::get('paths.public') . DS .
            Config::get('paths.files') . DS . 'user_' . $this->userId . DS;
    }

    private function hideFields()
    {
        foreach ($this->auths as $key => &$auth) {
            foreach ($auth as $field => $value) {
                if (in_array($field, self::HIDDEN_FIELDS)) {
                    unset($auth[$field]);
                }
            }
        }
    }

    public function findByProviderUid($provider, $provider_uid)
    {
        global $db1;
        $db1->query('SELECT * FROM '.$db1->db_prefix.'auths WHERE provider=:provider AND provider_uid=:provider_uid AND status=:status LIMIT 1');
        $db1->bind(':provider', $provider);
        $db1->bind(':provider_uid', $provider_uid);
        $db1->bind(':status', 1);

        return $db1->single();
    }

    // public function findByProviderUid($providerUid)
    // {
    //     global $db1;
    //     $db1->query('SELECT * FROM ' . $db1->db_prefix . 'auths WHERE provider_uid=:provider_uid AND status=:status LIMIT 1');
    //     $db1->bind(':provider_uid', $provider_uid);
    //     $db1->bind(':status', 1);
    //
    //     return $db1->single();
    // }

    #$new_user_id, $provider, $provider_uid, $email, $display_name, $first_name, $last_name, $profile_url, $website_url, $full_user_data

    //public function create($user_id, $provider, $provider_uid, $email, $display_name, $first_name, $last_name, $profile_url, $website_url, $full_user_data){
    public function create($arguments = array())
    {
        global $db1;

        foreach ($arguments as $key => $arg) {
            if (!empty($arg)) {
                $db_cols[$key] = $arg;
            }
        }

        /*
        echo '<pre>';
        print_r($db_cols);
        //var_dump($db_cols);
        echo '</pre>';
        */
        //exit();

        $keys = ''.implode(', ', array_keys($db_cols));
        $values = str_repeat('?, ', count($db_cols) - 1).'?';

        $i = 1;
        $db1->query('INSERT INTO '.$db1->db_prefix.'auths ( '.$keys.' ) VALUES ( '.$values.' )');

        foreach ($db_cols as $value) {
            $db1->bind($i++, $value);
        }

        /*
        $db1->query('INSERT INTO '.$db1->db_prefix.'authentications (user_id, provider, provider_uid, email, display_name, first_name, last_name, profile_url, website_url, created_at, full_user_data) VALUES (:user_id, :provider, :provider_uid, :email, :display_name, :first_name, :last_name, :profile_url, :website_url, NOW(), :full_user_data)');

        $db1->bind(':user_id', $user_id);
        $db1->bind(':provider', $provider);
        $db1->bind(':provider_uid', $provider_uid);
        $db1->bind(':email', $email);
        $db1->bind(':display_name', $display_name);
        $db1->bind(':first_name', $first_name);
        $db1->bind(':last_name', $last_name);
        $db1->bind(':profile_url', $profile_url);
        $db1->bind(':website_url', $website_url);
        $db1->bind(':full_user_data', $full_user_data);
        */

        $create = $db1->execute();

        if ($create) {
            return $db1->lastInsertId();
        }

        return false;
    }

    public function updateProvider($arguments, $id)
    {
        global $db1, $debug;

        foreach ($arguments as $key => $val) {
            if (!empty($val)) {
                $db_cols[$key] = $val;
            }
        }

        $set;
        $cnt = count($db_cols);
        $i = 0;
        foreach ($db_cols as $key => $val) {
            if (!empty($val)) {
                $set .= $key.'=:'.$key;
                if ($i == $cnt - 1) { // last
                } else {
                    $set .= ', ';
                }
            }
            ++$i;
        }

        //$query = 'UPDATE '.$db1->db_prefix.'auths SET '.$set.' WHERE id=:id';
        //$debug->display($query);
        //exit;

        $db1->query('UPDATE ' . $db1->db_prefix . 'auths SET ' . $set . ' WHERE id=:id');

        foreach ($db_cols as $key => $val) {
            $db1->bind(':'.$key, $val);
        }

        $db1->bind(':id', $id);

        $update = $db1->execute();

        if ($update) {
            return $id;
        }

        return false;
    }

    /**
     *
     * @param  int    $userId
     * @return
     */
    private function fetchAuths(int $userId)
    {
        global $db1;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'auths WHERE user_id = :user_id LIMIT 10');
        $db1->bind(':user_id', $userId);

        $this->auths = $db1->resultset() ?? [];
    }

    /**
     *
     * @param  int|null  $userId
     * @return array|null
     */
    public function auths(int $userId = null): ?array
    {
        if (!$userId) {
            return null;
        }

        $this->fetchAuths($userId);
        $this->hideFields();

        return $this->auths;
    }

    public function to_archive($user_id, $provider)
    {
        global $db1;
        $db1->query('SELECT * FROM '.$db1->db_prefix.'auths WHERE user_id = :user_id AND provider = :provider LIMIT 1');
        $db1->bind(':user_id', $user_id);
        $db1->bind(':provider', $provider);
        $result = $db1->single();

        $result['id'] = '';
        $result['created_at'] = date_create()->format('Y-m-d H:i:s');

        /*
        echo '<pre>';
        print_r($result);
        //var_dump($db_cols);
        echo '</pre>';
        */

        $keys = ''.implode(', ', array_keys($result));
        $values = str_repeat('?, ', count($result) - 1).'?';

        $i = 1;
        $db1->query('INSERT INTO '.$db1->db_prefix.'auths_archive ( '.$keys.' ) VALUES ( '.$values.' )');

        foreach ($result as $value) {
            $db1->bind($i++, $value);
        }

        $copy = $db1->execute();

        if ($copy) {
            $db1->query('DELETE FROM '.$db1->db_prefix.'auths WHERE user_id = :user_id AND provider = :provider LIMIT 1');
            $db1->bind(':user_id', $user_id);
            $db1->bind(':provider', $provider);
            $deleted = $db1->execute();
        }

        if ($deleted) {
            return true;
        }
    }

    public function createUserFolders($user_id, $folders)
    {
        $directory = new Directory();

        $path = APP_ROOT . Config::get('paths.public') . DS . Config::get('paths.files') . DS . 'user_' . $user_id . DS;

        foreach ($folders as $folder) {
            $directory->mkpath($path . $folder, 0775);

            if ($directory) {
                $result['success'][] = $path . $folder;
            } else {
                throw new Exception('Cannot create directory/s ' . $path . ' check permissons!');
            }
        }

        if ($result['success']) {
            return $result;
        }
    }

    public function saveImage($userId, $provider, $imgUrl)
    {
        $directory = new Directory();
        $seo = new Seo();

        $this->userId = $userId;
        $providerTail = 'images' . DS . $provider . DS;
        $path = $this->getuserFilesPath() . $providerTail;

        //exit($path);

        $directory->mkpath($path, 0775);

        if ($directory) {
            switch ($provider) {
                case 'draugiem':
                    $size = 'large';
                    $imgUrl = $this->setDraugiemImageForSize($imgUrl, $size);
                    break;
                case 'twitter':
                    $imgUrl = str_replace('_normal', '', $imgUrl); // get twitter bigger image
                    // https://twitter.com/beaterdj/profile_image?size=original
                    break;
            }

            if (is_callable('shell_exec') && false === stripos(ini_get('disable_functions'), 'shell_exec')) {
                shell_exec('find ' . $path . ' -type d -exec chmod 0775 {} +');
            }

            $filesHelper = new Files();
            $remoteFile = $filesHelper->getRemoteFile($imgUrl, $path);

            if (is_callable('shell_exec') && false === stripos(ini_get('disable_functions'), 'shell_exec')) {
                shell_exec('find ' . $path .' -type d -exec chmod 0775 {} +');
                shell_exec('find ' . $path .' -type f -exec chmod 0775 {} +');
            }

            $result['path'] = $path;
            $result['storename'] = $providerTail . $remoteFile->file->filename;
            $result['filename'] = $remoteFile->file->filename;

            return $result;
        } else {
            throw new Exception('Cannot create directory/s ' . $path .' check permissons!');
        }
    }

    // helper

    /**
     * Get user profile image URL with different size.
     *
     * @param string $img  User profile image URL from API (default size)
     * @param string $size Desired image size (icon/small/medium/large)
     */
    public function setDraugiemImageForSize($img, $size)
    {
        $sizes = array(
            'icon' => 'i_', //50x50px
            'small' => 'sm_', //100x100px (default)
            'medium' => 'm_', //215px wide
            'large' => 'l_', //710px wide
        );
        if (isset($sizes[$size])) {
            $img = str_replace('/sm_', '/'.$sizes[$size], $img);
        }

        return $img;
    }

    public function matchUserPictures($userId, $provider, $localPic, $remotePic)
    {
        $this->userId = $userId;
        $this->provider = $provider;
        $path = $this->getuserFilesPath();

        if ($provider == 'facebook') {
            $content = file_get_contents($remotePic . '&redirect=false');
            $data = json_decode($content);
            $remotePic = $data->data->url;
        }

        if (is_file($path . $localPic)) {
            $localSize = filesize($path . $localPic);
            $filesHelper = new Files();
            $remoteSize = $filesHelper->getRemoteFileSize($remotePic);

            //echo 'local: '. $localSize;
            //echo 'remote: '. $remoteSize;
            //exit;

            if ($localSize == $remoteSize) {
                return 1;
            }
        }

        return 0;
    }

    public function addProvider($userId, $provider, $userProfile)
    {
        $provider = mb_strtolower($provider);

        $argsProvider = [
            'user_id' => $userId,
            'provider' => $provider,
            'provider_uid' => $userProfile->identifier,
            'first_name' => $userProfile->firstName,
            'last_name' => $userProfile->lastName,
            'display_name' => $userProfile->displayName,
            'email' => $userProfile->email,
            'website_url' => $userProfile->webSiteURL,
            'picture' => $userProfile->photoURL,
            'profile_url' => $userProfile->profileURL,
            'full_user_data' => base64_encode(serialize($userProfile->full_user_data)),
            'created_at' => date_create()->format('Y-m-d H:i:s'),
        ];

        //to safely serialize
        //$safe_string_to_store = base64_encode(serialize($multidimensional_array));

        //to unserialize...
        //$array_restored_from_db = unserialize(base64_decode($encoded_serialized_string));

        $this->create($argsProvider);
    }
}
