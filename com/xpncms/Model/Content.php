<?php

namespace XpnCMS\Model;

use Core\Includes\Config;
use Symfony\Component\HttpFoundation\Session\Session;

class Content extends Model
{
    public $perPage = 4;

    /**
     * @param $id
     * @return mixed
     */
    public function getContent($id)
    {
        global $db1;

        $db1->query('SELECT
                        cont.cont_id,
                        cont.cont_name,
                        cont.cont_content,
                        cont.cont_alias,
                        cont.cont_added,
                        cont.cont_updated,
                        cont.cont_class,
                        cat.cat_name as cont_category,
                        cat.cat_alias as cat_alias,
                        cont.cont_imgs,
                        cont.cont_order
                    FROM '.$db1->db_prefix.'content cont
                    LEFT JOIN '.$db1->db_prefix.'categories cat
                    ON cont.cont_category = cat.cat_id
                    WHERE cont.cont_id = :id
                    LIMIT 0, 1');
        $db1->bind(':id', $id);
        $content = $db1->resultset();
        if ($content) {
            $res['content'][] = array_merge($content[0], $this->getContentRatings($content[0]['cont_id']));
        } else {
            $res['error'] = 'no content added!';
        }

        return $res;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getContentByCategory($id)
    {
        global $db1;

        $db1->query('SELECT
                       cont.cont_id,
                       cont.cont_name,
                       cont.cont_content,
                       cont.cont_tags,
                       cont.cont_alias,
                       cont.cont_added,
                       cont.cont_updated,
                       cont.cont_class,
                       cat.cat_name as cont_category,
                       cat.cat_alias as cat_alias,
                       cont.cont_imgs,
                       cont.cont_order FROM '.$db1->db_prefix.'content cont
                       LEFT JOIN '.$db1->db_prefix.'categories cat
                       ON cont.cont_category = cat.cat_id
                       WHERE cont.cont_category = :id
                       ORDER BY cont.cont_added DESC, cont.cont_order ASC LIMIT 0, 100');
        $db1->bind(':id', $id);
        $content = $db1->resultset();
        if ($content) {
            $res['content'] = $content;
        } else {
            $res['error'] = 'no content added!';
        }

        return $res;
    }

    /**
     * @param string $slug
     * @param int $start
     * @param int $limit
     * @param bool $random
     * @return mixed
     */
    public function getContentByCategorySlug(string $slug, int $start, int $limit, $random = false)
    {
        global $db1;

        $query = $this->baseQueryV2();
        $query .= 'where cat.cat_alias = :slug ';
        if (!$random) {
            $query .= 'order by cont.cont_added desc, cont.cont_order asc limit :start, :limit';
            $db1->query($query);
            $db1->bind(':start', $start);
        }

        if ($random) {
            $query .= 'order by rand() limit :limit';
            $db1->query($query);
        }

        $db1->bind(':slug', $slug);
        $db1->bind(':limit', $limit);

        return $db1->resultset();
    }

    /**
     * @return mixed
     */
    public function getAllCount()
    {
        global $db1;

        $db1->query('SELECT COUNT(*) AS count FROM ' . $db1->db_prefix . 'content');
        $content = $db1->single();

        return $content['count'];
    }

    /**
     * @param int $start
     * @param int $limit
     * @return mixed
     */
    public function getAllContent($start = 0, $limit = 100)
    {
        global $db1;

        $db1->query('SELECT cont.cont_id, cont.cont_name, cont.cont_content, cont.cont_tags, cont.cont_alias, cont.cont_added, cont.cont_updated, cont.cont_class, cat.cat_name as cont_category, cont.cont_imgs, cont.cont_order, cat.cat_alias FROM '.$db1->db_prefix.'content cont LEFT JOIN '.$db1->db_prefix.'categories cat
        ON cont.cont_category = cat.cat_id ORDER BY cont.cont_order ASC, cont.cont_added DESC LIMIT '.$start.', '.$limit.'');
        //$db1->bind(':id', $id);
        $content = $db1->resultset();
        if ($content) {
            $res['content'] = $content;
        } else {
            $res['error'] = 'no content added!';
        }

        return $res;
    }

    /**
     * @return string
     */
    private function baseQueryV2(): string
    {
        global $db1;

        $query = 'select cont.cont_id as id, cont.cont_name as name, ';
        $query .= 'cont.cont_content as body, cont.cont_tags as tags, ';
        $query .= 'cont.cont_alias as slug, cont.cont_added as created_at, ';
        $query .= 'cont.cont_updated as updated_at, cat.cat_name as category, ';
        $query .= 'cont.cont_imgs as image, cont.cont_order as sortorder, cat.cat_alias as category_slug ';
        $query .= 'from ' . $db1->db_prefix . 'content cont left join ' . $db1->db_prefix . 'categories cat ';
        $query .= 'on cont.cont_category = cat.cat_id ';

        return $query;
    }

    /**
     * @param int $start
     * @param int $limit
     * @return mixed
     */
    public function getAllContentV2($start = 0, $limit = 100, $random = false)
    {
        global $db1;

        $query = $this->baseQueryV2();
        if (!$random) {
            $query .= 'order by cont.cont_order ASC, cont.cont_added desc limit :start, :limit ';
            $db1->query($query);
            $db1->bind(':start', $start);
        }

        if ($random) {
            $query .= 'order by rand() limit :limit';
            $db1->query($query);
        }

        $db1->bind(':limit', $limit);

        return $db1->resultset();
    }

    /**
     * @param string $alias
     * @return array
     */
    public function getContentByAliasV2(string $alias)
    {
        global $db1;

        $query = $this->baseQueryV2();
        $query .= 'where cont.cont_alias = :alias order by cont.cont_order ASC, cont.cont_added limit 0, 1';

        $db1->query($query);
        $db1->bind(':alias', $alias);

        $content = $db1->single();

        return array_merge($this->getContentRatings($content['id']), $content);
    }

    /**
     * @param $post
     * @return array
     * @throws \Exception
     */
    public function getPostMeta($post): array
    {
        $pageTitle = _('Post') . ' : ' . $post['name'];
        $contentText = strip_tags(html_entity_decode($post['body']));
        $ogUrl = str_replace('https', 'http', Config::get('base.fbsiteurl')) . '/content/' . $post['slug'] . '/'; // without https to work fb comments
        $ogimage = urldecode(Config::get('base.fbsiteurl'). '/' . Config::get('paths.images') . '/'. $post['image']);

        $meta = [
            'pageTitle' => $pageTitle,
            'content' => $contentText,
            'ogdescription' => $contentText,
            'ogurl' => $ogUrl,
            'ogimage' => $ogimage,
            'ogtitle' => $pageTitle,
            'ogtype' => 'article',
            'twittertitle' => $pageTitle,
            'twitterdescription' => $contentText,
            'twitterimage' => $ogimage,
        ];

        return $meta;
    }

    /**
     * @param $alias
     * @return mixed
     */
    public function getContentByAlias($alias)
    {
        global $db1;

        $db1->query('SELECT cont.cont_id, cont.cont_name, cont.cont_content, cont.cont_alias, cont.cont_added, cont.cont_updated, cont.cont_class, cat.cat_name as cont_category, cont.cont_imgs, cont.cont_order, cat.cat_alias FROM '.$db1->db_prefix.'content cont LEFT JOIN '.$db1->db_prefix.'categories cat
ON cont.cont_category = cat.cat_id WHERE cont.cont_alias = :alias LIMIT 0, 1');
        $db1->bind(':alias', $alias);
        $content = $db1->resultset();
        if ($content) {
            $res['content'][] = array_merge($this->getContentRatings($content[0]['cont_id']), $content[0]);
        } else {
            $res['error'] = 'no content added!';
        }

        return $res;
    }

    /**
     * TODO need to optimize query using group by
     * @param $id
     * @return mixed
     */
    public function getContentRatings($id)
    {
        global $db1;

        $res['content']['likes'] = null;
        $res['content']['dislikes'] = null;

        // subquery
        /*
        $query = 'SELECT COUNT(*) as dislikes,
            (SELECT COUNT(*)
                FROM '.$db1->db_prefix.'action_category
                WHERE user_id=:user_id AND action_id=ac.action_id AND item_id=ac.item_id AND deleted_at IS NULL) as disliked
            FROM '.$db1->db_prefix.'content cont
                LEFT JOIN '.$db1->db_prefix.'action_category ac
                ON cont.cont_id = ac.item_id
                LEFT JOIN '.$db1->db_prefix.'actions a
                ON ac.action_id = a.id
                WHERE cont.cont_id=:id AND a.action=:action AND ac.deleted_at IS NULL';
        $db1->query($query);
        $db1->bind(':id', $id);
        $db1->bind(':user_id', $currentUser);
        $db1->bind(':action', 'disliked');
        $content = $db1->single();
        if ($content) {
            $res['content']['dislikes'] = $content['dislikes'];
            $res['content']['disliked'] = $content['disliked'];
        } else {
            $res['error'] = 'no content added!';
        }
        */

        $query = 'SELECT ac.*, u.first_name, u.last_name, u.display_name, u.profile_url, u.photo_url, c.cont_id
            FROM '.$db1->db_prefix.'action_category ac
                LEFT JOIN '.$db1->db_prefix.'actions a
                ON ac.action_id = a.id
                LEFT JOIN '.$db1->db_prefix.'users u
                ON ac.user_id = u.id
                LEFT JOIN '.$db1->db_prefix.'content c
                ON ac.item_id = c.cont_id
                WHERE ac.item_id IS NOT NULL AND ac.item_id=:id AND a.action=:action AND ac.deleted_at IS NULL';

        $db1->query($query);
        $db1->bind(':id', $id);
        $db1->bind(':action', 'liked');
        $content = $db1->resultset();
        if ($content) {
            $res['content']['likes'] = $content;
        }

        $query = 'SELECT ac.*, u.first_name, u.last_name, u.display_name, u.profile_url, u.photo_url, c.cont_id
            FROM '.$db1->db_prefix.'action_category ac
                LEFT JOIN '.$db1->db_prefix.'actions a
                ON ac.action_id = a.id
                LEFT JOIN '.$db1->db_prefix.'users u
                ON ac.user_id = u.id
                LEFT JOIN '.$db1->db_prefix.'content c
                ON ac.item_id = c.cont_id
                WHERE ac.item_id IS NOT NULL AND ac.item_id=:id AND a.action=:action AND ac.deleted_at IS NULL';
        $db1->query($query);
        $db1->bind(':id', $id);
        $db1->bind(':action', 'disliked');
        $content = $db1->resultset();
        if ($content) {
            $res['content']['dislikes'] = $content;
        }

        return $res['content'];
    }
}
