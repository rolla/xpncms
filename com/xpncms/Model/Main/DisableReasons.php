<?php

namespace Model\Main;

use Model\Main\Base\DisableReasons as BaseDisableReasons;

/**
 * Skeleton subclass for representing a row from the 'disable_reasons' table.
 *
 * Song disable reasons
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DisableReasons extends BaseDisableReasons
{

}
