<?php

namespace Model\Main;

use Model\Main\Base\GradioUserActionCategoryQuery as BaseGradioUserActionCategoryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'gradio_user_action_category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GradioUserActionCategoryQuery extends BaseGradioUserActionCategoryQuery
{

}
