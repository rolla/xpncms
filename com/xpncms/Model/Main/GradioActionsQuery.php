<?php

namespace Model\Main;

use Model\Main\Base\GradioActionsQuery as BaseGradioActionsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'gradio_actions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GradioActionsQuery extends BaseGradioActionsQuery
{

}
