<?php

namespace Model\Main\Map;

use Model\Main\GradioUserActionCategory;
use Model\Main\GradioUserActionCategoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_user_action_category' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioUserActionCategoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioUserActionCategoryTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_user_action_category';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioUserActionCategory';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioUserActionCategory';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_user_action_category.id';

    /**
     * the column name for the action_id field
     */
    const COL_ACTION_ID = 'gradio_user_action_category.action_id';

    /**
     * the column name for the category_id field
     */
    const COL_CATEGORY_ID = 'gradio_user_action_category.category_id';

    /**
     * the column name for the from_user_id field
     */
    const COL_FROM_USER_ID = 'gradio_user_action_category.from_user_id';

    /**
     * the column name for the to_user_id field
     */
    const COL_TO_USER_ID = 'gradio_user_action_category.to_user_id';

    /**
     * the column name for the item_id field
     */
    const COL_ITEM_ID = 'gradio_user_action_category.item_id';

    /**
     * the column name for the item_ids field
     */
    const COL_ITEM_IDS = 'gradio_user_action_category.item_ids';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'gradio_user_action_category.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'gradio_user_action_category.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'gradio_user_action_category.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ActionId', 'CategoryId', 'FromUserId', 'ToUserId', 'ItemId', 'ItemIds', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'actionId', 'categoryId', 'fromUserId', 'toUserId', 'itemId', 'itemIds', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(GradioUserActionCategoryTableMap::COL_ID, GradioUserActionCategoryTableMap::COL_ACTION_ID, GradioUserActionCategoryTableMap::COL_CATEGORY_ID, GradioUserActionCategoryTableMap::COL_FROM_USER_ID, GradioUserActionCategoryTableMap::COL_TO_USER_ID, GradioUserActionCategoryTableMap::COL_ITEM_ID, GradioUserActionCategoryTableMap::COL_ITEM_IDS, GradioUserActionCategoryTableMap::COL_CREATED_AT, GradioUserActionCategoryTableMap::COL_UPDATED_AT, GradioUserActionCategoryTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'action_id', 'category_id', 'from_user_id', 'to_user_id', 'item_id', 'item_ids', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ActionId' => 1, 'CategoryId' => 2, 'FromUserId' => 3, 'ToUserId' => 4, 'ItemId' => 5, 'ItemIds' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, 'DeletedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'actionId' => 1, 'categoryId' => 2, 'fromUserId' => 3, 'toUserId' => 4, 'itemId' => 5, 'itemIds' => 6, 'createdAt' => 7, 'updatedAt' => 8, 'deletedAt' => 9, ),
        self::TYPE_COLNAME       => array(GradioUserActionCategoryTableMap::COL_ID => 0, GradioUserActionCategoryTableMap::COL_ACTION_ID => 1, GradioUserActionCategoryTableMap::COL_CATEGORY_ID => 2, GradioUserActionCategoryTableMap::COL_FROM_USER_ID => 3, GradioUserActionCategoryTableMap::COL_TO_USER_ID => 4, GradioUserActionCategoryTableMap::COL_ITEM_ID => 5, GradioUserActionCategoryTableMap::COL_ITEM_IDS => 6, GradioUserActionCategoryTableMap::COL_CREATED_AT => 7, GradioUserActionCategoryTableMap::COL_UPDATED_AT => 8, GradioUserActionCategoryTableMap::COL_DELETED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'action_id' => 1, 'category_id' => 2, 'from_user_id' => 3, 'to_user_id' => 4, 'item_id' => 5, 'item_ids' => 6, 'created_at' => 7, 'updated_at' => 8, 'deleted_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_user_action_category');
        $this->setPhpName('GradioUserActionCategory');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioUserActionCategory');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('action_id', 'ActionId', 'INTEGER', true, null, null);
        $this->addColumn('category_id', 'CategoryId', 'INTEGER', true, null, null);
        $this->addColumn('from_user_id', 'FromUserId', 'INTEGER', false, null, null);
        $this->addColumn('to_user_id', 'ToUserId', 'INTEGER', true, null, null);
        $this->addColumn('item_id', 'ItemId', 'INTEGER', false, null, null);
        $this->addColumn('item_ids', 'ItemIds', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioUserActionCategoryTableMap::CLASS_DEFAULT : GradioUserActionCategoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioUserActionCategory object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioUserActionCategoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioUserActionCategoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioUserActionCategoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioUserActionCategoryTableMap::OM_CLASS;
            /** @var GradioUserActionCategory $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioUserActionCategoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioUserActionCategoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioUserActionCategoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioUserActionCategory $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioUserActionCategoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_ID);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_ACTION_ID);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_CATEGORY_ID);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_FROM_USER_ID);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_TO_USER_ID);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_ITEM_ID);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_ITEM_IDS);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(GradioUserActionCategoryTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.action_id');
            $criteria->addSelectColumn($alias . '.category_id');
            $criteria->addSelectColumn($alias . '.from_user_id');
            $criteria->addSelectColumn($alias . '.to_user_id');
            $criteria->addSelectColumn($alias . '.item_id');
            $criteria->addSelectColumn($alias . '.item_ids');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioUserActionCategoryTableMap::DATABASE_NAME)->getTable(GradioUserActionCategoryTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioUserActionCategoryTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioUserActionCategoryTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioUserActionCategoryTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioUserActionCategory or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioUserActionCategory object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserActionCategoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioUserActionCategory) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioUserActionCategoryTableMap::DATABASE_NAME);
            $criteria->add(GradioUserActionCategoryTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioUserActionCategoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioUserActionCategoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioUserActionCategoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_user_action_category table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioUserActionCategoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioUserActionCategory or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioUserActionCategory object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserActionCategoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioUserActionCategory object
        }

        if ($criteria->containsKey(GradioUserActionCategoryTableMap::COL_ID) && $criteria->keyContainsValue(GradioUserActionCategoryTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioUserActionCategoryTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioUserActionCategoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioUserActionCategoryTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioUserActionCategoryTableMap::buildTableMap();
