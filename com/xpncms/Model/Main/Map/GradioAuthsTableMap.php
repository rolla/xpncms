<?php

namespace Model\Main\Map;

use Model\Main\GradioAuths;
use Model\Main\GradioAuthsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_auths' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioAuthsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioAuthsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_auths';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioAuths';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioAuths';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_auths.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'gradio_auths.user_id';

    /**
     * the column name for the provider field
     */
    const COL_PROVIDER = 'gradio_auths.provider';

    /**
     * the column name for the provider_uid field
     */
    const COL_PROVIDER_UID = 'gradio_auths.provider_uid';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'gradio_auths.email';

    /**
     * the column name for the display_name field
     */
    const COL_DISPLAY_NAME = 'gradio_auths.display_name';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'gradio_auths.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'gradio_auths.last_name';

    /**
     * the column name for the profile_url field
     */
    const COL_PROFILE_URL = 'gradio_auths.profile_url';

    /**
     * the column name for the picture field
     */
    const COL_PICTURE = 'gradio_auths.picture';

    /**
     * the column name for the website_url field
     */
    const COL_WEBSITE_URL = 'gradio_auths.website_url';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'gradio_auths.created_at';

    /**
     * the column name for the full_user_data field
     */
    const COL_FULL_USER_DATA = 'gradio_auths.full_user_data';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'gradio_auths.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'Provider', 'ProviderUid', 'Email', 'DisplayName', 'FirstName', 'LastName', 'ProfileUrl', 'Picture', 'WebsiteUrl', 'CreatedAt', 'FullUserData', 'Status', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'provider', 'providerUid', 'email', 'displayName', 'firstName', 'lastName', 'profileUrl', 'picture', 'websiteUrl', 'createdAt', 'fullUserData', 'status', ),
        self::TYPE_COLNAME       => array(GradioAuthsTableMap::COL_ID, GradioAuthsTableMap::COL_USER_ID, GradioAuthsTableMap::COL_PROVIDER, GradioAuthsTableMap::COL_PROVIDER_UID, GradioAuthsTableMap::COL_EMAIL, GradioAuthsTableMap::COL_DISPLAY_NAME, GradioAuthsTableMap::COL_FIRST_NAME, GradioAuthsTableMap::COL_LAST_NAME, GradioAuthsTableMap::COL_PROFILE_URL, GradioAuthsTableMap::COL_PICTURE, GradioAuthsTableMap::COL_WEBSITE_URL, GradioAuthsTableMap::COL_CREATED_AT, GradioAuthsTableMap::COL_FULL_USER_DATA, GradioAuthsTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'provider', 'provider_uid', 'email', 'display_name', 'first_name', 'last_name', 'profile_url', 'picture', 'website_url', 'created_at', 'full_user_data', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'Provider' => 2, 'ProviderUid' => 3, 'Email' => 4, 'DisplayName' => 5, 'FirstName' => 6, 'LastName' => 7, 'ProfileUrl' => 8, 'Picture' => 9, 'WebsiteUrl' => 10, 'CreatedAt' => 11, 'FullUserData' => 12, 'Status' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'provider' => 2, 'providerUid' => 3, 'email' => 4, 'displayName' => 5, 'firstName' => 6, 'lastName' => 7, 'profileUrl' => 8, 'picture' => 9, 'websiteUrl' => 10, 'createdAt' => 11, 'fullUserData' => 12, 'status' => 13, ),
        self::TYPE_COLNAME       => array(GradioAuthsTableMap::COL_ID => 0, GradioAuthsTableMap::COL_USER_ID => 1, GradioAuthsTableMap::COL_PROVIDER => 2, GradioAuthsTableMap::COL_PROVIDER_UID => 3, GradioAuthsTableMap::COL_EMAIL => 4, GradioAuthsTableMap::COL_DISPLAY_NAME => 5, GradioAuthsTableMap::COL_FIRST_NAME => 6, GradioAuthsTableMap::COL_LAST_NAME => 7, GradioAuthsTableMap::COL_PROFILE_URL => 8, GradioAuthsTableMap::COL_PICTURE => 9, GradioAuthsTableMap::COL_WEBSITE_URL => 10, GradioAuthsTableMap::COL_CREATED_AT => 11, GradioAuthsTableMap::COL_FULL_USER_DATA => 12, GradioAuthsTableMap::COL_STATUS => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'provider' => 2, 'provider_uid' => 3, 'email' => 4, 'display_name' => 5, 'first_name' => 6, 'last_name' => 7, 'profile_url' => 8, 'picture' => 9, 'website_url' => 10, 'created_at' => 11, 'full_user_data' => 12, 'status' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_auths');
        $this->setPhpName('GradioAuths');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioAuths');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', true, null, null);
        $this->addColumn('provider', 'Provider', 'VARCHAR', true, 100, null);
        $this->addColumn('provider_uid', 'ProviderUid', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 200, null);
        $this->addColumn('display_name', 'DisplayName', 'VARCHAR', true, 150, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 100, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', true, 100, null);
        $this->addColumn('profile_url', 'ProfileUrl', 'VARCHAR', true, 300, null);
        $this->addColumn('picture', 'Picture', 'VARCHAR', false, 255, null);
        $this->addColumn('website_url', 'WebsiteUrl', 'VARCHAR', true, 300, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('full_user_data', 'FullUserData', 'LONGVARCHAR', true, null, null);
        $this->addColumn('status', 'Status', 'BOOLEAN', true, 1, true);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioAuthsTableMap::CLASS_DEFAULT : GradioAuthsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioAuths object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioAuthsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioAuthsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioAuthsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioAuthsTableMap::OM_CLASS;
            /** @var GradioAuths $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioAuthsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioAuthsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioAuthsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioAuths $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioAuthsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_ID);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_USER_ID);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_PROVIDER);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_PROVIDER_UID);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_EMAIL);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_DISPLAY_NAME);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_PROFILE_URL);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_PICTURE);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_WEBSITE_URL);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_FULL_USER_DATA);
            $criteria->addSelectColumn(GradioAuthsTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.provider');
            $criteria->addSelectColumn($alias . '.provider_uid');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.display_name');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.profile_url');
            $criteria->addSelectColumn($alias . '.picture');
            $criteria->addSelectColumn($alias . '.website_url');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.full_user_data');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioAuthsTableMap::DATABASE_NAME)->getTable(GradioAuthsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioAuthsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioAuthsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioAuthsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioAuths or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioAuths object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioAuthsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioAuths) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioAuthsTableMap::DATABASE_NAME);
            $criteria->add(GradioAuthsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioAuthsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioAuthsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioAuthsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_auths table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioAuthsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioAuths or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioAuths object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioAuthsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioAuths object
        }

        if ($criteria->containsKey(GradioAuthsTableMap::COL_ID) && $criteria->keyContainsValue(GradioAuthsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioAuthsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioAuthsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioAuthsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioAuthsTableMap::buildTableMap();
