<?php

namespace Model\Main\Map;

use Model\Main\GradioShows;
use Model\Main\GradioShowsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_shows' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioShowsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioShowsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_shows';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioShows';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioShows';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_shows.id';

    /**
     * the column name for the artist field
     */
    const COL_ARTIST = 'gradio_shows.artist';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'gradio_shows.title';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'gradio_shows.description';

    /**
     * the column name for the image field
     */
    const COL_IMAGE = 'gradio_shows.image';

    /**
     * the column name for the alias field
     */
    const COL_ALIAS = 'gradio_shows.alias';

    /**
     * the column name for the moderator_user_id field
     */
    const COL_MODERATOR_USER_ID = 'gradio_shows.moderator_user_id';

    /**
     * the column name for the director_user_id field
     */
    const COL_DIRECTOR_USER_ID = 'gradio_shows.director_user_id';

    /**
     * the column name for the plays_count field
     */
    const COL_PLAYS_COUNT = 'gradio_shows.plays_count';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'gradio_shows.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'gradio_shows.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'gradio_shows.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Artist', 'Title', 'Description', 'Image', 'Alias', 'ModeratorUserId', 'DirectorUserId', 'PlaysCount', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'artist', 'title', 'description', 'image', 'alias', 'moderatorUserId', 'directorUserId', 'playsCount', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(GradioShowsTableMap::COL_ID, GradioShowsTableMap::COL_ARTIST, GradioShowsTableMap::COL_TITLE, GradioShowsTableMap::COL_DESCRIPTION, GradioShowsTableMap::COL_IMAGE, GradioShowsTableMap::COL_ALIAS, GradioShowsTableMap::COL_MODERATOR_USER_ID, GradioShowsTableMap::COL_DIRECTOR_USER_ID, GradioShowsTableMap::COL_PLAYS_COUNT, GradioShowsTableMap::COL_CREATED_AT, GradioShowsTableMap::COL_UPDATED_AT, GradioShowsTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'artist', 'title', 'description', 'image', 'alias', 'moderator_user_id', 'director_user_id', 'plays_count', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Artist' => 1, 'Title' => 2, 'Description' => 3, 'Image' => 4, 'Alias' => 5, 'ModeratorUserId' => 6, 'DirectorUserId' => 7, 'PlaysCount' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, 'DeletedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'artist' => 1, 'title' => 2, 'description' => 3, 'image' => 4, 'alias' => 5, 'moderatorUserId' => 6, 'directorUserId' => 7, 'playsCount' => 8, 'createdAt' => 9, 'updatedAt' => 10, 'deletedAt' => 11, ),
        self::TYPE_COLNAME       => array(GradioShowsTableMap::COL_ID => 0, GradioShowsTableMap::COL_ARTIST => 1, GradioShowsTableMap::COL_TITLE => 2, GradioShowsTableMap::COL_DESCRIPTION => 3, GradioShowsTableMap::COL_IMAGE => 4, GradioShowsTableMap::COL_ALIAS => 5, GradioShowsTableMap::COL_MODERATOR_USER_ID => 6, GradioShowsTableMap::COL_DIRECTOR_USER_ID => 7, GradioShowsTableMap::COL_PLAYS_COUNT => 8, GradioShowsTableMap::COL_CREATED_AT => 9, GradioShowsTableMap::COL_UPDATED_AT => 10, GradioShowsTableMap::COL_DELETED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'artist' => 1, 'title' => 2, 'description' => 3, 'image' => 4, 'alias' => 5, 'moderator_user_id' => 6, 'director_user_id' => 7, 'plays_count' => 8, 'created_at' => 9, 'updated_at' => 10, 'deleted_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_shows');
        $this->setPhpName('GradioShows');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioShows');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('artist', 'Artist', 'VARCHAR', false, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('image', 'Image', 'LONGVARCHAR', false, null, null);
        $this->addColumn('alias', 'Alias', 'VARCHAR', false, 255, null);
        $this->addColumn('moderator_user_id', 'ModeratorUserId', 'INTEGER', false, null, null);
        $this->addColumn('director_user_id', 'DirectorUserId', 'INTEGER', false, null, null);
        $this->addColumn('plays_count', 'PlaysCount', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioShowsTableMap::CLASS_DEFAULT : GradioShowsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioShows object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioShowsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioShowsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioShowsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioShowsTableMap::OM_CLASS;
            /** @var GradioShows $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioShowsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioShowsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioShowsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioShows $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioShowsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioShowsTableMap::COL_ID);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_ARTIST);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_TITLE);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_IMAGE);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_ALIAS);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_MODERATOR_USER_ID);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_DIRECTOR_USER_ID);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_PLAYS_COUNT);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(GradioShowsTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.artist');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.image');
            $criteria->addSelectColumn($alias . '.alias');
            $criteria->addSelectColumn($alias . '.moderator_user_id');
            $criteria->addSelectColumn($alias . '.director_user_id');
            $criteria->addSelectColumn($alias . '.plays_count');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioShowsTableMap::DATABASE_NAME)->getTable(GradioShowsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioShowsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioShowsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioShowsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioShows or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioShows object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioShowsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioShows) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioShowsTableMap::DATABASE_NAME);
            $criteria->add(GradioShowsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioShowsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioShowsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioShowsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_shows table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioShowsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioShows or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioShows object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioShowsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioShows object
        }

        if ($criteria->containsKey(GradioShowsTableMap::COL_ID) && $criteria->keyContainsValue(GradioShowsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioShowsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioShowsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioShowsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioShowsTableMap::buildTableMap();
