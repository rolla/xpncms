<?php

namespace Model\Main\Map;

use Model\Main\GradioUserclasses;
use Model\Main\GradioUserclassesQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_userclasses' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioUserclassesTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioUserclassesTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_userclasses';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioUserclasses';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioUserclasses';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the userclass_id field
     */
    const COL_USERCLASS_ID = 'gradio_userclasses.userclass_id';

    /**
     * the column name for the userclass_name field
     */
    const COL_USERCLASS_NAME = 'gradio_userclasses.userclass_name';

    /**
     * the column name for the userclass_description field
     */
    const COL_USERCLASS_DESCRIPTION = 'gradio_userclasses.userclass_description';

    /**
     * the column name for the userclass_editclass field
     */
    const COL_USERCLASS_EDITCLASS = 'gradio_userclasses.userclass_editclass';

    /**
     * the column name for the userclass_parent field
     */
    const COL_USERCLASS_PARENT = 'gradio_userclasses.userclass_parent';

    /**
     * the column name for the userclass_accum field
     */
    const COL_USERCLASS_ACCUM = 'gradio_userclasses.userclass_accum';

    /**
     * the column name for the userclass_visibility field
     */
    const COL_USERCLASS_VISIBILITY = 'gradio_userclasses.userclass_visibility';

    /**
     * the column name for the userclass_type field
     */
    const COL_USERCLASS_TYPE = 'gradio_userclasses.userclass_type';

    /**
     * the column name for the userclass_icon field
     */
    const COL_USERCLASS_ICON = 'gradio_userclasses.userclass_icon';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('UserclassId', 'UserclassName', 'UserclassDescription', 'UserclassEditclass', 'UserclassParent', 'UserclassAccum', 'UserclassVisibility', 'UserclassType', 'UserclassIcon', ),
        self::TYPE_CAMELNAME     => array('userclassId', 'userclassName', 'userclassDescription', 'userclassEditclass', 'userclassParent', 'userclassAccum', 'userclassVisibility', 'userclassType', 'userclassIcon', ),
        self::TYPE_COLNAME       => array(GradioUserclassesTableMap::COL_USERCLASS_ID, GradioUserclassesTableMap::COL_USERCLASS_NAME, GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION, GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS, GradioUserclassesTableMap::COL_USERCLASS_PARENT, GradioUserclassesTableMap::COL_USERCLASS_ACCUM, GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY, GradioUserclassesTableMap::COL_USERCLASS_TYPE, GradioUserclassesTableMap::COL_USERCLASS_ICON, ),
        self::TYPE_FIELDNAME     => array('userclass_id', 'userclass_name', 'userclass_description', 'userclass_editclass', 'userclass_parent', 'userclass_accum', 'userclass_visibility', 'userclass_type', 'userclass_icon', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('UserclassId' => 0, 'UserclassName' => 1, 'UserclassDescription' => 2, 'UserclassEditclass' => 3, 'UserclassParent' => 4, 'UserclassAccum' => 5, 'UserclassVisibility' => 6, 'UserclassType' => 7, 'UserclassIcon' => 8, ),
        self::TYPE_CAMELNAME     => array('userclassId' => 0, 'userclassName' => 1, 'userclassDescription' => 2, 'userclassEditclass' => 3, 'userclassParent' => 4, 'userclassAccum' => 5, 'userclassVisibility' => 6, 'userclassType' => 7, 'userclassIcon' => 8, ),
        self::TYPE_COLNAME       => array(GradioUserclassesTableMap::COL_USERCLASS_ID => 0, GradioUserclassesTableMap::COL_USERCLASS_NAME => 1, GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION => 2, GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS => 3, GradioUserclassesTableMap::COL_USERCLASS_PARENT => 4, GradioUserclassesTableMap::COL_USERCLASS_ACCUM => 5, GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY => 6, GradioUserclassesTableMap::COL_USERCLASS_TYPE => 7, GradioUserclassesTableMap::COL_USERCLASS_ICON => 8, ),
        self::TYPE_FIELDNAME     => array('userclass_id' => 0, 'userclass_name' => 1, 'userclass_description' => 2, 'userclass_editclass' => 3, 'userclass_parent' => 4, 'userclass_accum' => 5, 'userclass_visibility' => 6, 'userclass_type' => 7, 'userclass_icon' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_userclasses');
        $this->setPhpName('GradioUserclasses');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioUserclasses');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('userclass_id', 'UserclassId', 'SMALLINT', true, 5, 0);
        $this->addColumn('userclass_name', 'UserclassName', 'VARCHAR', true, 100, '');
        $this->addColumn('userclass_description', 'UserclassDescription', 'VARCHAR', true, 250, '');
        $this->addColumn('userclass_editclass', 'UserclassEditclass', 'SMALLINT', true, 5, 0);
        $this->addColumn('userclass_parent', 'UserclassParent', 'SMALLINT', true, 5, 0);
        $this->addColumn('userclass_accum', 'UserclassAccum', 'VARCHAR', true, 250, '');
        $this->addColumn('userclass_visibility', 'UserclassVisibility', 'SMALLINT', true, 5, 0);
        $this->addColumn('userclass_type', 'UserclassType', 'BOOLEAN', true, 1, false);
        $this->addColumn('userclass_icon', 'UserclassIcon', 'VARCHAR', true, 250, '');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioUserclassesTableMap::CLASS_DEFAULT : GradioUserclassesTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioUserclasses object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioUserclassesTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioUserclassesTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioUserclassesTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioUserclassesTableMap::OM_CLASS;
            /** @var GradioUserclasses $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioUserclassesTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioUserclassesTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioUserclassesTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioUserclasses $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioUserclassesTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_ID);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_NAME);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_PARENT);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_ACCUM);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_TYPE);
            $criteria->addSelectColumn(GradioUserclassesTableMap::COL_USERCLASS_ICON);
        } else {
            $criteria->addSelectColumn($alias . '.userclass_id');
            $criteria->addSelectColumn($alias . '.userclass_name');
            $criteria->addSelectColumn($alias . '.userclass_description');
            $criteria->addSelectColumn($alias . '.userclass_editclass');
            $criteria->addSelectColumn($alias . '.userclass_parent');
            $criteria->addSelectColumn($alias . '.userclass_accum');
            $criteria->addSelectColumn($alias . '.userclass_visibility');
            $criteria->addSelectColumn($alias . '.userclass_type');
            $criteria->addSelectColumn($alias . '.userclass_icon');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioUserclassesTableMap::DATABASE_NAME)->getTable(GradioUserclassesTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioUserclassesTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioUserclassesTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioUserclassesTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioUserclasses or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioUserclasses object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioUserclasses) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioUserclassesTableMap::DATABASE_NAME);
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_ID, (array) $values, Criteria::IN);
        }

        $query = GradioUserclassesQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioUserclassesTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioUserclassesTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_userclasses table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioUserclassesQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioUserclasses or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioUserclasses object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioUserclasses object
        }


        // Set the correct dbName
        $query = GradioUserclassesQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioUserclassesTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioUserclassesTableMap::buildTableMap();
