<?php

namespace Model\Main\Map;

use Model\Main\GradioSessions;
use Model\Main\GradioSessionsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_sessions' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioSessionsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioSessionsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_sessions';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioSessions';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioSessions';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_sessions.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'gradio_sessions.user_id';

    /**
     * the column name for the vuser_id field
     */
    const COL_VUSER_ID = 'gradio_sessions.vuser_id';

    /**
     * the column name for the ip field
     */
    const COL_IP = 'gradio_sessions.ip';

    /**
     * the column name for the visits field
     */
    const COL_VISITS = 'gradio_sessions.visits';

    /**
     * the column name for the till_refresh field
     */
    const COL_TILL_REFRESH = 'gradio_sessions.till_refresh';

    /**
     * the column name for the clicks field
     */
    const COL_CLICKS = 'gradio_sessions.clicks';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'gradio_sessions.status';

    /**
     * the column name for the actions_timestamp field
     */
    const COL_ACTIONS_TIMESTAMP = 'gradio_sessions.actions_timestamp';

    /**
     * the column name for the currentvisit field
     */
    const COL_CURRENTVISIT = 'gradio_sessions.currentvisit';

    /**
     * the column name for the lastvisit field
     */
    const COL_LASTVISIT = 'gradio_sessions.lastvisit';

    /**
     * the column name for the added field
     */
    const COL_ADDED = 'gradio_sessions.added';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'VuserId', 'Ip', 'Visits', 'TillRefresh', 'Clicks', 'Status', 'ActionsTimestamp', 'Currentvisit', 'Lastvisit', 'Added', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'vuserId', 'ip', 'visits', 'tillRefresh', 'clicks', 'status', 'actionsTimestamp', 'currentvisit', 'lastvisit', 'added', ),
        self::TYPE_COLNAME       => array(GradioSessionsTableMap::COL_ID, GradioSessionsTableMap::COL_USER_ID, GradioSessionsTableMap::COL_VUSER_ID, GradioSessionsTableMap::COL_IP, GradioSessionsTableMap::COL_VISITS, GradioSessionsTableMap::COL_TILL_REFRESH, GradioSessionsTableMap::COL_CLICKS, GradioSessionsTableMap::COL_STATUS, GradioSessionsTableMap::COL_ACTIONS_TIMESTAMP, GradioSessionsTableMap::COL_CURRENTVISIT, GradioSessionsTableMap::COL_LASTVISIT, GradioSessionsTableMap::COL_ADDED, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'vuser_id', 'ip', 'visits', 'till_refresh', 'clicks', 'status', 'actions_timestamp', 'currentvisit', 'lastvisit', 'added', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'VuserId' => 2, 'Ip' => 3, 'Visits' => 4, 'TillRefresh' => 5, 'Clicks' => 6, 'Status' => 7, 'ActionsTimestamp' => 8, 'Currentvisit' => 9, 'Lastvisit' => 10, 'Added' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'vuserId' => 2, 'ip' => 3, 'visits' => 4, 'tillRefresh' => 5, 'clicks' => 6, 'status' => 7, 'actionsTimestamp' => 8, 'currentvisit' => 9, 'lastvisit' => 10, 'added' => 11, ),
        self::TYPE_COLNAME       => array(GradioSessionsTableMap::COL_ID => 0, GradioSessionsTableMap::COL_USER_ID => 1, GradioSessionsTableMap::COL_VUSER_ID => 2, GradioSessionsTableMap::COL_IP => 3, GradioSessionsTableMap::COL_VISITS => 4, GradioSessionsTableMap::COL_TILL_REFRESH => 5, GradioSessionsTableMap::COL_CLICKS => 6, GradioSessionsTableMap::COL_STATUS => 7, GradioSessionsTableMap::COL_ACTIONS_TIMESTAMP => 8, GradioSessionsTableMap::COL_CURRENTVISIT => 9, GradioSessionsTableMap::COL_LASTVISIT => 10, GradioSessionsTableMap::COL_ADDED => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'vuser_id' => 2, 'ip' => 3, 'visits' => 4, 'till_refresh' => 5, 'clicks' => 6, 'status' => 7, 'actions_timestamp' => 8, 'currentvisit' => 9, 'lastvisit' => 10, 'added' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_sessions');
        $this->setPhpName('GradioSessions');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioSessions');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', false, null, null);
        $this->addColumn('vuser_id', 'VuserId', 'VARCHAR', false, 16, null);
        $this->addColumn('ip', 'Ip', 'VARCHAR', true, 255, null);
        $this->addColumn('visits', 'Visits', 'INTEGER', true, null, null);
        $this->addColumn('till_refresh', 'TillRefresh', 'INTEGER', false, null, null);
        $this->addColumn('clicks', 'Clicks', 'INTEGER', true, null, null);
        $this->addColumn('status', 'Status', 'INTEGER', false, null, null);
        $this->addColumn('actions_timestamp', 'ActionsTimestamp', 'TIMESTAMP', false, null, null);
        $this->addColumn('currentvisit', 'Currentvisit', 'TIMESTAMP', false, null, null);
        $this->addColumn('lastvisit', 'Lastvisit', 'TIMESTAMP', false, null, null);
        $this->addColumn('added', 'Added', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioSessionsTableMap::CLASS_DEFAULT : GradioSessionsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioSessions object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioSessionsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioSessionsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioSessionsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioSessionsTableMap::OM_CLASS;
            /** @var GradioSessions $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioSessionsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioSessionsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioSessionsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioSessions $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioSessionsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_ID);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_USER_ID);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_VUSER_ID);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_IP);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_VISITS);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_TILL_REFRESH);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_CLICKS);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_STATUS);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_ACTIONS_TIMESTAMP);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_CURRENTVISIT);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_LASTVISIT);
            $criteria->addSelectColumn(GradioSessionsTableMap::COL_ADDED);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.vuser_id');
            $criteria->addSelectColumn($alias . '.ip');
            $criteria->addSelectColumn($alias . '.visits');
            $criteria->addSelectColumn($alias . '.till_refresh');
            $criteria->addSelectColumn($alias . '.clicks');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.actions_timestamp');
            $criteria->addSelectColumn($alias . '.currentvisit');
            $criteria->addSelectColumn($alias . '.lastvisit');
            $criteria->addSelectColumn($alias . '.added');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioSessionsTableMap::DATABASE_NAME)->getTable(GradioSessionsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioSessionsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioSessionsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioSessionsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioSessions or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioSessions object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioSessionsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioSessions) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioSessionsTableMap::DATABASE_NAME);
            $criteria->add(GradioSessionsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioSessionsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioSessionsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioSessionsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_sessions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioSessionsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioSessions or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioSessions object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioSessionsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioSessions object
        }

        if ($criteria->containsKey(GradioSessionsTableMap::COL_ID) && $criteria->keyContainsValue(GradioSessionsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioSessionsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioSessionsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioSessionsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioSessionsTableMap::buildTableMap();
