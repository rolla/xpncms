<?php

namespace Model\Main\Map;

use Model\Main\GradioNotifications;
use Model\Main\GradioNotificationsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_notifications' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioNotificationsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioNotificationsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_notifications';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioNotifications';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioNotifications';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_notifications.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'gradio_notifications.user_id';

    /**
     * the column name for the action_id field
     */
    const COL_ACTION_ID = 'gradio_notifications.action_id';

    /**
     * the column name for the level field
     */
    const COL_LEVEL = 'gradio_notifications.level';

    /**
     * the column name for the actions_category_id field
     */
    const COL_ACTIONS_CATEGORY_ID = 'gradio_notifications.actions_category_id';

    /**
     * the column name for the actions_item_id field
     */
    const COL_ACTIONS_ITEM_ID = 'gradio_notifications.actions_item_id';

    /**
     * the column name for the added field
     */
    const COL_ADDED = 'gradio_notifications.added';

    /**
     * the column name for the readed field
     */
    const COL_READED = 'gradio_notifications.readed';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'ActionId', 'Level', 'ActionsCategoryId', 'ActionsItemId', 'Added', 'Readed', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'actionId', 'level', 'actionsCategoryId', 'actionsItemId', 'added', 'readed', ),
        self::TYPE_COLNAME       => array(GradioNotificationsTableMap::COL_ID, GradioNotificationsTableMap::COL_USER_ID, GradioNotificationsTableMap::COL_ACTION_ID, GradioNotificationsTableMap::COL_LEVEL, GradioNotificationsTableMap::COL_ACTIONS_CATEGORY_ID, GradioNotificationsTableMap::COL_ACTIONS_ITEM_ID, GradioNotificationsTableMap::COL_ADDED, GradioNotificationsTableMap::COL_READED, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'action_id', 'level', 'actions_category_id', 'actions_item_id', 'added', 'readed', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'ActionId' => 2, 'Level' => 3, 'ActionsCategoryId' => 4, 'ActionsItemId' => 5, 'Added' => 6, 'Readed' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'actionId' => 2, 'level' => 3, 'actionsCategoryId' => 4, 'actionsItemId' => 5, 'added' => 6, 'readed' => 7, ),
        self::TYPE_COLNAME       => array(GradioNotificationsTableMap::COL_ID => 0, GradioNotificationsTableMap::COL_USER_ID => 1, GradioNotificationsTableMap::COL_ACTION_ID => 2, GradioNotificationsTableMap::COL_LEVEL => 3, GradioNotificationsTableMap::COL_ACTIONS_CATEGORY_ID => 4, GradioNotificationsTableMap::COL_ACTIONS_ITEM_ID => 5, GradioNotificationsTableMap::COL_ADDED => 6, GradioNotificationsTableMap::COL_READED => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'action_id' => 2, 'level' => 3, 'actions_category_id' => 4, 'actions_item_id' => 5, 'added' => 6, 'readed' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_notifications');
        $this->setPhpName('GradioNotifications');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioNotifications');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', true, null, null);
        $this->addColumn('action_id', 'ActionId', 'INTEGER', true, null, null);
        $this->addColumn('level', 'Level', 'INTEGER', true, null, null);
        $this->addColumn('actions_category_id', 'ActionsCategoryId', 'INTEGER', true, null, null);
        $this->addColumn('actions_item_id', 'ActionsItemId', 'INTEGER', true, null, null);
        $this->addColumn('added', 'Added', 'TIMESTAMP', true, null, null);
        $this->addColumn('readed', 'Readed', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioNotificationsTableMap::CLASS_DEFAULT : GradioNotificationsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioNotifications object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioNotificationsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioNotificationsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioNotificationsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioNotificationsTableMap::OM_CLASS;
            /** @var GradioNotifications $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioNotificationsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioNotificationsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioNotificationsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioNotifications $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioNotificationsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_ID);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_USER_ID);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_ACTION_ID);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_LEVEL);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_ACTIONS_CATEGORY_ID);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_ACTIONS_ITEM_ID);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_ADDED);
            $criteria->addSelectColumn(GradioNotificationsTableMap::COL_READED);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.action_id');
            $criteria->addSelectColumn($alias . '.level');
            $criteria->addSelectColumn($alias . '.actions_category_id');
            $criteria->addSelectColumn($alias . '.actions_item_id');
            $criteria->addSelectColumn($alias . '.added');
            $criteria->addSelectColumn($alias . '.readed');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioNotificationsTableMap::DATABASE_NAME)->getTable(GradioNotificationsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioNotificationsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioNotificationsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioNotificationsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioNotifications or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioNotifications object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioNotificationsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioNotifications) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioNotificationsTableMap::DATABASE_NAME);
            $criteria->add(GradioNotificationsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioNotificationsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioNotificationsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioNotificationsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_notifications table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioNotificationsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioNotifications or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioNotifications object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioNotificationsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioNotifications object
        }

        if ($criteria->containsKey(GradioNotificationsTableMap::COL_ID) && $criteria->keyContainsValue(GradioNotificationsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioNotificationsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioNotificationsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioNotificationsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioNotificationsTableMap::buildTableMap();
