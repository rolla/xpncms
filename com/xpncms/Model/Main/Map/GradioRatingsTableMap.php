<?php

namespace Model\Main\Map;

use Model\Main\GradioRatings;
use Model\Main\GradioRatingsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_ratings' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioRatingsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioRatingsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_ratings';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioRatings';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioRatings';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_ratings.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'gradio_ratings.user_id';

    /**
     * the column name for the item_id field
     */
    const COL_ITEM_ID = 'gradio_ratings.item_id';

    /**
     * the column name for the action_id field
     */
    const COL_ACTION_ID = 'gradio_ratings.action_id';

    /**
     * the column name for the category_id field
     */
    const COL_CATEGORY_ID = 'gradio_ratings.category_id';

    /**
     * the column name for the model field
     */
    const COL_MODEL = 'gradio_ratings.model';

    /**
     * the column name for the ip field
     */
    const COL_IP = 'gradio_ratings.ip';

    /**
     * the column name for the cepumi field
     */
    const COL_CEPUMI = 'gradio_ratings.cepumi';

    /**
     * the column name for the extrarating field
     */
    const COL_EXTRARATING = 'gradio_ratings.extrarating';

    /**
     * the column name for the nickname field
     */
    const COL_NICKNAME = 'gradio_ratings.nickname';

    /**
     * the column name for the comment field
     */
    const COL_COMMENT = 'gradio_ratings.comment';

    /**
     * the column name for the vuserid field
     */
    const COL_VUSERID = 'gradio_ratings.vuserid';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'gradio_ratings.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'gradio_ratings.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'gradio_ratings.deleted_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'ItemId', 'ActionId', 'CategoryId', 'Model', 'Ip', 'Cepumi', 'Extrarating', 'Nickname', 'Comment', 'Vuserid', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'itemId', 'actionId', 'categoryId', 'model', 'ip', 'cepumi', 'extrarating', 'nickname', 'comment', 'vuserid', 'createdAt', 'updatedAt', 'deletedAt', ),
        self::TYPE_COLNAME       => array(GradioRatingsTableMap::COL_ID, GradioRatingsTableMap::COL_USER_ID, GradioRatingsTableMap::COL_ITEM_ID, GradioRatingsTableMap::COL_ACTION_ID, GradioRatingsTableMap::COL_CATEGORY_ID, GradioRatingsTableMap::COL_MODEL, GradioRatingsTableMap::COL_IP, GradioRatingsTableMap::COL_CEPUMI, GradioRatingsTableMap::COL_EXTRARATING, GradioRatingsTableMap::COL_NICKNAME, GradioRatingsTableMap::COL_COMMENT, GradioRatingsTableMap::COL_VUSERID, GradioRatingsTableMap::COL_CREATED_AT, GradioRatingsTableMap::COL_UPDATED_AT, GradioRatingsTableMap::COL_DELETED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'item_id', 'action_id', 'category_id', 'model', 'ip', 'cepumi', 'extrarating', 'nickname', 'comment', 'vuserid', 'created_at', 'updated_at', 'deleted_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'ItemId' => 2, 'ActionId' => 3, 'CategoryId' => 4, 'Model' => 5, 'Ip' => 6, 'Cepumi' => 7, 'Extrarating' => 8, 'Nickname' => 9, 'Comment' => 10, 'Vuserid' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, 'DeletedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'itemId' => 2, 'actionId' => 3, 'categoryId' => 4, 'model' => 5, 'ip' => 6, 'cepumi' => 7, 'extrarating' => 8, 'nickname' => 9, 'comment' => 10, 'vuserid' => 11, 'createdAt' => 12, 'updatedAt' => 13, 'deletedAt' => 14, ),
        self::TYPE_COLNAME       => array(GradioRatingsTableMap::COL_ID => 0, GradioRatingsTableMap::COL_USER_ID => 1, GradioRatingsTableMap::COL_ITEM_ID => 2, GradioRatingsTableMap::COL_ACTION_ID => 3, GradioRatingsTableMap::COL_CATEGORY_ID => 4, GradioRatingsTableMap::COL_MODEL => 5, GradioRatingsTableMap::COL_IP => 6, GradioRatingsTableMap::COL_CEPUMI => 7, GradioRatingsTableMap::COL_EXTRARATING => 8, GradioRatingsTableMap::COL_NICKNAME => 9, GradioRatingsTableMap::COL_COMMENT => 10, GradioRatingsTableMap::COL_VUSERID => 11, GradioRatingsTableMap::COL_CREATED_AT => 12, GradioRatingsTableMap::COL_UPDATED_AT => 13, GradioRatingsTableMap::COL_DELETED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'item_id' => 2, 'action_id' => 3, 'category_id' => 4, 'model' => 5, 'ip' => 6, 'cepumi' => 7, 'extrarating' => 8, 'nickname' => 9, 'comment' => 10, 'vuserid' => 11, 'created_at' => 12, 'updated_at' => 13, 'deleted_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_ratings');
        $this->setPhpName('GradioRatings');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioRatings');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', true, null, null);
        $this->addColumn('item_id', 'ItemId', 'INTEGER', true, null, null);
        $this->addColumn('action_id', 'ActionId', 'INTEGER', true, null, null);
        $this->addColumn('category_id', 'CategoryId', 'INTEGER', true, null, null);
        $this->addColumn('model', 'Model', 'VARCHAR', false, 255, null);
        $this->addColumn('ip', 'Ip', 'VARCHAR', true, 50, null);
        $this->addColumn('cepumi', 'Cepumi', 'VARCHAR', true, 255, null);
        $this->addColumn('extrarating', 'Extrarating', 'LONGVARCHAR', false, null, null);
        $this->addColumn('nickname', 'Nickname', 'VARCHAR', true, 100, null);
        $this->addColumn('comment', 'Comment', 'VARCHAR', true, 255, null);
        $this->addColumn('vuserid', 'Vuserid', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioRatingsTableMap::CLASS_DEFAULT : GradioRatingsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioRatings object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioRatingsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioRatingsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioRatingsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioRatingsTableMap::OM_CLASS;
            /** @var GradioRatings $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioRatingsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioRatingsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioRatingsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioRatings $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioRatingsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_ID);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_USER_ID);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_ITEM_ID);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_ACTION_ID);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_CATEGORY_ID);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_MODEL);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_IP);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_CEPUMI);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_EXTRARATING);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_NICKNAME);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_COMMENT);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_VUSERID);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(GradioRatingsTableMap::COL_DELETED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.item_id');
            $criteria->addSelectColumn($alias . '.action_id');
            $criteria->addSelectColumn($alias . '.category_id');
            $criteria->addSelectColumn($alias . '.model');
            $criteria->addSelectColumn($alias . '.ip');
            $criteria->addSelectColumn($alias . '.cepumi');
            $criteria->addSelectColumn($alias . '.extrarating');
            $criteria->addSelectColumn($alias . '.nickname');
            $criteria->addSelectColumn($alias . '.comment');
            $criteria->addSelectColumn($alias . '.vuserid');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioRatingsTableMap::DATABASE_NAME)->getTable(GradioRatingsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioRatingsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioRatingsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioRatingsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioRatings or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioRatings object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioRatingsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioRatings) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioRatingsTableMap::DATABASE_NAME);
            $criteria->add(GradioRatingsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioRatingsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioRatingsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioRatingsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_ratings table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioRatingsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioRatings or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioRatings object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioRatingsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioRatings object
        }

        if ($criteria->containsKey(GradioRatingsTableMap::COL_ID) && $criteria->keyContainsValue(GradioRatingsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioRatingsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioRatingsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioRatingsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioRatingsTableMap::buildTableMap();
