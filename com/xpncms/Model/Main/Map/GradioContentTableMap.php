<?php

namespace Model\Main\Map;

use Model\Main\GradioContent;
use Model\Main\GradioContentQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_content' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioContentTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioContentTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_content';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioContent';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioContent';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the cont_id field
     */
    const COL_CONT_ID = 'gradio_content.cont_id';

    /**
     * the column name for the cont_name field
     */
    const COL_CONT_NAME = 'gradio_content.cont_name';

    /**
     * the column name for the cont_content field
     */
    const COL_CONT_CONTENT = 'gradio_content.cont_content';

    /**
     * the column name for the cont_tags field
     */
    const COL_CONT_TAGS = 'gradio_content.cont_tags';

    /**
     * the column name for the cont_alias field
     */
    const COL_CONT_ALIAS = 'gradio_content.cont_alias';

    /**
     * the column name for the cont_added field
     */
    const COL_CONT_ADDED = 'gradio_content.cont_added';

    /**
     * the column name for the cont_updated field
     */
    const COL_CONT_UPDATED = 'gradio_content.cont_updated';

    /**
     * the column name for the cont_class field
     */
    const COL_CONT_CLASS = 'gradio_content.cont_class';

    /**
     * the column name for the cont_category field
     */
    const COL_CONT_CATEGORY = 'gradio_content.cont_category';

    /**
     * the column name for the cont_imgs field
     */
    const COL_CONT_IMGS = 'gradio_content.cont_imgs';

    /**
     * the column name for the cont_order field
     */
    const COL_CONT_ORDER = 'gradio_content.cont_order';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('ContId', 'ContName', 'ContContent', 'ContTags', 'ContAlias', 'ContAdded', 'ContUpdated', 'ContClass', 'ContCategory', 'ContImgs', 'ContOrder', ),
        self::TYPE_CAMELNAME     => array('contId', 'contName', 'contContent', 'contTags', 'contAlias', 'contAdded', 'contUpdated', 'contClass', 'contCategory', 'contImgs', 'contOrder', ),
        self::TYPE_COLNAME       => array(GradioContentTableMap::COL_CONT_ID, GradioContentTableMap::COL_CONT_NAME, GradioContentTableMap::COL_CONT_CONTENT, GradioContentTableMap::COL_CONT_TAGS, GradioContentTableMap::COL_CONT_ALIAS, GradioContentTableMap::COL_CONT_ADDED, GradioContentTableMap::COL_CONT_UPDATED, GradioContentTableMap::COL_CONT_CLASS, GradioContentTableMap::COL_CONT_CATEGORY, GradioContentTableMap::COL_CONT_IMGS, GradioContentTableMap::COL_CONT_ORDER, ),
        self::TYPE_FIELDNAME     => array('cont_id', 'cont_name', 'cont_content', 'cont_tags', 'cont_alias', 'cont_added', 'cont_updated', 'cont_class', 'cont_category', 'cont_imgs', 'cont_order', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('ContId' => 0, 'ContName' => 1, 'ContContent' => 2, 'ContTags' => 3, 'ContAlias' => 4, 'ContAdded' => 5, 'ContUpdated' => 6, 'ContClass' => 7, 'ContCategory' => 8, 'ContImgs' => 9, 'ContOrder' => 10, ),
        self::TYPE_CAMELNAME     => array('contId' => 0, 'contName' => 1, 'contContent' => 2, 'contTags' => 3, 'contAlias' => 4, 'contAdded' => 5, 'contUpdated' => 6, 'contClass' => 7, 'contCategory' => 8, 'contImgs' => 9, 'contOrder' => 10, ),
        self::TYPE_COLNAME       => array(GradioContentTableMap::COL_CONT_ID => 0, GradioContentTableMap::COL_CONT_NAME => 1, GradioContentTableMap::COL_CONT_CONTENT => 2, GradioContentTableMap::COL_CONT_TAGS => 3, GradioContentTableMap::COL_CONT_ALIAS => 4, GradioContentTableMap::COL_CONT_ADDED => 5, GradioContentTableMap::COL_CONT_UPDATED => 6, GradioContentTableMap::COL_CONT_CLASS => 7, GradioContentTableMap::COL_CONT_CATEGORY => 8, GradioContentTableMap::COL_CONT_IMGS => 9, GradioContentTableMap::COL_CONT_ORDER => 10, ),
        self::TYPE_FIELDNAME     => array('cont_id' => 0, 'cont_name' => 1, 'cont_content' => 2, 'cont_tags' => 3, 'cont_alias' => 4, 'cont_added' => 5, 'cont_updated' => 6, 'cont_class' => 7, 'cont_category' => 8, 'cont_imgs' => 9, 'cont_order' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_content');
        $this->setPhpName('GradioContent');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioContent');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('cont_id', 'ContId', 'INTEGER', true, null, null);
        $this->addColumn('cont_name', 'ContName', 'VARCHAR', true, 200, null);
        $this->addColumn('cont_content', 'ContContent', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cont_tags', 'ContTags', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cont_alias', 'ContAlias', 'VARCHAR', true, 255, null);
        $this->addColumn('cont_added', 'ContAdded', 'TIMESTAMP', true, null, null);
        $this->addColumn('cont_updated', 'ContUpdated', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('cont_class', 'ContClass', 'INTEGER', true, null, null);
        $this->addColumn('cont_category', 'ContCategory', 'INTEGER', true, null, null);
        $this->addColumn('cont_imgs', 'ContImgs', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cont_order', 'ContOrder', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioContentTableMap::CLASS_DEFAULT : GradioContentTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioContent object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioContentTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioContentTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioContentTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioContentTableMap::OM_CLASS;
            /** @var GradioContent $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioContentTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioContentTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioContentTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioContent $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioContentTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_ID);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_NAME);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_CONTENT);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_TAGS);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_ALIAS);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_ADDED);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_UPDATED);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_CLASS);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_CATEGORY);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_IMGS);
            $criteria->addSelectColumn(GradioContentTableMap::COL_CONT_ORDER);
        } else {
            $criteria->addSelectColumn($alias . '.cont_id');
            $criteria->addSelectColumn($alias . '.cont_name');
            $criteria->addSelectColumn($alias . '.cont_content');
            $criteria->addSelectColumn($alias . '.cont_tags');
            $criteria->addSelectColumn($alias . '.cont_alias');
            $criteria->addSelectColumn($alias . '.cont_added');
            $criteria->addSelectColumn($alias . '.cont_updated');
            $criteria->addSelectColumn($alias . '.cont_class');
            $criteria->addSelectColumn($alias . '.cont_category');
            $criteria->addSelectColumn($alias . '.cont_imgs');
            $criteria->addSelectColumn($alias . '.cont_order');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioContentTableMap::DATABASE_NAME)->getTable(GradioContentTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioContentTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioContentTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioContentTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioContent or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioContent object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioContentTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioContent) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioContentTableMap::DATABASE_NAME);
            $criteria->add(GradioContentTableMap::COL_CONT_ID, (array) $values, Criteria::IN);
        }

        $query = GradioContentQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioContentTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioContentTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_content table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioContentQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioContent or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioContent object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioContentTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioContent object
        }

        if ($criteria->containsKey(GradioContentTableMap::COL_CONT_ID) && $criteria->keyContainsValue(GradioContentTableMap::COL_CONT_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioContentTableMap::COL_CONT_ID.')');
        }


        // Set the correct dbName
        $query = GradioContentQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioContentTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioContentTableMap::buildTableMap();
