<?php

namespace Model\Main\Map;

use Model\Main\GradioAuthsArchive;
use Model\Main\GradioAuthsArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_auths_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioAuthsArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioAuthsArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_auths_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioAuthsArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioAuthsArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_auths_archive.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'gradio_auths_archive.user_id';

    /**
     * the column name for the provider field
     */
    const COL_PROVIDER = 'gradio_auths_archive.provider';

    /**
     * the column name for the provider_uid field
     */
    const COL_PROVIDER_UID = 'gradio_auths_archive.provider_uid';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'gradio_auths_archive.email';

    /**
     * the column name for the display_name field
     */
    const COL_DISPLAY_NAME = 'gradio_auths_archive.display_name';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'gradio_auths_archive.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'gradio_auths_archive.last_name';

    /**
     * the column name for the profile_url field
     */
    const COL_PROFILE_URL = 'gradio_auths_archive.profile_url';

    /**
     * the column name for the website_url field
     */
    const COL_WEBSITE_URL = 'gradio_auths_archive.website_url';

    /**
     * the column name for the picture field
     */
    const COL_PICTURE = 'gradio_auths_archive.picture';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'gradio_auths_archive.created_at';

    /**
     * the column name for the full_user_data field
     */
    const COL_FULL_USER_DATA = 'gradio_auths_archive.full_user_data';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'gradio_auths_archive.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'Provider', 'ProviderUid', 'Email', 'DisplayName', 'FirstName', 'LastName', 'ProfileUrl', 'WebsiteUrl', 'Picture', 'CreatedAt', 'FullUserData', 'Status', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'provider', 'providerUid', 'email', 'displayName', 'firstName', 'lastName', 'profileUrl', 'websiteUrl', 'picture', 'createdAt', 'fullUserData', 'status', ),
        self::TYPE_COLNAME       => array(GradioAuthsArchiveTableMap::COL_ID, GradioAuthsArchiveTableMap::COL_USER_ID, GradioAuthsArchiveTableMap::COL_PROVIDER, GradioAuthsArchiveTableMap::COL_PROVIDER_UID, GradioAuthsArchiveTableMap::COL_EMAIL, GradioAuthsArchiveTableMap::COL_DISPLAY_NAME, GradioAuthsArchiveTableMap::COL_FIRST_NAME, GradioAuthsArchiveTableMap::COL_LAST_NAME, GradioAuthsArchiveTableMap::COL_PROFILE_URL, GradioAuthsArchiveTableMap::COL_WEBSITE_URL, GradioAuthsArchiveTableMap::COL_PICTURE, GradioAuthsArchiveTableMap::COL_CREATED_AT, GradioAuthsArchiveTableMap::COL_FULL_USER_DATA, GradioAuthsArchiveTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'provider', 'provider_uid', 'email', 'display_name', 'first_name', 'last_name', 'profile_url', 'website_url', 'picture', 'created_at', 'full_user_data', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'Provider' => 2, 'ProviderUid' => 3, 'Email' => 4, 'DisplayName' => 5, 'FirstName' => 6, 'LastName' => 7, 'ProfileUrl' => 8, 'WebsiteUrl' => 9, 'Picture' => 10, 'CreatedAt' => 11, 'FullUserData' => 12, 'Status' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'provider' => 2, 'providerUid' => 3, 'email' => 4, 'displayName' => 5, 'firstName' => 6, 'lastName' => 7, 'profileUrl' => 8, 'websiteUrl' => 9, 'picture' => 10, 'createdAt' => 11, 'fullUserData' => 12, 'status' => 13, ),
        self::TYPE_COLNAME       => array(GradioAuthsArchiveTableMap::COL_ID => 0, GradioAuthsArchiveTableMap::COL_USER_ID => 1, GradioAuthsArchiveTableMap::COL_PROVIDER => 2, GradioAuthsArchiveTableMap::COL_PROVIDER_UID => 3, GradioAuthsArchiveTableMap::COL_EMAIL => 4, GradioAuthsArchiveTableMap::COL_DISPLAY_NAME => 5, GradioAuthsArchiveTableMap::COL_FIRST_NAME => 6, GradioAuthsArchiveTableMap::COL_LAST_NAME => 7, GradioAuthsArchiveTableMap::COL_PROFILE_URL => 8, GradioAuthsArchiveTableMap::COL_WEBSITE_URL => 9, GradioAuthsArchiveTableMap::COL_PICTURE => 10, GradioAuthsArchiveTableMap::COL_CREATED_AT => 11, GradioAuthsArchiveTableMap::COL_FULL_USER_DATA => 12, GradioAuthsArchiveTableMap::COL_STATUS => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'provider' => 2, 'provider_uid' => 3, 'email' => 4, 'display_name' => 5, 'first_name' => 6, 'last_name' => 7, 'profile_url' => 8, 'website_url' => 9, 'picture' => 10, 'created_at' => 11, 'full_user_data' => 12, 'status' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_auths_archive');
        $this->setPhpName('GradioAuthsArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioAuthsArchive');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', true, null, null);
        $this->addColumn('provider', 'Provider', 'VARCHAR', true, 100, null);
        $this->addColumn('provider_uid', 'ProviderUid', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 200, null);
        $this->addColumn('display_name', 'DisplayName', 'VARCHAR', true, 150, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 100, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', true, 100, null);
        $this->addColumn('profile_url', 'ProfileUrl', 'VARCHAR', true, 300, null);
        $this->addColumn('website_url', 'WebsiteUrl', 'VARCHAR', true, 300, null);
        $this->addColumn('picture', 'Picture', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('full_user_data', 'FullUserData', 'LONGVARCHAR', true, null, null);
        $this->addColumn('status', 'Status', 'BOOLEAN', true, 1, true);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioAuthsArchiveTableMap::CLASS_DEFAULT : GradioAuthsArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioAuthsArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioAuthsArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioAuthsArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioAuthsArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioAuthsArchiveTableMap::OM_CLASS;
            /** @var GradioAuthsArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioAuthsArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioAuthsArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioAuthsArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioAuthsArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioAuthsArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_USER_ID);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_PROVIDER);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_PROVIDER_UID);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_EMAIL);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_DISPLAY_NAME);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_PROFILE_URL);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_WEBSITE_URL);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_PICTURE);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_FULL_USER_DATA);
            $criteria->addSelectColumn(GradioAuthsArchiveTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.provider');
            $criteria->addSelectColumn($alias . '.provider_uid');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.display_name');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.profile_url');
            $criteria->addSelectColumn($alias . '.website_url');
            $criteria->addSelectColumn($alias . '.picture');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.full_user_data');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioAuthsArchiveTableMap::DATABASE_NAME)->getTable(GradioAuthsArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioAuthsArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioAuthsArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioAuthsArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioAuthsArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioAuthsArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioAuthsArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioAuthsArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioAuthsArchiveTableMap::DATABASE_NAME);
            $criteria->add(GradioAuthsArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioAuthsArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioAuthsArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioAuthsArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_auths_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioAuthsArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioAuthsArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioAuthsArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioAuthsArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioAuthsArchive object
        }

        if ($criteria->containsKey(GradioAuthsArchiveTableMap::COL_ID) && $criteria->keyContainsValue(GradioAuthsArchiveTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioAuthsArchiveTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioAuthsArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioAuthsArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioAuthsArchiveTableMap::buildTableMap();
