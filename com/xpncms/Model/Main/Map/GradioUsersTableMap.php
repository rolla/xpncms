<?php

namespace Model\Main\Map;

use Model\Main\GradioUsers;
use Model\Main\GradioUsersQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_users' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioUsersTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioUsersTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_users';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioUsers';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioUsers';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 43;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 43;

    /**
     * the column name for the id field
     */
    const COL_ID = 'gradio_users.id';

    /**
     * the column name for the latest_provider field
     */
    const COL_LATEST_PROVIDER = 'gradio_users.latest_provider';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'gradio_users.username';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'gradio_users.password';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'gradio_users.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'gradio_users.last_name';

    /**
     * the column name for the display_name field
     */
    const COL_DISPLAY_NAME = 'gradio_users.display_name';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'gradio_users.description';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'gradio_users.email';

    /**
     * the column name for the email_verified field
     */
    const COL_EMAIL_VERIFIED = 'gradio_users.email_verified';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'gradio_users.phone';

    /**
     * the column name for the gender field
     */
    const COL_GENDER = 'gradio_users.gender';

    /**
     * the column name for the language field
     */
    const COL_LANGUAGE = 'gradio_users.language';

    /**
     * the column name for the birthday field
     */
    const COL_BIRTHDAY = 'gradio_users.birthday';

    /**
     * the column name for the age field
     */
    const COL_AGE = 'gradio_users.age';

    /**
     * the column name for the adult field
     */
    const COL_ADULT = 'gradio_users.adult';

    /**
     * the column name for the address field
     */
    const COL_ADDRESS = 'gradio_users.address';

    /**
     * the column name for the country field
     */
    const COL_COUNTRY = 'gradio_users.country';

    /**
     * the column name for the region field
     */
    const COL_REGION = 'gradio_users.region';

    /**
     * the column name for the city field
     */
    const COL_CITY = 'gradio_users.city';

    /**
     * the column name for the zip field
     */
    const COL_ZIP = 'gradio_users.zip';

    /**
     * the column name for the website_url field
     */
    const COL_WEBSITE_URL = 'gradio_users.website_url';

    /**
     * the column name for the profile_url field
     */
    const COL_PROFILE_URL = 'gradio_users.profile_url';

    /**
     * the column name for the photo_url field
     */
    const COL_PHOTO_URL = 'gradio_users.photo_url';

    /**
     * the column name for the ip field
     */
    const COL_IP = 'gradio_users.ip';

    /**
     * the column name for the visits field
     */
    const COL_VISITS = 'gradio_users.visits';

    /**
     * the column name for the currentvisit field
     */
    const COL_CURRENTVISIT = 'gradio_users.currentvisit';

    /**
     * the column name for the lastvisit field
     */
    const COL_LASTVISIT = 'gradio_users.lastvisit';

    /**
     * the column name for the updated field
     */
    const COL_UPDATED = 'gradio_users.updated';

    /**
     * the column name for the added field
     */
    const COL_ADDED = 'gradio_users.added';

    /**
     * the column name for the img field
     */
    const COL_IMG = 'gradio_users.img';

    /**
     * the column name for the imgs field
     */
    const COL_IMGS = 'gradio_users.imgs';

    /**
     * the column name for the imgm field
     */
    const COL_IMGM = 'gradio_users.imgm';

    /**
     * the column name for the imgl field
     */
    const COL_IMGL = 'gradio_users.imgl';

    /**
     * the column name for the prefs field
     */
    const COL_PREFS = 'gradio_users.prefs';

    /**
     * the column name for the admin field
     */
    const COL_ADMIN = 'gradio_users.admin';

    /**
     * the column name for the perms field
     */
    const COL_PERMS = 'gradio_users.perms';

    /**
     * the column name for the class field
     */
    const COL_CLASS = 'gradio_users.class';

    /**
     * the column name for the sess field
     */
    const COL_SESS = 'gradio_users.sess';

    /**
     * the column name for the ban field
     */
    const COL_BAN = 'gradio_users.ban';

    /**
     * the column name for the dr_created field
     */
    const COL_DR_CREATED = 'gradio_users.dr_created';

    /**
     * the column name for the dr_deleted field
     */
    const COL_DR_DELETED = 'gradio_users.dr_deleted';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'gradio_users.type';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'LatestProvider', 'Username', 'Password', 'FirstName', 'LastName', 'DisplayName', 'Description', 'Email', 'EmailVerified', 'Phone', 'Gender', 'Language', 'Birthday', 'Age', 'Adult', 'Address', 'Country', 'Region', 'City', 'Zip', 'WebsiteUrl', 'ProfileUrl', 'PhotoUrl', 'Ip', 'Visits', 'Currentvisit', 'Lastvisit', 'Updated', 'Added', 'Img', 'Imgs', 'Imgm', 'Imgl', 'Prefs', 'Admin', 'Perms', 'Class', 'Sess', 'Ban', 'DrCreated', 'DrDeleted', 'Type', ),
        self::TYPE_CAMELNAME     => array('id', 'latestProvider', 'username', 'password', 'firstName', 'lastName', 'displayName', 'description', 'email', 'emailVerified', 'phone', 'gender', 'language', 'birthday', 'age', 'adult', 'address', 'country', 'region', 'city', 'zip', 'websiteUrl', 'profileUrl', 'photoUrl', 'ip', 'visits', 'currentvisit', 'lastvisit', 'updated', 'added', 'img', 'imgs', 'imgm', 'imgl', 'prefs', 'admin', 'perms', 'class', 'sess', 'ban', 'drCreated', 'drDeleted', 'type', ),
        self::TYPE_COLNAME       => array(GradioUsersTableMap::COL_ID, GradioUsersTableMap::COL_LATEST_PROVIDER, GradioUsersTableMap::COL_USERNAME, GradioUsersTableMap::COL_PASSWORD, GradioUsersTableMap::COL_FIRST_NAME, GradioUsersTableMap::COL_LAST_NAME, GradioUsersTableMap::COL_DISPLAY_NAME, GradioUsersTableMap::COL_DESCRIPTION, GradioUsersTableMap::COL_EMAIL, GradioUsersTableMap::COL_EMAIL_VERIFIED, GradioUsersTableMap::COL_PHONE, GradioUsersTableMap::COL_GENDER, GradioUsersTableMap::COL_LANGUAGE, GradioUsersTableMap::COL_BIRTHDAY, GradioUsersTableMap::COL_AGE, GradioUsersTableMap::COL_ADULT, GradioUsersTableMap::COL_ADDRESS, GradioUsersTableMap::COL_COUNTRY, GradioUsersTableMap::COL_REGION, GradioUsersTableMap::COL_CITY, GradioUsersTableMap::COL_ZIP, GradioUsersTableMap::COL_WEBSITE_URL, GradioUsersTableMap::COL_PROFILE_URL, GradioUsersTableMap::COL_PHOTO_URL, GradioUsersTableMap::COL_IP, GradioUsersTableMap::COL_VISITS, GradioUsersTableMap::COL_CURRENTVISIT, GradioUsersTableMap::COL_LASTVISIT, GradioUsersTableMap::COL_UPDATED, GradioUsersTableMap::COL_ADDED, GradioUsersTableMap::COL_IMG, GradioUsersTableMap::COL_IMGS, GradioUsersTableMap::COL_IMGM, GradioUsersTableMap::COL_IMGL, GradioUsersTableMap::COL_PREFS, GradioUsersTableMap::COL_ADMIN, GradioUsersTableMap::COL_PERMS, GradioUsersTableMap::COL_CLASS, GradioUsersTableMap::COL_SESS, GradioUsersTableMap::COL_BAN, GradioUsersTableMap::COL_DR_CREATED, GradioUsersTableMap::COL_DR_DELETED, GradioUsersTableMap::COL_TYPE, ),
        self::TYPE_FIELDNAME     => array('id', 'latest_provider', 'username', 'password', 'first_name', 'last_name', 'display_name', 'description', 'email', 'email_verified', 'phone', 'gender', 'language', 'birthday', 'age', 'adult', 'address', 'country', 'region', 'city', 'zip', 'website_url', 'profile_url', 'photo_url', 'ip', 'visits', 'currentvisit', 'lastvisit', 'updated', 'added', 'img', 'imgs', 'imgm', 'imgl', 'prefs', 'admin', 'perms', 'class', 'sess', 'ban', 'dr_created', 'dr_deleted', 'type', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'LatestProvider' => 1, 'Username' => 2, 'Password' => 3, 'FirstName' => 4, 'LastName' => 5, 'DisplayName' => 6, 'Description' => 7, 'Email' => 8, 'EmailVerified' => 9, 'Phone' => 10, 'Gender' => 11, 'Language' => 12, 'Birthday' => 13, 'Age' => 14, 'Adult' => 15, 'Address' => 16, 'Country' => 17, 'Region' => 18, 'City' => 19, 'Zip' => 20, 'WebsiteUrl' => 21, 'ProfileUrl' => 22, 'PhotoUrl' => 23, 'Ip' => 24, 'Visits' => 25, 'Currentvisit' => 26, 'Lastvisit' => 27, 'Updated' => 28, 'Added' => 29, 'Img' => 30, 'Imgs' => 31, 'Imgm' => 32, 'Imgl' => 33, 'Prefs' => 34, 'Admin' => 35, 'Perms' => 36, 'Class' => 37, 'Sess' => 38, 'Ban' => 39, 'DrCreated' => 40, 'DrDeleted' => 41, 'Type' => 42, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'latestProvider' => 1, 'username' => 2, 'password' => 3, 'firstName' => 4, 'lastName' => 5, 'displayName' => 6, 'description' => 7, 'email' => 8, 'emailVerified' => 9, 'phone' => 10, 'gender' => 11, 'language' => 12, 'birthday' => 13, 'age' => 14, 'adult' => 15, 'address' => 16, 'country' => 17, 'region' => 18, 'city' => 19, 'zip' => 20, 'websiteUrl' => 21, 'profileUrl' => 22, 'photoUrl' => 23, 'ip' => 24, 'visits' => 25, 'currentvisit' => 26, 'lastvisit' => 27, 'updated' => 28, 'added' => 29, 'img' => 30, 'imgs' => 31, 'imgm' => 32, 'imgl' => 33, 'prefs' => 34, 'admin' => 35, 'perms' => 36, 'class' => 37, 'sess' => 38, 'ban' => 39, 'drCreated' => 40, 'drDeleted' => 41, 'type' => 42, ),
        self::TYPE_COLNAME       => array(GradioUsersTableMap::COL_ID => 0, GradioUsersTableMap::COL_LATEST_PROVIDER => 1, GradioUsersTableMap::COL_USERNAME => 2, GradioUsersTableMap::COL_PASSWORD => 3, GradioUsersTableMap::COL_FIRST_NAME => 4, GradioUsersTableMap::COL_LAST_NAME => 5, GradioUsersTableMap::COL_DISPLAY_NAME => 6, GradioUsersTableMap::COL_DESCRIPTION => 7, GradioUsersTableMap::COL_EMAIL => 8, GradioUsersTableMap::COL_EMAIL_VERIFIED => 9, GradioUsersTableMap::COL_PHONE => 10, GradioUsersTableMap::COL_GENDER => 11, GradioUsersTableMap::COL_LANGUAGE => 12, GradioUsersTableMap::COL_BIRTHDAY => 13, GradioUsersTableMap::COL_AGE => 14, GradioUsersTableMap::COL_ADULT => 15, GradioUsersTableMap::COL_ADDRESS => 16, GradioUsersTableMap::COL_COUNTRY => 17, GradioUsersTableMap::COL_REGION => 18, GradioUsersTableMap::COL_CITY => 19, GradioUsersTableMap::COL_ZIP => 20, GradioUsersTableMap::COL_WEBSITE_URL => 21, GradioUsersTableMap::COL_PROFILE_URL => 22, GradioUsersTableMap::COL_PHOTO_URL => 23, GradioUsersTableMap::COL_IP => 24, GradioUsersTableMap::COL_VISITS => 25, GradioUsersTableMap::COL_CURRENTVISIT => 26, GradioUsersTableMap::COL_LASTVISIT => 27, GradioUsersTableMap::COL_UPDATED => 28, GradioUsersTableMap::COL_ADDED => 29, GradioUsersTableMap::COL_IMG => 30, GradioUsersTableMap::COL_IMGS => 31, GradioUsersTableMap::COL_IMGM => 32, GradioUsersTableMap::COL_IMGL => 33, GradioUsersTableMap::COL_PREFS => 34, GradioUsersTableMap::COL_ADMIN => 35, GradioUsersTableMap::COL_PERMS => 36, GradioUsersTableMap::COL_CLASS => 37, GradioUsersTableMap::COL_SESS => 38, GradioUsersTableMap::COL_BAN => 39, GradioUsersTableMap::COL_DR_CREATED => 40, GradioUsersTableMap::COL_DR_DELETED => 41, GradioUsersTableMap::COL_TYPE => 42, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'latest_provider' => 1, 'username' => 2, 'password' => 3, 'first_name' => 4, 'last_name' => 5, 'display_name' => 6, 'description' => 7, 'email' => 8, 'email_verified' => 9, 'phone' => 10, 'gender' => 11, 'language' => 12, 'birthday' => 13, 'age' => 14, 'adult' => 15, 'address' => 16, 'country' => 17, 'region' => 18, 'city' => 19, 'zip' => 20, 'website_url' => 21, 'profile_url' => 22, 'photo_url' => 23, 'ip' => 24, 'visits' => 25, 'currentvisit' => 26, 'lastvisit' => 27, 'updated' => 28, 'added' => 29, 'img' => 30, 'imgs' => 31, 'imgm' => 32, 'imgl' => 33, 'prefs' => 34, 'admin' => 35, 'perms' => 36, 'class' => 37, 'sess' => 38, 'ban' => 39, 'dr_created' => 40, 'dr_deleted' => 41, 'type' => 42, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_users');
        $this->setPhpName('GradioUsers');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioUsers');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('latest_provider', 'LatestProvider', 'VARCHAR', false, 25, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 255, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 255, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', true, 255, null);
        $this->addColumn('display_name', 'DisplayName', 'VARCHAR', true, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', true, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 255, null);
        $this->addColumn('email_verified', 'EmailVerified', 'VARCHAR', true, 255, null);
        $this->addColumn('phone', 'Phone', 'INTEGER', true, null, null);
        $this->addColumn('gender', 'Gender', 'VARCHAR', true, 255, null);
        $this->addColumn('language', 'Language', 'VARCHAR', true, 255, null);
        $this->addColumn('birthday', 'Birthday', 'DATE', false, null, null);
        $this->addColumn('age', 'Age', 'INTEGER', true, 10, null);
        $this->addColumn('adult', 'Adult', 'INTEGER', true, 10, null);
        $this->addColumn('address', 'Address', 'VARCHAR', true, 255, null);
        $this->addColumn('country', 'Country', 'VARCHAR', true, 255, null);
        $this->addColumn('region', 'Region', 'VARCHAR', true, 255, null);
        $this->addColumn('city', 'City', 'VARCHAR', true, 255, null);
        $this->addColumn('zip', 'Zip', 'VARCHAR', true, 255, null);
        $this->addColumn('website_url', 'WebsiteUrl', 'VARCHAR', true, 255, null);
        $this->addColumn('profile_url', 'ProfileUrl', 'VARCHAR', true, 255, null);
        $this->addColumn('photo_url', 'PhotoUrl', 'VARCHAR', true, 255, null);
        $this->addColumn('ip', 'Ip', 'VARCHAR', true, 255, null);
        $this->addColumn('visits', 'Visits', 'INTEGER', true, null, null);
        $this->addColumn('currentvisit', 'Currentvisit', 'TIMESTAMP', false, null, null);
        $this->addColumn('lastvisit', 'Lastvisit', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated', 'Updated', 'TIMESTAMP', false, null, null);
        $this->addColumn('added', 'Added', 'TIMESTAMP', false, null, null);
        $this->addColumn('img', 'Img', 'VARCHAR', true, 255, null);
        $this->addColumn('imgs', 'Imgs', 'VARCHAR', true, 255, null);
        $this->addColumn('imgm', 'Imgm', 'VARCHAR', true, 255, null);
        $this->addColumn('imgl', 'Imgl', 'VARCHAR', true, 255, null);
        $this->addColumn('prefs', 'Prefs', 'LONGVARCHAR', true, null, null);
        $this->addColumn('admin', 'Admin', 'TINYINT', true, 3, 0);
        $this->addColumn('perms', 'Perms', 'LONGVARCHAR', true, null, null);
        $this->addColumn('class', 'Class', 'LONGVARCHAR', true, null, null);
        $this->addColumn('sess', 'Sess', 'VARCHAR', true, 255, null);
        $this->addColumn('ban', 'Ban', 'INTEGER', true, 10, null);
        $this->addColumn('dr_created', 'DrCreated', 'VARCHAR', true, 255, null);
        $this->addColumn('dr_deleted', 'DrDeleted', 'VARCHAR', true, 255, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioUsersTableMap::CLASS_DEFAULT : GradioUsersTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioUsers object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioUsersTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioUsersTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioUsersTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioUsersTableMap::OM_CLASS;
            /** @var GradioUsers $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioUsersTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioUsersTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioUsersTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioUsers $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioUsersTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioUsersTableMap::COL_ID);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_LATEST_PROVIDER);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_USERNAME);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_DISPLAY_NAME);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_EMAIL);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_EMAIL_VERIFIED);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_PHONE);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_GENDER);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_LANGUAGE);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_BIRTHDAY);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_AGE);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_ADULT);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_ADDRESS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_COUNTRY);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_REGION);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_CITY);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_ZIP);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_WEBSITE_URL);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_PROFILE_URL);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_PHOTO_URL);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_IP);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_VISITS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_CURRENTVISIT);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_LASTVISIT);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_UPDATED);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_ADDED);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_IMG);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_IMGS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_IMGM);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_IMGL);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_PREFS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_ADMIN);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_PERMS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_CLASS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_SESS);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_BAN);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_DR_CREATED);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_DR_DELETED);
            $criteria->addSelectColumn(GradioUsersTableMap::COL_TYPE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.latest_provider');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.display_name');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.email_verified');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.gender');
            $criteria->addSelectColumn($alias . '.language');
            $criteria->addSelectColumn($alias . '.birthday');
            $criteria->addSelectColumn($alias . '.age');
            $criteria->addSelectColumn($alias . '.adult');
            $criteria->addSelectColumn($alias . '.address');
            $criteria->addSelectColumn($alias . '.country');
            $criteria->addSelectColumn($alias . '.region');
            $criteria->addSelectColumn($alias . '.city');
            $criteria->addSelectColumn($alias . '.zip');
            $criteria->addSelectColumn($alias . '.website_url');
            $criteria->addSelectColumn($alias . '.profile_url');
            $criteria->addSelectColumn($alias . '.photo_url');
            $criteria->addSelectColumn($alias . '.ip');
            $criteria->addSelectColumn($alias . '.visits');
            $criteria->addSelectColumn($alias . '.currentvisit');
            $criteria->addSelectColumn($alias . '.lastvisit');
            $criteria->addSelectColumn($alias . '.updated');
            $criteria->addSelectColumn($alias . '.added');
            $criteria->addSelectColumn($alias . '.img');
            $criteria->addSelectColumn($alias . '.imgs');
            $criteria->addSelectColumn($alias . '.imgm');
            $criteria->addSelectColumn($alias . '.imgl');
            $criteria->addSelectColumn($alias . '.prefs');
            $criteria->addSelectColumn($alias . '.admin');
            $criteria->addSelectColumn($alias . '.perms');
            $criteria->addSelectColumn($alias . '.class');
            $criteria->addSelectColumn($alias . '.sess');
            $criteria->addSelectColumn($alias . '.ban');
            $criteria->addSelectColumn($alias . '.dr_created');
            $criteria->addSelectColumn($alias . '.dr_deleted');
            $criteria->addSelectColumn($alias . '.type');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioUsersTableMap::DATABASE_NAME)->getTable(GradioUsersTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioUsersTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioUsersTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioUsersTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioUsers or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioUsers object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioUsers) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioUsersTableMap::DATABASE_NAME);
            $criteria->add(GradioUsersTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = GradioUsersQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioUsersTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioUsersTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioUsersQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioUsers or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioUsers object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioUsers object
        }

        if ($criteria->containsKey(GradioUsersTableMap::COL_ID) && $criteria->keyContainsValue(GradioUsersTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioUsersTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = GradioUsersQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioUsersTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioUsersTableMap::buildTableMap();
