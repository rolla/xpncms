<?php

namespace Model\Main\Map;

use Model\Main\GradioCategories;
use Model\Main\GradioCategoriesQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gradio_categories' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GradioCategoriesTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Main.Map.GradioCategoriesTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio_gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gradio_categories';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Main\\GradioCategories';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Main.GradioCategories';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the cat_id field
     */
    const COL_CAT_ID = 'gradio_categories.cat_id';

    /**
     * the column name for the cat_name field
     */
    const COL_CAT_NAME = 'gradio_categories.cat_name';

    /**
     * the column name for the cat_content field
     */
    const COL_CAT_CONTENT = 'gradio_categories.cat_content';

    /**
     * the column name for the cat_alias field
     */
    const COL_CAT_ALIAS = 'gradio_categories.cat_alias';

    /**
     * the column name for the cat_added field
     */
    const COL_CAT_ADDED = 'gradio_categories.cat_added';

    /**
     * the column name for the cat_updated field
     */
    const COL_CAT_UPDATED = 'gradio_categories.cat_updated';

    /**
     * the column name for the cat_class field
     */
    const COL_CAT_CLASS = 'gradio_categories.cat_class';

    /**
     * the column name for the cat_parent field
     */
    const COL_CAT_PARENT = 'gradio_categories.cat_parent';

    /**
     * the column name for the cat_imgs field
     */
    const COL_CAT_IMGS = 'gradio_categories.cat_imgs';

    /**
     * the column name for the cat_order field
     */
    const COL_CAT_ORDER = 'gradio_categories.cat_order';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('CatId', 'CatName', 'CatContent', 'CatAlias', 'CatAdded', 'CatUpdated', 'CatClass', 'CatParent', 'CatImgs', 'CatOrder', ),
        self::TYPE_CAMELNAME     => array('catId', 'catName', 'catContent', 'catAlias', 'catAdded', 'catUpdated', 'catClass', 'catParent', 'catImgs', 'catOrder', ),
        self::TYPE_COLNAME       => array(GradioCategoriesTableMap::COL_CAT_ID, GradioCategoriesTableMap::COL_CAT_NAME, GradioCategoriesTableMap::COL_CAT_CONTENT, GradioCategoriesTableMap::COL_CAT_ALIAS, GradioCategoriesTableMap::COL_CAT_ADDED, GradioCategoriesTableMap::COL_CAT_UPDATED, GradioCategoriesTableMap::COL_CAT_CLASS, GradioCategoriesTableMap::COL_CAT_PARENT, GradioCategoriesTableMap::COL_CAT_IMGS, GradioCategoriesTableMap::COL_CAT_ORDER, ),
        self::TYPE_FIELDNAME     => array('cat_id', 'cat_name', 'cat_content', 'cat_alias', 'cat_added', 'cat_updated', 'cat_class', 'cat_parent', 'cat_imgs', 'cat_order', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('CatId' => 0, 'CatName' => 1, 'CatContent' => 2, 'CatAlias' => 3, 'CatAdded' => 4, 'CatUpdated' => 5, 'CatClass' => 6, 'CatParent' => 7, 'CatImgs' => 8, 'CatOrder' => 9, ),
        self::TYPE_CAMELNAME     => array('catId' => 0, 'catName' => 1, 'catContent' => 2, 'catAlias' => 3, 'catAdded' => 4, 'catUpdated' => 5, 'catClass' => 6, 'catParent' => 7, 'catImgs' => 8, 'catOrder' => 9, ),
        self::TYPE_COLNAME       => array(GradioCategoriesTableMap::COL_CAT_ID => 0, GradioCategoriesTableMap::COL_CAT_NAME => 1, GradioCategoriesTableMap::COL_CAT_CONTENT => 2, GradioCategoriesTableMap::COL_CAT_ALIAS => 3, GradioCategoriesTableMap::COL_CAT_ADDED => 4, GradioCategoriesTableMap::COL_CAT_UPDATED => 5, GradioCategoriesTableMap::COL_CAT_CLASS => 6, GradioCategoriesTableMap::COL_CAT_PARENT => 7, GradioCategoriesTableMap::COL_CAT_IMGS => 8, GradioCategoriesTableMap::COL_CAT_ORDER => 9, ),
        self::TYPE_FIELDNAME     => array('cat_id' => 0, 'cat_name' => 1, 'cat_content' => 2, 'cat_alias' => 3, 'cat_added' => 4, 'cat_updated' => 5, 'cat_class' => 6, 'cat_parent' => 7, 'cat_imgs' => 8, 'cat_order' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gradio_categories');
        $this->setPhpName('GradioCategories');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Main\\GradioCategories');
        $this->setPackage('Model.Main');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('cat_id', 'CatId', 'INTEGER', true, null, null);
        $this->addColumn('cat_name', 'CatName', 'VARCHAR', true, 255, null);
        $this->addColumn('cat_content', 'CatContent', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cat_alias', 'CatAlias', 'VARCHAR', true, 255, null);
        $this->addColumn('cat_added', 'CatAdded', 'TIMESTAMP', true, null, null);
        $this->addColumn('cat_updated', 'CatUpdated', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('cat_class', 'CatClass', 'INTEGER', true, null, null);
        $this->addColumn('cat_parent', 'CatParent', 'INTEGER', true, null, 0);
        $this->addColumn('cat_imgs', 'CatImgs', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cat_order', 'CatOrder', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('CatId', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GradioCategoriesTableMap::CLASS_DEFAULT : GradioCategoriesTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GradioCategories object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GradioCategoriesTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GradioCategoriesTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GradioCategoriesTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GradioCategoriesTableMap::OM_CLASS;
            /** @var GradioCategories $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GradioCategoriesTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GradioCategoriesTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GradioCategoriesTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GradioCategories $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GradioCategoriesTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_ID);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_NAME);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_CONTENT);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_ALIAS);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_ADDED);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_UPDATED);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_CLASS);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_PARENT);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_IMGS);
            $criteria->addSelectColumn(GradioCategoriesTableMap::COL_CAT_ORDER);
        } else {
            $criteria->addSelectColumn($alias . '.cat_id');
            $criteria->addSelectColumn($alias . '.cat_name');
            $criteria->addSelectColumn($alias . '.cat_content');
            $criteria->addSelectColumn($alias . '.cat_alias');
            $criteria->addSelectColumn($alias . '.cat_added');
            $criteria->addSelectColumn($alias . '.cat_updated');
            $criteria->addSelectColumn($alias . '.cat_class');
            $criteria->addSelectColumn($alias . '.cat_parent');
            $criteria->addSelectColumn($alias . '.cat_imgs');
            $criteria->addSelectColumn($alias . '.cat_order');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GradioCategoriesTableMap::DATABASE_NAME)->getTable(GradioCategoriesTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GradioCategoriesTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GradioCategoriesTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GradioCategoriesTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GradioCategories or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GradioCategories object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioCategoriesTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Main\GradioCategories) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GradioCategoriesTableMap::DATABASE_NAME);
            $criteria->add(GradioCategoriesTableMap::COL_CAT_ID, (array) $values, Criteria::IN);
        }

        $query = GradioCategoriesQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GradioCategoriesTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GradioCategoriesTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gradio_categories table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GradioCategoriesQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GradioCategories or Criteria object.
     *
     * @param mixed               $criteria Criteria or GradioCategories object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioCategoriesTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GradioCategories object
        }

        if ($criteria->containsKey(GradioCategoriesTableMap::COL_CAT_ID) && $criteria->keyContainsValue(GradioCategoriesTableMap::COL_CAT_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GradioCategoriesTableMap::COL_CAT_ID.')');
        }


        // Set the correct dbName
        $query = GradioCategoriesQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GradioCategoriesTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GradioCategoriesTableMap::buildTableMap();
