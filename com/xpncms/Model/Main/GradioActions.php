<?php

namespace Model\Main;

use Model\Main\Base\GradioActions as BaseGradioActions;

/**
 * Skeleton subclass for representing a row from the 'gradio_actions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GradioActions extends BaseGradioActions
{

}
