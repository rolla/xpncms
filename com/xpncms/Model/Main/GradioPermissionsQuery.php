<?php

namespace Model\Main;

use Model\Main\Base\GradioPermissionsQuery as BaseGradioPermissionsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'gradio_permissions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GradioPermissionsQuery extends BaseGradioPermissionsQuery
{

}
