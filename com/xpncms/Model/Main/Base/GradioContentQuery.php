<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioContent as ChildGradioContent;
use Model\Main\GradioContentQuery as ChildGradioContentQuery;
use Model\Main\Map\GradioContentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_content' table.
 *
 *
 *
 * @method     ChildGradioContentQuery orderByContId($order = Criteria::ASC) Order by the cont_id column
 * @method     ChildGradioContentQuery orderByContName($order = Criteria::ASC) Order by the cont_name column
 * @method     ChildGradioContentQuery orderByContContent($order = Criteria::ASC) Order by the cont_content column
 * @method     ChildGradioContentQuery orderByContTags($order = Criteria::ASC) Order by the cont_tags column
 * @method     ChildGradioContentQuery orderByContAlias($order = Criteria::ASC) Order by the cont_alias column
 * @method     ChildGradioContentQuery orderByContAdded($order = Criteria::ASC) Order by the cont_added column
 * @method     ChildGradioContentQuery orderByContUpdated($order = Criteria::ASC) Order by the cont_updated column
 * @method     ChildGradioContentQuery orderByContClass($order = Criteria::ASC) Order by the cont_class column
 * @method     ChildGradioContentQuery orderByContCategory($order = Criteria::ASC) Order by the cont_category column
 * @method     ChildGradioContentQuery orderByContImgs($order = Criteria::ASC) Order by the cont_imgs column
 * @method     ChildGradioContentQuery orderByContOrder($order = Criteria::ASC) Order by the cont_order column
 *
 * @method     ChildGradioContentQuery groupByContId() Group by the cont_id column
 * @method     ChildGradioContentQuery groupByContName() Group by the cont_name column
 * @method     ChildGradioContentQuery groupByContContent() Group by the cont_content column
 * @method     ChildGradioContentQuery groupByContTags() Group by the cont_tags column
 * @method     ChildGradioContentQuery groupByContAlias() Group by the cont_alias column
 * @method     ChildGradioContentQuery groupByContAdded() Group by the cont_added column
 * @method     ChildGradioContentQuery groupByContUpdated() Group by the cont_updated column
 * @method     ChildGradioContentQuery groupByContClass() Group by the cont_class column
 * @method     ChildGradioContentQuery groupByContCategory() Group by the cont_category column
 * @method     ChildGradioContentQuery groupByContImgs() Group by the cont_imgs column
 * @method     ChildGradioContentQuery groupByContOrder() Group by the cont_order column
 *
 * @method     ChildGradioContentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioContentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioContentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioContentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioContentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioContentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioContent findOne(ConnectionInterface $con = null) Return the first ChildGradioContent matching the query
 * @method     ChildGradioContent findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioContent matching the query, or a new ChildGradioContent object populated from the query conditions when no match is found
 *
 * @method     ChildGradioContent findOneByContId(int $cont_id) Return the first ChildGradioContent filtered by the cont_id column
 * @method     ChildGradioContent findOneByContName(string $cont_name) Return the first ChildGradioContent filtered by the cont_name column
 * @method     ChildGradioContent findOneByContContent(string $cont_content) Return the first ChildGradioContent filtered by the cont_content column
 * @method     ChildGradioContent findOneByContTags(string $cont_tags) Return the first ChildGradioContent filtered by the cont_tags column
 * @method     ChildGradioContent findOneByContAlias(string $cont_alias) Return the first ChildGradioContent filtered by the cont_alias column
 * @method     ChildGradioContent findOneByContAdded(string $cont_added) Return the first ChildGradioContent filtered by the cont_added column
 * @method     ChildGradioContent findOneByContUpdated(string $cont_updated) Return the first ChildGradioContent filtered by the cont_updated column
 * @method     ChildGradioContent findOneByContClass(int $cont_class) Return the first ChildGradioContent filtered by the cont_class column
 * @method     ChildGradioContent findOneByContCategory(int $cont_category) Return the first ChildGradioContent filtered by the cont_category column
 * @method     ChildGradioContent findOneByContImgs(string $cont_imgs) Return the first ChildGradioContent filtered by the cont_imgs column
 * @method     ChildGradioContent findOneByContOrder(int $cont_order) Return the first ChildGradioContent filtered by the cont_order column *

 * @method     ChildGradioContent requirePk($key, ConnectionInterface $con = null) Return the ChildGradioContent by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOne(ConnectionInterface $con = null) Return the first ChildGradioContent matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioContent requireOneByContId(int $cont_id) Return the first ChildGradioContent filtered by the cont_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContName(string $cont_name) Return the first ChildGradioContent filtered by the cont_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContContent(string $cont_content) Return the first ChildGradioContent filtered by the cont_content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContTags(string $cont_tags) Return the first ChildGradioContent filtered by the cont_tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContAlias(string $cont_alias) Return the first ChildGradioContent filtered by the cont_alias column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContAdded(string $cont_added) Return the first ChildGradioContent filtered by the cont_added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContUpdated(string $cont_updated) Return the first ChildGradioContent filtered by the cont_updated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContClass(int $cont_class) Return the first ChildGradioContent filtered by the cont_class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContCategory(int $cont_category) Return the first ChildGradioContent filtered by the cont_category column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContImgs(string $cont_imgs) Return the first ChildGradioContent filtered by the cont_imgs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioContent requireOneByContOrder(int $cont_order) Return the first ChildGradioContent filtered by the cont_order column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioContent[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioContent objects based on current ModelCriteria
 * @method     ChildGradioContent[]|ObjectCollection findByContId(int $cont_id) Return ChildGradioContent objects filtered by the cont_id column
 * @method     ChildGradioContent[]|ObjectCollection findByContName(string $cont_name) Return ChildGradioContent objects filtered by the cont_name column
 * @method     ChildGradioContent[]|ObjectCollection findByContContent(string $cont_content) Return ChildGradioContent objects filtered by the cont_content column
 * @method     ChildGradioContent[]|ObjectCollection findByContTags(string $cont_tags) Return ChildGradioContent objects filtered by the cont_tags column
 * @method     ChildGradioContent[]|ObjectCollection findByContAlias(string $cont_alias) Return ChildGradioContent objects filtered by the cont_alias column
 * @method     ChildGradioContent[]|ObjectCollection findByContAdded(string $cont_added) Return ChildGradioContent objects filtered by the cont_added column
 * @method     ChildGradioContent[]|ObjectCollection findByContUpdated(string $cont_updated) Return ChildGradioContent objects filtered by the cont_updated column
 * @method     ChildGradioContent[]|ObjectCollection findByContClass(int $cont_class) Return ChildGradioContent objects filtered by the cont_class column
 * @method     ChildGradioContent[]|ObjectCollection findByContCategory(int $cont_category) Return ChildGradioContent objects filtered by the cont_category column
 * @method     ChildGradioContent[]|ObjectCollection findByContImgs(string $cont_imgs) Return ChildGradioContent objects filtered by the cont_imgs column
 * @method     ChildGradioContent[]|ObjectCollection findByContOrder(int $cont_order) Return ChildGradioContent objects filtered by the cont_order column
 * @method     ChildGradioContent[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioContentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioContentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioContent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioContentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioContentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioContentQuery) {
            return $criteria;
        }
        $query = new ChildGradioContentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioContent|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioContentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioContentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioContent A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT cont_id, cont_name, cont_content, cont_tags, cont_alias, cont_added, cont_updated, cont_class, cont_category, cont_imgs, cont_order FROM gradio_content WHERE cont_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioContent $obj */
            $obj = new ChildGradioContent();
            $obj->hydrate($row);
            GradioContentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioContent|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the cont_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContId(1234); // WHERE cont_id = 1234
     * $query->filterByContId(array(12, 34)); // WHERE cont_id IN (12, 34)
     * $query->filterByContId(array('min' => 12)); // WHERE cont_id > 12
     * </code>
     *
     * @param     mixed $contId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContId($contId = null, $comparison = null)
    {
        if (is_array($contId)) {
            $useMinMax = false;
            if (isset($contId['min'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_ID, $contId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contId['max'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_ID, $contId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_ID, $contId, $comparison);
    }

    /**
     * Filter the query on the cont_name column
     *
     * Example usage:
     * <code>
     * $query->filterByContName('fooValue');   // WHERE cont_name = 'fooValue'
     * $query->filterByContName('%fooValue%', Criteria::LIKE); // WHERE cont_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContName($contName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_NAME, $contName, $comparison);
    }

    /**
     * Filter the query on the cont_content column
     *
     * Example usage:
     * <code>
     * $query->filterByContContent('fooValue');   // WHERE cont_content = 'fooValue'
     * $query->filterByContContent('%fooValue%', Criteria::LIKE); // WHERE cont_content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contContent The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContContent($contContent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contContent)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_CONTENT, $contContent, $comparison);
    }

    /**
     * Filter the query on the cont_tags column
     *
     * Example usage:
     * <code>
     * $query->filterByContTags('fooValue');   // WHERE cont_tags = 'fooValue'
     * $query->filterByContTags('%fooValue%', Criteria::LIKE); // WHERE cont_tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contTags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContTags($contTags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contTags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_TAGS, $contTags, $comparison);
    }

    /**
     * Filter the query on the cont_alias column
     *
     * Example usage:
     * <code>
     * $query->filterByContAlias('fooValue');   // WHERE cont_alias = 'fooValue'
     * $query->filterByContAlias('%fooValue%', Criteria::LIKE); // WHERE cont_alias LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contAlias The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContAlias($contAlias = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contAlias)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_ALIAS, $contAlias, $comparison);
    }

    /**
     * Filter the query on the cont_added column
     *
     * Example usage:
     * <code>
     * $query->filterByContAdded('2011-03-14'); // WHERE cont_added = '2011-03-14'
     * $query->filterByContAdded('now'); // WHERE cont_added = '2011-03-14'
     * $query->filterByContAdded(array('max' => 'yesterday')); // WHERE cont_added > '2011-03-13'
     * </code>
     *
     * @param     mixed $contAdded The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContAdded($contAdded = null, $comparison = null)
    {
        if (is_array($contAdded)) {
            $useMinMax = false;
            if (isset($contAdded['min'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_ADDED, $contAdded['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contAdded['max'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_ADDED, $contAdded['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_ADDED, $contAdded, $comparison);
    }

    /**
     * Filter the query on the cont_updated column
     *
     * Example usage:
     * <code>
     * $query->filterByContUpdated('2011-03-14'); // WHERE cont_updated = '2011-03-14'
     * $query->filterByContUpdated('now'); // WHERE cont_updated = '2011-03-14'
     * $query->filterByContUpdated(array('max' => 'yesterday')); // WHERE cont_updated > '2011-03-13'
     * </code>
     *
     * @param     mixed $contUpdated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContUpdated($contUpdated = null, $comparison = null)
    {
        if (is_array($contUpdated)) {
            $useMinMax = false;
            if (isset($contUpdated['min'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_UPDATED, $contUpdated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contUpdated['max'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_UPDATED, $contUpdated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_UPDATED, $contUpdated, $comparison);
    }

    /**
     * Filter the query on the cont_class column
     *
     * Example usage:
     * <code>
     * $query->filterByContClass(1234); // WHERE cont_class = 1234
     * $query->filterByContClass(array(12, 34)); // WHERE cont_class IN (12, 34)
     * $query->filterByContClass(array('min' => 12)); // WHERE cont_class > 12
     * </code>
     *
     * @param     mixed $contClass The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContClass($contClass = null, $comparison = null)
    {
        if (is_array($contClass)) {
            $useMinMax = false;
            if (isset($contClass['min'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_CLASS, $contClass['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contClass['max'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_CLASS, $contClass['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_CLASS, $contClass, $comparison);
    }

    /**
     * Filter the query on the cont_category column
     *
     * Example usage:
     * <code>
     * $query->filterByContCategory(1234); // WHERE cont_category = 1234
     * $query->filterByContCategory(array(12, 34)); // WHERE cont_category IN (12, 34)
     * $query->filterByContCategory(array('min' => 12)); // WHERE cont_category > 12
     * </code>
     *
     * @param     mixed $contCategory The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContCategory($contCategory = null, $comparison = null)
    {
        if (is_array($contCategory)) {
            $useMinMax = false;
            if (isset($contCategory['min'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_CATEGORY, $contCategory['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contCategory['max'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_CATEGORY, $contCategory['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_CATEGORY, $contCategory, $comparison);
    }

    /**
     * Filter the query on the cont_imgs column
     *
     * Example usage:
     * <code>
     * $query->filterByContImgs('fooValue');   // WHERE cont_imgs = 'fooValue'
     * $query->filterByContImgs('%fooValue%', Criteria::LIKE); // WHERE cont_imgs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contImgs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContImgs($contImgs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contImgs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_IMGS, $contImgs, $comparison);
    }

    /**
     * Filter the query on the cont_order column
     *
     * Example usage:
     * <code>
     * $query->filterByContOrder(1234); // WHERE cont_order = 1234
     * $query->filterByContOrder(array(12, 34)); // WHERE cont_order IN (12, 34)
     * $query->filterByContOrder(array('min' => 12)); // WHERE cont_order > 12
     * </code>
     *
     * @param     mixed $contOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function filterByContOrder($contOrder = null, $comparison = null)
    {
        if (is_array($contOrder)) {
            $useMinMax = false;
            if (isset($contOrder['min'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_ORDER, $contOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contOrder['max'])) {
                $this->addUsingAlias(GradioContentTableMap::COL_CONT_ORDER, $contOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioContentTableMap::COL_CONT_ORDER, $contOrder, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioContent $gradioContent Object to remove from the list of results
     *
     * @return $this|ChildGradioContentQuery The current query, for fluid interface
     */
    public function prune($gradioContent = null)
    {
        if ($gradioContent) {
            $this->addUsingAlias(GradioContentTableMap::COL_CONT_ID, $gradioContent->getContId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_content table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioContentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioContentTableMap::clearInstancePool();
            GradioContentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioContentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioContentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioContentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioContentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioContentQuery
