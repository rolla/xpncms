<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioShows as ChildGradioShows;
use Model\Main\GradioShowsQuery as ChildGradioShowsQuery;
use Model\Main\Map\GradioShowsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_shows' table.
 *
 *
 *
 * @method     ChildGradioShowsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioShowsQuery orderByArtist($order = Criteria::ASC) Order by the artist column
 * @method     ChildGradioShowsQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildGradioShowsQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildGradioShowsQuery orderByImage($order = Criteria::ASC) Order by the image column
 * @method     ChildGradioShowsQuery orderByAlias($order = Criteria::ASC) Order by the alias column
 * @method     ChildGradioShowsQuery orderByModeratorUserId($order = Criteria::ASC) Order by the moderator_user_id column
 * @method     ChildGradioShowsQuery orderByDirectorUserId($order = Criteria::ASC) Order by the director_user_id column
 * @method     ChildGradioShowsQuery orderByPlaysCount($order = Criteria::ASC) Order by the plays_count column
 * @method     ChildGradioShowsQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildGradioShowsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildGradioShowsQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildGradioShowsQuery groupById() Group by the id column
 * @method     ChildGradioShowsQuery groupByArtist() Group by the artist column
 * @method     ChildGradioShowsQuery groupByTitle() Group by the title column
 * @method     ChildGradioShowsQuery groupByDescription() Group by the description column
 * @method     ChildGradioShowsQuery groupByImage() Group by the image column
 * @method     ChildGradioShowsQuery groupByAlias() Group by the alias column
 * @method     ChildGradioShowsQuery groupByModeratorUserId() Group by the moderator_user_id column
 * @method     ChildGradioShowsQuery groupByDirectorUserId() Group by the director_user_id column
 * @method     ChildGradioShowsQuery groupByPlaysCount() Group by the plays_count column
 * @method     ChildGradioShowsQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildGradioShowsQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildGradioShowsQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildGradioShowsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioShowsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioShowsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioShowsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioShowsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioShowsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioShows findOne(ConnectionInterface $con = null) Return the first ChildGradioShows matching the query
 * @method     ChildGradioShows findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioShows matching the query, or a new ChildGradioShows object populated from the query conditions when no match is found
 *
 * @method     ChildGradioShows findOneById(int $id) Return the first ChildGradioShows filtered by the id column
 * @method     ChildGradioShows findOneByArtist(string $artist) Return the first ChildGradioShows filtered by the artist column
 * @method     ChildGradioShows findOneByTitle(string $title) Return the first ChildGradioShows filtered by the title column
 * @method     ChildGradioShows findOneByDescription(string $description) Return the first ChildGradioShows filtered by the description column
 * @method     ChildGradioShows findOneByImage(string $image) Return the first ChildGradioShows filtered by the image column
 * @method     ChildGradioShows findOneByAlias(string $alias) Return the first ChildGradioShows filtered by the alias column
 * @method     ChildGradioShows findOneByModeratorUserId(int $moderator_user_id) Return the first ChildGradioShows filtered by the moderator_user_id column
 * @method     ChildGradioShows findOneByDirectorUserId(int $director_user_id) Return the first ChildGradioShows filtered by the director_user_id column
 * @method     ChildGradioShows findOneByPlaysCount(int $plays_count) Return the first ChildGradioShows filtered by the plays_count column
 * @method     ChildGradioShows findOneByCreatedAt(string $created_at) Return the first ChildGradioShows filtered by the created_at column
 * @method     ChildGradioShows findOneByUpdatedAt(string $updated_at) Return the first ChildGradioShows filtered by the updated_at column
 * @method     ChildGradioShows findOneByDeletedAt(string $deleted_at) Return the first ChildGradioShows filtered by the deleted_at column *

 * @method     ChildGradioShows requirePk($key, ConnectionInterface $con = null) Return the ChildGradioShows by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOne(ConnectionInterface $con = null) Return the first ChildGradioShows matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioShows requireOneById(int $id) Return the first ChildGradioShows filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByArtist(string $artist) Return the first ChildGradioShows filtered by the artist column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByTitle(string $title) Return the first ChildGradioShows filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByDescription(string $description) Return the first ChildGradioShows filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByImage(string $image) Return the first ChildGradioShows filtered by the image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByAlias(string $alias) Return the first ChildGradioShows filtered by the alias column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByModeratorUserId(int $moderator_user_id) Return the first ChildGradioShows filtered by the moderator_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByDirectorUserId(int $director_user_id) Return the first ChildGradioShows filtered by the director_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByPlaysCount(int $plays_count) Return the first ChildGradioShows filtered by the plays_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByCreatedAt(string $created_at) Return the first ChildGradioShows filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByUpdatedAt(string $updated_at) Return the first ChildGradioShows filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioShows requireOneByDeletedAt(string $deleted_at) Return the first ChildGradioShows filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioShows[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioShows objects based on current ModelCriteria
 * @method     ChildGradioShows[]|ObjectCollection findById(int $id) Return ChildGradioShows objects filtered by the id column
 * @method     ChildGradioShows[]|ObjectCollection findByArtist(string $artist) Return ChildGradioShows objects filtered by the artist column
 * @method     ChildGradioShows[]|ObjectCollection findByTitle(string $title) Return ChildGradioShows objects filtered by the title column
 * @method     ChildGradioShows[]|ObjectCollection findByDescription(string $description) Return ChildGradioShows objects filtered by the description column
 * @method     ChildGradioShows[]|ObjectCollection findByImage(string $image) Return ChildGradioShows objects filtered by the image column
 * @method     ChildGradioShows[]|ObjectCollection findByAlias(string $alias) Return ChildGradioShows objects filtered by the alias column
 * @method     ChildGradioShows[]|ObjectCollection findByModeratorUserId(int $moderator_user_id) Return ChildGradioShows objects filtered by the moderator_user_id column
 * @method     ChildGradioShows[]|ObjectCollection findByDirectorUserId(int $director_user_id) Return ChildGradioShows objects filtered by the director_user_id column
 * @method     ChildGradioShows[]|ObjectCollection findByPlaysCount(int $plays_count) Return ChildGradioShows objects filtered by the plays_count column
 * @method     ChildGradioShows[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildGradioShows objects filtered by the created_at column
 * @method     ChildGradioShows[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildGradioShows objects filtered by the updated_at column
 * @method     ChildGradioShows[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildGradioShows objects filtered by the deleted_at column
 * @method     ChildGradioShows[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioShowsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioShowsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioShows', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioShowsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioShowsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioShowsQuery) {
            return $criteria;
        }
        $query = new ChildGradioShowsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioShows|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioShowsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioShowsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioShows A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, artist, title, description, image, alias, moderator_user_id, director_user_id, plays_count, created_at, updated_at, deleted_at FROM gradio_shows WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioShows $obj */
            $obj = new ChildGradioShows();
            $obj->hydrate($row);
            GradioShowsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioShows|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioShowsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioShowsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the artist column
     *
     * Example usage:
     * <code>
     * $query->filterByArtist('fooValue');   // WHERE artist = 'fooValue'
     * $query->filterByArtist('%fooValue%', Criteria::LIKE); // WHERE artist LIKE '%fooValue%'
     * </code>
     *
     * @param     string $artist The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByArtist($artist = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($artist)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_ARTIST, $artist, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the image column
     *
     * Example usage:
     * <code>
     * $query->filterByImage('fooValue');   // WHERE image = 'fooValue'
     * $query->filterByImage('%fooValue%', Criteria::LIKE); // WHERE image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $image The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByImage($image = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($image)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_IMAGE, $image, $comparison);
    }

    /**
     * Filter the query on the alias column
     *
     * Example usage:
     * <code>
     * $query->filterByAlias('fooValue');   // WHERE alias = 'fooValue'
     * $query->filterByAlias('%fooValue%', Criteria::LIKE); // WHERE alias LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alias The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByAlias($alias = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alias)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_ALIAS, $alias, $comparison);
    }

    /**
     * Filter the query on the moderator_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByModeratorUserId(1234); // WHERE moderator_user_id = 1234
     * $query->filterByModeratorUserId(array(12, 34)); // WHERE moderator_user_id IN (12, 34)
     * $query->filterByModeratorUserId(array('min' => 12)); // WHERE moderator_user_id > 12
     * </code>
     *
     * @param     mixed $moderatorUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByModeratorUserId($moderatorUserId = null, $comparison = null)
    {
        if (is_array($moderatorUserId)) {
            $useMinMax = false;
            if (isset($moderatorUserId['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_MODERATOR_USER_ID, $moderatorUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moderatorUserId['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_MODERATOR_USER_ID, $moderatorUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_MODERATOR_USER_ID, $moderatorUserId, $comparison);
    }

    /**
     * Filter the query on the director_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDirectorUserId(1234); // WHERE director_user_id = 1234
     * $query->filterByDirectorUserId(array(12, 34)); // WHERE director_user_id IN (12, 34)
     * $query->filterByDirectorUserId(array('min' => 12)); // WHERE director_user_id > 12
     * </code>
     *
     * @param     mixed $directorUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByDirectorUserId($directorUserId = null, $comparison = null)
    {
        if (is_array($directorUserId)) {
            $useMinMax = false;
            if (isset($directorUserId['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_DIRECTOR_USER_ID, $directorUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($directorUserId['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_DIRECTOR_USER_ID, $directorUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_DIRECTOR_USER_ID, $directorUserId, $comparison);
    }

    /**
     * Filter the query on the plays_count column
     *
     * Example usage:
     * <code>
     * $query->filterByPlaysCount(1234); // WHERE plays_count = 1234
     * $query->filterByPlaysCount(array(12, 34)); // WHERE plays_count IN (12, 34)
     * $query->filterByPlaysCount(array('min' => 12)); // WHERE plays_count > 12
     * </code>
     *
     * @param     mixed $playsCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByPlaysCount($playsCount = null, $comparison = null)
    {
        if (is_array($playsCount)) {
            $useMinMax = false;
            if (isset($playsCount['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_PLAYS_COUNT, $playsCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($playsCount['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_PLAYS_COUNT, $playsCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_PLAYS_COUNT, $playsCount, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(GradioShowsTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioShowsTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioShows $gradioShows Object to remove from the list of results
     *
     * @return $this|ChildGradioShowsQuery The current query, for fluid interface
     */
    public function prune($gradioShows = null)
    {
        if ($gradioShows) {
            $this->addUsingAlias(GradioShowsTableMap::COL_ID, $gradioShows->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_shows table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioShowsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioShowsTableMap::clearInstancePool();
            GradioShowsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioShowsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioShowsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioShowsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioShowsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioShowsQuery
