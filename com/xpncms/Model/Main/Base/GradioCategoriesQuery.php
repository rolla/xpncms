<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioCategories as ChildGradioCategories;
use Model\Main\GradioCategoriesQuery as ChildGradioCategoriesQuery;
use Model\Main\Map\GradioCategoriesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_categories' table.
 *
 *
 *
 * @method     ChildGradioCategoriesQuery orderByCatId($order = Criteria::ASC) Order by the cat_id column
 * @method     ChildGradioCategoriesQuery orderByCatName($order = Criteria::ASC) Order by the cat_name column
 * @method     ChildGradioCategoriesQuery orderByCatContent($order = Criteria::ASC) Order by the cat_content column
 * @method     ChildGradioCategoriesQuery orderByCatAlias($order = Criteria::ASC) Order by the cat_alias column
 * @method     ChildGradioCategoriesQuery orderByCatAdded($order = Criteria::ASC) Order by the cat_added column
 * @method     ChildGradioCategoriesQuery orderByCatUpdated($order = Criteria::ASC) Order by the cat_updated column
 * @method     ChildGradioCategoriesQuery orderByCatClass($order = Criteria::ASC) Order by the cat_class column
 * @method     ChildGradioCategoriesQuery orderByCatParent($order = Criteria::ASC) Order by the cat_parent column
 * @method     ChildGradioCategoriesQuery orderByCatImgs($order = Criteria::ASC) Order by the cat_imgs column
 * @method     ChildGradioCategoriesQuery orderByCatOrder($order = Criteria::ASC) Order by the cat_order column
 *
 * @method     ChildGradioCategoriesQuery groupByCatId() Group by the cat_id column
 * @method     ChildGradioCategoriesQuery groupByCatName() Group by the cat_name column
 * @method     ChildGradioCategoriesQuery groupByCatContent() Group by the cat_content column
 * @method     ChildGradioCategoriesQuery groupByCatAlias() Group by the cat_alias column
 * @method     ChildGradioCategoriesQuery groupByCatAdded() Group by the cat_added column
 * @method     ChildGradioCategoriesQuery groupByCatUpdated() Group by the cat_updated column
 * @method     ChildGradioCategoriesQuery groupByCatClass() Group by the cat_class column
 * @method     ChildGradioCategoriesQuery groupByCatParent() Group by the cat_parent column
 * @method     ChildGradioCategoriesQuery groupByCatImgs() Group by the cat_imgs column
 * @method     ChildGradioCategoriesQuery groupByCatOrder() Group by the cat_order column
 *
 * @method     ChildGradioCategoriesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioCategoriesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioCategoriesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioCategoriesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioCategoriesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioCategoriesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioCategories findOne(ConnectionInterface $con = null) Return the first ChildGradioCategories matching the query
 * @method     ChildGradioCategories findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioCategories matching the query, or a new ChildGradioCategories object populated from the query conditions when no match is found
 *
 * @method     ChildGradioCategories findOneByCatId(int $cat_id) Return the first ChildGradioCategories filtered by the cat_id column
 * @method     ChildGradioCategories findOneByCatName(string $cat_name) Return the first ChildGradioCategories filtered by the cat_name column
 * @method     ChildGradioCategories findOneByCatContent(string $cat_content) Return the first ChildGradioCategories filtered by the cat_content column
 * @method     ChildGradioCategories findOneByCatAlias(string $cat_alias) Return the first ChildGradioCategories filtered by the cat_alias column
 * @method     ChildGradioCategories findOneByCatAdded(string $cat_added) Return the first ChildGradioCategories filtered by the cat_added column
 * @method     ChildGradioCategories findOneByCatUpdated(string $cat_updated) Return the first ChildGradioCategories filtered by the cat_updated column
 * @method     ChildGradioCategories findOneByCatClass(int $cat_class) Return the first ChildGradioCategories filtered by the cat_class column
 * @method     ChildGradioCategories findOneByCatParent(int $cat_parent) Return the first ChildGradioCategories filtered by the cat_parent column
 * @method     ChildGradioCategories findOneByCatImgs(string $cat_imgs) Return the first ChildGradioCategories filtered by the cat_imgs column
 * @method     ChildGradioCategories findOneByCatOrder(int $cat_order) Return the first ChildGradioCategories filtered by the cat_order column *

 * @method     ChildGradioCategories requirePk($key, ConnectionInterface $con = null) Return the ChildGradioCategories by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOne(ConnectionInterface $con = null) Return the first ChildGradioCategories matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioCategories requireOneByCatId(int $cat_id) Return the first ChildGradioCategories filtered by the cat_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatName(string $cat_name) Return the first ChildGradioCategories filtered by the cat_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatContent(string $cat_content) Return the first ChildGradioCategories filtered by the cat_content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatAlias(string $cat_alias) Return the first ChildGradioCategories filtered by the cat_alias column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatAdded(string $cat_added) Return the first ChildGradioCategories filtered by the cat_added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatUpdated(string $cat_updated) Return the first ChildGradioCategories filtered by the cat_updated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatClass(int $cat_class) Return the first ChildGradioCategories filtered by the cat_class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatParent(int $cat_parent) Return the first ChildGradioCategories filtered by the cat_parent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatImgs(string $cat_imgs) Return the first ChildGradioCategories filtered by the cat_imgs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioCategories requireOneByCatOrder(int $cat_order) Return the first ChildGradioCategories filtered by the cat_order column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioCategories[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioCategories objects based on current ModelCriteria
 * @method     ChildGradioCategories[]|ObjectCollection findByCatId(int $cat_id) Return ChildGradioCategories objects filtered by the cat_id column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatName(string $cat_name) Return ChildGradioCategories objects filtered by the cat_name column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatContent(string $cat_content) Return ChildGradioCategories objects filtered by the cat_content column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatAlias(string $cat_alias) Return ChildGradioCategories objects filtered by the cat_alias column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatAdded(string $cat_added) Return ChildGradioCategories objects filtered by the cat_added column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatUpdated(string $cat_updated) Return ChildGradioCategories objects filtered by the cat_updated column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatClass(int $cat_class) Return ChildGradioCategories objects filtered by the cat_class column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatParent(int $cat_parent) Return ChildGradioCategories objects filtered by the cat_parent column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatImgs(string $cat_imgs) Return ChildGradioCategories objects filtered by the cat_imgs column
 * @method     ChildGradioCategories[]|ObjectCollection findByCatOrder(int $cat_order) Return ChildGradioCategories objects filtered by the cat_order column
 * @method     ChildGradioCategories[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioCategoriesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioCategoriesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioCategories', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioCategoriesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioCategoriesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioCategoriesQuery) {
            return $criteria;
        }
        $query = new ChildGradioCategoriesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioCategories|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioCategoriesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioCategoriesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioCategories A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT cat_id, cat_name, cat_content, cat_alias, cat_added, cat_updated, cat_class, cat_parent, cat_imgs, cat_order FROM gradio_categories WHERE cat_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioCategories $obj */
            $obj = new ChildGradioCategories();
            $obj->hydrate($row);
            GradioCategoriesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioCategories|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the cat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCatId(1234); // WHERE cat_id = 1234
     * $query->filterByCatId(array(12, 34)); // WHERE cat_id IN (12, 34)
     * $query->filterByCatId(array('min' => 12)); // WHERE cat_id > 12
     * </code>
     *
     * @param     mixed $catId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatId($catId = null, $comparison = null)
    {
        if (is_array($catId)) {
            $useMinMax = false;
            if (isset($catId['min'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ID, $catId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catId['max'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ID, $catId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ID, $catId, $comparison);
    }

    /**
     * Filter the query on the cat_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCatName('fooValue');   // WHERE cat_name = 'fooValue'
     * $query->filterByCatName('%fooValue%', Criteria::LIKE); // WHERE cat_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $catName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatName($catName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($catName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_NAME, $catName, $comparison);
    }

    /**
     * Filter the query on the cat_content column
     *
     * Example usage:
     * <code>
     * $query->filterByCatContent('fooValue');   // WHERE cat_content = 'fooValue'
     * $query->filterByCatContent('%fooValue%', Criteria::LIKE); // WHERE cat_content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $catContent The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatContent($catContent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($catContent)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_CONTENT, $catContent, $comparison);
    }

    /**
     * Filter the query on the cat_alias column
     *
     * Example usage:
     * <code>
     * $query->filterByCatAlias('fooValue');   // WHERE cat_alias = 'fooValue'
     * $query->filterByCatAlias('%fooValue%', Criteria::LIKE); // WHERE cat_alias LIKE '%fooValue%'
     * </code>
     *
     * @param     string $catAlias The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatAlias($catAlias = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($catAlias)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ALIAS, $catAlias, $comparison);
    }

    /**
     * Filter the query on the cat_added column
     *
     * Example usage:
     * <code>
     * $query->filterByCatAdded('2011-03-14'); // WHERE cat_added = '2011-03-14'
     * $query->filterByCatAdded('now'); // WHERE cat_added = '2011-03-14'
     * $query->filterByCatAdded(array('max' => 'yesterday')); // WHERE cat_added > '2011-03-13'
     * </code>
     *
     * @param     mixed $catAdded The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatAdded($catAdded = null, $comparison = null)
    {
        if (is_array($catAdded)) {
            $useMinMax = false;
            if (isset($catAdded['min'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ADDED, $catAdded['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catAdded['max'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ADDED, $catAdded['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ADDED, $catAdded, $comparison);
    }

    /**
     * Filter the query on the cat_updated column
     *
     * Example usage:
     * <code>
     * $query->filterByCatUpdated('2011-03-14'); // WHERE cat_updated = '2011-03-14'
     * $query->filterByCatUpdated('now'); // WHERE cat_updated = '2011-03-14'
     * $query->filterByCatUpdated(array('max' => 'yesterday')); // WHERE cat_updated > '2011-03-13'
     * </code>
     *
     * @param     mixed $catUpdated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatUpdated($catUpdated = null, $comparison = null)
    {
        if (is_array($catUpdated)) {
            $useMinMax = false;
            if (isset($catUpdated['min'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_UPDATED, $catUpdated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catUpdated['max'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_UPDATED, $catUpdated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_UPDATED, $catUpdated, $comparison);
    }

    /**
     * Filter the query on the cat_class column
     *
     * Example usage:
     * <code>
     * $query->filterByCatClass(1234); // WHERE cat_class = 1234
     * $query->filterByCatClass(array(12, 34)); // WHERE cat_class IN (12, 34)
     * $query->filterByCatClass(array('min' => 12)); // WHERE cat_class > 12
     * </code>
     *
     * @param     mixed $catClass The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatClass($catClass = null, $comparison = null)
    {
        if (is_array($catClass)) {
            $useMinMax = false;
            if (isset($catClass['min'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_CLASS, $catClass['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catClass['max'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_CLASS, $catClass['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_CLASS, $catClass, $comparison);
    }

    /**
     * Filter the query on the cat_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByCatParent(1234); // WHERE cat_parent = 1234
     * $query->filterByCatParent(array(12, 34)); // WHERE cat_parent IN (12, 34)
     * $query->filterByCatParent(array('min' => 12)); // WHERE cat_parent > 12
     * </code>
     *
     * @param     mixed $catParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatParent($catParent = null, $comparison = null)
    {
        if (is_array($catParent)) {
            $useMinMax = false;
            if (isset($catParent['min'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_PARENT, $catParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catParent['max'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_PARENT, $catParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_PARENT, $catParent, $comparison);
    }

    /**
     * Filter the query on the cat_imgs column
     *
     * Example usage:
     * <code>
     * $query->filterByCatImgs('fooValue');   // WHERE cat_imgs = 'fooValue'
     * $query->filterByCatImgs('%fooValue%', Criteria::LIKE); // WHERE cat_imgs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $catImgs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatImgs($catImgs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($catImgs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_IMGS, $catImgs, $comparison);
    }

    /**
     * Filter the query on the cat_order column
     *
     * Example usage:
     * <code>
     * $query->filterByCatOrder(1234); // WHERE cat_order = 1234
     * $query->filterByCatOrder(array(12, 34)); // WHERE cat_order IN (12, 34)
     * $query->filterByCatOrder(array('min' => 12)); // WHERE cat_order > 12
     * </code>
     *
     * @param     mixed $catOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function filterByCatOrder($catOrder = null, $comparison = null)
    {
        if (is_array($catOrder)) {
            $useMinMax = false;
            if (isset($catOrder['min'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ORDER, $catOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catOrder['max'])) {
                $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ORDER, $catOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ORDER, $catOrder, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioCategories $gradioCategories Object to remove from the list of results
     *
     * @return $this|ChildGradioCategoriesQuery The current query, for fluid interface
     */
    public function prune($gradioCategories = null)
    {
        if ($gradioCategories) {
            $this->addUsingAlias(GradioCategoriesTableMap::COL_CAT_ID, $gradioCategories->getCatId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_categories table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioCategoriesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioCategoriesTableMap::clearInstancePool();
            GradioCategoriesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioCategoriesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioCategoriesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioCategoriesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioCategoriesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioCategoriesQuery
