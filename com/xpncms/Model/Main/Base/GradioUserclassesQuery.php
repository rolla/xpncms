<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioUserclasses as ChildGradioUserclasses;
use Model\Main\GradioUserclassesQuery as ChildGradioUserclassesQuery;
use Model\Main\Map\GradioUserclassesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_userclasses' table.
 *
 *
 *
 * @method     ChildGradioUserclassesQuery orderByUserclassId($order = Criteria::ASC) Order by the userclass_id column
 * @method     ChildGradioUserclassesQuery orderByUserclassName($order = Criteria::ASC) Order by the userclass_name column
 * @method     ChildGradioUserclassesQuery orderByUserclassDescription($order = Criteria::ASC) Order by the userclass_description column
 * @method     ChildGradioUserclassesQuery orderByUserclassEditclass($order = Criteria::ASC) Order by the userclass_editclass column
 * @method     ChildGradioUserclassesQuery orderByUserclassParent($order = Criteria::ASC) Order by the userclass_parent column
 * @method     ChildGradioUserclassesQuery orderByUserclassAccum($order = Criteria::ASC) Order by the userclass_accum column
 * @method     ChildGradioUserclassesQuery orderByUserclassVisibility($order = Criteria::ASC) Order by the userclass_visibility column
 * @method     ChildGradioUserclassesQuery orderByUserclassType($order = Criteria::ASC) Order by the userclass_type column
 * @method     ChildGradioUserclassesQuery orderByUserclassIcon($order = Criteria::ASC) Order by the userclass_icon column
 *
 * @method     ChildGradioUserclassesQuery groupByUserclassId() Group by the userclass_id column
 * @method     ChildGradioUserclassesQuery groupByUserclassName() Group by the userclass_name column
 * @method     ChildGradioUserclassesQuery groupByUserclassDescription() Group by the userclass_description column
 * @method     ChildGradioUserclassesQuery groupByUserclassEditclass() Group by the userclass_editclass column
 * @method     ChildGradioUserclassesQuery groupByUserclassParent() Group by the userclass_parent column
 * @method     ChildGradioUserclassesQuery groupByUserclassAccum() Group by the userclass_accum column
 * @method     ChildGradioUserclassesQuery groupByUserclassVisibility() Group by the userclass_visibility column
 * @method     ChildGradioUserclassesQuery groupByUserclassType() Group by the userclass_type column
 * @method     ChildGradioUserclassesQuery groupByUserclassIcon() Group by the userclass_icon column
 *
 * @method     ChildGradioUserclassesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioUserclassesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioUserclassesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioUserclassesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioUserclassesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioUserclassesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioUserclasses findOne(ConnectionInterface $con = null) Return the first ChildGradioUserclasses matching the query
 * @method     ChildGradioUserclasses findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioUserclasses matching the query, or a new ChildGradioUserclasses object populated from the query conditions when no match is found
 *
 * @method     ChildGradioUserclasses findOneByUserclassId(int $userclass_id) Return the first ChildGradioUserclasses filtered by the userclass_id column
 * @method     ChildGradioUserclasses findOneByUserclassName(string $userclass_name) Return the first ChildGradioUserclasses filtered by the userclass_name column
 * @method     ChildGradioUserclasses findOneByUserclassDescription(string $userclass_description) Return the first ChildGradioUserclasses filtered by the userclass_description column
 * @method     ChildGradioUserclasses findOneByUserclassEditclass(int $userclass_editclass) Return the first ChildGradioUserclasses filtered by the userclass_editclass column
 * @method     ChildGradioUserclasses findOneByUserclassParent(int $userclass_parent) Return the first ChildGradioUserclasses filtered by the userclass_parent column
 * @method     ChildGradioUserclasses findOneByUserclassAccum(string $userclass_accum) Return the first ChildGradioUserclasses filtered by the userclass_accum column
 * @method     ChildGradioUserclasses findOneByUserclassVisibility(int $userclass_visibility) Return the first ChildGradioUserclasses filtered by the userclass_visibility column
 * @method     ChildGradioUserclasses findOneByUserclassType(boolean $userclass_type) Return the first ChildGradioUserclasses filtered by the userclass_type column
 * @method     ChildGradioUserclasses findOneByUserclassIcon(string $userclass_icon) Return the first ChildGradioUserclasses filtered by the userclass_icon column *

 * @method     ChildGradioUserclasses requirePk($key, ConnectionInterface $con = null) Return the ChildGradioUserclasses by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOne(ConnectionInterface $con = null) Return the first ChildGradioUserclasses matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioUserclasses requireOneByUserclassId(int $userclass_id) Return the first ChildGradioUserclasses filtered by the userclass_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassName(string $userclass_name) Return the first ChildGradioUserclasses filtered by the userclass_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassDescription(string $userclass_description) Return the first ChildGradioUserclasses filtered by the userclass_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassEditclass(int $userclass_editclass) Return the first ChildGradioUserclasses filtered by the userclass_editclass column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassParent(int $userclass_parent) Return the first ChildGradioUserclasses filtered by the userclass_parent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassAccum(string $userclass_accum) Return the first ChildGradioUserclasses filtered by the userclass_accum column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassVisibility(int $userclass_visibility) Return the first ChildGradioUserclasses filtered by the userclass_visibility column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassType(boolean $userclass_type) Return the first ChildGradioUserclasses filtered by the userclass_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserclasses requireOneByUserclassIcon(string $userclass_icon) Return the first ChildGradioUserclasses filtered by the userclass_icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioUserclasses[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioUserclasses objects based on current ModelCriteria
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassId(int $userclass_id) Return ChildGradioUserclasses objects filtered by the userclass_id column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassName(string $userclass_name) Return ChildGradioUserclasses objects filtered by the userclass_name column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassDescription(string $userclass_description) Return ChildGradioUserclasses objects filtered by the userclass_description column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassEditclass(int $userclass_editclass) Return ChildGradioUserclasses objects filtered by the userclass_editclass column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassParent(int $userclass_parent) Return ChildGradioUserclasses objects filtered by the userclass_parent column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassAccum(string $userclass_accum) Return ChildGradioUserclasses objects filtered by the userclass_accum column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassVisibility(int $userclass_visibility) Return ChildGradioUserclasses objects filtered by the userclass_visibility column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassType(boolean $userclass_type) Return ChildGradioUserclasses objects filtered by the userclass_type column
 * @method     ChildGradioUserclasses[]|ObjectCollection findByUserclassIcon(string $userclass_icon) Return ChildGradioUserclasses objects filtered by the userclass_icon column
 * @method     ChildGradioUserclasses[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioUserclassesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioUserclassesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioUserclasses', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioUserclassesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioUserclassesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioUserclassesQuery) {
            return $criteria;
        }
        $query = new ChildGradioUserclassesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioUserclasses|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioUserclassesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioUserclasses A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT userclass_id, userclass_name, userclass_description, userclass_editclass, userclass_parent, userclass_accum, userclass_visibility, userclass_type, userclass_icon FROM gradio_userclasses WHERE userclass_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioUserclasses $obj */
            $obj = new ChildGradioUserclasses();
            $obj->hydrate($row);
            GradioUserclassesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioUserclasses|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the userclass_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassId(1234); // WHERE userclass_id = 1234
     * $query->filterByUserclassId(array(12, 34)); // WHERE userclass_id IN (12, 34)
     * $query->filterByUserclassId(array('min' => 12)); // WHERE userclass_id > 12
     * </code>
     *
     * @param     mixed $userclassId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassId($userclassId = null, $comparison = null)
    {
        if (is_array($userclassId)) {
            $useMinMax = false;
            if (isset($userclassId['min'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ID, $userclassId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userclassId['max'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ID, $userclassId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ID, $userclassId, $comparison);
    }

    /**
     * Filter the query on the userclass_name column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassName('fooValue');   // WHERE userclass_name = 'fooValue'
     * $query->filterByUserclassName('%fooValue%', Criteria::LIKE); // WHERE userclass_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userclassName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassName($userclassName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userclassName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_NAME, $userclassName, $comparison);
    }

    /**
     * Filter the query on the userclass_description column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassDescription('fooValue');   // WHERE userclass_description = 'fooValue'
     * $query->filterByUserclassDescription('%fooValue%', Criteria::LIKE); // WHERE userclass_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userclassDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassDescription($userclassDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userclassDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION, $userclassDescription, $comparison);
    }

    /**
     * Filter the query on the userclass_editclass column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassEditclass(1234); // WHERE userclass_editclass = 1234
     * $query->filterByUserclassEditclass(array(12, 34)); // WHERE userclass_editclass IN (12, 34)
     * $query->filterByUserclassEditclass(array('min' => 12)); // WHERE userclass_editclass > 12
     * </code>
     *
     * @param     mixed $userclassEditclass The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassEditclass($userclassEditclass = null, $comparison = null)
    {
        if (is_array($userclassEditclass)) {
            $useMinMax = false;
            if (isset($userclassEditclass['min'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS, $userclassEditclass['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userclassEditclass['max'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS, $userclassEditclass['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS, $userclassEditclass, $comparison);
    }

    /**
     * Filter the query on the userclass_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassParent(1234); // WHERE userclass_parent = 1234
     * $query->filterByUserclassParent(array(12, 34)); // WHERE userclass_parent IN (12, 34)
     * $query->filterByUserclassParent(array('min' => 12)); // WHERE userclass_parent > 12
     * </code>
     *
     * @param     mixed $userclassParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassParent($userclassParent = null, $comparison = null)
    {
        if (is_array($userclassParent)) {
            $useMinMax = false;
            if (isset($userclassParent['min'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_PARENT, $userclassParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userclassParent['max'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_PARENT, $userclassParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_PARENT, $userclassParent, $comparison);
    }

    /**
     * Filter the query on the userclass_accum column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassAccum('fooValue');   // WHERE userclass_accum = 'fooValue'
     * $query->filterByUserclassAccum('%fooValue%', Criteria::LIKE); // WHERE userclass_accum LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userclassAccum The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassAccum($userclassAccum = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userclassAccum)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ACCUM, $userclassAccum, $comparison);
    }

    /**
     * Filter the query on the userclass_visibility column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassVisibility(1234); // WHERE userclass_visibility = 1234
     * $query->filterByUserclassVisibility(array(12, 34)); // WHERE userclass_visibility IN (12, 34)
     * $query->filterByUserclassVisibility(array('min' => 12)); // WHERE userclass_visibility > 12
     * </code>
     *
     * @param     mixed $userclassVisibility The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassVisibility($userclassVisibility = null, $comparison = null)
    {
        if (is_array($userclassVisibility)) {
            $useMinMax = false;
            if (isset($userclassVisibility['min'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY, $userclassVisibility['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userclassVisibility['max'])) {
                $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY, $userclassVisibility['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY, $userclassVisibility, $comparison);
    }

    /**
     * Filter the query on the userclass_type column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassType(true); // WHERE userclass_type = true
     * $query->filterByUserclassType('yes'); // WHERE userclass_type = true
     * </code>
     *
     * @param     boolean|string $userclassType The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassType($userclassType = null, $comparison = null)
    {
        if (is_string($userclassType)) {
            $userclassType = in_array(strtolower($userclassType), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_TYPE, $userclassType, $comparison);
    }

    /**
     * Filter the query on the userclass_icon column
     *
     * Example usage:
     * <code>
     * $query->filterByUserclassIcon('fooValue');   // WHERE userclass_icon = 'fooValue'
     * $query->filterByUserclassIcon('%fooValue%', Criteria::LIKE); // WHERE userclass_icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userclassIcon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function filterByUserclassIcon($userclassIcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userclassIcon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ICON, $userclassIcon, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioUserclasses $gradioUserclasses Object to remove from the list of results
     *
     * @return $this|ChildGradioUserclassesQuery The current query, for fluid interface
     */
    public function prune($gradioUserclasses = null)
    {
        if ($gradioUserclasses) {
            $this->addUsingAlias(GradioUserclassesTableMap::COL_USERCLASS_ID, $gradioUserclasses->getUserclassId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_userclasses table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioUserclassesTableMap::clearInstancePool();
            GradioUserclassesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioUserclassesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioUserclassesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioUserclassesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioUserclassesQuery
