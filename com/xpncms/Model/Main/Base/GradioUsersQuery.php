<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioUsers as ChildGradioUsers;
use Model\Main\GradioUsersQuery as ChildGradioUsersQuery;
use Model\Main\Map\GradioUsersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_users' table.
 *
 *
 *
 * @method     ChildGradioUsersQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioUsersQuery orderByLatestProvider($order = Criteria::ASC) Order by the latest_provider column
 * @method     ChildGradioUsersQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildGradioUsersQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildGradioUsersQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildGradioUsersQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildGradioUsersQuery orderByDisplayName($order = Criteria::ASC) Order by the display_name column
 * @method     ChildGradioUsersQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildGradioUsersQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildGradioUsersQuery orderByEmailVerified($order = Criteria::ASC) Order by the email_verified column
 * @method     ChildGradioUsersQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildGradioUsersQuery orderByGender($order = Criteria::ASC) Order by the gender column
 * @method     ChildGradioUsersQuery orderByLanguage($order = Criteria::ASC) Order by the language column
 * @method     ChildGradioUsersQuery orderByBirthday($order = Criteria::ASC) Order by the birthday column
 * @method     ChildGradioUsersQuery orderByAge($order = Criteria::ASC) Order by the age column
 * @method     ChildGradioUsersQuery orderByAdult($order = Criteria::ASC) Order by the adult column
 * @method     ChildGradioUsersQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method     ChildGradioUsersQuery orderByCountry($order = Criteria::ASC) Order by the country column
 * @method     ChildGradioUsersQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method     ChildGradioUsersQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildGradioUsersQuery orderByZip($order = Criteria::ASC) Order by the zip column
 * @method     ChildGradioUsersQuery orderByWebsiteUrl($order = Criteria::ASC) Order by the website_url column
 * @method     ChildGradioUsersQuery orderByProfileUrl($order = Criteria::ASC) Order by the profile_url column
 * @method     ChildGradioUsersQuery orderByPhotoUrl($order = Criteria::ASC) Order by the photo_url column
 * @method     ChildGradioUsersQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildGradioUsersQuery orderByVisits($order = Criteria::ASC) Order by the visits column
 * @method     ChildGradioUsersQuery orderByCurrentvisit($order = Criteria::ASC) Order by the currentvisit column
 * @method     ChildGradioUsersQuery orderByLastvisit($order = Criteria::ASC) Order by the lastvisit column
 * @method     ChildGradioUsersQuery orderByUpdated($order = Criteria::ASC) Order by the updated column
 * @method     ChildGradioUsersQuery orderByAdded($order = Criteria::ASC) Order by the added column
 * @method     ChildGradioUsersQuery orderByImg($order = Criteria::ASC) Order by the img column
 * @method     ChildGradioUsersQuery orderByImgs($order = Criteria::ASC) Order by the imgs column
 * @method     ChildGradioUsersQuery orderByImgm($order = Criteria::ASC) Order by the imgm column
 * @method     ChildGradioUsersQuery orderByImgl($order = Criteria::ASC) Order by the imgl column
 * @method     ChildGradioUsersQuery orderByPrefs($order = Criteria::ASC) Order by the prefs column
 * @method     ChildGradioUsersQuery orderByAdmin($order = Criteria::ASC) Order by the admin column
 * @method     ChildGradioUsersQuery orderByPerms($order = Criteria::ASC) Order by the perms column
 * @method     ChildGradioUsersQuery orderByClass($order = Criteria::ASC) Order by the class column
 * @method     ChildGradioUsersQuery orderBySess($order = Criteria::ASC) Order by the sess column
 * @method     ChildGradioUsersQuery orderByBan($order = Criteria::ASC) Order by the ban column
 * @method     ChildGradioUsersQuery orderByDrCreated($order = Criteria::ASC) Order by the dr_created column
 * @method     ChildGradioUsersQuery orderByDrDeleted($order = Criteria::ASC) Order by the dr_deleted column
 * @method     ChildGradioUsersQuery orderByType($order = Criteria::ASC) Order by the type column
 *
 * @method     ChildGradioUsersQuery groupById() Group by the id column
 * @method     ChildGradioUsersQuery groupByLatestProvider() Group by the latest_provider column
 * @method     ChildGradioUsersQuery groupByUsername() Group by the username column
 * @method     ChildGradioUsersQuery groupByPassword() Group by the password column
 * @method     ChildGradioUsersQuery groupByFirstName() Group by the first_name column
 * @method     ChildGradioUsersQuery groupByLastName() Group by the last_name column
 * @method     ChildGradioUsersQuery groupByDisplayName() Group by the display_name column
 * @method     ChildGradioUsersQuery groupByDescription() Group by the description column
 * @method     ChildGradioUsersQuery groupByEmail() Group by the email column
 * @method     ChildGradioUsersQuery groupByEmailVerified() Group by the email_verified column
 * @method     ChildGradioUsersQuery groupByPhone() Group by the phone column
 * @method     ChildGradioUsersQuery groupByGender() Group by the gender column
 * @method     ChildGradioUsersQuery groupByLanguage() Group by the language column
 * @method     ChildGradioUsersQuery groupByBirthday() Group by the birthday column
 * @method     ChildGradioUsersQuery groupByAge() Group by the age column
 * @method     ChildGradioUsersQuery groupByAdult() Group by the adult column
 * @method     ChildGradioUsersQuery groupByAddress() Group by the address column
 * @method     ChildGradioUsersQuery groupByCountry() Group by the country column
 * @method     ChildGradioUsersQuery groupByRegion() Group by the region column
 * @method     ChildGradioUsersQuery groupByCity() Group by the city column
 * @method     ChildGradioUsersQuery groupByZip() Group by the zip column
 * @method     ChildGradioUsersQuery groupByWebsiteUrl() Group by the website_url column
 * @method     ChildGradioUsersQuery groupByProfileUrl() Group by the profile_url column
 * @method     ChildGradioUsersQuery groupByPhotoUrl() Group by the photo_url column
 * @method     ChildGradioUsersQuery groupByIp() Group by the ip column
 * @method     ChildGradioUsersQuery groupByVisits() Group by the visits column
 * @method     ChildGradioUsersQuery groupByCurrentvisit() Group by the currentvisit column
 * @method     ChildGradioUsersQuery groupByLastvisit() Group by the lastvisit column
 * @method     ChildGradioUsersQuery groupByUpdated() Group by the updated column
 * @method     ChildGradioUsersQuery groupByAdded() Group by the added column
 * @method     ChildGradioUsersQuery groupByImg() Group by the img column
 * @method     ChildGradioUsersQuery groupByImgs() Group by the imgs column
 * @method     ChildGradioUsersQuery groupByImgm() Group by the imgm column
 * @method     ChildGradioUsersQuery groupByImgl() Group by the imgl column
 * @method     ChildGradioUsersQuery groupByPrefs() Group by the prefs column
 * @method     ChildGradioUsersQuery groupByAdmin() Group by the admin column
 * @method     ChildGradioUsersQuery groupByPerms() Group by the perms column
 * @method     ChildGradioUsersQuery groupByClass() Group by the class column
 * @method     ChildGradioUsersQuery groupBySess() Group by the sess column
 * @method     ChildGradioUsersQuery groupByBan() Group by the ban column
 * @method     ChildGradioUsersQuery groupByDrCreated() Group by the dr_created column
 * @method     ChildGradioUsersQuery groupByDrDeleted() Group by the dr_deleted column
 * @method     ChildGradioUsersQuery groupByType() Group by the type column
 *
 * @method     ChildGradioUsersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioUsersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioUsersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioUsersQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioUsersQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioUsersQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioUsers findOne(ConnectionInterface $con = null) Return the first ChildGradioUsers matching the query
 * @method     ChildGradioUsers findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioUsers matching the query, or a new ChildGradioUsers object populated from the query conditions when no match is found
 *
 * @method     ChildGradioUsers findOneById(int $id) Return the first ChildGradioUsers filtered by the id column
 * @method     ChildGradioUsers findOneByLatestProvider(string $latest_provider) Return the first ChildGradioUsers filtered by the latest_provider column
 * @method     ChildGradioUsers findOneByUsername(string $username) Return the first ChildGradioUsers filtered by the username column
 * @method     ChildGradioUsers findOneByPassword(string $password) Return the first ChildGradioUsers filtered by the password column
 * @method     ChildGradioUsers findOneByFirstName(string $first_name) Return the first ChildGradioUsers filtered by the first_name column
 * @method     ChildGradioUsers findOneByLastName(string $last_name) Return the first ChildGradioUsers filtered by the last_name column
 * @method     ChildGradioUsers findOneByDisplayName(string $display_name) Return the first ChildGradioUsers filtered by the display_name column
 * @method     ChildGradioUsers findOneByDescription(string $description) Return the first ChildGradioUsers filtered by the description column
 * @method     ChildGradioUsers findOneByEmail(string $email) Return the first ChildGradioUsers filtered by the email column
 * @method     ChildGradioUsers findOneByEmailVerified(string $email_verified) Return the first ChildGradioUsers filtered by the email_verified column
 * @method     ChildGradioUsers findOneByPhone(int $phone) Return the first ChildGradioUsers filtered by the phone column
 * @method     ChildGradioUsers findOneByGender(string $gender) Return the first ChildGradioUsers filtered by the gender column
 * @method     ChildGradioUsers findOneByLanguage(string $language) Return the first ChildGradioUsers filtered by the language column
 * @method     ChildGradioUsers findOneByBirthday(string $birthday) Return the first ChildGradioUsers filtered by the birthday column
 * @method     ChildGradioUsers findOneByAge(int $age) Return the first ChildGradioUsers filtered by the age column
 * @method     ChildGradioUsers findOneByAdult(int $adult) Return the first ChildGradioUsers filtered by the adult column
 * @method     ChildGradioUsers findOneByAddress(string $address) Return the first ChildGradioUsers filtered by the address column
 * @method     ChildGradioUsers findOneByCountry(string $country) Return the first ChildGradioUsers filtered by the country column
 * @method     ChildGradioUsers findOneByRegion(string $region) Return the first ChildGradioUsers filtered by the region column
 * @method     ChildGradioUsers findOneByCity(string $city) Return the first ChildGradioUsers filtered by the city column
 * @method     ChildGradioUsers findOneByZip(string $zip) Return the first ChildGradioUsers filtered by the zip column
 * @method     ChildGradioUsers findOneByWebsiteUrl(string $website_url) Return the first ChildGradioUsers filtered by the website_url column
 * @method     ChildGradioUsers findOneByProfileUrl(string $profile_url) Return the first ChildGradioUsers filtered by the profile_url column
 * @method     ChildGradioUsers findOneByPhotoUrl(string $photo_url) Return the first ChildGradioUsers filtered by the photo_url column
 * @method     ChildGradioUsers findOneByIp(string $ip) Return the first ChildGradioUsers filtered by the ip column
 * @method     ChildGradioUsers findOneByVisits(int $visits) Return the first ChildGradioUsers filtered by the visits column
 * @method     ChildGradioUsers findOneByCurrentvisit(string $currentvisit) Return the first ChildGradioUsers filtered by the currentvisit column
 * @method     ChildGradioUsers findOneByLastvisit(string $lastvisit) Return the first ChildGradioUsers filtered by the lastvisit column
 * @method     ChildGradioUsers findOneByUpdated(string $updated) Return the first ChildGradioUsers filtered by the updated column
 * @method     ChildGradioUsers findOneByAdded(string $added) Return the first ChildGradioUsers filtered by the added column
 * @method     ChildGradioUsers findOneByImg(string $img) Return the first ChildGradioUsers filtered by the img column
 * @method     ChildGradioUsers findOneByImgs(string $imgs) Return the first ChildGradioUsers filtered by the imgs column
 * @method     ChildGradioUsers findOneByImgm(string $imgm) Return the first ChildGradioUsers filtered by the imgm column
 * @method     ChildGradioUsers findOneByImgl(string $imgl) Return the first ChildGradioUsers filtered by the imgl column
 * @method     ChildGradioUsers findOneByPrefs(string $prefs) Return the first ChildGradioUsers filtered by the prefs column
 * @method     ChildGradioUsers findOneByAdmin(int $admin) Return the first ChildGradioUsers filtered by the admin column
 * @method     ChildGradioUsers findOneByPerms(string $perms) Return the first ChildGradioUsers filtered by the perms column
 * @method     ChildGradioUsers findOneByClass(string $class) Return the first ChildGradioUsers filtered by the class column
 * @method     ChildGradioUsers findOneBySess(string $sess) Return the first ChildGradioUsers filtered by the sess column
 * @method     ChildGradioUsers findOneByBan(int $ban) Return the first ChildGradioUsers filtered by the ban column
 * @method     ChildGradioUsers findOneByDrCreated(string $dr_created) Return the first ChildGradioUsers filtered by the dr_created column
 * @method     ChildGradioUsers findOneByDrDeleted(string $dr_deleted) Return the first ChildGradioUsers filtered by the dr_deleted column
 * @method     ChildGradioUsers findOneByType(string $type) Return the first ChildGradioUsers filtered by the type column *

 * @method     ChildGradioUsers requirePk($key, ConnectionInterface $con = null) Return the ChildGradioUsers by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOne(ConnectionInterface $con = null) Return the first ChildGradioUsers matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioUsers requireOneById(int $id) Return the first ChildGradioUsers filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByLatestProvider(string $latest_provider) Return the first ChildGradioUsers filtered by the latest_provider column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByUsername(string $username) Return the first ChildGradioUsers filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByPassword(string $password) Return the first ChildGradioUsers filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByFirstName(string $first_name) Return the first ChildGradioUsers filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByLastName(string $last_name) Return the first ChildGradioUsers filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByDisplayName(string $display_name) Return the first ChildGradioUsers filtered by the display_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByDescription(string $description) Return the first ChildGradioUsers filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByEmail(string $email) Return the first ChildGradioUsers filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByEmailVerified(string $email_verified) Return the first ChildGradioUsers filtered by the email_verified column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByPhone(int $phone) Return the first ChildGradioUsers filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByGender(string $gender) Return the first ChildGradioUsers filtered by the gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByLanguage(string $language) Return the first ChildGradioUsers filtered by the language column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByBirthday(string $birthday) Return the first ChildGradioUsers filtered by the birthday column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByAge(int $age) Return the first ChildGradioUsers filtered by the age column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByAdult(int $adult) Return the first ChildGradioUsers filtered by the adult column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByAddress(string $address) Return the first ChildGradioUsers filtered by the address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByCountry(string $country) Return the first ChildGradioUsers filtered by the country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByRegion(string $region) Return the first ChildGradioUsers filtered by the region column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByCity(string $city) Return the first ChildGradioUsers filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByZip(string $zip) Return the first ChildGradioUsers filtered by the zip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByWebsiteUrl(string $website_url) Return the first ChildGradioUsers filtered by the website_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByProfileUrl(string $profile_url) Return the first ChildGradioUsers filtered by the profile_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByPhotoUrl(string $photo_url) Return the first ChildGradioUsers filtered by the photo_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByIp(string $ip) Return the first ChildGradioUsers filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByVisits(int $visits) Return the first ChildGradioUsers filtered by the visits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByCurrentvisit(string $currentvisit) Return the first ChildGradioUsers filtered by the currentvisit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByLastvisit(string $lastvisit) Return the first ChildGradioUsers filtered by the lastvisit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByUpdated(string $updated) Return the first ChildGradioUsers filtered by the updated column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByAdded(string $added) Return the first ChildGradioUsers filtered by the added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByImg(string $img) Return the first ChildGradioUsers filtered by the img column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByImgs(string $imgs) Return the first ChildGradioUsers filtered by the imgs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByImgm(string $imgm) Return the first ChildGradioUsers filtered by the imgm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByImgl(string $imgl) Return the first ChildGradioUsers filtered by the imgl column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByPrefs(string $prefs) Return the first ChildGradioUsers filtered by the prefs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByAdmin(int $admin) Return the first ChildGradioUsers filtered by the admin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByPerms(string $perms) Return the first ChildGradioUsers filtered by the perms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByClass(string $class) Return the first ChildGradioUsers filtered by the class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneBySess(string $sess) Return the first ChildGradioUsers filtered by the sess column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByBan(int $ban) Return the first ChildGradioUsers filtered by the ban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByDrCreated(string $dr_created) Return the first ChildGradioUsers filtered by the dr_created column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByDrDeleted(string $dr_deleted) Return the first ChildGradioUsers filtered by the dr_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUsers requireOneByType(string $type) Return the first ChildGradioUsers filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioUsers[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioUsers objects based on current ModelCriteria
 * @method     ChildGradioUsers[]|ObjectCollection findById(int $id) Return ChildGradioUsers objects filtered by the id column
 * @method     ChildGradioUsers[]|ObjectCollection findByLatestProvider(string $latest_provider) Return ChildGradioUsers objects filtered by the latest_provider column
 * @method     ChildGradioUsers[]|ObjectCollection findByUsername(string $username) Return ChildGradioUsers objects filtered by the username column
 * @method     ChildGradioUsers[]|ObjectCollection findByPassword(string $password) Return ChildGradioUsers objects filtered by the password column
 * @method     ChildGradioUsers[]|ObjectCollection findByFirstName(string $first_name) Return ChildGradioUsers objects filtered by the first_name column
 * @method     ChildGradioUsers[]|ObjectCollection findByLastName(string $last_name) Return ChildGradioUsers objects filtered by the last_name column
 * @method     ChildGradioUsers[]|ObjectCollection findByDisplayName(string $display_name) Return ChildGradioUsers objects filtered by the display_name column
 * @method     ChildGradioUsers[]|ObjectCollection findByDescription(string $description) Return ChildGradioUsers objects filtered by the description column
 * @method     ChildGradioUsers[]|ObjectCollection findByEmail(string $email) Return ChildGradioUsers objects filtered by the email column
 * @method     ChildGradioUsers[]|ObjectCollection findByEmailVerified(string $email_verified) Return ChildGradioUsers objects filtered by the email_verified column
 * @method     ChildGradioUsers[]|ObjectCollection findByPhone(int $phone) Return ChildGradioUsers objects filtered by the phone column
 * @method     ChildGradioUsers[]|ObjectCollection findByGender(string $gender) Return ChildGradioUsers objects filtered by the gender column
 * @method     ChildGradioUsers[]|ObjectCollection findByLanguage(string $language) Return ChildGradioUsers objects filtered by the language column
 * @method     ChildGradioUsers[]|ObjectCollection findByBirthday(string $birthday) Return ChildGradioUsers objects filtered by the birthday column
 * @method     ChildGradioUsers[]|ObjectCollection findByAge(int $age) Return ChildGradioUsers objects filtered by the age column
 * @method     ChildGradioUsers[]|ObjectCollection findByAdult(int $adult) Return ChildGradioUsers objects filtered by the adult column
 * @method     ChildGradioUsers[]|ObjectCollection findByAddress(string $address) Return ChildGradioUsers objects filtered by the address column
 * @method     ChildGradioUsers[]|ObjectCollection findByCountry(string $country) Return ChildGradioUsers objects filtered by the country column
 * @method     ChildGradioUsers[]|ObjectCollection findByRegion(string $region) Return ChildGradioUsers objects filtered by the region column
 * @method     ChildGradioUsers[]|ObjectCollection findByCity(string $city) Return ChildGradioUsers objects filtered by the city column
 * @method     ChildGradioUsers[]|ObjectCollection findByZip(string $zip) Return ChildGradioUsers objects filtered by the zip column
 * @method     ChildGradioUsers[]|ObjectCollection findByWebsiteUrl(string $website_url) Return ChildGradioUsers objects filtered by the website_url column
 * @method     ChildGradioUsers[]|ObjectCollection findByProfileUrl(string $profile_url) Return ChildGradioUsers objects filtered by the profile_url column
 * @method     ChildGradioUsers[]|ObjectCollection findByPhotoUrl(string $photo_url) Return ChildGradioUsers objects filtered by the photo_url column
 * @method     ChildGradioUsers[]|ObjectCollection findByIp(string $ip) Return ChildGradioUsers objects filtered by the ip column
 * @method     ChildGradioUsers[]|ObjectCollection findByVisits(int $visits) Return ChildGradioUsers objects filtered by the visits column
 * @method     ChildGradioUsers[]|ObjectCollection findByCurrentvisit(string $currentvisit) Return ChildGradioUsers objects filtered by the currentvisit column
 * @method     ChildGradioUsers[]|ObjectCollection findByLastvisit(string $lastvisit) Return ChildGradioUsers objects filtered by the lastvisit column
 * @method     ChildGradioUsers[]|ObjectCollection findByUpdated(string $updated) Return ChildGradioUsers objects filtered by the updated column
 * @method     ChildGradioUsers[]|ObjectCollection findByAdded(string $added) Return ChildGradioUsers objects filtered by the added column
 * @method     ChildGradioUsers[]|ObjectCollection findByImg(string $img) Return ChildGradioUsers objects filtered by the img column
 * @method     ChildGradioUsers[]|ObjectCollection findByImgs(string $imgs) Return ChildGradioUsers objects filtered by the imgs column
 * @method     ChildGradioUsers[]|ObjectCollection findByImgm(string $imgm) Return ChildGradioUsers objects filtered by the imgm column
 * @method     ChildGradioUsers[]|ObjectCollection findByImgl(string $imgl) Return ChildGradioUsers objects filtered by the imgl column
 * @method     ChildGradioUsers[]|ObjectCollection findByPrefs(string $prefs) Return ChildGradioUsers objects filtered by the prefs column
 * @method     ChildGradioUsers[]|ObjectCollection findByAdmin(int $admin) Return ChildGradioUsers objects filtered by the admin column
 * @method     ChildGradioUsers[]|ObjectCollection findByPerms(string $perms) Return ChildGradioUsers objects filtered by the perms column
 * @method     ChildGradioUsers[]|ObjectCollection findByClass(string $class) Return ChildGradioUsers objects filtered by the class column
 * @method     ChildGradioUsers[]|ObjectCollection findBySess(string $sess) Return ChildGradioUsers objects filtered by the sess column
 * @method     ChildGradioUsers[]|ObjectCollection findByBan(int $ban) Return ChildGradioUsers objects filtered by the ban column
 * @method     ChildGradioUsers[]|ObjectCollection findByDrCreated(string $dr_created) Return ChildGradioUsers objects filtered by the dr_created column
 * @method     ChildGradioUsers[]|ObjectCollection findByDrDeleted(string $dr_deleted) Return ChildGradioUsers objects filtered by the dr_deleted column
 * @method     ChildGradioUsers[]|ObjectCollection findByType(string $type) Return ChildGradioUsers objects filtered by the type column
 * @method     ChildGradioUsers[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioUsersQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioUsersQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioUsers', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioUsersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioUsersQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioUsersQuery) {
            return $criteria;
        }
        $query = new ChildGradioUsersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioUsers|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioUsersTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioUsers A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, latest_provider, username, password, first_name, last_name, display_name, description, email, email_verified, phone, gender, language, birthday, age, adult, address, country, region, city, zip, website_url, profile_url, photo_url, ip, visits, currentvisit, lastvisit, updated, added, img, imgs, imgm, imgl, prefs, admin, perms, class, sess, ban, dr_created, dr_deleted, type FROM gradio_users WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioUsers $obj */
            $obj = new ChildGradioUsers();
            $obj->hydrate($row);
            GradioUsersTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioUsers|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioUsersTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioUsersTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the latest_provider column
     *
     * Example usage:
     * <code>
     * $query->filterByLatestProvider('fooValue');   // WHERE latest_provider = 'fooValue'
     * $query->filterByLatestProvider('%fooValue%', Criteria::LIKE); // WHERE latest_provider LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latestProvider The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByLatestProvider($latestProvider = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latestProvider)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_LATEST_PROVIDER, $latestProvider, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%', Criteria::LIKE); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the display_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayName('fooValue');   // WHERE display_name = 'fooValue'
     * $query->filterByDisplayName('%fooValue%', Criteria::LIKE); // WHERE display_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $displayName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByDisplayName($displayName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($displayName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_DISPLAY_NAME, $displayName, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the email_verified column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailVerified('fooValue');   // WHERE email_verified = 'fooValue'
     * $query->filterByEmailVerified('%fooValue%', Criteria::LIKE); // WHERE email_verified LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailVerified The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByEmailVerified($emailVerified = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailVerified)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_EMAIL_VERIFIED, $emailVerified, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone(1234); // WHERE phone = 1234
     * $query->filterByPhone(array(12, 34)); // WHERE phone IN (12, 34)
     * $query->filterByPhone(array('min' => 12)); // WHERE phone > 12
     * </code>
     *
     * @param     mixed $phone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (is_array($phone)) {
            $useMinMax = false;
            if (isset($phone['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_PHONE, $phone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($phone['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_PHONE, $phone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE gender = 'fooValue'
     * $query->filterByGender('%fooValue%', Criteria::LIKE); // WHERE gender LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gender The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the language column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguage('fooValue');   // WHERE language = 'fooValue'
     * $query->filterByLanguage('%fooValue%', Criteria::LIKE); // WHERE language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $language The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByLanguage($language = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($language)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_LANGUAGE, $language, $comparison);
    }

    /**
     * Filter the query on the birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE birthday = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE birthday = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE birthday > '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_BIRTHDAY, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_BIRTHDAY, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_BIRTHDAY, $birthday, $comparison);
    }

    /**
     * Filter the query on the age column
     *
     * Example usage:
     * <code>
     * $query->filterByAge(1234); // WHERE age = 1234
     * $query->filterByAge(array(12, 34)); // WHERE age IN (12, 34)
     * $query->filterByAge(array('min' => 12)); // WHERE age > 12
     * </code>
     *
     * @param     mixed $age The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByAge($age = null, $comparison = null)
    {
        if (is_array($age)) {
            $useMinMax = false;
            if (isset($age['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_AGE, $age['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($age['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_AGE, $age['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_AGE, $age, $comparison);
    }

    /**
     * Filter the query on the adult column
     *
     * Example usage:
     * <code>
     * $query->filterByAdult(1234); // WHERE adult = 1234
     * $query->filterByAdult(array(12, 34)); // WHERE adult IN (12, 34)
     * $query->filterByAdult(array('min' => 12)); // WHERE adult > 12
     * </code>
     *
     * @param     mixed $adult The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByAdult($adult = null, $comparison = null)
    {
        if (is_array($adult)) {
            $useMinMax = false;
            if (isset($adult['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ADULT, $adult['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($adult['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ADULT, $adult['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_ADULT, $adult, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%', Criteria::LIKE); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE country = 'fooValue'
     * $query->filterByCountry('%fooValue%', Criteria::LIKE); // WHERE country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE region = 'fooValue'
     * $query->filterByRegion('%fooValue%', Criteria::LIKE); // WHERE region LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_REGION, $region, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%', Criteria::LIKE); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the zip column
     *
     * Example usage:
     * <code>
     * $query->filterByZip('fooValue');   // WHERE zip = 'fooValue'
     * $query->filterByZip('%fooValue%', Criteria::LIKE); // WHERE zip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByZip($zip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_ZIP, $zip, $comparison);
    }

    /**
     * Filter the query on the website_url column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsiteUrl('fooValue');   // WHERE website_url = 'fooValue'
     * $query->filterByWebsiteUrl('%fooValue%', Criteria::LIKE); // WHERE website_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $websiteUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByWebsiteUrl($websiteUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($websiteUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_WEBSITE_URL, $websiteUrl, $comparison);
    }

    /**
     * Filter the query on the profile_url column
     *
     * Example usage:
     * <code>
     * $query->filterByProfileUrl('fooValue');   // WHERE profile_url = 'fooValue'
     * $query->filterByProfileUrl('%fooValue%', Criteria::LIKE); // WHERE profile_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profileUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByProfileUrl($profileUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profileUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_PROFILE_URL, $profileUrl, $comparison);
    }

    /**
     * Filter the query on the photo_url column
     *
     * Example usage:
     * <code>
     * $query->filterByPhotoUrl('fooValue');   // WHERE photo_url = 'fooValue'
     * $query->filterByPhotoUrl('%fooValue%', Criteria::LIKE); // WHERE photo_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $photoUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPhotoUrl($photoUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($photoUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_PHOTO_URL, $photoUrl, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the visits column
     *
     * Example usage:
     * <code>
     * $query->filterByVisits(1234); // WHERE visits = 1234
     * $query->filterByVisits(array(12, 34)); // WHERE visits IN (12, 34)
     * $query->filterByVisits(array('min' => 12)); // WHERE visits > 12
     * </code>
     *
     * @param     mixed $visits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByVisits($visits = null, $comparison = null)
    {
        if (is_array($visits)) {
            $useMinMax = false;
            if (isset($visits['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_VISITS, $visits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($visits['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_VISITS, $visits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_VISITS, $visits, $comparison);
    }

    /**
     * Filter the query on the currentvisit column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrentvisit('2011-03-14'); // WHERE currentvisit = '2011-03-14'
     * $query->filterByCurrentvisit('now'); // WHERE currentvisit = '2011-03-14'
     * $query->filterByCurrentvisit(array('max' => 'yesterday')); // WHERE currentvisit > '2011-03-13'
     * </code>
     *
     * @param     mixed $currentvisit The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByCurrentvisit($currentvisit = null, $comparison = null)
    {
        if (is_array($currentvisit)) {
            $useMinMax = false;
            if (isset($currentvisit['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_CURRENTVISIT, $currentvisit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($currentvisit['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_CURRENTVISIT, $currentvisit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_CURRENTVISIT, $currentvisit, $comparison);
    }

    /**
     * Filter the query on the lastvisit column
     *
     * Example usage:
     * <code>
     * $query->filterByLastvisit('2011-03-14'); // WHERE lastvisit = '2011-03-14'
     * $query->filterByLastvisit('now'); // WHERE lastvisit = '2011-03-14'
     * $query->filterByLastvisit(array('max' => 'yesterday')); // WHERE lastvisit > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastvisit The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByLastvisit($lastvisit = null, $comparison = null)
    {
        if (is_array($lastvisit)) {
            $useMinMax = false;
            if (isset($lastvisit['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_LASTVISIT, $lastvisit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastvisit['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_LASTVISIT, $lastvisit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_LASTVISIT, $lastvisit, $comparison);
    }

    /**
     * Filter the query on the updated column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdated('2011-03-14'); // WHERE updated = '2011-03-14'
     * $query->filterByUpdated('now'); // WHERE updated = '2011-03-14'
     * $query->filterByUpdated(array('max' => 'yesterday')); // WHERE updated > '2011-03-13'
     * </code>
     *
     * @param     mixed $updated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByUpdated($updated = null, $comparison = null)
    {
        if (is_array($updated)) {
            $useMinMax = false;
            if (isset($updated['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_UPDATED, $updated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updated['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_UPDATED, $updated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_UPDATED, $updated, $comparison);
    }

    /**
     * Filter the query on the added column
     *
     * Example usage:
     * <code>
     * $query->filterByAdded('2011-03-14'); // WHERE added = '2011-03-14'
     * $query->filterByAdded('now'); // WHERE added = '2011-03-14'
     * $query->filterByAdded(array('max' => 'yesterday')); // WHERE added > '2011-03-13'
     * </code>
     *
     * @param     mixed $added The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByAdded($added = null, $comparison = null)
    {
        if (is_array($added)) {
            $useMinMax = false;
            if (isset($added['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ADDED, $added['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($added['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ADDED, $added['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_ADDED, $added, $comparison);
    }

    /**
     * Filter the query on the img column
     *
     * Example usage:
     * <code>
     * $query->filterByImg('fooValue');   // WHERE img = 'fooValue'
     * $query->filterByImg('%fooValue%', Criteria::LIKE); // WHERE img LIKE '%fooValue%'
     * </code>
     *
     * @param     string $img The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByImg($img = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($img)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_IMG, $img, $comparison);
    }

    /**
     * Filter the query on the imgs column
     *
     * Example usage:
     * <code>
     * $query->filterByImgs('fooValue');   // WHERE imgs = 'fooValue'
     * $query->filterByImgs('%fooValue%', Criteria::LIKE); // WHERE imgs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imgs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByImgs($imgs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imgs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_IMGS, $imgs, $comparison);
    }

    /**
     * Filter the query on the imgm column
     *
     * Example usage:
     * <code>
     * $query->filterByImgm('fooValue');   // WHERE imgm = 'fooValue'
     * $query->filterByImgm('%fooValue%', Criteria::LIKE); // WHERE imgm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imgm The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByImgm($imgm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imgm)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_IMGM, $imgm, $comparison);
    }

    /**
     * Filter the query on the imgl column
     *
     * Example usage:
     * <code>
     * $query->filterByImgl('fooValue');   // WHERE imgl = 'fooValue'
     * $query->filterByImgl('%fooValue%', Criteria::LIKE); // WHERE imgl LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imgl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByImgl($imgl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imgl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_IMGL, $imgl, $comparison);
    }

    /**
     * Filter the query on the prefs column
     *
     * Example usage:
     * <code>
     * $query->filterByPrefs('fooValue');   // WHERE prefs = 'fooValue'
     * $query->filterByPrefs('%fooValue%', Criteria::LIKE); // WHERE prefs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prefs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPrefs($prefs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prefs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_PREFS, $prefs, $comparison);
    }

    /**
     * Filter the query on the admin column
     *
     * Example usage:
     * <code>
     * $query->filterByAdmin(1234); // WHERE admin = 1234
     * $query->filterByAdmin(array(12, 34)); // WHERE admin IN (12, 34)
     * $query->filterByAdmin(array('min' => 12)); // WHERE admin > 12
     * </code>
     *
     * @param     mixed $admin The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByAdmin($admin = null, $comparison = null)
    {
        if (is_array($admin)) {
            $useMinMax = false;
            if (isset($admin['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ADMIN, $admin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($admin['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_ADMIN, $admin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_ADMIN, $admin, $comparison);
    }

    /**
     * Filter the query on the perms column
     *
     * Example usage:
     * <code>
     * $query->filterByPerms('fooValue');   // WHERE perms = 'fooValue'
     * $query->filterByPerms('%fooValue%', Criteria::LIKE); // WHERE perms LIKE '%fooValue%'
     * </code>
     *
     * @param     string $perms The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByPerms($perms = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($perms)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_PERMS, $perms, $comparison);
    }

    /**
     * Filter the query on the class column
     *
     * Example usage:
     * <code>
     * $query->filterByClass('fooValue');   // WHERE class = 'fooValue'
     * $query->filterByClass('%fooValue%', Criteria::LIKE); // WHERE class LIKE '%fooValue%'
     * </code>
     *
     * @param     string $class The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByClass($class = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($class)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_CLASS, $class, $comparison);
    }

    /**
     * Filter the query on the sess column
     *
     * Example usage:
     * <code>
     * $query->filterBySess('fooValue');   // WHERE sess = 'fooValue'
     * $query->filterBySess('%fooValue%', Criteria::LIKE); // WHERE sess LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sess The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterBySess($sess = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sess)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_SESS, $sess, $comparison);
    }

    /**
     * Filter the query on the ban column
     *
     * Example usage:
     * <code>
     * $query->filterByBan(1234); // WHERE ban = 1234
     * $query->filterByBan(array(12, 34)); // WHERE ban IN (12, 34)
     * $query->filterByBan(array('min' => 12)); // WHERE ban > 12
     * </code>
     *
     * @param     mixed $ban The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByBan($ban = null, $comparison = null)
    {
        if (is_array($ban)) {
            $useMinMax = false;
            if (isset($ban['min'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_BAN, $ban['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ban['max'])) {
                $this->addUsingAlias(GradioUsersTableMap::COL_BAN, $ban['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_BAN, $ban, $comparison);
    }

    /**
     * Filter the query on the dr_created column
     *
     * Example usage:
     * <code>
     * $query->filterByDrCreated('fooValue');   // WHERE dr_created = 'fooValue'
     * $query->filterByDrCreated('%fooValue%', Criteria::LIKE); // WHERE dr_created LIKE '%fooValue%'
     * </code>
     *
     * @param     string $drCreated The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByDrCreated($drCreated = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($drCreated)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_DR_CREATED, $drCreated, $comparison);
    }

    /**
     * Filter the query on the dr_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByDrDeleted('fooValue');   // WHERE dr_deleted = 'fooValue'
     * $query->filterByDrDeleted('%fooValue%', Criteria::LIKE); // WHERE dr_deleted LIKE '%fooValue%'
     * </code>
     *
     * @param     string $drDeleted The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByDrDeleted($drDeleted = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($drDeleted)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_DR_DELETED, $drDeleted, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUsersTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioUsers $gradioUsers Object to remove from the list of results
     *
     * @return $this|ChildGradioUsersQuery The current query, for fluid interface
     */
    public function prune($gradioUsers = null)
    {
        if ($gradioUsers) {
            $this->addUsingAlias(GradioUsersTableMap::COL_ID, $gradioUsers->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioUsersTableMap::clearInstancePool();
            GradioUsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioUsersTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioUsersTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioUsersTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioUsersQuery
