<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioUserclassesQuery as ChildGradioUserclassesQuery;
use Model\Main\Map\GradioUserclassesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'gradio_userclasses' table.
 *
 *
 *
 * @package    propel.generator.Model.Main.Base
 */
abstract class GradioUserclasses implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Main\\Map\\GradioUserclassesTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the userclass_id field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $userclass_id;

    /**
     * The value for the userclass_name field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $userclass_name;

    /**
     * The value for the userclass_description field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $userclass_description;

    /**
     * The value for the userclass_editclass field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $userclass_editclass;

    /**
     * The value for the userclass_parent field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $userclass_parent;

    /**
     * The value for the userclass_accum field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $userclass_accum;

    /**
     * The value for the userclass_visibility field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $userclass_visibility;

    /**
     * The value for the userclass_type field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $userclass_type;

    /**
     * The value for the userclass_icon field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $userclass_icon;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->userclass_id = 0;
        $this->userclass_name = '';
        $this->userclass_description = '';
        $this->userclass_editclass = 0;
        $this->userclass_parent = 0;
        $this->userclass_accum = '';
        $this->userclass_visibility = 0;
        $this->userclass_type = false;
        $this->userclass_icon = '';
    }

    /**
     * Initializes internal state of Model\Main\Base\GradioUserclasses object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>GradioUserclasses</code> instance.  If
     * <code>obj</code> is an instance of <code>GradioUserclasses</code>, delegates to
     * <code>equals(GradioUserclasses)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|GradioUserclasses The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [userclass_id] column value.
     *
     * @return int
     */
    public function getUserclassId()
    {
        return $this->userclass_id;
    }

    /**
     * Get the [userclass_name] column value.
     *
     * @return string
     */
    public function getUserclassName()
    {
        return $this->userclass_name;
    }

    /**
     * Get the [userclass_description] column value.
     *
     * @return string
     */
    public function getUserclassDescription()
    {
        return $this->userclass_description;
    }

    /**
     * Get the [userclass_editclass] column value.
     *
     * @return int
     */
    public function getUserclassEditclass()
    {
        return $this->userclass_editclass;
    }

    /**
     * Get the [userclass_parent] column value.
     *
     * @return int
     */
    public function getUserclassParent()
    {
        return $this->userclass_parent;
    }

    /**
     * Get the [userclass_accum] column value.
     *
     * @return string
     */
    public function getUserclassAccum()
    {
        return $this->userclass_accum;
    }

    /**
     * Get the [userclass_visibility] column value.
     *
     * @return int
     */
    public function getUserclassVisibility()
    {
        return $this->userclass_visibility;
    }

    /**
     * Get the [userclass_type] column value.
     *
     * @return boolean
     */
    public function getUserclassType()
    {
        return $this->userclass_type;
    }

    /**
     * Get the [userclass_type] column value.
     *
     * @return boolean
     */
    public function isUserclassType()
    {
        return $this->getUserclassType();
    }

    /**
     * Get the [userclass_icon] column value.
     *
     * @return string
     */
    public function getUserclassIcon()
    {
        return $this->userclass_icon;
    }

    /**
     * Set the value of [userclass_id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->userclass_id !== $v) {
            $this->userclass_id = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_ID] = true;
        }

        return $this;
    } // setUserclassId()

    /**
     * Set the value of [userclass_name] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->userclass_name !== $v) {
            $this->userclass_name = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_NAME] = true;
        }

        return $this;
    } // setUserclassName()

    /**
     * Set the value of [userclass_description] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->userclass_description !== $v) {
            $this->userclass_description = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION] = true;
        }

        return $this;
    } // setUserclassDescription()

    /**
     * Set the value of [userclass_editclass] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassEditclass($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->userclass_editclass !== $v) {
            $this->userclass_editclass = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS] = true;
        }

        return $this;
    } // setUserclassEditclass()

    /**
     * Set the value of [userclass_parent] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassParent($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->userclass_parent !== $v) {
            $this->userclass_parent = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_PARENT] = true;
        }

        return $this;
    } // setUserclassParent()

    /**
     * Set the value of [userclass_accum] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassAccum($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->userclass_accum !== $v) {
            $this->userclass_accum = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_ACCUM] = true;
        }

        return $this;
    } // setUserclassAccum()

    /**
     * Set the value of [userclass_visibility] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassVisibility($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->userclass_visibility !== $v) {
            $this->userclass_visibility = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY] = true;
        }

        return $this;
    } // setUserclassVisibility()

    /**
     * Sets the value of the [userclass_type] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassType($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->userclass_type !== $v) {
            $this->userclass_type = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_TYPE] = true;
        }

        return $this;
    } // setUserclassType()

    /**
     * Set the value of [userclass_icon] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUserclasses The current object (for fluent API support)
     */
    public function setUserclassIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->userclass_icon !== $v) {
            $this->userclass_icon = $v;
            $this->modifiedColumns[GradioUserclassesTableMap::COL_USERCLASS_ICON] = true;
        }

        return $this;
    } // setUserclassIcon()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->userclass_id !== 0) {
                return false;
            }

            if ($this->userclass_name !== '') {
                return false;
            }

            if ($this->userclass_description !== '') {
                return false;
            }

            if ($this->userclass_editclass !== 0) {
                return false;
            }

            if ($this->userclass_parent !== 0) {
                return false;
            }

            if ($this->userclass_accum !== '') {
                return false;
            }

            if ($this->userclass_visibility !== 0) {
                return false;
            }

            if ($this->userclass_type !== false) {
                return false;
            }

            if ($this->userclass_icon !== '') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassDescription', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassEditclass', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_editclass = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassParent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_parent = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassAccum', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_accum = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassVisibility', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_visibility = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_type = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : GradioUserclassesTableMap::translateFieldName('UserclassIcon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->userclass_icon = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = GradioUserclassesTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Main\\GradioUserclasses'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildGradioUserclassesQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see GradioUserclasses::setDeleted()
     * @see GradioUserclasses::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildGradioUserclassesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserclassesTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                GradioUserclassesTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_id';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_name';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_description';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_editclass';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_PARENT)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_parent';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_ACCUM)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_accum';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_visibility';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_type';
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_ICON)) {
            $modifiedColumns[':p' . $index++]  = 'userclass_icon';
        }

        $sql = sprintf(
            'INSERT INTO gradio_userclasses (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'userclass_id':
                        $stmt->bindValue($identifier, $this->userclass_id, PDO::PARAM_INT);
                        break;
                    case 'userclass_name':
                        $stmt->bindValue($identifier, $this->userclass_name, PDO::PARAM_STR);
                        break;
                    case 'userclass_description':
                        $stmt->bindValue($identifier, $this->userclass_description, PDO::PARAM_STR);
                        break;
                    case 'userclass_editclass':
                        $stmt->bindValue($identifier, $this->userclass_editclass, PDO::PARAM_INT);
                        break;
                    case 'userclass_parent':
                        $stmt->bindValue($identifier, $this->userclass_parent, PDO::PARAM_INT);
                        break;
                    case 'userclass_accum':
                        $stmt->bindValue($identifier, $this->userclass_accum, PDO::PARAM_STR);
                        break;
                    case 'userclass_visibility':
                        $stmt->bindValue($identifier, $this->userclass_visibility, PDO::PARAM_INT);
                        break;
                    case 'userclass_type':
                        $stmt->bindValue($identifier, (int) $this->userclass_type, PDO::PARAM_INT);
                        break;
                    case 'userclass_icon':
                        $stmt->bindValue($identifier, $this->userclass_icon, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GradioUserclassesTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getUserclassId();
                break;
            case 1:
                return $this->getUserclassName();
                break;
            case 2:
                return $this->getUserclassDescription();
                break;
            case 3:
                return $this->getUserclassEditclass();
                break;
            case 4:
                return $this->getUserclassParent();
                break;
            case 5:
                return $this->getUserclassAccum();
                break;
            case 6:
                return $this->getUserclassVisibility();
                break;
            case 7:
                return $this->getUserclassType();
                break;
            case 8:
                return $this->getUserclassIcon();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['GradioUserclasses'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['GradioUserclasses'][$this->hashCode()] = true;
        $keys = GradioUserclassesTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getUserclassId(),
            $keys[1] => $this->getUserclassName(),
            $keys[2] => $this->getUserclassDescription(),
            $keys[3] => $this->getUserclassEditclass(),
            $keys[4] => $this->getUserclassParent(),
            $keys[5] => $this->getUserclassAccum(),
            $keys[6] => $this->getUserclassVisibility(),
            $keys[7] => $this->getUserclassType(),
            $keys[8] => $this->getUserclassIcon(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Main\GradioUserclasses
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GradioUserclassesTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Main\GradioUserclasses
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setUserclassId($value);
                break;
            case 1:
                $this->setUserclassName($value);
                break;
            case 2:
                $this->setUserclassDescription($value);
                break;
            case 3:
                $this->setUserclassEditclass($value);
                break;
            case 4:
                $this->setUserclassParent($value);
                break;
            case 5:
                $this->setUserclassAccum($value);
                break;
            case 6:
                $this->setUserclassVisibility($value);
                break;
            case 7:
                $this->setUserclassType($value);
                break;
            case 8:
                $this->setUserclassIcon($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = GradioUserclassesTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setUserclassId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUserclassName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setUserclassDescription($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setUserclassEditclass($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUserclassParent($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUserclassAccum($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUserclassVisibility($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUserclassType($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUserclassIcon($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Main\GradioUserclasses The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(GradioUserclassesTableMap::DATABASE_NAME);

        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_ID)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_ID, $this->userclass_id);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_NAME)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_NAME, $this->userclass_name);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_DESCRIPTION, $this->userclass_description);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_EDITCLASS, $this->userclass_editclass);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_PARENT)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_PARENT, $this->userclass_parent);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_ACCUM)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_ACCUM, $this->userclass_accum);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_VISIBILITY, $this->userclass_visibility);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_TYPE)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_TYPE, $this->userclass_type);
        }
        if ($this->isColumnModified(GradioUserclassesTableMap::COL_USERCLASS_ICON)) {
            $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_ICON, $this->userclass_icon);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildGradioUserclassesQuery::create();
        $criteria->add(GradioUserclassesTableMap::COL_USERCLASS_ID, $this->userclass_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getUserclassId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getUserclassId();
    }

    /**
     * Generic method to set the primary key (userclass_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setUserclassId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getUserclassId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Main\GradioUserclasses (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUserclassId($this->getUserclassId());
        $copyObj->setUserclassName($this->getUserclassName());
        $copyObj->setUserclassDescription($this->getUserclassDescription());
        $copyObj->setUserclassEditclass($this->getUserclassEditclass());
        $copyObj->setUserclassParent($this->getUserclassParent());
        $copyObj->setUserclassAccum($this->getUserclassAccum());
        $copyObj->setUserclassVisibility($this->getUserclassVisibility());
        $copyObj->setUserclassType($this->getUserclassType());
        $copyObj->setUserclassIcon($this->getUserclassIcon());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Main\GradioUserclasses Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->userclass_id = null;
        $this->userclass_name = null;
        $this->userclass_description = null;
        $this->userclass_editclass = null;
        $this->userclass_parent = null;
        $this->userclass_accum = null;
        $this->userclass_visibility = null;
        $this->userclass_type = null;
        $this->userclass_icon = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(GradioUserclassesTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
