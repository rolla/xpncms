<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioSessions as ChildGradioSessions;
use Model\Main\GradioSessionsQuery as ChildGradioSessionsQuery;
use Model\Main\Map\GradioSessionsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_sessions' table.
 *
 *
 *
 * @method     ChildGradioSessionsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioSessionsQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildGradioSessionsQuery orderByVuserId($order = Criteria::ASC) Order by the vuser_id column
 * @method     ChildGradioSessionsQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildGradioSessionsQuery orderByVisits($order = Criteria::ASC) Order by the visits column
 * @method     ChildGradioSessionsQuery orderByTillRefresh($order = Criteria::ASC) Order by the till_refresh column
 * @method     ChildGradioSessionsQuery orderByClicks($order = Criteria::ASC) Order by the clicks column
 * @method     ChildGradioSessionsQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildGradioSessionsQuery orderByActionsTimestamp($order = Criteria::ASC) Order by the actions_timestamp column
 * @method     ChildGradioSessionsQuery orderByCurrentvisit($order = Criteria::ASC) Order by the currentvisit column
 * @method     ChildGradioSessionsQuery orderByLastvisit($order = Criteria::ASC) Order by the lastvisit column
 * @method     ChildGradioSessionsQuery orderByAdded($order = Criteria::ASC) Order by the added column
 *
 * @method     ChildGradioSessionsQuery groupById() Group by the id column
 * @method     ChildGradioSessionsQuery groupByUserId() Group by the user_id column
 * @method     ChildGradioSessionsQuery groupByVuserId() Group by the vuser_id column
 * @method     ChildGradioSessionsQuery groupByIp() Group by the ip column
 * @method     ChildGradioSessionsQuery groupByVisits() Group by the visits column
 * @method     ChildGradioSessionsQuery groupByTillRefresh() Group by the till_refresh column
 * @method     ChildGradioSessionsQuery groupByClicks() Group by the clicks column
 * @method     ChildGradioSessionsQuery groupByStatus() Group by the status column
 * @method     ChildGradioSessionsQuery groupByActionsTimestamp() Group by the actions_timestamp column
 * @method     ChildGradioSessionsQuery groupByCurrentvisit() Group by the currentvisit column
 * @method     ChildGradioSessionsQuery groupByLastvisit() Group by the lastvisit column
 * @method     ChildGradioSessionsQuery groupByAdded() Group by the added column
 *
 * @method     ChildGradioSessionsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioSessionsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioSessionsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioSessionsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioSessionsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioSessionsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioSessions findOne(ConnectionInterface $con = null) Return the first ChildGradioSessions matching the query
 * @method     ChildGradioSessions findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioSessions matching the query, or a new ChildGradioSessions object populated from the query conditions when no match is found
 *
 * @method     ChildGradioSessions findOneById(int $id) Return the first ChildGradioSessions filtered by the id column
 * @method     ChildGradioSessions findOneByUserId(int $user_id) Return the first ChildGradioSessions filtered by the user_id column
 * @method     ChildGradioSessions findOneByVuserId(string $vuser_id) Return the first ChildGradioSessions filtered by the vuser_id column
 * @method     ChildGradioSessions findOneByIp(string $ip) Return the first ChildGradioSessions filtered by the ip column
 * @method     ChildGradioSessions findOneByVisits(int $visits) Return the first ChildGradioSessions filtered by the visits column
 * @method     ChildGradioSessions findOneByTillRefresh(int $till_refresh) Return the first ChildGradioSessions filtered by the till_refresh column
 * @method     ChildGradioSessions findOneByClicks(int $clicks) Return the first ChildGradioSessions filtered by the clicks column
 * @method     ChildGradioSessions findOneByStatus(int $status) Return the first ChildGradioSessions filtered by the status column
 * @method     ChildGradioSessions findOneByActionsTimestamp(string $actions_timestamp) Return the first ChildGradioSessions filtered by the actions_timestamp column
 * @method     ChildGradioSessions findOneByCurrentvisit(string $currentvisit) Return the first ChildGradioSessions filtered by the currentvisit column
 * @method     ChildGradioSessions findOneByLastvisit(string $lastvisit) Return the first ChildGradioSessions filtered by the lastvisit column
 * @method     ChildGradioSessions findOneByAdded(string $added) Return the first ChildGradioSessions filtered by the added column *

 * @method     ChildGradioSessions requirePk($key, ConnectionInterface $con = null) Return the ChildGradioSessions by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOne(ConnectionInterface $con = null) Return the first ChildGradioSessions matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioSessions requireOneById(int $id) Return the first ChildGradioSessions filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByUserId(int $user_id) Return the first ChildGradioSessions filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByVuserId(string $vuser_id) Return the first ChildGradioSessions filtered by the vuser_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByIp(string $ip) Return the first ChildGradioSessions filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByVisits(int $visits) Return the first ChildGradioSessions filtered by the visits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByTillRefresh(int $till_refresh) Return the first ChildGradioSessions filtered by the till_refresh column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByClicks(int $clicks) Return the first ChildGradioSessions filtered by the clicks column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByStatus(int $status) Return the first ChildGradioSessions filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByActionsTimestamp(string $actions_timestamp) Return the first ChildGradioSessions filtered by the actions_timestamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByCurrentvisit(string $currentvisit) Return the first ChildGradioSessions filtered by the currentvisit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByLastvisit(string $lastvisit) Return the first ChildGradioSessions filtered by the lastvisit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioSessions requireOneByAdded(string $added) Return the first ChildGradioSessions filtered by the added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioSessions[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioSessions objects based on current ModelCriteria
 * @method     ChildGradioSessions[]|ObjectCollection findById(int $id) Return ChildGradioSessions objects filtered by the id column
 * @method     ChildGradioSessions[]|ObjectCollection findByUserId(int $user_id) Return ChildGradioSessions objects filtered by the user_id column
 * @method     ChildGradioSessions[]|ObjectCollection findByVuserId(string $vuser_id) Return ChildGradioSessions objects filtered by the vuser_id column
 * @method     ChildGradioSessions[]|ObjectCollection findByIp(string $ip) Return ChildGradioSessions objects filtered by the ip column
 * @method     ChildGradioSessions[]|ObjectCollection findByVisits(int $visits) Return ChildGradioSessions objects filtered by the visits column
 * @method     ChildGradioSessions[]|ObjectCollection findByTillRefresh(int $till_refresh) Return ChildGradioSessions objects filtered by the till_refresh column
 * @method     ChildGradioSessions[]|ObjectCollection findByClicks(int $clicks) Return ChildGradioSessions objects filtered by the clicks column
 * @method     ChildGradioSessions[]|ObjectCollection findByStatus(int $status) Return ChildGradioSessions objects filtered by the status column
 * @method     ChildGradioSessions[]|ObjectCollection findByActionsTimestamp(string $actions_timestamp) Return ChildGradioSessions objects filtered by the actions_timestamp column
 * @method     ChildGradioSessions[]|ObjectCollection findByCurrentvisit(string $currentvisit) Return ChildGradioSessions objects filtered by the currentvisit column
 * @method     ChildGradioSessions[]|ObjectCollection findByLastvisit(string $lastvisit) Return ChildGradioSessions objects filtered by the lastvisit column
 * @method     ChildGradioSessions[]|ObjectCollection findByAdded(string $added) Return ChildGradioSessions objects filtered by the added column
 * @method     ChildGradioSessions[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioSessionsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioSessionsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioSessions', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioSessionsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioSessionsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioSessionsQuery) {
            return $criteria;
        }
        $query = new ChildGradioSessionsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioSessions|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioSessionsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioSessionsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioSessions A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, vuser_id, ip, visits, till_refresh, clicks, status, actions_timestamp, currentvisit, lastvisit, added FROM gradio_sessions WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioSessions $obj */
            $obj = new ChildGradioSessions();
            $obj->hydrate($row);
            GradioSessionsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioSessions|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioSessionsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioSessionsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the vuser_id column
     *
     * Example usage:
     * <code>
     * $query->filterByVuserId('fooValue');   // WHERE vuser_id = 'fooValue'
     * $query->filterByVuserId('%fooValue%', Criteria::LIKE); // WHERE vuser_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vuserId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByVuserId($vuserId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vuserId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_VUSER_ID, $vuserId, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the visits column
     *
     * Example usage:
     * <code>
     * $query->filterByVisits(1234); // WHERE visits = 1234
     * $query->filterByVisits(array(12, 34)); // WHERE visits IN (12, 34)
     * $query->filterByVisits(array('min' => 12)); // WHERE visits > 12
     * </code>
     *
     * @param     mixed $visits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByVisits($visits = null, $comparison = null)
    {
        if (is_array($visits)) {
            $useMinMax = false;
            if (isset($visits['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_VISITS, $visits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($visits['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_VISITS, $visits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_VISITS, $visits, $comparison);
    }

    /**
     * Filter the query on the till_refresh column
     *
     * Example usage:
     * <code>
     * $query->filterByTillRefresh(1234); // WHERE till_refresh = 1234
     * $query->filterByTillRefresh(array(12, 34)); // WHERE till_refresh IN (12, 34)
     * $query->filterByTillRefresh(array('min' => 12)); // WHERE till_refresh > 12
     * </code>
     *
     * @param     mixed $tillRefresh The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByTillRefresh($tillRefresh = null, $comparison = null)
    {
        if (is_array($tillRefresh)) {
            $useMinMax = false;
            if (isset($tillRefresh['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_TILL_REFRESH, $tillRefresh['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tillRefresh['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_TILL_REFRESH, $tillRefresh['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_TILL_REFRESH, $tillRefresh, $comparison);
    }

    /**
     * Filter the query on the clicks column
     *
     * Example usage:
     * <code>
     * $query->filterByClicks(1234); // WHERE clicks = 1234
     * $query->filterByClicks(array(12, 34)); // WHERE clicks IN (12, 34)
     * $query->filterByClicks(array('min' => 12)); // WHERE clicks > 12
     * </code>
     *
     * @param     mixed $clicks The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByClicks($clicks = null, $comparison = null)
    {
        if (is_array($clicks)) {
            $useMinMax = false;
            if (isset($clicks['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_CLICKS, $clicks['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clicks['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_CLICKS, $clicks['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_CLICKS, $clicks, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the actions_timestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByActionsTimestamp('2011-03-14'); // WHERE actions_timestamp = '2011-03-14'
     * $query->filterByActionsTimestamp('now'); // WHERE actions_timestamp = '2011-03-14'
     * $query->filterByActionsTimestamp(array('max' => 'yesterday')); // WHERE actions_timestamp > '2011-03-13'
     * </code>
     *
     * @param     mixed $actionsTimestamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByActionsTimestamp($actionsTimestamp = null, $comparison = null)
    {
        if (is_array($actionsTimestamp)) {
            $useMinMax = false;
            if (isset($actionsTimestamp['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_ACTIONS_TIMESTAMP, $actionsTimestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionsTimestamp['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_ACTIONS_TIMESTAMP, $actionsTimestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_ACTIONS_TIMESTAMP, $actionsTimestamp, $comparison);
    }

    /**
     * Filter the query on the currentvisit column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrentvisit('2011-03-14'); // WHERE currentvisit = '2011-03-14'
     * $query->filterByCurrentvisit('now'); // WHERE currentvisit = '2011-03-14'
     * $query->filterByCurrentvisit(array('max' => 'yesterday')); // WHERE currentvisit > '2011-03-13'
     * </code>
     *
     * @param     mixed $currentvisit The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByCurrentvisit($currentvisit = null, $comparison = null)
    {
        if (is_array($currentvisit)) {
            $useMinMax = false;
            if (isset($currentvisit['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_CURRENTVISIT, $currentvisit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($currentvisit['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_CURRENTVISIT, $currentvisit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_CURRENTVISIT, $currentvisit, $comparison);
    }

    /**
     * Filter the query on the lastvisit column
     *
     * Example usage:
     * <code>
     * $query->filterByLastvisit('2011-03-14'); // WHERE lastvisit = '2011-03-14'
     * $query->filterByLastvisit('now'); // WHERE lastvisit = '2011-03-14'
     * $query->filterByLastvisit(array('max' => 'yesterday')); // WHERE lastvisit > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastvisit The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByLastvisit($lastvisit = null, $comparison = null)
    {
        if (is_array($lastvisit)) {
            $useMinMax = false;
            if (isset($lastvisit['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_LASTVISIT, $lastvisit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastvisit['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_LASTVISIT, $lastvisit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_LASTVISIT, $lastvisit, $comparison);
    }

    /**
     * Filter the query on the added column
     *
     * Example usage:
     * <code>
     * $query->filterByAdded('2011-03-14'); // WHERE added = '2011-03-14'
     * $query->filterByAdded('now'); // WHERE added = '2011-03-14'
     * $query->filterByAdded(array('max' => 'yesterday')); // WHERE added > '2011-03-13'
     * </code>
     *
     * @param     mixed $added The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function filterByAdded($added = null, $comparison = null)
    {
        if (is_array($added)) {
            $useMinMax = false;
            if (isset($added['min'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_ADDED, $added['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($added['max'])) {
                $this->addUsingAlias(GradioSessionsTableMap::COL_ADDED, $added['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioSessionsTableMap::COL_ADDED, $added, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioSessions $gradioSessions Object to remove from the list of results
     *
     * @return $this|ChildGradioSessionsQuery The current query, for fluid interface
     */
    public function prune($gradioSessions = null)
    {
        if ($gradioSessions) {
            $this->addUsingAlias(GradioSessionsTableMap::COL_ID, $gradioSessions->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_sessions table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioSessionsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioSessionsTableMap::clearInstancePool();
            GradioSessionsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioSessionsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioSessionsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioSessionsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioSessionsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioSessionsQuery
