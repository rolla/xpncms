<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioRatingsOld as ChildGradioRatingsOld;
use Model\Main\GradioRatingsOldQuery as ChildGradioRatingsOldQuery;
use Model\Main\Map\GradioRatingsOldTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_ratings_old' table.
 *
 *
 *
 * @method     ChildGradioRatingsOldQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioRatingsOldQuery orderBySongid($order = Criteria::ASC) Order by the songid column
 * @method     ChildGradioRatingsOldQuery orderByRating($order = Criteria::ASC) Order by the rating column
 * @method     ChildGradioRatingsOldQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildGradioRatingsOldQuery orderByCepumi($order = Criteria::ASC) Order by the cepumi column
 * @method     ChildGradioRatingsOldQuery orderByExtrarating($order = Criteria::ASC) Order by the extrarating column
 * @method     ChildGradioRatingsOldQuery orderByNickname($order = Criteria::ASC) Order by the nickname column
 * @method     ChildGradioRatingsOldQuery orderByComment($order = Criteria::ASC) Order by the comment column
 * @method     ChildGradioRatingsOldQuery orderByVuserid($order = Criteria::ASC) Order by the vuserid column
 * @method     ChildGradioRatingsOldQuery orderByTime($order = Criteria::ASC) Order by the time column
 *
 * @method     ChildGradioRatingsOldQuery groupById() Group by the id column
 * @method     ChildGradioRatingsOldQuery groupBySongid() Group by the songid column
 * @method     ChildGradioRatingsOldQuery groupByRating() Group by the rating column
 * @method     ChildGradioRatingsOldQuery groupByIp() Group by the ip column
 * @method     ChildGradioRatingsOldQuery groupByCepumi() Group by the cepumi column
 * @method     ChildGradioRatingsOldQuery groupByExtrarating() Group by the extrarating column
 * @method     ChildGradioRatingsOldQuery groupByNickname() Group by the nickname column
 * @method     ChildGradioRatingsOldQuery groupByComment() Group by the comment column
 * @method     ChildGradioRatingsOldQuery groupByVuserid() Group by the vuserid column
 * @method     ChildGradioRatingsOldQuery groupByTime() Group by the time column
 *
 * @method     ChildGradioRatingsOldQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioRatingsOldQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioRatingsOldQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioRatingsOldQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioRatingsOldQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioRatingsOldQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioRatingsOld findOne(ConnectionInterface $con = null) Return the first ChildGradioRatingsOld matching the query
 * @method     ChildGradioRatingsOld findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioRatingsOld matching the query, or a new ChildGradioRatingsOld object populated from the query conditions when no match is found
 *
 * @method     ChildGradioRatingsOld findOneById(int $id) Return the first ChildGradioRatingsOld filtered by the id column
 * @method     ChildGradioRatingsOld findOneBySongid(int $songid) Return the first ChildGradioRatingsOld filtered by the songid column
 * @method     ChildGradioRatingsOld findOneByRating(string $rating) Return the first ChildGradioRatingsOld filtered by the rating column
 * @method     ChildGradioRatingsOld findOneByIp(string $ip) Return the first ChildGradioRatingsOld filtered by the ip column
 * @method     ChildGradioRatingsOld findOneByCepumi(string $cepumi) Return the first ChildGradioRatingsOld filtered by the cepumi column
 * @method     ChildGradioRatingsOld findOneByExtrarating(int $extrarating) Return the first ChildGradioRatingsOld filtered by the extrarating column
 * @method     ChildGradioRatingsOld findOneByNickname(string $nickname) Return the first ChildGradioRatingsOld filtered by the nickname column
 * @method     ChildGradioRatingsOld findOneByComment(string $comment) Return the first ChildGradioRatingsOld filtered by the comment column
 * @method     ChildGradioRatingsOld findOneByVuserid(int $vuserid) Return the first ChildGradioRatingsOld filtered by the vuserid column
 * @method     ChildGradioRatingsOld findOneByTime(string $time) Return the first ChildGradioRatingsOld filtered by the time column *

 * @method     ChildGradioRatingsOld requirePk($key, ConnectionInterface $con = null) Return the ChildGradioRatingsOld by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOne(ConnectionInterface $con = null) Return the first ChildGradioRatingsOld matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioRatingsOld requireOneById(int $id) Return the first ChildGradioRatingsOld filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneBySongid(int $songid) Return the first ChildGradioRatingsOld filtered by the songid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByRating(string $rating) Return the first ChildGradioRatingsOld filtered by the rating column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByIp(string $ip) Return the first ChildGradioRatingsOld filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByCepumi(string $cepumi) Return the first ChildGradioRatingsOld filtered by the cepumi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByExtrarating(int $extrarating) Return the first ChildGradioRatingsOld filtered by the extrarating column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByNickname(string $nickname) Return the first ChildGradioRatingsOld filtered by the nickname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByComment(string $comment) Return the first ChildGradioRatingsOld filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByVuserid(int $vuserid) Return the first ChildGradioRatingsOld filtered by the vuserid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatingsOld requireOneByTime(string $time) Return the first ChildGradioRatingsOld filtered by the time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioRatingsOld[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioRatingsOld objects based on current ModelCriteria
 * @method     ChildGradioRatingsOld[]|ObjectCollection findById(int $id) Return ChildGradioRatingsOld objects filtered by the id column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findBySongid(int $songid) Return ChildGradioRatingsOld objects filtered by the songid column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByRating(string $rating) Return ChildGradioRatingsOld objects filtered by the rating column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByIp(string $ip) Return ChildGradioRatingsOld objects filtered by the ip column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByCepumi(string $cepumi) Return ChildGradioRatingsOld objects filtered by the cepumi column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByExtrarating(int $extrarating) Return ChildGradioRatingsOld objects filtered by the extrarating column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByNickname(string $nickname) Return ChildGradioRatingsOld objects filtered by the nickname column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByComment(string $comment) Return ChildGradioRatingsOld objects filtered by the comment column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByVuserid(int $vuserid) Return ChildGradioRatingsOld objects filtered by the vuserid column
 * @method     ChildGradioRatingsOld[]|ObjectCollection findByTime(string $time) Return ChildGradioRatingsOld objects filtered by the time column
 * @method     ChildGradioRatingsOld[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioRatingsOldQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioRatingsOldQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioRatingsOld', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioRatingsOldQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioRatingsOldQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioRatingsOldQuery) {
            return $criteria;
        }
        $query = new ChildGradioRatingsOldQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioRatingsOld|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioRatingsOldTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioRatingsOldTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioRatingsOld A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, songid, rating, ip, cepumi, extrarating, nickname, comment, vuserid, time FROM gradio_ratings_old WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioRatingsOld $obj */
            $obj = new ChildGradioRatingsOld();
            $obj->hydrate($row);
            GradioRatingsOldTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioRatingsOld|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the songid column
     *
     * Example usage:
     * <code>
     * $query->filterBySongid(1234); // WHERE songid = 1234
     * $query->filterBySongid(array(12, 34)); // WHERE songid IN (12, 34)
     * $query->filterBySongid(array('min' => 12)); // WHERE songid > 12
     * </code>
     *
     * @param     mixed $songid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterBySongid($songid = null, $comparison = null)
    {
        if (is_array($songid)) {
            $useMinMax = false;
            if (isset($songid['min'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_SONGID, $songid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($songid['max'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_SONGID, $songid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_SONGID, $songid, $comparison);
    }

    /**
     * Filter the query on the rating column
     *
     * Example usage:
     * <code>
     * $query->filterByRating('fooValue');   // WHERE rating = 'fooValue'
     * $query->filterByRating('%fooValue%', Criteria::LIKE); // WHERE rating LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rating The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByRating($rating = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rating)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_RATING, $rating, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the cepumi column
     *
     * Example usage:
     * <code>
     * $query->filterByCepumi('fooValue');   // WHERE cepumi = 'fooValue'
     * $query->filterByCepumi('%fooValue%', Criteria::LIKE); // WHERE cepumi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cepumi The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByCepumi($cepumi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cepumi)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_CEPUMI, $cepumi, $comparison);
    }

    /**
     * Filter the query on the extrarating column
     *
     * Example usage:
     * <code>
     * $query->filterByExtrarating(1234); // WHERE extrarating = 1234
     * $query->filterByExtrarating(array(12, 34)); // WHERE extrarating IN (12, 34)
     * $query->filterByExtrarating(array('min' => 12)); // WHERE extrarating > 12
     * </code>
     *
     * @param     mixed $extrarating The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByExtrarating($extrarating = null, $comparison = null)
    {
        if (is_array($extrarating)) {
            $useMinMax = false;
            if (isset($extrarating['min'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_EXTRARATING, $extrarating['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($extrarating['max'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_EXTRARATING, $extrarating['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_EXTRARATING, $extrarating, $comparison);
    }

    /**
     * Filter the query on the nickname column
     *
     * Example usage:
     * <code>
     * $query->filterByNickname('fooValue');   // WHERE nickname = 'fooValue'
     * $query->filterByNickname('%fooValue%', Criteria::LIKE); // WHERE nickname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nickname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByNickname($nickname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nickname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_NICKNAME, $nickname, $comparison);
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%', Criteria::LIKE); // WHERE comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comment The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByComment($comment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_COMMENT, $comment, $comparison);
    }

    /**
     * Filter the query on the vuserid column
     *
     * Example usage:
     * <code>
     * $query->filterByVuserid(1234); // WHERE vuserid = 1234
     * $query->filterByVuserid(array(12, 34)); // WHERE vuserid IN (12, 34)
     * $query->filterByVuserid(array('min' => 12)); // WHERE vuserid > 12
     * </code>
     *
     * @param     mixed $vuserid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByVuserid($vuserid = null, $comparison = null)
    {
        if (is_array($vuserid)) {
            $useMinMax = false;
            if (isset($vuserid['min'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_VUSERID, $vuserid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vuserid['max'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_VUSERID, $vuserid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_VUSERID, $vuserid, $comparison);
    }

    /**
     * Filter the query on the time column
     *
     * Example usage:
     * <code>
     * $query->filterByTime('2011-03-14'); // WHERE time = '2011-03-14'
     * $query->filterByTime('now'); // WHERE time = '2011-03-14'
     * $query->filterByTime(array('max' => 'yesterday')); // WHERE time > '2011-03-13'
     * </code>
     *
     * @param     mixed $time The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function filterByTime($time = null, $comparison = null)
    {
        if (is_array($time)) {
            $useMinMax = false;
            if (isset($time['min'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_TIME, $time['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($time['max'])) {
                $this->addUsingAlias(GradioRatingsOldTableMap::COL_TIME, $time['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsOldTableMap::COL_TIME, $time, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioRatingsOld $gradioRatingsOld Object to remove from the list of results
     *
     * @return $this|ChildGradioRatingsOldQuery The current query, for fluid interface
     */
    public function prune($gradioRatingsOld = null)
    {
        if ($gradioRatingsOld) {
            $this->addUsingAlias(GradioRatingsOldTableMap::COL_ID, $gradioRatingsOld->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_ratings_old table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioRatingsOldTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioRatingsOldTableMap::clearInstancePool();
            GradioRatingsOldTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioRatingsOldTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioRatingsOldTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioRatingsOldTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioRatingsOldTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioRatingsOldQuery
