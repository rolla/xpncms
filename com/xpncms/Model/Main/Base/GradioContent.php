<?php

namespace Model\Main\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Main\GradioContentQuery as ChildGradioContentQuery;
use Model\Main\Map\GradioContentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'gradio_content' table.
 *
 *
 *
 * @package    propel.generator.Model.Main.Base
 */
abstract class GradioContent implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Main\\Map\\GradioContentTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the cont_id field.
     *
     * @var        int
     */
    protected $cont_id;

    /**
     * The value for the cont_name field.
     *
     * @var        string
     */
    protected $cont_name;

    /**
     * The value for the cont_content field.
     *
     * @var        string
     */
    protected $cont_content;

    /**
     * The value for the cont_tags field.
     *
     * @var        string
     */
    protected $cont_tags;

    /**
     * The value for the cont_alias field.
     *
     * @var        string
     */
    protected $cont_alias;

    /**
     * The value for the cont_added field.
     *
     * @var        DateTime
     */
    protected $cont_added;

    /**
     * The value for the cont_updated field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime
     */
    protected $cont_updated;

    /**
     * The value for the cont_class field.
     *
     * @var        int
     */
    protected $cont_class;

    /**
     * The value for the cont_category field.
     *
     * @var        int
     */
    protected $cont_category;

    /**
     * The value for the cont_imgs field.
     *
     * @var        string
     */
    protected $cont_imgs;

    /**
     * The value for the cont_order field.
     *
     * @var        int
     */
    protected $cont_order;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
    }

    /**
     * Initializes internal state of Model\Main\Base\GradioContent object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>GradioContent</code> instance.  If
     * <code>obj</code> is an instance of <code>GradioContent</code>, delegates to
     * <code>equals(GradioContent)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|GradioContent The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [cont_id] column value.
     *
     * @return int
     */
    public function getContId()
    {
        return $this->cont_id;
    }

    /**
     * Get the [cont_name] column value.
     *
     * @return string
     */
    public function getContName()
    {
        return $this->cont_name;
    }

    /**
     * Get the [cont_content] column value.
     *
     * @return string
     */
    public function getContContent()
    {
        return $this->cont_content;
    }

    /**
     * Get the [cont_tags] column value.
     *
     * @return string
     */
    public function getContTags()
    {
        return $this->cont_tags;
    }

    /**
     * Get the [cont_alias] column value.
     *
     * @return string
     */
    public function getContAlias()
    {
        return $this->cont_alias;
    }

    /**
     * Get the [optionally formatted] temporal [cont_added] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getContAdded($format = NULL)
    {
        if ($format === null) {
            return $this->cont_added;
        } else {
            return $this->cont_added instanceof \DateTimeInterface ? $this->cont_added->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [cont_updated] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getContUpdated($format = NULL)
    {
        if ($format === null) {
            return $this->cont_updated;
        } else {
            return $this->cont_updated instanceof \DateTimeInterface ? $this->cont_updated->format($format) : null;
        }
    }

    /**
     * Get the [cont_class] column value.
     *
     * @return int
     */
    public function getContClass()
    {
        return $this->cont_class;
    }

    /**
     * Get the [cont_category] column value.
     *
     * @return int
     */
    public function getContCategory()
    {
        return $this->cont_category;
    }

    /**
     * Get the [cont_imgs] column value.
     *
     * @return string
     */
    public function getContImgs()
    {
        return $this->cont_imgs;
    }

    /**
     * Get the [cont_order] column value.
     *
     * @return int
     */
    public function getContOrder()
    {
        return $this->cont_order;
    }

    /**
     * Set the value of [cont_id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cont_id !== $v) {
            $this->cont_id = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_ID] = true;
        }

        return $this;
    } // setContId()

    /**
     * Set the value of [cont_name] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cont_name !== $v) {
            $this->cont_name = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_NAME] = true;
        }

        return $this;
    } // setContName()

    /**
     * Set the value of [cont_content] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContContent($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cont_content !== $v) {
            $this->cont_content = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_CONTENT] = true;
        }

        return $this;
    } // setContContent()

    /**
     * Set the value of [cont_tags] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContTags($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cont_tags !== $v) {
            $this->cont_tags = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_TAGS] = true;
        }

        return $this;
    } // setContTags()

    /**
     * Set the value of [cont_alias] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContAlias($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cont_alias !== $v) {
            $this->cont_alias = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_ALIAS] = true;
        }

        return $this;
    } // setContAlias()

    /**
     * Sets the value of [cont_added] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContAdded($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cont_added !== null || $dt !== null) {
            if ($this->cont_added === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cont_added->format("Y-m-d H:i:s.u")) {
                $this->cont_added = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioContentTableMap::COL_CONT_ADDED] = true;
            }
        } // if either are not null

        return $this;
    } // setContAdded()

    /**
     * Sets the value of [cont_updated] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContUpdated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cont_updated !== null || $dt !== null) {
            if ($this->cont_updated === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cont_updated->format("Y-m-d H:i:s.u")) {
                $this->cont_updated = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioContentTableMap::COL_CONT_UPDATED] = true;
            }
        } // if either are not null

        return $this;
    } // setContUpdated()

    /**
     * Set the value of [cont_class] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContClass($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cont_class !== $v) {
            $this->cont_class = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_CLASS] = true;
        }

        return $this;
    } // setContClass()

    /**
     * Set the value of [cont_category] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContCategory($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cont_category !== $v) {
            $this->cont_category = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_CATEGORY] = true;
        }

        return $this;
    } // setContCategory()

    /**
     * Set the value of [cont_imgs] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContImgs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cont_imgs !== $v) {
            $this->cont_imgs = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_IMGS] = true;
        }

        return $this;
    } // setContImgs()

    /**
     * Set the value of [cont_order] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioContent The current object (for fluent API support)
     */
    public function setContOrder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cont_order !== $v) {
            $this->cont_order = $v;
            $this->modifiedColumns[GradioContentTableMap::COL_CONT_ORDER] = true;
        }

        return $this;
    } // setContOrder()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : GradioContentTableMap::translateFieldName('ContId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : GradioContentTableMap::translateFieldName('ContName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : GradioContentTableMap::translateFieldName('ContContent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_content = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : GradioContentTableMap::translateFieldName('ContTags', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_tags = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : GradioContentTableMap::translateFieldName('ContAlias', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_alias = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : GradioContentTableMap::translateFieldName('ContAdded', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cont_added = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : GradioContentTableMap::translateFieldName('ContUpdated', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cont_updated = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : GradioContentTableMap::translateFieldName('ContClass', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_class = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : GradioContentTableMap::translateFieldName('ContCategory', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_category = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : GradioContentTableMap::translateFieldName('ContImgs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_imgs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : GradioContentTableMap::translateFieldName('ContOrder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cont_order = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = GradioContentTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Main\\GradioContent'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioContentTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildGradioContentQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see GradioContent::setDeleted()
     * @see GradioContent::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioContentTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildGradioContentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioContentTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                GradioContentTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[GradioContentTableMap::COL_CONT_ID] = true;
        if (null !== $this->cont_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . GradioContentTableMap::COL_CONT_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cont_id';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'cont_name';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_CONTENT)) {
            $modifiedColumns[':p' . $index++]  = 'cont_content';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_TAGS)) {
            $modifiedColumns[':p' . $index++]  = 'cont_tags';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ALIAS)) {
            $modifiedColumns[':p' . $index++]  = 'cont_alias';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ADDED)) {
            $modifiedColumns[':p' . $index++]  = 'cont_added';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_UPDATED)) {
            $modifiedColumns[':p' . $index++]  = 'cont_updated';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_CLASS)) {
            $modifiedColumns[':p' . $index++]  = 'cont_class';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_CATEGORY)) {
            $modifiedColumns[':p' . $index++]  = 'cont_category';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_IMGS)) {
            $modifiedColumns[':p' . $index++]  = 'cont_imgs';
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ORDER)) {
            $modifiedColumns[':p' . $index++]  = 'cont_order';
        }

        $sql = sprintf(
            'INSERT INTO gradio_content (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'cont_id':
                        $stmt->bindValue($identifier, $this->cont_id, PDO::PARAM_INT);
                        break;
                    case 'cont_name':
                        $stmt->bindValue($identifier, $this->cont_name, PDO::PARAM_STR);
                        break;
                    case 'cont_content':
                        $stmt->bindValue($identifier, $this->cont_content, PDO::PARAM_STR);
                        break;
                    case 'cont_tags':
                        $stmt->bindValue($identifier, $this->cont_tags, PDO::PARAM_STR);
                        break;
                    case 'cont_alias':
                        $stmt->bindValue($identifier, $this->cont_alias, PDO::PARAM_STR);
                        break;
                    case 'cont_added':
                        $stmt->bindValue($identifier, $this->cont_added ? $this->cont_added->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cont_updated':
                        $stmt->bindValue($identifier, $this->cont_updated ? $this->cont_updated->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cont_class':
                        $stmt->bindValue($identifier, $this->cont_class, PDO::PARAM_INT);
                        break;
                    case 'cont_category':
                        $stmt->bindValue($identifier, $this->cont_category, PDO::PARAM_INT);
                        break;
                    case 'cont_imgs':
                        $stmt->bindValue($identifier, $this->cont_imgs, PDO::PARAM_STR);
                        break;
                    case 'cont_order':
                        $stmt->bindValue($identifier, $this->cont_order, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setContId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GradioContentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getContId();
                break;
            case 1:
                return $this->getContName();
                break;
            case 2:
                return $this->getContContent();
                break;
            case 3:
                return $this->getContTags();
                break;
            case 4:
                return $this->getContAlias();
                break;
            case 5:
                return $this->getContAdded();
                break;
            case 6:
                return $this->getContUpdated();
                break;
            case 7:
                return $this->getContClass();
                break;
            case 8:
                return $this->getContCategory();
                break;
            case 9:
                return $this->getContImgs();
                break;
            case 10:
                return $this->getContOrder();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['GradioContent'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['GradioContent'][$this->hashCode()] = true;
        $keys = GradioContentTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getContId(),
            $keys[1] => $this->getContName(),
            $keys[2] => $this->getContContent(),
            $keys[3] => $this->getContTags(),
            $keys[4] => $this->getContAlias(),
            $keys[5] => $this->getContAdded(),
            $keys[6] => $this->getContUpdated(),
            $keys[7] => $this->getContClass(),
            $keys[8] => $this->getContCategory(),
            $keys[9] => $this->getContImgs(),
            $keys[10] => $this->getContOrder(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Main\GradioContent
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GradioContentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Main\GradioContent
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setContId($value);
                break;
            case 1:
                $this->setContName($value);
                break;
            case 2:
                $this->setContContent($value);
                break;
            case 3:
                $this->setContTags($value);
                break;
            case 4:
                $this->setContAlias($value);
                break;
            case 5:
                $this->setContAdded($value);
                break;
            case 6:
                $this->setContUpdated($value);
                break;
            case 7:
                $this->setContClass($value);
                break;
            case 8:
                $this->setContCategory($value);
                break;
            case 9:
                $this->setContImgs($value);
                break;
            case 10:
                $this->setContOrder($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = GradioContentTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setContId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setContName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setContContent($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setContTags($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setContAlias($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setContAdded($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setContUpdated($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setContClass($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setContCategory($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setContImgs($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setContOrder($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Main\GradioContent The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(GradioContentTableMap::DATABASE_NAME);

        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ID)) {
            $criteria->add(GradioContentTableMap::COL_CONT_ID, $this->cont_id);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_NAME)) {
            $criteria->add(GradioContentTableMap::COL_CONT_NAME, $this->cont_name);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_CONTENT)) {
            $criteria->add(GradioContentTableMap::COL_CONT_CONTENT, $this->cont_content);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_TAGS)) {
            $criteria->add(GradioContentTableMap::COL_CONT_TAGS, $this->cont_tags);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ALIAS)) {
            $criteria->add(GradioContentTableMap::COL_CONT_ALIAS, $this->cont_alias);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ADDED)) {
            $criteria->add(GradioContentTableMap::COL_CONT_ADDED, $this->cont_added);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_UPDATED)) {
            $criteria->add(GradioContentTableMap::COL_CONT_UPDATED, $this->cont_updated);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_CLASS)) {
            $criteria->add(GradioContentTableMap::COL_CONT_CLASS, $this->cont_class);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_CATEGORY)) {
            $criteria->add(GradioContentTableMap::COL_CONT_CATEGORY, $this->cont_category);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_IMGS)) {
            $criteria->add(GradioContentTableMap::COL_CONT_IMGS, $this->cont_imgs);
        }
        if ($this->isColumnModified(GradioContentTableMap::COL_CONT_ORDER)) {
            $criteria->add(GradioContentTableMap::COL_CONT_ORDER, $this->cont_order);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildGradioContentQuery::create();
        $criteria->add(GradioContentTableMap::COL_CONT_ID, $this->cont_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getContId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getContId();
    }

    /**
     * Generic method to set the primary key (cont_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setContId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getContId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Main\GradioContent (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setContName($this->getContName());
        $copyObj->setContContent($this->getContContent());
        $copyObj->setContTags($this->getContTags());
        $copyObj->setContAlias($this->getContAlias());
        $copyObj->setContAdded($this->getContAdded());
        $copyObj->setContUpdated($this->getContUpdated());
        $copyObj->setContClass($this->getContClass());
        $copyObj->setContCategory($this->getContCategory());
        $copyObj->setContImgs($this->getContImgs());
        $copyObj->setContOrder($this->getContOrder());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setContId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Main\GradioContent Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->cont_id = null;
        $this->cont_name = null;
        $this->cont_content = null;
        $this->cont_tags = null;
        $this->cont_alias = null;
        $this->cont_added = null;
        $this->cont_updated = null;
        $this->cont_class = null;
        $this->cont_category = null;
        $this->cont_imgs = null;
        $this->cont_order = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(GradioContentTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
