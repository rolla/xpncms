<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioAuthsArchive as ChildGradioAuthsArchive;
use Model\Main\GradioAuthsArchiveQuery as ChildGradioAuthsArchiveQuery;
use Model\Main\Map\GradioAuthsArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_auths_archive' table.
 *
 *
 *
 * @method     ChildGradioAuthsArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioAuthsArchiveQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildGradioAuthsArchiveQuery orderByProvider($order = Criteria::ASC) Order by the provider column
 * @method     ChildGradioAuthsArchiveQuery orderByProviderUid($order = Criteria::ASC) Order by the provider_uid column
 * @method     ChildGradioAuthsArchiveQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildGradioAuthsArchiveQuery orderByDisplayName($order = Criteria::ASC) Order by the display_name column
 * @method     ChildGradioAuthsArchiveQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildGradioAuthsArchiveQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildGradioAuthsArchiveQuery orderByProfileUrl($order = Criteria::ASC) Order by the profile_url column
 * @method     ChildGradioAuthsArchiveQuery orderByWebsiteUrl($order = Criteria::ASC) Order by the website_url column
 * @method     ChildGradioAuthsArchiveQuery orderByPicture($order = Criteria::ASC) Order by the picture column
 * @method     ChildGradioAuthsArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildGradioAuthsArchiveQuery orderByFullUserData($order = Criteria::ASC) Order by the full_user_data column
 * @method     ChildGradioAuthsArchiveQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildGradioAuthsArchiveQuery groupById() Group by the id column
 * @method     ChildGradioAuthsArchiveQuery groupByUserId() Group by the user_id column
 * @method     ChildGradioAuthsArchiveQuery groupByProvider() Group by the provider column
 * @method     ChildGradioAuthsArchiveQuery groupByProviderUid() Group by the provider_uid column
 * @method     ChildGradioAuthsArchiveQuery groupByEmail() Group by the email column
 * @method     ChildGradioAuthsArchiveQuery groupByDisplayName() Group by the display_name column
 * @method     ChildGradioAuthsArchiveQuery groupByFirstName() Group by the first_name column
 * @method     ChildGradioAuthsArchiveQuery groupByLastName() Group by the last_name column
 * @method     ChildGradioAuthsArchiveQuery groupByProfileUrl() Group by the profile_url column
 * @method     ChildGradioAuthsArchiveQuery groupByWebsiteUrl() Group by the website_url column
 * @method     ChildGradioAuthsArchiveQuery groupByPicture() Group by the picture column
 * @method     ChildGradioAuthsArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildGradioAuthsArchiveQuery groupByFullUserData() Group by the full_user_data column
 * @method     ChildGradioAuthsArchiveQuery groupByStatus() Group by the status column
 *
 * @method     ChildGradioAuthsArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioAuthsArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioAuthsArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioAuthsArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioAuthsArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioAuthsArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioAuthsArchive findOne(ConnectionInterface $con = null) Return the first ChildGradioAuthsArchive matching the query
 * @method     ChildGradioAuthsArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioAuthsArchive matching the query, or a new ChildGradioAuthsArchive object populated from the query conditions when no match is found
 *
 * @method     ChildGradioAuthsArchive findOneById(int $id) Return the first ChildGradioAuthsArchive filtered by the id column
 * @method     ChildGradioAuthsArchive findOneByUserId(int $user_id) Return the first ChildGradioAuthsArchive filtered by the user_id column
 * @method     ChildGradioAuthsArchive findOneByProvider(string $provider) Return the first ChildGradioAuthsArchive filtered by the provider column
 * @method     ChildGradioAuthsArchive findOneByProviderUid(string $provider_uid) Return the first ChildGradioAuthsArchive filtered by the provider_uid column
 * @method     ChildGradioAuthsArchive findOneByEmail(string $email) Return the first ChildGradioAuthsArchive filtered by the email column
 * @method     ChildGradioAuthsArchive findOneByDisplayName(string $display_name) Return the first ChildGradioAuthsArchive filtered by the display_name column
 * @method     ChildGradioAuthsArchive findOneByFirstName(string $first_name) Return the first ChildGradioAuthsArchive filtered by the first_name column
 * @method     ChildGradioAuthsArchive findOneByLastName(string $last_name) Return the first ChildGradioAuthsArchive filtered by the last_name column
 * @method     ChildGradioAuthsArchive findOneByProfileUrl(string $profile_url) Return the first ChildGradioAuthsArchive filtered by the profile_url column
 * @method     ChildGradioAuthsArchive findOneByWebsiteUrl(string $website_url) Return the first ChildGradioAuthsArchive filtered by the website_url column
 * @method     ChildGradioAuthsArchive findOneByPicture(string $picture) Return the first ChildGradioAuthsArchive filtered by the picture column
 * @method     ChildGradioAuthsArchive findOneByCreatedAt(string $created_at) Return the first ChildGradioAuthsArchive filtered by the created_at column
 * @method     ChildGradioAuthsArchive findOneByFullUserData(string $full_user_data) Return the first ChildGradioAuthsArchive filtered by the full_user_data column
 * @method     ChildGradioAuthsArchive findOneByStatus(boolean $status) Return the first ChildGradioAuthsArchive filtered by the status column *

 * @method     ChildGradioAuthsArchive requirePk($key, ConnectionInterface $con = null) Return the ChildGradioAuthsArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOne(ConnectionInterface $con = null) Return the first ChildGradioAuthsArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioAuthsArchive requireOneById(int $id) Return the first ChildGradioAuthsArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByUserId(int $user_id) Return the first ChildGradioAuthsArchive filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByProvider(string $provider) Return the first ChildGradioAuthsArchive filtered by the provider column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByProviderUid(string $provider_uid) Return the first ChildGradioAuthsArchive filtered by the provider_uid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByEmail(string $email) Return the first ChildGradioAuthsArchive filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByDisplayName(string $display_name) Return the first ChildGradioAuthsArchive filtered by the display_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByFirstName(string $first_name) Return the first ChildGradioAuthsArchive filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByLastName(string $last_name) Return the first ChildGradioAuthsArchive filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByProfileUrl(string $profile_url) Return the first ChildGradioAuthsArchive filtered by the profile_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByWebsiteUrl(string $website_url) Return the first ChildGradioAuthsArchive filtered by the website_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByPicture(string $picture) Return the first ChildGradioAuthsArchive filtered by the picture column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByCreatedAt(string $created_at) Return the first ChildGradioAuthsArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByFullUserData(string $full_user_data) Return the first ChildGradioAuthsArchive filtered by the full_user_data column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioAuthsArchive requireOneByStatus(boolean $status) Return the first ChildGradioAuthsArchive filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioAuthsArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioAuthsArchive objects based on current ModelCriteria
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findById(int $id) Return ChildGradioAuthsArchive objects filtered by the id column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByUserId(int $user_id) Return ChildGradioAuthsArchive objects filtered by the user_id column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByProvider(string $provider) Return ChildGradioAuthsArchive objects filtered by the provider column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByProviderUid(string $provider_uid) Return ChildGradioAuthsArchive objects filtered by the provider_uid column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByEmail(string $email) Return ChildGradioAuthsArchive objects filtered by the email column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByDisplayName(string $display_name) Return ChildGradioAuthsArchive objects filtered by the display_name column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByFirstName(string $first_name) Return ChildGradioAuthsArchive objects filtered by the first_name column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByLastName(string $last_name) Return ChildGradioAuthsArchive objects filtered by the last_name column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByProfileUrl(string $profile_url) Return ChildGradioAuthsArchive objects filtered by the profile_url column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByWebsiteUrl(string $website_url) Return ChildGradioAuthsArchive objects filtered by the website_url column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByPicture(string $picture) Return ChildGradioAuthsArchive objects filtered by the picture column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildGradioAuthsArchive objects filtered by the created_at column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByFullUserData(string $full_user_data) Return ChildGradioAuthsArchive objects filtered by the full_user_data column
 * @method     ChildGradioAuthsArchive[]|ObjectCollection findByStatus(boolean $status) Return ChildGradioAuthsArchive objects filtered by the status column
 * @method     ChildGradioAuthsArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioAuthsArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioAuthsArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioAuthsArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioAuthsArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioAuthsArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioAuthsArchiveQuery) {
            return $criteria;
        }
        $query = new ChildGradioAuthsArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioAuthsArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioAuthsArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioAuthsArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioAuthsArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, provider, provider_uid, email, display_name, first_name, last_name, profile_url, website_url, picture, created_at, full_user_data, status FROM gradio_auths_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioAuthsArchive $obj */
            $obj = new ChildGradioAuthsArchive();
            $obj->hydrate($row);
            GradioAuthsArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioAuthsArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the provider column
     *
     * Example usage:
     * <code>
     * $query->filterByProvider('fooValue');   // WHERE provider = 'fooValue'
     * $query->filterByProvider('%fooValue%', Criteria::LIKE); // WHERE provider LIKE '%fooValue%'
     * </code>
     *
     * @param     string $provider The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByProvider($provider = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($provider)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_PROVIDER, $provider, $comparison);
    }

    /**
     * Filter the query on the provider_uid column
     *
     * Example usage:
     * <code>
     * $query->filterByProviderUid('fooValue');   // WHERE provider_uid = 'fooValue'
     * $query->filterByProviderUid('%fooValue%', Criteria::LIKE); // WHERE provider_uid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $providerUid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByProviderUid($providerUid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($providerUid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_PROVIDER_UID, $providerUid, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the display_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayName('fooValue');   // WHERE display_name = 'fooValue'
     * $query->filterByDisplayName('%fooValue%', Criteria::LIKE); // WHERE display_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $displayName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByDisplayName($displayName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($displayName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_DISPLAY_NAME, $displayName, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%', Criteria::LIKE); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the profile_url column
     *
     * Example usage:
     * <code>
     * $query->filterByProfileUrl('fooValue');   // WHERE profile_url = 'fooValue'
     * $query->filterByProfileUrl('%fooValue%', Criteria::LIKE); // WHERE profile_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profileUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByProfileUrl($profileUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profileUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_PROFILE_URL, $profileUrl, $comparison);
    }

    /**
     * Filter the query on the website_url column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsiteUrl('fooValue');   // WHERE website_url = 'fooValue'
     * $query->filterByWebsiteUrl('%fooValue%', Criteria::LIKE); // WHERE website_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $websiteUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByWebsiteUrl($websiteUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($websiteUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_WEBSITE_URL, $websiteUrl, $comparison);
    }

    /**
     * Filter the query on the picture column
     *
     * Example usage:
     * <code>
     * $query->filterByPicture('fooValue');   // WHERE picture = 'fooValue'
     * $query->filterByPicture('%fooValue%', Criteria::LIKE); // WHERE picture LIKE '%fooValue%'
     * </code>
     *
     * @param     string $picture The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByPicture($picture = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($picture)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_PICTURE, $picture, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the full_user_data column
     *
     * Example usage:
     * <code>
     * $query->filterByFullUserData('fooValue');   // WHERE full_user_data = 'fooValue'
     * $query->filterByFullUserData('%fooValue%', Criteria::LIKE); // WHERE full_user_data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fullUserData The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByFullUserData($fullUserData = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fullUserData)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_FULL_USER_DATA, $fullUserData, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioAuthsArchive $gradioAuthsArchive Object to remove from the list of results
     *
     * @return $this|ChildGradioAuthsArchiveQuery The current query, for fluid interface
     */
    public function prune($gradioAuthsArchive = null)
    {
        if ($gradioAuthsArchive) {
            $this->addUsingAlias(GradioAuthsArchiveTableMap::COL_ID, $gradioAuthsArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_auths_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioAuthsArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioAuthsArchiveTableMap::clearInstancePool();
            GradioAuthsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioAuthsArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioAuthsArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioAuthsArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioAuthsArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioAuthsArchiveQuery
