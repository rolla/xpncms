<?php

namespace Model\Main\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Main\GradioUsersQuery as ChildGradioUsersQuery;
use Model\Main\Map\GradioUsersTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'gradio_users' table.
 *
 *
 *
 * @package    propel.generator.Model.Main.Base
 */
abstract class GradioUsers implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Main\\Map\\GradioUsersTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the latest_provider field.
     *
     * @var        string
     */
    protected $latest_provider;

    /**
     * The value for the username field.
     *
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the first_name field.
     *
     * @var        string
     */
    protected $first_name;

    /**
     * The value for the last_name field.
     *
     * @var        string
     */
    protected $last_name;

    /**
     * The value for the display_name field.
     *
     * @var        string
     */
    protected $display_name;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the email_verified field.
     *
     * @var        string
     */
    protected $email_verified;

    /**
     * The value for the phone field.
     *
     * @var        int
     */
    protected $phone;

    /**
     * The value for the gender field.
     *
     * @var        string
     */
    protected $gender;

    /**
     * The value for the language field.
     *
     * @var        string
     */
    protected $language;

    /**
     * The value for the birthday field.
     *
     * @var        DateTime
     */
    protected $birthday;

    /**
     * The value for the age field.
     *
     * @var        int
     */
    protected $age;

    /**
     * The value for the adult field.
     *
     * @var        int
     */
    protected $adult;

    /**
     * The value for the address field.
     *
     * @var        string
     */
    protected $address;

    /**
     * The value for the country field.
     *
     * @var        string
     */
    protected $country;

    /**
     * The value for the region field.
     *
     * @var        string
     */
    protected $region;

    /**
     * The value for the city field.
     *
     * @var        string
     */
    protected $city;

    /**
     * The value for the zip field.
     *
     * @var        string
     */
    protected $zip;

    /**
     * The value for the website_url field.
     *
     * @var        string
     */
    protected $website_url;

    /**
     * The value for the profile_url field.
     *
     * @var        string
     */
    protected $profile_url;

    /**
     * The value for the photo_url field.
     *
     * @var        string
     */
    protected $photo_url;

    /**
     * The value for the ip field.
     *
     * @var        string
     */
    protected $ip;

    /**
     * The value for the visits field.
     *
     * @var        int
     */
    protected $visits;

    /**
     * The value for the currentvisit field.
     *
     * @var        DateTime
     */
    protected $currentvisit;

    /**
     * The value for the lastvisit field.
     *
     * @var        DateTime
     */
    protected $lastvisit;

    /**
     * The value for the updated field.
     *
     * @var        DateTime
     */
    protected $updated;

    /**
     * The value for the added field.
     *
     * @var        DateTime
     */
    protected $added;

    /**
     * The value for the img field.
     *
     * @var        string
     */
    protected $img;

    /**
     * The value for the imgs field.
     *
     * @var        string
     */
    protected $imgs;

    /**
     * The value for the imgm field.
     *
     * @var        string
     */
    protected $imgm;

    /**
     * The value for the imgl field.
     *
     * @var        string
     */
    protected $imgl;

    /**
     * The value for the prefs field.
     *
     * @var        string
     */
    protected $prefs;

    /**
     * The value for the admin field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $admin;

    /**
     * The value for the perms field.
     *
     * @var        string
     */
    protected $perms;

    /**
     * The value for the class field.
     *
     * @var        string
     */
    protected $class;

    /**
     * The value for the sess field.
     *
     * @var        string
     */
    protected $sess;

    /**
     * The value for the ban field.
     *
     * @var        int
     */
    protected $ban;

    /**
     * The value for the dr_created field.
     *
     * @var        string
     */
    protected $dr_created;

    /**
     * The value for the dr_deleted field.
     *
     * @var        string
     */
    protected $dr_deleted;

    /**
     * The value for the type field.
     *
     * @var        string
     */
    protected $type;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->admin = 0;
    }

    /**
     * Initializes internal state of Model\Main\Base\GradioUsers object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>GradioUsers</code> instance.  If
     * <code>obj</code> is an instance of <code>GradioUsers</code>, delegates to
     * <code>equals(GradioUsers)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|GradioUsers The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [latest_provider] column value.
     *
     * @return string
     */
    public function getLatestProvider()
    {
        return $this->latest_provider;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [first_name] column value.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Get the [last_name] column value.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Get the [display_name] column value.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [email_verified] column value.
     *
     * @return string
     */
    public function getEmailVerified()
    {
        return $this->email_verified;
    }

    /**
     * Get the [phone] column value.
     *
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [gender] column value.
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get the [language] column value.
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Get the [optionally formatted] temporal [birthday] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthday($format = NULL)
    {
        if ($format === null) {
            return $this->birthday;
        } else {
            return $this->birthday instanceof \DateTimeInterface ? $this->birthday->format($format) : null;
        }
    }

    /**
     * Get the [age] column value.
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Get the [adult] column value.
     *
     * @return int
     */
    public function getAdult()
    {
        return $this->adult;
    }

    /**
     * Get the [address] column value.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the [country] column value.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get the [region] column value.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get the [city] column value.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get the [zip] column value.
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Get the [website_url] column value.
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->website_url;
    }

    /**
     * Get the [profile_url] column value.
     *
     * @return string
     */
    public function getProfileUrl()
    {
        return $this->profile_url;
    }

    /**
     * Get the [photo_url] column value.
     *
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photo_url;
    }

    /**
     * Get the [ip] column value.
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Get the [visits] column value.
     *
     * @return int
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * Get the [optionally formatted] temporal [currentvisit] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCurrentvisit($format = NULL)
    {
        if ($format === null) {
            return $this->currentvisit;
        } else {
            return $this->currentvisit instanceof \DateTimeInterface ? $this->currentvisit->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [lastvisit] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastvisit($format = NULL)
    {
        if ($format === null) {
            return $this->lastvisit;
        } else {
            return $this->lastvisit instanceof \DateTimeInterface ? $this->lastvisit->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdated($format = NULL)
    {
        if ($format === null) {
            return $this->updated;
        } else {
            return $this->updated instanceof \DateTimeInterface ? $this->updated->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [added] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getAdded($format = NULL)
    {
        if ($format === null) {
            return $this->added;
        } else {
            return $this->added instanceof \DateTimeInterface ? $this->added->format($format) : null;
        }
    }

    /**
     * Get the [img] column value.
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Get the [imgs] column value.
     *
     * @return string
     */
    public function getImgs()
    {
        return $this->imgs;
    }

    /**
     * Get the [imgm] column value.
     *
     * @return string
     */
    public function getImgm()
    {
        return $this->imgm;
    }

    /**
     * Get the [imgl] column value.
     *
     * @return string
     */
    public function getImgl()
    {
        return $this->imgl;
    }

    /**
     * Get the [prefs] column value.
     *
     * @return string
     */
    public function getPrefs()
    {
        return $this->prefs;
    }

    /**
     * Get the [admin] column value.
     *
     * @return int
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Get the [perms] column value.
     *
     * @return string
     */
    public function getPerms()
    {
        return $this->perms;
    }

    /**
     * Get the [class] column value.
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get the [sess] column value.
     *
     * @return string
     */
    public function getSess()
    {
        return $this->sess;
    }

    /**
     * Get the [ban] column value.
     *
     * @return int
     */
    public function getBan()
    {
        return $this->ban;
    }

    /**
     * Get the [dr_created] column value.
     *
     * @return string
     */
    public function getDrCreated()
    {
        return $this->dr_created;
    }

    /**
     * Get the [dr_deleted] column value.
     *
     * @return string
     */
    public function getDrDeleted()
    {
        return $this->dr_deleted;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [latest_provider] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setLatestProvider($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->latest_provider !== $v) {
            $this->latest_provider = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_LATEST_PROVIDER] = true;
        }

        return $this;
    } // setLatestProvider()

    /**
     * Set the value of [username] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [first_name] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_name !== $v) {
            $this->first_name = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_FIRST_NAME] = true;
        }

        return $this;
    } // setFirstName()

    /**
     * Set the value of [last_name] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setLastName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->last_name !== $v) {
            $this->last_name = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_LAST_NAME] = true;
        }

        return $this;
    } // setLastName()

    /**
     * Set the value of [display_name] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setDisplayName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->display_name !== $v) {
            $this->display_name = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_DISPLAY_NAME] = true;
        }

        return $this;
    } // setDisplayName()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [email_verified] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setEmailVerified($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_verified !== $v) {
            $this->email_verified = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_EMAIL_VERIFIED] = true;
        }

        return $this;
    } // setEmailVerified()

    /**
     * Set the value of [phone] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [gender] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_GENDER] = true;
        }

        return $this;
    } // setGender()

    /**
     * Set the value of [language] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setLanguage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->language !== $v) {
            $this->language = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_LANGUAGE] = true;
        }

        return $this;
    } // setLanguage()

    /**
     * Sets the value of [birthday] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setBirthday($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->birthday !== null || $dt !== null) {
            if ($this->birthday === null || $dt === null || $dt->format("Y-m-d") !== $this->birthday->format("Y-m-d")) {
                $this->birthday = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioUsersTableMap::COL_BIRTHDAY] = true;
            }
        } // if either are not null

        return $this;
    } // setBirthday()

    /**
     * Set the value of [age] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setAge($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->age !== $v) {
            $this->age = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_AGE] = true;
        }

        return $this;
    } // setAge()

    /**
     * Set the value of [adult] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setAdult($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->adult !== $v) {
            $this->adult = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_ADULT] = true;
        }

        return $this;
    } // setAdult()

    /**
     * Set the value of [address] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_ADDRESS] = true;
        }

        return $this;
    } // setAddress()

    /**
     * Set the value of [country] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->country !== $v) {
            $this->country = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_COUNTRY] = true;
        }

        return $this;
    } // setCountry()

    /**
     * Set the value of [region] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setRegion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->region !== $v) {
            $this->region = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_REGION] = true;
        }

        return $this;
    } // setRegion()

    /**
     * Set the value of [city] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city !== $v) {
            $this->city = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_CITY] = true;
        }

        return $this;
    } // setCity()

    /**
     * Set the value of [zip] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setZip($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zip !== $v) {
            $this->zip = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_ZIP] = true;
        }

        return $this;
    } // setZip()

    /**
     * Set the value of [website_url] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setWebsiteUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->website_url !== $v) {
            $this->website_url = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_WEBSITE_URL] = true;
        }

        return $this;
    } // setWebsiteUrl()

    /**
     * Set the value of [profile_url] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setProfileUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->profile_url !== $v) {
            $this->profile_url = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_PROFILE_URL] = true;
        }

        return $this;
    } // setProfileUrl()

    /**
     * Set the value of [photo_url] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setPhotoUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->photo_url !== $v) {
            $this->photo_url = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_PHOTO_URL] = true;
        }

        return $this;
    } // setPhotoUrl()

    /**
     * Set the value of [ip] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setIp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ip !== $v) {
            $this->ip = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_IP] = true;
        }

        return $this;
    } // setIp()

    /**
     * Set the value of [visits] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setVisits($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->visits !== $v) {
            $this->visits = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_VISITS] = true;
        }

        return $this;
    } // setVisits()

    /**
     * Sets the value of [currentvisit] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setCurrentvisit($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->currentvisit !== null || $dt !== null) {
            if ($this->currentvisit === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->currentvisit->format("Y-m-d H:i:s.u")) {
                $this->currentvisit = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioUsersTableMap::COL_CURRENTVISIT] = true;
            }
        } // if either are not null

        return $this;
    } // setCurrentvisit()

    /**
     * Sets the value of [lastvisit] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setLastvisit($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->lastvisit !== null || $dt !== null) {
            if ($this->lastvisit === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->lastvisit->format("Y-m-d H:i:s.u")) {
                $this->lastvisit = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioUsersTableMap::COL_LASTVISIT] = true;
            }
        } // if either are not null

        return $this;
    } // setLastvisit()

    /**
     * Sets the value of [updated] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setUpdated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated !== null || $dt !== null) {
            if ($this->updated === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated->format("Y-m-d H:i:s.u")) {
                $this->updated = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioUsersTableMap::COL_UPDATED] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdated()

    /**
     * Sets the value of [added] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setAdded($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->added !== null || $dt !== null) {
            if ($this->added === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->added->format("Y-m-d H:i:s.u")) {
                $this->added = $dt === null ? null : clone $dt;
                $this->modifiedColumns[GradioUsersTableMap::COL_ADDED] = true;
            }
        } // if either are not null

        return $this;
    } // setAdded()

    /**
     * Set the value of [img] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setImg($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->img !== $v) {
            $this->img = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_IMG] = true;
        }

        return $this;
    } // setImg()

    /**
     * Set the value of [imgs] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setImgs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->imgs !== $v) {
            $this->imgs = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_IMGS] = true;
        }

        return $this;
    } // setImgs()

    /**
     * Set the value of [imgm] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setImgm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->imgm !== $v) {
            $this->imgm = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_IMGM] = true;
        }

        return $this;
    } // setImgm()

    /**
     * Set the value of [imgl] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setImgl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->imgl !== $v) {
            $this->imgl = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_IMGL] = true;
        }

        return $this;
    } // setImgl()

    /**
     * Set the value of [prefs] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setPrefs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prefs !== $v) {
            $this->prefs = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_PREFS] = true;
        }

        return $this;
    } // setPrefs()

    /**
     * Set the value of [admin] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setAdmin($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->admin !== $v) {
            $this->admin = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_ADMIN] = true;
        }

        return $this;
    } // setAdmin()

    /**
     * Set the value of [perms] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setPerms($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->perms !== $v) {
            $this->perms = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_PERMS] = true;
        }

        return $this;
    } // setPerms()

    /**
     * Set the value of [class] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setClass($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->class !== $v) {
            $this->class = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_CLASS] = true;
        }

        return $this;
    } // setClass()

    /**
     * Set the value of [sess] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setSess($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sess !== $v) {
            $this->sess = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_SESS] = true;
        }

        return $this;
    } // setSess()

    /**
     * Set the value of [ban] column.
     *
     * @param int $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setBan($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ban !== $v) {
            $this->ban = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_BAN] = true;
        }

        return $this;
    } // setBan()

    /**
     * Set the value of [dr_created] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setDrCreated($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dr_created !== $v) {
            $this->dr_created = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_DR_CREATED] = true;
        }

        return $this;
    } // setDrCreated()

    /**
     * Set the value of [dr_deleted] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setDrDeleted($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dr_deleted !== $v) {
            $this->dr_deleted = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_DR_DELETED] = true;
        }

        return $this;
    } // setDrDeleted()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Model\Main\GradioUsers The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[GradioUsersTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->admin !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : GradioUsersTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : GradioUsersTableMap::translateFieldName('LatestProvider', TableMap::TYPE_PHPNAME, $indexType)];
            $this->latest_provider = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : GradioUsersTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : GradioUsersTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : GradioUsersTableMap::translateFieldName('FirstName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->first_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : GradioUsersTableMap::translateFieldName('LastName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->last_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : GradioUsersTableMap::translateFieldName('DisplayName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->display_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : GradioUsersTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : GradioUsersTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : GradioUsersTableMap::translateFieldName('EmailVerified', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email_verified = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : GradioUsersTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : GradioUsersTableMap::translateFieldName('Gender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gender = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : GradioUsersTableMap::translateFieldName('Language', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : GradioUsersTableMap::translateFieldName('Birthday', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->birthday = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : GradioUsersTableMap::translateFieldName('Age', TableMap::TYPE_PHPNAME, $indexType)];
            $this->age = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : GradioUsersTableMap::translateFieldName('Adult', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adult = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : GradioUsersTableMap::translateFieldName('Address', TableMap::TYPE_PHPNAME, $indexType)];
            $this->address = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : GradioUsersTableMap::translateFieldName('Country', TableMap::TYPE_PHPNAME, $indexType)];
            $this->country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : GradioUsersTableMap::translateFieldName('Region', TableMap::TYPE_PHPNAME, $indexType)];
            $this->region = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : GradioUsersTableMap::translateFieldName('City', TableMap::TYPE_PHPNAME, $indexType)];
            $this->city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : GradioUsersTableMap::translateFieldName('Zip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : GradioUsersTableMap::translateFieldName('WebsiteUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->website_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : GradioUsersTableMap::translateFieldName('ProfileUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->profile_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : GradioUsersTableMap::translateFieldName('PhotoUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->photo_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : GradioUsersTableMap::translateFieldName('Ip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : GradioUsersTableMap::translateFieldName('Visits', TableMap::TYPE_PHPNAME, $indexType)];
            $this->visits = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : GradioUsersTableMap::translateFieldName('Currentvisit', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->currentvisit = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : GradioUsersTableMap::translateFieldName('Lastvisit', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->lastvisit = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : GradioUsersTableMap::translateFieldName('Updated', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : GradioUsersTableMap::translateFieldName('Added', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->added = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : GradioUsersTableMap::translateFieldName('Img', TableMap::TYPE_PHPNAME, $indexType)];
            $this->img = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : GradioUsersTableMap::translateFieldName('Imgs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->imgs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : GradioUsersTableMap::translateFieldName('Imgm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->imgm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : GradioUsersTableMap::translateFieldName('Imgl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->imgl = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : GradioUsersTableMap::translateFieldName('Prefs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prefs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : GradioUsersTableMap::translateFieldName('Admin', TableMap::TYPE_PHPNAME, $indexType)];
            $this->admin = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : GradioUsersTableMap::translateFieldName('Perms', TableMap::TYPE_PHPNAME, $indexType)];
            $this->perms = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : GradioUsersTableMap::translateFieldName('Class', TableMap::TYPE_PHPNAME, $indexType)];
            $this->class = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : GradioUsersTableMap::translateFieldName('Sess', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sess = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : GradioUsersTableMap::translateFieldName('Ban', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ban = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : GradioUsersTableMap::translateFieldName('DrCreated', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dr_created = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 41 + $startcol : GradioUsersTableMap::translateFieldName('DrDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dr_deleted = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 42 + $startcol : GradioUsersTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 43; // 43 = GradioUsersTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Main\\GradioUsers'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildGradioUsersQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see GradioUsers::setDeleted()
     * @see GradioUsers::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildGradioUsersQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUsersTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                GradioUsersTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[GradioUsersTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . GradioUsersTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(GradioUsersTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LATEST_PROVIDER)) {
            $modifiedColumns[':p' . $index++]  = 'latest_provider';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'username';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'first_name';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LAST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'last_name';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DISPLAY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'display_name';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_EMAIL_VERIFIED)) {
            $modifiedColumns[':p' . $index++]  = 'email_verified';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'phone';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_GENDER)) {
            $modifiedColumns[':p' . $index++]  = 'gender';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LANGUAGE)) {
            $modifiedColumns[':p' . $index++]  = 'language';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = 'birthday';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_AGE)) {
            $modifiedColumns[':p' . $index++]  = 'age';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADULT)) {
            $modifiedColumns[':p' . $index++]  = 'adult';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'address';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'country';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_REGION)) {
            $modifiedColumns[':p' . $index++]  = 'region';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'city';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ZIP)) {
            $modifiedColumns[':p' . $index++]  = 'zip';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_WEBSITE_URL)) {
            $modifiedColumns[':p' . $index++]  = 'website_url';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PROFILE_URL)) {
            $modifiedColumns[':p' . $index++]  = 'profile_url';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PHOTO_URL)) {
            $modifiedColumns[':p' . $index++]  = 'photo_url';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IP)) {
            $modifiedColumns[':p' . $index++]  = 'ip';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_VISITS)) {
            $modifiedColumns[':p' . $index++]  = 'visits';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_CURRENTVISIT)) {
            $modifiedColumns[':p' . $index++]  = 'currentvisit';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LASTVISIT)) {
            $modifiedColumns[':p' . $index++]  = 'lastvisit';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_UPDATED)) {
            $modifiedColumns[':p' . $index++]  = 'updated';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADDED)) {
            $modifiedColumns[':p' . $index++]  = 'added';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMG)) {
            $modifiedColumns[':p' . $index++]  = 'img';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMGS)) {
            $modifiedColumns[':p' . $index++]  = 'imgs';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMGM)) {
            $modifiedColumns[':p' . $index++]  = 'imgm';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMGL)) {
            $modifiedColumns[':p' . $index++]  = 'imgl';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PREFS)) {
            $modifiedColumns[':p' . $index++]  = 'prefs';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADMIN)) {
            $modifiedColumns[':p' . $index++]  = 'admin';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PERMS)) {
            $modifiedColumns[':p' . $index++]  = 'perms';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_CLASS)) {
            $modifiedColumns[':p' . $index++]  = 'class';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_SESS)) {
            $modifiedColumns[':p' . $index++]  = 'sess';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_BAN)) {
            $modifiedColumns[':p' . $index++]  = 'ban';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DR_CREATED)) {
            $modifiedColumns[':p' . $index++]  = 'dr_created';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DR_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'dr_deleted';
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }

        $sql = sprintf(
            'INSERT INTO gradio_users (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'latest_provider':
                        $stmt->bindValue($identifier, $this->latest_provider, PDO::PARAM_STR);
                        break;
                    case 'username':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'first_name':
                        $stmt->bindValue($identifier, $this->first_name, PDO::PARAM_STR);
                        break;
                    case 'last_name':
                        $stmt->bindValue($identifier, $this->last_name, PDO::PARAM_STR);
                        break;
                    case 'display_name':
                        $stmt->bindValue($identifier, $this->display_name, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'email_verified':
                        $stmt->bindValue($identifier, $this->email_verified, PDO::PARAM_STR);
                        break;
                    case 'phone':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_INT);
                        break;
                    case 'gender':
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case 'language':
                        $stmt->bindValue($identifier, $this->language, PDO::PARAM_STR);
                        break;
                    case 'birthday':
                        $stmt->bindValue($identifier, $this->birthday ? $this->birthday->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'age':
                        $stmt->bindValue($identifier, $this->age, PDO::PARAM_INT);
                        break;
                    case 'adult':
                        $stmt->bindValue($identifier, $this->adult, PDO::PARAM_INT);
                        break;
                    case 'address':
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case 'country':
                        $stmt->bindValue($identifier, $this->country, PDO::PARAM_STR);
                        break;
                    case 'region':
                        $stmt->bindValue($identifier, $this->region, PDO::PARAM_STR);
                        break;
                    case 'city':
                        $stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
                        break;
                    case 'zip':
                        $stmt->bindValue($identifier, $this->zip, PDO::PARAM_STR);
                        break;
                    case 'website_url':
                        $stmt->bindValue($identifier, $this->website_url, PDO::PARAM_STR);
                        break;
                    case 'profile_url':
                        $stmt->bindValue($identifier, $this->profile_url, PDO::PARAM_STR);
                        break;
                    case 'photo_url':
                        $stmt->bindValue($identifier, $this->photo_url, PDO::PARAM_STR);
                        break;
                    case 'ip':
                        $stmt->bindValue($identifier, $this->ip, PDO::PARAM_STR);
                        break;
                    case 'visits':
                        $stmt->bindValue($identifier, $this->visits, PDO::PARAM_INT);
                        break;
                    case 'currentvisit':
                        $stmt->bindValue($identifier, $this->currentvisit ? $this->currentvisit->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'lastvisit':
                        $stmt->bindValue($identifier, $this->lastvisit ? $this->lastvisit->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated':
                        $stmt->bindValue($identifier, $this->updated ? $this->updated->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'added':
                        $stmt->bindValue($identifier, $this->added ? $this->added->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'img':
                        $stmt->bindValue($identifier, $this->img, PDO::PARAM_STR);
                        break;
                    case 'imgs':
                        $stmt->bindValue($identifier, $this->imgs, PDO::PARAM_STR);
                        break;
                    case 'imgm':
                        $stmt->bindValue($identifier, $this->imgm, PDO::PARAM_STR);
                        break;
                    case 'imgl':
                        $stmt->bindValue($identifier, $this->imgl, PDO::PARAM_STR);
                        break;
                    case 'prefs':
                        $stmt->bindValue($identifier, $this->prefs, PDO::PARAM_STR);
                        break;
                    case 'admin':
                        $stmt->bindValue($identifier, $this->admin, PDO::PARAM_INT);
                        break;
                    case 'perms':
                        $stmt->bindValue($identifier, $this->perms, PDO::PARAM_STR);
                        break;
                    case 'class':
                        $stmt->bindValue($identifier, $this->class, PDO::PARAM_STR);
                        break;
                    case 'sess':
                        $stmt->bindValue($identifier, $this->sess, PDO::PARAM_STR);
                        break;
                    case 'ban':
                        $stmt->bindValue($identifier, $this->ban, PDO::PARAM_INT);
                        break;
                    case 'dr_created':
                        $stmt->bindValue($identifier, $this->dr_created, PDO::PARAM_STR);
                        break;
                    case 'dr_deleted':
                        $stmt->bindValue($identifier, $this->dr_deleted, PDO::PARAM_STR);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GradioUsersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLatestProvider();
                break;
            case 2:
                return $this->getUsername();
                break;
            case 3:
                return $this->getPassword();
                break;
            case 4:
                return $this->getFirstName();
                break;
            case 5:
                return $this->getLastName();
                break;
            case 6:
                return $this->getDisplayName();
                break;
            case 7:
                return $this->getDescription();
                break;
            case 8:
                return $this->getEmail();
                break;
            case 9:
                return $this->getEmailVerified();
                break;
            case 10:
                return $this->getPhone();
                break;
            case 11:
                return $this->getGender();
                break;
            case 12:
                return $this->getLanguage();
                break;
            case 13:
                return $this->getBirthday();
                break;
            case 14:
                return $this->getAge();
                break;
            case 15:
                return $this->getAdult();
                break;
            case 16:
                return $this->getAddress();
                break;
            case 17:
                return $this->getCountry();
                break;
            case 18:
                return $this->getRegion();
                break;
            case 19:
                return $this->getCity();
                break;
            case 20:
                return $this->getZip();
                break;
            case 21:
                return $this->getWebsiteUrl();
                break;
            case 22:
                return $this->getProfileUrl();
                break;
            case 23:
                return $this->getPhotoUrl();
                break;
            case 24:
                return $this->getIp();
                break;
            case 25:
                return $this->getVisits();
                break;
            case 26:
                return $this->getCurrentvisit();
                break;
            case 27:
                return $this->getLastvisit();
                break;
            case 28:
                return $this->getUpdated();
                break;
            case 29:
                return $this->getAdded();
                break;
            case 30:
                return $this->getImg();
                break;
            case 31:
                return $this->getImgs();
                break;
            case 32:
                return $this->getImgm();
                break;
            case 33:
                return $this->getImgl();
                break;
            case 34:
                return $this->getPrefs();
                break;
            case 35:
                return $this->getAdmin();
                break;
            case 36:
                return $this->getPerms();
                break;
            case 37:
                return $this->getClass();
                break;
            case 38:
                return $this->getSess();
                break;
            case 39:
                return $this->getBan();
                break;
            case 40:
                return $this->getDrCreated();
                break;
            case 41:
                return $this->getDrDeleted();
                break;
            case 42:
                return $this->getType();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['GradioUsers'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['GradioUsers'][$this->hashCode()] = true;
        $keys = GradioUsersTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLatestProvider(),
            $keys[2] => $this->getUsername(),
            $keys[3] => $this->getPassword(),
            $keys[4] => $this->getFirstName(),
            $keys[5] => $this->getLastName(),
            $keys[6] => $this->getDisplayName(),
            $keys[7] => $this->getDescription(),
            $keys[8] => $this->getEmail(),
            $keys[9] => $this->getEmailVerified(),
            $keys[10] => $this->getPhone(),
            $keys[11] => $this->getGender(),
            $keys[12] => $this->getLanguage(),
            $keys[13] => $this->getBirthday(),
            $keys[14] => $this->getAge(),
            $keys[15] => $this->getAdult(),
            $keys[16] => $this->getAddress(),
            $keys[17] => $this->getCountry(),
            $keys[18] => $this->getRegion(),
            $keys[19] => $this->getCity(),
            $keys[20] => $this->getZip(),
            $keys[21] => $this->getWebsiteUrl(),
            $keys[22] => $this->getProfileUrl(),
            $keys[23] => $this->getPhotoUrl(),
            $keys[24] => $this->getIp(),
            $keys[25] => $this->getVisits(),
            $keys[26] => $this->getCurrentvisit(),
            $keys[27] => $this->getLastvisit(),
            $keys[28] => $this->getUpdated(),
            $keys[29] => $this->getAdded(),
            $keys[30] => $this->getImg(),
            $keys[31] => $this->getImgs(),
            $keys[32] => $this->getImgm(),
            $keys[33] => $this->getImgl(),
            $keys[34] => $this->getPrefs(),
            $keys[35] => $this->getAdmin(),
            $keys[36] => $this->getPerms(),
            $keys[37] => $this->getClass(),
            $keys[38] => $this->getSess(),
            $keys[39] => $this->getBan(),
            $keys[40] => $this->getDrCreated(),
            $keys[41] => $this->getDrDeleted(),
            $keys[42] => $this->getType(),
        );
        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[26]] instanceof \DateTimeInterface) {
            $result[$keys[26]] = $result[$keys[26]]->format('c');
        }

        if ($result[$keys[27]] instanceof \DateTimeInterface) {
            $result[$keys[27]] = $result[$keys[27]]->format('c');
        }

        if ($result[$keys[28]] instanceof \DateTimeInterface) {
            $result[$keys[28]] = $result[$keys[28]]->format('c');
        }

        if ($result[$keys[29]] instanceof \DateTimeInterface) {
            $result[$keys[29]] = $result[$keys[29]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Main\GradioUsers
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GradioUsersTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Main\GradioUsers
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLatestProvider($value);
                break;
            case 2:
                $this->setUsername($value);
                break;
            case 3:
                $this->setPassword($value);
                break;
            case 4:
                $this->setFirstName($value);
                break;
            case 5:
                $this->setLastName($value);
                break;
            case 6:
                $this->setDisplayName($value);
                break;
            case 7:
                $this->setDescription($value);
                break;
            case 8:
                $this->setEmail($value);
                break;
            case 9:
                $this->setEmailVerified($value);
                break;
            case 10:
                $this->setPhone($value);
                break;
            case 11:
                $this->setGender($value);
                break;
            case 12:
                $this->setLanguage($value);
                break;
            case 13:
                $this->setBirthday($value);
                break;
            case 14:
                $this->setAge($value);
                break;
            case 15:
                $this->setAdult($value);
                break;
            case 16:
                $this->setAddress($value);
                break;
            case 17:
                $this->setCountry($value);
                break;
            case 18:
                $this->setRegion($value);
                break;
            case 19:
                $this->setCity($value);
                break;
            case 20:
                $this->setZip($value);
                break;
            case 21:
                $this->setWebsiteUrl($value);
                break;
            case 22:
                $this->setProfileUrl($value);
                break;
            case 23:
                $this->setPhotoUrl($value);
                break;
            case 24:
                $this->setIp($value);
                break;
            case 25:
                $this->setVisits($value);
                break;
            case 26:
                $this->setCurrentvisit($value);
                break;
            case 27:
                $this->setLastvisit($value);
                break;
            case 28:
                $this->setUpdated($value);
                break;
            case 29:
                $this->setAdded($value);
                break;
            case 30:
                $this->setImg($value);
                break;
            case 31:
                $this->setImgs($value);
                break;
            case 32:
                $this->setImgm($value);
                break;
            case 33:
                $this->setImgl($value);
                break;
            case 34:
                $this->setPrefs($value);
                break;
            case 35:
                $this->setAdmin($value);
                break;
            case 36:
                $this->setPerms($value);
                break;
            case 37:
                $this->setClass($value);
                break;
            case 38:
                $this->setSess($value);
                break;
            case 39:
                $this->setBan($value);
                break;
            case 40:
                $this->setDrCreated($value);
                break;
            case 41:
                $this->setDrDeleted($value);
                break;
            case 42:
                $this->setType($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = GradioUsersTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setLatestProvider($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setUsername($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPassword($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setFirstName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLastName($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDisplayName($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDescription($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setEmail($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setEmailVerified($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPhone($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setGender($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setLanguage($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setBirthday($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setAge($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setAdult($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setAddress($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCountry($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setRegion($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCity($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setZip($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setWebsiteUrl($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setProfileUrl($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setPhotoUrl($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setIp($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setVisits($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setCurrentvisit($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setLastvisit($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setUpdated($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setAdded($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setImg($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setImgs($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setImgm($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setImgl($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setPrefs($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setAdmin($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setPerms($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setClass($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setSess($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setBan($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setDrCreated($arr[$keys[40]]);
        }
        if (array_key_exists($keys[41], $arr)) {
            $this->setDrDeleted($arr[$keys[41]]);
        }
        if (array_key_exists($keys[42], $arr)) {
            $this->setType($arr[$keys[42]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Main\GradioUsers The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(GradioUsersTableMap::DATABASE_NAME);

        if ($this->isColumnModified(GradioUsersTableMap::COL_ID)) {
            $criteria->add(GradioUsersTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LATEST_PROVIDER)) {
            $criteria->add(GradioUsersTableMap::COL_LATEST_PROVIDER, $this->latest_provider);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_USERNAME)) {
            $criteria->add(GradioUsersTableMap::COL_USERNAME, $this->username);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PASSWORD)) {
            $criteria->add(GradioUsersTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_FIRST_NAME)) {
            $criteria->add(GradioUsersTableMap::COL_FIRST_NAME, $this->first_name);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LAST_NAME)) {
            $criteria->add(GradioUsersTableMap::COL_LAST_NAME, $this->last_name);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DISPLAY_NAME)) {
            $criteria->add(GradioUsersTableMap::COL_DISPLAY_NAME, $this->display_name);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DESCRIPTION)) {
            $criteria->add(GradioUsersTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_EMAIL)) {
            $criteria->add(GradioUsersTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_EMAIL_VERIFIED)) {
            $criteria->add(GradioUsersTableMap::COL_EMAIL_VERIFIED, $this->email_verified);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PHONE)) {
            $criteria->add(GradioUsersTableMap::COL_PHONE, $this->phone);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_GENDER)) {
            $criteria->add(GradioUsersTableMap::COL_GENDER, $this->gender);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LANGUAGE)) {
            $criteria->add(GradioUsersTableMap::COL_LANGUAGE, $this->language);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_BIRTHDAY)) {
            $criteria->add(GradioUsersTableMap::COL_BIRTHDAY, $this->birthday);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_AGE)) {
            $criteria->add(GradioUsersTableMap::COL_AGE, $this->age);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADULT)) {
            $criteria->add(GradioUsersTableMap::COL_ADULT, $this->adult);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADDRESS)) {
            $criteria->add(GradioUsersTableMap::COL_ADDRESS, $this->address);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_COUNTRY)) {
            $criteria->add(GradioUsersTableMap::COL_COUNTRY, $this->country);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_REGION)) {
            $criteria->add(GradioUsersTableMap::COL_REGION, $this->region);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_CITY)) {
            $criteria->add(GradioUsersTableMap::COL_CITY, $this->city);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ZIP)) {
            $criteria->add(GradioUsersTableMap::COL_ZIP, $this->zip);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_WEBSITE_URL)) {
            $criteria->add(GradioUsersTableMap::COL_WEBSITE_URL, $this->website_url);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PROFILE_URL)) {
            $criteria->add(GradioUsersTableMap::COL_PROFILE_URL, $this->profile_url);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PHOTO_URL)) {
            $criteria->add(GradioUsersTableMap::COL_PHOTO_URL, $this->photo_url);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IP)) {
            $criteria->add(GradioUsersTableMap::COL_IP, $this->ip);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_VISITS)) {
            $criteria->add(GradioUsersTableMap::COL_VISITS, $this->visits);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_CURRENTVISIT)) {
            $criteria->add(GradioUsersTableMap::COL_CURRENTVISIT, $this->currentvisit);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_LASTVISIT)) {
            $criteria->add(GradioUsersTableMap::COL_LASTVISIT, $this->lastvisit);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_UPDATED)) {
            $criteria->add(GradioUsersTableMap::COL_UPDATED, $this->updated);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADDED)) {
            $criteria->add(GradioUsersTableMap::COL_ADDED, $this->added);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMG)) {
            $criteria->add(GradioUsersTableMap::COL_IMG, $this->img);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMGS)) {
            $criteria->add(GradioUsersTableMap::COL_IMGS, $this->imgs);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMGM)) {
            $criteria->add(GradioUsersTableMap::COL_IMGM, $this->imgm);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_IMGL)) {
            $criteria->add(GradioUsersTableMap::COL_IMGL, $this->imgl);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PREFS)) {
            $criteria->add(GradioUsersTableMap::COL_PREFS, $this->prefs);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_ADMIN)) {
            $criteria->add(GradioUsersTableMap::COL_ADMIN, $this->admin);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_PERMS)) {
            $criteria->add(GradioUsersTableMap::COL_PERMS, $this->perms);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_CLASS)) {
            $criteria->add(GradioUsersTableMap::COL_CLASS, $this->class);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_SESS)) {
            $criteria->add(GradioUsersTableMap::COL_SESS, $this->sess);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_BAN)) {
            $criteria->add(GradioUsersTableMap::COL_BAN, $this->ban);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DR_CREATED)) {
            $criteria->add(GradioUsersTableMap::COL_DR_CREATED, $this->dr_created);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_DR_DELETED)) {
            $criteria->add(GradioUsersTableMap::COL_DR_DELETED, $this->dr_deleted);
        }
        if ($this->isColumnModified(GradioUsersTableMap::COL_TYPE)) {
            $criteria->add(GradioUsersTableMap::COL_TYPE, $this->type);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildGradioUsersQuery::create();
        $criteria->add(GradioUsersTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Main\GradioUsers (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLatestProvider($this->getLatestProvider());
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setFirstName($this->getFirstName());
        $copyObj->setLastName($this->getLastName());
        $copyObj->setDisplayName($this->getDisplayName());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setEmailVerified($this->getEmailVerified());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setGender($this->getGender());
        $copyObj->setLanguage($this->getLanguage());
        $copyObj->setBirthday($this->getBirthday());
        $copyObj->setAge($this->getAge());
        $copyObj->setAdult($this->getAdult());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setRegion($this->getRegion());
        $copyObj->setCity($this->getCity());
        $copyObj->setZip($this->getZip());
        $copyObj->setWebsiteUrl($this->getWebsiteUrl());
        $copyObj->setProfileUrl($this->getProfileUrl());
        $copyObj->setPhotoUrl($this->getPhotoUrl());
        $copyObj->setIp($this->getIp());
        $copyObj->setVisits($this->getVisits());
        $copyObj->setCurrentvisit($this->getCurrentvisit());
        $copyObj->setLastvisit($this->getLastvisit());
        $copyObj->setUpdated($this->getUpdated());
        $copyObj->setAdded($this->getAdded());
        $copyObj->setImg($this->getImg());
        $copyObj->setImgs($this->getImgs());
        $copyObj->setImgm($this->getImgm());
        $copyObj->setImgl($this->getImgl());
        $copyObj->setPrefs($this->getPrefs());
        $copyObj->setAdmin($this->getAdmin());
        $copyObj->setPerms($this->getPerms());
        $copyObj->setClass($this->getClass());
        $copyObj->setSess($this->getSess());
        $copyObj->setBan($this->getBan());
        $copyObj->setDrCreated($this->getDrCreated());
        $copyObj->setDrDeleted($this->getDrDeleted());
        $copyObj->setType($this->getType());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Main\GradioUsers Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->latest_provider = null;
        $this->username = null;
        $this->password = null;
        $this->first_name = null;
        $this->last_name = null;
        $this->display_name = null;
        $this->description = null;
        $this->email = null;
        $this->email_verified = null;
        $this->phone = null;
        $this->gender = null;
        $this->language = null;
        $this->birthday = null;
        $this->age = null;
        $this->adult = null;
        $this->address = null;
        $this->country = null;
        $this->region = null;
        $this->city = null;
        $this->zip = null;
        $this->website_url = null;
        $this->profile_url = null;
        $this->photo_url = null;
        $this->ip = null;
        $this->visits = null;
        $this->currentvisit = null;
        $this->lastvisit = null;
        $this->updated = null;
        $this->added = null;
        $this->img = null;
        $this->imgs = null;
        $this->imgm = null;
        $this->imgl = null;
        $this->prefs = null;
        $this->admin = null;
        $this->perms = null;
        $this->class = null;
        $this->sess = null;
        $this->ban = null;
        $this->dr_created = null;
        $this->dr_deleted = null;
        $this->type = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(GradioUsersTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
