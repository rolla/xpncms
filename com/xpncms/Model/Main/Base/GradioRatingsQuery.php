<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioRatings as ChildGradioRatings;
use Model\Main\GradioRatingsQuery as ChildGradioRatingsQuery;
use Model\Main\Map\GradioRatingsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_ratings' table.
 *
 *
 *
 * @method     ChildGradioRatingsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioRatingsQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildGradioRatingsQuery orderByItemId($order = Criteria::ASC) Order by the item_id column
 * @method     ChildGradioRatingsQuery orderByActionId($order = Criteria::ASC) Order by the action_id column
 * @method     ChildGradioRatingsQuery orderByCategoryId($order = Criteria::ASC) Order by the category_id column
 * @method     ChildGradioRatingsQuery orderByModel($order = Criteria::ASC) Order by the model column
 * @method     ChildGradioRatingsQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildGradioRatingsQuery orderByCepumi($order = Criteria::ASC) Order by the cepumi column
 * @method     ChildGradioRatingsQuery orderByExtrarating($order = Criteria::ASC) Order by the extrarating column
 * @method     ChildGradioRatingsQuery orderByNickname($order = Criteria::ASC) Order by the nickname column
 * @method     ChildGradioRatingsQuery orderByComment($order = Criteria::ASC) Order by the comment column
 * @method     ChildGradioRatingsQuery orderByVuserid($order = Criteria::ASC) Order by the vuserid column
 * @method     ChildGradioRatingsQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildGradioRatingsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildGradioRatingsQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildGradioRatingsQuery groupById() Group by the id column
 * @method     ChildGradioRatingsQuery groupByUserId() Group by the user_id column
 * @method     ChildGradioRatingsQuery groupByItemId() Group by the item_id column
 * @method     ChildGradioRatingsQuery groupByActionId() Group by the action_id column
 * @method     ChildGradioRatingsQuery groupByCategoryId() Group by the category_id column
 * @method     ChildGradioRatingsQuery groupByModel() Group by the model column
 * @method     ChildGradioRatingsQuery groupByIp() Group by the ip column
 * @method     ChildGradioRatingsQuery groupByCepumi() Group by the cepumi column
 * @method     ChildGradioRatingsQuery groupByExtrarating() Group by the extrarating column
 * @method     ChildGradioRatingsQuery groupByNickname() Group by the nickname column
 * @method     ChildGradioRatingsQuery groupByComment() Group by the comment column
 * @method     ChildGradioRatingsQuery groupByVuserid() Group by the vuserid column
 * @method     ChildGradioRatingsQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildGradioRatingsQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildGradioRatingsQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildGradioRatingsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioRatingsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioRatingsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioRatingsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioRatingsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioRatingsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioRatings findOne(ConnectionInterface $con = null) Return the first ChildGradioRatings matching the query
 * @method     ChildGradioRatings findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioRatings matching the query, or a new ChildGradioRatings object populated from the query conditions when no match is found
 *
 * @method     ChildGradioRatings findOneById(int $id) Return the first ChildGradioRatings filtered by the id column
 * @method     ChildGradioRatings findOneByUserId(int $user_id) Return the first ChildGradioRatings filtered by the user_id column
 * @method     ChildGradioRatings findOneByItemId(int $item_id) Return the first ChildGradioRatings filtered by the item_id column
 * @method     ChildGradioRatings findOneByActionId(int $action_id) Return the first ChildGradioRatings filtered by the action_id column
 * @method     ChildGradioRatings findOneByCategoryId(int $category_id) Return the first ChildGradioRatings filtered by the category_id column
 * @method     ChildGradioRatings findOneByModel(string $model) Return the first ChildGradioRatings filtered by the model column
 * @method     ChildGradioRatings findOneByIp(string $ip) Return the first ChildGradioRatings filtered by the ip column
 * @method     ChildGradioRatings findOneByCepumi(string $cepumi) Return the first ChildGradioRatings filtered by the cepumi column
 * @method     ChildGradioRatings findOneByExtrarating(string $extrarating) Return the first ChildGradioRatings filtered by the extrarating column
 * @method     ChildGradioRatings findOneByNickname(string $nickname) Return the first ChildGradioRatings filtered by the nickname column
 * @method     ChildGradioRatings findOneByComment(string $comment) Return the first ChildGradioRatings filtered by the comment column
 * @method     ChildGradioRatings findOneByVuserid(int $vuserid) Return the first ChildGradioRatings filtered by the vuserid column
 * @method     ChildGradioRatings findOneByCreatedAt(string $created_at) Return the first ChildGradioRatings filtered by the created_at column
 * @method     ChildGradioRatings findOneByUpdatedAt(string $updated_at) Return the first ChildGradioRatings filtered by the updated_at column
 * @method     ChildGradioRatings findOneByDeletedAt(string $deleted_at) Return the first ChildGradioRatings filtered by the deleted_at column *

 * @method     ChildGradioRatings requirePk($key, ConnectionInterface $con = null) Return the ChildGradioRatings by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOne(ConnectionInterface $con = null) Return the first ChildGradioRatings matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioRatings requireOneById(int $id) Return the first ChildGradioRatings filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByUserId(int $user_id) Return the first ChildGradioRatings filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByItemId(int $item_id) Return the first ChildGradioRatings filtered by the item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByActionId(int $action_id) Return the first ChildGradioRatings filtered by the action_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByCategoryId(int $category_id) Return the first ChildGradioRatings filtered by the category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByModel(string $model) Return the first ChildGradioRatings filtered by the model column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByIp(string $ip) Return the first ChildGradioRatings filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByCepumi(string $cepumi) Return the first ChildGradioRatings filtered by the cepumi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByExtrarating(string $extrarating) Return the first ChildGradioRatings filtered by the extrarating column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByNickname(string $nickname) Return the first ChildGradioRatings filtered by the nickname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByComment(string $comment) Return the first ChildGradioRatings filtered by the comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByVuserid(int $vuserid) Return the first ChildGradioRatings filtered by the vuserid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByCreatedAt(string $created_at) Return the first ChildGradioRatings filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByUpdatedAt(string $updated_at) Return the first ChildGradioRatings filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioRatings requireOneByDeletedAt(string $deleted_at) Return the first ChildGradioRatings filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioRatings[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioRatings objects based on current ModelCriteria
 * @method     ChildGradioRatings[]|ObjectCollection findById(int $id) Return ChildGradioRatings objects filtered by the id column
 * @method     ChildGradioRatings[]|ObjectCollection findByUserId(int $user_id) Return ChildGradioRatings objects filtered by the user_id column
 * @method     ChildGradioRatings[]|ObjectCollection findByItemId(int $item_id) Return ChildGradioRatings objects filtered by the item_id column
 * @method     ChildGradioRatings[]|ObjectCollection findByActionId(int $action_id) Return ChildGradioRatings objects filtered by the action_id column
 * @method     ChildGradioRatings[]|ObjectCollection findByCategoryId(int $category_id) Return ChildGradioRatings objects filtered by the category_id column
 * @method     ChildGradioRatings[]|ObjectCollection findByModel(string $model) Return ChildGradioRatings objects filtered by the model column
 * @method     ChildGradioRatings[]|ObjectCollection findByIp(string $ip) Return ChildGradioRatings objects filtered by the ip column
 * @method     ChildGradioRatings[]|ObjectCollection findByCepumi(string $cepumi) Return ChildGradioRatings objects filtered by the cepumi column
 * @method     ChildGradioRatings[]|ObjectCollection findByExtrarating(string $extrarating) Return ChildGradioRatings objects filtered by the extrarating column
 * @method     ChildGradioRatings[]|ObjectCollection findByNickname(string $nickname) Return ChildGradioRatings objects filtered by the nickname column
 * @method     ChildGradioRatings[]|ObjectCollection findByComment(string $comment) Return ChildGradioRatings objects filtered by the comment column
 * @method     ChildGradioRatings[]|ObjectCollection findByVuserid(int $vuserid) Return ChildGradioRatings objects filtered by the vuserid column
 * @method     ChildGradioRatings[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildGradioRatings objects filtered by the created_at column
 * @method     ChildGradioRatings[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildGradioRatings objects filtered by the updated_at column
 * @method     ChildGradioRatings[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildGradioRatings objects filtered by the deleted_at column
 * @method     ChildGradioRatings[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioRatingsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioRatingsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioRatings', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioRatingsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioRatingsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioRatingsQuery) {
            return $criteria;
        }
        $query = new ChildGradioRatingsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioRatings|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioRatingsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioRatingsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioRatings A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, item_id, action_id, category_id, model, ip, cepumi, extrarating, nickname, comment, vuserid, created_at, updated_at, deleted_at FROM gradio_ratings WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioRatings $obj */
            $obj = new ChildGradioRatings();
            $obj->hydrate($row);
            GradioRatingsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioRatings|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioRatingsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioRatingsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the item_id column
     *
     * Example usage:
     * <code>
     * $query->filterByItemId(1234); // WHERE item_id = 1234
     * $query->filterByItemId(array(12, 34)); // WHERE item_id IN (12, 34)
     * $query->filterByItemId(array('min' => 12)); // WHERE item_id > 12
     * </code>
     *
     * @param     mixed $itemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByItemId($itemId = null, $comparison = null)
    {
        if (is_array($itemId)) {
            $useMinMax = false;
            if (isset($itemId['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_ITEM_ID, $itemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($itemId['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_ITEM_ID, $itemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_ITEM_ID, $itemId, $comparison);
    }

    /**
     * Filter the query on the action_id column
     *
     * Example usage:
     * <code>
     * $query->filterByActionId(1234); // WHERE action_id = 1234
     * $query->filterByActionId(array(12, 34)); // WHERE action_id IN (12, 34)
     * $query->filterByActionId(array('min' => 12)); // WHERE action_id > 12
     * </code>
     *
     * @param     mixed $actionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByActionId($actionId = null, $comparison = null)
    {
        if (is_array($actionId)) {
            $useMinMax = false;
            if (isset($actionId['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_ACTION_ID, $actionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionId['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_ACTION_ID, $actionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_ACTION_ID, $actionId, $comparison);
    }

    /**
     * Filter the query on the category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE category_id = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE category_id IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE category_id > 12
     * </code>
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the model column
     *
     * Example usage:
     * <code>
     * $query->filterByModel('fooValue');   // WHERE model = 'fooValue'
     * $query->filterByModel('%fooValue%', Criteria::LIKE); // WHERE model LIKE '%fooValue%'
     * </code>
     *
     * @param     string $model The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByModel($model = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($model)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_MODEL, $model, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the cepumi column
     *
     * Example usage:
     * <code>
     * $query->filterByCepumi('fooValue');   // WHERE cepumi = 'fooValue'
     * $query->filterByCepumi('%fooValue%', Criteria::LIKE); // WHERE cepumi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cepumi The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByCepumi($cepumi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cepumi)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_CEPUMI, $cepumi, $comparison);
    }

    /**
     * Filter the query on the extrarating column
     *
     * Example usage:
     * <code>
     * $query->filterByExtrarating('fooValue');   // WHERE extrarating = 'fooValue'
     * $query->filterByExtrarating('%fooValue%', Criteria::LIKE); // WHERE extrarating LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extrarating The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByExtrarating($extrarating = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extrarating)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_EXTRARATING, $extrarating, $comparison);
    }

    /**
     * Filter the query on the nickname column
     *
     * Example usage:
     * <code>
     * $query->filterByNickname('fooValue');   // WHERE nickname = 'fooValue'
     * $query->filterByNickname('%fooValue%', Criteria::LIKE); // WHERE nickname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nickname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByNickname($nickname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nickname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_NICKNAME, $nickname, $comparison);
    }

    /**
     * Filter the query on the comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE comment = 'fooValue'
     * $query->filterByComment('%fooValue%', Criteria::LIKE); // WHERE comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comment The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByComment($comment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_COMMENT, $comment, $comparison);
    }

    /**
     * Filter the query on the vuserid column
     *
     * Example usage:
     * <code>
     * $query->filterByVuserid(1234); // WHERE vuserid = 1234
     * $query->filterByVuserid(array(12, 34)); // WHERE vuserid IN (12, 34)
     * $query->filterByVuserid(array('min' => 12)); // WHERE vuserid > 12
     * </code>
     *
     * @param     mixed $vuserid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByVuserid($vuserid = null, $comparison = null)
    {
        if (is_array($vuserid)) {
            $useMinMax = false;
            if (isset($vuserid['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_VUSERID, $vuserid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vuserid['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_VUSERID, $vuserid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_VUSERID, $vuserid, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(GradioRatingsTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioRatingsTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioRatings $gradioRatings Object to remove from the list of results
     *
     * @return $this|ChildGradioRatingsQuery The current query, for fluid interface
     */
    public function prune($gradioRatings = null)
    {
        if ($gradioRatings) {
            $this->addUsingAlias(GradioRatingsTableMap::COL_ID, $gradioRatings->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_ratings table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioRatingsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioRatingsTableMap::clearInstancePool();
            GradioRatingsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioRatingsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioRatingsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioRatingsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioRatingsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioRatingsQuery
