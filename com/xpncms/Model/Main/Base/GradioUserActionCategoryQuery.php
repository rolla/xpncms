<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioUserActionCategory as ChildGradioUserActionCategory;
use Model\Main\GradioUserActionCategoryQuery as ChildGradioUserActionCategoryQuery;
use Model\Main\Map\GradioUserActionCategoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_user_action_category' table.
 *
 *
 *
 * @method     ChildGradioUserActionCategoryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioUserActionCategoryQuery orderByActionId($order = Criteria::ASC) Order by the action_id column
 * @method     ChildGradioUserActionCategoryQuery orderByCategoryId($order = Criteria::ASC) Order by the category_id column
 * @method     ChildGradioUserActionCategoryQuery orderByFromUserId($order = Criteria::ASC) Order by the from_user_id column
 * @method     ChildGradioUserActionCategoryQuery orderByToUserId($order = Criteria::ASC) Order by the to_user_id column
 * @method     ChildGradioUserActionCategoryQuery orderByItemId($order = Criteria::ASC) Order by the item_id column
 * @method     ChildGradioUserActionCategoryQuery orderByItemIds($order = Criteria::ASC) Order by the item_ids column
 * @method     ChildGradioUserActionCategoryQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildGradioUserActionCategoryQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildGradioUserActionCategoryQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 *
 * @method     ChildGradioUserActionCategoryQuery groupById() Group by the id column
 * @method     ChildGradioUserActionCategoryQuery groupByActionId() Group by the action_id column
 * @method     ChildGradioUserActionCategoryQuery groupByCategoryId() Group by the category_id column
 * @method     ChildGradioUserActionCategoryQuery groupByFromUserId() Group by the from_user_id column
 * @method     ChildGradioUserActionCategoryQuery groupByToUserId() Group by the to_user_id column
 * @method     ChildGradioUserActionCategoryQuery groupByItemId() Group by the item_id column
 * @method     ChildGradioUserActionCategoryQuery groupByItemIds() Group by the item_ids column
 * @method     ChildGradioUserActionCategoryQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildGradioUserActionCategoryQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildGradioUserActionCategoryQuery groupByDeletedAt() Group by the deleted_at column
 *
 * @method     ChildGradioUserActionCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioUserActionCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioUserActionCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioUserActionCategoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioUserActionCategoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioUserActionCategoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioUserActionCategory findOne(ConnectionInterface $con = null) Return the first ChildGradioUserActionCategory matching the query
 * @method     ChildGradioUserActionCategory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioUserActionCategory matching the query, or a new ChildGradioUserActionCategory object populated from the query conditions when no match is found
 *
 * @method     ChildGradioUserActionCategory findOneById(int $id) Return the first ChildGradioUserActionCategory filtered by the id column
 * @method     ChildGradioUserActionCategory findOneByActionId(int $action_id) Return the first ChildGradioUserActionCategory filtered by the action_id column
 * @method     ChildGradioUserActionCategory findOneByCategoryId(int $category_id) Return the first ChildGradioUserActionCategory filtered by the category_id column
 * @method     ChildGradioUserActionCategory findOneByFromUserId(int $from_user_id) Return the first ChildGradioUserActionCategory filtered by the from_user_id column
 * @method     ChildGradioUserActionCategory findOneByToUserId(int $to_user_id) Return the first ChildGradioUserActionCategory filtered by the to_user_id column
 * @method     ChildGradioUserActionCategory findOneByItemId(int $item_id) Return the first ChildGradioUserActionCategory filtered by the item_id column
 * @method     ChildGradioUserActionCategory findOneByItemIds(string $item_ids) Return the first ChildGradioUserActionCategory filtered by the item_ids column
 * @method     ChildGradioUserActionCategory findOneByCreatedAt(string $created_at) Return the first ChildGradioUserActionCategory filtered by the created_at column
 * @method     ChildGradioUserActionCategory findOneByUpdatedAt(string $updated_at) Return the first ChildGradioUserActionCategory filtered by the updated_at column
 * @method     ChildGradioUserActionCategory findOneByDeletedAt(string $deleted_at) Return the first ChildGradioUserActionCategory filtered by the deleted_at column *

 * @method     ChildGradioUserActionCategory requirePk($key, ConnectionInterface $con = null) Return the ChildGradioUserActionCategory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOne(ConnectionInterface $con = null) Return the first ChildGradioUserActionCategory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioUserActionCategory requireOneById(int $id) Return the first ChildGradioUserActionCategory filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByActionId(int $action_id) Return the first ChildGradioUserActionCategory filtered by the action_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByCategoryId(int $category_id) Return the first ChildGradioUserActionCategory filtered by the category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByFromUserId(int $from_user_id) Return the first ChildGradioUserActionCategory filtered by the from_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByToUserId(int $to_user_id) Return the first ChildGradioUserActionCategory filtered by the to_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByItemId(int $item_id) Return the first ChildGradioUserActionCategory filtered by the item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByItemIds(string $item_ids) Return the first ChildGradioUserActionCategory filtered by the item_ids column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByCreatedAt(string $created_at) Return the first ChildGradioUserActionCategory filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByUpdatedAt(string $updated_at) Return the first ChildGradioUserActionCategory filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioUserActionCategory requireOneByDeletedAt(string $deleted_at) Return the first ChildGradioUserActionCategory filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioUserActionCategory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioUserActionCategory objects based on current ModelCriteria
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findById(int $id) Return ChildGradioUserActionCategory objects filtered by the id column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByActionId(int $action_id) Return ChildGradioUserActionCategory objects filtered by the action_id column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByCategoryId(int $category_id) Return ChildGradioUserActionCategory objects filtered by the category_id column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByFromUserId(int $from_user_id) Return ChildGradioUserActionCategory objects filtered by the from_user_id column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByToUserId(int $to_user_id) Return ChildGradioUserActionCategory objects filtered by the to_user_id column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByItemId(int $item_id) Return ChildGradioUserActionCategory objects filtered by the item_id column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByItemIds(string $item_ids) Return ChildGradioUserActionCategory objects filtered by the item_ids column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildGradioUserActionCategory objects filtered by the created_at column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildGradioUserActionCategory objects filtered by the updated_at column
 * @method     ChildGradioUserActionCategory[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildGradioUserActionCategory objects filtered by the deleted_at column
 * @method     ChildGradioUserActionCategory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioUserActionCategoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioUserActionCategoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioUserActionCategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioUserActionCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioUserActionCategoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioUserActionCategoryQuery) {
            return $criteria;
        }
        $query = new ChildGradioUserActionCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioUserActionCategory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioUserActionCategoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioUserActionCategoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioUserActionCategory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, action_id, category_id, from_user_id, to_user_id, item_id, item_ids, created_at, updated_at, deleted_at FROM gradio_user_action_category WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioUserActionCategory $obj */
            $obj = new ChildGradioUserActionCategory();
            $obj->hydrate($row);
            GradioUserActionCategoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioUserActionCategory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the action_id column
     *
     * Example usage:
     * <code>
     * $query->filterByActionId(1234); // WHERE action_id = 1234
     * $query->filterByActionId(array(12, 34)); // WHERE action_id IN (12, 34)
     * $query->filterByActionId(array('min' => 12)); // WHERE action_id > 12
     * </code>
     *
     * @param     mixed $actionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByActionId($actionId = null, $comparison = null)
    {
        if (is_array($actionId)) {
            $useMinMax = false;
            if (isset($actionId['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ACTION_ID, $actionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionId['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ACTION_ID, $actionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ACTION_ID, $actionId, $comparison);
    }

    /**
     * Filter the query on the category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE category_id = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE category_id IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE category_id > 12
     * </code>
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the from_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFromUserId(1234); // WHERE from_user_id = 1234
     * $query->filterByFromUserId(array(12, 34)); // WHERE from_user_id IN (12, 34)
     * $query->filterByFromUserId(array('min' => 12)); // WHERE from_user_id > 12
     * </code>
     *
     * @param     mixed $fromUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByFromUserId($fromUserId = null, $comparison = null)
    {
        if (is_array($fromUserId)) {
            $useMinMax = false;
            if (isset($fromUserId['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_FROM_USER_ID, $fromUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fromUserId['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_FROM_USER_ID, $fromUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_FROM_USER_ID, $fromUserId, $comparison);
    }

    /**
     * Filter the query on the to_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByToUserId(1234); // WHERE to_user_id = 1234
     * $query->filterByToUserId(array(12, 34)); // WHERE to_user_id IN (12, 34)
     * $query->filterByToUserId(array('min' => 12)); // WHERE to_user_id > 12
     * </code>
     *
     * @param     mixed $toUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByToUserId($toUserId = null, $comparison = null)
    {
        if (is_array($toUserId)) {
            $useMinMax = false;
            if (isset($toUserId['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_TO_USER_ID, $toUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toUserId['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_TO_USER_ID, $toUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_TO_USER_ID, $toUserId, $comparison);
    }

    /**
     * Filter the query on the item_id column
     *
     * Example usage:
     * <code>
     * $query->filterByItemId(1234); // WHERE item_id = 1234
     * $query->filterByItemId(array(12, 34)); // WHERE item_id IN (12, 34)
     * $query->filterByItemId(array('min' => 12)); // WHERE item_id > 12
     * </code>
     *
     * @param     mixed $itemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByItemId($itemId = null, $comparison = null)
    {
        if (is_array($itemId)) {
            $useMinMax = false;
            if (isset($itemId['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ITEM_ID, $itemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($itemId['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ITEM_ID, $itemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ITEM_ID, $itemId, $comparison);
    }

    /**
     * Filter the query on the item_ids column
     *
     * Example usage:
     * <code>
     * $query->filterByItemIds('fooValue');   // WHERE item_ids = 'fooValue'
     * $query->filterByItemIds('%fooValue%', Criteria::LIKE); // WHERE item_ids LIKE '%fooValue%'
     * </code>
     *
     * @param     string $itemIds The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByItemIds($itemIds = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($itemIds)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ITEM_IDS, $itemIds, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioUserActionCategory $gradioUserActionCategory Object to remove from the list of results
     *
     * @return $this|ChildGradioUserActionCategoryQuery The current query, for fluid interface
     */
    public function prune($gradioUserActionCategory = null)
    {
        if ($gradioUserActionCategory) {
            $this->addUsingAlias(GradioUserActionCategoryTableMap::COL_ID, $gradioUserActionCategory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_user_action_category table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserActionCategoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioUserActionCategoryTableMap::clearInstancePool();
            GradioUserActionCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioUserActionCategoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioUserActionCategoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioUserActionCategoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioUserActionCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioUserActionCategoryQuery
