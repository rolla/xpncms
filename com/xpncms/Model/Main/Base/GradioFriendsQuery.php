<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioFriends as ChildGradioFriends;
use Model\Main\GradioFriendsQuery as ChildGradioFriendsQuery;
use Model\Main\Map\GradioFriendsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_friends' table.
 *
 *
 *
 * @method     ChildGradioFriendsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioFriendsQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildGradioFriendsQuery orderByFriendId($order = Criteria::ASC) Order by the friend_id column
 * @method     ChildGradioFriendsQuery orderByAdded($order = Criteria::ASC) Order by the added column
 * @method     ChildGradioFriendsQuery orderByConfirmed($order = Criteria::ASC) Order by the confirmed column
 *
 * @method     ChildGradioFriendsQuery groupById() Group by the id column
 * @method     ChildGradioFriendsQuery groupByUserId() Group by the user_id column
 * @method     ChildGradioFriendsQuery groupByFriendId() Group by the friend_id column
 * @method     ChildGradioFriendsQuery groupByAdded() Group by the added column
 * @method     ChildGradioFriendsQuery groupByConfirmed() Group by the confirmed column
 *
 * @method     ChildGradioFriendsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioFriendsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioFriendsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioFriendsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioFriendsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioFriendsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioFriends findOne(ConnectionInterface $con = null) Return the first ChildGradioFriends matching the query
 * @method     ChildGradioFriends findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioFriends matching the query, or a new ChildGradioFriends object populated from the query conditions when no match is found
 *
 * @method     ChildGradioFriends findOneById(int $id) Return the first ChildGradioFriends filtered by the id column
 * @method     ChildGradioFriends findOneByUserId(int $user_id) Return the first ChildGradioFriends filtered by the user_id column
 * @method     ChildGradioFriends findOneByFriendId(int $friend_id) Return the first ChildGradioFriends filtered by the friend_id column
 * @method     ChildGradioFriends findOneByAdded(string $added) Return the first ChildGradioFriends filtered by the added column
 * @method     ChildGradioFriends findOneByConfirmed(string $confirmed) Return the first ChildGradioFriends filtered by the confirmed column *

 * @method     ChildGradioFriends requirePk($key, ConnectionInterface $con = null) Return the ChildGradioFriends by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioFriends requireOne(ConnectionInterface $con = null) Return the first ChildGradioFriends matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioFriends requireOneById(int $id) Return the first ChildGradioFriends filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioFriends requireOneByUserId(int $user_id) Return the first ChildGradioFriends filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioFriends requireOneByFriendId(int $friend_id) Return the first ChildGradioFriends filtered by the friend_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioFriends requireOneByAdded(string $added) Return the first ChildGradioFriends filtered by the added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioFriends requireOneByConfirmed(string $confirmed) Return the first ChildGradioFriends filtered by the confirmed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioFriends[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioFriends objects based on current ModelCriteria
 * @method     ChildGradioFriends[]|ObjectCollection findById(int $id) Return ChildGradioFriends objects filtered by the id column
 * @method     ChildGradioFriends[]|ObjectCollection findByUserId(int $user_id) Return ChildGradioFriends objects filtered by the user_id column
 * @method     ChildGradioFriends[]|ObjectCollection findByFriendId(int $friend_id) Return ChildGradioFriends objects filtered by the friend_id column
 * @method     ChildGradioFriends[]|ObjectCollection findByAdded(string $added) Return ChildGradioFriends objects filtered by the added column
 * @method     ChildGradioFriends[]|ObjectCollection findByConfirmed(string $confirmed) Return ChildGradioFriends objects filtered by the confirmed column
 * @method     ChildGradioFriends[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioFriendsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioFriendsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioFriends', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioFriendsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioFriendsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioFriendsQuery) {
            return $criteria;
        }
        $query = new ChildGradioFriendsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioFriends|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioFriendsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioFriendsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioFriends A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, friend_id, added, confirmed FROM gradio_friends WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioFriends $obj */
            $obj = new ChildGradioFriends();
            $obj->hydrate($row);
            GradioFriendsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioFriends|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioFriendsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioFriendsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioFriendsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioFriendsTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the friend_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFriendId(1234); // WHERE friend_id = 1234
     * $query->filterByFriendId(array(12, 34)); // WHERE friend_id IN (12, 34)
     * $query->filterByFriendId(array('min' => 12)); // WHERE friend_id > 12
     * </code>
     *
     * @param     mixed $friendId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterByFriendId($friendId = null, $comparison = null)
    {
        if (is_array($friendId)) {
            $useMinMax = false;
            if (isset($friendId['min'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_FRIEND_ID, $friendId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($friendId['max'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_FRIEND_ID, $friendId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioFriendsTableMap::COL_FRIEND_ID, $friendId, $comparison);
    }

    /**
     * Filter the query on the added column
     *
     * Example usage:
     * <code>
     * $query->filterByAdded('2011-03-14'); // WHERE added = '2011-03-14'
     * $query->filterByAdded('now'); // WHERE added = '2011-03-14'
     * $query->filterByAdded(array('max' => 'yesterday')); // WHERE added > '2011-03-13'
     * </code>
     *
     * @param     mixed $added The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterByAdded($added = null, $comparison = null)
    {
        if (is_array($added)) {
            $useMinMax = false;
            if (isset($added['min'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_ADDED, $added['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($added['max'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_ADDED, $added['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioFriendsTableMap::COL_ADDED, $added, $comparison);
    }

    /**
     * Filter the query on the confirmed column
     *
     * Example usage:
     * <code>
     * $query->filterByConfirmed('2011-03-14'); // WHERE confirmed = '2011-03-14'
     * $query->filterByConfirmed('now'); // WHERE confirmed = '2011-03-14'
     * $query->filterByConfirmed(array('max' => 'yesterday')); // WHERE confirmed > '2011-03-13'
     * </code>
     *
     * @param     mixed $confirmed The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function filterByConfirmed($confirmed = null, $comparison = null)
    {
        if (is_array($confirmed)) {
            $useMinMax = false;
            if (isset($confirmed['min'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_CONFIRMED, $confirmed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($confirmed['max'])) {
                $this->addUsingAlias(GradioFriendsTableMap::COL_CONFIRMED, $confirmed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioFriendsTableMap::COL_CONFIRMED, $confirmed, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioFriends $gradioFriends Object to remove from the list of results
     *
     * @return $this|ChildGradioFriendsQuery The current query, for fluid interface
     */
    public function prune($gradioFriends = null)
    {
        if ($gradioFriends) {
            $this->addUsingAlias(GradioFriendsTableMap::COL_ID, $gradioFriends->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_friends table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioFriendsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioFriendsTableMap::clearInstancePool();
            GradioFriendsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioFriendsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioFriendsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioFriendsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioFriendsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioFriendsQuery
