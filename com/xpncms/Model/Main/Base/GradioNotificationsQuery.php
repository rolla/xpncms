<?php

namespace Model\Main\Base;

use \Exception;
use \PDO;
use Model\Main\GradioNotifications as ChildGradioNotifications;
use Model\Main\GradioNotificationsQuery as ChildGradioNotificationsQuery;
use Model\Main\Map\GradioNotificationsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'gradio_notifications' table.
 *
 *
 *
 * @method     ChildGradioNotificationsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildGradioNotificationsQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildGradioNotificationsQuery orderByActionId($order = Criteria::ASC) Order by the action_id column
 * @method     ChildGradioNotificationsQuery orderByLevel($order = Criteria::ASC) Order by the level column
 * @method     ChildGradioNotificationsQuery orderByActionsCategoryId($order = Criteria::ASC) Order by the actions_category_id column
 * @method     ChildGradioNotificationsQuery orderByActionsItemId($order = Criteria::ASC) Order by the actions_item_id column
 * @method     ChildGradioNotificationsQuery orderByAdded($order = Criteria::ASC) Order by the added column
 * @method     ChildGradioNotificationsQuery orderByReaded($order = Criteria::ASC) Order by the readed column
 *
 * @method     ChildGradioNotificationsQuery groupById() Group by the id column
 * @method     ChildGradioNotificationsQuery groupByUserId() Group by the user_id column
 * @method     ChildGradioNotificationsQuery groupByActionId() Group by the action_id column
 * @method     ChildGradioNotificationsQuery groupByLevel() Group by the level column
 * @method     ChildGradioNotificationsQuery groupByActionsCategoryId() Group by the actions_category_id column
 * @method     ChildGradioNotificationsQuery groupByActionsItemId() Group by the actions_item_id column
 * @method     ChildGradioNotificationsQuery groupByAdded() Group by the added column
 * @method     ChildGradioNotificationsQuery groupByReaded() Group by the readed column
 *
 * @method     ChildGradioNotificationsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGradioNotificationsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGradioNotificationsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGradioNotificationsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGradioNotificationsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGradioNotificationsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGradioNotifications findOne(ConnectionInterface $con = null) Return the first ChildGradioNotifications matching the query
 * @method     ChildGradioNotifications findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGradioNotifications matching the query, or a new ChildGradioNotifications object populated from the query conditions when no match is found
 *
 * @method     ChildGradioNotifications findOneById(int $id) Return the first ChildGradioNotifications filtered by the id column
 * @method     ChildGradioNotifications findOneByUserId(int $user_id) Return the first ChildGradioNotifications filtered by the user_id column
 * @method     ChildGradioNotifications findOneByActionId(int $action_id) Return the first ChildGradioNotifications filtered by the action_id column
 * @method     ChildGradioNotifications findOneByLevel(int $level) Return the first ChildGradioNotifications filtered by the level column
 * @method     ChildGradioNotifications findOneByActionsCategoryId(int $actions_category_id) Return the first ChildGradioNotifications filtered by the actions_category_id column
 * @method     ChildGradioNotifications findOneByActionsItemId(int $actions_item_id) Return the first ChildGradioNotifications filtered by the actions_item_id column
 * @method     ChildGradioNotifications findOneByAdded(string $added) Return the first ChildGradioNotifications filtered by the added column
 * @method     ChildGradioNotifications findOneByReaded(string $readed) Return the first ChildGradioNotifications filtered by the readed column *

 * @method     ChildGradioNotifications requirePk($key, ConnectionInterface $con = null) Return the ChildGradioNotifications by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOne(ConnectionInterface $con = null) Return the first ChildGradioNotifications matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioNotifications requireOneById(int $id) Return the first ChildGradioNotifications filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByUserId(int $user_id) Return the first ChildGradioNotifications filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByActionId(int $action_id) Return the first ChildGradioNotifications filtered by the action_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByLevel(int $level) Return the first ChildGradioNotifications filtered by the level column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByActionsCategoryId(int $actions_category_id) Return the first ChildGradioNotifications filtered by the actions_category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByActionsItemId(int $actions_item_id) Return the first ChildGradioNotifications filtered by the actions_item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByAdded(string $added) Return the first ChildGradioNotifications filtered by the added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGradioNotifications requireOneByReaded(string $readed) Return the first ChildGradioNotifications filtered by the readed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGradioNotifications[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGradioNotifications objects based on current ModelCriteria
 * @method     ChildGradioNotifications[]|ObjectCollection findById(int $id) Return ChildGradioNotifications objects filtered by the id column
 * @method     ChildGradioNotifications[]|ObjectCollection findByUserId(int $user_id) Return ChildGradioNotifications objects filtered by the user_id column
 * @method     ChildGradioNotifications[]|ObjectCollection findByActionId(int $action_id) Return ChildGradioNotifications objects filtered by the action_id column
 * @method     ChildGradioNotifications[]|ObjectCollection findByLevel(int $level) Return ChildGradioNotifications objects filtered by the level column
 * @method     ChildGradioNotifications[]|ObjectCollection findByActionsCategoryId(int $actions_category_id) Return ChildGradioNotifications objects filtered by the actions_category_id column
 * @method     ChildGradioNotifications[]|ObjectCollection findByActionsItemId(int $actions_item_id) Return ChildGradioNotifications objects filtered by the actions_item_id column
 * @method     ChildGradioNotifications[]|ObjectCollection findByAdded(string $added) Return ChildGradioNotifications objects filtered by the added column
 * @method     ChildGradioNotifications[]|ObjectCollection findByReaded(string $readed) Return ChildGradioNotifications objects filtered by the readed column
 * @method     ChildGradioNotifications[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GradioNotificationsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Main\Base\GradioNotificationsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio_gradio', $modelName = '\\Model\\Main\\GradioNotifications', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGradioNotificationsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGradioNotificationsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGradioNotificationsQuery) {
            return $criteria;
        }
        $query = new ChildGradioNotificationsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGradioNotifications|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GradioNotificationsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GradioNotificationsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGradioNotifications A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, action_id, level, actions_category_id, actions_item_id, added, readed FROM gradio_notifications WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGradioNotifications $obj */
            $obj = new ChildGradioNotifications();
            $obj->hydrate($row);
            GradioNotificationsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGradioNotifications|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the action_id column
     *
     * Example usage:
     * <code>
     * $query->filterByActionId(1234); // WHERE action_id = 1234
     * $query->filterByActionId(array(12, 34)); // WHERE action_id IN (12, 34)
     * $query->filterByActionId(array('min' => 12)); // WHERE action_id > 12
     * </code>
     *
     * @param     mixed $actionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByActionId($actionId = null, $comparison = null)
    {
        if (is_array($actionId)) {
            $useMinMax = false;
            if (isset($actionId['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTION_ID, $actionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionId['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTION_ID, $actionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTION_ID, $actionId, $comparison);
    }

    /**
     * Filter the query on the level column
     *
     * Example usage:
     * <code>
     * $query->filterByLevel(1234); // WHERE level = 1234
     * $query->filterByLevel(array(12, 34)); // WHERE level IN (12, 34)
     * $query->filterByLevel(array('min' => 12)); // WHERE level > 12
     * </code>
     *
     * @param     mixed $level The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByLevel($level = null, $comparison = null)
    {
        if (is_array($level)) {
            $useMinMax = false;
            if (isset($level['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_LEVEL, $level['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($level['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_LEVEL, $level['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_LEVEL, $level, $comparison);
    }

    /**
     * Filter the query on the actions_category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByActionsCategoryId(1234); // WHERE actions_category_id = 1234
     * $query->filterByActionsCategoryId(array(12, 34)); // WHERE actions_category_id IN (12, 34)
     * $query->filterByActionsCategoryId(array('min' => 12)); // WHERE actions_category_id > 12
     * </code>
     *
     * @param     mixed $actionsCategoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByActionsCategoryId($actionsCategoryId = null, $comparison = null)
    {
        if (is_array($actionsCategoryId)) {
            $useMinMax = false;
            if (isset($actionsCategoryId['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTIONS_CATEGORY_ID, $actionsCategoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionsCategoryId['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTIONS_CATEGORY_ID, $actionsCategoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTIONS_CATEGORY_ID, $actionsCategoryId, $comparison);
    }

    /**
     * Filter the query on the actions_item_id column
     *
     * Example usage:
     * <code>
     * $query->filterByActionsItemId(1234); // WHERE actions_item_id = 1234
     * $query->filterByActionsItemId(array(12, 34)); // WHERE actions_item_id IN (12, 34)
     * $query->filterByActionsItemId(array('min' => 12)); // WHERE actions_item_id > 12
     * </code>
     *
     * @param     mixed $actionsItemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByActionsItemId($actionsItemId = null, $comparison = null)
    {
        if (is_array($actionsItemId)) {
            $useMinMax = false;
            if (isset($actionsItemId['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTIONS_ITEM_ID, $actionsItemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionsItemId['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTIONS_ITEM_ID, $actionsItemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ACTIONS_ITEM_ID, $actionsItemId, $comparison);
    }

    /**
     * Filter the query on the added column
     *
     * Example usage:
     * <code>
     * $query->filterByAdded('2011-03-14'); // WHERE added = '2011-03-14'
     * $query->filterByAdded('now'); // WHERE added = '2011-03-14'
     * $query->filterByAdded(array('max' => 'yesterday')); // WHERE added > '2011-03-13'
     * </code>
     *
     * @param     mixed $added The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByAdded($added = null, $comparison = null)
    {
        if (is_array($added)) {
            $useMinMax = false;
            if (isset($added['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ADDED, $added['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($added['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_ADDED, $added['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_ADDED, $added, $comparison);
    }

    /**
     * Filter the query on the readed column
     *
     * Example usage:
     * <code>
     * $query->filterByReaded('2011-03-14'); // WHERE readed = '2011-03-14'
     * $query->filterByReaded('now'); // WHERE readed = '2011-03-14'
     * $query->filterByReaded(array('max' => 'yesterday')); // WHERE readed > '2011-03-13'
     * </code>
     *
     * @param     mixed $readed The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function filterByReaded($readed = null, $comparison = null)
    {
        if (is_array($readed)) {
            $useMinMax = false;
            if (isset($readed['min'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_READED, $readed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($readed['max'])) {
                $this->addUsingAlias(GradioNotificationsTableMap::COL_READED, $readed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GradioNotificationsTableMap::COL_READED, $readed, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGradioNotifications $gradioNotifications Object to remove from the list of results
     *
     * @return $this|ChildGradioNotificationsQuery The current query, for fluid interface
     */
    public function prune($gradioNotifications = null)
    {
        if ($gradioNotifications) {
            $this->addUsingAlias(GradioNotificationsTableMap::COL_ID, $gradioNotifications->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the gradio_notifications table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioNotificationsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GradioNotificationsTableMap::clearInstancePool();
            GradioNotificationsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GradioNotificationsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GradioNotificationsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GradioNotificationsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GradioNotificationsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GradioNotificationsQuery
