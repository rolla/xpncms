<?php

namespace Model\Main;

use Model\Main\Base\GradioUserclassesQuery as BaseGradioUserclassesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'gradio_userclasses' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GradioUserclassesQuery extends BaseGradioUserclassesQuery
{

}
