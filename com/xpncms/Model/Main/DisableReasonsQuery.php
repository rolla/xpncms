<?php

namespace Model\Main;

use Model\Main\Base\DisableReasonsQuery as BaseDisableReasonsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'disable_reasons' table.
 *
 * Song disable reasons
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DisableReasonsQuery extends BaseDisableReasonsQuery
{

}
