<?php

namespace Model\Main;

use Model\Main\Base\GradioActionCategory as BaseGradioActionCategory;

/**
 * Skeleton subclass for representing a row from the 'gradio_action_category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GradioActionCategory extends BaseGradioActionCategory
{

}
