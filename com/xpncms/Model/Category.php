<?php namespace XpnCMS\Model;

use Core\Includes\Config;

class Category extends Model
{

    public function __construct()
    {
        //parent::__construct();
        //$this->index();
    }

    public function Index()
    {

    }

    /**
     * @return string
     */
    private function baseQueryV2(): string
    {
        global $db1;

        $query = 'select cat_id as id, cat_name as name, ';
        $query .= 'cat_content as body, cat_alias as slug, ';
        $query .= 'cat_added as created_at, cat_updated as updated_at, ';
        $query .= 'cat_imgs as image, cat_order as sortorder ';
        $query .= 'from ' . $db1->db_prefix . 'categories ';

        return $query;
    }

    public function getCategory($id)
    {
        global $db1;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories WHERE cat_id = :id LIMIT 0, 1');
        $db1->bind(':id', $id);
        $categories = $db1->resultset();
        if ($categories) {
            $res['categories'] = $categories;
        } else {
            $res['error'] = 'no category found!';
        }

        return $res;
    }

    public function getCategoryByAlias($alias)
    {
        global $db1;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories WHERE cat_alias = :alias LIMIT 0, 1');
        $db1->bind(':alias', $alias);
        $categories = $db1->resultset();
        if ($categories) {
            $res['categories'] = $categories;
        } else {
            $res['error'] = 'no category found!';
        }

        return $res;
    }

    /**
     * @param $alias
     * @return mixed
     */
    public function getCategoryByAliasV2($alias)
    {
        global $db1;

        $query = $this->baseQueryV2();
        $query .= 'where cat_alias = :alias limit 0, 1';
        $db1->query($query);
        $db1->bind(':alias', $alias);

        return $db1->single();
    }

    /**
     * @param $category
     * @return array
     * @throws \Exception
     */
    public function getCategoryMeta($category): array
    {
        $pageTitle = _('Category')  . ' : ' . $category['name'];
        $contentText = strip_tags(html_entity_decode($category['body']));
        $ogUrl = str_replace('https', 'http', Config::get('base.fbsiteurl')) . '/category/' . $category['slug']; // without https to work fb comments
        $ogimage = urldecode(Config::get('base.fbsiteurl'). '/' . Config::get('paths.images') . '/'. $category['image']);

        $meta = [
            'pageTitle' => $pageTitle,
            'content' => $contentText,
            'ogdescription' => $contentText,
            'ogurl' => $ogUrl,
            'ogimage' => $ogimage,
            'ogtitle' => $pageTitle,
            'ogtype' => 'article',
            'twittertitle' => $pageTitle,
            'twitterdescription' => $contentText,
            'twitterimage' => $ogimage,
        ];

        return $meta;
    }

    public function getAllCategories($start = 0, $limit = 100)
    {
        global $db1;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories ORDER BY cat_added DESC LIMIT ' . $start . ', ' . $limit . '');
        //$db1->bind(':id', $id);
        $categories = $db1->resultset();
        if ($categories) {
            $res['categories'] = $categories;
        } else {
            $res['error'] = 'no categories added!';
        }

        return $res;
    }

    /**
     * @param int $start
     * @param int $limit
     * @param bool $random
     * @return mixed
     */
    public function getAllCategoriesV2($start = 0, $limit = 100, $random = false)
    {
        global $db1;

        $query = $this->baseQueryV2();
        $query .= '';

        if (!$random) {
            $query .= 'order by created_at desc limit :start, :limit ';
            $db1->query($query);
            $db1->bind(':start', $start);
        }

        if ($random) {
            $query .= 'order by rand() limit :limit';
            $db1->query($query);
        }

        $db1->bind(':limit', $limit);

        return $db1->resultset();
    }

    public function getCategoryTreeForParentId($parent_id = 0, $prev_path = null)
    {
        global $db1;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories WHERE cat_parent = :parentID');
        $db1->bind(':parentID', $parent_id);
        $catTree = $db1->resultset();

        $categories = array();

        foreach ($catTree as $key => $val) {
            $category = array();
            $category['id'] = $val['cat_id'];
            //$category['samdb_db_path'] = $prev_path.'/'.$val['name'];
            $category['name'] = $val['cat_name'];
            $category['content'] = $val['cat_content'];
            $category['parentID'] = $val['cat_parent'];
            $category['alias'] = $val['cat_alias'];
            $category['img'] = $val['cat_imgs'];
            $category['sub_category'] = $this->getCategoryTreeForParentId($category['id']);

            $categories[$key] = $category;
        }

        return $categories;
    }
}
