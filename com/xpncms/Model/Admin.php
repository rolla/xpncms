<?php

namespace XpnCMS\Model;

use Core\Helpers\Seo;
use Core\Includes\Config;

class Admin extends Model {

    private $defContImg;
    private $defCatImg;

    function __construct()
    {
        parent::__construct();

        $this->defContImg = Config::get('base.url') . Config::get('paths.themes') . '/';
        $this->defContImg .= $this->getTheme() . Config::get('paths.themes') . '/';
        $this->defContImg .= 'img/default/' . Config::get('images.defCont');

        $this->defCatImg = Config::get('base.url') . Config::get('paths.themes') . '/';
        $this->defCatImg .= $this->getTheme() . Config::get('paths.themes') . '/';
        $this->defCatImg .= 'img/default/' . Config::get('images.defCat');
    }

    public function addContent($content_type, $data)
    {
        global $db1, $debug;
        $seo = new Seo();

        //$debug->Display($data, false, true);

        $cont_imgs = $this->defContImg;
        $cat_imgs = $this->defCatImg;

        if ($content_type == 'content')
        {
            if (isset($data['cont_imgs']) && !empty($data['cont_imgs']))
            {
                $cont_imgs = str_replace('\\', '/', $data['cont_imgs']); // Replacing backslashes with forward slashes
            }

            if (isset($data['cont_alias']) && !empty($data['cont_alias']))
            {
                $cont_alias = $data['cont_alias'];
            }
            else
            {
                $cont_alias = $seo->generateRandomString(5);
            }

            $db1->query('INSERT INTO ' . $db1->db_prefix . 'content (cont_name, cont_content, cont_tags, cont_alias, cont_added, cont_updated, cont_category, cont_imgs, cont_order) VALUES (:cont_name, :cont_content, :cont_tags, :cont_alias, :cont_added, :cont_updated, :cont_category, :cont_imgs, :cont_order)');
            $db1->bind(':cont_name', $data['cont_name']);
            $db1->bind(':cont_content', $data['cont_content']);
            $db1->bind(':cont_tags', $data['cont_tags']);
            $db1->bind(':cont_alias', $cont_alias);
            $datetime = date_create()->format('Y-m-d H:i:s');
            $db1->bind(':cont_added', $datetime);
            $db1->bind(':cont_updated', $datetime);
            $db1->bind(':cont_category', $data['cont_category']);
            $db1->bind(':cont_imgs', $cont_imgs);
            $db1->bind(':cont_order', $data['cont_order']);
            $add = $db1->execute();
            if ($add)
            {
                $result = ['ok' => $db1->lastInsertId()];
            }
            else
            {
                $result = ['error' => '<div class="alert alert-error atbildes-pazinojums">saturs netika pievienots!</div>'];
            }
        }
        elseif ($content_type == 'category')
        {
            if (isset($data['cat_imgs']) && !empty($data['cat_imgs']))
            {
                $cat_imgs = str_replace('\\', '/', $data['cat_imgs']); // Replacing backslashes with forward slashes
            }

            if (isset($data['cat_alias']) && !empty($data['cat_alias']))
            {
                $cat_alias = $data['cat_alias'];
            }
            else
            {
                $cat_alias = $seo->generateRandomString(5);
            }

            $db1->query('INSERT INTO ' . $db1->db_prefix . 'categories (cat_name, cat_content, cat_alias, cat_added, cat_updated, cat_class, cat_parent, cat_imgs, cat_order) VALUES (:cat_name, :cat_content, :cat_alias, :cat_added, :cat_updated, :cat_class, :cat_parent, :cat_imgs, :cat_order)');
            $db1->bind(':cat_name', $data['cat_name']);
            $db1->bind(':cat_content', $data['cat_content']);
            //$db1->bind(':cat_tags', $data['cat_tags']);
            $db1->bind(':cat_alias', $cat_alias);
            $datetime = date_create()->format('Y-m-d H:i:s');
            $db1->bind(':cat_added', $datetime);
            $db1->bind(':cat_updated', $datetime);
            $db1->bind(':cat_class', $data['cat_class']);
            $db1->bind(':cat_parent', $data['cat_parent']);
            $db1->bind(':cat_imgs', $cat_imgs);
            $db1->bind(':cat_order', $data['cat_order']);
            $add = $db1->execute();
            if ($add)
            {
                $result = ['ok' => $db1->lastInsertId()];
            }
            else
            {
                $result = ['error' => '<div class="alert alert-error atbildes-pazinojums">saturs netika pievienots!</div>'];
            }
        }

        //$debug->Display($res, false, true);
        return $result;
    }

    public function updateContent($data)
    {
        global $db1;
        $seo = new Seo();

        $cont_imgs = $this->defContImg;

        if (isset($data['cont_imgs']) && !empty($data['cont_imgs']))
        {
            $cont_imgs = str_replace('\\', '/', $data['cont_imgs']); // Replacing backslashes with forward slashes
        }

        $cont_alias = $seo->generateRandomString(5);
        if (isset($data['cont_alias']) && !empty($data['cont_alias']))
        {
            $cont_alias = $data['cont_alias'];
        }

        $db1->query('UPDATE ' . $db1->db_prefix . 'content SET cont_name = :cont_name, cont_content = :cont_content, cont_tags = :cont_tags, cont_alias = :cont_alias, cont_updated = :cont_updated, cont_category = :cont_category, cont_imgs = :cont_imgs WHERE cont_id = :cont_id');
        $db1->bind(':cont_name', $data['cont_name']);
        $db1->bind(':cont_content', $data['cont_content']);
        $db1->bind(':cont_tags', $data['cont_tags']);
        $db1->bind(':cont_alias', $data['cont_alias']);
        $db1->bind(':cont_updated', date('Y-m-d H:i:s'));
        $db1->bind(':cont_category', $data['cont_category']);
        $db1->bind(':cont_imgs', $cont_imgs);
        $db1->bind(':cont_id', $data['cont_id']);

        return $db1->execute();
    }

    public function updateCategory($data)
    {
        global $db1;
        $seo = new Seo();

        $cat_imgs = $this->defCatImg;

        if (isset($data['cat_imgs']) && !empty($data['cat_imgs']))
        {
            $cat_imgs = str_replace('\\', '/', $data['cat_imgs']); // Replacing backslashes with forward slashes
        }

        $cat_alias = $seo->generateRandomString(5);
        if (isset($data['cat_alias']) && !empty($data['cat_alias']))
        {
            $cat_alias = $data['cat_alias'];
        }

        $db1->query('UPDATE ' . $db1->db_prefix . 'categories SET cat_name = :cat_name, cat_content = :cat_content, cat_alias = :cat_alias, cat_updated = :cat_updated, cat_class = :cat_class, cat_parent = :cat_parent, cat_imgs = :cat_imgs, cat_order = :cat_order WHERE cat_id = :cat_id');

        $db1->bind(':cat_name', $data['cat_name']);
        $db1->bind(':cat_content', $data['cat_content']);
        $db1->bind(':cat_alias', $data['cat_alias']);
        $db1->bind(':cat_updated', date('Y-m-d H:i:s'));
        $db1->bind(':cat_class', $data['cat_class']);
        $db1->bind(':cat_parent', $data['cat_parent']);
        $db1->bind(':cat_imgs', $cat_imgs);
        $db1->bind(':cat_order', $data['cat_order']);
        $db1->bind(':cat_id', $data['cat_id']);
        $save = $db1->execute();

        if ($save)
        {
            $result = ['ok' => $db1->lastInsertId()];
        }
        else
        {
            $result = ['error' => 'category not added'];
        }

        return $result;
    }

    public function deleteContent($content_type, $id)
    {
        global $db1, $debug;
        if ($content_type == 'content')
        {
            $db1->query('DELETE FROM ' . $db1->db_prefix . 'content WHERE cont_id = :cont_id');
            $db1->bind(':cont_id', $id);
            $del = $db1->execute();
            if ($del)
            {
                $result = ['ok' => $id];
            }
            else
            {
                $result = ['error' => 'error deleting content with id: ' . $id . ''];
            }
        }
        elseif ($content_type == 'category')
        {
            $db1->query('DELETE FROM ' . $db1->db_prefix . 'categories WHERE cat_id = :cat_id');
            $db1->bind(':cat_id', $id);
            $del = $db1->execute();
            if ($del)
            {
                $result = ['ok' => $id];
            }
            else
            {
                $result = ['error' => 'error deleting category with id: ' . $id . ''];
            }
        }
        //$debug->Display($res, false, true);
        return $result;
    }

    public function getAllContent($start, $limit)
    {
        global $db1, $debug;

        $db1->query('SELECT cont.cont_id, cont.cont_name, cont.cont_content, cont.cont_alias, cont.cont_added, cont.cont_updated, cont.cont_class, cat.cat_name as cont_category, cont.cont_imgs, cont.cont_order FROM ' . $db1->db_prefix . 'content cont LEFT JOIN ' . $db1->db_prefix . 'categories cat
ON cont.cont_category = cat.cat_id ORDER BY cont.cont_added DESC, cont.cont_updated DESC, cont.cont_order DESC LIMIT :start, :limit');
        $db1->bind(':start', intval($start));
        $db1->bind(':limit', intval($limit));

        //$debug->Display($db1, false, true);

        $content = $db1->resultset();

        if ($content)
        {
            $res['content'] = $content;
        }
        else
        {
            $res['error'] = 'no content added!';
        }

        //$debug->Display($res, false, true);
        return $res;
    }

    public function getContent($id)
    {
        global $db1, $debug;

        $db1->query('SELECT cont.cont_id, cont.cont_name, cont.cont_content, cont.cont_tags, cont.cont_alias, cont.cont_added, cont.cont_updated, cont.cont_class, cat.cat_name as cont_category, cont.cont_imgs, cont.cont_order FROM ' . $db1->db_prefix . 'content cont LEFT JOIN ' . $db1->db_prefix . 'categories cat
ON cont.cont_category = cat.cat_id WHERE cont.cont_id = :id LIMIT 0, 1');
        $db1->bind(':id', $id);
        $content = $db1->single();
        if ($content)
        {
            $res['content'] = $content;
        }
        else
        {
            $res['error'] = 'no content added!';
        }

        //$debug->Display($res, false, true);
        return $res;
    }

    public function getContentByAlias($alias)
    {
        global $db1, $debug;

        $db1->query('SELECT cont.cont_id, cont.cont_name, cont.cont_content, cont.cont_tags, cont.cont_alias, cont.cont_added, cont.cont_updated, cont.cont_class, cat.cat_name as cont_category, cat.cat_alias, cont.cont_imgs, cont.cont_order FROM ' . $db1->db_prefix . 'content cont LEFT JOIN ' . $db1->db_prefix . 'categories cat
ON cont.cont_category = cat.cat_id WHERE cont.cont_alias = :alias LIMIT 0, 1');
        $db1->bind(':alias', $alias);
        $content = $db1->single();
        if ($content)
        {
            $res['content'] = $content;
        }
        else
        {
            $res['error'] = 'no content added!';
        }

        //$debug->Display($res, false, true);
        return $res;
    }

    public function getLatestContent()
    {
        global $db1, $debug;

        $db1->query('SELECT cont.cont_id, cont.cont_name, cont.cont_content, cont.cont_alias, cont.cont_added, cont.cont_updated, cont.cont_class, cat.cat_name as cont_category, cont.cont_imgs, cont.cont_order, cat.cat_alias FROM ' . $db1->db_prefix . 'content cont LEFT JOIN ' . $db1->db_prefix . 'categories cat
ON cont.cont_category = cat.cat_id ORDER BY cont.cont_added DESC, cont.cont_updated DESC, cont.cont_order DESC LIMIT 0, 10');
        $content = $db1->resultset();
        if ($content)
        {
            $res['content'] = $content;
        }
        else
        {
            $res['error'] = 'no content added!';
        }

        //$debug->Display($res, false, true);
        return $res;
    }

    public function getCategory($id)
    {
        global $db1, $debug;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories WHERE cat_id = :id LIMIT 0, 1');
        $db1->bind(':id', $id);
        $category = $db1->resultset();
        if ($category)
        {
            $res['category'] = $category;
        }
        else
        {
            $res['error'] = 'no category added!';
        }
        //$debug->Display($res, false, true);
        return $res;
    }

    public function getCategoryByAlias($alias)
    {
        global $db1, $debug;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories WHERE cat_alias = :alias LIMIT 0, 1');
        $db1->bind(':alias', $alias);
        $category = $db1->resultset();
        if ($category)
        {
            $res['category'] = $category;
        }
        else
        {
            $res['error'] = 'no category found!';
        }
        //$debug->Display($res, false, true);
        return $res;
    }

    public function getAllCategories($start = 0, $limit = 100)
    {
        global $db1, $debug;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories LIMIT ' . $start . ', ' . $limit . '');
        //$db1->bind(':id', $id);
        $categories = $db1->resultset();
        if ($categories)
        {
            $res['categories'] = $categories;
        }
        else
        {
            $res['error'] = 'no categories added!';
        }

        //$debug->Display($res, false, true);
        return $res;
    }

    public function getCategoryTreeForParentId($parent_id = 0, $prev_path = null)
    {
        global $db1;

        //$this->_process_dir = getcwd();

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'categories WHERE cat_parent = :parentID');
        $db1->bind(':parentID', $parent_id);
        $catTree = $db1->resultset();

        //$this->Debug($catTree);

        $categories = [];
        //$this->db->from('categories');
        //$this->db->where('parent_id', $parent_id);
        //$result = $this->db->get()->result();
        //$this->_getCategoryTreeForParentId_total = count($catTree);
        //echo '<pre>';
        //print_r($this);
        //exit();

        foreach ($catTree as $key => $val)
        {

            // Calculate the percentation
            //$this->_getCategoryTreeForParentId_progress = intval($this->_getCategoryTreeForParentId_i/$this->_getCategoryTreeForParentId_total * 100);
            //echo '<pre>';
            //print_r($key);
            //echo '</pre>';

            $category = [];
            $category['id'] = $val['cat_id'];
            //$category['samdb_db_path'] = $prev_path.'/'.$val['name'];
            $category['name'] = $val['cat_name'];
            $category['parentID'] = $val['cat_parent'];
            $category['slug'] = $val['cat_alias'];
            $category['sub_category'] = $this->getCategoryTreeForParentId($category['id']);

            //$arr = explode("/", $category['path']);
            //$what_you_want = $arr[count($arr)-2];
            //$category['short_name'] = $arr[2];
            //$categories[$val['cat_id']] = $category;
            $categories[$key] = $category;

            //$tmp_path = $this->_process_dir.$category['samdb_db_path'].DIRECTORY_SEPARATOR."all-".$category['name'].DIRECTORY_SEPARATOR;
            //$tmp_path2 = $this->_process_dir.$category['samdb_db_path'].DIRECTORY_SEPARATOR;
            //echo $tmp_path."<br /> ";
            //echo $tmp_path2."<br /> ";
            //$this->_getCategoryTreeForParentId_i++;

            /*
              // This is for the buffer achieve the minimum size in order to flush data
              echo str_repeat(' ',1024*64);

              // Send output to browser immediately
              flush();
              ob_flush();

              // Sleep one second so we can see the delay
              usleep(500);

              // Javascript for updating the progress bar and information
              echo '<script language="javascript">

              //$("#load-db-progress .progress-bar").animate({width: "'.$this->_getCategoryTreeForParentId_progress.'%"});
              //$("#load-db-progress .progress-bar .sr-only").html("'.$this->_getCategoryTreeForParentId_i.' DB Genres processed...");

              document.getElementById("load-db-progress").innerHTML="<div style=\"width:'.$this->_getCategoryTreeForParentId_progress.'%; background-color:#ddd;\">&nbsp;</div>";
              document.getElementById("load-db-information").innerHTML="'.$this->_getCategoryTreeForParentId_i.' DB Genres processed...";
              </script>';
             */
        }

        //$result = $categories;

        /*
          $result['_main_dir_created'] = $this->_main_dir_created;
          $result['_err_main_dir_created'] = $this->_err_main_dir_created;
          $result['_all_dir_created'] = $this->_all_dir_created;
          $result['_err_all_dir_created'] = $this->_err_all_dir_created;
          $result['_create_date_copied'] = $this->_create_date_copied;
          $result['_err_create_date_copied'] = $this->_err_create_date_copied;
          $result['_create_date_moved'] = $this->_create_date_moved;
          $result['_err_create_date_moved'] = $this->_err_create_date_moved;
         */

        //return $result;
        //$this->_getCategoryTreeForParentId_progress = $percent;


        /*
          if($this->_getCategoryTreeForParentId_progress == 99){
          // Tell user that the process is completed
          echo '
          <script language="javascript">
          document.getElementById("load-db-progress").innerHTML="<div style=\"width:100%;background-color:green;\">&nbsp;</div>";
          document.getElementById("load-db-information").innerHTML="'.$this->_getCategoryTreeForParentId_i.' DB Genres loaded!";
          </script>';
          }
         */

        return $categories;
    }

}
