<?php

namespace XpnCMS\Model;

use Core\Includes\Config;
use DateTime;

class Online extends Model
{
    public function __construct()
    {
    }

    public function getOnlineUsers()
    {
        global $db1;

        $db1->query('select s.user_id, max(s.lastvisit) as lastvisit, s.status, u.latest_provider, u.first_name, u.last_name, u.display_name, u.description, u.photo_url from ' . $db1->db_prefix . 'sessions s
		join (
            select id, latest_provider, first_name, last_name, display_name, description, photo_url from ' . $db1->db_prefix . 'users
        ) u
        on s.user_id = u.id
        where s.user_id is not null
        group by s.user_id, s.status
		order by lastvisit desc
		limit 0, 50
		');

        $users = $db1->resultset();

        $onlineUsers = 0;
        if (count($users)) {
            $curDate = new DateTime(date('Y-m-d H:i:s'));
            $curDate = $curDate->modify('- ' . Config::get('prefs.onlinefreq')); // - 1 hour
            foreach ($users as $key => $user) {
                $users[$key]['display_name'] = $user['display_name'] ? $user['display_name'] : $user['username'];
                if ($user['lastvisit'] != null) {
                    $lastvisit = new DateTime($user['lastvisit']);
                    if ($lastvisit >= $curDate && $users[$key]['status'] == 1) {
                        $users[$key]['online'] = true;
                        ++$onlineUsers;
                    }
                }

                $image = $user['photo_url'];
                if (!preg_match('~^(?:f|ht)tps?://~i', $user['photo_url'])) {
                    $filesPath = Config::get('base.url') .
                        Config::get('paths.files') . '/' . 'user_' . $user['user_id'] . '/';
                    $image = $filesPath . $user['photo_url'];
                }
                $users[$key]['image'] = $image;
            }

            return ['items' => $users, 'online' => $onlineUsers];
        }

        return false;
    }

    public function getOnlineGuests()
    {
        global $db1;

        $db1->query('select max(s.lastvisit) as lastvisit, s.vuser_id, s.ip, s.status from ' . $db1->db_prefix . 'sessions s
                where s.user_id is null
                group by s.vuser_id, s.status, s.ip
		order by lastvisit desc
		limit 0, 50
		');

        $users = $db1->resultset();

        $guestImages = [
            'guest-1.jpg',
            'guest-2.png',
            'guest-3.png',
            'guest-4.jpg',
            'guest-5.png',
            'guest-6.jpg'
        ];

        $onlineUsers = 0;
        if (count($users)) {
            $curDate = new DateTime(date('Y-m-d H:i:s'));
            $curDate = $curDate->modify('- ' . Config::get('prefs.onlinefreq')); // - 1 hour
            foreach ($users as $key => $user) {
                if ($user['lastvisit'] != null) {
                    $lastvisit = new DateTime($user['lastvisit']);
                    if ($lastvisit >= $curDate && $users[$key]['status'] == 1) {
                        $users[$key]['online'] = true;
                        ++$onlineUsers;
                    }
                }

                $image = Config::get('base.url') . Config::get('paths.images') . '/' . 'users' . '/' . $guestImages[array_rand($guestImages)];
                $users[$key]['image'] = $image;
            }

            return ['items' => $users, 'online' => $onlineUsers];
        }

        return false;
    }
}
