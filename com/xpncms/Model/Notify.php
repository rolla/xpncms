<?php

namespace XpnCMS\Model;

use StringTemplate\Engine;

class Notify extends Model {

    public function __construct()
    {

    }

    public function getCode($code = null, $var = [''])
    {
        $engine = new Engine(':', '');

        switch ($code)
        {
            // core errors
            case 10:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Need to login!'),
                    'salert' => 'need to login'
                ];
                break;

            case 11:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('You dont have permissions to access this page!'),
                    'salert' => 'need to be admin'
                ];
                break;

            case 12:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('You dont have permissions to access this page!'),
                    'salert' => 'need to be admin'
                ];
                break;

            case 13:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('At least one social network must be connected!'),
                    'salert' => 'at least one need to be connected'
                ];
                break;

            case 14:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Only logged in user can vote about tune!'),
                    'salert' => 'only logged users can vote!'
                ];
                break;

            case 15:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Error adding content!'),
                    'salert' => 'error adding content'
                ];
                break;

            case 16:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Only logged in user can see they tune votes!'),
                    'salert' => 'only logged users can see likes dislikes!'
                ];
                break;

            case 17:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Error uploading file :errorMsg'),
                ];
                break;

            // core success
            case 201:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Success'),
                    'text' => _('Rating added'),
                    'salert' => 'rating added'
                ];
                break;

            case 202:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("Content :name added with ID: :id"),
                ];
                break;

            case 203:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Success'),
                    'text' => _('Category added with ID:'),
                    'salert' => 'category added with id:'
                ];
                break;

            case 204:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Success'),
                    'text' => _('Successfully deleted') . ' ' . $var[0] . ' ' . _('Files from smarty cache'),
                    'salert' => 'smarty cache cleared'
                ];
                break;

            case 205:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Success'),
                    'text' => _('Successfully deleted') . ' ' . $var[0] . ' ' . _('Files from phpThumb cache'),
                    'salert' => 'phpthumb cache cleared'
                ];
                break;

            case 206:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Success'),
                    'text' => _('Successfully deleted') . ' ' . $var[0] . ' ' . _('Files from sacy cache'),
                    'salert' => 'sacy cache cleared'
                ];
                break;

            case 207:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Tune rated'),
                    'text' => _(":tuneTitle added to your likes"),
                    'salert' => 'tune liked',
                ];
                break;

            case 208:
                $res = [
                    'success' => $code,
                    'type' => 'success',
                    'title' => _('Tune rated'),
                    'text' => _(":tuneTitle added to your dislikes"),
                    'salert' => 'tune disliked',
                ];
                break;

            // core info
            case 301:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _('Rating updated'),
                    'salert' => 'rating updated'
                ];
                break;

            case 302:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("Content :name updated with ID: :id"),
                ];
                break;

            case 303:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Tune updated'),
                    'text' => _(":tuneTitle with following changes :changes"),
                ];
                break;

            case 304:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("@:username updated tune information - :tuneTitle"),
                ];
                break;

            case 305:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("@:username liked tune - :tuneTitle"),
                ];
                break;

            case 306:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("@:username disliked tune - :tuneTitle"),
                ];
                break;

            case 307:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("@:username again rated - :tuneTitle"),
                ];
                break;

            case 308:
                $res = [
                    'info' => $code,
                    'type' => 'info',
                    'title' => _('Info'),
                    'text' => _("Hello @:username seems u have new profile picture, do you want to update it also here?"),
                ];
                break;

            // core notice
            case 401:
                $res = [
                    'notice' => $code,
                    'type' => 'notice',
                    'title' => _('Notice'),
                    'text' => _('Same rating'),
                    'salert' => 'same rating'
                ];
                break;

            case 402:
                $res = [
                    'notice' => $code,
                    'type' => 'notice',
                    'title' => _('Notice'),
                    'text' => _('Section') . ' ' . $var[0] . ' ' . _('currently updating, check back later'),
                    'salert' => 'this section currently is not available'
                ];
                break;

            // errors
            case 101:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('No data'),
                    'salert' => 'no data from db'
                ];
                break;

            case 102:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Nothing found'),
                    'salert' => 'no search results'
                ];
                break;

            case 103:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Category doesnt exist'),
                    'salert' => 'category not exist'
                ];
                break;

            case 104:
                $res = [
                    'info' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _("Cannot update :name with ID: :id"),
                ];
                break;

            default:
                $res = [
                    'error' => $code,
                    'type' => 'error',
                    'title' => _('Error'),
                    'text' => _('Category doesnt exist'),
                    'salert' => 'no error short explain'
                ];
        }

        $res['text'] = $engine->render($res['text'], $var);

        return $res;
    }
}
