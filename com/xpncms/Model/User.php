<?php

namespace XpnCMS\Model;

use Core\Includes\Action;
use Core\Includes\Config;
use PhpRbac\Rbac;
use stdClass;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class User extends Model
{
    public $userData = null; // user data array

    private $tillRefresh;

    private $user;

    const HIDDEN_FIELDS = [
        'address',
        'birthday',
        'admin',
        'adult',
        'age',
        'class',
        'dr_created',
        'dr_deleted',
        'email',
        'email_verified',
        'first_name',
        'img',
        'imgl',
        'imgm',
        'imgs',
        'last_name',
        'password',
        'perms',
        'phone',
        'prefs',
        'sess',
        'type',
    ];

    private function hideFields()
    {
        foreach ($this->user as $field => $value) {
            if (in_array($field, self::HIDDEN_FIELDS)) {
                unset($this->user[$field]);
            }
        }
    }

    /**
     *
     * @return stdClass
     */
    public static function auth(): ?stdClass
    {
        $session = new Session();
        $user = $session->get('user');
        $session->save();

        return $user ? (object)$user : null;
    }

    /**
     *
     * @return int
     */
    public static function authId()
    {
        $session = new Session();
        $currrentUserId = $session->get('userId');
        $session->save();

        return $currrentUserId ? $currrentUserId : 0;
    }

    public function checkUsr()
    {
        global $db1, $debug;

        if ($this->authId()) {
            $db1->query('SELECT * FROM ' . $db1->db_prefix . 'users WHERE id=:id');
            $db1->bind(':id', $this->authId());
            $chkusr = $db1->single();

            if ($chkusr != null) {
                $this->userData = $chkusr;

                return true;
            }
        }

        return false;
    }

    public function checkVUsr()
    {
        global $db1;
        $request = Request::createFromGlobals();
        $vuserId = $request->cookies->get('vuserId');

        if ($vuserId) {
            $db1->query('select vuser_id from ' . $db1->db_prefix . 'sessions where vuser_id=:vuserId');
            $db1->bind(':vuserId', $vuserId);
            return $db1->single();
        }

        return false;
    }

    public function checkClass($user_classes)
    {
        global $db1, $debug;

        //$ids     = array(1, 2, 3, 7, 8, 9);

        //$inQuery = implode(',', array_fill(0, count($user_classes), '?'));

        //$debug->Display($user_classes, 0, 1);

        //$classes = implode(",", (array)$user_classes);

        if ($this->authId()) {
            //$bindString = helper::bindParamArray("id", $_GET['ids'], $bindArray);
            //$userConditions .= " AND users.id IN($bindString)";

            $bindString = implode(',', (array) $user_classes);

            //$bindString = $db1->bindParamArray("class_id_", $user_classes, $bindArray);

            //$debug->Display($bindArray, 0, 1);

            $db1->query(
                'SELECT id, class FROM ' . $db1->db_prefix . 'users WHERE id=:id '
                . 'AND class IN (' . $bindString . ') LIMIT 0, 1'
            );
            $db1->bind(':id', $this->authId()); //

            /*
            foreach ($bindArray as $key => $value) {
                $db1->bind($key, $value);
            }
            */

            //$db1->bind(':start', 0);
            //$db1->bind(':limit', 1);

            //$db1->bind(':class', $class); // admin
            //$db1->bind(':class', $this->userclass_list); // admin
            //$db1->bind(':classes', $classes); // admin
            // bindvalue is 1-indexed, so $k+1

            $chkusr = $db1->single();

            //$debug->Display($db1, 0, 1);

            if ($chkusr != null) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getUserPic($args = [])
    {
        $args = ['mode' => 'lightbox', 'width' => 250, 'height' => 250];

        return false;
    }

    # $password, $first_name, $last_name, $display_name, $description, $email,
    # $email_verified, $phone, $gender, $language, $birth, $age, $address,
    # $country, $region, $city, $zip, $website_url, $profile_url, $photo_url

    // public function create($password, $first_name, $last_name, $display_name,
    // $description, $email, $email_verified, $phone, $gender, $language,
    // $birth, $age, $address, $country, $region, $city, $zip, $website_url,
    // $profile_url, $photo_url){

    public function create($arguments = [])
    {
        global $db1;
        /*
        $args = func_get_args();

        echo '<pre>';
        //print_r($args);
        var_dump($args);
        echo '</pre>';
        */

        foreach ($arguments as $key => $arg) {
            if (!empty($arg)) {
                $db_cols[$key] = $arg;
            }
        }

        /*
        echo '<pre>';
        print_r($db_cols);
        //var_dump($db_cols);
        echo '</pre>';
        */

        //exit();


        # erros atrast, problēma bija, ka tukšu uzskatīja kā null,
        # bet datubāzē nebija atļaujas tādu ievietot, ieliku PDO::NULL_TO_STRING, opcijās un NEKAS neaizgāja!
        # uztaisīju, lai izvāc tukšās vērtības un liek tikai kurām kaut kas ir!
        # šito vajag izfiškot, jo ļoti daudz laiku prasīja!!!

        $keys = ''.implode(', ', array_keys($db_cols));
        $values = str_repeat('?, ', count($db_cols) - 1).'?';

        $i = 1;
        $db1->query('INSERT INTO '.$db1->db_prefix.'users ( '.$keys.' ) VALUES ( '.$values.' )');

        foreach ($db_cols as $value) {
            $db1->bind($i++, $value);
        }

        // errors ar phone nez kāpēc - jo nebija tips int!!!
        // $db1->query('INSERT INTO '.$db1->db_prefix.'users (
        // password, first_name, last_name, display_name, description, email,
        // email_verified, phone, gender, language, birthday, age, address,
        // country, region, city, zip, website_url, profile_url, photo_url, ip,
        // added ) VALUES (:password, :first_name, :last_name, :display_name,
        // :description, :email, :email_verified, :phone, :gender, :language,
        // :birthday, :age, :address, :country :region, :city, :zip, :website_url,
        // :profile_url, :photo_url, :ip, :added )');

        // $db1->query('INSERT INTO '.$db1->db_prefix.'users (
        // password, first_name, last_name, display_name, description, email,
        // email_verified, gender, birthday, age, region, website_url,
        // profile_url, photo_url, ip, added, phone ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )');

        // $db1->query('INSERT INTO '.$db1->db_prefix.'users (
        // password, first_name, last_name, display_name, description, email,
        // email_verified, gender, age) VALUES (:password, :first_name,
        // :last_name, :display_name, :description, :email, :email_verified, :gender, :age)');

        /*
        if(!empty($birth)){
            $birthday = new DateTime($birth);
        }else{
            $birthday = new DateTime('1900-01-01');
        }
        */

        //(empty($address['street2']) ? 'Yes <br />' : 'No <br />');
        /*
        $db1->bind(':password', (!empty($password) ? $password : ''));
        $db1->bind(':first_name', (!empty($first_name) ? $first_name : ''));
        $db1->bind(':last_name', (!empty($last_name) ? $last_name : ''));
        $db1->bind(':display_name', (!empty($display_name) ? $display_name : ''));
        $db1->bind(':description', (!empty($description) ? $description : ''));
        $db1->bind(':email', (!empty($email) ? $email : ''));
        $db1->bind(':email_verified', (!empty($email_verified) ? $email_verified : ''));
        $db1->bind(':phone', (!empty($phone) ? $phone : ''));
        $db1->bind(':gender', (!empty($gender) ? $gender : ''));
        $db1->bind(':language', (!empty($language) ? $language : ''));
        $db1->bind(':birthday', $birthday->format('Y-m-d'));
        $db1->bind(':age', (!empty($age) ? $age : ''));
        $db1->bind(':address', (!empty($address)? $address : ''));
        $db1->bind(':country', (!empty($country) ? $country : ''));
        $db1->bind(':region', (!empty($region) ? $region : ''));
        $db1->bind(':city', (!empty($city) ? $city : ''));
        $db1->bind(':zip', (!empty($zip) ? $zip : ''));
        $db1->bind(':website_url', (!empty($website_url) ? $website_url : ''));
        $db1->bind(':profile_url', (!empty($profile_url) ? $profile_url : ''));
        $db1->bind(':photo_url', (!empty($photo_url) ? $photo_url : ''));
        $db1->bind(':ip', $_SERVER['REMOTE_ADDR']);
        $db1->bind(':added', date_create()->format('Y-m-d H:i:s'));
        */

        /*
        $db1->bind(':password', '');
        $db1->bind(':first_name', '');
        $db1->bind(':last_name', '');
        $db1->bind(':display_name', '');
        $db1->bind(':description', '');
        $db1->bind(':email', '');
        $db1->bind(':email_verified', '');
        $db1->bind(':phone', '');
        $db1->bind(':gender', '');
        $db1->bind(':language', '');
        $db1->bind(':birthday', '');
        $db1->bind(':age', '');
        $db1->bind(':address', '');
        $db1->bind(':country', '');
        $db1->bind(':region', '');
        $db1->bind(':city', '');
        $db1->bind(':zip', '');
        $db1->bind(':website_url', '');
        $db1->bind(':profile_url', '');
        $db1->bind(':photo_url', '');
        $db1->bind(':ip', '');
        $db1->bind(':added', '');
        */

        /*
        $db1->bind(1, $password);
        $db1->bind(2, $first_name);
        $db1->bind(3, $last_name);
        $db1->bind(4, $display_name);
        $db1->bind(5, $description);
        $db1->bind(6, $email);
        $db1->bind(7, $email_verified);
        $db1->bind(17, (int)$phone);
        $db1->bind(8, $gender);
        //$db1->bind(9, $language);
        $db1->bind(9, $birthday->format('Y-m-d'));
        $db1->bind(10, (int)$age);
        //$db1->bind(11, $address); // errors
        //$db1->bind(11, $country); // errors
        $db1->bind(11, $region);
        //$db1->bind(12, $city);
        //$db1->bind(12, $zip);
        $db1->bind(12, $website_url);
        $db1->bind(13, $profile_url);
        $db1->bind(14, $photo_url);
        $db1->bind(15, $_SERVER['REMOTE_ADDR']);
        $db1->bind(16, date_create()->format('Y-m-d H:i:s'));
        */

        /*
        echo '<pre>';
        //print_r($db1->errorInfo());
        var_export($db1->errorInfo());
        echo '</pre>';
        */

        /*
        echo '<pre>';
        print_r($db1);
        echo '</pre>';

        echo '<pre>';
        print_r($db1->debugDumpParams());
        echo '</pre>';
        */

        /*
        echo '<pre>';
        //print_r($db1->errorInfo());
        var_export($db1);
        echo '</pre>';
        */

        $exec = $db1->execute();

        /*
        echo '<pre>';
        //print_r($db1->errors());
        echo '</pre>';


        echo '<br /><br /><br /><br />';

        if($exec){
            echo '<h1>Beidzot iekšā!</h1>';
        }else{
            echo '<h1>Nepievienoju!</h1>';
        }
        */

        //exit();

        //die('te esam?');

        $result = $db1->lastInsertId();

        return $result;

        /*
        $sql = "INSERT INTO gradio_users ( email, password, name, surname, added ) VALUES ( '$email', '$password', '$first_name', '$last_name', NOW() ) ";
        mysql_query_excute($sql);
        return mysql_insert_id();
        */
    }

    public function updateUser($arguments, $id, $providerUid = null)
    {
        global $db1, $debug;

        foreach ($arguments as $key => $val) {
            if (!empty($val)) {
                $db_cols[$key] = $val;
            }
        }

        $set;
        $cnt = count($db_cols);
        $i = 0;
        foreach ($db_cols as $key => $val) {
            if (!empty($val)) {
                $set .= $key.'=:'.$key;
                if ($i == $cnt - 1) { // last
                } else {
                    $set .= ', ';
                }
            }
            ++$i;
        }

        //$query = 'UPDATE '.$db1->db_prefix.'auths SET '.$set.' WHERE id=:id';
        //$debug->display($query);
        //exit;

        $db1->query('UPDATE ' . $db1->db_prefix . 'users SET ' . $set . ' WHERE id=:id');

        foreach ($db_cols as $key => $val) {
            $db1->bind(':'.$key, $val);
        }

        $db1->bind(':id', $id);

        $update = $db1->execute();

        if (!empty($db_cols['photo_url']) && $providerUid) {
            $this->updatePhoto($id, $db_cols['photo_url'], $providerUid);
        }

        if ($update) {
            return $id;
        }

        return false;
    }

    public function updateUserV2(array $payload)
    {
        global $db1;

        $dbCols = [];
        $id = $payload['id'];
        unset($payload['id']);

        $placeHolders = [];
        foreach ($payload as $key => $val) {
            if (!empty($val)) {
                $dbCols[$key] = $val;
                $placeHolders[] = $key . '=:' . $key;
            }
        }
        $set = implode(',', $placeHolders);
        $set .= ',updated=:updated';

        $db1->query('UPDATE ' . $db1->db_prefix . 'users SET ' . $set . ' WHERE id=:id');

        foreach ($dbCols as $key => $val) {
            $db1->bind(':' . $key, $val);
        }

        $db1->bind(':id', $id);
        $db1->bind(':updated', date('Y-m-d H:i:s'));
        $db1->execute();

        $user = $this->get($id);
        $session = new Session();
        $session->set('user', $user);
        $session->save();

        return $user;
    }

    public function updatePhoto($userId, $path, $providerUid = null)
    {
        global $db1;

        $db1->query('update ' . $db1->db_prefix . 'users set photo_url=:photoUrl, updated=:updated where id=:id');
        $db1->bind(':photoUrl', $path);
        $db1->bind(':updated', date('Y-m-d H:i:s'));
        $db1->bind(':id', $userId);
        $result = $db1->execute();

        if ($providerUid) {
            $db1->query(
                'update ' . $db1->db_prefix . 'auths '
                . 'set picture=:picture '
                . 'where user_id=:userId and provider_uid=:providerUid'
            );
            $db1->bind(':picture', basename($path));
            $db1->bind(':userId', $userId);
            $db1->bind(':providerUid', $providerUid);
            $db1->execute();
        }

        return $result;
    }

    public function createVuser()
    {
        global $db1;

        $session = new Session();
        $request = Request::createFromGlobals();

        // ALTER TABLE `gradio_sessions` CHANGE `vuser` `vuser_id` VARCHAR(16) NULL DEFAULT NULL;
        $currentUserId = $this->authId() ? $this->authId() : null;

        $vuserId = substr(md5(rand()), 0, 16);
        $cookie = new Cookie('vuserId', $vuserId, time()+2592000); // 1 month
        $response = new JsonResponse();
        $response->headers->setCookie($cookie);

        $date = date('Y-m-d H:i:s');
        $data = [
            'user_id' => $currentUserId,
            'vuser_id' => $vuserId, // in db table must be with index to perfom much faster search
            'ip' => IP,
            'visits' => 1,
            'status' => 1,
            'lastvisit' => $date,
            'added' => $date
        ];

        foreach ($data as $key => $arg) {
            if (!empty($arg)) {
                $dbCols[$key] = $arg;
            }
        }

        // TODO
        # erros atrast, problēma bija, ka tukšu uzskatīja kā null, bet datubāzē nebija atļaujas tādu ievietot,
        # ieliku PDO::NULL_TO_STRING, opcijās un NEKAS neaizgāja!
        # uztaisīju, lai izvāc tukšās vērtības un liek tikai kurām kaut kas ir!
        # šito vajag izfiškot, jo ļoti daudz laiku prasīja!!!

        $keys = implode(', ', array_keys($dbCols));
        $values = str_repeat('?, ', count($dbCols) - 1).'?';

        $i = 1;
        $db1->query('INSERT INTO ' . $db1->db_prefix . 'sessions (' . $keys . ') VALUES (' . $values . ')');

        foreach ($dbCols as $value) {
            $db1->bind($i++, $value);
        }
        $db1->execute();
        $session->set('online', true);
        $session->save();

        $addHumanAction = Action::addActivity('guest_loggedin', 'site', $db1->lastInsertId(), null);

        // this part sometimes often stops login flow from social networks
//        $result = [
//            'status' => 'success',
//            'response' => [
//                'vuserId' => $vuserId
//            ]
//        ];
//
//        $response->setData($result);
//        $response->send();
    }

    private function addPicture()
    {
        $this->user['filesPath'] = Config::get('base.url') . Config::get('paths.files') . '/user_'  . $this->user['id'] . '/';
        $this->user['image'] = Config::get('base.url') .
                Config::get('paths.files') . '/user_'  . $this->user['id'] . '/' . $this->user['photo_url'];
    }

    private function fetchUser(int $userId)
    {
        $users = $this->getUsers([$userId]);

        $this->user = $users[0] ?? null;
    }

    public function getUsers(array $userIds)
    {
        global $db1;

        $db1->query('SELECT * FROM ' . $db1->db_prefix . 'users WHERE id in (:ids)');
        $db1->bind(':ids', implode(',', $userIds));

        return $db1->resultset();
    }

    public function get($userId)
    {
        $this->fetchUser($userId);
        if ($this->user) {
            $this->user['exists'] = true;
            $this->addPicture();
            $this->hideFields();
        } else {
            $this->user['exists'] = false;
        }

        return $this->user;
    }

    public function find_by_email($email)
    {
        global $db1;
        $db1->query('SELECT * FROM '.$db1->db_prefix.'users WHERE email=:email LIMIT 1');
        $db1->bind(':email', $email);

        return $db1->single();
    }

    public function friends()
    {
        /*

        SELECT u.id, u.first_name, u.last_name FROM gradio_friends f
        INNER JOIN gradio_users u
        ON u.id=f.user_id
        WHERE f.friend_id = 100 AND confirmed !=  '0000-00-00 00:00:00'
        UNION
        SELECT u.id, u.first_name, u.last_name FROM gradio_friends f
        INNER JOIN gradio_users u
        ON u.id=f.friend_id
        WHERE f.user_id = 100 AND confirmed !=  '0000-00-00 00:00:00'

        SELECT u.id, u.first_name, u.last_name FROM `gradio_friends` f
        LEFT JOIN gradio_users u
        ON u.id=f.user_id
        WHERE f.friend_id = 100 OR f.user_id = 100

        SELECT u.id, u.first_name, u.last_name FROM gradio_friends f
        INNER JOIN gradio_users u
        ON u.id=f.friend_id

        INNER JOIN gradio_users us
        ON us.id=f.user_id
        WHERE f.friend_id = 100 AND f.user_id = 100
        */
    }

    public function find_by_email_and_password($email, $password)
    {
        global $db1;
        $db1->query('SELECT * FROM '.$db1->db_prefix.'users WHERE email=:email AND password=:password LIMIT 1');
        $db1->bind(':email', $email);
        $db1->bind(':password', $password);

        return $db1->single();
    }

    public function currentVisit()
    {
        global $db1;

        $session = new Session();
        $request = Request::createFromGlobals();
        $cookie = $request->cookies;
        $currentUserId = $this->authId() ? $this->authId() : null;
        $vuserId = $cookie->get('vuserId');
        $lastActivity = $session->get('lastActivity') ? $session->get('lastActivity') : time();

        if ($currentUserId) {
            $db1->query('UPDATE ' . $db1->db_prefix . 'users SET currentvisit=:currentvisit WHERE id=:id');
            $db1->bind(':id', $currentUserId);
            $db1->bind(':currentvisit', date('Y-m-d H:i:s'));
            $db1->execute();
        }

        $this->tillRefresh =  $lastActivity + Config::get('prefs.pingfreq') - time();
        $session->set('tillRefresh', $this->tillRefresh);

        if ($vuserId) {
            // $status 0 - offline, 1 - online, 2 - loggedout, 3 - guest online,
            $table = $db1->db_prefix . 'sessions';
            $db1->query('update ' . $table . ' set currentvisit=:currentVisit,'
                    . ' clicks=clicks+1, till_refresh=:tillRefresh,'
                    . ' status=:status where vuser_id=:vuserId');
            $db1->bind(':currentVisit', date('Y-m-d H:i:s'));
            $db1->bind(':tillRefresh', $this->tillRefresh);
            $db1->bind(':status', $session->get('online') ? $session->get('online') : 0); //set as online
            $db1->bind(':vuserId', $vuserId);
            $db1->execute();
        }
        $session->set('currentActivity', time());
        $session->save();

        $this->lastVisit();
    }

    public function lastVisit($force = false)
    {
        global $db1;

        $session = new Session();
        $request = Request::createFromGlobals();
        $cookie = $request->cookies;
        $currentUserId = $this->authId() ? $this->authId() : null;
        $vuserId = $cookie->get('vuserId');
        $lastVisitCheckTime = $session->get('lastActivity') + Config::get('prefs.pingfreq');
        $loginLogoutCheckTime = $session->get('lastActivity') + Config::get('prefs.pingfreq') * 12;
        $latestProvider = $session->get('provider');

        if ($currentUserId) {
            $user = $this->get($currentUserId);
        }

        if ($currentUserId && ($lastVisitCheckTime < time() || !$user['lastvisit'] || $force === true)) {
            $db1->query('UPDATE ' . $db1->db_prefix .
                    'users set latest_provider=:latestProvider,'
                    . ' lastvisit=:lastvisit where id=:id');
            $db1->bind(':latestProvider', $latestProvider);
            $db1->bind(':lastvisit', date('Y-m-d H:i:s'));
            $db1->bind(':id', $currentUserId);
            $db1->execute();
            $session->set('lastActivity', time());
        }

        $checkVUsr = $this->checkVUsr();

        if ($vuserId && $checkVUsr && ($lastVisitCheckTime < time() || $force === true)) {
            $db1->query('UPDATE ' . $db1->db_prefix .
                    'sessions set user_id=:user_id, lastvisit=:lastvisit,'
                    . ' ip=:ip where vuser_id=:vuserId');
            $db1->bind(':vuserId', $vuserId);
            $db1->bind(':user_id', $currentUserId);
            $db1->bind(':lastvisit', date('Y-m-d H:i:s'));
            $db1->bind(':ip', IP);
            $db1->execute();
            $session->set('lastActivity', time());
        }

        if (!$checkVUsr) {
            $this->createVuser();
        }

        //$this->userVisits();

        if ($currentUserId && (!$session->get('online') && $loginLogoutCheckTime < time() || $force === true)) {
            $session->save();
            Action::addActivity('loggedin', 'site', $currentUserId, null);
        }
    }

    public function userVisits()
    {
        global $db1;

        $session = new Session();

        $request = Request::createFromGlobals();
        $cookie = $request->cookies;
        $currentUserId = $this->authId() ? $this->authId() : null;
        $vuserId = $cookie->get('vuserId');

        if ($currentUserId && !$session->get('online')) {
            // initiate value
            $session->set('online', true);
            $session->save();
            $db1->query('UPDATE ' . $db1->db_prefix . 'users SET visits=visits+1 WHERE id=:id');
            $db1->bind(':id', $this->authId());
            $db1->execute();

            $db1->query('UPDATE ' . $db1->db_prefix
                    . 'sessions SET visits=visits+1, status=:status WHERE vuser_id=:vuserId');
            $db1->bind(':vuserId', $cookie->get('vuserId'));
            $db1->bind(':status', 1); // online
            $db1->execute();

            //$addHumanAction = $this->addActivity->addHumanAction('loggedin', 'site', null, null);
            //print_r($addHumanAction);

            //$result['addHumanAction'] = $addHumanAction;
        }

        /*
        if($cookie->Exists('vuser') && !$session->get('online')){
            // initiate value
            $session->set('online', true);
            $db1->query('UPDATE '.$db1->db_prefix.'sessions SET visits=visits+1 WHERE vuser=:vuser');
            $db1->bind(':vuser', $cookie->Get('vuser'));
            $db1->execute();
        }
        */
    }

    public function getActionsIds(int $userId, int $start, int $limit)
    {
        global $db1;

        $db1->query('SELECT id FROM '. $db1->db_prefix .
          'action_category WHERE user_id=:user_id and deleted_at is null order by created_at desc LIMIT :start, :limit');

        $db1->bind(':start', $start);
        $db1->bind(':limit', $limit);
        $db1->bind(':user_id', $userId);

        // return array_column($db1->resultset(), 'id');
        return $db1->resultset();
    }

    public function getActionsIdsByLastId(int $userId, int $lastId)
    {
        global $db1;

        $db1->query('SELECT id FROM '. $db1->db_prefix .
          'action_category WHERE user_id=:user_id and deleted_at is null and id > :last_id order by created_at desc');

        $db1->bind(':user_id', $userId);
        $db1->bind(':last_id', $lastId);

        return $db1->resultset();
    }

    /**
     *
     * @return array|null
     */
    public function roles(): ?array
    {
        $rbac = new Rbac();

        return $this->auth() ? $rbac->Users->allRoles($this->auth()->id) : null;
    }

    public function saveNewUser($userProfile)
    {
        $argsUser = [
            'password' => rand(),
            'username' => mb_strtolower($userProfile->firstName),
            'first_name' => $userProfile->firstName,
            'last_name' => $userProfile->lastName,
            //'display_name' => $userProfile->displayName,
            'display_name' => $userProfile->firstName,
            'description' => $userProfile->description,
            'email' => $userProfile->email,
            'email_verified' => $userProfile->emailVerified,
            'phone' => $userProfile->phone,
            'gender' => $userProfile->gender,
            'language' => $userProfile->language,
            //'birthday'
            // => new DateTime($userProfile->birthYear.'-'
            // .$userProfile->birthMonth.'-'.$userProfile->birthDay),
            'age' => $userProfile->age,
            'address' => $userProfile->address,
            'country' => $userProfile->country,
            'region' => $userProfile->region,
            'city' => $userProfile->city,
            'zip' => $userProfile->zip,
            'website_url' => $userProfile->webSiteURL,
            'profile_url' => $userProfile->profileURL,
            'photo_url' => '',
            'ip' => IP,
            'added' => date_create()->format('Y-m-d H:i:s'),
        ];

        $newUserId = $this->create($argsUser);

        $rbac = new Rbac();
        $rbac->Users->assign($newUserId, 5);

        return $newUserId;
    }
}
