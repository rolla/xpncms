<?php

namespace Model\Song;

use Model\Song\Base\SongsVersion as BaseSongsVersion;

/**
 * Skeleton subclass for representing a row from the 'songs_version' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SongsVersion extends BaseSongsVersion
{

}
