<?php

namespace Model\Song;

use Model\Song\Base\SongsInformation as BaseSongsInformation;

/**
 * Skeleton subclass for representing a row from the 'songs_information' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SongsInformation extends BaseSongsInformation
{

}
