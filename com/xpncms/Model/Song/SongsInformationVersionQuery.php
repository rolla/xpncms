<?php

namespace Model\Song;

use Model\Song\Base\SongsInformationVersionQuery as BaseSongsInformationVersionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'songs_information_version' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SongsInformationVersionQuery extends BaseSongsInformationVersionQuery
{

}
