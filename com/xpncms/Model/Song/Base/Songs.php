<?php

namespace Model\Song\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Song\Songs as ChildSongs;
use Model\Song\SongsQuery as ChildSongsQuery;
use Model\Song\SongsVersion as ChildSongsVersion;
use Model\Song\SongsVersionQuery as ChildSongsVersionQuery;
use Model\Song\Map\SongsTableMap;
use Model\Song\Map\SongsVersionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'songs' table.
 *
 *
 *
 * @package    propel.generator.Model.Song.Base
 */
abstract class Songs implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Song\\Map\\SongsTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the path field.
     *
     * @var        string
     */
    protected $path;

    /**
     * The value for the enabled field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $enabled;

    /**
     * The value for the date_played field.
     *
     * Note: this column has a database default value of: '2002-01-01 00:00:01.000000'
     * @var        DateTime
     */
    protected $date_played;

    /**
     * The value for the artist_played field.
     *
     * Note: this column has a database default value of: '2002-01-01 00:00:01.000000'
     * @var        DateTime
     */
    protected $artist_played;

    /**
     * The value for the count_played field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $count_played;

    /**
     * The value for the play_limit field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $play_limit;

    /**
     * The value for the limit_action field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $limit_action;

    /**
     * The value for the start_date field.
     *
     * Note: this column has a database default value of: '2002-01-01 00:00:01.000000'
     * @var        DateTime
     */
    protected $start_date;

    /**
     * The value for the end_date field.
     *
     * Note: this column has a database default value of: '2002-01-01 00:00:01.000000'
     * @var        DateTime
     */
    protected $end_date;

    /**
     * The value for the song_type field.
     *
     * @var        int
     */
    protected $song_type;

    /**
     * The value for the id_subcat field.
     *
     * @var        int
     */
    protected $id_subcat;

    /**
     * The value for the id_genre field.
     *
     * @var        int
     */
    protected $id_genre;

    /**
     * The value for the weight field.
     *
     * Note: this column has a database default value of: 50.0
     * @var        double
     */
    protected $weight;

    /**
     * The value for the duration field.
     *
     * @var        double
     */
    protected $duration;

    /**
     * The value for the cue_times field.
     *
     * Note: this column has a database default value of: '&'
     * @var        string
     */
    protected $cue_times;

    /**
     * The value for the precise_cue field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $precise_cue;

    /**
     * The value for the fade_type field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $fade_type;

    /**
     * The value for the end_type field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $end_type;

    /**
     * The value for the overlay field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $overlay;

    /**
     * The value for the artist field.
     *
     * @var        string
     */
    protected $artist;

    /**
     * The value for the original_artist field.
     *
     * @var        string
     */
    protected $original_artist;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the album field.
     *
     * @var        string
     */
    protected $album;

    /**
     * The value for the composer field.
     *
     * @var        string
     */
    protected $composer;

    /**
     * The value for the year field.
     *
     * Note: this column has a database default value of: '1900'
     * @var        string
     */
    protected $year;

    /**
     * The value for the track_no field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $track_no;

    /**
     * The value for the disc_no field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $disc_no;

    /**
     * The value for the publisher field.
     *
     * @var        string
     */
    protected $publisher;

    /**
     * The value for the copyright field.
     *
     * @var        string
     */
    protected $copyright;

    /**
     * The value for the isrc field.
     *
     * @var        string
     */
    protected $isrc;

    /**
     * The value for the bpm field.
     *
     * @var        double
     */
    protected $bpm;

    /**
     * The value for the comments field.
     *
     * @var        string
     */
    protected $comments;

    /**
     * The value for the sweepers field.
     *
     * @var        string
     */
    protected $sweepers;

    /**
     * The value for the album_art field.
     *
     * Note: this column has a database default value of: 'no_image.jpg'
     * @var        string
     */
    protected $album_art;

    /**
     * The value for the buy_link field.
     *
     * Note: this column has a database default value of: 'http://'
     * @var        string
     */
    protected $buy_link;

    /**
     * The value for the tdate_played field.
     *
     * Note: this column has a database default value of: '2002-01-01 00:00:01.000000'
     * @var        DateTime
     */
    protected $tdate_played;

    /**
     * The value for the tartist_played field.
     *
     * Note: this column has a database default value of: '2002-01-01 00:00:01.000000'
     * @var        DateTime
     */
    protected $tartist_played;

    /**
     * The value for the date_added field.
     *
     * @var        DateTime
     */
    protected $date_added;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the version field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $version;

    /**
     * The value for the version_created_at field.
     *
     * @var        DateTime
     */
    protected $version_created_at;

    /**
     * The value for the version_created_by field.
     *
     * @var        string
     */
    protected $version_created_by;

    /**
     * The value for the version_comment field.
     *
     * @var        string
     */
    protected $version_comment;

    /**
     * @var        ObjectCollection|ChildSongsVersion[] Collection to store aggregation of ChildSongsVersion objects.
     */
    protected $collSongsVersions;
    protected $collSongsVersionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // versionable behavior


    /**
     * @var bool
     */
    protected $enforceVersion = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSongsVersion[]
     */
    protected $songsVersionsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->enabled = 0;
        $this->date_played = PropelDateTime::newInstance('2002-01-01 00:00:01.000000', null, 'DateTime');
        $this->artist_played = PropelDateTime::newInstance('2002-01-01 00:00:01.000000', null, 'DateTime');
        $this->count_played = 0;
        $this->play_limit = 0;
        $this->limit_action = 0;
        $this->start_date = PropelDateTime::newInstance('2002-01-01 00:00:01.000000', null, 'DateTime');
        $this->end_date = PropelDateTime::newInstance('2002-01-01 00:00:01.000000', null, 'DateTime');
        $this->weight = 50.0;
        $this->cue_times = '&';
        $this->precise_cue = false;
        $this->fade_type = false;
        $this->end_type = false;
        $this->overlay = false;
        $this->year = '1900';
        $this->track_no = 0;
        $this->disc_no = 0;
        $this->album_art = 'no_image.jpg';
        $this->buy_link = 'http://';
        $this->tdate_played = PropelDateTime::newInstance('2002-01-01 00:00:01.000000', null, 'DateTime');
        $this->tartist_played = PropelDateTime::newInstance('2002-01-01 00:00:01.000000', null, 'DateTime');
        $this->version = 0;
    }

    /**
     * Initializes internal state of Model\Song\Base\Songs object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Songs</code> instance.  If
     * <code>obj</code> is an instance of <code>Songs</code>, delegates to
     * <code>equals(Songs)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Songs The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [path] column value.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get the [enabled] column value.
     *
     * @return int
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get the [optionally formatted] temporal [date_played] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePlayed($format = NULL)
    {
        if ($format === null) {
            return $this->date_played;
        } else {
            return $this->date_played instanceof \DateTimeInterface ? $this->date_played->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [artist_played] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getArtistPlayed($format = NULL)
    {
        if ($format === null) {
            return $this->artist_played;
        } else {
            return $this->artist_played instanceof \DateTimeInterface ? $this->artist_played->format($format) : null;
        }
    }

    /**
     * Get the [count_played] column value.
     *
     * @return int
     */
    public function getCountPlayed()
    {
        return $this->count_played;
    }

    /**
     * Get the [play_limit] column value.
     *
     * @return int
     */
    public function getPlayLimit()
    {
        return $this->play_limit;
    }

    /**
     * Get the [limit_action] column value.
     *
     * @return int
     */
    public function getLimitAction()
    {
        return $this->limit_action;
    }

    /**
     * Get the [optionally formatted] temporal [start_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartDate($format = NULL)
    {
        if ($format === null) {
            return $this->start_date;
        } else {
            return $this->start_date instanceof \DateTimeInterface ? $this->start_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [end_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndDate($format = NULL)
    {
        if ($format === null) {
            return $this->end_date;
        } else {
            return $this->end_date instanceof \DateTimeInterface ? $this->end_date->format($format) : null;
        }
    }

    /**
     * Get the [song_type] column value.
     *
     * @return int
     */
    public function getSongType()
    {
        return $this->song_type;
    }

    /**
     * Get the [id_subcat] column value.
     *
     * @return int
     */
    public function getIdSubcat()
    {
        return $this->id_subcat;
    }

    /**
     * Get the [id_genre] column value.
     *
     * @return int
     */
    public function getIdGenre()
    {
        return $this->id_genre;
    }

    /**
     * Get the [weight] column value.
     *
     * @return double
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get the [duration] column value.
     *
     * @return double
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Get the [cue_times] column value.
     *
     * @return string
     */
    public function getCueTimes()
    {
        return $this->cue_times;
    }

    /**
     * Get the [precise_cue] column value.
     *
     * @return boolean
     */
    public function getPreciseCue()
    {
        return $this->precise_cue;
    }

    /**
     * Get the [precise_cue] column value.
     *
     * @return boolean
     */
    public function isPreciseCue()
    {
        return $this->getPreciseCue();
    }

    /**
     * Get the [fade_type] column value.
     *
     * @return boolean
     */
    public function getFadeType()
    {
        return $this->fade_type;
    }

    /**
     * Get the [fade_type] column value.
     *
     * @return boolean
     */
    public function isFadeType()
    {
        return $this->getFadeType();
    }

    /**
     * Get the [end_type] column value.
     *
     * @return boolean
     */
    public function getEndType()
    {
        return $this->end_type;
    }

    /**
     * Get the [end_type] column value.
     *
     * @return boolean
     */
    public function isEndType()
    {
        return $this->getEndType();
    }

    /**
     * Get the [overlay] column value.
     *
     * @return boolean
     */
    public function getOverlay()
    {
        return $this->overlay;
    }

    /**
     * Get the [overlay] column value.
     *
     * @return boolean
     */
    public function isOverlay()
    {
        return $this->getOverlay();
    }

    /**
     * Get the [artist] column value.
     *
     * @return string
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Get the [original_artist] column value.
     *
     * @return string
     */
    public function getOriginalArtist()
    {
        return $this->original_artist;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [album] column value.
     *
     * @return string
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Get the [composer] column value.
     *
     * @return string
     */
    public function getComposer()
    {
        return $this->composer;
    }

    /**
     * Get the [year] column value.
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Get the [track_no] column value.
     *
     * @return int
     */
    public function getTrackNo()
    {
        return $this->track_no;
    }

    /**
     * Get the [disc_no] column value.
     *
     * @return int
     */
    public function getDiscNo()
    {
        return $this->disc_no;
    }

    /**
     * Get the [publisher] column value.
     *
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Get the [copyright] column value.
     *
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Get the [isrc] column value.
     *
     * @return string
     */
    public function getIsrc()
    {
        return $this->isrc;
    }

    /**
     * Get the [bpm] column value.
     *
     * @return double
     */
    public function getBpm()
    {
        return $this->bpm;
    }

    /**
     * Get the [comments] column value.
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Get the [sweepers] column value.
     *
     * @return string
     */
    public function getSweepers()
    {
        return $this->sweepers;
    }

    /**
     * Get the [album_art] column value.
     *
     * @return string
     */
    public function getAlbumArt()
    {
        return $this->album_art;
    }

    /**
     * Get the [buy_link] column value.
     *
     * @return string
     */
    public function getBuyLink()
    {
        return $this->buy_link;
    }

    /**
     * Get the [optionally formatted] temporal [tdate_played] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTdatePlayed($format = NULL)
    {
        if ($format === null) {
            return $this->tdate_played;
        } else {
            return $this->tdate_played instanceof \DateTimeInterface ? $this->tdate_played->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [tartist_played] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTartistPlayed($format = NULL)
    {
        if ($format === null) {
            return $this->tartist_played;
        } else {
            return $this->tartist_played instanceof \DateTimeInterface ? $this->tartist_played->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [date_added] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateAdded($format = NULL)
    {
        if ($format === null) {
            return $this->date_added;
        } else {
            return $this->date_added instanceof \DateTimeInterface ? $this->date_added->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [version] column value.
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Get the [optionally formatted] temporal [version_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getVersionCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->version_created_at;
        } else {
            return $this->version_created_at instanceof \DateTimeInterface ? $this->version_created_at->format($format) : null;
        }
    }

    /**
     * Get the [version_created_by] column value.
     *
     * @return string
     */
    public function getVersionCreatedBy()
    {
        return $this->version_created_by;
    }

    /**
     * Get the [version_comment] column value.
     *
     * @return string
     */
    public function getVersionComment()
    {
        return $this->version_comment;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SongsTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [path] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setPath($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->path !== $v) {
            $this->path = $v;
            $this->modifiedColumns[SongsTableMap::COL_PATH] = true;
        }

        return $this;
    } // setPath()

    /**
     * Set the value of [enabled] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setEnabled($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->enabled !== $v) {
            $this->enabled = $v;
            $this->modifiedColumns[SongsTableMap::COL_ENABLED] = true;
        }

        return $this;
    } // setEnabled()

    /**
     * Sets the value of [date_played] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setDatePlayed($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_played !== null || $dt !== null) {
            if ( ($dt != $this->date_played) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === '2002-01-01 00:00:01.000000') // or the entered value matches the default
                 ) {
                $this->date_played = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_DATE_PLAYED] = true;
            }
        } // if either are not null

        return $this;
    } // setDatePlayed()

    /**
     * Sets the value of [artist_played] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setArtistPlayed($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->artist_played !== null || $dt !== null) {
            if ( ($dt != $this->artist_played) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === '2002-01-01 00:00:01.000000') // or the entered value matches the default
                 ) {
                $this->artist_played = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_ARTIST_PLAYED] = true;
            }
        } // if either are not null

        return $this;
    } // setArtistPlayed()

    /**
     * Set the value of [count_played] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setCountPlayed($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->count_played !== $v) {
            $this->count_played = $v;
            $this->modifiedColumns[SongsTableMap::COL_COUNT_PLAYED] = true;
        }

        return $this;
    } // setCountPlayed()

    /**
     * Set the value of [play_limit] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setPlayLimit($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->play_limit !== $v) {
            $this->play_limit = $v;
            $this->modifiedColumns[SongsTableMap::COL_PLAY_LIMIT] = true;
        }

        return $this;
    } // setPlayLimit()

    /**
     * Set the value of [limit_action] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setLimitAction($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->limit_action !== $v) {
            $this->limit_action = $v;
            $this->modifiedColumns[SongsTableMap::COL_LIMIT_ACTION] = true;
        }

        return $this;
    } // setLimitAction()

    /**
     * Sets the value of [start_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setStartDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->start_date !== null || $dt !== null) {
            if ( ($dt != $this->start_date) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === '2002-01-01 00:00:01.000000') // or the entered value matches the default
                 ) {
                $this->start_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_START_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setStartDate()

    /**
     * Sets the value of [end_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setEndDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->end_date !== null || $dt !== null) {
            if ( ($dt != $this->end_date) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === '2002-01-01 00:00:01.000000') // or the entered value matches the default
                 ) {
                $this->end_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_END_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setEndDate()

    /**
     * Set the value of [song_type] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setSongType($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->song_type !== $v) {
            $this->song_type = $v;
            $this->modifiedColumns[SongsTableMap::COL_SONG_TYPE] = true;
        }

        return $this;
    } // setSongType()

    /**
     * Set the value of [id_subcat] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setIdSubcat($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_subcat !== $v) {
            $this->id_subcat = $v;
            $this->modifiedColumns[SongsTableMap::COL_ID_SUBCAT] = true;
        }

        return $this;
    } // setIdSubcat()

    /**
     * Set the value of [id_genre] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setIdGenre($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_genre !== $v) {
            $this->id_genre = $v;
            $this->modifiedColumns[SongsTableMap::COL_ID_GENRE] = true;
        }

        return $this;
    } // setIdGenre()

    /**
     * Set the value of [weight] column.
     *
     * @param double $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setWeight($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->weight !== $v) {
            $this->weight = $v;
            $this->modifiedColumns[SongsTableMap::COL_WEIGHT] = true;
        }

        return $this;
    } // setWeight()

    /**
     * Set the value of [duration] column.
     *
     * @param double $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setDuration($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->duration !== $v) {
            $this->duration = $v;
            $this->modifiedColumns[SongsTableMap::COL_DURATION] = true;
        }

        return $this;
    } // setDuration()

    /**
     * Set the value of [cue_times] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setCueTimes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cue_times !== $v) {
            $this->cue_times = $v;
            $this->modifiedColumns[SongsTableMap::COL_CUE_TIMES] = true;
        }

        return $this;
    } // setCueTimes()

    /**
     * Sets the value of the [precise_cue] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setPreciseCue($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->precise_cue !== $v) {
            $this->precise_cue = $v;
            $this->modifiedColumns[SongsTableMap::COL_PRECISE_CUE] = true;
        }

        return $this;
    } // setPreciseCue()

    /**
     * Sets the value of the [fade_type] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setFadeType($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->fade_type !== $v) {
            $this->fade_type = $v;
            $this->modifiedColumns[SongsTableMap::COL_FADE_TYPE] = true;
        }

        return $this;
    } // setFadeType()

    /**
     * Sets the value of the [end_type] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setEndType($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->end_type !== $v) {
            $this->end_type = $v;
            $this->modifiedColumns[SongsTableMap::COL_END_TYPE] = true;
        }

        return $this;
    } // setEndType()

    /**
     * Sets the value of the [overlay] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setOverlay($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->overlay !== $v) {
            $this->overlay = $v;
            $this->modifiedColumns[SongsTableMap::COL_OVERLAY] = true;
        }

        return $this;
    } // setOverlay()

    /**
     * Set the value of [artist] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setArtist($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->artist !== $v) {
            $this->artist = $v;
            $this->modifiedColumns[SongsTableMap::COL_ARTIST] = true;
        }

        return $this;
    } // setArtist()

    /**
     * Set the value of [original_artist] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setOriginalArtist($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->original_artist !== $v) {
            $this->original_artist = $v;
            $this->modifiedColumns[SongsTableMap::COL_ORIGINAL_ARTIST] = true;
        }

        return $this;
    } // setOriginalArtist()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[SongsTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [album] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setAlbum($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->album !== $v) {
            $this->album = $v;
            $this->modifiedColumns[SongsTableMap::COL_ALBUM] = true;
        }

        return $this;
    } // setAlbum()

    /**
     * Set the value of [composer] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setComposer($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->composer !== $v) {
            $this->composer = $v;
            $this->modifiedColumns[SongsTableMap::COL_COMPOSER] = true;
        }

        return $this;
    } // setComposer()

    /**
     * Set the value of [year] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setYear($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->year !== $v) {
            $this->year = $v;
            $this->modifiedColumns[SongsTableMap::COL_YEAR] = true;
        }

        return $this;
    } // setYear()

    /**
     * Set the value of [track_no] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setTrackNo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->track_no !== $v) {
            $this->track_no = $v;
            $this->modifiedColumns[SongsTableMap::COL_TRACK_NO] = true;
        }

        return $this;
    } // setTrackNo()

    /**
     * Set the value of [disc_no] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setDiscNo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->disc_no !== $v) {
            $this->disc_no = $v;
            $this->modifiedColumns[SongsTableMap::COL_DISC_NO] = true;
        }

        return $this;
    } // setDiscNo()

    /**
     * Set the value of [publisher] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setPublisher($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->publisher !== $v) {
            $this->publisher = $v;
            $this->modifiedColumns[SongsTableMap::COL_PUBLISHER] = true;
        }

        return $this;
    } // setPublisher()

    /**
     * Set the value of [copyright] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setCopyright($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->copyright !== $v) {
            $this->copyright = $v;
            $this->modifiedColumns[SongsTableMap::COL_COPYRIGHT] = true;
        }

        return $this;
    } // setCopyright()

    /**
     * Set the value of [isrc] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setIsrc($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->isrc !== $v) {
            $this->isrc = $v;
            $this->modifiedColumns[SongsTableMap::COL_ISRC] = true;
        }

        return $this;
    } // setIsrc()

    /**
     * Set the value of [bpm] column.
     *
     * @param double $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setBpm($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->bpm !== $v) {
            $this->bpm = $v;
            $this->modifiedColumns[SongsTableMap::COL_BPM] = true;
        }

        return $this;
    } // setBpm()

    /**
     * Set the value of [comments] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setComments($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->comments !== $v) {
            $this->comments = $v;
            $this->modifiedColumns[SongsTableMap::COL_COMMENTS] = true;
        }

        return $this;
    } // setComments()

    /**
     * Set the value of [sweepers] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setSweepers($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sweepers !== $v) {
            $this->sweepers = $v;
            $this->modifiedColumns[SongsTableMap::COL_SWEEPERS] = true;
        }

        return $this;
    } // setSweepers()

    /**
     * Set the value of [album_art] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setAlbumArt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->album_art !== $v) {
            $this->album_art = $v;
            $this->modifiedColumns[SongsTableMap::COL_ALBUM_ART] = true;
        }

        return $this;
    } // setAlbumArt()

    /**
     * Set the value of [buy_link] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setBuyLink($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->buy_link !== $v) {
            $this->buy_link = $v;
            $this->modifiedColumns[SongsTableMap::COL_BUY_LINK] = true;
        }

        return $this;
    } // setBuyLink()

    /**
     * Sets the value of [tdate_played] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setTdatePlayed($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tdate_played !== null || $dt !== null) {
            if ( ($dt != $this->tdate_played) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === '2002-01-01 00:00:01.000000') // or the entered value matches the default
                 ) {
                $this->tdate_played = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_TDATE_PLAYED] = true;
            }
        } // if either are not null

        return $this;
    } // setTdatePlayed()

    /**
     * Sets the value of [tartist_played] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setTartistPlayed($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->tartist_played !== null || $dt !== null) {
            if ( ($dt != $this->tartist_played) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === '2002-01-01 00:00:01.000000') // or the entered value matches the default
                 ) {
                $this->tartist_played = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_TARTIST_PLAYED] = true;
            }
        } // if either are not null

        return $this;
    } // setTartistPlayed()

    /**
     * Sets the value of [date_added] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setDateAdded($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_added !== null || $dt !== null) {
            if ($this->date_added === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->date_added->format("Y-m-d H:i:s.u")) {
                $this->date_added = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_DATE_ADDED] = true;
            }
        } // if either are not null

        return $this;
    } // setDateAdded()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [version] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setVersion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->version !== $v) {
            $this->version = $v;
            $this->modifiedColumns[SongsTableMap::COL_VERSION] = true;
        }

        return $this;
    } // setVersion()

    /**
     * Sets the value of [version_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setVersionCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->version_created_at !== null || $dt !== null) {
            if ($this->version_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->version_created_at->format("Y-m-d H:i:s.u")) {
                $this->version_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsTableMap::COL_VERSION_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setVersionCreatedAt()

    /**
     * Set the value of [version_created_by] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setVersionCreatedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->version_created_by !== $v) {
            $this->version_created_by = $v;
            $this->modifiedColumns[SongsTableMap::COL_VERSION_CREATED_BY] = true;
        }

        return $this;
    } // setVersionCreatedBy()

    /**
     * Set the value of [version_comment] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function setVersionComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->version_comment !== $v) {
            $this->version_comment = $v;
            $this->modifiedColumns[SongsTableMap::COL_VERSION_COMMENT] = true;
        }

        return $this;
    } // setVersionComment()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->enabled !== 0) {
                return false;
            }

            if ($this->date_played && $this->date_played->format('Y-m-d H:i:s.u') !== '2002-01-01 00:00:01.000000') {
                return false;
            }

            if ($this->artist_played && $this->artist_played->format('Y-m-d H:i:s.u') !== '2002-01-01 00:00:01.000000') {
                return false;
            }

            if ($this->count_played !== 0) {
                return false;
            }

            if ($this->play_limit !== 0) {
                return false;
            }

            if ($this->limit_action !== 0) {
                return false;
            }

            if ($this->start_date && $this->start_date->format('Y-m-d H:i:s.u') !== '2002-01-01 00:00:01.000000') {
                return false;
            }

            if ($this->end_date && $this->end_date->format('Y-m-d H:i:s.u') !== '2002-01-01 00:00:01.000000') {
                return false;
            }

            if ($this->weight !== 50.0) {
                return false;
            }

            if ($this->cue_times !== '&') {
                return false;
            }

            if ($this->precise_cue !== false) {
                return false;
            }

            if ($this->fade_type !== false) {
                return false;
            }

            if ($this->end_type !== false) {
                return false;
            }

            if ($this->overlay !== false) {
                return false;
            }

            if ($this->year !== '1900') {
                return false;
            }

            if ($this->track_no !== 0) {
                return false;
            }

            if ($this->disc_no !== 0) {
                return false;
            }

            if ($this->album_art !== 'no_image.jpg') {
                return false;
            }

            if ($this->buy_link !== 'http://') {
                return false;
            }

            if ($this->tdate_played && $this->tdate_played->format('Y-m-d H:i:s.u') !== '2002-01-01 00:00:01.000000') {
                return false;
            }

            if ($this->tartist_played && $this->tartist_played->format('Y-m-d H:i:s.u') !== '2002-01-01 00:00:01.000000') {
                return false;
            }

            if ($this->version !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SongsTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SongsTableMap::translateFieldName('Path', TableMap::TYPE_PHPNAME, $indexType)];
            $this->path = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SongsTableMap::translateFieldName('Enabled', TableMap::TYPE_PHPNAME, $indexType)];
            $this->enabled = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SongsTableMap::translateFieldName('DatePlayed', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->date_played = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SongsTableMap::translateFieldName('ArtistPlayed', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->artist_played = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SongsTableMap::translateFieldName('CountPlayed', TableMap::TYPE_PHPNAME, $indexType)];
            $this->count_played = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SongsTableMap::translateFieldName('PlayLimit', TableMap::TYPE_PHPNAME, $indexType)];
            $this->play_limit = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SongsTableMap::translateFieldName('LimitAction', TableMap::TYPE_PHPNAME, $indexType)];
            $this->limit_action = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SongsTableMap::translateFieldName('StartDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->start_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SongsTableMap::translateFieldName('EndDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->end_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SongsTableMap::translateFieldName('SongType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->song_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SongsTableMap::translateFieldName('IdSubcat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_subcat = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SongsTableMap::translateFieldName('IdGenre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_genre = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SongsTableMap::translateFieldName('Weight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weight = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SongsTableMap::translateFieldName('Duration', TableMap::TYPE_PHPNAME, $indexType)];
            $this->duration = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SongsTableMap::translateFieldName('CueTimes', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cue_times = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : SongsTableMap::translateFieldName('PreciseCue', TableMap::TYPE_PHPNAME, $indexType)];
            $this->precise_cue = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : SongsTableMap::translateFieldName('FadeType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fade_type = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : SongsTableMap::translateFieldName('EndType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->end_type = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : SongsTableMap::translateFieldName('Overlay', TableMap::TYPE_PHPNAME, $indexType)];
            $this->overlay = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : SongsTableMap::translateFieldName('Artist', TableMap::TYPE_PHPNAME, $indexType)];
            $this->artist = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : SongsTableMap::translateFieldName('OriginalArtist', TableMap::TYPE_PHPNAME, $indexType)];
            $this->original_artist = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : SongsTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : SongsTableMap::translateFieldName('Album', TableMap::TYPE_PHPNAME, $indexType)];
            $this->album = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : SongsTableMap::translateFieldName('Composer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->composer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : SongsTableMap::translateFieldName('Year', TableMap::TYPE_PHPNAME, $indexType)];
            $this->year = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : SongsTableMap::translateFieldName('TrackNo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->track_no = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : SongsTableMap::translateFieldName('DiscNo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->disc_no = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : SongsTableMap::translateFieldName('Publisher', TableMap::TYPE_PHPNAME, $indexType)];
            $this->publisher = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : SongsTableMap::translateFieldName('Copyright', TableMap::TYPE_PHPNAME, $indexType)];
            $this->copyright = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : SongsTableMap::translateFieldName('Isrc', TableMap::TYPE_PHPNAME, $indexType)];
            $this->isrc = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : SongsTableMap::translateFieldName('Bpm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bpm = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : SongsTableMap::translateFieldName('Comments', TableMap::TYPE_PHPNAME, $indexType)];
            $this->comments = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : SongsTableMap::translateFieldName('Sweepers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sweepers = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : SongsTableMap::translateFieldName('AlbumArt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->album_art = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : SongsTableMap::translateFieldName('BuyLink', TableMap::TYPE_PHPNAME, $indexType)];
            $this->buy_link = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : SongsTableMap::translateFieldName('TdatePlayed', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->tdate_played = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : SongsTableMap::translateFieldName('TartistPlayed', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->tartist_played = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : SongsTableMap::translateFieldName('DateAdded', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->date_added = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : SongsTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : SongsTableMap::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)];
            $this->version = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 41 + $startcol : SongsTableMap::translateFieldName('VersionCreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->version_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 42 + $startcol : SongsTableMap::translateFieldName('VersionCreatedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->version_created_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 43 + $startcol : SongsTableMap::translateFieldName('VersionComment', TableMap::TYPE_PHPNAME, $indexType)];
            $this->version_comment = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 44; // 44 = SongsTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Song\\Songs'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SongsTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSongsQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSongsVersions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Songs::setDeleted()
     * @see Songs::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSongsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            // versionable behavior
            if ($this->isVersioningNecessary()) {
                $this->setVersion($this->isNew() ? 1 : $this->getLastVersionNumber($con) + 1);
                if (!$this->isColumnModified(SongsTableMap::COL_VERSION_CREATED_AT)) {
                    $this->setVersionCreatedAt(time());
                }
                $createVersion = true; // for postSave hook
            }
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                // versionable behavior
                if (isset($createVersion)) {
                    $this->addVersion($con);
                }
                SongsTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->songsVersionsScheduledForDeletion !== null) {
                if (!$this->songsVersionsScheduledForDeletion->isEmpty()) {
                    \Model\Song\SongsVersionQuery::create()
                        ->filterByPrimaryKeys($this->songsVersionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->songsVersionsScheduledForDeletion = null;
                }
            }

            if ($this->collSongsVersions !== null) {
                foreach ($this->collSongsVersions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SongsTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SongsTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SongsTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ID';
        }
        if ($this->isColumnModified(SongsTableMap::COL_PATH)) {
            $modifiedColumns[':p' . $index++]  = 'path';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ENABLED)) {
            $modifiedColumns[':p' . $index++]  = 'enabled';
        }
        if ($this->isColumnModified(SongsTableMap::COL_DATE_PLAYED)) {
            $modifiedColumns[':p' . $index++]  = 'date_played';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ARTIST_PLAYED)) {
            $modifiedColumns[':p' . $index++]  = 'artist_played';
        }
        if ($this->isColumnModified(SongsTableMap::COL_COUNT_PLAYED)) {
            $modifiedColumns[':p' . $index++]  = 'count_played';
        }
        if ($this->isColumnModified(SongsTableMap::COL_PLAY_LIMIT)) {
            $modifiedColumns[':p' . $index++]  = 'play_limit';
        }
        if ($this->isColumnModified(SongsTableMap::COL_LIMIT_ACTION)) {
            $modifiedColumns[':p' . $index++]  = 'limit_action';
        }
        if ($this->isColumnModified(SongsTableMap::COL_START_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'start_date';
        }
        if ($this->isColumnModified(SongsTableMap::COL_END_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'end_date';
        }
        if ($this->isColumnModified(SongsTableMap::COL_SONG_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'song_type';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ID_SUBCAT)) {
            $modifiedColumns[':p' . $index++]  = 'id_subcat';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ID_GENRE)) {
            $modifiedColumns[':p' . $index++]  = 'id_genre';
        }
        if ($this->isColumnModified(SongsTableMap::COL_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'weight';
        }
        if ($this->isColumnModified(SongsTableMap::COL_DURATION)) {
            $modifiedColumns[':p' . $index++]  = 'duration';
        }
        if ($this->isColumnModified(SongsTableMap::COL_CUE_TIMES)) {
            $modifiedColumns[':p' . $index++]  = 'cue_times';
        }
        if ($this->isColumnModified(SongsTableMap::COL_PRECISE_CUE)) {
            $modifiedColumns[':p' . $index++]  = 'precise_cue';
        }
        if ($this->isColumnModified(SongsTableMap::COL_FADE_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'fade_type';
        }
        if ($this->isColumnModified(SongsTableMap::COL_END_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'end_type';
        }
        if ($this->isColumnModified(SongsTableMap::COL_OVERLAY)) {
            $modifiedColumns[':p' . $index++]  = 'overlay';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ARTIST)) {
            $modifiedColumns[':p' . $index++]  = 'artist';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ORIGINAL_ARTIST)) {
            $modifiedColumns[':p' . $index++]  = 'original_artist';
        }
        if ($this->isColumnModified(SongsTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ALBUM)) {
            $modifiedColumns[':p' . $index++]  = 'album';
        }
        if ($this->isColumnModified(SongsTableMap::COL_COMPOSER)) {
            $modifiedColumns[':p' . $index++]  = 'composer';
        }
        if ($this->isColumnModified(SongsTableMap::COL_YEAR)) {
            $modifiedColumns[':p' . $index++]  = 'year';
        }
        if ($this->isColumnModified(SongsTableMap::COL_TRACK_NO)) {
            $modifiedColumns[':p' . $index++]  = 'track_no';
        }
        if ($this->isColumnModified(SongsTableMap::COL_DISC_NO)) {
            $modifiedColumns[':p' . $index++]  = 'disc_no';
        }
        if ($this->isColumnModified(SongsTableMap::COL_PUBLISHER)) {
            $modifiedColumns[':p' . $index++]  = 'publisher';
        }
        if ($this->isColumnModified(SongsTableMap::COL_COPYRIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'copyright';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ISRC)) {
            $modifiedColumns[':p' . $index++]  = 'isrc';
        }
        if ($this->isColumnModified(SongsTableMap::COL_BPM)) {
            $modifiedColumns[':p' . $index++]  = 'bpm';
        }
        if ($this->isColumnModified(SongsTableMap::COL_COMMENTS)) {
            $modifiedColumns[':p' . $index++]  = 'comments';
        }
        if ($this->isColumnModified(SongsTableMap::COL_SWEEPERS)) {
            $modifiedColumns[':p' . $index++]  = 'sweepers';
        }
        if ($this->isColumnModified(SongsTableMap::COL_ALBUM_ART)) {
            $modifiedColumns[':p' . $index++]  = 'album_art';
        }
        if ($this->isColumnModified(SongsTableMap::COL_BUY_LINK)) {
            $modifiedColumns[':p' . $index++]  = 'buy_link';
        }
        if ($this->isColumnModified(SongsTableMap::COL_TDATE_PLAYED)) {
            $modifiedColumns[':p' . $index++]  = 'tdate_played';
        }
        if ($this->isColumnModified(SongsTableMap::COL_TARTIST_PLAYED)) {
            $modifiedColumns[':p' . $index++]  = 'tartist_played';
        }
        if ($this->isColumnModified(SongsTableMap::COL_DATE_ADDED)) {
            $modifiedColumns[':p' . $index++]  = 'date_added';
        }
        if ($this->isColumnModified(SongsTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION)) {
            $modifiedColumns[':p' . $index++]  = 'version';
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'version_created_at';
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION_CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'version_created_by';
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = 'version_comment';
        }

        $sql = sprintf(
            'INSERT INTO songs (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'ID':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'path':
                        $stmt->bindValue($identifier, $this->path, PDO::PARAM_STR);
                        break;
                    case 'enabled':
                        $stmt->bindValue($identifier, $this->enabled, PDO::PARAM_INT);
                        break;
                    case 'date_played':
                        $stmt->bindValue($identifier, $this->date_played ? $this->date_played->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'artist_played':
                        $stmt->bindValue($identifier, $this->artist_played ? $this->artist_played->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'count_played':
                        $stmt->bindValue($identifier, $this->count_played, PDO::PARAM_INT);
                        break;
                    case 'play_limit':
                        $stmt->bindValue($identifier, $this->play_limit, PDO::PARAM_INT);
                        break;
                    case 'limit_action':
                        $stmt->bindValue($identifier, $this->limit_action, PDO::PARAM_INT);
                        break;
                    case 'start_date':
                        $stmt->bindValue($identifier, $this->start_date ? $this->start_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'end_date':
                        $stmt->bindValue($identifier, $this->end_date ? $this->end_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'song_type':
                        $stmt->bindValue($identifier, $this->song_type, PDO::PARAM_INT);
                        break;
                    case 'id_subcat':
                        $stmt->bindValue($identifier, $this->id_subcat, PDO::PARAM_INT);
                        break;
                    case 'id_genre':
                        $stmt->bindValue($identifier, $this->id_genre, PDO::PARAM_INT);
                        break;
                    case 'weight':
                        $stmt->bindValue($identifier, $this->weight, PDO::PARAM_STR);
                        break;
                    case 'duration':
                        $stmt->bindValue($identifier, $this->duration, PDO::PARAM_STR);
                        break;
                    case 'cue_times':
                        $stmt->bindValue($identifier, $this->cue_times, PDO::PARAM_STR);
                        break;
                    case 'precise_cue':
                        $stmt->bindValue($identifier, (int) $this->precise_cue, PDO::PARAM_INT);
                        break;
                    case 'fade_type':
                        $stmt->bindValue($identifier, (int) $this->fade_type, PDO::PARAM_INT);
                        break;
                    case 'end_type':
                        $stmt->bindValue($identifier, (int) $this->end_type, PDO::PARAM_INT);
                        break;
                    case 'overlay':
                        $stmt->bindValue($identifier, (int) $this->overlay, PDO::PARAM_INT);
                        break;
                    case 'artist':
                        $stmt->bindValue($identifier, $this->artist, PDO::PARAM_STR);
                        break;
                    case 'original_artist':
                        $stmt->bindValue($identifier, $this->original_artist, PDO::PARAM_STR);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'album':
                        $stmt->bindValue($identifier, $this->album, PDO::PARAM_STR);
                        break;
                    case 'composer':
                        $stmt->bindValue($identifier, $this->composer, PDO::PARAM_STR);
                        break;
                    case 'year':
                        $stmt->bindValue($identifier, $this->year, PDO::PARAM_STR);
                        break;
                    case 'track_no':
                        $stmt->bindValue($identifier, $this->track_no, PDO::PARAM_INT);
                        break;
                    case 'disc_no':
                        $stmt->bindValue($identifier, $this->disc_no, PDO::PARAM_INT);
                        break;
                    case 'publisher':
                        $stmt->bindValue($identifier, $this->publisher, PDO::PARAM_STR);
                        break;
                    case 'copyright':
                        $stmt->bindValue($identifier, $this->copyright, PDO::PARAM_STR);
                        break;
                    case 'isrc':
                        $stmt->bindValue($identifier, $this->isrc, PDO::PARAM_STR);
                        break;
                    case 'bpm':
                        $stmt->bindValue($identifier, $this->bpm, PDO::PARAM_STR);
                        break;
                    case 'comments':
                        $stmt->bindValue($identifier, $this->comments, PDO::PARAM_STR);
                        break;
                    case 'sweepers':
                        $stmt->bindValue($identifier, $this->sweepers, PDO::PARAM_STR);
                        break;
                    case 'album_art':
                        $stmt->bindValue($identifier, $this->album_art, PDO::PARAM_STR);
                        break;
                    case 'buy_link':
                        $stmt->bindValue($identifier, $this->buy_link, PDO::PARAM_STR);
                        break;
                    case 'tdate_played':
                        $stmt->bindValue($identifier, $this->tdate_played ? $this->tdate_played->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'tartist_played':
                        $stmt->bindValue($identifier, $this->tartist_played ? $this->tartist_played->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'date_added':
                        $stmt->bindValue($identifier, $this->date_added ? $this->date_added->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'version':
                        $stmt->bindValue($identifier, $this->version, PDO::PARAM_INT);
                        break;
                    case 'version_created_at':
                        $stmt->bindValue($identifier, $this->version_created_at ? $this->version_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'version_created_by':
                        $stmt->bindValue($identifier, $this->version_created_by, PDO::PARAM_STR);
                        break;
                    case 'version_comment':
                        $stmt->bindValue($identifier, $this->version_comment, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SongsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPath();
                break;
            case 2:
                return $this->getEnabled();
                break;
            case 3:
                return $this->getDatePlayed();
                break;
            case 4:
                return $this->getArtistPlayed();
                break;
            case 5:
                return $this->getCountPlayed();
                break;
            case 6:
                return $this->getPlayLimit();
                break;
            case 7:
                return $this->getLimitAction();
                break;
            case 8:
                return $this->getStartDate();
                break;
            case 9:
                return $this->getEndDate();
                break;
            case 10:
                return $this->getSongType();
                break;
            case 11:
                return $this->getIdSubcat();
                break;
            case 12:
                return $this->getIdGenre();
                break;
            case 13:
                return $this->getWeight();
                break;
            case 14:
                return $this->getDuration();
                break;
            case 15:
                return $this->getCueTimes();
                break;
            case 16:
                return $this->getPreciseCue();
                break;
            case 17:
                return $this->getFadeType();
                break;
            case 18:
                return $this->getEndType();
                break;
            case 19:
                return $this->getOverlay();
                break;
            case 20:
                return $this->getArtist();
                break;
            case 21:
                return $this->getOriginalArtist();
                break;
            case 22:
                return $this->getTitle();
                break;
            case 23:
                return $this->getAlbum();
                break;
            case 24:
                return $this->getComposer();
                break;
            case 25:
                return $this->getYear();
                break;
            case 26:
                return $this->getTrackNo();
                break;
            case 27:
                return $this->getDiscNo();
                break;
            case 28:
                return $this->getPublisher();
                break;
            case 29:
                return $this->getCopyright();
                break;
            case 30:
                return $this->getIsrc();
                break;
            case 31:
                return $this->getBpm();
                break;
            case 32:
                return $this->getComments();
                break;
            case 33:
                return $this->getSweepers();
                break;
            case 34:
                return $this->getAlbumArt();
                break;
            case 35:
                return $this->getBuyLink();
                break;
            case 36:
                return $this->getTdatePlayed();
                break;
            case 37:
                return $this->getTartistPlayed();
                break;
            case 38:
                return $this->getDateAdded();
                break;
            case 39:
                return $this->getUpdatedAt();
                break;
            case 40:
                return $this->getVersion();
                break;
            case 41:
                return $this->getVersionCreatedAt();
                break;
            case 42:
                return $this->getVersionCreatedBy();
                break;
            case 43:
                return $this->getVersionComment();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Songs'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Songs'][$this->hashCode()] = true;
        $keys = SongsTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPath(),
            $keys[2] => $this->getEnabled(),
            $keys[3] => $this->getDatePlayed(),
            $keys[4] => $this->getArtistPlayed(),
            $keys[5] => $this->getCountPlayed(),
            $keys[6] => $this->getPlayLimit(),
            $keys[7] => $this->getLimitAction(),
            $keys[8] => $this->getStartDate(),
            $keys[9] => $this->getEndDate(),
            $keys[10] => $this->getSongType(),
            $keys[11] => $this->getIdSubcat(),
            $keys[12] => $this->getIdGenre(),
            $keys[13] => $this->getWeight(),
            $keys[14] => $this->getDuration(),
            $keys[15] => $this->getCueTimes(),
            $keys[16] => $this->getPreciseCue(),
            $keys[17] => $this->getFadeType(),
            $keys[18] => $this->getEndType(),
            $keys[19] => $this->getOverlay(),
            $keys[20] => $this->getArtist(),
            $keys[21] => $this->getOriginalArtist(),
            $keys[22] => $this->getTitle(),
            $keys[23] => $this->getAlbum(),
            $keys[24] => $this->getComposer(),
            $keys[25] => $this->getYear(),
            $keys[26] => $this->getTrackNo(),
            $keys[27] => $this->getDiscNo(),
            $keys[28] => $this->getPublisher(),
            $keys[29] => $this->getCopyright(),
            $keys[30] => $this->getIsrc(),
            $keys[31] => $this->getBpm(),
            $keys[32] => $this->getComments(),
            $keys[33] => $this->getSweepers(),
            $keys[34] => $this->getAlbumArt(),
            $keys[35] => $this->getBuyLink(),
            $keys[36] => $this->getTdatePlayed(),
            $keys[37] => $this->getTartistPlayed(),
            $keys[38] => $this->getDateAdded(),
            $keys[39] => $this->getUpdatedAt(),
            $keys[40] => $this->getVersion(),
            $keys[41] => $this->getVersionCreatedAt(),
            $keys[42] => $this->getVersionCreatedBy(),
            $keys[43] => $this->getVersionComment(),
        );
        if ($result[$keys[3]] instanceof \DateTimeInterface) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[36]] instanceof \DateTimeInterface) {
            $result[$keys[36]] = $result[$keys[36]]->format('c');
        }

        if ($result[$keys[37]] instanceof \DateTimeInterface) {
            $result[$keys[37]] = $result[$keys[37]]->format('c');
        }

        if ($result[$keys[38]] instanceof \DateTimeInterface) {
            $result[$keys[38]] = $result[$keys[38]]->format('c');
        }

        if ($result[$keys[39]] instanceof \DateTimeInterface) {
            $result[$keys[39]] = $result[$keys[39]]->format('c');
        }

        if ($result[$keys[41]] instanceof \DateTimeInterface) {
            $result[$keys[41]] = $result[$keys[41]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collSongsVersions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'songsVersions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'songs_versions';
                        break;
                    default:
                        $key = 'SongsVersions';
                }

                $result[$key] = $this->collSongsVersions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Song\Songs
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SongsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Song\Songs
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPath($value);
                break;
            case 2:
                $this->setEnabled($value);
                break;
            case 3:
                $this->setDatePlayed($value);
                break;
            case 4:
                $this->setArtistPlayed($value);
                break;
            case 5:
                $this->setCountPlayed($value);
                break;
            case 6:
                $this->setPlayLimit($value);
                break;
            case 7:
                $this->setLimitAction($value);
                break;
            case 8:
                $this->setStartDate($value);
                break;
            case 9:
                $this->setEndDate($value);
                break;
            case 10:
                $this->setSongType($value);
                break;
            case 11:
                $this->setIdSubcat($value);
                break;
            case 12:
                $this->setIdGenre($value);
                break;
            case 13:
                $this->setWeight($value);
                break;
            case 14:
                $this->setDuration($value);
                break;
            case 15:
                $this->setCueTimes($value);
                break;
            case 16:
                $this->setPreciseCue($value);
                break;
            case 17:
                $this->setFadeType($value);
                break;
            case 18:
                $this->setEndType($value);
                break;
            case 19:
                $this->setOverlay($value);
                break;
            case 20:
                $this->setArtist($value);
                break;
            case 21:
                $this->setOriginalArtist($value);
                break;
            case 22:
                $this->setTitle($value);
                break;
            case 23:
                $this->setAlbum($value);
                break;
            case 24:
                $this->setComposer($value);
                break;
            case 25:
                $this->setYear($value);
                break;
            case 26:
                $this->setTrackNo($value);
                break;
            case 27:
                $this->setDiscNo($value);
                break;
            case 28:
                $this->setPublisher($value);
                break;
            case 29:
                $this->setCopyright($value);
                break;
            case 30:
                $this->setIsrc($value);
                break;
            case 31:
                $this->setBpm($value);
                break;
            case 32:
                $this->setComments($value);
                break;
            case 33:
                $this->setSweepers($value);
                break;
            case 34:
                $this->setAlbumArt($value);
                break;
            case 35:
                $this->setBuyLink($value);
                break;
            case 36:
                $this->setTdatePlayed($value);
                break;
            case 37:
                $this->setTartistPlayed($value);
                break;
            case 38:
                $this->setDateAdded($value);
                break;
            case 39:
                $this->setUpdatedAt($value);
                break;
            case 40:
                $this->setVersion($value);
                break;
            case 41:
                $this->setVersionCreatedAt($value);
                break;
            case 42:
                $this->setVersionCreatedBy($value);
                break;
            case 43:
                $this->setVersionComment($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SongsTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPath($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setEnabled($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDatePlayed($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setArtistPlayed($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCountPlayed($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPlayLimit($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setLimitAction($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setStartDate($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setEndDate($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setSongType($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setIdSubcat($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setIdGenre($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setWeight($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setDuration($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCueTimes($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setPreciseCue($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setFadeType($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setEndType($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setOverlay($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setArtist($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setOriginalArtist($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setTitle($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setAlbum($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setComposer($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setYear($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setTrackNo($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setDiscNo($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setPublisher($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setCopyright($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setIsrc($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setBpm($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setComments($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setSweepers($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setAlbumArt($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setBuyLink($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setTdatePlayed($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setTartistPlayed($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setDateAdded($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setUpdatedAt($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setVersion($arr[$keys[40]]);
        }
        if (array_key_exists($keys[41], $arr)) {
            $this->setVersionCreatedAt($arr[$keys[41]]);
        }
        if (array_key_exists($keys[42], $arr)) {
            $this->setVersionCreatedBy($arr[$keys[42]]);
        }
        if (array_key_exists($keys[43], $arr)) {
            $this->setVersionComment($arr[$keys[43]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Song\Songs The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SongsTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SongsTableMap::COL_ID)) {
            $criteria->add(SongsTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SongsTableMap::COL_PATH)) {
            $criteria->add(SongsTableMap::COL_PATH, $this->path);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ENABLED)) {
            $criteria->add(SongsTableMap::COL_ENABLED, $this->enabled);
        }
        if ($this->isColumnModified(SongsTableMap::COL_DATE_PLAYED)) {
            $criteria->add(SongsTableMap::COL_DATE_PLAYED, $this->date_played);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ARTIST_PLAYED)) {
            $criteria->add(SongsTableMap::COL_ARTIST_PLAYED, $this->artist_played);
        }
        if ($this->isColumnModified(SongsTableMap::COL_COUNT_PLAYED)) {
            $criteria->add(SongsTableMap::COL_COUNT_PLAYED, $this->count_played);
        }
        if ($this->isColumnModified(SongsTableMap::COL_PLAY_LIMIT)) {
            $criteria->add(SongsTableMap::COL_PLAY_LIMIT, $this->play_limit);
        }
        if ($this->isColumnModified(SongsTableMap::COL_LIMIT_ACTION)) {
            $criteria->add(SongsTableMap::COL_LIMIT_ACTION, $this->limit_action);
        }
        if ($this->isColumnModified(SongsTableMap::COL_START_DATE)) {
            $criteria->add(SongsTableMap::COL_START_DATE, $this->start_date);
        }
        if ($this->isColumnModified(SongsTableMap::COL_END_DATE)) {
            $criteria->add(SongsTableMap::COL_END_DATE, $this->end_date);
        }
        if ($this->isColumnModified(SongsTableMap::COL_SONG_TYPE)) {
            $criteria->add(SongsTableMap::COL_SONG_TYPE, $this->song_type);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ID_SUBCAT)) {
            $criteria->add(SongsTableMap::COL_ID_SUBCAT, $this->id_subcat);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ID_GENRE)) {
            $criteria->add(SongsTableMap::COL_ID_GENRE, $this->id_genre);
        }
        if ($this->isColumnModified(SongsTableMap::COL_WEIGHT)) {
            $criteria->add(SongsTableMap::COL_WEIGHT, $this->weight);
        }
        if ($this->isColumnModified(SongsTableMap::COL_DURATION)) {
            $criteria->add(SongsTableMap::COL_DURATION, $this->duration);
        }
        if ($this->isColumnModified(SongsTableMap::COL_CUE_TIMES)) {
            $criteria->add(SongsTableMap::COL_CUE_TIMES, $this->cue_times);
        }
        if ($this->isColumnModified(SongsTableMap::COL_PRECISE_CUE)) {
            $criteria->add(SongsTableMap::COL_PRECISE_CUE, $this->precise_cue);
        }
        if ($this->isColumnModified(SongsTableMap::COL_FADE_TYPE)) {
            $criteria->add(SongsTableMap::COL_FADE_TYPE, $this->fade_type);
        }
        if ($this->isColumnModified(SongsTableMap::COL_END_TYPE)) {
            $criteria->add(SongsTableMap::COL_END_TYPE, $this->end_type);
        }
        if ($this->isColumnModified(SongsTableMap::COL_OVERLAY)) {
            $criteria->add(SongsTableMap::COL_OVERLAY, $this->overlay);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ARTIST)) {
            $criteria->add(SongsTableMap::COL_ARTIST, $this->artist);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ORIGINAL_ARTIST)) {
            $criteria->add(SongsTableMap::COL_ORIGINAL_ARTIST, $this->original_artist);
        }
        if ($this->isColumnModified(SongsTableMap::COL_TITLE)) {
            $criteria->add(SongsTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ALBUM)) {
            $criteria->add(SongsTableMap::COL_ALBUM, $this->album);
        }
        if ($this->isColumnModified(SongsTableMap::COL_COMPOSER)) {
            $criteria->add(SongsTableMap::COL_COMPOSER, $this->composer);
        }
        if ($this->isColumnModified(SongsTableMap::COL_YEAR)) {
            $criteria->add(SongsTableMap::COL_YEAR, $this->year);
        }
        if ($this->isColumnModified(SongsTableMap::COL_TRACK_NO)) {
            $criteria->add(SongsTableMap::COL_TRACK_NO, $this->track_no);
        }
        if ($this->isColumnModified(SongsTableMap::COL_DISC_NO)) {
            $criteria->add(SongsTableMap::COL_DISC_NO, $this->disc_no);
        }
        if ($this->isColumnModified(SongsTableMap::COL_PUBLISHER)) {
            $criteria->add(SongsTableMap::COL_PUBLISHER, $this->publisher);
        }
        if ($this->isColumnModified(SongsTableMap::COL_COPYRIGHT)) {
            $criteria->add(SongsTableMap::COL_COPYRIGHT, $this->copyright);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ISRC)) {
            $criteria->add(SongsTableMap::COL_ISRC, $this->isrc);
        }
        if ($this->isColumnModified(SongsTableMap::COL_BPM)) {
            $criteria->add(SongsTableMap::COL_BPM, $this->bpm);
        }
        if ($this->isColumnModified(SongsTableMap::COL_COMMENTS)) {
            $criteria->add(SongsTableMap::COL_COMMENTS, $this->comments);
        }
        if ($this->isColumnModified(SongsTableMap::COL_SWEEPERS)) {
            $criteria->add(SongsTableMap::COL_SWEEPERS, $this->sweepers);
        }
        if ($this->isColumnModified(SongsTableMap::COL_ALBUM_ART)) {
            $criteria->add(SongsTableMap::COL_ALBUM_ART, $this->album_art);
        }
        if ($this->isColumnModified(SongsTableMap::COL_BUY_LINK)) {
            $criteria->add(SongsTableMap::COL_BUY_LINK, $this->buy_link);
        }
        if ($this->isColumnModified(SongsTableMap::COL_TDATE_PLAYED)) {
            $criteria->add(SongsTableMap::COL_TDATE_PLAYED, $this->tdate_played);
        }
        if ($this->isColumnModified(SongsTableMap::COL_TARTIST_PLAYED)) {
            $criteria->add(SongsTableMap::COL_TARTIST_PLAYED, $this->tartist_played);
        }
        if ($this->isColumnModified(SongsTableMap::COL_DATE_ADDED)) {
            $criteria->add(SongsTableMap::COL_DATE_ADDED, $this->date_added);
        }
        if ($this->isColumnModified(SongsTableMap::COL_UPDATED_AT)) {
            $criteria->add(SongsTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION)) {
            $criteria->add(SongsTableMap::COL_VERSION, $this->version);
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION_CREATED_AT)) {
            $criteria->add(SongsTableMap::COL_VERSION_CREATED_AT, $this->version_created_at);
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION_CREATED_BY)) {
            $criteria->add(SongsTableMap::COL_VERSION_CREATED_BY, $this->version_created_by);
        }
        if ($this->isColumnModified(SongsTableMap::COL_VERSION_COMMENT)) {
            $criteria->add(SongsTableMap::COL_VERSION_COMMENT, $this->version_comment);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSongsQuery::create();
        $criteria->add(SongsTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Song\Songs (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPath($this->getPath());
        $copyObj->setEnabled($this->getEnabled());
        $copyObj->setDatePlayed($this->getDatePlayed());
        $copyObj->setArtistPlayed($this->getArtistPlayed());
        $copyObj->setCountPlayed($this->getCountPlayed());
        $copyObj->setPlayLimit($this->getPlayLimit());
        $copyObj->setLimitAction($this->getLimitAction());
        $copyObj->setStartDate($this->getStartDate());
        $copyObj->setEndDate($this->getEndDate());
        $copyObj->setSongType($this->getSongType());
        $copyObj->setIdSubcat($this->getIdSubcat());
        $copyObj->setIdGenre($this->getIdGenre());
        $copyObj->setWeight($this->getWeight());
        $copyObj->setDuration($this->getDuration());
        $copyObj->setCueTimes($this->getCueTimes());
        $copyObj->setPreciseCue($this->getPreciseCue());
        $copyObj->setFadeType($this->getFadeType());
        $copyObj->setEndType($this->getEndType());
        $copyObj->setOverlay($this->getOverlay());
        $copyObj->setArtist($this->getArtist());
        $copyObj->setOriginalArtist($this->getOriginalArtist());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setAlbum($this->getAlbum());
        $copyObj->setComposer($this->getComposer());
        $copyObj->setYear($this->getYear());
        $copyObj->setTrackNo($this->getTrackNo());
        $copyObj->setDiscNo($this->getDiscNo());
        $copyObj->setPublisher($this->getPublisher());
        $copyObj->setCopyright($this->getCopyright());
        $copyObj->setIsrc($this->getIsrc());
        $copyObj->setBpm($this->getBpm());
        $copyObj->setComments($this->getComments());
        $copyObj->setSweepers($this->getSweepers());
        $copyObj->setAlbumArt($this->getAlbumArt());
        $copyObj->setBuyLink($this->getBuyLink());
        $copyObj->setTdatePlayed($this->getTdatePlayed());
        $copyObj->setTartistPlayed($this->getTartistPlayed());
        $copyObj->setDateAdded($this->getDateAdded());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setVersion($this->getVersion());
        $copyObj->setVersionCreatedAt($this->getVersionCreatedAt());
        $copyObj->setVersionCreatedBy($this->getVersionCreatedBy());
        $copyObj->setVersionComment($this->getVersionComment());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSongsVersions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSongsVersion($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Song\Songs Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SongsVersion' == $relationName) {
            $this->initSongsVersions();
            return;
        }
    }

    /**
     * Clears out the collSongsVersions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSongsVersions()
     */
    public function clearSongsVersions()
    {
        $this->collSongsVersions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSongsVersions collection loaded partially.
     */
    public function resetPartialSongsVersions($v = true)
    {
        $this->collSongsVersionsPartial = $v;
    }

    /**
     * Initializes the collSongsVersions collection.
     *
     * By default this just sets the collSongsVersions collection to an empty array (like clearcollSongsVersions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSongsVersions($overrideExisting = true)
    {
        if (null !== $this->collSongsVersions && !$overrideExisting) {
            return;
        }

        $collectionClassName = SongsVersionTableMap::getTableMap()->getCollectionClassName();

        $this->collSongsVersions = new $collectionClassName;
        $this->collSongsVersions->setModel('\Model\Song\SongsVersion');
    }

    /**
     * Gets an array of ChildSongsVersion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSongs is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSongsVersion[] List of ChildSongsVersion objects
     * @throws PropelException
     */
    public function getSongsVersions(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSongsVersionsPartial && !$this->isNew();
        if (null === $this->collSongsVersions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSongsVersions) {
                // return empty collection
                $this->initSongsVersions();
            } else {
                $collSongsVersions = ChildSongsVersionQuery::create(null, $criteria)
                    ->filterBySongs($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSongsVersionsPartial && count($collSongsVersions)) {
                        $this->initSongsVersions(false);

                        foreach ($collSongsVersions as $obj) {
                            if (false == $this->collSongsVersions->contains($obj)) {
                                $this->collSongsVersions->append($obj);
                            }
                        }

                        $this->collSongsVersionsPartial = true;
                    }

                    return $collSongsVersions;
                }

                if ($partial && $this->collSongsVersions) {
                    foreach ($this->collSongsVersions as $obj) {
                        if ($obj->isNew()) {
                            $collSongsVersions[] = $obj;
                        }
                    }
                }

                $this->collSongsVersions = $collSongsVersions;
                $this->collSongsVersionsPartial = false;
            }
        }

        return $this->collSongsVersions;
    }

    /**
     * Sets a collection of ChildSongsVersion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $songsVersions A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSongs The current object (for fluent API support)
     */
    public function setSongsVersions(Collection $songsVersions, ConnectionInterface $con = null)
    {
        /** @var ChildSongsVersion[] $songsVersionsToDelete */
        $songsVersionsToDelete = $this->getSongsVersions(new Criteria(), $con)->diff($songsVersions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->songsVersionsScheduledForDeletion = clone $songsVersionsToDelete;

        foreach ($songsVersionsToDelete as $songsVersionRemoved) {
            $songsVersionRemoved->setSongs(null);
        }

        $this->collSongsVersions = null;
        foreach ($songsVersions as $songsVersion) {
            $this->addSongsVersion($songsVersion);
        }

        $this->collSongsVersions = $songsVersions;
        $this->collSongsVersionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SongsVersion objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SongsVersion objects.
     * @throws PropelException
     */
    public function countSongsVersions(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSongsVersionsPartial && !$this->isNew();
        if (null === $this->collSongsVersions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSongsVersions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSongsVersions());
            }

            $query = ChildSongsVersionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySongs($this)
                ->count($con);
        }

        return count($this->collSongsVersions);
    }

    /**
     * Method called to associate a ChildSongsVersion object to this object
     * through the ChildSongsVersion foreign key attribute.
     *
     * @param  ChildSongsVersion $l ChildSongsVersion
     * @return $this|\Model\Song\Songs The current object (for fluent API support)
     */
    public function addSongsVersion(ChildSongsVersion $l)
    {
        if ($this->collSongsVersions === null) {
            $this->initSongsVersions();
            $this->collSongsVersionsPartial = true;
        }

        if (!$this->collSongsVersions->contains($l)) {
            $this->doAddSongsVersion($l);

            if ($this->songsVersionsScheduledForDeletion and $this->songsVersionsScheduledForDeletion->contains($l)) {
                $this->songsVersionsScheduledForDeletion->remove($this->songsVersionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSongsVersion $songsVersion The ChildSongsVersion object to add.
     */
    protected function doAddSongsVersion(ChildSongsVersion $songsVersion)
    {
        $this->collSongsVersions[]= $songsVersion;
        $songsVersion->setSongs($this);
    }

    /**
     * @param  ChildSongsVersion $songsVersion The ChildSongsVersion object to remove.
     * @return $this|ChildSongs The current object (for fluent API support)
     */
    public function removeSongsVersion(ChildSongsVersion $songsVersion)
    {
        if ($this->getSongsVersions()->contains($songsVersion)) {
            $pos = $this->collSongsVersions->search($songsVersion);
            $this->collSongsVersions->remove($pos);
            if (null === $this->songsVersionsScheduledForDeletion) {
                $this->songsVersionsScheduledForDeletion = clone $this->collSongsVersions;
                $this->songsVersionsScheduledForDeletion->clear();
            }
            $this->songsVersionsScheduledForDeletion[]= clone $songsVersion;
            $songsVersion->setSongs(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->path = null;
        $this->enabled = null;
        $this->date_played = null;
        $this->artist_played = null;
        $this->count_played = null;
        $this->play_limit = null;
        $this->limit_action = null;
        $this->start_date = null;
        $this->end_date = null;
        $this->song_type = null;
        $this->id_subcat = null;
        $this->id_genre = null;
        $this->weight = null;
        $this->duration = null;
        $this->cue_times = null;
        $this->precise_cue = null;
        $this->fade_type = null;
        $this->end_type = null;
        $this->overlay = null;
        $this->artist = null;
        $this->original_artist = null;
        $this->title = null;
        $this->album = null;
        $this->composer = null;
        $this->year = null;
        $this->track_no = null;
        $this->disc_no = null;
        $this->publisher = null;
        $this->copyright = null;
        $this->isrc = null;
        $this->bpm = null;
        $this->comments = null;
        $this->sweepers = null;
        $this->album_art = null;
        $this->buy_link = null;
        $this->tdate_played = null;
        $this->tartist_played = null;
        $this->date_added = null;
        $this->updated_at = null;
        $this->version = null;
        $this->version_created_at = null;
        $this->version_created_by = null;
        $this->version_comment = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSongsVersions) {
                foreach ($this->collSongsVersions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSongsVersions = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SongsTableMap::DEFAULT_STRING_FORMAT);
    }

    // versionable behavior

    /**
     * Enforce a new Version of this object upon next save.
     *
     * @return $this|\Model\Song\Songs
     */
    public function enforceVersioning()
    {
        $this->enforceVersion = true;

        return $this;
    }

    /**
     * Checks whether the current state must be recorded as a version
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     * @return  boolean
     */
    public function isVersioningNecessary(ConnectionInterface $con = null)
    {
        if ($this->alreadyInSave) {
            return false;
        }

        if ($this->enforceVersion) {
            return true;
        }

        if (ChildSongsQuery::isVersioningEnabled() && ($this->isNew() || $this->isModified()) || $this->isDeleted()) {
            return true;
        }

        return false;
    }

    /**
     * Creates a version of the current object and saves it.
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  ChildSongsVersion A version object
     */
    public function addVersion(ConnectionInterface $con = null)
    {
        $this->enforceVersion = false;

        $version = new ChildSongsVersion();
        $version->setId($this->getId());
        $version->setPath($this->getPath());
        $version->setEnabled($this->getEnabled());
        $version->setDatePlayed($this->getDatePlayed());
        $version->setArtistPlayed($this->getArtistPlayed());
        $version->setCountPlayed($this->getCountPlayed());
        $version->setPlayLimit($this->getPlayLimit());
        $version->setLimitAction($this->getLimitAction());
        $version->setStartDate($this->getStartDate());
        $version->setEndDate($this->getEndDate());
        $version->setSongType($this->getSongType());
        $version->setIdSubcat($this->getIdSubcat());
        $version->setIdGenre($this->getIdGenre());
        $version->setWeight($this->getWeight());
        $version->setDuration($this->getDuration());
        $version->setCueTimes($this->getCueTimes());
        $version->setPreciseCue($this->getPreciseCue());
        $version->setFadeType($this->getFadeType());
        $version->setEndType($this->getEndType());
        $version->setOverlay($this->getOverlay());
        $version->setArtist($this->getArtist());
        $version->setOriginalArtist($this->getOriginalArtist());
        $version->setTitle($this->getTitle());
        $version->setAlbum($this->getAlbum());
        $version->setComposer($this->getComposer());
        $version->setYear($this->getYear());
        $version->setTrackNo($this->getTrackNo());
        $version->setDiscNo($this->getDiscNo());
        $version->setPublisher($this->getPublisher());
        $version->setCopyright($this->getCopyright());
        $version->setIsrc($this->getIsrc());
        $version->setBpm($this->getBpm());
        $version->setComments($this->getComments());
        $version->setSweepers($this->getSweepers());
        $version->setAlbumArt($this->getAlbumArt());
        $version->setBuyLink($this->getBuyLink());
        $version->setTdatePlayed($this->getTdatePlayed());
        $version->setTartistPlayed($this->getTartistPlayed());
        $version->setDateAdded($this->getDateAdded());
        $version->setUpdatedAt($this->getUpdatedAt());
        $version->setVersion($this->getVersion());
        $version->setVersionCreatedAt($this->getVersionCreatedAt());
        $version->setVersionCreatedBy($this->getVersionCreatedBy());
        $version->setVersionComment($this->getVersionComment());
        $version->setSongs($this);
        $version->save($con);

        return $version;
    }

    /**
     * Sets the properties of the current object to the value they had at a specific version
     *
     * @param   integer $versionNumber The version number to read
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  $this|ChildSongs The current object (for fluent API support)
     */
    public function toVersion($versionNumber, ConnectionInterface $con = null)
    {
        $version = $this->getOneVersion($versionNumber, $con);
        if (!$version) {
            throw new PropelException(sprintf('No ChildSongs object found with version %d', $version));
        }
        $this->populateFromVersion($version, $con);

        return $this;
    }

    /**
     * Sets the properties of the current object to the value they had at a specific version
     *
     * @param ChildSongsVersion $version The version object to use
     * @param ConnectionInterface   $con the connection to use
     * @param array                 $loadedObjects objects that been loaded in a chain of populateFromVersion calls on referrer or fk objects.
     *
     * @return $this|ChildSongs The current object (for fluent API support)
     */
    public function populateFromVersion($version, $con = null, &$loadedObjects = array())
    {
        $loadedObjects['ChildSongs'][$version->getId()][$version->getVersion()] = $this;
        $this->setId($version->getId());
        $this->setPath($version->getPath());
        $this->setEnabled($version->getEnabled());
        $this->setDatePlayed($version->getDatePlayed());
        $this->setArtistPlayed($version->getArtistPlayed());
        $this->setCountPlayed($version->getCountPlayed());
        $this->setPlayLimit($version->getPlayLimit());
        $this->setLimitAction($version->getLimitAction());
        $this->setStartDate($version->getStartDate());
        $this->setEndDate($version->getEndDate());
        $this->setSongType($version->getSongType());
        $this->setIdSubcat($version->getIdSubcat());
        $this->setIdGenre($version->getIdGenre());
        $this->setWeight($version->getWeight());
        $this->setDuration($version->getDuration());
        $this->setCueTimes($version->getCueTimes());
        $this->setPreciseCue($version->getPreciseCue());
        $this->setFadeType($version->getFadeType());
        $this->setEndType($version->getEndType());
        $this->setOverlay($version->getOverlay());
        $this->setArtist($version->getArtist());
        $this->setOriginalArtist($version->getOriginalArtist());
        $this->setTitle($version->getTitle());
        $this->setAlbum($version->getAlbum());
        $this->setComposer($version->getComposer());
        $this->setYear($version->getYear());
        $this->setTrackNo($version->getTrackNo());
        $this->setDiscNo($version->getDiscNo());
        $this->setPublisher($version->getPublisher());
        $this->setCopyright($version->getCopyright());
        $this->setIsrc($version->getIsrc());
        $this->setBpm($version->getBpm());
        $this->setComments($version->getComments());
        $this->setSweepers($version->getSweepers());
        $this->setAlbumArt($version->getAlbumArt());
        $this->setBuyLink($version->getBuyLink());
        $this->setTdatePlayed($version->getTdatePlayed());
        $this->setTartistPlayed($version->getTartistPlayed());
        $this->setDateAdded($version->getDateAdded());
        $this->setUpdatedAt($version->getUpdatedAt());
        $this->setVersion($version->getVersion());
        $this->setVersionCreatedAt($version->getVersionCreatedAt());
        $this->setVersionCreatedBy($version->getVersionCreatedBy());
        $this->setVersionComment($version->getVersionComment());

        return $this;
    }

    /**
     * Gets the latest persisted version number for the current object
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  integer
     */
    public function getLastVersionNumber(ConnectionInterface $con = null)
    {
        $v = ChildSongsVersionQuery::create()
            ->filterBySongs($this)
            ->orderByVersion('desc')
            ->findOne($con);
        if (!$v) {
            return 0;
        }

        return $v->getVersion();
    }

    /**
     * Checks whether the current object is the latest one
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  Boolean
     */
    public function isLastVersion(ConnectionInterface $con = null)
    {
        return $this->getLastVersionNumber($con) == $this->getVersion();
    }

    /**
     * Retrieves a version object for this entity and a version number
     *
     * @param   integer $versionNumber The version number to read
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  ChildSongsVersion A version object
     */
    public function getOneVersion($versionNumber, ConnectionInterface $con = null)
    {
        return ChildSongsVersionQuery::create()
            ->filterBySongs($this)
            ->filterByVersion($versionNumber)
            ->findOne($con);
    }

    /**
     * Gets all the versions of this object, in incremental order
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  ObjectCollection|ChildSongsVersion[] A list of ChildSongsVersion objects
     */
    public function getAllVersions(ConnectionInterface $con = null)
    {
        $criteria = new Criteria();
        $criteria->addAscendingOrderByColumn(SongsVersionTableMap::COL_VERSION);

        return $this->getSongsVersions($criteria, $con);
    }

    /**
     * Compares the current object with another of its version.
     * <code>
     * print_r($book->compareVersion(1));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param   integer             $versionNumber
     * @param   string              $keys Main key used for the result diff (versions|columns)
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     * @param   array               $ignoredColumns  The columns to exclude from the diff.
     *
     * @return  array A list of differences
     */
    public function compareVersion($versionNumber, $keys = 'columns', ConnectionInterface $con = null, $ignoredColumns = array())
    {
        $fromVersion = $this->toArray();
        $toVersion = $this->getOneVersion($versionNumber, $con)->toArray();

        return $this->computeDiff($fromVersion, $toVersion, $keys, $ignoredColumns);
    }

    /**
     * Compares two versions of the current object.
     * <code>
     * print_r($book->compareVersions(1, 2));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param   integer             $fromVersionNumber
     * @param   integer             $toVersionNumber
     * @param   string              $keys Main key used for the result diff (versions|columns)
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     * @param   array               $ignoredColumns  The columns to exclude from the diff.
     *
     * @return  array A list of differences
     */
    public function compareVersions($fromVersionNumber, $toVersionNumber, $keys = 'columns', ConnectionInterface $con = null, $ignoredColumns = array())
    {
        $fromVersion = $this->getOneVersion($fromVersionNumber, $con)->toArray();
        $toVersion = $this->getOneVersion($toVersionNumber, $con)->toArray();

        return $this->computeDiff($fromVersion, $toVersion, $keys, $ignoredColumns);
    }

    /**
     * Computes the diff between two versions.
     * <code>
     * print_r($book->computeDiff(1, 2));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param   array     $fromVersion     An array representing the original version.
     * @param   array     $toVersion       An array representing the destination version.
     * @param   string    $keys            Main key used for the result diff (versions|columns).
     * @param   array     $ignoredColumns  The columns to exclude from the diff.
     *
     * @return  array A list of differences
     */
    protected function computeDiff($fromVersion, $toVersion, $keys = 'columns', $ignoredColumns = array())
    {
        $fromVersionNumber = $fromVersion['Version'];
        $toVersionNumber = $toVersion['Version'];
        $ignoredColumns = array_merge(array(
            'Version',
            'VersionCreatedAt',
            'VersionCreatedBy',
            'VersionComment',
        ), $ignoredColumns);
        $diff = array();
        foreach ($fromVersion as $key => $value) {
            if (in_array($key, $ignoredColumns)) {
                continue;
            }
            if ($toVersion[$key] != $value) {
                switch ($keys) {
                    case 'versions':
                        $diff[$fromVersionNumber][$key] = $value;
                        $diff[$toVersionNumber][$key] = $toVersion[$key];
                        break;
                    default:
                        $diff[$key] = array(
                            $fromVersionNumber => $value,
                            $toVersionNumber => $toVersion[$key],
                        );
                        break;
                }
            }
        }

        return $diff;
    }
    /**
     * retrieve the last $number versions.
     *
     * @param  Integer             $number The number of record to return.
     * @param  Criteria            $criteria The Criteria object containing modified values.
     * @param  ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return PropelCollection|\Model\Song\SongsVersion[] List of \Model\Song\SongsVersion objects
     */
    public function getLastVersions($number = 10, $criteria = null, ConnectionInterface $con = null)
    {
        $criteria = ChildSongsVersionQuery::create(null, $criteria);
        $criteria->addDescendingOrderByColumn(SongsVersionTableMap::COL_VERSION);
        $criteria->limit($number);

        return $this->getSongsVersions($criteria, $con);
    }
    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
