<?php

namespace Model\Song\Base;

use \Exception;
use \PDO;
use Model\Song\SongsInformation as ChildSongsInformation;
use Model\Song\SongsInformationQuery as ChildSongsInformationQuery;
use Model\Song\Map\SongsInformationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'songs_information' table.
 *
 *
 *
 * @method     ChildSongsInformationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSongsInformationQuery orderBySongId($order = Criteria::ASC) Order by the song_id column
 * @method     ChildSongsInformationQuery orderByArtist($order = Criteria::ASC) Order by the artist column
 * @method     ChildSongsInformationQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSongsInformationQuery orderByRemix($order = Criteria::ASC) Order by the remix column
 * @method     ChildSongsInformationQuery orderByLabel($order = Criteria::ASC) Order by the label column
 * @method     ChildSongsInformationQuery orderByGenre($order = Criteria::ASC) Order by the genre column
 * @method     ChildSongsInformationQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildSongsInformationQuery orderByBeatport($order = Criteria::ASC) Order by the beatport column
 * @method     ChildSongsInformationQuery orderBySoundcloud($order = Criteria::ASC) Order by the soundcloud column
 * @method     ChildSongsInformationQuery orderByYoutube($order = Criteria::ASC) Order by the youtube column
 * @method     ChildSongsInformationQuery orderByItunes($order = Criteria::ASC) Order by the itunes column
 * @method     ChildSongsInformationQuery orderByCover($order = Criteria::ASC) Order by the cover column
 * @method     ChildSongsInformationQuery orderByAddedBy($order = Criteria::ASC) Order by the added_by column
 * @method     ChildSongsInformationQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildSongsInformationQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildSongsInformationQuery orderByStart($order = Criteria::ASC) Order by the start column
 * @method     ChildSongsInformationQuery orderByEnd($order = Criteria::ASC) Order by the end column
 * @method     ChildSongsInformationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildSongsInformationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildSongsInformationQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildSongsInformationQuery orderByVersion($order = Criteria::ASC) Order by the version column
 * @method     ChildSongsInformationQuery orderByVersionCreatedAt($order = Criteria::ASC) Order by the version_created_at column
 * @method     ChildSongsInformationQuery orderByVersionCreatedBy($order = Criteria::ASC) Order by the version_created_by column
 * @method     ChildSongsInformationQuery orderByVersionComment($order = Criteria::ASC) Order by the version_comment column
 *
 * @method     ChildSongsInformationQuery groupById() Group by the id column
 * @method     ChildSongsInformationQuery groupBySongId() Group by the song_id column
 * @method     ChildSongsInformationQuery groupByArtist() Group by the artist column
 * @method     ChildSongsInformationQuery groupByTitle() Group by the title column
 * @method     ChildSongsInformationQuery groupByRemix() Group by the remix column
 * @method     ChildSongsInformationQuery groupByLabel() Group by the label column
 * @method     ChildSongsInformationQuery groupByGenre() Group by the genre column
 * @method     ChildSongsInformationQuery groupByUrl() Group by the url column
 * @method     ChildSongsInformationQuery groupByBeatport() Group by the beatport column
 * @method     ChildSongsInformationQuery groupBySoundcloud() Group by the soundcloud column
 * @method     ChildSongsInformationQuery groupByYoutube() Group by the youtube column
 * @method     ChildSongsInformationQuery groupByItunes() Group by the itunes column
 * @method     ChildSongsInformationQuery groupByCover() Group by the cover column
 * @method     ChildSongsInformationQuery groupByAddedBy() Group by the added_by column
 * @method     ChildSongsInformationQuery groupByUserId() Group by the user_id column
 * @method     ChildSongsInformationQuery groupByStatus() Group by the status column
 * @method     ChildSongsInformationQuery groupByStart() Group by the start column
 * @method     ChildSongsInformationQuery groupByEnd() Group by the end column
 * @method     ChildSongsInformationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildSongsInformationQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildSongsInformationQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildSongsInformationQuery groupByVersion() Group by the version column
 * @method     ChildSongsInformationQuery groupByVersionCreatedAt() Group by the version_created_at column
 * @method     ChildSongsInformationQuery groupByVersionCreatedBy() Group by the version_created_by column
 * @method     ChildSongsInformationQuery groupByVersionComment() Group by the version_comment column
 *
 * @method     ChildSongsInformationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSongsInformationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSongsInformationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSongsInformationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSongsInformationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSongsInformationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSongsInformationQuery leftJoinSongsInformationVersion($relationAlias = null) Adds a LEFT JOIN clause to the query using the SongsInformationVersion relation
 * @method     ChildSongsInformationQuery rightJoinSongsInformationVersion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SongsInformationVersion relation
 * @method     ChildSongsInformationQuery innerJoinSongsInformationVersion($relationAlias = null) Adds a INNER JOIN clause to the query using the SongsInformationVersion relation
 *
 * @method     ChildSongsInformationQuery joinWithSongsInformationVersion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SongsInformationVersion relation
 *
 * @method     ChildSongsInformationQuery leftJoinWithSongsInformationVersion() Adds a LEFT JOIN clause and with to the query using the SongsInformationVersion relation
 * @method     ChildSongsInformationQuery rightJoinWithSongsInformationVersion() Adds a RIGHT JOIN clause and with to the query using the SongsInformationVersion relation
 * @method     ChildSongsInformationQuery innerJoinWithSongsInformationVersion() Adds a INNER JOIN clause and with to the query using the SongsInformationVersion relation
 *
 * @method     \Model\Song\SongsInformationVersionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSongsInformation findOne(ConnectionInterface $con = null) Return the first ChildSongsInformation matching the query
 * @method     ChildSongsInformation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSongsInformation matching the query, or a new ChildSongsInformation object populated from the query conditions when no match is found
 *
 * @method     ChildSongsInformation findOneById(int $id) Return the first ChildSongsInformation filtered by the id column
 * @method     ChildSongsInformation findOneBySongId(int $song_id) Return the first ChildSongsInformation filtered by the song_id column
 * @method     ChildSongsInformation findOneByArtist(string $artist) Return the first ChildSongsInformation filtered by the artist column
 * @method     ChildSongsInformation findOneByTitle(string $title) Return the first ChildSongsInformation filtered by the title column
 * @method     ChildSongsInformation findOneByRemix(string $remix) Return the first ChildSongsInformation filtered by the remix column
 * @method     ChildSongsInformation findOneByLabel(string $label) Return the first ChildSongsInformation filtered by the label column
 * @method     ChildSongsInformation findOneByGenre(int $genre) Return the first ChildSongsInformation filtered by the genre column
 * @method     ChildSongsInformation findOneByUrl(string $url) Return the first ChildSongsInformation filtered by the url column
 * @method     ChildSongsInformation findOneByBeatport(string $beatport) Return the first ChildSongsInformation filtered by the beatport column
 * @method     ChildSongsInformation findOneBySoundcloud(string $soundcloud) Return the first ChildSongsInformation filtered by the soundcloud column
 * @method     ChildSongsInformation findOneByYoutube(string $youtube) Return the first ChildSongsInformation filtered by the youtube column
 * @method     ChildSongsInformation findOneByItunes(string $itunes) Return the first ChildSongsInformation filtered by the itunes column
 * @method     ChildSongsInformation findOneByCover(string $cover) Return the first ChildSongsInformation filtered by the cover column
 * @method     ChildSongsInformation findOneByAddedBy(int $added_by) Return the first ChildSongsInformation filtered by the added_by column
 * @method     ChildSongsInformation findOneByUserId(int $user_id) Return the first ChildSongsInformation filtered by the user_id column
 * @method     ChildSongsInformation findOneByStatus(int $status) Return the first ChildSongsInformation filtered by the status column
 * @method     ChildSongsInformation findOneByStart(string $start) Return the first ChildSongsInformation filtered by the start column
 * @method     ChildSongsInformation findOneByEnd(string $end) Return the first ChildSongsInformation filtered by the end column
 * @method     ChildSongsInformation findOneByCreatedAt(string $created_at) Return the first ChildSongsInformation filtered by the created_at column
 * @method     ChildSongsInformation findOneByUpdatedAt(string $updated_at) Return the first ChildSongsInformation filtered by the updated_at column
 * @method     ChildSongsInformation findOneByDeletedAt(string $deleted_at) Return the first ChildSongsInformation filtered by the deleted_at column
 * @method     ChildSongsInformation findOneByVersion(int $version) Return the first ChildSongsInformation filtered by the version column
 * @method     ChildSongsInformation findOneByVersionCreatedAt(string $version_created_at) Return the first ChildSongsInformation filtered by the version_created_at column
 * @method     ChildSongsInformation findOneByVersionCreatedBy(string $version_created_by) Return the first ChildSongsInformation filtered by the version_created_by column
 * @method     ChildSongsInformation findOneByVersionComment(string $version_comment) Return the first ChildSongsInformation filtered by the version_comment column *

 * @method     ChildSongsInformation requirePk($key, ConnectionInterface $con = null) Return the ChildSongsInformation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOne(ConnectionInterface $con = null) Return the first ChildSongsInformation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSongsInformation requireOneById(int $id) Return the first ChildSongsInformation filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneBySongId(int $song_id) Return the first ChildSongsInformation filtered by the song_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByArtist(string $artist) Return the first ChildSongsInformation filtered by the artist column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByTitle(string $title) Return the first ChildSongsInformation filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByRemix(string $remix) Return the first ChildSongsInformation filtered by the remix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByLabel(string $label) Return the first ChildSongsInformation filtered by the label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByGenre(int $genre) Return the first ChildSongsInformation filtered by the genre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByUrl(string $url) Return the first ChildSongsInformation filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByBeatport(string $beatport) Return the first ChildSongsInformation filtered by the beatport column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneBySoundcloud(string $soundcloud) Return the first ChildSongsInformation filtered by the soundcloud column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByYoutube(string $youtube) Return the first ChildSongsInformation filtered by the youtube column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByItunes(string $itunes) Return the first ChildSongsInformation filtered by the itunes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByCover(string $cover) Return the first ChildSongsInformation filtered by the cover column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByAddedBy(int $added_by) Return the first ChildSongsInformation filtered by the added_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByUserId(int $user_id) Return the first ChildSongsInformation filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByStatus(int $status) Return the first ChildSongsInformation filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByStart(string $start) Return the first ChildSongsInformation filtered by the start column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByEnd(string $end) Return the first ChildSongsInformation filtered by the end column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByCreatedAt(string $created_at) Return the first ChildSongsInformation filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByUpdatedAt(string $updated_at) Return the first ChildSongsInformation filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByDeletedAt(string $deleted_at) Return the first ChildSongsInformation filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByVersion(int $version) Return the first ChildSongsInformation filtered by the version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByVersionCreatedAt(string $version_created_at) Return the first ChildSongsInformation filtered by the version_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByVersionCreatedBy(string $version_created_by) Return the first ChildSongsInformation filtered by the version_created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongsInformation requireOneByVersionComment(string $version_comment) Return the first ChildSongsInformation filtered by the version_comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSongsInformation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSongsInformation objects based on current ModelCriteria
 * @method     ChildSongsInformation[]|ObjectCollection findById(int $id) Return ChildSongsInformation objects filtered by the id column
 * @method     ChildSongsInformation[]|ObjectCollection findBySongId(int $song_id) Return ChildSongsInformation objects filtered by the song_id column
 * @method     ChildSongsInformation[]|ObjectCollection findByArtist(string $artist) Return ChildSongsInformation objects filtered by the artist column
 * @method     ChildSongsInformation[]|ObjectCollection findByTitle(string $title) Return ChildSongsInformation objects filtered by the title column
 * @method     ChildSongsInformation[]|ObjectCollection findByRemix(string $remix) Return ChildSongsInformation objects filtered by the remix column
 * @method     ChildSongsInformation[]|ObjectCollection findByLabel(string $label) Return ChildSongsInformation objects filtered by the label column
 * @method     ChildSongsInformation[]|ObjectCollection findByGenre(int $genre) Return ChildSongsInformation objects filtered by the genre column
 * @method     ChildSongsInformation[]|ObjectCollection findByUrl(string $url) Return ChildSongsInformation objects filtered by the url column
 * @method     ChildSongsInformation[]|ObjectCollection findByBeatport(string $beatport) Return ChildSongsInformation objects filtered by the beatport column
 * @method     ChildSongsInformation[]|ObjectCollection findBySoundcloud(string $soundcloud) Return ChildSongsInformation objects filtered by the soundcloud column
 * @method     ChildSongsInformation[]|ObjectCollection findByYoutube(string $youtube) Return ChildSongsInformation objects filtered by the youtube column
 * @method     ChildSongsInformation[]|ObjectCollection findByItunes(string $itunes) Return ChildSongsInformation objects filtered by the itunes column
 * @method     ChildSongsInformation[]|ObjectCollection findByCover(string $cover) Return ChildSongsInformation objects filtered by the cover column
 * @method     ChildSongsInformation[]|ObjectCollection findByAddedBy(int $added_by) Return ChildSongsInformation objects filtered by the added_by column
 * @method     ChildSongsInformation[]|ObjectCollection findByUserId(int $user_id) Return ChildSongsInformation objects filtered by the user_id column
 * @method     ChildSongsInformation[]|ObjectCollection findByStatus(int $status) Return ChildSongsInformation objects filtered by the status column
 * @method     ChildSongsInformation[]|ObjectCollection findByStart(string $start) Return ChildSongsInformation objects filtered by the start column
 * @method     ChildSongsInformation[]|ObjectCollection findByEnd(string $end) Return ChildSongsInformation objects filtered by the end column
 * @method     ChildSongsInformation[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildSongsInformation objects filtered by the created_at column
 * @method     ChildSongsInformation[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildSongsInformation objects filtered by the updated_at column
 * @method     ChildSongsInformation[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildSongsInformation objects filtered by the deleted_at column
 * @method     ChildSongsInformation[]|ObjectCollection findByVersion(int $version) Return ChildSongsInformation objects filtered by the version column
 * @method     ChildSongsInformation[]|ObjectCollection findByVersionCreatedAt(string $version_created_at) Return ChildSongsInformation objects filtered by the version_created_at column
 * @method     ChildSongsInformation[]|ObjectCollection findByVersionCreatedBy(string $version_created_by) Return ChildSongsInformation objects filtered by the version_created_by column
 * @method     ChildSongsInformation[]|ObjectCollection findByVersionComment(string $version_comment) Return ChildSongsInformation objects filtered by the version_comment column
 * @method     ChildSongsInformation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SongsInformationQuery extends ModelCriteria
{

    // versionable behavior

    /**
     * Whether the versioning is enabled
     */
    static $isVersioningEnabled = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Song\Base\SongsInformationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio', $modelName = '\\Model\\Song\\SongsInformation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSongsInformationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSongsInformationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSongsInformationQuery) {
            return $criteria;
        }
        $query = new ChildSongsInformationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSongsInformation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SongsInformationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSongsInformation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, song_id, artist, title, remix, label, genre, url, beatport, soundcloud, youtube, itunes, cover, added_by, user_id, status, start, end, created_at, updated_at, deleted_at, version, version_created_at, version_created_by, version_comment FROM songs_information WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSongsInformation $obj */
            $obj = new ChildSongsInformation();
            $obj->hydrate($row);
            SongsInformationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSongsInformation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SongsInformationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SongsInformationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the song_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySongId(1234); // WHERE song_id = 1234
     * $query->filterBySongId(array(12, 34)); // WHERE song_id IN (12, 34)
     * $query->filterBySongId(array('min' => 12)); // WHERE song_id > 12
     * </code>
     *
     * @param     mixed $songId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterBySongId($songId = null, $comparison = null)
    {
        if (is_array($songId)) {
            $useMinMax = false;
            if (isset($songId['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_SONG_ID, $songId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($songId['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_SONG_ID, $songId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_SONG_ID, $songId, $comparison);
    }

    /**
     * Filter the query on the artist column
     *
     * Example usage:
     * <code>
     * $query->filterByArtist('fooValue');   // WHERE artist = 'fooValue'
     * $query->filterByArtist('%fooValue%', Criteria::LIKE); // WHERE artist LIKE '%fooValue%'
     * </code>
     *
     * @param     string $artist The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByArtist($artist = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($artist)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_ARTIST, $artist, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the remix column
     *
     * Example usage:
     * <code>
     * $query->filterByRemix('fooValue');   // WHERE remix = 'fooValue'
     * $query->filterByRemix('%fooValue%', Criteria::LIKE); // WHERE remix LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remix The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByRemix($remix = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remix)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_REMIX, $remix, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the genre column
     *
     * Example usage:
     * <code>
     * $query->filterByGenre(1234); // WHERE genre = 1234
     * $query->filterByGenre(array(12, 34)); // WHERE genre IN (12, 34)
     * $query->filterByGenre(array('min' => 12)); // WHERE genre > 12
     * </code>
     *
     * @param     mixed $genre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByGenre($genre = null, $comparison = null)
    {
        if (is_array($genre)) {
            $useMinMax = false;
            if (isset($genre['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_GENRE, $genre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($genre['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_GENRE, $genre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_GENRE, $genre, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the beatport column
     *
     * Example usage:
     * <code>
     * $query->filterByBeatport('fooValue');   // WHERE beatport = 'fooValue'
     * $query->filterByBeatport('%fooValue%', Criteria::LIKE); // WHERE beatport LIKE '%fooValue%'
     * </code>
     *
     * @param     string $beatport The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByBeatport($beatport = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($beatport)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_BEATPORT, $beatport, $comparison);
    }

    /**
     * Filter the query on the soundcloud column
     *
     * Example usage:
     * <code>
     * $query->filterBySoundcloud('fooValue');   // WHERE soundcloud = 'fooValue'
     * $query->filterBySoundcloud('%fooValue%', Criteria::LIKE); // WHERE soundcloud LIKE '%fooValue%'
     * </code>
     *
     * @param     string $soundcloud The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterBySoundcloud($soundcloud = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($soundcloud)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_SOUNDCLOUD, $soundcloud, $comparison);
    }

    /**
     * Filter the query on the youtube column
     *
     * Example usage:
     * <code>
     * $query->filterByYoutube('fooValue');   // WHERE youtube = 'fooValue'
     * $query->filterByYoutube('%fooValue%', Criteria::LIKE); // WHERE youtube LIKE '%fooValue%'
     * </code>
     *
     * @param     string $youtube The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByYoutube($youtube = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($youtube)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_YOUTUBE, $youtube, $comparison);
    }

    /**
     * Filter the query on the itunes column
     *
     * Example usage:
     * <code>
     * $query->filterByItunes('fooValue');   // WHERE itunes = 'fooValue'
     * $query->filterByItunes('%fooValue%', Criteria::LIKE); // WHERE itunes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $itunes The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByItunes($itunes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($itunes)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_ITUNES, $itunes, $comparison);
    }

    /**
     * Filter the query on the cover column
     *
     * Example usage:
     * <code>
     * $query->filterByCover('fooValue');   // WHERE cover = 'fooValue'
     * $query->filterByCover('%fooValue%', Criteria::LIKE); // WHERE cover LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cover The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByCover($cover = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cover)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_COVER, $cover, $comparison);
    }

    /**
     * Filter the query on the added_by column
     *
     * Example usage:
     * <code>
     * $query->filterByAddedBy(1234); // WHERE added_by = 1234
     * $query->filterByAddedBy(array(12, 34)); // WHERE added_by IN (12, 34)
     * $query->filterByAddedBy(array('min' => 12)); // WHERE added_by > 12
     * </code>
     *
     * @param     mixed $addedBy The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByAddedBy($addedBy = null, $comparison = null)
    {
        if (is_array($addedBy)) {
            $useMinMax = false;
            if (isset($addedBy['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_ADDED_BY, $addedBy['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addedBy['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_ADDED_BY, $addedBy['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_ADDED_BY, $addedBy, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the start column
     *
     * Example usage:
     * <code>
     * $query->filterByStart('2011-03-14'); // WHERE start = '2011-03-14'
     * $query->filterByStart('now'); // WHERE start = '2011-03-14'
     * $query->filterByStart(array('max' => 'yesterday')); // WHERE start > '2011-03-13'
     * </code>
     *
     * @param     mixed $start The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByStart($start = null, $comparison = null)
    {
        if (is_array($start)) {
            $useMinMax = false;
            if (isset($start['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_START, $start['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($start['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_START, $start['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_START, $start, $comparison);
    }

    /**
     * Filter the query on the end column
     *
     * Example usage:
     * <code>
     * $query->filterByEnd('2011-03-14'); // WHERE end = '2011-03-14'
     * $query->filterByEnd('now'); // WHERE end = '2011-03-14'
     * $query->filterByEnd(array('max' => 'yesterday')); // WHERE end > '2011-03-13'
     * </code>
     *
     * @param     mixed $end The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByEnd($end = null, $comparison = null)
    {
        if (is_array($end)) {
            $useMinMax = false;
            if (isset($end['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_END, $end['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($end['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_END, $end['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_END, $end, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the version column
     *
     * Example usage:
     * <code>
     * $query->filterByVersion(1234); // WHERE version = 1234
     * $query->filterByVersion(array(12, 34)); // WHERE version IN (12, 34)
     * $query->filterByVersion(array('min' => 12)); // WHERE version > 12
     * </code>
     *
     * @param     mixed $version The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByVersion($version = null, $comparison = null)
    {
        if (is_array($version)) {
            $useMinMax = false;
            if (isset($version['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_VERSION, $version['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($version['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_VERSION, $version['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_VERSION, $version, $comparison);
    }

    /**
     * Filter the query on the version_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByVersionCreatedAt('2011-03-14'); // WHERE version_created_at = '2011-03-14'
     * $query->filterByVersionCreatedAt('now'); // WHERE version_created_at = '2011-03-14'
     * $query->filterByVersionCreatedAt(array('max' => 'yesterday')); // WHERE version_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $versionCreatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByVersionCreatedAt($versionCreatedAt = null, $comparison = null)
    {
        if (is_array($versionCreatedAt)) {
            $useMinMax = false;
            if (isset($versionCreatedAt['min'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_VERSION_CREATED_AT, $versionCreatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($versionCreatedAt['max'])) {
                $this->addUsingAlias(SongsInformationTableMap::COL_VERSION_CREATED_AT, $versionCreatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_VERSION_CREATED_AT, $versionCreatedAt, $comparison);
    }

    /**
     * Filter the query on the version_created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByVersionCreatedBy('fooValue');   // WHERE version_created_by = 'fooValue'
     * $query->filterByVersionCreatedBy('%fooValue%', Criteria::LIKE); // WHERE version_created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versionCreatedBy The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByVersionCreatedBy($versionCreatedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versionCreatedBy)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_VERSION_CREATED_BY, $versionCreatedBy, $comparison);
    }

    /**
     * Filter the query on the version_comment column
     *
     * Example usage:
     * <code>
     * $query->filterByVersionComment('fooValue');   // WHERE version_comment = 'fooValue'
     * $query->filterByVersionComment('%fooValue%', Criteria::LIKE); // WHERE version_comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versionComment The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterByVersionComment($versionComment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versionComment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsInformationTableMap::COL_VERSION_COMMENT, $versionComment, $comparison);
    }

    /**
     * Filter the query by a related \Model\Song\SongsInformationVersion object
     *
     * @param \Model\Song\SongsInformationVersion|ObjectCollection $songsInformationVersion the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSongsInformationQuery The current query, for fluid interface
     */
    public function filterBySongsInformationVersion($songsInformationVersion, $comparison = null)
    {
        if ($songsInformationVersion instanceof \Model\Song\SongsInformationVersion) {
            return $this
                ->addUsingAlias(SongsInformationTableMap::COL_ID, $songsInformationVersion->getId(), $comparison);
        } elseif ($songsInformationVersion instanceof ObjectCollection) {
            return $this
                ->useSongsInformationVersionQuery()
                ->filterByPrimaryKeys($songsInformationVersion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySongsInformationVersion() only accepts arguments of type \Model\Song\SongsInformationVersion or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SongsInformationVersion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function joinSongsInformationVersion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SongsInformationVersion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SongsInformationVersion');
        }

        return $this;
    }

    /**
     * Use the SongsInformationVersion relation SongsInformationVersion object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Song\SongsInformationVersionQuery A secondary query class using the current class as primary query
     */
    public function useSongsInformationVersionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSongsInformationVersion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SongsInformationVersion', '\Model\Song\SongsInformationVersionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSongsInformation $songsInformation Object to remove from the list of results
     *
     * @return $this|ChildSongsInformationQuery The current query, for fluid interface
     */
    public function prune($songsInformation = null)
    {
        if ($songsInformation) {
            $this->addUsingAlias(SongsInformationTableMap::COL_ID, $songsInformation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the songs_information table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SongsInformationTableMap::clearInstancePool();
            SongsInformationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SongsInformationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SongsInformationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SongsInformationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // versionable behavior

    /**
     * Checks whether versioning is enabled
     *
     * @return boolean
     */
    static public function isVersioningEnabled()
    {
        return self::$isVersioningEnabled;
    }

    /**
     * Enables versioning
     */
    static public function enableVersioning()
    {
        self::$isVersioningEnabled = true;
    }

    /**
     * Disables versioning
     */
    static public function disableVersioning()
    {
        self::$isVersioningEnabled = false;
    }

} // SongsInformationQuery
