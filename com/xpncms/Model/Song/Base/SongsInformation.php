<?php

namespace Model\Song\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Song\SongsInformation as ChildSongsInformation;
use Model\Song\SongsInformationQuery as ChildSongsInformationQuery;
use Model\Song\SongsInformationVersion as ChildSongsInformationVersion;
use Model\Song\SongsInformationVersionQuery as ChildSongsInformationVersionQuery;
use Model\Song\Map\SongsInformationTableMap;
use Model\Song\Map\SongsInformationVersionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'songs_information' table.
 *
 *
 *
 * @package    propel.generator.Model.Song.Base
 */
abstract class SongsInformation implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Song\\Map\\SongsInformationTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the song_id field.
     *
     * @var        int
     */
    protected $song_id;

    /**
     * The value for the artist field.
     *
     * @var        string
     */
    protected $artist;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the remix field.
     *
     * @var        string
     */
    protected $remix;

    /**
     * The value for the label field.
     *
     * @var        string
     */
    protected $label;

    /**
     * The value for the genre field.
     *
     * @var        int
     */
    protected $genre;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the beatport field.
     *
     * @var        string
     */
    protected $beatport;

    /**
     * The value for the soundcloud field.
     *
     * @var        string
     */
    protected $soundcloud;

    /**
     * The value for the youtube field.
     *
     * @var        string
     */
    protected $youtube;

    /**
     * The value for the itunes field.
     *
     * @var        string
     */
    protected $itunes;

    /**
     * The value for the cover field.
     *
     * @var        string
     */
    protected $cover;

    /**
     * The value for the added_by field.
     *
     * @var        int
     */
    protected $added_by;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the status field.
     *
     * @var        int
     */
    protected $status;

    /**
     * The value for the start field.
     *
     * @var        DateTime
     */
    protected $start;

    /**
     * The value for the end field.
     *
     * @var        DateTime
     */
    protected $end;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the version field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $version;

    /**
     * The value for the version_created_at field.
     *
     * @var        DateTime
     */
    protected $version_created_at;

    /**
     * The value for the version_created_by field.
     *
     * @var        string
     */
    protected $version_created_by;

    /**
     * The value for the version_comment field.
     *
     * @var        string
     */
    protected $version_comment;

    /**
     * @var        ObjectCollection|ChildSongsInformationVersion[] Collection to store aggregation of ChildSongsInformationVersion objects.
     */
    protected $collSongsInformationVersions;
    protected $collSongsInformationVersionsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // versionable behavior


    /**
     * @var bool
     */
    protected $enforceVersion = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSongsInformationVersion[]
     */
    protected $songsInformationVersionsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->version = 0;
    }

    /**
     * Initializes internal state of Model\Song\Base\SongsInformation object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SongsInformation</code> instance.  If
     * <code>obj</code> is an instance of <code>SongsInformation</code>, delegates to
     * <code>equals(SongsInformation)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|SongsInformation The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [song_id] column value.
     *
     * @return int
     */
    public function getSongId()
    {
        return $this->song_id;
    }

    /**
     * Get the [artist] column value.
     *
     * @return string
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [remix] column value.
     *
     * @return string
     */
    public function getRemix()
    {
        return $this->remix;
    }

    /**
     * Get the [label] column value.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Get the [genre] column value.
     *
     * @return int
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [beatport] column value.
     *
     * @return string
     */
    public function getBeatport()
    {
        return $this->beatport;
    }

    /**
     * Get the [soundcloud] column value.
     *
     * @return string
     */
    public function getSoundcloud()
    {
        return $this->soundcloud;
    }

    /**
     * Get the [youtube] column value.
     *
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Get the [itunes] column value.
     *
     * @return string
     */
    public function getItunes()
    {
        return $this->itunes;
    }

    /**
     * Get the [cover] column value.
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Get the [added_by] column value.
     *
     * @return int
     */
    public function getAddedBy()
    {
        return $this->added_by;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [optionally formatted] temporal [start] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStart($format = NULL)
    {
        if ($format === null) {
            return $this->start;
        } else {
            return $this->start instanceof \DateTimeInterface ? $this->start->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [end] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEnd($format = NULL)
    {
        if ($format === null) {
            return $this->end;
        } else {
            return $this->end instanceof \DateTimeInterface ? $this->end->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [version] column value.
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Get the [optionally formatted] temporal [version_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getVersionCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->version_created_at;
        } else {
            return $this->version_created_at instanceof \DateTimeInterface ? $this->version_created_at->format($format) : null;
        }
    }

    /**
     * Get the [version_created_by] column value.
     *
     * @return string
     */
    public function getVersionCreatedBy()
    {
        return $this->version_created_by;
    }

    /**
     * Get the [version_comment] column value.
     *
     * @return string
     */
    public function getVersionComment()
    {
        return $this->version_comment;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [song_id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setSongId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->song_id !== $v) {
            $this->song_id = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_SONG_ID] = true;
        }

        return $this;
    } // setSongId()

    /**
     * Set the value of [artist] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setArtist($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->artist !== $v) {
            $this->artist = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_ARTIST] = true;
        }

        return $this;
    } // setArtist()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [remix] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setRemix($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remix !== $v) {
            $this->remix = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_REMIX] = true;
        }

        return $this;
    } // setRemix()

    /**
     * Set the value of [label] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setLabel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->label !== $v) {
            $this->label = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_LABEL] = true;
        }

        return $this;
    } // setLabel()

    /**
     * Set the value of [genre] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setGenre($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->genre !== $v) {
            $this->genre = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_GENRE] = true;
        }

        return $this;
    } // setGenre()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [beatport] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setBeatport($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->beatport !== $v) {
            $this->beatport = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_BEATPORT] = true;
        }

        return $this;
    } // setBeatport()

    /**
     * Set the value of [soundcloud] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setSoundcloud($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->soundcloud !== $v) {
            $this->soundcloud = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_SOUNDCLOUD] = true;
        }

        return $this;
    } // setSoundcloud()

    /**
     * Set the value of [youtube] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setYoutube($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->youtube !== $v) {
            $this->youtube = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_YOUTUBE] = true;
        }

        return $this;
    } // setYoutube()

    /**
     * Set the value of [itunes] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setItunes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->itunes !== $v) {
            $this->itunes = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_ITUNES] = true;
        }

        return $this;
    } // setItunes()

    /**
     * Set the value of [cover] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setCover($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cover !== $v) {
            $this->cover = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_COVER] = true;
        }

        return $this;
    } // setCover()

    /**
     * Set the value of [added_by] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setAddedBy($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->added_by !== $v) {
            $this->added_by = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_ADDED_BY] = true;
        }

        return $this;
    } // setAddedBy()

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_USER_ID] = true;
        }

        return $this;
    } // setUserId()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Sets the value of [start] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setStart($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->start !== null || $dt !== null) {
            if ($this->start === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->start->format("Y-m-d H:i:s.u")) {
                $this->start = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsInformationTableMap::COL_START] = true;
            }
        } // if either are not null

        return $this;
    } // setStart()

    /**
     * Sets the value of [end] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setEnd($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->end !== null || $dt !== null) {
            if ($this->end === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->end->format("Y-m-d H:i:s.u")) {
                $this->end = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsInformationTableMap::COL_END] = true;
            }
        } // if either are not null

        return $this;
    } // setEnd()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsInformationTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsInformationTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsInformationTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Set the value of [version] column.
     *
     * @param int $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setVersion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->version !== $v) {
            $this->version = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_VERSION] = true;
        }

        return $this;
    } // setVersion()

    /**
     * Sets the value of [version_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setVersionCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->version_created_at !== null || $dt !== null) {
            if ($this->version_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->version_created_at->format("Y-m-d H:i:s.u")) {
                $this->version_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SongsInformationTableMap::COL_VERSION_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setVersionCreatedAt()

    /**
     * Set the value of [version_created_by] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setVersionCreatedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->version_created_by !== $v) {
            $this->version_created_by = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_VERSION_CREATED_BY] = true;
        }

        return $this;
    } // setVersionCreatedBy()

    /**
     * Set the value of [version_comment] column.
     *
     * @param string $v new value
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function setVersionComment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->version_comment !== $v) {
            $this->version_comment = $v;
            $this->modifiedColumns[SongsInformationTableMap::COL_VERSION_COMMENT] = true;
        }

        return $this;
    } // setVersionComment()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->version !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SongsInformationTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SongsInformationTableMap::translateFieldName('SongId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->song_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SongsInformationTableMap::translateFieldName('Artist', TableMap::TYPE_PHPNAME, $indexType)];
            $this->artist = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SongsInformationTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SongsInformationTableMap::translateFieldName('Remix', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remix = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SongsInformationTableMap::translateFieldName('Label', TableMap::TYPE_PHPNAME, $indexType)];
            $this->label = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SongsInformationTableMap::translateFieldName('Genre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->genre = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SongsInformationTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SongsInformationTableMap::translateFieldName('Beatport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->beatport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SongsInformationTableMap::translateFieldName('Soundcloud', TableMap::TYPE_PHPNAME, $indexType)];
            $this->soundcloud = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SongsInformationTableMap::translateFieldName('Youtube', TableMap::TYPE_PHPNAME, $indexType)];
            $this->youtube = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SongsInformationTableMap::translateFieldName('Itunes', TableMap::TYPE_PHPNAME, $indexType)];
            $this->itunes = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SongsInformationTableMap::translateFieldName('Cover', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cover = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SongsInformationTableMap::translateFieldName('AddedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->added_by = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SongsInformationTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SongsInformationTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : SongsInformationTableMap::translateFieldName('Start', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->start = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : SongsInformationTableMap::translateFieldName('End', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->end = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : SongsInformationTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : SongsInformationTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : SongsInformationTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : SongsInformationTableMap::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)];
            $this->version = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : SongsInformationTableMap::translateFieldName('VersionCreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->version_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : SongsInformationTableMap::translateFieldName('VersionCreatedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->version_created_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : SongsInformationTableMap::translateFieldName('VersionComment', TableMap::TYPE_PHPNAME, $indexType)];
            $this->version_comment = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 25; // 25 = SongsInformationTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Song\\SongsInformation'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSongsInformationQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSongsInformationVersions = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SongsInformation::setDeleted()
     * @see SongsInformation::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSongsInformationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            // versionable behavior
            if ($this->isVersioningNecessary()) {
                $this->setVersion($this->isNew() ? 1 : $this->getLastVersionNumber($con) + 1);
                if (!$this->isColumnModified(SongsInformationTableMap::COL_VERSION_CREATED_AT)) {
                    $this->setVersionCreatedAt(time());
                }
                $createVersion = true; // for postSave hook
            }
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                // versionable behavior
                if (isset($createVersion)) {
                    $this->addVersion($con);
                }
                SongsInformationTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->songsInformationVersionsScheduledForDeletion !== null) {
                if (!$this->songsInformationVersionsScheduledForDeletion->isEmpty()) {
                    \Model\Song\SongsInformationVersionQuery::create()
                        ->filterByPrimaryKeys($this->songsInformationVersionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->songsInformationVersionsScheduledForDeletion = null;
                }
            }

            if ($this->collSongsInformationVersions !== null) {
                foreach ($this->collSongsInformationVersions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SongsInformationTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SongsInformationTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SongsInformationTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_SONG_ID)) {
            $modifiedColumns[':p' . $index++]  = 'song_id';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_ARTIST)) {
            $modifiedColumns[':p' . $index++]  = 'artist';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_REMIX)) {
            $modifiedColumns[':p' . $index++]  = 'remix';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_LABEL)) {
            $modifiedColumns[':p' . $index++]  = 'label';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_GENRE)) {
            $modifiedColumns[':p' . $index++]  = 'genre';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_BEATPORT)) {
            $modifiedColumns[':p' . $index++]  = 'beatport';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_SOUNDCLOUD)) {
            $modifiedColumns[':p' . $index++]  = 'soundcloud';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_YOUTUBE)) {
            $modifiedColumns[':p' . $index++]  = 'youtube';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_ITUNES)) {
            $modifiedColumns[':p' . $index++]  = 'itunes';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_COVER)) {
            $modifiedColumns[':p' . $index++]  = 'cover';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_ADDED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'added_by';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_START)) {
            $modifiedColumns[':p' . $index++]  = 'start';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_END)) {
            $modifiedColumns[':p' . $index++]  = 'end';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION)) {
            $modifiedColumns[':p' . $index++]  = 'version';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'version_created_at';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION_CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'version_created_by';
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION_COMMENT)) {
            $modifiedColumns[':p' . $index++]  = 'version_comment';
        }

        $sql = sprintf(
            'INSERT INTO songs_information (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'song_id':
                        $stmt->bindValue($identifier, $this->song_id, PDO::PARAM_INT);
                        break;
                    case 'artist':
                        $stmt->bindValue($identifier, $this->artist, PDO::PARAM_STR);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'remix':
                        $stmt->bindValue($identifier, $this->remix, PDO::PARAM_STR);
                        break;
                    case 'label':
                        $stmt->bindValue($identifier, $this->label, PDO::PARAM_STR);
                        break;
                    case 'genre':
                        $stmt->bindValue($identifier, $this->genre, PDO::PARAM_INT);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case 'beatport':
                        $stmt->bindValue($identifier, $this->beatport, PDO::PARAM_STR);
                        break;
                    case 'soundcloud':
                        $stmt->bindValue($identifier, $this->soundcloud, PDO::PARAM_STR);
                        break;
                    case 'youtube':
                        $stmt->bindValue($identifier, $this->youtube, PDO::PARAM_STR);
                        break;
                    case 'itunes':
                        $stmt->bindValue($identifier, $this->itunes, PDO::PARAM_STR);
                        break;
                    case 'cover':
                        $stmt->bindValue($identifier, $this->cover, PDO::PARAM_STR);
                        break;
                    case 'added_by':
                        $stmt->bindValue($identifier, $this->added_by, PDO::PARAM_INT);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case 'start':
                        $stmt->bindValue($identifier, $this->start ? $this->start->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'end':
                        $stmt->bindValue($identifier, $this->end ? $this->end->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'version':
                        $stmt->bindValue($identifier, $this->version, PDO::PARAM_INT);
                        break;
                    case 'version_created_at':
                        $stmt->bindValue($identifier, $this->version_created_at ? $this->version_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'version_created_by':
                        $stmt->bindValue($identifier, $this->version_created_by, PDO::PARAM_STR);
                        break;
                    case 'version_comment':
                        $stmt->bindValue($identifier, $this->version_comment, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SongsInformationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSongId();
                break;
            case 2:
                return $this->getArtist();
                break;
            case 3:
                return $this->getTitle();
                break;
            case 4:
                return $this->getRemix();
                break;
            case 5:
                return $this->getLabel();
                break;
            case 6:
                return $this->getGenre();
                break;
            case 7:
                return $this->getUrl();
                break;
            case 8:
                return $this->getBeatport();
                break;
            case 9:
                return $this->getSoundcloud();
                break;
            case 10:
                return $this->getYoutube();
                break;
            case 11:
                return $this->getItunes();
                break;
            case 12:
                return $this->getCover();
                break;
            case 13:
                return $this->getAddedBy();
                break;
            case 14:
                return $this->getUserId();
                break;
            case 15:
                return $this->getStatus();
                break;
            case 16:
                return $this->getStart();
                break;
            case 17:
                return $this->getEnd();
                break;
            case 18:
                return $this->getCreatedAt();
                break;
            case 19:
                return $this->getUpdatedAt();
                break;
            case 20:
                return $this->getDeletedAt();
                break;
            case 21:
                return $this->getVersion();
                break;
            case 22:
                return $this->getVersionCreatedAt();
                break;
            case 23:
                return $this->getVersionCreatedBy();
                break;
            case 24:
                return $this->getVersionComment();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SongsInformation'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SongsInformation'][$this->hashCode()] = true;
        $keys = SongsInformationTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSongId(),
            $keys[2] => $this->getArtist(),
            $keys[3] => $this->getTitle(),
            $keys[4] => $this->getRemix(),
            $keys[5] => $this->getLabel(),
            $keys[6] => $this->getGenre(),
            $keys[7] => $this->getUrl(),
            $keys[8] => $this->getBeatport(),
            $keys[9] => $this->getSoundcloud(),
            $keys[10] => $this->getYoutube(),
            $keys[11] => $this->getItunes(),
            $keys[12] => $this->getCover(),
            $keys[13] => $this->getAddedBy(),
            $keys[14] => $this->getUserId(),
            $keys[15] => $this->getStatus(),
            $keys[16] => $this->getStart(),
            $keys[17] => $this->getEnd(),
            $keys[18] => $this->getCreatedAt(),
            $keys[19] => $this->getUpdatedAt(),
            $keys[20] => $this->getDeletedAt(),
            $keys[21] => $this->getVersion(),
            $keys[22] => $this->getVersionCreatedAt(),
            $keys[23] => $this->getVersionCreatedBy(),
            $keys[24] => $this->getVersionComment(),
        );
        if ($result[$keys[16]] instanceof \DateTimeInterface) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTimeInterface) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTimeInterface) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        if ($result[$keys[22]] instanceof \DateTimeInterface) {
            $result[$keys[22]] = $result[$keys[22]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collSongsInformationVersions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'songsInformationVersions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'songs_information_versions';
                        break;
                    default:
                        $key = 'SongsInformationVersions';
                }

                $result[$key] = $this->collSongsInformationVersions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Song\SongsInformation
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SongsInformationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Song\SongsInformation
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSongId($value);
                break;
            case 2:
                $this->setArtist($value);
                break;
            case 3:
                $this->setTitle($value);
                break;
            case 4:
                $this->setRemix($value);
                break;
            case 5:
                $this->setLabel($value);
                break;
            case 6:
                $this->setGenre($value);
                break;
            case 7:
                $this->setUrl($value);
                break;
            case 8:
                $this->setBeatport($value);
                break;
            case 9:
                $this->setSoundcloud($value);
                break;
            case 10:
                $this->setYoutube($value);
                break;
            case 11:
                $this->setItunes($value);
                break;
            case 12:
                $this->setCover($value);
                break;
            case 13:
                $this->setAddedBy($value);
                break;
            case 14:
                $this->setUserId($value);
                break;
            case 15:
                $this->setStatus($value);
                break;
            case 16:
                $this->setStart($value);
                break;
            case 17:
                $this->setEnd($value);
                break;
            case 18:
                $this->setCreatedAt($value);
                break;
            case 19:
                $this->setUpdatedAt($value);
                break;
            case 20:
                $this->setDeletedAt($value);
                break;
            case 21:
                $this->setVersion($value);
                break;
            case 22:
                $this->setVersionCreatedAt($value);
                break;
            case 23:
                $this->setVersionCreatedBy($value);
                break;
            case 24:
                $this->setVersionComment($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SongsInformationTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSongId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setArtist($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTitle($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setRemix($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLabel($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setGenre($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setBeatport($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setSoundcloud($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setYoutube($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setItunes($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCover($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setAddedBy($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setUserId($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setStatus($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setStart($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setEnd($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCreatedAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setUpdatedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setDeletedAt($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setVersion($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setVersionCreatedAt($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setVersionCreatedBy($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setVersionComment($arr[$keys[24]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Song\SongsInformation The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SongsInformationTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SongsInformationTableMap::COL_ID)) {
            $criteria->add(SongsInformationTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_SONG_ID)) {
            $criteria->add(SongsInformationTableMap::COL_SONG_ID, $this->song_id);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_ARTIST)) {
            $criteria->add(SongsInformationTableMap::COL_ARTIST, $this->artist);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_TITLE)) {
            $criteria->add(SongsInformationTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_REMIX)) {
            $criteria->add(SongsInformationTableMap::COL_REMIX, $this->remix);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_LABEL)) {
            $criteria->add(SongsInformationTableMap::COL_LABEL, $this->label);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_GENRE)) {
            $criteria->add(SongsInformationTableMap::COL_GENRE, $this->genre);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_URL)) {
            $criteria->add(SongsInformationTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_BEATPORT)) {
            $criteria->add(SongsInformationTableMap::COL_BEATPORT, $this->beatport);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_SOUNDCLOUD)) {
            $criteria->add(SongsInformationTableMap::COL_SOUNDCLOUD, $this->soundcloud);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_YOUTUBE)) {
            $criteria->add(SongsInformationTableMap::COL_YOUTUBE, $this->youtube);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_ITUNES)) {
            $criteria->add(SongsInformationTableMap::COL_ITUNES, $this->itunes);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_COVER)) {
            $criteria->add(SongsInformationTableMap::COL_COVER, $this->cover);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_ADDED_BY)) {
            $criteria->add(SongsInformationTableMap::COL_ADDED_BY, $this->added_by);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_USER_ID)) {
            $criteria->add(SongsInformationTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_STATUS)) {
            $criteria->add(SongsInformationTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_START)) {
            $criteria->add(SongsInformationTableMap::COL_START, $this->start);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_END)) {
            $criteria->add(SongsInformationTableMap::COL_END, $this->end);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_CREATED_AT)) {
            $criteria->add(SongsInformationTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_UPDATED_AT)) {
            $criteria->add(SongsInformationTableMap::COL_UPDATED_AT, $this->updated_at);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_DELETED_AT)) {
            $criteria->add(SongsInformationTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION)) {
            $criteria->add(SongsInformationTableMap::COL_VERSION, $this->version);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION_CREATED_AT)) {
            $criteria->add(SongsInformationTableMap::COL_VERSION_CREATED_AT, $this->version_created_at);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION_CREATED_BY)) {
            $criteria->add(SongsInformationTableMap::COL_VERSION_CREATED_BY, $this->version_created_by);
        }
        if ($this->isColumnModified(SongsInformationTableMap::COL_VERSION_COMMENT)) {
            $criteria->add(SongsInformationTableMap::COL_VERSION_COMMENT, $this->version_comment);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSongsInformationQuery::create();
        $criteria->add(SongsInformationTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Song\SongsInformation (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSongId($this->getSongId());
        $copyObj->setArtist($this->getArtist());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setRemix($this->getRemix());
        $copyObj->setLabel($this->getLabel());
        $copyObj->setGenre($this->getGenre());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setBeatport($this->getBeatport());
        $copyObj->setSoundcloud($this->getSoundcloud());
        $copyObj->setYoutube($this->getYoutube());
        $copyObj->setItunes($this->getItunes());
        $copyObj->setCover($this->getCover());
        $copyObj->setAddedBy($this->getAddedBy());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setStart($this->getStart());
        $copyObj->setEnd($this->getEnd());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setVersion($this->getVersion());
        $copyObj->setVersionCreatedAt($this->getVersionCreatedAt());
        $copyObj->setVersionCreatedBy($this->getVersionCreatedBy());
        $copyObj->setVersionComment($this->getVersionComment());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSongsInformationVersions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSongsInformationVersion($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Song\SongsInformation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SongsInformationVersion' == $relationName) {
            $this->initSongsInformationVersions();
            return;
        }
    }

    /**
     * Clears out the collSongsInformationVersions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSongsInformationVersions()
     */
    public function clearSongsInformationVersions()
    {
        $this->collSongsInformationVersions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSongsInformationVersions collection loaded partially.
     */
    public function resetPartialSongsInformationVersions($v = true)
    {
        $this->collSongsInformationVersionsPartial = $v;
    }

    /**
     * Initializes the collSongsInformationVersions collection.
     *
     * By default this just sets the collSongsInformationVersions collection to an empty array (like clearcollSongsInformationVersions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSongsInformationVersions($overrideExisting = true)
    {
        if (null !== $this->collSongsInformationVersions && !$overrideExisting) {
            return;
        }

        $collectionClassName = SongsInformationVersionTableMap::getTableMap()->getCollectionClassName();

        $this->collSongsInformationVersions = new $collectionClassName;
        $this->collSongsInformationVersions->setModel('\Model\Song\SongsInformationVersion');
    }

    /**
     * Gets an array of ChildSongsInformationVersion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSongsInformation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSongsInformationVersion[] List of ChildSongsInformationVersion objects
     * @throws PropelException
     */
    public function getSongsInformationVersions(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSongsInformationVersionsPartial && !$this->isNew();
        if (null === $this->collSongsInformationVersions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSongsInformationVersions) {
                // return empty collection
                $this->initSongsInformationVersions();
            } else {
                $collSongsInformationVersions = ChildSongsInformationVersionQuery::create(null, $criteria)
                    ->filterBySongsInformation($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSongsInformationVersionsPartial && count($collSongsInformationVersions)) {
                        $this->initSongsInformationVersions(false);

                        foreach ($collSongsInformationVersions as $obj) {
                            if (false == $this->collSongsInformationVersions->contains($obj)) {
                                $this->collSongsInformationVersions->append($obj);
                            }
                        }

                        $this->collSongsInformationVersionsPartial = true;
                    }

                    return $collSongsInformationVersions;
                }

                if ($partial && $this->collSongsInformationVersions) {
                    foreach ($this->collSongsInformationVersions as $obj) {
                        if ($obj->isNew()) {
                            $collSongsInformationVersions[] = $obj;
                        }
                    }
                }

                $this->collSongsInformationVersions = $collSongsInformationVersions;
                $this->collSongsInformationVersionsPartial = false;
            }
        }

        return $this->collSongsInformationVersions;
    }

    /**
     * Sets a collection of ChildSongsInformationVersion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $songsInformationVersions A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSongsInformation The current object (for fluent API support)
     */
    public function setSongsInformationVersions(Collection $songsInformationVersions, ConnectionInterface $con = null)
    {
        /** @var ChildSongsInformationVersion[] $songsInformationVersionsToDelete */
        $songsInformationVersionsToDelete = $this->getSongsInformationVersions(new Criteria(), $con)->diff($songsInformationVersions);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->songsInformationVersionsScheduledForDeletion = clone $songsInformationVersionsToDelete;

        foreach ($songsInformationVersionsToDelete as $songsInformationVersionRemoved) {
            $songsInformationVersionRemoved->setSongsInformation(null);
        }

        $this->collSongsInformationVersions = null;
        foreach ($songsInformationVersions as $songsInformationVersion) {
            $this->addSongsInformationVersion($songsInformationVersion);
        }

        $this->collSongsInformationVersions = $songsInformationVersions;
        $this->collSongsInformationVersionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SongsInformationVersion objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SongsInformationVersion objects.
     * @throws PropelException
     */
    public function countSongsInformationVersions(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSongsInformationVersionsPartial && !$this->isNew();
        if (null === $this->collSongsInformationVersions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSongsInformationVersions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSongsInformationVersions());
            }

            $query = ChildSongsInformationVersionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySongsInformation($this)
                ->count($con);
        }

        return count($this->collSongsInformationVersions);
    }

    /**
     * Method called to associate a ChildSongsInformationVersion object to this object
     * through the ChildSongsInformationVersion foreign key attribute.
     *
     * @param  ChildSongsInformationVersion $l ChildSongsInformationVersion
     * @return $this|\Model\Song\SongsInformation The current object (for fluent API support)
     */
    public function addSongsInformationVersion(ChildSongsInformationVersion $l)
    {
        if ($this->collSongsInformationVersions === null) {
            $this->initSongsInformationVersions();
            $this->collSongsInformationVersionsPartial = true;
        }

        if (!$this->collSongsInformationVersions->contains($l)) {
            $this->doAddSongsInformationVersion($l);

            if ($this->songsInformationVersionsScheduledForDeletion and $this->songsInformationVersionsScheduledForDeletion->contains($l)) {
                $this->songsInformationVersionsScheduledForDeletion->remove($this->songsInformationVersionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSongsInformationVersion $songsInformationVersion The ChildSongsInformationVersion object to add.
     */
    protected function doAddSongsInformationVersion(ChildSongsInformationVersion $songsInformationVersion)
    {
        $this->collSongsInformationVersions[]= $songsInformationVersion;
        $songsInformationVersion->setSongsInformation($this);
    }

    /**
     * @param  ChildSongsInformationVersion $songsInformationVersion The ChildSongsInformationVersion object to remove.
     * @return $this|ChildSongsInformation The current object (for fluent API support)
     */
    public function removeSongsInformationVersion(ChildSongsInformationVersion $songsInformationVersion)
    {
        if ($this->getSongsInformationVersions()->contains($songsInformationVersion)) {
            $pos = $this->collSongsInformationVersions->search($songsInformationVersion);
            $this->collSongsInformationVersions->remove($pos);
            if (null === $this->songsInformationVersionsScheduledForDeletion) {
                $this->songsInformationVersionsScheduledForDeletion = clone $this->collSongsInformationVersions;
                $this->songsInformationVersionsScheduledForDeletion->clear();
            }
            $this->songsInformationVersionsScheduledForDeletion[]= clone $songsInformationVersion;
            $songsInformationVersion->setSongsInformation(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->song_id = null;
        $this->artist = null;
        $this->title = null;
        $this->remix = null;
        $this->label = null;
        $this->genre = null;
        $this->url = null;
        $this->beatport = null;
        $this->soundcloud = null;
        $this->youtube = null;
        $this->itunes = null;
        $this->cover = null;
        $this->added_by = null;
        $this->user_id = null;
        $this->status = null;
        $this->start = null;
        $this->end = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->deleted_at = null;
        $this->version = null;
        $this->version_created_at = null;
        $this->version_created_by = null;
        $this->version_comment = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSongsInformationVersions) {
                foreach ($this->collSongsInformationVersions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSongsInformationVersions = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SongsInformationTableMap::DEFAULT_STRING_FORMAT);
    }

    // versionable behavior

    /**
     * Enforce a new Version of this object upon next save.
     *
     * @return $this|\Model\Song\SongsInformation
     */
    public function enforceVersioning()
    {
        $this->enforceVersion = true;

        return $this;
    }

    /**
     * Checks whether the current state must be recorded as a version
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     * @return  boolean
     */
    public function isVersioningNecessary(ConnectionInterface $con = null)
    {
        if ($this->alreadyInSave) {
            return false;
        }

        if ($this->enforceVersion) {
            return true;
        }

        if (ChildSongsInformationQuery::isVersioningEnabled() && ($this->isNew() || $this->isModified()) || $this->isDeleted()) {
            return true;
        }

        return false;
    }

    /**
     * Creates a version of the current object and saves it.
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  ChildSongsInformationVersion A version object
     */
    public function addVersion(ConnectionInterface $con = null)
    {
        $this->enforceVersion = false;

        $version = new ChildSongsInformationVersion();
        $version->setId($this->getId());
        $version->setSongId($this->getSongId());
        $version->setArtist($this->getArtist());
        $version->setTitle($this->getTitle());
        $version->setRemix($this->getRemix());
        $version->setLabel($this->getLabel());
        $version->setGenre($this->getGenre());
        $version->setUrl($this->getUrl());
        $version->setBeatport($this->getBeatport());
        $version->setSoundcloud($this->getSoundcloud());
        $version->setYoutube($this->getYoutube());
        $version->setItunes($this->getItunes());
        $version->setCover($this->getCover());
        $version->setAddedBy($this->getAddedBy());
        $version->setUserId($this->getUserId());
        $version->setStatus($this->getStatus());
        $version->setStart($this->getStart());
        $version->setEnd($this->getEnd());
        $version->setCreatedAt($this->getCreatedAt());
        $version->setUpdatedAt($this->getUpdatedAt());
        $version->setDeletedAt($this->getDeletedAt());
        $version->setVersion($this->getVersion());
        $version->setVersionCreatedAt($this->getVersionCreatedAt());
        $version->setVersionCreatedBy($this->getVersionCreatedBy());
        $version->setVersionComment($this->getVersionComment());
        $version->setSongsInformation($this);
        $version->save($con);

        return $version;
    }

    /**
     * Sets the properties of the current object to the value they had at a specific version
     *
     * @param   integer $versionNumber The version number to read
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  $this|ChildSongsInformation The current object (for fluent API support)
     */
    public function toVersion($versionNumber, ConnectionInterface $con = null)
    {
        $version = $this->getOneVersion($versionNumber, $con);
        if (!$version) {
            throw new PropelException(sprintf('No ChildSongsInformation object found with version %d', $version));
        }
        $this->populateFromVersion($version, $con);

        return $this;
    }

    /**
     * Sets the properties of the current object to the value they had at a specific version
     *
     * @param ChildSongsInformationVersion $version The version object to use
     * @param ConnectionInterface   $con the connection to use
     * @param array                 $loadedObjects objects that been loaded in a chain of populateFromVersion calls on referrer or fk objects.
     *
     * @return $this|ChildSongsInformation The current object (for fluent API support)
     */
    public function populateFromVersion($version, $con = null, &$loadedObjects = array())
    {
        $loadedObjects['ChildSongsInformation'][$version->getId()][$version->getVersion()] = $this;
        $this->setId($version->getId());
        $this->setSongId($version->getSongId());
        $this->setArtist($version->getArtist());
        $this->setTitle($version->getTitle());
        $this->setRemix($version->getRemix());
        $this->setLabel($version->getLabel());
        $this->setGenre($version->getGenre());
        $this->setUrl($version->getUrl());
        $this->setBeatport($version->getBeatport());
        $this->setSoundcloud($version->getSoundcloud());
        $this->setYoutube($version->getYoutube());
        $this->setItunes($version->getItunes());
        $this->setCover($version->getCover());
        $this->setAddedBy($version->getAddedBy());
        $this->setUserId($version->getUserId());
        $this->setStatus($version->getStatus());
        $this->setStart($version->getStart());
        $this->setEnd($version->getEnd());
        $this->setCreatedAt($version->getCreatedAt());
        $this->setUpdatedAt($version->getUpdatedAt());
        $this->setDeletedAt($version->getDeletedAt());
        $this->setVersion($version->getVersion());
        $this->setVersionCreatedAt($version->getVersionCreatedAt());
        $this->setVersionCreatedBy($version->getVersionCreatedBy());
        $this->setVersionComment($version->getVersionComment());

        return $this;
    }

    /**
     * Gets the latest persisted version number for the current object
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  integer
     */
    public function getLastVersionNumber(ConnectionInterface $con = null)
    {
        $v = ChildSongsInformationVersionQuery::create()
            ->filterBySongsInformation($this)
            ->orderByVersion('desc')
            ->findOne($con);
        if (!$v) {
            return 0;
        }

        return $v->getVersion();
    }

    /**
     * Checks whether the current object is the latest one
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  Boolean
     */
    public function isLastVersion(ConnectionInterface $con = null)
    {
        return $this->getLastVersionNumber($con) == $this->getVersion();
    }

    /**
     * Retrieves a version object for this entity and a version number
     *
     * @param   integer $versionNumber The version number to read
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  ChildSongsInformationVersion A version object
     */
    public function getOneVersion($versionNumber, ConnectionInterface $con = null)
    {
        return ChildSongsInformationVersionQuery::create()
            ->filterBySongsInformation($this)
            ->filterByVersion($versionNumber)
            ->findOne($con);
    }

    /**
     * Gets all the versions of this object, in incremental order
     *
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return  ObjectCollection|ChildSongsInformationVersion[] A list of ChildSongsInformationVersion objects
     */
    public function getAllVersions(ConnectionInterface $con = null)
    {
        $criteria = new Criteria();
        $criteria->addAscendingOrderByColumn(SongsInformationVersionTableMap::COL_VERSION);

        return $this->getSongsInformationVersions($criteria, $con);
    }

    /**
     * Compares the current object with another of its version.
     * <code>
     * print_r($book->compareVersion(1));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param   integer             $versionNumber
     * @param   string              $keys Main key used for the result diff (versions|columns)
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     * @param   array               $ignoredColumns  The columns to exclude from the diff.
     *
     * @return  array A list of differences
     */
    public function compareVersion($versionNumber, $keys = 'columns', ConnectionInterface $con = null, $ignoredColumns = array())
    {
        $fromVersion = $this->toArray();
        $toVersion = $this->getOneVersion($versionNumber, $con)->toArray();

        return $this->computeDiff($fromVersion, $toVersion, $keys, $ignoredColumns);
    }

    /**
     * Compares two versions of the current object.
     * <code>
     * print_r($book->compareVersions(1, 2));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param   integer             $fromVersionNumber
     * @param   integer             $toVersionNumber
     * @param   string              $keys Main key used for the result diff (versions|columns)
     * @param   ConnectionInterface $con The ConnectionInterface connection to use.
     * @param   array               $ignoredColumns  The columns to exclude from the diff.
     *
     * @return  array A list of differences
     */
    public function compareVersions($fromVersionNumber, $toVersionNumber, $keys = 'columns', ConnectionInterface $con = null, $ignoredColumns = array())
    {
        $fromVersion = $this->getOneVersion($fromVersionNumber, $con)->toArray();
        $toVersion = $this->getOneVersion($toVersionNumber, $con)->toArray();

        return $this->computeDiff($fromVersion, $toVersion, $keys, $ignoredColumns);
    }

    /**
     * Computes the diff between two versions.
     * <code>
     * print_r($book->computeDiff(1, 2));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param   array     $fromVersion     An array representing the original version.
     * @param   array     $toVersion       An array representing the destination version.
     * @param   string    $keys            Main key used for the result diff (versions|columns).
     * @param   array     $ignoredColumns  The columns to exclude from the diff.
     *
     * @return  array A list of differences
     */
    protected function computeDiff($fromVersion, $toVersion, $keys = 'columns', $ignoredColumns = array())
    {
        $fromVersionNumber = $fromVersion['Version'];
        $toVersionNumber = $toVersion['Version'];
        $ignoredColumns = array_merge(array(
            'Version',
            'VersionCreatedAt',
            'VersionCreatedBy',
            'VersionComment',
        ), $ignoredColumns);
        $diff = array();
        foreach ($fromVersion as $key => $value) {
            if (in_array($key, $ignoredColumns)) {
                continue;
            }
            if ($toVersion[$key] != $value) {
                switch ($keys) {
                    case 'versions':
                        $diff[$fromVersionNumber][$key] = $value;
                        $diff[$toVersionNumber][$key] = $toVersion[$key];
                        break;
                    default:
                        $diff[$key] = array(
                            $fromVersionNumber => $value,
                            $toVersionNumber => $toVersion[$key],
                        );
                        break;
                }
            }
        }

        return $diff;
    }
    /**
     * retrieve the last $number versions.
     *
     * @param  Integer             $number The number of record to return.
     * @param  Criteria            $criteria The Criteria object containing modified values.
     * @param  ConnectionInterface $con The ConnectionInterface connection to use.
     *
     * @return PropelCollection|\Model\Song\SongsInformationVersion[] List of \Model\Song\SongsInformationVersion objects
     */
    public function getLastVersions($number = 10, $criteria = null, ConnectionInterface $con = null)
    {
        $criteria = ChildSongsInformationVersionQuery::create(null, $criteria);
        $criteria->addDescendingOrderByColumn(SongsInformationVersionTableMap::COL_VERSION);
        $criteria->limit($number);

        return $this->getSongsInformationVersions($criteria, $con);
    }
    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
