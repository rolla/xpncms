<?php

namespace Model\Song\Base;

use \Exception;
use \PDO;
use Model\Song\Songs as ChildSongs;
use Model\Song\SongsQuery as ChildSongsQuery;
use Model\Song\Map\SongsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'songs' table.
 *
 *
 *
 * @method     ChildSongsQuery orderById($order = Criteria::ASC) Order by the ID column
 * @method     ChildSongsQuery orderByPath($order = Criteria::ASC) Order by the path column
 * @method     ChildSongsQuery orderByEnabled($order = Criteria::ASC) Order by the enabled column
 * @method     ChildSongsQuery orderByDatePlayed($order = Criteria::ASC) Order by the date_played column
 * @method     ChildSongsQuery orderByArtistPlayed($order = Criteria::ASC) Order by the artist_played column
 * @method     ChildSongsQuery orderByCountPlayed($order = Criteria::ASC) Order by the count_played column
 * @method     ChildSongsQuery orderByPlayLimit($order = Criteria::ASC) Order by the play_limit column
 * @method     ChildSongsQuery orderByLimitAction($order = Criteria::ASC) Order by the limit_action column
 * @method     ChildSongsQuery orderByStartDate($order = Criteria::ASC) Order by the start_date column
 * @method     ChildSongsQuery orderByEndDate($order = Criteria::ASC) Order by the end_date column
 * @method     ChildSongsQuery orderBySongType($order = Criteria::ASC) Order by the song_type column
 * @method     ChildSongsQuery orderByIdSubcat($order = Criteria::ASC) Order by the id_subcat column
 * @method     ChildSongsQuery orderByIdGenre($order = Criteria::ASC) Order by the id_genre column
 * @method     ChildSongsQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildSongsQuery orderByDuration($order = Criteria::ASC) Order by the duration column
 * @method     ChildSongsQuery orderByCueTimes($order = Criteria::ASC) Order by the cue_times column
 * @method     ChildSongsQuery orderByPreciseCue($order = Criteria::ASC) Order by the precise_cue column
 * @method     ChildSongsQuery orderByFadeType($order = Criteria::ASC) Order by the fade_type column
 * @method     ChildSongsQuery orderByEndType($order = Criteria::ASC) Order by the end_type column
 * @method     ChildSongsQuery orderByOverlay($order = Criteria::ASC) Order by the overlay column
 * @method     ChildSongsQuery orderByArtist($order = Criteria::ASC) Order by the artist column
 * @method     ChildSongsQuery orderByOriginalArtist($order = Criteria::ASC) Order by the original_artist column
 * @method     ChildSongsQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSongsQuery orderByAlbum($order = Criteria::ASC) Order by the album column
 * @method     ChildSongsQuery orderByComposer($order = Criteria::ASC) Order by the composer column
 * @method     ChildSongsQuery orderByYear($order = Criteria::ASC) Order by the year column
 * @method     ChildSongsQuery orderByTrackNo($order = Criteria::ASC) Order by the track_no column
 * @method     ChildSongsQuery orderByDiscNo($order = Criteria::ASC) Order by the disc_no column
 * @method     ChildSongsQuery orderByPublisher($order = Criteria::ASC) Order by the publisher column
 * @method     ChildSongsQuery orderByCopyright($order = Criteria::ASC) Order by the copyright column
 * @method     ChildSongsQuery orderByIsrc($order = Criteria::ASC) Order by the isrc column
 * @method     ChildSongsQuery orderByBpm($order = Criteria::ASC) Order by the bpm column
 * @method     ChildSongsQuery orderByComments($order = Criteria::ASC) Order by the comments column
 * @method     ChildSongsQuery orderBySweepers($order = Criteria::ASC) Order by the sweepers column
 * @method     ChildSongsQuery orderByAlbumArt($order = Criteria::ASC) Order by the album_art column
 * @method     ChildSongsQuery orderByBuyLink($order = Criteria::ASC) Order by the buy_link column
 * @method     ChildSongsQuery orderByTdatePlayed($order = Criteria::ASC) Order by the tdate_played column
 * @method     ChildSongsQuery orderByTartistPlayed($order = Criteria::ASC) Order by the tartist_played column
 * @method     ChildSongsQuery orderByDateAdded($order = Criteria::ASC) Order by the date_added column
 * @method     ChildSongsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildSongsQuery orderByVersion($order = Criteria::ASC) Order by the version column
 * @method     ChildSongsQuery orderByVersionCreatedAt($order = Criteria::ASC) Order by the version_created_at column
 * @method     ChildSongsQuery orderByVersionCreatedBy($order = Criteria::ASC) Order by the version_created_by column
 * @method     ChildSongsQuery orderByVersionComment($order = Criteria::ASC) Order by the version_comment column
 *
 * @method     ChildSongsQuery groupById() Group by the ID column
 * @method     ChildSongsQuery groupByPath() Group by the path column
 * @method     ChildSongsQuery groupByEnabled() Group by the enabled column
 * @method     ChildSongsQuery groupByDatePlayed() Group by the date_played column
 * @method     ChildSongsQuery groupByArtistPlayed() Group by the artist_played column
 * @method     ChildSongsQuery groupByCountPlayed() Group by the count_played column
 * @method     ChildSongsQuery groupByPlayLimit() Group by the play_limit column
 * @method     ChildSongsQuery groupByLimitAction() Group by the limit_action column
 * @method     ChildSongsQuery groupByStartDate() Group by the start_date column
 * @method     ChildSongsQuery groupByEndDate() Group by the end_date column
 * @method     ChildSongsQuery groupBySongType() Group by the song_type column
 * @method     ChildSongsQuery groupByIdSubcat() Group by the id_subcat column
 * @method     ChildSongsQuery groupByIdGenre() Group by the id_genre column
 * @method     ChildSongsQuery groupByWeight() Group by the weight column
 * @method     ChildSongsQuery groupByDuration() Group by the duration column
 * @method     ChildSongsQuery groupByCueTimes() Group by the cue_times column
 * @method     ChildSongsQuery groupByPreciseCue() Group by the precise_cue column
 * @method     ChildSongsQuery groupByFadeType() Group by the fade_type column
 * @method     ChildSongsQuery groupByEndType() Group by the end_type column
 * @method     ChildSongsQuery groupByOverlay() Group by the overlay column
 * @method     ChildSongsQuery groupByArtist() Group by the artist column
 * @method     ChildSongsQuery groupByOriginalArtist() Group by the original_artist column
 * @method     ChildSongsQuery groupByTitle() Group by the title column
 * @method     ChildSongsQuery groupByAlbum() Group by the album column
 * @method     ChildSongsQuery groupByComposer() Group by the composer column
 * @method     ChildSongsQuery groupByYear() Group by the year column
 * @method     ChildSongsQuery groupByTrackNo() Group by the track_no column
 * @method     ChildSongsQuery groupByDiscNo() Group by the disc_no column
 * @method     ChildSongsQuery groupByPublisher() Group by the publisher column
 * @method     ChildSongsQuery groupByCopyright() Group by the copyright column
 * @method     ChildSongsQuery groupByIsrc() Group by the isrc column
 * @method     ChildSongsQuery groupByBpm() Group by the bpm column
 * @method     ChildSongsQuery groupByComments() Group by the comments column
 * @method     ChildSongsQuery groupBySweepers() Group by the sweepers column
 * @method     ChildSongsQuery groupByAlbumArt() Group by the album_art column
 * @method     ChildSongsQuery groupByBuyLink() Group by the buy_link column
 * @method     ChildSongsQuery groupByTdatePlayed() Group by the tdate_played column
 * @method     ChildSongsQuery groupByTartistPlayed() Group by the tartist_played column
 * @method     ChildSongsQuery groupByDateAdded() Group by the date_added column
 * @method     ChildSongsQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildSongsQuery groupByVersion() Group by the version column
 * @method     ChildSongsQuery groupByVersionCreatedAt() Group by the version_created_at column
 * @method     ChildSongsQuery groupByVersionCreatedBy() Group by the version_created_by column
 * @method     ChildSongsQuery groupByVersionComment() Group by the version_comment column
 *
 * @method     ChildSongsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSongsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSongsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSongsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSongsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSongsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSongsQuery leftJoinSongsVersion($relationAlias = null) Adds a LEFT JOIN clause to the query using the SongsVersion relation
 * @method     ChildSongsQuery rightJoinSongsVersion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SongsVersion relation
 * @method     ChildSongsQuery innerJoinSongsVersion($relationAlias = null) Adds a INNER JOIN clause to the query using the SongsVersion relation
 *
 * @method     ChildSongsQuery joinWithSongsVersion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SongsVersion relation
 *
 * @method     ChildSongsQuery leftJoinWithSongsVersion() Adds a LEFT JOIN clause and with to the query using the SongsVersion relation
 * @method     ChildSongsQuery rightJoinWithSongsVersion() Adds a RIGHT JOIN clause and with to the query using the SongsVersion relation
 * @method     ChildSongsQuery innerJoinWithSongsVersion() Adds a INNER JOIN clause and with to the query using the SongsVersion relation
 *
 * @method     \Model\Song\SongsVersionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSongs findOne(ConnectionInterface $con = null) Return the first ChildSongs matching the query
 * @method     ChildSongs findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSongs matching the query, or a new ChildSongs object populated from the query conditions when no match is found
 *
 * @method     ChildSongs findOneById(int $ID) Return the first ChildSongs filtered by the ID column
 * @method     ChildSongs findOneByPath(string $path) Return the first ChildSongs filtered by the path column
 * @method     ChildSongs findOneByEnabled(int $enabled) Return the first ChildSongs filtered by the enabled column
 * @method     ChildSongs findOneByDatePlayed(string $date_played) Return the first ChildSongs filtered by the date_played column
 * @method     ChildSongs findOneByArtistPlayed(string $artist_played) Return the first ChildSongs filtered by the artist_played column
 * @method     ChildSongs findOneByCountPlayed(int $count_played) Return the first ChildSongs filtered by the count_played column
 * @method     ChildSongs findOneByPlayLimit(int $play_limit) Return the first ChildSongs filtered by the play_limit column
 * @method     ChildSongs findOneByLimitAction(int $limit_action) Return the first ChildSongs filtered by the limit_action column
 * @method     ChildSongs findOneByStartDate(string $start_date) Return the first ChildSongs filtered by the start_date column
 * @method     ChildSongs findOneByEndDate(string $end_date) Return the first ChildSongs filtered by the end_date column
 * @method     ChildSongs findOneBySongType(int $song_type) Return the first ChildSongs filtered by the song_type column
 * @method     ChildSongs findOneByIdSubcat(int $id_subcat) Return the first ChildSongs filtered by the id_subcat column
 * @method     ChildSongs findOneByIdGenre(int $id_genre) Return the first ChildSongs filtered by the id_genre column
 * @method     ChildSongs findOneByWeight(double $weight) Return the first ChildSongs filtered by the weight column
 * @method     ChildSongs findOneByDuration(double $duration) Return the first ChildSongs filtered by the duration column
 * @method     ChildSongs findOneByCueTimes(string $cue_times) Return the first ChildSongs filtered by the cue_times column
 * @method     ChildSongs findOneByPreciseCue(boolean $precise_cue) Return the first ChildSongs filtered by the precise_cue column
 * @method     ChildSongs findOneByFadeType(boolean $fade_type) Return the first ChildSongs filtered by the fade_type column
 * @method     ChildSongs findOneByEndType(boolean $end_type) Return the first ChildSongs filtered by the end_type column
 * @method     ChildSongs findOneByOverlay(boolean $overlay) Return the first ChildSongs filtered by the overlay column
 * @method     ChildSongs findOneByArtist(string $artist) Return the first ChildSongs filtered by the artist column
 * @method     ChildSongs findOneByOriginalArtist(string $original_artist) Return the first ChildSongs filtered by the original_artist column
 * @method     ChildSongs findOneByTitle(string $title) Return the first ChildSongs filtered by the title column
 * @method     ChildSongs findOneByAlbum(string $album) Return the first ChildSongs filtered by the album column
 * @method     ChildSongs findOneByComposer(string $composer) Return the first ChildSongs filtered by the composer column
 * @method     ChildSongs findOneByYear(string $year) Return the first ChildSongs filtered by the year column
 * @method     ChildSongs findOneByTrackNo(int $track_no) Return the first ChildSongs filtered by the track_no column
 * @method     ChildSongs findOneByDiscNo(int $disc_no) Return the first ChildSongs filtered by the disc_no column
 * @method     ChildSongs findOneByPublisher(string $publisher) Return the first ChildSongs filtered by the publisher column
 * @method     ChildSongs findOneByCopyright(string $copyright) Return the first ChildSongs filtered by the copyright column
 * @method     ChildSongs findOneByIsrc(string $isrc) Return the first ChildSongs filtered by the isrc column
 * @method     ChildSongs findOneByBpm(double $bpm) Return the first ChildSongs filtered by the bpm column
 * @method     ChildSongs findOneByComments(string $comments) Return the first ChildSongs filtered by the comments column
 * @method     ChildSongs findOneBySweepers(string $sweepers) Return the first ChildSongs filtered by the sweepers column
 * @method     ChildSongs findOneByAlbumArt(string $album_art) Return the first ChildSongs filtered by the album_art column
 * @method     ChildSongs findOneByBuyLink(string $buy_link) Return the first ChildSongs filtered by the buy_link column
 * @method     ChildSongs findOneByTdatePlayed(string $tdate_played) Return the first ChildSongs filtered by the tdate_played column
 * @method     ChildSongs findOneByTartistPlayed(string $tartist_played) Return the first ChildSongs filtered by the tartist_played column
 * @method     ChildSongs findOneByDateAdded(string $date_added) Return the first ChildSongs filtered by the date_added column
 * @method     ChildSongs findOneByUpdatedAt(string $updated_at) Return the first ChildSongs filtered by the updated_at column
 * @method     ChildSongs findOneByVersion(int $version) Return the first ChildSongs filtered by the version column
 * @method     ChildSongs findOneByVersionCreatedAt(string $version_created_at) Return the first ChildSongs filtered by the version_created_at column
 * @method     ChildSongs findOneByVersionCreatedBy(string $version_created_by) Return the first ChildSongs filtered by the version_created_by column
 * @method     ChildSongs findOneByVersionComment(string $version_comment) Return the first ChildSongs filtered by the version_comment column *

 * @method     ChildSongs requirePk($key, ConnectionInterface $con = null) Return the ChildSongs by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOne(ConnectionInterface $con = null) Return the first ChildSongs matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSongs requireOneById(int $ID) Return the first ChildSongs filtered by the ID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByPath(string $path) Return the first ChildSongs filtered by the path column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByEnabled(int $enabled) Return the first ChildSongs filtered by the enabled column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByDatePlayed(string $date_played) Return the first ChildSongs filtered by the date_played column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByArtistPlayed(string $artist_played) Return the first ChildSongs filtered by the artist_played column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByCountPlayed(int $count_played) Return the first ChildSongs filtered by the count_played column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByPlayLimit(int $play_limit) Return the first ChildSongs filtered by the play_limit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByLimitAction(int $limit_action) Return the first ChildSongs filtered by the limit_action column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByStartDate(string $start_date) Return the first ChildSongs filtered by the start_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByEndDate(string $end_date) Return the first ChildSongs filtered by the end_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneBySongType(int $song_type) Return the first ChildSongs filtered by the song_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByIdSubcat(int $id_subcat) Return the first ChildSongs filtered by the id_subcat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByIdGenre(int $id_genre) Return the first ChildSongs filtered by the id_genre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByWeight(double $weight) Return the first ChildSongs filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByDuration(double $duration) Return the first ChildSongs filtered by the duration column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByCueTimes(string $cue_times) Return the first ChildSongs filtered by the cue_times column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByPreciseCue(boolean $precise_cue) Return the first ChildSongs filtered by the precise_cue column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByFadeType(boolean $fade_type) Return the first ChildSongs filtered by the fade_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByEndType(boolean $end_type) Return the first ChildSongs filtered by the end_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByOverlay(boolean $overlay) Return the first ChildSongs filtered by the overlay column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByArtist(string $artist) Return the first ChildSongs filtered by the artist column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByOriginalArtist(string $original_artist) Return the first ChildSongs filtered by the original_artist column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByTitle(string $title) Return the first ChildSongs filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByAlbum(string $album) Return the first ChildSongs filtered by the album column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByComposer(string $composer) Return the first ChildSongs filtered by the composer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByYear(string $year) Return the first ChildSongs filtered by the year column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByTrackNo(int $track_no) Return the first ChildSongs filtered by the track_no column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByDiscNo(int $disc_no) Return the first ChildSongs filtered by the disc_no column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByPublisher(string $publisher) Return the first ChildSongs filtered by the publisher column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByCopyright(string $copyright) Return the first ChildSongs filtered by the copyright column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByIsrc(string $isrc) Return the first ChildSongs filtered by the isrc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByBpm(double $bpm) Return the first ChildSongs filtered by the bpm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByComments(string $comments) Return the first ChildSongs filtered by the comments column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneBySweepers(string $sweepers) Return the first ChildSongs filtered by the sweepers column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByAlbumArt(string $album_art) Return the first ChildSongs filtered by the album_art column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByBuyLink(string $buy_link) Return the first ChildSongs filtered by the buy_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByTdatePlayed(string $tdate_played) Return the first ChildSongs filtered by the tdate_played column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByTartistPlayed(string $tartist_played) Return the first ChildSongs filtered by the tartist_played column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByDateAdded(string $date_added) Return the first ChildSongs filtered by the date_added column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByUpdatedAt(string $updated_at) Return the first ChildSongs filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByVersion(int $version) Return the first ChildSongs filtered by the version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByVersionCreatedAt(string $version_created_at) Return the first ChildSongs filtered by the version_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByVersionCreatedBy(string $version_created_by) Return the first ChildSongs filtered by the version_created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSongs requireOneByVersionComment(string $version_comment) Return the first ChildSongs filtered by the version_comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSongs[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSongs objects based on current ModelCriteria
 * @method     ChildSongs[]|ObjectCollection findById(int $ID) Return ChildSongs objects filtered by the ID column
 * @method     ChildSongs[]|ObjectCollection findByPath(string $path) Return ChildSongs objects filtered by the path column
 * @method     ChildSongs[]|ObjectCollection findByEnabled(int $enabled) Return ChildSongs objects filtered by the enabled column
 * @method     ChildSongs[]|ObjectCollection findByDatePlayed(string $date_played) Return ChildSongs objects filtered by the date_played column
 * @method     ChildSongs[]|ObjectCollection findByArtistPlayed(string $artist_played) Return ChildSongs objects filtered by the artist_played column
 * @method     ChildSongs[]|ObjectCollection findByCountPlayed(int $count_played) Return ChildSongs objects filtered by the count_played column
 * @method     ChildSongs[]|ObjectCollection findByPlayLimit(int $play_limit) Return ChildSongs objects filtered by the play_limit column
 * @method     ChildSongs[]|ObjectCollection findByLimitAction(int $limit_action) Return ChildSongs objects filtered by the limit_action column
 * @method     ChildSongs[]|ObjectCollection findByStartDate(string $start_date) Return ChildSongs objects filtered by the start_date column
 * @method     ChildSongs[]|ObjectCollection findByEndDate(string $end_date) Return ChildSongs objects filtered by the end_date column
 * @method     ChildSongs[]|ObjectCollection findBySongType(int $song_type) Return ChildSongs objects filtered by the song_type column
 * @method     ChildSongs[]|ObjectCollection findByIdSubcat(int $id_subcat) Return ChildSongs objects filtered by the id_subcat column
 * @method     ChildSongs[]|ObjectCollection findByIdGenre(int $id_genre) Return ChildSongs objects filtered by the id_genre column
 * @method     ChildSongs[]|ObjectCollection findByWeight(double $weight) Return ChildSongs objects filtered by the weight column
 * @method     ChildSongs[]|ObjectCollection findByDuration(double $duration) Return ChildSongs objects filtered by the duration column
 * @method     ChildSongs[]|ObjectCollection findByCueTimes(string $cue_times) Return ChildSongs objects filtered by the cue_times column
 * @method     ChildSongs[]|ObjectCollection findByPreciseCue(boolean $precise_cue) Return ChildSongs objects filtered by the precise_cue column
 * @method     ChildSongs[]|ObjectCollection findByFadeType(boolean $fade_type) Return ChildSongs objects filtered by the fade_type column
 * @method     ChildSongs[]|ObjectCollection findByEndType(boolean $end_type) Return ChildSongs objects filtered by the end_type column
 * @method     ChildSongs[]|ObjectCollection findByOverlay(boolean $overlay) Return ChildSongs objects filtered by the overlay column
 * @method     ChildSongs[]|ObjectCollection findByArtist(string $artist) Return ChildSongs objects filtered by the artist column
 * @method     ChildSongs[]|ObjectCollection findByOriginalArtist(string $original_artist) Return ChildSongs objects filtered by the original_artist column
 * @method     ChildSongs[]|ObjectCollection findByTitle(string $title) Return ChildSongs objects filtered by the title column
 * @method     ChildSongs[]|ObjectCollection findByAlbum(string $album) Return ChildSongs objects filtered by the album column
 * @method     ChildSongs[]|ObjectCollection findByComposer(string $composer) Return ChildSongs objects filtered by the composer column
 * @method     ChildSongs[]|ObjectCollection findByYear(string $year) Return ChildSongs objects filtered by the year column
 * @method     ChildSongs[]|ObjectCollection findByTrackNo(int $track_no) Return ChildSongs objects filtered by the track_no column
 * @method     ChildSongs[]|ObjectCollection findByDiscNo(int $disc_no) Return ChildSongs objects filtered by the disc_no column
 * @method     ChildSongs[]|ObjectCollection findByPublisher(string $publisher) Return ChildSongs objects filtered by the publisher column
 * @method     ChildSongs[]|ObjectCollection findByCopyright(string $copyright) Return ChildSongs objects filtered by the copyright column
 * @method     ChildSongs[]|ObjectCollection findByIsrc(string $isrc) Return ChildSongs objects filtered by the isrc column
 * @method     ChildSongs[]|ObjectCollection findByBpm(double $bpm) Return ChildSongs objects filtered by the bpm column
 * @method     ChildSongs[]|ObjectCollection findByComments(string $comments) Return ChildSongs objects filtered by the comments column
 * @method     ChildSongs[]|ObjectCollection findBySweepers(string $sweepers) Return ChildSongs objects filtered by the sweepers column
 * @method     ChildSongs[]|ObjectCollection findByAlbumArt(string $album_art) Return ChildSongs objects filtered by the album_art column
 * @method     ChildSongs[]|ObjectCollection findByBuyLink(string $buy_link) Return ChildSongs objects filtered by the buy_link column
 * @method     ChildSongs[]|ObjectCollection findByTdatePlayed(string $tdate_played) Return ChildSongs objects filtered by the tdate_played column
 * @method     ChildSongs[]|ObjectCollection findByTartistPlayed(string $tartist_played) Return ChildSongs objects filtered by the tartist_played column
 * @method     ChildSongs[]|ObjectCollection findByDateAdded(string $date_added) Return ChildSongs objects filtered by the date_added column
 * @method     ChildSongs[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildSongs objects filtered by the updated_at column
 * @method     ChildSongs[]|ObjectCollection findByVersion(int $version) Return ChildSongs objects filtered by the version column
 * @method     ChildSongs[]|ObjectCollection findByVersionCreatedAt(string $version_created_at) Return ChildSongs objects filtered by the version_created_at column
 * @method     ChildSongs[]|ObjectCollection findByVersionCreatedBy(string $version_created_by) Return ChildSongs objects filtered by the version_created_by column
 * @method     ChildSongs[]|ObjectCollection findByVersionComment(string $version_comment) Return ChildSongs objects filtered by the version_comment column
 * @method     ChildSongs[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SongsQuery extends ModelCriteria
{

    // versionable behavior

    /**
     * Whether the versioning is enabled
     */
    static $isVersioningEnabled = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Song\Base\SongsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'gradio', $modelName = '\\Model\\Song\\Songs', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSongsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSongsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSongsQuery) {
            return $criteria;
        }
        $query = new ChildSongsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSongs|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SongsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SongsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSongs A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT ID, path, enabled, date_played, artist_played, count_played, play_limit, limit_action, start_date, end_date, song_type, id_subcat, id_genre, weight, duration, cue_times, precise_cue, fade_type, end_type, overlay, artist, original_artist, title, album, composer, year, track_no, disc_no, publisher, copyright, isrc, bpm, comments, sweepers, album_art, buy_link, tdate_played, tartist_played, date_added, updated_at, version, version_created_at, version_created_by, version_comment FROM songs WHERE ID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSongs $obj */
            $obj = new ChildSongs();
            $obj->hydrate($row);
            SongsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSongs|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SongsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SongsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the ID column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE ID = 1234
     * $query->filterById(array(12, 34)); // WHERE ID IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE ID > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the path column
     *
     * Example usage:
     * <code>
     * $query->filterByPath('fooValue');   // WHERE path = 'fooValue'
     * $query->filterByPath('%fooValue%', Criteria::LIKE); // WHERE path LIKE '%fooValue%'
     * </code>
     *
     * @param     string $path The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByPath($path = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($path)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_PATH, $path, $comparison);
    }

    /**
     * Filter the query on the enabled column
     *
     * Example usage:
     * <code>
     * $query->filterByEnabled(1234); // WHERE enabled = 1234
     * $query->filterByEnabled(array(12, 34)); // WHERE enabled IN (12, 34)
     * $query->filterByEnabled(array('min' => 12)); // WHERE enabled > 12
     * </code>
     *
     * @param     mixed $enabled The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByEnabled($enabled = null, $comparison = null)
    {
        if (is_array($enabled)) {
            $useMinMax = false;
            if (isset($enabled['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_ENABLED, $enabled['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enabled['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_ENABLED, $enabled['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ENABLED, $enabled, $comparison);
    }

    /**
     * Filter the query on the date_played column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePlayed('2011-03-14'); // WHERE date_played = '2011-03-14'
     * $query->filterByDatePlayed('now'); // WHERE date_played = '2011-03-14'
     * $query->filterByDatePlayed(array('max' => 'yesterday')); // WHERE date_played > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePlayed The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByDatePlayed($datePlayed = null, $comparison = null)
    {
        if (is_array($datePlayed)) {
            $useMinMax = false;
            if (isset($datePlayed['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_DATE_PLAYED, $datePlayed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePlayed['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_DATE_PLAYED, $datePlayed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_DATE_PLAYED, $datePlayed, $comparison);
    }

    /**
     * Filter the query on the artist_played column
     *
     * Example usage:
     * <code>
     * $query->filterByArtistPlayed('2011-03-14'); // WHERE artist_played = '2011-03-14'
     * $query->filterByArtistPlayed('now'); // WHERE artist_played = '2011-03-14'
     * $query->filterByArtistPlayed(array('max' => 'yesterday')); // WHERE artist_played > '2011-03-13'
     * </code>
     *
     * @param     mixed $artistPlayed The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByArtistPlayed($artistPlayed = null, $comparison = null)
    {
        if (is_array($artistPlayed)) {
            $useMinMax = false;
            if (isset($artistPlayed['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_ARTIST_PLAYED, $artistPlayed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($artistPlayed['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_ARTIST_PLAYED, $artistPlayed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ARTIST_PLAYED, $artistPlayed, $comparison);
    }

    /**
     * Filter the query on the count_played column
     *
     * Example usage:
     * <code>
     * $query->filterByCountPlayed(1234); // WHERE count_played = 1234
     * $query->filterByCountPlayed(array(12, 34)); // WHERE count_played IN (12, 34)
     * $query->filterByCountPlayed(array('min' => 12)); // WHERE count_played > 12
     * </code>
     *
     * @param     mixed $countPlayed The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByCountPlayed($countPlayed = null, $comparison = null)
    {
        if (is_array($countPlayed)) {
            $useMinMax = false;
            if (isset($countPlayed['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_COUNT_PLAYED, $countPlayed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countPlayed['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_COUNT_PLAYED, $countPlayed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_COUNT_PLAYED, $countPlayed, $comparison);
    }

    /**
     * Filter the query on the play_limit column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayLimit(1234); // WHERE play_limit = 1234
     * $query->filterByPlayLimit(array(12, 34)); // WHERE play_limit IN (12, 34)
     * $query->filterByPlayLimit(array('min' => 12)); // WHERE play_limit > 12
     * </code>
     *
     * @param     mixed $playLimit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByPlayLimit($playLimit = null, $comparison = null)
    {
        if (is_array($playLimit)) {
            $useMinMax = false;
            if (isset($playLimit['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_PLAY_LIMIT, $playLimit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($playLimit['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_PLAY_LIMIT, $playLimit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_PLAY_LIMIT, $playLimit, $comparison);
    }

    /**
     * Filter the query on the limit_action column
     *
     * Example usage:
     * <code>
     * $query->filterByLimitAction(1234); // WHERE limit_action = 1234
     * $query->filterByLimitAction(array(12, 34)); // WHERE limit_action IN (12, 34)
     * $query->filterByLimitAction(array('min' => 12)); // WHERE limit_action > 12
     * </code>
     *
     * @param     mixed $limitAction The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByLimitAction($limitAction = null, $comparison = null)
    {
        if (is_array($limitAction)) {
            $useMinMax = false;
            if (isset($limitAction['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_LIMIT_ACTION, $limitAction['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($limitAction['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_LIMIT_ACTION, $limitAction['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_LIMIT_ACTION, $limitAction, $comparison);
    }

    /**
     * Filter the query on the start_date column
     *
     * Example usage:
     * <code>
     * $query->filterByStartDate('2011-03-14'); // WHERE start_date = '2011-03-14'
     * $query->filterByStartDate('now'); // WHERE start_date = '2011-03-14'
     * $query->filterByStartDate(array('max' => 'yesterday')); // WHERE start_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $startDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByStartDate($startDate = null, $comparison = null)
    {
        if (is_array($startDate)) {
            $useMinMax = false;
            if (isset($startDate['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_START_DATE, $startDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startDate['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_START_DATE, $startDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_START_DATE, $startDate, $comparison);
    }

    /**
     * Filter the query on the end_date column
     *
     * Example usage:
     * <code>
     * $query->filterByEndDate('2011-03-14'); // WHERE end_date = '2011-03-14'
     * $query->filterByEndDate('now'); // WHERE end_date = '2011-03-14'
     * $query->filterByEndDate(array('max' => 'yesterday')); // WHERE end_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $endDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByEndDate($endDate = null, $comparison = null)
    {
        if (is_array($endDate)) {
            $useMinMax = false;
            if (isset($endDate['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_END_DATE, $endDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endDate['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_END_DATE, $endDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_END_DATE, $endDate, $comparison);
    }

    /**
     * Filter the query on the song_type column
     *
     * Example usage:
     * <code>
     * $query->filterBySongType(1234); // WHERE song_type = 1234
     * $query->filterBySongType(array(12, 34)); // WHERE song_type IN (12, 34)
     * $query->filterBySongType(array('min' => 12)); // WHERE song_type > 12
     * </code>
     *
     * @param     mixed $songType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterBySongType($songType = null, $comparison = null)
    {
        if (is_array($songType)) {
            $useMinMax = false;
            if (isset($songType['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_SONG_TYPE, $songType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($songType['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_SONG_TYPE, $songType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_SONG_TYPE, $songType, $comparison);
    }

    /**
     * Filter the query on the id_subcat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdSubcat(1234); // WHERE id_subcat = 1234
     * $query->filterByIdSubcat(array(12, 34)); // WHERE id_subcat IN (12, 34)
     * $query->filterByIdSubcat(array('min' => 12)); // WHERE id_subcat > 12
     * </code>
     *
     * @param     mixed $idSubcat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByIdSubcat($idSubcat = null, $comparison = null)
    {
        if (is_array($idSubcat)) {
            $useMinMax = false;
            if (isset($idSubcat['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_ID_SUBCAT, $idSubcat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idSubcat['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_ID_SUBCAT, $idSubcat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ID_SUBCAT, $idSubcat, $comparison);
    }

    /**
     * Filter the query on the id_genre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdGenre(1234); // WHERE id_genre = 1234
     * $query->filterByIdGenre(array(12, 34)); // WHERE id_genre IN (12, 34)
     * $query->filterByIdGenre(array('min' => 12)); // WHERE id_genre > 12
     * </code>
     *
     * @param     mixed $idGenre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByIdGenre($idGenre = null, $comparison = null)
    {
        if (is_array($idGenre)) {
            $useMinMax = false;
            if (isset($idGenre['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_ID_GENRE, $idGenre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idGenre['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_ID_GENRE, $idGenre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ID_GENRE, $idGenre, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the duration column
     *
     * Example usage:
     * <code>
     * $query->filterByDuration(1234); // WHERE duration = 1234
     * $query->filterByDuration(array(12, 34)); // WHERE duration IN (12, 34)
     * $query->filterByDuration(array('min' => 12)); // WHERE duration > 12
     * </code>
     *
     * @param     mixed $duration The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByDuration($duration = null, $comparison = null)
    {
        if (is_array($duration)) {
            $useMinMax = false;
            if (isset($duration['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_DURATION, $duration['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($duration['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_DURATION, $duration['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_DURATION, $duration, $comparison);
    }

    /**
     * Filter the query on the cue_times column
     *
     * Example usage:
     * <code>
     * $query->filterByCueTimes('fooValue');   // WHERE cue_times = 'fooValue'
     * $query->filterByCueTimes('%fooValue%', Criteria::LIKE); // WHERE cue_times LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cueTimes The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByCueTimes($cueTimes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cueTimes)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_CUE_TIMES, $cueTimes, $comparison);
    }

    /**
     * Filter the query on the precise_cue column
     *
     * Example usage:
     * <code>
     * $query->filterByPreciseCue(true); // WHERE precise_cue = true
     * $query->filterByPreciseCue('yes'); // WHERE precise_cue = true
     * </code>
     *
     * @param     boolean|string $preciseCue The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByPreciseCue($preciseCue = null, $comparison = null)
    {
        if (is_string($preciseCue)) {
            $preciseCue = in_array(strtolower($preciseCue), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SongsTableMap::COL_PRECISE_CUE, $preciseCue, $comparison);
    }

    /**
     * Filter the query on the fade_type column
     *
     * Example usage:
     * <code>
     * $query->filterByFadeType(true); // WHERE fade_type = true
     * $query->filterByFadeType('yes'); // WHERE fade_type = true
     * </code>
     *
     * @param     boolean|string $fadeType The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByFadeType($fadeType = null, $comparison = null)
    {
        if (is_string($fadeType)) {
            $fadeType = in_array(strtolower($fadeType), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SongsTableMap::COL_FADE_TYPE, $fadeType, $comparison);
    }

    /**
     * Filter the query on the end_type column
     *
     * Example usage:
     * <code>
     * $query->filterByEndType(true); // WHERE end_type = true
     * $query->filterByEndType('yes'); // WHERE end_type = true
     * </code>
     *
     * @param     boolean|string $endType The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByEndType($endType = null, $comparison = null)
    {
        if (is_string($endType)) {
            $endType = in_array(strtolower($endType), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SongsTableMap::COL_END_TYPE, $endType, $comparison);
    }

    /**
     * Filter the query on the overlay column
     *
     * Example usage:
     * <code>
     * $query->filterByOverlay(true); // WHERE overlay = true
     * $query->filterByOverlay('yes'); // WHERE overlay = true
     * </code>
     *
     * @param     boolean|string $overlay The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByOverlay($overlay = null, $comparison = null)
    {
        if (is_string($overlay)) {
            $overlay = in_array(strtolower($overlay), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SongsTableMap::COL_OVERLAY, $overlay, $comparison);
    }

    /**
     * Filter the query on the artist column
     *
     * Example usage:
     * <code>
     * $query->filterByArtist('fooValue');   // WHERE artist = 'fooValue'
     * $query->filterByArtist('%fooValue%', Criteria::LIKE); // WHERE artist LIKE '%fooValue%'
     * </code>
     *
     * @param     string $artist The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByArtist($artist = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($artist)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ARTIST, $artist, $comparison);
    }

    /**
     * Filter the query on the original_artist column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalArtist('fooValue');   // WHERE original_artist = 'fooValue'
     * $query->filterByOriginalArtist('%fooValue%', Criteria::LIKE); // WHERE original_artist LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalArtist The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByOriginalArtist($originalArtist = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalArtist)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ORIGINAL_ARTIST, $originalArtist, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the album column
     *
     * Example usage:
     * <code>
     * $query->filterByAlbum('fooValue');   // WHERE album = 'fooValue'
     * $query->filterByAlbum('%fooValue%', Criteria::LIKE); // WHERE album LIKE '%fooValue%'
     * </code>
     *
     * @param     string $album The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByAlbum($album = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($album)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ALBUM, $album, $comparison);
    }

    /**
     * Filter the query on the composer column
     *
     * Example usage:
     * <code>
     * $query->filterByComposer('fooValue');   // WHERE composer = 'fooValue'
     * $query->filterByComposer('%fooValue%', Criteria::LIKE); // WHERE composer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $composer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByComposer($composer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($composer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_COMPOSER, $composer, $comparison);
    }

    /**
     * Filter the query on the year column
     *
     * Example usage:
     * <code>
     * $query->filterByYear('fooValue');   // WHERE year = 'fooValue'
     * $query->filterByYear('%fooValue%', Criteria::LIKE); // WHERE year LIKE '%fooValue%'
     * </code>
     *
     * @param     string $year The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByYear($year = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($year)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_YEAR, $year, $comparison);
    }

    /**
     * Filter the query on the track_no column
     *
     * Example usage:
     * <code>
     * $query->filterByTrackNo(1234); // WHERE track_no = 1234
     * $query->filterByTrackNo(array(12, 34)); // WHERE track_no IN (12, 34)
     * $query->filterByTrackNo(array('min' => 12)); // WHERE track_no > 12
     * </code>
     *
     * @param     mixed $trackNo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByTrackNo($trackNo = null, $comparison = null)
    {
        if (is_array($trackNo)) {
            $useMinMax = false;
            if (isset($trackNo['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_TRACK_NO, $trackNo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trackNo['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_TRACK_NO, $trackNo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_TRACK_NO, $trackNo, $comparison);
    }

    /**
     * Filter the query on the disc_no column
     *
     * Example usage:
     * <code>
     * $query->filterByDiscNo(1234); // WHERE disc_no = 1234
     * $query->filterByDiscNo(array(12, 34)); // WHERE disc_no IN (12, 34)
     * $query->filterByDiscNo(array('min' => 12)); // WHERE disc_no > 12
     * </code>
     *
     * @param     mixed $discNo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByDiscNo($discNo = null, $comparison = null)
    {
        if (is_array($discNo)) {
            $useMinMax = false;
            if (isset($discNo['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_DISC_NO, $discNo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($discNo['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_DISC_NO, $discNo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_DISC_NO, $discNo, $comparison);
    }

    /**
     * Filter the query on the publisher column
     *
     * Example usage:
     * <code>
     * $query->filterByPublisher('fooValue');   // WHERE publisher = 'fooValue'
     * $query->filterByPublisher('%fooValue%', Criteria::LIKE); // WHERE publisher LIKE '%fooValue%'
     * </code>
     *
     * @param     string $publisher The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByPublisher($publisher = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($publisher)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_PUBLISHER, $publisher, $comparison);
    }

    /**
     * Filter the query on the copyright column
     *
     * Example usage:
     * <code>
     * $query->filterByCopyright('fooValue');   // WHERE copyright = 'fooValue'
     * $query->filterByCopyright('%fooValue%', Criteria::LIKE); // WHERE copyright LIKE '%fooValue%'
     * </code>
     *
     * @param     string $copyright The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByCopyright($copyright = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($copyright)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_COPYRIGHT, $copyright, $comparison);
    }

    /**
     * Filter the query on the isrc column
     *
     * Example usage:
     * <code>
     * $query->filterByIsrc('fooValue');   // WHERE isrc = 'fooValue'
     * $query->filterByIsrc('%fooValue%', Criteria::LIKE); // WHERE isrc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $isrc The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByIsrc($isrc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($isrc)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ISRC, $isrc, $comparison);
    }

    /**
     * Filter the query on the bpm column
     *
     * Example usage:
     * <code>
     * $query->filterByBpm(1234); // WHERE bpm = 1234
     * $query->filterByBpm(array(12, 34)); // WHERE bpm IN (12, 34)
     * $query->filterByBpm(array('min' => 12)); // WHERE bpm > 12
     * </code>
     *
     * @param     mixed $bpm The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByBpm($bpm = null, $comparison = null)
    {
        if (is_array($bpm)) {
            $useMinMax = false;
            if (isset($bpm['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_BPM, $bpm['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bpm['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_BPM, $bpm['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_BPM, $bpm, $comparison);
    }

    /**
     * Filter the query on the comments column
     *
     * Example usage:
     * <code>
     * $query->filterByComments('fooValue');   // WHERE comments = 'fooValue'
     * $query->filterByComments('%fooValue%', Criteria::LIKE); // WHERE comments LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comments The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByComments($comments = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comments)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_COMMENTS, $comments, $comparison);
    }

    /**
     * Filter the query on the sweepers column
     *
     * Example usage:
     * <code>
     * $query->filterBySweepers('fooValue');   // WHERE sweepers = 'fooValue'
     * $query->filterBySweepers('%fooValue%', Criteria::LIKE); // WHERE sweepers LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sweepers The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterBySweepers($sweepers = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sweepers)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_SWEEPERS, $sweepers, $comparison);
    }

    /**
     * Filter the query on the album_art column
     *
     * Example usage:
     * <code>
     * $query->filterByAlbumArt('fooValue');   // WHERE album_art = 'fooValue'
     * $query->filterByAlbumArt('%fooValue%', Criteria::LIKE); // WHERE album_art LIKE '%fooValue%'
     * </code>
     *
     * @param     string $albumArt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByAlbumArt($albumArt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($albumArt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_ALBUM_ART, $albumArt, $comparison);
    }

    /**
     * Filter the query on the buy_link column
     *
     * Example usage:
     * <code>
     * $query->filterByBuyLink('fooValue');   // WHERE buy_link = 'fooValue'
     * $query->filterByBuyLink('%fooValue%', Criteria::LIKE); // WHERE buy_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $buyLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByBuyLink($buyLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($buyLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_BUY_LINK, $buyLink, $comparison);
    }

    /**
     * Filter the query on the tdate_played column
     *
     * Example usage:
     * <code>
     * $query->filterByTdatePlayed('2011-03-14'); // WHERE tdate_played = '2011-03-14'
     * $query->filterByTdatePlayed('now'); // WHERE tdate_played = '2011-03-14'
     * $query->filterByTdatePlayed(array('max' => 'yesterday')); // WHERE tdate_played > '2011-03-13'
     * </code>
     *
     * @param     mixed $tdatePlayed The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByTdatePlayed($tdatePlayed = null, $comparison = null)
    {
        if (is_array($tdatePlayed)) {
            $useMinMax = false;
            if (isset($tdatePlayed['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_TDATE_PLAYED, $tdatePlayed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tdatePlayed['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_TDATE_PLAYED, $tdatePlayed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_TDATE_PLAYED, $tdatePlayed, $comparison);
    }

    /**
     * Filter the query on the tartist_played column
     *
     * Example usage:
     * <code>
     * $query->filterByTartistPlayed('2011-03-14'); // WHERE tartist_played = '2011-03-14'
     * $query->filterByTartistPlayed('now'); // WHERE tartist_played = '2011-03-14'
     * $query->filterByTartistPlayed(array('max' => 'yesterday')); // WHERE tartist_played > '2011-03-13'
     * </code>
     *
     * @param     mixed $tartistPlayed The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByTartistPlayed($tartistPlayed = null, $comparison = null)
    {
        if (is_array($tartistPlayed)) {
            $useMinMax = false;
            if (isset($tartistPlayed['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_TARTIST_PLAYED, $tartistPlayed['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tartistPlayed['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_TARTIST_PLAYED, $tartistPlayed['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_TARTIST_PLAYED, $tartistPlayed, $comparison);
    }

    /**
     * Filter the query on the date_added column
     *
     * Example usage:
     * <code>
     * $query->filterByDateAdded('2011-03-14'); // WHERE date_added = '2011-03-14'
     * $query->filterByDateAdded('now'); // WHERE date_added = '2011-03-14'
     * $query->filterByDateAdded(array('max' => 'yesterday')); // WHERE date_added > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateAdded The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByDateAdded($dateAdded = null, $comparison = null)
    {
        if (is_array($dateAdded)) {
            $useMinMax = false;
            if (isset($dateAdded['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_DATE_ADDED, $dateAdded['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateAdded['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_DATE_ADDED, $dateAdded['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_DATE_ADDED, $dateAdded, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the version column
     *
     * Example usage:
     * <code>
     * $query->filterByVersion(1234); // WHERE version = 1234
     * $query->filterByVersion(array(12, 34)); // WHERE version IN (12, 34)
     * $query->filterByVersion(array('min' => 12)); // WHERE version > 12
     * </code>
     *
     * @param     mixed $version The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByVersion($version = null, $comparison = null)
    {
        if (is_array($version)) {
            $useMinMax = false;
            if (isset($version['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_VERSION, $version['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($version['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_VERSION, $version['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_VERSION, $version, $comparison);
    }

    /**
     * Filter the query on the version_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByVersionCreatedAt('2011-03-14'); // WHERE version_created_at = '2011-03-14'
     * $query->filterByVersionCreatedAt('now'); // WHERE version_created_at = '2011-03-14'
     * $query->filterByVersionCreatedAt(array('max' => 'yesterday')); // WHERE version_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $versionCreatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByVersionCreatedAt($versionCreatedAt = null, $comparison = null)
    {
        if (is_array($versionCreatedAt)) {
            $useMinMax = false;
            if (isset($versionCreatedAt['min'])) {
                $this->addUsingAlias(SongsTableMap::COL_VERSION_CREATED_AT, $versionCreatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($versionCreatedAt['max'])) {
                $this->addUsingAlias(SongsTableMap::COL_VERSION_CREATED_AT, $versionCreatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_VERSION_CREATED_AT, $versionCreatedAt, $comparison);
    }

    /**
     * Filter the query on the version_created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByVersionCreatedBy('fooValue');   // WHERE version_created_by = 'fooValue'
     * $query->filterByVersionCreatedBy('%fooValue%', Criteria::LIKE); // WHERE version_created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versionCreatedBy The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByVersionCreatedBy($versionCreatedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versionCreatedBy)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_VERSION_CREATED_BY, $versionCreatedBy, $comparison);
    }

    /**
     * Filter the query on the version_comment column
     *
     * Example usage:
     * <code>
     * $query->filterByVersionComment('fooValue');   // WHERE version_comment = 'fooValue'
     * $query->filterByVersionComment('%fooValue%', Criteria::LIKE); // WHERE version_comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $versionComment The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function filterByVersionComment($versionComment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($versionComment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SongsTableMap::COL_VERSION_COMMENT, $versionComment, $comparison);
    }

    /**
     * Filter the query by a related \Model\Song\SongsVersion object
     *
     * @param \Model\Song\SongsVersion|ObjectCollection $songsVersion the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSongsQuery The current query, for fluid interface
     */
    public function filterBySongsVersion($songsVersion, $comparison = null)
    {
        if ($songsVersion instanceof \Model\Song\SongsVersion) {
            return $this
                ->addUsingAlias(SongsTableMap::COL_ID, $songsVersion->getId(), $comparison);
        } elseif ($songsVersion instanceof ObjectCollection) {
            return $this
                ->useSongsVersionQuery()
                ->filterByPrimaryKeys($songsVersion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySongsVersion() only accepts arguments of type \Model\Song\SongsVersion or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SongsVersion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function joinSongsVersion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SongsVersion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SongsVersion');
        }

        return $this;
    }

    /**
     * Use the SongsVersion relation SongsVersion object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Song\SongsVersionQuery A secondary query class using the current class as primary query
     */
    public function useSongsVersionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSongsVersion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SongsVersion', '\Model\Song\SongsVersionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSongs $songs Object to remove from the list of results
     *
     * @return $this|ChildSongsQuery The current query, for fluid interface
     */
    public function prune($songs = null)
    {
        if ($songs) {
            $this->addUsingAlias(SongsTableMap::COL_ID, $songs->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the songs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SongsTableMap::clearInstancePool();
            SongsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SongsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SongsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SongsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // versionable behavior

    /**
     * Checks whether versioning is enabled
     *
     * @return boolean
     */
    static public function isVersioningEnabled()
    {
        return self::$isVersioningEnabled;
    }

    /**
     * Enables versioning
     */
    static public function enableVersioning()
    {
        self::$isVersioningEnabled = true;
    }

    /**
     * Disables versioning
     */
    static public function disableVersioning()
    {
        self::$isVersioningEnabled = false;
    }

} // SongsQuery
