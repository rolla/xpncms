<?php

namespace Model\Song\Map;

use Model\Song\SongsInformation;
use Model\Song\SongsInformationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'songs_information' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SongsInformationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Song.Map.SongsInformationTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'songs_information';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Song\\SongsInformation';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Song.SongsInformation';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 25;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 25;

    /**
     * the column name for the id field
     */
    const COL_ID = 'songs_information.id';

    /**
     * the column name for the song_id field
     */
    const COL_SONG_ID = 'songs_information.song_id';

    /**
     * the column name for the artist field
     */
    const COL_ARTIST = 'songs_information.artist';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'songs_information.title';

    /**
     * the column name for the remix field
     */
    const COL_REMIX = 'songs_information.remix';

    /**
     * the column name for the label field
     */
    const COL_LABEL = 'songs_information.label';

    /**
     * the column name for the genre field
     */
    const COL_GENRE = 'songs_information.genre';

    /**
     * the column name for the url field
     */
    const COL_URL = 'songs_information.url';

    /**
     * the column name for the beatport field
     */
    const COL_BEATPORT = 'songs_information.beatport';

    /**
     * the column name for the soundcloud field
     */
    const COL_SOUNDCLOUD = 'songs_information.soundcloud';

    /**
     * the column name for the youtube field
     */
    const COL_YOUTUBE = 'songs_information.youtube';

    /**
     * the column name for the itunes field
     */
    const COL_ITUNES = 'songs_information.itunes';

    /**
     * the column name for the cover field
     */
    const COL_COVER = 'songs_information.cover';

    /**
     * the column name for the added_by field
     */
    const COL_ADDED_BY = 'songs_information.added_by';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'songs_information.user_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'songs_information.status';

    /**
     * the column name for the start field
     */
    const COL_START = 'songs_information.start';

    /**
     * the column name for the end field
     */
    const COL_END = 'songs_information.end';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'songs_information.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'songs_information.updated_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'songs_information.deleted_at';

    /**
     * the column name for the version field
     */
    const COL_VERSION = 'songs_information.version';

    /**
     * the column name for the version_created_at field
     */
    const COL_VERSION_CREATED_AT = 'songs_information.version_created_at';

    /**
     * the column name for the version_created_by field
     */
    const COL_VERSION_CREATED_BY = 'songs_information.version_created_by';

    /**
     * the column name for the version_comment field
     */
    const COL_VERSION_COMMENT = 'songs_information.version_comment';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SongId', 'Artist', 'Title', 'Remix', 'Label', 'Genre', 'Url', 'Beatport', 'Soundcloud', 'Youtube', 'Itunes', 'Cover', 'AddedBy', 'UserId', 'Status', 'Start', 'End', 'CreatedAt', 'UpdatedAt', 'DeletedAt', 'Version', 'VersionCreatedAt', 'VersionCreatedBy', 'VersionComment', ),
        self::TYPE_CAMELNAME     => array('id', 'songId', 'artist', 'title', 'remix', 'label', 'genre', 'url', 'beatport', 'soundcloud', 'youtube', 'itunes', 'cover', 'addedBy', 'userId', 'status', 'start', 'end', 'createdAt', 'updatedAt', 'deletedAt', 'version', 'versionCreatedAt', 'versionCreatedBy', 'versionComment', ),
        self::TYPE_COLNAME       => array(SongsInformationTableMap::COL_ID, SongsInformationTableMap::COL_SONG_ID, SongsInformationTableMap::COL_ARTIST, SongsInformationTableMap::COL_TITLE, SongsInformationTableMap::COL_REMIX, SongsInformationTableMap::COL_LABEL, SongsInformationTableMap::COL_GENRE, SongsInformationTableMap::COL_URL, SongsInformationTableMap::COL_BEATPORT, SongsInformationTableMap::COL_SOUNDCLOUD, SongsInformationTableMap::COL_YOUTUBE, SongsInformationTableMap::COL_ITUNES, SongsInformationTableMap::COL_COVER, SongsInformationTableMap::COL_ADDED_BY, SongsInformationTableMap::COL_USER_ID, SongsInformationTableMap::COL_STATUS, SongsInformationTableMap::COL_START, SongsInformationTableMap::COL_END, SongsInformationTableMap::COL_CREATED_AT, SongsInformationTableMap::COL_UPDATED_AT, SongsInformationTableMap::COL_DELETED_AT, SongsInformationTableMap::COL_VERSION, SongsInformationTableMap::COL_VERSION_CREATED_AT, SongsInformationTableMap::COL_VERSION_CREATED_BY, SongsInformationTableMap::COL_VERSION_COMMENT, ),
        self::TYPE_FIELDNAME     => array('id', 'song_id', 'artist', 'title', 'remix', 'label', 'genre', 'url', 'beatport', 'soundcloud', 'youtube', 'itunes', 'cover', 'added_by', 'user_id', 'status', 'start', 'end', 'created_at', 'updated_at', 'deleted_at', 'version', 'version_created_at', 'version_created_by', 'version_comment', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SongId' => 1, 'Artist' => 2, 'Title' => 3, 'Remix' => 4, 'Label' => 5, 'Genre' => 6, 'Url' => 7, 'Beatport' => 8, 'Soundcloud' => 9, 'Youtube' => 10, 'Itunes' => 11, 'Cover' => 12, 'AddedBy' => 13, 'UserId' => 14, 'Status' => 15, 'Start' => 16, 'End' => 17, 'CreatedAt' => 18, 'UpdatedAt' => 19, 'DeletedAt' => 20, 'Version' => 21, 'VersionCreatedAt' => 22, 'VersionCreatedBy' => 23, 'VersionComment' => 24, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'songId' => 1, 'artist' => 2, 'title' => 3, 'remix' => 4, 'label' => 5, 'genre' => 6, 'url' => 7, 'beatport' => 8, 'soundcloud' => 9, 'youtube' => 10, 'itunes' => 11, 'cover' => 12, 'addedBy' => 13, 'userId' => 14, 'status' => 15, 'start' => 16, 'end' => 17, 'createdAt' => 18, 'updatedAt' => 19, 'deletedAt' => 20, 'version' => 21, 'versionCreatedAt' => 22, 'versionCreatedBy' => 23, 'versionComment' => 24, ),
        self::TYPE_COLNAME       => array(SongsInformationTableMap::COL_ID => 0, SongsInformationTableMap::COL_SONG_ID => 1, SongsInformationTableMap::COL_ARTIST => 2, SongsInformationTableMap::COL_TITLE => 3, SongsInformationTableMap::COL_REMIX => 4, SongsInformationTableMap::COL_LABEL => 5, SongsInformationTableMap::COL_GENRE => 6, SongsInformationTableMap::COL_URL => 7, SongsInformationTableMap::COL_BEATPORT => 8, SongsInformationTableMap::COL_SOUNDCLOUD => 9, SongsInformationTableMap::COL_YOUTUBE => 10, SongsInformationTableMap::COL_ITUNES => 11, SongsInformationTableMap::COL_COVER => 12, SongsInformationTableMap::COL_ADDED_BY => 13, SongsInformationTableMap::COL_USER_ID => 14, SongsInformationTableMap::COL_STATUS => 15, SongsInformationTableMap::COL_START => 16, SongsInformationTableMap::COL_END => 17, SongsInformationTableMap::COL_CREATED_AT => 18, SongsInformationTableMap::COL_UPDATED_AT => 19, SongsInformationTableMap::COL_DELETED_AT => 20, SongsInformationTableMap::COL_VERSION => 21, SongsInformationTableMap::COL_VERSION_CREATED_AT => 22, SongsInformationTableMap::COL_VERSION_CREATED_BY => 23, SongsInformationTableMap::COL_VERSION_COMMENT => 24, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'song_id' => 1, 'artist' => 2, 'title' => 3, 'remix' => 4, 'label' => 5, 'genre' => 6, 'url' => 7, 'beatport' => 8, 'soundcloud' => 9, 'youtube' => 10, 'itunes' => 11, 'cover' => 12, 'added_by' => 13, 'user_id' => 14, 'status' => 15, 'start' => 16, 'end' => 17, 'created_at' => 18, 'updated_at' => 19, 'deleted_at' => 20, 'version' => 21, 'version_created_at' => 22, 'version_created_by' => 23, 'version_comment' => 24, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('songs_information');
        $this->setPhpName('SongsInformation');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Song\\SongsInformation');
        $this->setPackage('Model.Song');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('song_id', 'SongId', 'INTEGER', true, 10, null);
        $this->addColumn('artist', 'Artist', 'VARCHAR', false, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('remix', 'Remix', 'VARCHAR', false, 255, null);
        $this->addColumn('label', 'Label', 'VARCHAR', false, 255, null);
        $this->addColumn('genre', 'Genre', 'INTEGER', false, null, null);
        $this->addColumn('url', 'Url', 'VARCHAR', false, 1000, null);
        $this->addColumn('beatport', 'Beatport', 'VARCHAR', false, 1000, null);
        $this->addColumn('soundcloud', 'Soundcloud', 'VARCHAR', false, 1000, null);
        $this->addColumn('youtube', 'Youtube', 'VARCHAR', false, 1000, null);
        $this->addColumn('itunes', 'Itunes', 'VARCHAR', false, 1000, null);
        $this->addColumn('cover', 'Cover', 'LONGVARCHAR', false, null, null);
        $this->addColumn('added_by', 'AddedBy', 'INTEGER', false, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', false, null, null);
        $this->addColumn('status', 'Status', 'TINYINT', false, null, null);
        $this->addColumn('start', 'Start', 'TIMESTAMP', false, null, null);
        $this->addColumn('end', 'End', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('version', 'Version', 'INTEGER', false, null, 0);
        $this->addColumn('version_created_at', 'VersionCreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('version_created_by', 'VersionCreatedBy', 'VARCHAR', false, 100, null);
        $this->addColumn('version_comment', 'VersionComment', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SongsInformationVersion', '\\Model\\Song\\SongsInformationVersion', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SongsInformationVersions', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'versionable' => array('version_column' => 'version', 'version_table' => '', 'log_created_at' => 'true', 'log_created_by' => 'true', 'log_comment' => 'true', 'version_created_at_column' => 'version_created_at', 'version_created_by_column' => 'version_created_by', 'version_comment_column' => 'version_comment', 'indices' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to songs_information     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SongsInformationVersionTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SongsInformationTableMap::CLASS_DEFAULT : SongsInformationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SongsInformation object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SongsInformationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SongsInformationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SongsInformationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SongsInformationTableMap::OM_CLASS;
            /** @var SongsInformation $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SongsInformationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SongsInformationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SongsInformationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SongsInformation $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SongsInformationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SongsInformationTableMap::COL_ID);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_SONG_ID);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_ARTIST);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_TITLE);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_REMIX);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_LABEL);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_GENRE);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_URL);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_BEATPORT);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_SOUNDCLOUD);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_YOUTUBE);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_ITUNES);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_COVER);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_ADDED_BY);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_USER_ID);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_STATUS);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_START);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_END);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_VERSION);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_VERSION_CREATED_AT);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_VERSION_CREATED_BY);
            $criteria->addSelectColumn(SongsInformationTableMap::COL_VERSION_COMMENT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.song_id');
            $criteria->addSelectColumn($alias . '.artist');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.remix');
            $criteria->addSelectColumn($alias . '.label');
            $criteria->addSelectColumn($alias . '.genre');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.beatport');
            $criteria->addSelectColumn($alias . '.soundcloud');
            $criteria->addSelectColumn($alias . '.youtube');
            $criteria->addSelectColumn($alias . '.itunes');
            $criteria->addSelectColumn($alias . '.cover');
            $criteria->addSelectColumn($alias . '.added_by');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.start');
            $criteria->addSelectColumn($alias . '.end');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.version');
            $criteria->addSelectColumn($alias . '.version_created_at');
            $criteria->addSelectColumn($alias . '.version_created_by');
            $criteria->addSelectColumn($alias . '.version_comment');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SongsInformationTableMap::DATABASE_NAME)->getTable(SongsInformationTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SongsInformationTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SongsInformationTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SongsInformationTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SongsInformation or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SongsInformation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Song\SongsInformation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SongsInformationTableMap::DATABASE_NAME);
            $criteria->add(SongsInformationTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SongsInformationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SongsInformationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SongsInformationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the songs_information table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SongsInformationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SongsInformation or Criteria object.
     *
     * @param mixed               $criteria Criteria or SongsInformation object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsInformationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SongsInformation object
        }

        if ($criteria->containsKey(SongsInformationTableMap::COL_ID) && $criteria->keyContainsValue(SongsInformationTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SongsInformationTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SongsInformationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SongsInformationTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SongsInformationTableMap::buildTableMap();
