<?php

namespace Model\Song\Map;

use Model\Song\Songs;
use Model\Song\SongsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'songs' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SongsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Song.Map.SongsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'songs';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Song\\Songs';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Song.Songs';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 44;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 44;

    /**
     * the column name for the ID field
     */
    const COL_ID = 'songs.ID';

    /**
     * the column name for the path field
     */
    const COL_PATH = 'songs.path';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'songs.enabled';

    /**
     * the column name for the date_played field
     */
    const COL_DATE_PLAYED = 'songs.date_played';

    /**
     * the column name for the artist_played field
     */
    const COL_ARTIST_PLAYED = 'songs.artist_played';

    /**
     * the column name for the count_played field
     */
    const COL_COUNT_PLAYED = 'songs.count_played';

    /**
     * the column name for the play_limit field
     */
    const COL_PLAY_LIMIT = 'songs.play_limit';

    /**
     * the column name for the limit_action field
     */
    const COL_LIMIT_ACTION = 'songs.limit_action';

    /**
     * the column name for the start_date field
     */
    const COL_START_DATE = 'songs.start_date';

    /**
     * the column name for the end_date field
     */
    const COL_END_DATE = 'songs.end_date';

    /**
     * the column name for the song_type field
     */
    const COL_SONG_TYPE = 'songs.song_type';

    /**
     * the column name for the id_subcat field
     */
    const COL_ID_SUBCAT = 'songs.id_subcat';

    /**
     * the column name for the id_genre field
     */
    const COL_ID_GENRE = 'songs.id_genre';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'songs.weight';

    /**
     * the column name for the duration field
     */
    const COL_DURATION = 'songs.duration';

    /**
     * the column name for the cue_times field
     */
    const COL_CUE_TIMES = 'songs.cue_times';

    /**
     * the column name for the precise_cue field
     */
    const COL_PRECISE_CUE = 'songs.precise_cue';

    /**
     * the column name for the fade_type field
     */
    const COL_FADE_TYPE = 'songs.fade_type';

    /**
     * the column name for the end_type field
     */
    const COL_END_TYPE = 'songs.end_type';

    /**
     * the column name for the overlay field
     */
    const COL_OVERLAY = 'songs.overlay';

    /**
     * the column name for the artist field
     */
    const COL_ARTIST = 'songs.artist';

    /**
     * the column name for the original_artist field
     */
    const COL_ORIGINAL_ARTIST = 'songs.original_artist';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'songs.title';

    /**
     * the column name for the album field
     */
    const COL_ALBUM = 'songs.album';

    /**
     * the column name for the composer field
     */
    const COL_COMPOSER = 'songs.composer';

    /**
     * the column name for the year field
     */
    const COL_YEAR = 'songs.year';

    /**
     * the column name for the track_no field
     */
    const COL_TRACK_NO = 'songs.track_no';

    /**
     * the column name for the disc_no field
     */
    const COL_DISC_NO = 'songs.disc_no';

    /**
     * the column name for the publisher field
     */
    const COL_PUBLISHER = 'songs.publisher';

    /**
     * the column name for the copyright field
     */
    const COL_COPYRIGHT = 'songs.copyright';

    /**
     * the column name for the isrc field
     */
    const COL_ISRC = 'songs.isrc';

    /**
     * the column name for the bpm field
     */
    const COL_BPM = 'songs.bpm';

    /**
     * the column name for the comments field
     */
    const COL_COMMENTS = 'songs.comments';

    /**
     * the column name for the sweepers field
     */
    const COL_SWEEPERS = 'songs.sweepers';

    /**
     * the column name for the album_art field
     */
    const COL_ALBUM_ART = 'songs.album_art';

    /**
     * the column name for the buy_link field
     */
    const COL_BUY_LINK = 'songs.buy_link';

    /**
     * the column name for the tdate_played field
     */
    const COL_TDATE_PLAYED = 'songs.tdate_played';

    /**
     * the column name for the tartist_played field
     */
    const COL_TARTIST_PLAYED = 'songs.tartist_played';

    /**
     * the column name for the date_added field
     */
    const COL_DATE_ADDED = 'songs.date_added';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'songs.updated_at';

    /**
     * the column name for the version field
     */
    const COL_VERSION = 'songs.version';

    /**
     * the column name for the version_created_at field
     */
    const COL_VERSION_CREATED_AT = 'songs.version_created_at';

    /**
     * the column name for the version_created_by field
     */
    const COL_VERSION_CREATED_BY = 'songs.version_created_by';

    /**
     * the column name for the version_comment field
     */
    const COL_VERSION_COMMENT = 'songs.version_comment';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Path', 'Enabled', 'DatePlayed', 'ArtistPlayed', 'CountPlayed', 'PlayLimit', 'LimitAction', 'StartDate', 'EndDate', 'SongType', 'IdSubcat', 'IdGenre', 'Weight', 'Duration', 'CueTimes', 'PreciseCue', 'FadeType', 'EndType', 'Overlay', 'Artist', 'OriginalArtist', 'Title', 'Album', 'Composer', 'Year', 'TrackNo', 'DiscNo', 'Publisher', 'Copyright', 'Isrc', 'Bpm', 'Comments', 'Sweepers', 'AlbumArt', 'BuyLink', 'TdatePlayed', 'TartistPlayed', 'DateAdded', 'UpdatedAt', 'Version', 'VersionCreatedAt', 'VersionCreatedBy', 'VersionComment', ),
        self::TYPE_CAMELNAME     => array('id', 'path', 'enabled', 'datePlayed', 'artistPlayed', 'countPlayed', 'playLimit', 'limitAction', 'startDate', 'endDate', 'songType', 'idSubcat', 'idGenre', 'weight', 'duration', 'cueTimes', 'preciseCue', 'fadeType', 'endType', 'overlay', 'artist', 'originalArtist', 'title', 'album', 'composer', 'year', 'trackNo', 'discNo', 'publisher', 'copyright', 'isrc', 'bpm', 'comments', 'sweepers', 'albumArt', 'buyLink', 'tdatePlayed', 'tartistPlayed', 'dateAdded', 'updatedAt', 'version', 'versionCreatedAt', 'versionCreatedBy', 'versionComment', ),
        self::TYPE_COLNAME       => array(SongsTableMap::COL_ID, SongsTableMap::COL_PATH, SongsTableMap::COL_ENABLED, SongsTableMap::COL_DATE_PLAYED, SongsTableMap::COL_ARTIST_PLAYED, SongsTableMap::COL_COUNT_PLAYED, SongsTableMap::COL_PLAY_LIMIT, SongsTableMap::COL_LIMIT_ACTION, SongsTableMap::COL_START_DATE, SongsTableMap::COL_END_DATE, SongsTableMap::COL_SONG_TYPE, SongsTableMap::COL_ID_SUBCAT, SongsTableMap::COL_ID_GENRE, SongsTableMap::COL_WEIGHT, SongsTableMap::COL_DURATION, SongsTableMap::COL_CUE_TIMES, SongsTableMap::COL_PRECISE_CUE, SongsTableMap::COL_FADE_TYPE, SongsTableMap::COL_END_TYPE, SongsTableMap::COL_OVERLAY, SongsTableMap::COL_ARTIST, SongsTableMap::COL_ORIGINAL_ARTIST, SongsTableMap::COL_TITLE, SongsTableMap::COL_ALBUM, SongsTableMap::COL_COMPOSER, SongsTableMap::COL_YEAR, SongsTableMap::COL_TRACK_NO, SongsTableMap::COL_DISC_NO, SongsTableMap::COL_PUBLISHER, SongsTableMap::COL_COPYRIGHT, SongsTableMap::COL_ISRC, SongsTableMap::COL_BPM, SongsTableMap::COL_COMMENTS, SongsTableMap::COL_SWEEPERS, SongsTableMap::COL_ALBUM_ART, SongsTableMap::COL_BUY_LINK, SongsTableMap::COL_TDATE_PLAYED, SongsTableMap::COL_TARTIST_PLAYED, SongsTableMap::COL_DATE_ADDED, SongsTableMap::COL_UPDATED_AT, SongsTableMap::COL_VERSION, SongsTableMap::COL_VERSION_CREATED_AT, SongsTableMap::COL_VERSION_CREATED_BY, SongsTableMap::COL_VERSION_COMMENT, ),
        self::TYPE_FIELDNAME     => array('ID', 'path', 'enabled', 'date_played', 'artist_played', 'count_played', 'play_limit', 'limit_action', 'start_date', 'end_date', 'song_type', 'id_subcat', 'id_genre', 'weight', 'duration', 'cue_times', 'precise_cue', 'fade_type', 'end_type', 'overlay', 'artist', 'original_artist', 'title', 'album', 'composer', 'year', 'track_no', 'disc_no', 'publisher', 'copyright', 'isrc', 'bpm', 'comments', 'sweepers', 'album_art', 'buy_link', 'tdate_played', 'tartist_played', 'date_added', 'updated_at', 'version', 'version_created_at', 'version_created_by', 'version_comment', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Path' => 1, 'Enabled' => 2, 'DatePlayed' => 3, 'ArtistPlayed' => 4, 'CountPlayed' => 5, 'PlayLimit' => 6, 'LimitAction' => 7, 'StartDate' => 8, 'EndDate' => 9, 'SongType' => 10, 'IdSubcat' => 11, 'IdGenre' => 12, 'Weight' => 13, 'Duration' => 14, 'CueTimes' => 15, 'PreciseCue' => 16, 'FadeType' => 17, 'EndType' => 18, 'Overlay' => 19, 'Artist' => 20, 'OriginalArtist' => 21, 'Title' => 22, 'Album' => 23, 'Composer' => 24, 'Year' => 25, 'TrackNo' => 26, 'DiscNo' => 27, 'Publisher' => 28, 'Copyright' => 29, 'Isrc' => 30, 'Bpm' => 31, 'Comments' => 32, 'Sweepers' => 33, 'AlbumArt' => 34, 'BuyLink' => 35, 'TdatePlayed' => 36, 'TartistPlayed' => 37, 'DateAdded' => 38, 'UpdatedAt' => 39, 'Version' => 40, 'VersionCreatedAt' => 41, 'VersionCreatedBy' => 42, 'VersionComment' => 43, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'path' => 1, 'enabled' => 2, 'datePlayed' => 3, 'artistPlayed' => 4, 'countPlayed' => 5, 'playLimit' => 6, 'limitAction' => 7, 'startDate' => 8, 'endDate' => 9, 'songType' => 10, 'idSubcat' => 11, 'idGenre' => 12, 'weight' => 13, 'duration' => 14, 'cueTimes' => 15, 'preciseCue' => 16, 'fadeType' => 17, 'endType' => 18, 'overlay' => 19, 'artist' => 20, 'originalArtist' => 21, 'title' => 22, 'album' => 23, 'composer' => 24, 'year' => 25, 'trackNo' => 26, 'discNo' => 27, 'publisher' => 28, 'copyright' => 29, 'isrc' => 30, 'bpm' => 31, 'comments' => 32, 'sweepers' => 33, 'albumArt' => 34, 'buyLink' => 35, 'tdatePlayed' => 36, 'tartistPlayed' => 37, 'dateAdded' => 38, 'updatedAt' => 39, 'version' => 40, 'versionCreatedAt' => 41, 'versionCreatedBy' => 42, 'versionComment' => 43, ),
        self::TYPE_COLNAME       => array(SongsTableMap::COL_ID => 0, SongsTableMap::COL_PATH => 1, SongsTableMap::COL_ENABLED => 2, SongsTableMap::COL_DATE_PLAYED => 3, SongsTableMap::COL_ARTIST_PLAYED => 4, SongsTableMap::COL_COUNT_PLAYED => 5, SongsTableMap::COL_PLAY_LIMIT => 6, SongsTableMap::COL_LIMIT_ACTION => 7, SongsTableMap::COL_START_DATE => 8, SongsTableMap::COL_END_DATE => 9, SongsTableMap::COL_SONG_TYPE => 10, SongsTableMap::COL_ID_SUBCAT => 11, SongsTableMap::COL_ID_GENRE => 12, SongsTableMap::COL_WEIGHT => 13, SongsTableMap::COL_DURATION => 14, SongsTableMap::COL_CUE_TIMES => 15, SongsTableMap::COL_PRECISE_CUE => 16, SongsTableMap::COL_FADE_TYPE => 17, SongsTableMap::COL_END_TYPE => 18, SongsTableMap::COL_OVERLAY => 19, SongsTableMap::COL_ARTIST => 20, SongsTableMap::COL_ORIGINAL_ARTIST => 21, SongsTableMap::COL_TITLE => 22, SongsTableMap::COL_ALBUM => 23, SongsTableMap::COL_COMPOSER => 24, SongsTableMap::COL_YEAR => 25, SongsTableMap::COL_TRACK_NO => 26, SongsTableMap::COL_DISC_NO => 27, SongsTableMap::COL_PUBLISHER => 28, SongsTableMap::COL_COPYRIGHT => 29, SongsTableMap::COL_ISRC => 30, SongsTableMap::COL_BPM => 31, SongsTableMap::COL_COMMENTS => 32, SongsTableMap::COL_SWEEPERS => 33, SongsTableMap::COL_ALBUM_ART => 34, SongsTableMap::COL_BUY_LINK => 35, SongsTableMap::COL_TDATE_PLAYED => 36, SongsTableMap::COL_TARTIST_PLAYED => 37, SongsTableMap::COL_DATE_ADDED => 38, SongsTableMap::COL_UPDATED_AT => 39, SongsTableMap::COL_VERSION => 40, SongsTableMap::COL_VERSION_CREATED_AT => 41, SongsTableMap::COL_VERSION_CREATED_BY => 42, SongsTableMap::COL_VERSION_COMMENT => 43, ),
        self::TYPE_FIELDNAME     => array('ID' => 0, 'path' => 1, 'enabled' => 2, 'date_played' => 3, 'artist_played' => 4, 'count_played' => 5, 'play_limit' => 6, 'limit_action' => 7, 'start_date' => 8, 'end_date' => 9, 'song_type' => 10, 'id_subcat' => 11, 'id_genre' => 12, 'weight' => 13, 'duration' => 14, 'cue_times' => 15, 'precise_cue' => 16, 'fade_type' => 17, 'end_type' => 18, 'overlay' => 19, 'artist' => 20, 'original_artist' => 21, 'title' => 22, 'album' => 23, 'composer' => 24, 'year' => 25, 'track_no' => 26, 'disc_no' => 27, 'publisher' => 28, 'copyright' => 29, 'isrc' => 30, 'bpm' => 31, 'comments' => 32, 'sweepers' => 33, 'album_art' => 34, 'buy_link' => 35, 'tdate_played' => 36, 'tartist_played' => 37, 'date_added' => 38, 'updated_at' => 39, 'version' => 40, 'version_created_at' => 41, 'version_created_by' => 42, 'version_comment' => 43, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('songs');
        $this->setPhpName('Songs');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Song\\Songs');
        $this->setPackage('Model.Song');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('path', 'Path', 'VARCHAR', true, 255, null);
        $this->addColumn('enabled', 'Enabled', 'INTEGER', true, 1, 0);
        $this->addColumn('date_played', 'DatePlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('artist_played', 'ArtistPlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('count_played', 'CountPlayed', 'SMALLINT', true, 9, 0);
        $this->addColumn('play_limit', 'PlayLimit', 'INTEGER', true, null, 0);
        $this->addColumn('limit_action', 'LimitAction', 'INTEGER', true, 1, 0);
        $this->addColumn('start_date', 'StartDate', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('end_date', 'EndDate', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('song_type', 'SongType', 'TINYINT', true, 2, null);
        $this->addColumn('id_subcat', 'IdSubcat', 'INTEGER', true, null, null);
        $this->addColumn('id_genre', 'IdGenre', 'INTEGER', true, null, null);
        $this->addColumn('weight', 'Weight', 'DOUBLE', true, 5, 50);
        $this->addColumn('duration', 'Duration', 'DOUBLE', true, 11, null);
        $this->addColumn('cue_times', 'CueTimes', 'VARCHAR', true, 255, '&');
        $this->addColumn('precise_cue', 'PreciseCue', 'BOOLEAN', true, 1, false);
        $this->addColumn('fade_type', 'FadeType', 'BOOLEAN', true, 1, false);
        $this->addColumn('end_type', 'EndType', 'BOOLEAN', true, 1, false);
        $this->addColumn('overlay', 'Overlay', 'BOOLEAN', true, 1, false);
        $this->addColumn('artist', 'Artist', 'VARCHAR', true, 255, null);
        $this->addColumn('original_artist', 'OriginalArtist', 'VARCHAR', true, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('album', 'Album', 'VARCHAR', true, 255, null);
        $this->addColumn('composer', 'Composer', 'VARCHAR', true, 255, null);
        $this->addColumn('year', 'Year', 'VARCHAR', true, 4, '1900');
        $this->addColumn('track_no', 'TrackNo', 'SMALLINT', true, null, 0);
        $this->addColumn('disc_no', 'DiscNo', 'SMALLINT', true, null, 0);
        $this->addColumn('publisher', 'Publisher', 'VARCHAR', true, 255, null);
        $this->addColumn('copyright', 'Copyright', 'VARCHAR', true, 255, null);
        $this->addColumn('isrc', 'Isrc', 'VARCHAR', true, 255, null);
        $this->addColumn('bpm', 'Bpm', 'DOUBLE', true, 11, null);
        $this->addColumn('comments', 'Comments', 'LONGVARCHAR', false, null, null);
        $this->addColumn('sweepers', 'Sweepers', 'VARCHAR', false, 250, null);
        $this->addColumn('album_art', 'AlbumArt', 'VARCHAR', true, 255, 'no_image.jpg');
        $this->addColumn('buy_link', 'BuyLink', 'VARCHAR', true, 255, 'http://');
        $this->addColumn('tdate_played', 'TdatePlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('tartist_played', 'TartistPlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('date_added', 'DateAdded', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('version', 'Version', 'INTEGER', false, null, 0);
        $this->addColumn('version_created_at', 'VersionCreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('version_created_by', 'VersionCreatedBy', 'VARCHAR', false, 100, null);
        $this->addColumn('version_comment', 'VersionComment', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SongsVersion', '\\Model\\Song\\SongsVersion', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ID',
    1 => ':ID',
  ),
), 'CASCADE', null, 'SongsVersions', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'versionable' => array('version_column' => 'version', 'version_table' => '', 'log_created_at' => 'true', 'log_created_by' => 'true', 'log_comment' => 'true', 'version_created_at_column' => 'version_created_at', 'version_created_by_column' => 'version_created_by', 'version_comment_column' => 'version_comment', 'indices' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to songs     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SongsVersionTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SongsTableMap::CLASS_DEFAULT : SongsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Songs object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SongsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SongsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SongsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SongsTableMap::OM_CLASS;
            /** @var Songs $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SongsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SongsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SongsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Songs $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SongsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SongsTableMap::COL_ID);
            $criteria->addSelectColumn(SongsTableMap::COL_PATH);
            $criteria->addSelectColumn(SongsTableMap::COL_ENABLED);
            $criteria->addSelectColumn(SongsTableMap::COL_DATE_PLAYED);
            $criteria->addSelectColumn(SongsTableMap::COL_ARTIST_PLAYED);
            $criteria->addSelectColumn(SongsTableMap::COL_COUNT_PLAYED);
            $criteria->addSelectColumn(SongsTableMap::COL_PLAY_LIMIT);
            $criteria->addSelectColumn(SongsTableMap::COL_LIMIT_ACTION);
            $criteria->addSelectColumn(SongsTableMap::COL_START_DATE);
            $criteria->addSelectColumn(SongsTableMap::COL_END_DATE);
            $criteria->addSelectColumn(SongsTableMap::COL_SONG_TYPE);
            $criteria->addSelectColumn(SongsTableMap::COL_ID_SUBCAT);
            $criteria->addSelectColumn(SongsTableMap::COL_ID_GENRE);
            $criteria->addSelectColumn(SongsTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(SongsTableMap::COL_DURATION);
            $criteria->addSelectColumn(SongsTableMap::COL_CUE_TIMES);
            $criteria->addSelectColumn(SongsTableMap::COL_PRECISE_CUE);
            $criteria->addSelectColumn(SongsTableMap::COL_FADE_TYPE);
            $criteria->addSelectColumn(SongsTableMap::COL_END_TYPE);
            $criteria->addSelectColumn(SongsTableMap::COL_OVERLAY);
            $criteria->addSelectColumn(SongsTableMap::COL_ARTIST);
            $criteria->addSelectColumn(SongsTableMap::COL_ORIGINAL_ARTIST);
            $criteria->addSelectColumn(SongsTableMap::COL_TITLE);
            $criteria->addSelectColumn(SongsTableMap::COL_ALBUM);
            $criteria->addSelectColumn(SongsTableMap::COL_COMPOSER);
            $criteria->addSelectColumn(SongsTableMap::COL_YEAR);
            $criteria->addSelectColumn(SongsTableMap::COL_TRACK_NO);
            $criteria->addSelectColumn(SongsTableMap::COL_DISC_NO);
            $criteria->addSelectColumn(SongsTableMap::COL_PUBLISHER);
            $criteria->addSelectColumn(SongsTableMap::COL_COPYRIGHT);
            $criteria->addSelectColumn(SongsTableMap::COL_ISRC);
            $criteria->addSelectColumn(SongsTableMap::COL_BPM);
            $criteria->addSelectColumn(SongsTableMap::COL_COMMENTS);
            $criteria->addSelectColumn(SongsTableMap::COL_SWEEPERS);
            $criteria->addSelectColumn(SongsTableMap::COL_ALBUM_ART);
            $criteria->addSelectColumn(SongsTableMap::COL_BUY_LINK);
            $criteria->addSelectColumn(SongsTableMap::COL_TDATE_PLAYED);
            $criteria->addSelectColumn(SongsTableMap::COL_TARTIST_PLAYED);
            $criteria->addSelectColumn(SongsTableMap::COL_DATE_ADDED);
            $criteria->addSelectColumn(SongsTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(SongsTableMap::COL_VERSION);
            $criteria->addSelectColumn(SongsTableMap::COL_VERSION_CREATED_AT);
            $criteria->addSelectColumn(SongsTableMap::COL_VERSION_CREATED_BY);
            $criteria->addSelectColumn(SongsTableMap::COL_VERSION_COMMENT);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.path');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.date_played');
            $criteria->addSelectColumn($alias . '.artist_played');
            $criteria->addSelectColumn($alias . '.count_played');
            $criteria->addSelectColumn($alias . '.play_limit');
            $criteria->addSelectColumn($alias . '.limit_action');
            $criteria->addSelectColumn($alias . '.start_date');
            $criteria->addSelectColumn($alias . '.end_date');
            $criteria->addSelectColumn($alias . '.song_type');
            $criteria->addSelectColumn($alias . '.id_subcat');
            $criteria->addSelectColumn($alias . '.id_genre');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.duration');
            $criteria->addSelectColumn($alias . '.cue_times');
            $criteria->addSelectColumn($alias . '.precise_cue');
            $criteria->addSelectColumn($alias . '.fade_type');
            $criteria->addSelectColumn($alias . '.end_type');
            $criteria->addSelectColumn($alias . '.overlay');
            $criteria->addSelectColumn($alias . '.artist');
            $criteria->addSelectColumn($alias . '.original_artist');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.album');
            $criteria->addSelectColumn($alias . '.composer');
            $criteria->addSelectColumn($alias . '.year');
            $criteria->addSelectColumn($alias . '.track_no');
            $criteria->addSelectColumn($alias . '.disc_no');
            $criteria->addSelectColumn($alias . '.publisher');
            $criteria->addSelectColumn($alias . '.copyright');
            $criteria->addSelectColumn($alias . '.isrc');
            $criteria->addSelectColumn($alias . '.bpm');
            $criteria->addSelectColumn($alias . '.comments');
            $criteria->addSelectColumn($alias . '.sweepers');
            $criteria->addSelectColumn($alias . '.album_art');
            $criteria->addSelectColumn($alias . '.buy_link');
            $criteria->addSelectColumn($alias . '.tdate_played');
            $criteria->addSelectColumn($alias . '.tartist_played');
            $criteria->addSelectColumn($alias . '.date_added');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.version');
            $criteria->addSelectColumn($alias . '.version_created_at');
            $criteria->addSelectColumn($alias . '.version_created_by');
            $criteria->addSelectColumn($alias . '.version_comment');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SongsTableMap::DATABASE_NAME)->getTable(SongsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SongsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SongsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SongsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Songs or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Songs object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Song\Songs) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SongsTableMap::DATABASE_NAME);
            $criteria->add(SongsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SongsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SongsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SongsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the songs table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SongsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Songs or Criteria object.
     *
     * @param mixed               $criteria Criteria or Songs object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Songs object
        }

        if ($criteria->containsKey(SongsTableMap::COL_ID) && $criteria->keyContainsValue(SongsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SongsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SongsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SongsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SongsTableMap::buildTableMap();
