<?php

namespace Model\Song\Map;

use Model\Song\SongsVersion;
use Model\Song\SongsVersionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'songs_version' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SongsVersionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Song.Map.SongsVersionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'gradio';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'songs_version';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Song\\SongsVersion';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Song.SongsVersion';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 44;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 44;

    /**
     * the column name for the ID field
     */
    const COL_ID = 'songs_version.ID';

    /**
     * the column name for the path field
     */
    const COL_PATH = 'songs_version.path';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'songs_version.enabled';

    /**
     * the column name for the date_played field
     */
    const COL_DATE_PLAYED = 'songs_version.date_played';

    /**
     * the column name for the artist_played field
     */
    const COL_ARTIST_PLAYED = 'songs_version.artist_played';

    /**
     * the column name for the count_played field
     */
    const COL_COUNT_PLAYED = 'songs_version.count_played';

    /**
     * the column name for the play_limit field
     */
    const COL_PLAY_LIMIT = 'songs_version.play_limit';

    /**
     * the column name for the limit_action field
     */
    const COL_LIMIT_ACTION = 'songs_version.limit_action';

    /**
     * the column name for the start_date field
     */
    const COL_START_DATE = 'songs_version.start_date';

    /**
     * the column name for the end_date field
     */
    const COL_END_DATE = 'songs_version.end_date';

    /**
     * the column name for the song_type field
     */
    const COL_SONG_TYPE = 'songs_version.song_type';

    /**
     * the column name for the id_subcat field
     */
    const COL_ID_SUBCAT = 'songs_version.id_subcat';

    /**
     * the column name for the id_genre field
     */
    const COL_ID_GENRE = 'songs_version.id_genre';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'songs_version.weight';

    /**
     * the column name for the duration field
     */
    const COL_DURATION = 'songs_version.duration';

    /**
     * the column name for the cue_times field
     */
    const COL_CUE_TIMES = 'songs_version.cue_times';

    /**
     * the column name for the precise_cue field
     */
    const COL_PRECISE_CUE = 'songs_version.precise_cue';

    /**
     * the column name for the fade_type field
     */
    const COL_FADE_TYPE = 'songs_version.fade_type';

    /**
     * the column name for the end_type field
     */
    const COL_END_TYPE = 'songs_version.end_type';

    /**
     * the column name for the overlay field
     */
    const COL_OVERLAY = 'songs_version.overlay';

    /**
     * the column name for the artist field
     */
    const COL_ARTIST = 'songs_version.artist';

    /**
     * the column name for the original_artist field
     */
    const COL_ORIGINAL_ARTIST = 'songs_version.original_artist';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'songs_version.title';

    /**
     * the column name for the album field
     */
    const COL_ALBUM = 'songs_version.album';

    /**
     * the column name for the composer field
     */
    const COL_COMPOSER = 'songs_version.composer';

    /**
     * the column name for the year field
     */
    const COL_YEAR = 'songs_version.year';

    /**
     * the column name for the track_no field
     */
    const COL_TRACK_NO = 'songs_version.track_no';

    /**
     * the column name for the disc_no field
     */
    const COL_DISC_NO = 'songs_version.disc_no';

    /**
     * the column name for the publisher field
     */
    const COL_PUBLISHER = 'songs_version.publisher';

    /**
     * the column name for the copyright field
     */
    const COL_COPYRIGHT = 'songs_version.copyright';

    /**
     * the column name for the isrc field
     */
    const COL_ISRC = 'songs_version.isrc';

    /**
     * the column name for the bpm field
     */
    const COL_BPM = 'songs_version.bpm';

    /**
     * the column name for the comments field
     */
    const COL_COMMENTS = 'songs_version.comments';

    /**
     * the column name for the sweepers field
     */
    const COL_SWEEPERS = 'songs_version.sweepers';

    /**
     * the column name for the album_art field
     */
    const COL_ALBUM_ART = 'songs_version.album_art';

    /**
     * the column name for the buy_link field
     */
    const COL_BUY_LINK = 'songs_version.buy_link';

    /**
     * the column name for the tdate_played field
     */
    const COL_TDATE_PLAYED = 'songs_version.tdate_played';

    /**
     * the column name for the tartist_played field
     */
    const COL_TARTIST_PLAYED = 'songs_version.tartist_played';

    /**
     * the column name for the date_added field
     */
    const COL_DATE_ADDED = 'songs_version.date_added';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'songs_version.updated_at';

    /**
     * the column name for the version field
     */
    const COL_VERSION = 'songs_version.version';

    /**
     * the column name for the version_created_at field
     */
    const COL_VERSION_CREATED_AT = 'songs_version.version_created_at';

    /**
     * the column name for the version_created_by field
     */
    const COL_VERSION_CREATED_BY = 'songs_version.version_created_by';

    /**
     * the column name for the version_comment field
     */
    const COL_VERSION_COMMENT = 'songs_version.version_comment';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Path', 'Enabled', 'DatePlayed', 'ArtistPlayed', 'CountPlayed', 'PlayLimit', 'LimitAction', 'StartDate', 'EndDate', 'SongType', 'IdSubcat', 'IdGenre', 'Weight', 'Duration', 'CueTimes', 'PreciseCue', 'FadeType', 'EndType', 'Overlay', 'Artist', 'OriginalArtist', 'Title', 'Album', 'Composer', 'Year', 'TrackNo', 'DiscNo', 'Publisher', 'Copyright', 'Isrc', 'Bpm', 'Comments', 'Sweepers', 'AlbumArt', 'BuyLink', 'TdatePlayed', 'TartistPlayed', 'DateAdded', 'UpdatedAt', 'Version', 'VersionCreatedAt', 'VersionCreatedBy', 'VersionComment', ),
        self::TYPE_CAMELNAME     => array('id', 'path', 'enabled', 'datePlayed', 'artistPlayed', 'countPlayed', 'playLimit', 'limitAction', 'startDate', 'endDate', 'songType', 'idSubcat', 'idGenre', 'weight', 'duration', 'cueTimes', 'preciseCue', 'fadeType', 'endType', 'overlay', 'artist', 'originalArtist', 'title', 'album', 'composer', 'year', 'trackNo', 'discNo', 'publisher', 'copyright', 'isrc', 'bpm', 'comments', 'sweepers', 'albumArt', 'buyLink', 'tdatePlayed', 'tartistPlayed', 'dateAdded', 'updatedAt', 'version', 'versionCreatedAt', 'versionCreatedBy', 'versionComment', ),
        self::TYPE_COLNAME       => array(SongsVersionTableMap::COL_ID, SongsVersionTableMap::COL_PATH, SongsVersionTableMap::COL_ENABLED, SongsVersionTableMap::COL_DATE_PLAYED, SongsVersionTableMap::COL_ARTIST_PLAYED, SongsVersionTableMap::COL_COUNT_PLAYED, SongsVersionTableMap::COL_PLAY_LIMIT, SongsVersionTableMap::COL_LIMIT_ACTION, SongsVersionTableMap::COL_START_DATE, SongsVersionTableMap::COL_END_DATE, SongsVersionTableMap::COL_SONG_TYPE, SongsVersionTableMap::COL_ID_SUBCAT, SongsVersionTableMap::COL_ID_GENRE, SongsVersionTableMap::COL_WEIGHT, SongsVersionTableMap::COL_DURATION, SongsVersionTableMap::COL_CUE_TIMES, SongsVersionTableMap::COL_PRECISE_CUE, SongsVersionTableMap::COL_FADE_TYPE, SongsVersionTableMap::COL_END_TYPE, SongsVersionTableMap::COL_OVERLAY, SongsVersionTableMap::COL_ARTIST, SongsVersionTableMap::COL_ORIGINAL_ARTIST, SongsVersionTableMap::COL_TITLE, SongsVersionTableMap::COL_ALBUM, SongsVersionTableMap::COL_COMPOSER, SongsVersionTableMap::COL_YEAR, SongsVersionTableMap::COL_TRACK_NO, SongsVersionTableMap::COL_DISC_NO, SongsVersionTableMap::COL_PUBLISHER, SongsVersionTableMap::COL_COPYRIGHT, SongsVersionTableMap::COL_ISRC, SongsVersionTableMap::COL_BPM, SongsVersionTableMap::COL_COMMENTS, SongsVersionTableMap::COL_SWEEPERS, SongsVersionTableMap::COL_ALBUM_ART, SongsVersionTableMap::COL_BUY_LINK, SongsVersionTableMap::COL_TDATE_PLAYED, SongsVersionTableMap::COL_TARTIST_PLAYED, SongsVersionTableMap::COL_DATE_ADDED, SongsVersionTableMap::COL_UPDATED_AT, SongsVersionTableMap::COL_VERSION, SongsVersionTableMap::COL_VERSION_CREATED_AT, SongsVersionTableMap::COL_VERSION_CREATED_BY, SongsVersionTableMap::COL_VERSION_COMMENT, ),
        self::TYPE_FIELDNAME     => array('ID', 'path', 'enabled', 'date_played', 'artist_played', 'count_played', 'play_limit', 'limit_action', 'start_date', 'end_date', 'song_type', 'id_subcat', 'id_genre', 'weight', 'duration', 'cue_times', 'precise_cue', 'fade_type', 'end_type', 'overlay', 'artist', 'original_artist', 'title', 'album', 'composer', 'year', 'track_no', 'disc_no', 'publisher', 'copyright', 'isrc', 'bpm', 'comments', 'sweepers', 'album_art', 'buy_link', 'tdate_played', 'tartist_played', 'date_added', 'updated_at', 'version', 'version_created_at', 'version_created_by', 'version_comment', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Path' => 1, 'Enabled' => 2, 'DatePlayed' => 3, 'ArtistPlayed' => 4, 'CountPlayed' => 5, 'PlayLimit' => 6, 'LimitAction' => 7, 'StartDate' => 8, 'EndDate' => 9, 'SongType' => 10, 'IdSubcat' => 11, 'IdGenre' => 12, 'Weight' => 13, 'Duration' => 14, 'CueTimes' => 15, 'PreciseCue' => 16, 'FadeType' => 17, 'EndType' => 18, 'Overlay' => 19, 'Artist' => 20, 'OriginalArtist' => 21, 'Title' => 22, 'Album' => 23, 'Composer' => 24, 'Year' => 25, 'TrackNo' => 26, 'DiscNo' => 27, 'Publisher' => 28, 'Copyright' => 29, 'Isrc' => 30, 'Bpm' => 31, 'Comments' => 32, 'Sweepers' => 33, 'AlbumArt' => 34, 'BuyLink' => 35, 'TdatePlayed' => 36, 'TartistPlayed' => 37, 'DateAdded' => 38, 'UpdatedAt' => 39, 'Version' => 40, 'VersionCreatedAt' => 41, 'VersionCreatedBy' => 42, 'VersionComment' => 43, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'path' => 1, 'enabled' => 2, 'datePlayed' => 3, 'artistPlayed' => 4, 'countPlayed' => 5, 'playLimit' => 6, 'limitAction' => 7, 'startDate' => 8, 'endDate' => 9, 'songType' => 10, 'idSubcat' => 11, 'idGenre' => 12, 'weight' => 13, 'duration' => 14, 'cueTimes' => 15, 'preciseCue' => 16, 'fadeType' => 17, 'endType' => 18, 'overlay' => 19, 'artist' => 20, 'originalArtist' => 21, 'title' => 22, 'album' => 23, 'composer' => 24, 'year' => 25, 'trackNo' => 26, 'discNo' => 27, 'publisher' => 28, 'copyright' => 29, 'isrc' => 30, 'bpm' => 31, 'comments' => 32, 'sweepers' => 33, 'albumArt' => 34, 'buyLink' => 35, 'tdatePlayed' => 36, 'tartistPlayed' => 37, 'dateAdded' => 38, 'updatedAt' => 39, 'version' => 40, 'versionCreatedAt' => 41, 'versionCreatedBy' => 42, 'versionComment' => 43, ),
        self::TYPE_COLNAME       => array(SongsVersionTableMap::COL_ID => 0, SongsVersionTableMap::COL_PATH => 1, SongsVersionTableMap::COL_ENABLED => 2, SongsVersionTableMap::COL_DATE_PLAYED => 3, SongsVersionTableMap::COL_ARTIST_PLAYED => 4, SongsVersionTableMap::COL_COUNT_PLAYED => 5, SongsVersionTableMap::COL_PLAY_LIMIT => 6, SongsVersionTableMap::COL_LIMIT_ACTION => 7, SongsVersionTableMap::COL_START_DATE => 8, SongsVersionTableMap::COL_END_DATE => 9, SongsVersionTableMap::COL_SONG_TYPE => 10, SongsVersionTableMap::COL_ID_SUBCAT => 11, SongsVersionTableMap::COL_ID_GENRE => 12, SongsVersionTableMap::COL_WEIGHT => 13, SongsVersionTableMap::COL_DURATION => 14, SongsVersionTableMap::COL_CUE_TIMES => 15, SongsVersionTableMap::COL_PRECISE_CUE => 16, SongsVersionTableMap::COL_FADE_TYPE => 17, SongsVersionTableMap::COL_END_TYPE => 18, SongsVersionTableMap::COL_OVERLAY => 19, SongsVersionTableMap::COL_ARTIST => 20, SongsVersionTableMap::COL_ORIGINAL_ARTIST => 21, SongsVersionTableMap::COL_TITLE => 22, SongsVersionTableMap::COL_ALBUM => 23, SongsVersionTableMap::COL_COMPOSER => 24, SongsVersionTableMap::COL_YEAR => 25, SongsVersionTableMap::COL_TRACK_NO => 26, SongsVersionTableMap::COL_DISC_NO => 27, SongsVersionTableMap::COL_PUBLISHER => 28, SongsVersionTableMap::COL_COPYRIGHT => 29, SongsVersionTableMap::COL_ISRC => 30, SongsVersionTableMap::COL_BPM => 31, SongsVersionTableMap::COL_COMMENTS => 32, SongsVersionTableMap::COL_SWEEPERS => 33, SongsVersionTableMap::COL_ALBUM_ART => 34, SongsVersionTableMap::COL_BUY_LINK => 35, SongsVersionTableMap::COL_TDATE_PLAYED => 36, SongsVersionTableMap::COL_TARTIST_PLAYED => 37, SongsVersionTableMap::COL_DATE_ADDED => 38, SongsVersionTableMap::COL_UPDATED_AT => 39, SongsVersionTableMap::COL_VERSION => 40, SongsVersionTableMap::COL_VERSION_CREATED_AT => 41, SongsVersionTableMap::COL_VERSION_CREATED_BY => 42, SongsVersionTableMap::COL_VERSION_COMMENT => 43, ),
        self::TYPE_FIELDNAME     => array('ID' => 0, 'path' => 1, 'enabled' => 2, 'date_played' => 3, 'artist_played' => 4, 'count_played' => 5, 'play_limit' => 6, 'limit_action' => 7, 'start_date' => 8, 'end_date' => 9, 'song_type' => 10, 'id_subcat' => 11, 'id_genre' => 12, 'weight' => 13, 'duration' => 14, 'cue_times' => 15, 'precise_cue' => 16, 'fade_type' => 17, 'end_type' => 18, 'overlay' => 19, 'artist' => 20, 'original_artist' => 21, 'title' => 22, 'album' => 23, 'composer' => 24, 'year' => 25, 'track_no' => 26, 'disc_no' => 27, 'publisher' => 28, 'copyright' => 29, 'isrc' => 30, 'bpm' => 31, 'comments' => 32, 'sweepers' => 33, 'album_art' => 34, 'buy_link' => 35, 'tdate_played' => 36, 'tartist_played' => 37, 'date_added' => 38, 'updated_at' => 39, 'version' => 40, 'version_created_at' => 41, 'version_created_by' => 42, 'version_comment' => 43, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('songs_version');
        $this->setPhpName('SongsVersion');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Song\\SongsVersion');
        $this->setPackage('Model.Song');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('ID', 'Id', 'INTEGER' , 'songs', 'ID', true, null, null);
        $this->addColumn('path', 'Path', 'VARCHAR', true, 255, null);
        $this->addColumn('enabled', 'Enabled', 'INTEGER', true, 1, 0);
        $this->addColumn('date_played', 'DatePlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('artist_played', 'ArtistPlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('count_played', 'CountPlayed', 'SMALLINT', true, 9, 0);
        $this->addColumn('play_limit', 'PlayLimit', 'INTEGER', true, null, 0);
        $this->addColumn('limit_action', 'LimitAction', 'INTEGER', true, 1, 0);
        $this->addColumn('start_date', 'StartDate', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('end_date', 'EndDate', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('song_type', 'SongType', 'TINYINT', true, 2, null);
        $this->addColumn('id_subcat', 'IdSubcat', 'INTEGER', true, null, null);
        $this->addColumn('id_genre', 'IdGenre', 'INTEGER', true, null, null);
        $this->addColumn('weight', 'Weight', 'DOUBLE', true, 5, 50);
        $this->addColumn('duration', 'Duration', 'DOUBLE', true, 11, null);
        $this->addColumn('cue_times', 'CueTimes', 'VARCHAR', true, 255, '&');
        $this->addColumn('precise_cue', 'PreciseCue', 'BOOLEAN', true, 1, false);
        $this->addColumn('fade_type', 'FadeType', 'BOOLEAN', true, 1, false);
        $this->addColumn('end_type', 'EndType', 'BOOLEAN', true, 1, false);
        $this->addColumn('overlay', 'Overlay', 'BOOLEAN', true, 1, false);
        $this->addColumn('artist', 'Artist', 'VARCHAR', true, 255, null);
        $this->addColumn('original_artist', 'OriginalArtist', 'VARCHAR', true, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('album', 'Album', 'VARCHAR', true, 255, null);
        $this->addColumn('composer', 'Composer', 'VARCHAR', true, 255, null);
        $this->addColumn('year', 'Year', 'VARCHAR', true, 4, '1900');
        $this->addColumn('track_no', 'TrackNo', 'SMALLINT', true, null, 0);
        $this->addColumn('disc_no', 'DiscNo', 'SMALLINT', true, null, 0);
        $this->addColumn('publisher', 'Publisher', 'VARCHAR', true, 255, null);
        $this->addColumn('copyright', 'Copyright', 'VARCHAR', true, 255, null);
        $this->addColumn('isrc', 'Isrc', 'VARCHAR', true, 255, null);
        $this->addColumn('bpm', 'Bpm', 'DOUBLE', true, 11, null);
        $this->addColumn('comments', 'Comments', 'LONGVARCHAR', false, null, null);
        $this->addColumn('sweepers', 'Sweepers', 'VARCHAR', false, 250, null);
        $this->addColumn('album_art', 'AlbumArt', 'VARCHAR', true, 255, 'no_image.jpg');
        $this->addColumn('buy_link', 'BuyLink', 'VARCHAR', true, 255, 'http://');
        $this->addColumn('tdate_played', 'TdatePlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('tartist_played', 'TartistPlayed', 'TIMESTAMP', false, null, '2002-01-01 00:00:01');
        $this->addColumn('date_added', 'DateAdded', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addPrimaryKey('version', 'Version', 'INTEGER', true, null, 0);
        $this->addColumn('version_created_at', 'VersionCreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('version_created_by', 'VersionCreatedBy', 'VARCHAR', false, 100, null);
        $this->addColumn('version_comment', 'VersionComment', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Songs', '\\Model\\Song\\Songs', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ID',
    1 => ':ID',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Model\Song\SongsVersion $obj A \Model\Song\SongsVersion object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getId() || is_scalar($obj->getId()) || is_callable([$obj->getId(), '__toString']) ? (string) $obj->getId() : $obj->getId()), (null === $obj->getVersion() || is_scalar($obj->getVersion()) || is_callable([$obj->getVersion(), '__toString']) ? (string) $obj->getVersion() : $obj->getVersion())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Model\Song\SongsVersion object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Model\Song\SongsVersion) {
                $key = serialize([(null === $value->getId() || is_scalar($value->getId()) || is_callable([$value->getId(), '__toString']) ? (string) $value->getId() : $value->getId()), (null === $value->getVersion() || is_scalar($value->getVersion()) || is_callable([$value->getVersion(), '__toString']) ? (string) $value->getVersion() : $value->getVersion())]);

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Model\Song\SongsVersion object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 40 + $offset : static::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 40 + $offset : static::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 40 + $offset : static::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 40 + $offset : static::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 40 + $offset : static::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 40 + $offset : static::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 40 + $offset
                : self::translateFieldName('Version', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SongsVersionTableMap::CLASS_DEFAULT : SongsVersionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SongsVersion object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SongsVersionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SongsVersionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SongsVersionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SongsVersionTableMap::OM_CLASS;
            /** @var SongsVersion $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SongsVersionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SongsVersionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SongsVersionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SongsVersion $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SongsVersionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ID);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_PATH);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ENABLED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_DATE_PLAYED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ARTIST_PLAYED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_COUNT_PLAYED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_PLAY_LIMIT);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_LIMIT_ACTION);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_START_DATE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_END_DATE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_SONG_TYPE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ID_SUBCAT);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ID_GENRE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_DURATION);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_CUE_TIMES);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_PRECISE_CUE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_FADE_TYPE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_END_TYPE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_OVERLAY);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ARTIST);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ORIGINAL_ARTIST);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_TITLE);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ALBUM);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_COMPOSER);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_YEAR);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_TRACK_NO);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_DISC_NO);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_PUBLISHER);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_COPYRIGHT);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ISRC);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_BPM);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_COMMENTS);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_SWEEPERS);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_ALBUM_ART);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_BUY_LINK);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_TDATE_PLAYED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_TARTIST_PLAYED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_DATE_ADDED);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_VERSION);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_VERSION_CREATED_AT);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_VERSION_CREATED_BY);
            $criteria->addSelectColumn(SongsVersionTableMap::COL_VERSION_COMMENT);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.path');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.date_played');
            $criteria->addSelectColumn($alias . '.artist_played');
            $criteria->addSelectColumn($alias . '.count_played');
            $criteria->addSelectColumn($alias . '.play_limit');
            $criteria->addSelectColumn($alias . '.limit_action');
            $criteria->addSelectColumn($alias . '.start_date');
            $criteria->addSelectColumn($alias . '.end_date');
            $criteria->addSelectColumn($alias . '.song_type');
            $criteria->addSelectColumn($alias . '.id_subcat');
            $criteria->addSelectColumn($alias . '.id_genre');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.duration');
            $criteria->addSelectColumn($alias . '.cue_times');
            $criteria->addSelectColumn($alias . '.precise_cue');
            $criteria->addSelectColumn($alias . '.fade_type');
            $criteria->addSelectColumn($alias . '.end_type');
            $criteria->addSelectColumn($alias . '.overlay');
            $criteria->addSelectColumn($alias . '.artist');
            $criteria->addSelectColumn($alias . '.original_artist');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.album');
            $criteria->addSelectColumn($alias . '.composer');
            $criteria->addSelectColumn($alias . '.year');
            $criteria->addSelectColumn($alias . '.track_no');
            $criteria->addSelectColumn($alias . '.disc_no');
            $criteria->addSelectColumn($alias . '.publisher');
            $criteria->addSelectColumn($alias . '.copyright');
            $criteria->addSelectColumn($alias . '.isrc');
            $criteria->addSelectColumn($alias . '.bpm');
            $criteria->addSelectColumn($alias . '.comments');
            $criteria->addSelectColumn($alias . '.sweepers');
            $criteria->addSelectColumn($alias . '.album_art');
            $criteria->addSelectColumn($alias . '.buy_link');
            $criteria->addSelectColumn($alias . '.tdate_played');
            $criteria->addSelectColumn($alias . '.tartist_played');
            $criteria->addSelectColumn($alias . '.date_added');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.version');
            $criteria->addSelectColumn($alias . '.version_created_at');
            $criteria->addSelectColumn($alias . '.version_created_by');
            $criteria->addSelectColumn($alias . '.version_comment');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SongsVersionTableMap::DATABASE_NAME)->getTable(SongsVersionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SongsVersionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SongsVersionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SongsVersionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SongsVersion or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SongsVersion object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsVersionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Song\SongsVersion) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SongsVersionTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(SongsVersionTableMap::COL_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(SongsVersionTableMap::COL_VERSION, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = SongsVersionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SongsVersionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SongsVersionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the songs_version table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SongsVersionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SongsVersion or Criteria object.
     *
     * @param mixed               $criteria Criteria or SongsVersion object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SongsVersionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SongsVersion object
        }


        // Set the correct dbName
        $query = SongsVersionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SongsVersionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SongsVersionTableMap::buildTableMap();
