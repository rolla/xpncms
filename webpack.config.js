'use strict';

const template = process.env.TEMPLATE;
const devPublicUrl = process.env.DEV_PUBLIC_URL;
const socketUrl = process.env.SOCKET_URL;
const redisPrefix = process.env.REDIS_PREFIX;

const path = require('path');
const Webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

const enableBundle = process.env.ENABLE_BUNDLE_ANALYZE === 'true';

const isProd = process.env.NODE_ENV === 'production';
const isDev = process.env.NODE_ENV === 'development';
const templatePath = `${__dirname}/com/xpncms/View/templates/${template}`;
const outputPath = `${__dirname}/public/themes/${template}/assets/`;

process.traceDeprecation = true;

console.log('Production mode: ' + isProd);
console.log('SOCKET_URL: ' + socketUrl);
console.log('REDIS_PREFIX: ' + redisPrefix);

const pkg = require('./package.json');

const config = {
    entry: {
        app: [
          '@babel/polyfill',
          `${templatePath}/src/main`
        ],
    },
    mode: process.env.NODE_ENV,
    output: {
        path: outputPath,
        filename: isDev ? '[name].[hash].js' : '[name].[contenthash].js', // we need this for runtime hash instead of runtime.js
        chunkFilename: '[name].[contenthash].js',
        publicPath: '/themes/' + template + '/assets/',
    },
    watchOptions: {
      ignored: /node_modules/
    },
    module: {
        // noParse: /jquery|lodash/,
        rules: [
          {
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /node_modules/
          },

          {
            test: /\.(jpe?g|png|gif|svg)$/,
            loader: 'image-webpack-loader',
            // This will apply the loader before the other ones
            enforce: 'pre',
          },

          {
            test: /\.vue$/,
            use: [
              {
                loader: 'vue-loader'
              },
            ]
          },

          {
            test: /\.sass$/,
            use: [
              'vue-style-loader',
              'css-loader',
              {
                loader: 'sass-loader',
                // Requires sass-loader@^9.0.0
                options: {
                  // This is the path to your variables
                  // additionalData: "@import '@/styles/variables.scss'"
                },
              },
            ],
          },
          // SCSS has different line endings than SASS
          // and needs a semicolon after the import.
          {
            test: /\.scss$/,
            use: [
              'vue-style-loader',
              'css-loader',
              {
                loader: 'sass-loader',
                // Requires sass-loader@^9.0.0
                options: {
                  // This is the path to your variables
                  // additionalData: "@import '@/styles/variables.scss'"
                },
              },
            ],
          },

          {
            test: /\.css$/,
            use: [
              {
                loader: MiniCssExtractPlugin.loader,
              },
              'css-loader',
            ],
          },

          {
            test: /\.html$/,
            loader: 'vue-template-loader',
          },

          {
            test: /\.(jpe?g|png|gif)$/,
            loader: 'url-loader',
            options: {
              // Inline files smaller than 10 kB (10240 bytes)
              limit: 10 * 1024,
            },
          },

          {
            test: /\.svg$/,
            loader: 'svg-url-loader',
            options: {
              // Inline files smaller than 10 kB (10240 bytes)
              limit: 10 * 1024,
              // Remove the quotes from the url
              // (they’re unnecessary in most cases)
              noquotes: true,
            },
          },

          {
            test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader',
            options: {
              // Inline files smaller than 10 kB (10240 bytes)
              limit: 10 * 1024,
            }
          },

          {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules|bower_components)/,
            include: `${templatePath}/src`,
            use: {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
                presets: ['@babel/preset-env'],
                plugins: [
                  '@babel/plugin-syntax-dynamic-import',
                  '@babel/plugin-proposal-object-rest-spread',
                ]
              }
            },
          },
        ]
    },

    resolve: {
      alias: {
        jquery: 'jquery/src/jquery',
        node_modules: path.join(__dirname, 'node_modules'),
        '@': `${templatePath}/src`,
      }
    },

    node: {
      fs: 'empty'
    },

    plugins: [
      new CleanWebpackPlugin(),

      new VueLoaderPlugin(),

      new MiniCssExtractPlugin({
        chunkFilename: '[id].[contenthash].css',
        ignoreOrder: false, // Enable to remove warnings about conflicting order
      }),

      new Webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
      }),

      new Webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
          'SOCKET_URL': JSON.stringify(socketUrl),
          'REDIS_PREFIX': JSON.stringify(redisPrefix),
        }
      }),

      new HtmlWebpackPlugin({
        filename: `${templatePath}/index.tpl`,
        template: `${templatePath}/src/index.ejs`,
        hash: false
      }),

      new Webpack.ContextReplacementPlugin(
        /moment[/\\]locale/,
        /(|en-gb|lv).js/
      ),

      new VuetifyLoaderPlugin({
        progressiveImages: true
      }),
    ],

    optimization: {
      moduleIds: 'hashed',
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all',
          },
        },
      },
      namedModules: true,
      minimizer: []
    }
};

if (isProd) {
  config.optimization.minimizer.push(
    new TerserJSPlugin({})
  );

  config.optimization.minimizer.push(
    new OptimizeCssAssetsPlugin({})
  );

  config.plugins.push(
    new Webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"',
    }),
  );

  config.plugins.push(
    new Webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  );

} else {
  config.devtool = 'source-map';

  config.devServer = {
    hot: true,
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 8080,
    host: '0.0.0.0',
    public: devPublicUrl,
    /*
    proxy: {
      '/api': {
        target: 'https://dev.gradio.lv/gradio/now',
        secure: false
      }
    }
    */
  };

  config.plugins.push(
    new Webpack.HotModuleReplacementPlugin(),
  )
}

if (enableBundle) {
  config.plugins.push(
    new BundleAnalyzerPlugin({analyzerHost: '0.0.0.0', analyzerPort: 42539})
  );
}

module.exports = config
