<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Core\Includes\Config;

$capsule = new Capsule;

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => Config::get('databases.home.host'),
    'database' => Config::get('databases.home.name'),
    'username' => Config::get('databases.home.user'),
    'password' => Config::get('databases.home.password'),
    'prefix' => Config::get('databases.home.prefix'),
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();
