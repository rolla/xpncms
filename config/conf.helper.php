<?php

$possibleHttps = [
    'HTTP_X_FORWARDED_PROTO',
    'HTTPS',
];

$isHttps = false;
foreach ($possibleHttps as $https) {
    if (isset($_SERVER[$https]) && $_SERVER[$https] == 'https') {
        $isHttps = true;
        break;
    } else if (isset($_SERVER[$https]) && $_SERVER[$https] != 'off') {
        $isHttps = true;
    }
}

define('REQUEST_SCHEME', $isHttps ? 'https' : 'http');
define('TEMPLATE', getenv('FRONTEND_THEME'));

function getHost()
{
    $possibleHostSources = array('HTTP_X_FORWARDED_HOST', 'HTTP_HOST', 'SERVER_NAME', 'SERVER_ADDR');
    $sourceTransformations = array(
        "HTTP_X_FORWARDED_HOST" => function ($value) {
            $elements = explode(',', $value);
            return trim(end($elements));
        }
    );
    $host = '';
    foreach ($possibleHostSources as $source) {
        if (!empty($host)) break;
        if (empty($_SERVER[$source])) continue;
        $host = $_SERVER[$source];
        if (array_key_exists($source, $sourceTransformations)) {
            $host = $sourceTransformations[$source]($host);
        }
    }

    // Remove port number from host
    $host = preg_replace('/:\d+$/', '', $host);

    return trim($host);
}

$APP_PUBLIC_URL = REQUEST_SCHEME . '://' . getHost(); // without trailing slash
$isHttpXForwardedFor = isset($_SERVER['HTTP_X_FORWARDED_FOR']);
$ignoredPorts = [80, 443];
if (!$isHttpXForwardedFor && !in_array($_SERVER['SERVER_PORT'], $ignoredPorts)) {
    $APP_PUBLIC_URL .= ':' . $_SERVER['SERVER_PORT']; // without trailing slash
}
define('SITE_URL', $APP_PUBLIC_URL);
define('BASE_URL', '/');

$apiUrl = BASE_URL;
if (BACKEND_API) {
    $apiUrl = BASE_URL . 'api/';
}
define('API_URL', $apiUrl);
