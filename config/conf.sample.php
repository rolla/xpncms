<?php

/* if not use docker uncomment these
putenv("APP_TITLE=XpnCMS");
putenv("APP_SHORT_TITLE=xpncms");
putenv("APP_SHORT_DESCRIPTION=awesome cms");
putenv("APP_DESCRIPTION=This what it feels like :)");
putenv("APP_IMAGE=https://gradio.lv/img/logos/g-radio-logo.png");
putenv("DEFAULT_LOCALE=en_US");
putenv("ENVIRONMENT=development");
putenv("FRONTEND_THEME=gradio-material");
putenv("FRONTEND_V2=true");
putenv("SLACK_WEB_HOOK=https://hooks.slack.com/services/slackwebhook");
putenv("UTILS_URL=https://utils.gradio.lv/");
putenv("STREAM_AAC=https://streams.urdt.lv/gradio.mp4a");
putenv("STREAM_MP3=https://streams.urdt.lv/gradio.mp3");

putenv("DB_GRADIO_HOST=mariadb");
putenv("DB_GRADIO_USERNAME=root");
putenv("DB_GRADIO_PASSWORD=yourpass");
putenv("DB_GRADIO_DATABASE=devgradio_gradio");

putenv("DB_RDJ_HOST=mariadb");
putenv("DB_RDJ_USERNAME=root");
putenv("DB_RDJ_PASSWORD=yourpass");
putenv("DB_RDJ_DATABASE=devgradio_radiodj");

putenv("DB_UTILS_HOST=mariadb");
putenv("DB_UTILS_USERNAME=root");
putenv("DB_UTILS_PASSWORD=yourpass");
putenv("DB_UTILS_DATABASE=devgradio_utils");

putenv("DB_PLAY_HOST=mariadb");
putenv("DB_PLAY_USERNAME=root");
putenv("DB_PLAY_PASSWORD=yourpass");
putenv("DB_PLAY_DATABASE=devgradio_play");
putenv("SESSION_DOMAIN=.docker.dev.gradio.lv");
*/

define('APP_TITLE', getenv('APP_TITLE'));
define('APP_SHORT_TITLE', getenv('APP_SHORT_TITLE'));
define('APP_SHORT_DESCRIPTION', getenv('APP_SHORT_DESCRIPTION'));
define('APP_DESCRIPTION', getenv('APP_DESCRIPTION'));
define('APP_IMAGE', getenv('APP_IMAGE'));
define('DEFAULT_LOCALE', getenv('DEFAULT_LOCALE'));
define('ENVIRONMENT', getenv('ENVIRONMENT')); // possible options development or production
define('BACKEND_API', true); // set to true if backend serve another server
define('FRONTEND_THEME', getenv('FRONTEND_THEME'));
define('FRONTEND_V2', getenv('FRONTEND_V2') === 'true'); // in general use only one smarty template meta.tpl
define('BACKEND_THEME', 'gradio-admin');

include_once 'conf.helper.php';

$conf = [
    'settings' => [
        'debug' => [
            'enabled' => true,
            'level' => E_ALL & ~E_NOTICE & ~E_WARNING,
            'whoopsips' => ['::1', '127.0.0.1', 'localhost', '172.29.0.1'],
            'smartydebug' => false,
            'jsdebug' => false,
            'debugbar' => false,
        ],
        'cahe' => [
            'smarty' => true,
            'sacy' => true,
            'phpthumb' => true,
        ],
        'base' => [
            'url' => BASE_URL,
            'siteurl' => SITE_URL,
            'fbsiteurl' => 'https://gradio.lv', // for facebook comments, before was http
            'fbsiteoldurl' => 'http://gradio.lv',
            'apiUrl' => $apiUrl . 'v2/',
        ],
        'site' => [
            'title' => APP_TITLE,
            'shortTitle' => APP_SHORT_TITLE,
            'desc' => 'XpnCMS',
            'timezone' => 'Europe/Riga',
            'template' => TEMPLATE,
            'frontendTheme' => FRONTEND_THEME,
            'backendTheme' => BACKEND_THEME,
            'startpage' => 'index',
            'environment' => ENVIRONMENT,
            'locale' => DEFAULT_LOCALE,
            'phpThumbUrl' => '/libs/phpThumb/',
            'phpThumbCacheHash' => '20160526151411',
            'meta' => [
                'contenttype' => 'text/html; charset=utf-8',
                'contentstyletype' => 'text/css',
                'contentlanguage' => 'en',
                'content' => 'XpnCMS',
                'keywords' => 'XpnCMS',
                'copyright' => 'Rolla, Universal Radio DJ Team © 2001 - 2019 urdt.lv',
                'author' => 'Beater aka Ray Rolla',
                'robots' => 'noindex, nofollow',
                'abstract' => 'XpnCMS',
                'distribution' => 'local',
                'webauthor' => 'Beater aka Ray Rolla',
                'ogurl' => SITE_URL,
                'ogtype' => 'website',
                'ogtitle' => APP_TITLE . ' - ' . APP_SHORT_DESCRIPTION,
                'ogdescription' => APP_DESCRIPTION,
                'ogsitename' => APP_TITLE . ' - ' . APP_SHORT_DESCRIPTION,
                'oglocale' => DEFAULT_LOCALE,
                'ogimage' => APP_IMAGE,
                'twittercard' => 'summary_large_image',
                'twittersite' => '@gradiolv',
                'twittercreator' => '@gradiolv',
                'twitterurl' => SITE_URL,
                'twittertitle' => APP_TITLE . ' - ' . APP_SHORT_DESCRIPTION,
                'twitterdescription' => APP_DESCRIPTION,
                'twitterimage' => APP_IMAGE,
                'fbadmins' => '',
                'fbappid' => '',
                'googlesitev' => '',
                'name' => APP_TITLE . ' - ' . APP_SHORT_DESCRIPTION,
                'description' => APP_DESCRIPTION,
                'image' => APP_IMAGE,
            ],
            'social' => [
                'facebook' => ['show' => true, 'profile_url' => 'xpncms', 'app_id' => ''],
                'youtube' => ['show' => true, 'profile_url' => 'xpncms', 'app_id' => ''],
                'twitter' => ['show' => true, 'profile_url' => 'xpncms', 'app_id' => ''],
                'mixcloud' => ['show' => true, 'profile_url' => 'xpncms', 'app_id' => ''],
                'soundcloud' => ['show' => true, 'profile_url' => 'xpncms', 'app_id' => ''],
            ],
        ],
        'paths' => [
            'public' => 'public',
            'assets' => 'assets',
            'cache' => 'cache',
            'css' => 'css',
            'files' => 'files',
            'images' => 'img',
            'js' => 'js',
            'libs' => 'libs',
            'themes' => 'themes',
        ],
        'userFolders' => ['images', 'music', 'video', 'private'],
        'databases' => [
            'home' => [
                'host' => getenv('DB_GRADIO_HOST'),
                'user' => getenv('DB_GRADIO_USERNAME'),
                'password' => getenv('DB_GRADIO_PASSWORD'),
                'name' => getenv('DB_GRADIO_DATABASE'),
                'prefix' => 'gradio_',
                'charset' => 'utf8',
            ],
            'remote' => [
                'host' => getenv('DB_RDJ_HOST'),
                'user' => getenv('DB_RDJ_USERNAME'),
                'password' => getenv('DB_RDJ_PASSWORD'),
                'name' => getenv('DB_RDJ_DATABASE'),
                'prefix' => '',
                'charset' => 'utf8',
            ],
            'utils' => [
                'host' => getenv('DB_UTILS_HOST'),
                'user' => getenv('DB_UTILS_USERNAME'),
                'password' => getenv('DB_UTILS_PASSWORD'),
                'name' => getenv('DB_UTILS_DATABASE'),
                'prefix' => '',
                'charset' => 'utf8',
            ],
            'play' => [
                'host' => getenv('DB_PLAY_HOST'),
                'user' => getenv('DB_PLAY_USERNAME'),
                'password' => getenv('DB_PLAY_PASSWORD'),
                'name' => getenv('DB_PLAY_DATABASE'),
                'prefix' => '',
                'charset' => 'utf8',
            ],
        ],
        'version' => [
            'name' => 'alfa',
            'number' => '2.0.0',
        ],
        'files' => [
            'userpath' => 'public/files/', // deprecated
        ],
        'images' => [
            'defaultpath' => 'xpncms-logo.png',
            'defaddcontent' => 'add-content.png',
            'defCont' => 'content.png',
            'defCat' => 'content.png',
            'defNotFound' => 'not-found.png',
        ],
        'plugins' => [
            'gradio' => [
                'prefs' => [
                    'covers' => 'https://covers.gradio.lv/',
                    'coversMnt' => '/var/www/covers/',
                    'baseurl' => 'master.urdt.lv',
                    'uploaded' => '/tmp/',
                    'relativeUploaded' => 'uploaded/', //relative from coversMnt
                    'tuneurl' => 'tune',
                    'utilsurl' => getenv('UTILS_URL'),
                    'driveBy' => [
                        'text' => 'Drive by',
                        'linkText' => 'stream.urdt.lv',
                        'url' => 'http://stream.urdt.lv',
                    ],
                    'streams' => [
                        'opus' => [
                            'name' => _("【G】OPUS 160 Kb/s"),
                            'url' => 'https://streams.urdt.lv/gradio.opus',
                            'icon' => '',
                        ],
                        'aacp' => [
                            'name' => _("【G】AAC 128 Kb/s"),
                            'url' => getenv('STREAM_AAC'),
                            'icon' => '',
                        ],
                        'mp3' => [
                            'name' => _("【G】MP3 320 Kb/s"),
                            'url' => getenv('STREAM_MP3'),
                            'icon' => '',
                        ],
                    ],
                ],
            ],
        ],
        'prefs' => [
            'pingfreq' => 300,
            'socialnetworks' => [
                'facebook' => [
                    'fbadmins' => '',
                    'fbappid' => '',
                ],
                'google' => [
                    'siteverification' => '',
                ],
                'lastfm' => [
                    'user' => '',
                    'password' => '',
                    'apikey' => '',
                ],
            ],
            'services' => [
                'slack' => [
                    'webhook' => getenv('SLACK_WEB_HOOK'),
                    'channel' => '#gradio',
                    'devChannel' => '#dev',
                    'icon' => ':loudspeaker:',
                ],
            ],
            'googleanalytics' => ['id' => 'UA-xxxxxxxx-y'],
            'piwik' => ['url' => 'https://stats.urdt.lv/', 'id' => 14, 'token' => 'token here'],
        ],
    ],
];
