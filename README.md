# xpncms install on Linux:

### Install prerequisites

```sh
# to work add-apt-repository
sudo apt install -y software-properties-common python-software-properties

# add newest php
sudo add-apt-repository ppa:ondrej/php &&
sudo apt update

# let the install begin
sudo apt install -y apt-transport-https git unzip zip nginx redis-server curl mcrypt npm php7.2 php7.2-common php7.2-cli php7.2-fpm php7.2-mbstring php7.2-xml php7.2-gd php7.2-mysql php7.2-bcmath php7.2-zip php7.2-curl php7.2-sqlite &&
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer &&
sudo chown -R $USER ~/.composer/

# php tools for atom and build
composer global require hirak/prestissimo
composer global require friendsofphp/php-cs-fixer
composer global require phpunit/phpunit
composer global require phpunit/dbunit
composer global require phing/phing
composer global require phpdocumentor/phpdocumentor
composer global require sebastian/phpcpd
composer global require phploc/phploc
composer global require phpmd/phpmd
composer global require squizlabs/php_codesniffer

# add binaries to PATH (IMPORTANT find out correct composer bin path)
export PATH=~/.composer/vendor/bin:$PATH
echo 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> ~/.bashrc

# javascript stuff
sudo npm install -g n
sudo npm install -g bower

# yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn -y

# cd to APP_ROOT
# install atom packages from file https://work.urdt.lv/s/oYBTd9CRWR4HkPJ
apm install --packages-file bin/atom/package-list.txt

```

### Install project

```sh
git clone https://gitlab.com/raitis-rolis/xpncms.git
cd xpncms
./bin/install.sh full verbose
```
---

##### copy configuration sample files
```sh
cp -v config/conf.sample.php config/conf.php
cp -v config/hybridauth_config.sample.php config/hybridauth_config.php
```

##### Edit configuration files
config/conf.php file

config/hybridauth_config.php

---
##### this example **server** = http://xpncms.com
##### this example **web_location** = /xpncms/

##### go to public/.htaccess set correct:
RewriteBase **web_location**

---
##### this example **physical_location** = /var/sentora/hostdata/xpncms/public_html/xpncms/public
##### Need to apache config point to public folder:
DocumentRoot "**physical_location**"
```apache
<Directory "physical_location">
  Options +FollowSymLinks -Indexes
  AllowOverride All
  Require all granted
</Directory>
```
---
##### check phpinfo() to allow **shell_exec** and **suhosin.executor.include.whitelist = phar**
##### Need for locales and sacy css, js minifyer 
---
##### copy recursively `install/vendor` content to `vendor`
```sh
cp -Rv install/vendor/* vendor
```
##### copy recursively `install/assets` content to `public/assets`
```sh
cp -Rv install/assets/* public/assets
```
#### If you hit `bower update` after this copy, you need to `repeat recursive copy!`

#### Find new translations in php files
```bash
find ./com/xpncms/ -iname "*.php" | xargs xgettext --add-comments=TRANSLATORS: --keyword=gettext --keyword=_ --from-code utf-8 -o ./com/xpncms/Locale/code-en.pot;
```
#### Find new translations in smarty {t}{/t} block's
```php
php ./vendor/smarty-gettext/smarty-gettext/tsmarty2c.php -o ./com/xpncms/Locale/t-block-en.pot ./com/xpncms/;
```
#### Merge above translations
```bash
msgcat ./com/xpncms/Locale/code-en.pot ./com/xpncms/Locale/t-block-en.pot -o ./com/xpncms/Locale/template-en.pot;
```

##### Open translation *.po file with poedit (located in ./com/xpncms/Locale/`your lang` ...) choose Catalog -> Update from POT file... browse for ./com/xpncms/Locale/template-en.pot

# For best Debugging start PHP built in server in root folder, and watch terminal when go to http://localhost:8000
```php
php -S localhost:8000 -t public
```

# To css, js debug
```
https://gradio.dev/?_sacy_debug=3
```

##### Go to URL server/web_location/install follow instructions
# Enjoy :) xpncms

