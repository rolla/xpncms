module.exports = {
  env: {
    browser: true,
    node: true,
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
     parser: 'babel-eslint',
  },
  extends: [
    // add more generic ruleset here, such as:
    'eslint:recommended',
    'plugin:vue/essential'
  ],
  plugins: [
    'vuetify'
  ],
  rules: {
    // override/add rules settings here, such as:
    'no-undef': 'warn',
    'no-unused-vars': 'warn',
    'vue/no-unused-vars': 'warn',
    'vuetify/no-deprecated-classes': 'error',
    'vuetify/no-deprecated-components': 'error',
    'vuetify/no-legacy-grid': 'error',
    'vuetify/grid-unknown-attributes': 'error',
  }
}
