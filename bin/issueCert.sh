#!/bin/bash

set -e

domain="docker.dev.gradio.lv"

preMsg () {
  echo ""
  echo "####################################"
  echo "wildcard certificate generator using acme.sh"
  echo ""
  echo "${domain} and *.${domain} certificate issue and renew"
  echo ""
  echo "note: you will need to verify both txt records, adding these one by one, if one is succesfully verified it will skip it and goes to next one"
  echo ""
  echo "usage: `basename $0` issue|renew|list"
  echo "default: `basename $0` list"
  echo "test dns with dig TXT _acme-challenge.${domain} @ns1.urdt.lv @ns2.urdt.lv @ns2.afraid.org"
  echo "####################################"
  echo ""
}

preMsg

read -p "press enter to continue"

if [ ! -d ./.acme.sh ]; then
    mkdir ./.acme.sh
fi

mode=${1:-list}

docker run --rm  -it  \
  -v "$(pwd)/.acme.sh":/acme.sh  \
  neilpang/acme.sh  --${mode} -d ${domain} -d *.${domain} --dns --yes-I-know-dns-manual-mode-enough-go-ahead-please
