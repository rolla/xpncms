#!/bin/bash

start=`date +%s`
echo
echo "xpnCMS install script"
echo
echo "================================"
echo "Usage: `basename $0` fast|full silent|verbose"
echo "Default: `basename $0` fast silent"
echo "================================"
echo

full=${1-fast}
verbose=${2-silent}

cd "$(dirname "$0")"
cd ../

#pwd

if [ $full = "full" ]; then
    if [ $verbose = "verbose" ]; then
        rm -vrf node_modules
    else
        rm -rf node_modules
    fi

    if [ $verbose = "verbose" ]; then
        rm -vrf package-lock.json
    else
        rm -rf package-lock.json
    fi

    if [ $verbose = "verbose" ]; then
        rm -vrf yarn.lock
    else
        rm -rf yarn.lock
    fi

    if [ $verbose = "verbose" ]; then
        rm -vrf public/assets
    else
        rm -rf public/assets
    fi
fi

if [ $verbose = "verbose" ]; then
    cp -vR install/assets/* public/assets
else
    cp -R install/assets/* public/assets
fi

if [ $full = "full" ]; then
    if [ $verbose = "verbose" ]; then
        git clone -b 2.1 --single-branch https://github.com/Studio-42/elFinder.git public/assets/elfinder
    else
        git clone --quiet -b 2.1 --single-branch https://github.com/Studio-42/elFinder.git public/assets/elfinder
    fi
fi

if [ $verbose = "verbose" ]; then
    yarn install
else
    yarn install --ignore-engines --silent --loglevel=error
fi

if [ $verbose = "verbose" ]; then
    yarn build
else
    yarn build --silent
fi

if [ $full = "full" ]; then
    if [ $verbose = "verbose" ]; then
        rm -vrf vendor
        rm -vrf composer.lock
    else
        rm -rf vendor
        rm -rf composer.lock
    fi
fi

if [ $verbose = "verbose" ]; then
    composer install
else
    composer install --quiet
fi

echo
bash bin/clearcache.sh

end=`date +%s`

runtime=$((end-start))

echo
echo "================================"
echo "Build complete in ${runtime} seconds!"
echo "================================"
echo
