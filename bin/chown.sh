#!/bin/bash

cd "$(dirname "$0")" &&
cd ../ &&

# pwd

chown -vR rolla:www-data ./
chown -vR rolla:www-data ./vendor ./cache ./public/cache
