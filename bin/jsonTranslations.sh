#!/bin/bash

TEMPLATE="gradio-material";
cd "$(dirname "$0")" &&
cd ../ &&

# pwd

./node_modules/.bin/gettext-compile --output com/xpncms/View/templates/$TEMPLATE/src/translations.json \
com/xpncms/Locale/lv_LV/LC_MESSAGES/lv_LV.po com/xpncms/Locale/en_US/LC_MESSAGES/en_US.po;
