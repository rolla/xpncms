#!/bin/bash

cd "$(dirname "$0")"
cd ../

#pwd

rm -vRf cache/templates_c/*.php public/cache/sacy/*.{js,css}

echo "Smarty, sacy cache cleared!"
