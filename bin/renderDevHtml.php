<?php

use Core\Includes\Config;

define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', realpath('./' . DS) . DS);

if (php_sapi_name() == 'cli-server') {

}

$bootstrapFile = APP_ROOT . 'app' . DS . 'bootstrap.php';

require_once($bootstrapFile);


function render()
{
    global $smarty;

    $indexTpl = APP_ROOT . 'com/xpncms/View/templates/' . Config::get('site.frontendTheme') . '/index.tpl';
    $outputHtml = $smarty->fetch($indexTpl);
    $outputHtmlFile = fopen("index.html", "w") or die("Unable to open file!");
    fwrite($outputHtmlFile, $outputHtml);
    fclose($outputHtmlFile);

    $indexHtml = APP_ROOT . 'public/index.html';

    if (file_exists($indexHtml)) {
        unlink($indexHtml);
    }

    rename('index.html', $indexHtml);

    echo PHP_EOL . PHP_EOL;
    echo 'html file rendered in ' . $indexHtml . PHP_EOL . PHP_EOL;
}

render();
