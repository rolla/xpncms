#!/bin/bash

# exit script if error
# set -e

# config
APP_NAME=gradio
APP_URL=docker.dev.gradio.lv
APP_SCHEME=https
USER=rolla
GROUP=www-data
FRONTEND_TEMPLATE=gradio-material

CONTAINER_PREFIX=gradio_
IMAGE_PREFIX=gradio/image

MYSQL_USER=root
MYSQL_PASSWORD=yourpass
DOWNLOAD_PATH=./tmp/

UTILS_DB_NAME=devgradio_utils
RADIODJ_DB_NAME=devgradio_radiodj
GRADIO_DB_NAME=devgradio_gradio
PLAY_DB_NAME=devgradio_play

DOWNLOAD_ENDPOINT=https://work.urdt.lv/remote.php/webdav/backup/
DOWNLOAD_USER=gradio
# DOWNLOAD_PASSWORD=""

# docker services
FRONTEND_SERVICE=frontend
API_SERVICE=api
NGINX_SERVICE=nginx
DB_SERVICE=mariadb
PHPMYADMIN_SERVICE=my-admin
DOCKER_HOSTER_SERVICE=docker-hoster

CONTAINERS="${CONTAINER_PREFIX}${FRONTEND_SERVICE} ${CONTAINER_PREFIX}${API_SERVICE} ${CONTAINER_PREFIX}${NGINX_SERVICE} \
            ${CONTAINER_PREFIX}${DB_SERVICE} ${CONTAINER_PREFIX}${PHPMYADMIN_SERVICE} ${CONTAINER_PREFIX}${DOCKER_HOSTER_SERVICE}"

IMAGES="${IMAGE_PREFIX}:gradiofrontend ${IMAGE_PREFIX}:gradioapi nginx mariadb phpmyadmin/phpmyadmin dvdarias/docker-hoster"

APP_ENV=${1:-development}
NODE_ENV=${APP_ENV}

BINPATH="$( cd "$(dirname "$0")"; pwd -P )"
cd $BINPATH
cd ../
pwd

# load init_docker function
. bin/docker/scripts/initDocker.sh

. bin/docker/scripts/buildDocker.sh


init () {
  init_docker

  echo "What kind of images you want to use"
  echo ""
  options=("docker hub" "build my own" "Exit")
  select opt in "${options[@]}"
  do
      case $opt in
          "docker hub" )

              echo ""
              echo "please auth with"
              echo "docker login -u gradio"
              echo ""

              # eval "projectImagesArr=($IMAGES)"
              # for projectimage in "${projectImagesArr[@]}"; do
              #     docker pull ${projectimage}
              # done

              docker-compose pull

              break
              ;;
          "build my own" )
              build
              break
              ;;
          "Exit" )
              exit 0
              ;;
          *)  echo "Invalid option";;
      esac
  done
}

build () {
  echo ""
  echo "####################################"
  echo "building docker containers"
  echo "####################################"
  echo ""

  # docker-compose build --parallel
  docker-compose -f docker-compose.yml -f docker-compose-build.yml build

  # docker build -f bin/docker/frontend.dockerfile -t gradio/image:gradiofrontend .
  # docker build -f bin/docker/api.dockerfile -t gradio/image:gradioapi .
  # docker-compose -f docker-compose.yml -f docker-compose-build.yml push
}

stop () {
  echo ""
  echo "####################################"
  echo "stopping docker containers"
  echo "####################################"
  echo ""

  # stop
  docker-compose down --remove-orphans
}

start () {
  echo ""
  echo "####################################"
  echo "starting docker containers"
  echo "####################################"
  echo ""

  # start
  docker-compose up -d
}

endMsgStarted () {

  preMsg

  echo ""
  echo "####################################"
  echo "all containers started open ${APP_SCHEME}://${APP_URL}"
  echo "####################################"
  echo "more services:"
  echo "phpmyadmin: http://pma.${APP_URL}"
  echo "####################################"
  echo ""
}

endMsgStopped () {

  preMsg

  echo ""
  echo "####################################"
  echo "all containers stoped"
  echo "####################################"
  echo ""
}

preMsg () {
  echo ""
  echo "####################################"
  echo "${APP_NAME} docker install"
  echo "your user and group ${USER}:${GROUP}"
  echo ""
  echo "usage: `basename $0` testing|development|staging|production"
  echo "default: `basename $0` development"
  echo "####################################"
  echo ""
}

prestartdb () {
  echo ""
  echo "start db service to avoid futher using sleep"
  echo ""
  docker-compose up -d ${DB_SERVICE}
  # quick hack to wait for db started
  # sleep 10
}

preMsg

echo "What task you want to execute?"
echo ""
options=("Restart" "Deploy" "Start" "Stop" "Update" "Build" "Init" "Exit")
select opt in "${options[@]}"
do
    case $opt in
        "Restart" )
            stop
            start
            endMsgStarted
            break
            ;;
        "Deploy" )
            prestartdb
            deploy
            start
            endMsgStarted
            break
            ;;
        "Start" )
            start
            endMsgStarted
            break
            ;;
        "Stop" )
            stop
            endMsgStopped
            break
            ;;
        "Update" )
            update
            stop
            start
            endMsgStarted
            break
            ;;
        "Build" )
            build
            break
            ;;
        "Init" )
            stop
            init
            prestartdb
            deploy
            start
            endMsgStarted
            break
            ;;
        "Exit" )
            exit 0
            ;;
        *)  echo "Invalid option";;
    esac
done
