FROM gradio/image:gradioapi

MAINTAINER Raitis Rolis <raitis.rolis@gmail.com>

ENV DEBIAN_FRONTEND "noninteractive"

USER root

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

USER api
