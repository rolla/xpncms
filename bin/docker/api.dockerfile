FROM php:7.2-fpm

MAINTAINER Raitis Rolis <raitis.rolis@gmail.com>

ENV DEBIAN_FRONTEND "noninteractive"

ARG ADDITIONAL_LOCALES="lv_LV.UTF-8"

RUN apt-get clean && apt-get -qq -y update && apt-get -qq -y install --no-install-recommends \
    locales gettext zip unzip git \
    # for imagick
    graphicsmagick-imagemagick-compat libgd-dev libpng-dev libjpeg-dev libwebp-dev libxpm-dev libmagickwand-dev \
    && pecl install imagick redis \
    && echo '' >> /usr/share/locale/locale.alias \
    && temp="${ADDITIONAL_LOCALES%\"}" \
    && temp="${temp#\"}" \
    && for i in ${temp}; do sed -i "/$i/s/^#//g" /etc/locale.gen; done \
    && locale-gen

RUN docker-php-ext-enable imagick redis && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) pdo_mysql gettext gd zip opcache \
    &&  rm -rf /tmp/pear

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN useradd -ms /bin/bash api

RUN apt-get autoclean && apt-get clean && apt-get autoremove

USER api

WORKDIR /var/www
