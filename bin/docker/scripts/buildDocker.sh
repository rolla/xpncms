#!/bin/bash

clearapi () {
  rm -rf cache/templates_c/*.php public/cache/sacy/*.{js,css}
  echo "Smarty, sacy cache cleared!"

  docker exec -it ${CONTAINER_PREFIX}${API_SERVICE} composer dumpautoload
}

import_db () {
  echo "creating databases"
  echo ""
  docker exec -it ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e "create database if not exists ${UTILS_DB_NAME};"
  docker exec -it ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e "create database if not exists ${RADIODJ_DB_NAME};"
  docker exec -it ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e "create database if not exists ${GRADIO_DB_NAME};"
  docker exec -it ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e "create database if not exists ${PLAY_DB_NAME};"

  if [ ! -d ${DOWNLOAD_PATH} ]; then
    mkdir -p ${DOWNLOAD_PATH}
  fi

  echo "downloading databases to ${DOWNLOAD_PATH}"
  echo ""
  curl -u "${DOWNLOAD_USER}:${DOWNLOAD_PASSWORD}" -o ${DOWNLOAD_PATH}utils.sql.gz ${DOWNLOAD_ENDPOINT}utils.sql.gz
  curl -u "${DOWNLOAD_USER}:${DOWNLOAD_PASSWORD}" -o ${DOWNLOAD_PATH}radiodj.sql.gz ${DOWNLOAD_ENDPOINT}radiodj.sql.gz
  curl -u "${DOWNLOAD_USER}:${DOWNLOAD_PASSWORD}" -o ${DOWNLOAD_PATH}gradio.sql.gz ${DOWNLOAD_ENDPOINT}gradio.sql.gz
  curl -u "${DOWNLOAD_USER}:${DOWNLOAD_PASSWORD}" -o ${DOWNLOAD_PATH}play.sql.gz ${DOWNLOAD_ENDPOINT}play.sql.gz

  echo "importing databases"
  echo ""
  zcat ${DOWNLOAD_PATH}utils.sql.gz | docker exec -i ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${UTILS_DB_NAME}
  zcat ${DOWNLOAD_PATH}radiodj.sql.gz | docker exec -i ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${RADIODJ_DB_NAME}
  zcat ${DOWNLOAD_PATH}gradio.sql.gz | docker exec -i ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${GRADIO_DB_NAME}
  zcat ${DOWNLOAD_PATH}play.sql.gz | docker exec -i ${CONTAINER_PREFIX}${DB_SERVICE} mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${PLAY_DB_NAME}
}


import_certificates () {

  # to create and upload
  # tar -czvf docker.dev.gradio.lv.acme.sh.tar.gz ./.acme.sh
  # certArchive="docker.dev.gradio.lv.acme.sh.tar.gz" && curl -u "gradio:${DOWNLOAD_PASSWORD}" -T ${certArchive} "https://work.urdt.lv/remote.php/dav/files/gradio/backup/${certArchive}"

  certArchive="docker.dev.gradio.lv.acme.sh.tar.gz"

  echo "downloading certificates to ${DOWNLOAD_PATH}"
  echo ""
  curl -u "${DOWNLOAD_USER}:${DOWNLOAD_PASSWORD}" -o ${DOWNLOAD_PATH}${certArchive} ${DOWNLOAD_ENDPOINT}${certArchive}

  echo "extracting certificates"
  echo ""
  tar xzf ${DOWNLOAD_PATH}${certArchive}
}

deploy () {
  echo ""
  echo "####################################"
  echo "deploying docker containers"
  echo "####################################"
  echo ""


  echo ""
  read -p "do you want to import databases? [y/N]: " importDb
  importDb=${importDb:-N}
  if [ "${importDb}" == "y" ] ; then
    import_db
  fi

  echo ""
  read -p "do you want to import certificates? [y/N]: " importCertificates
  importCertificates=${importCertificates:-N}
  if [ "${importCertificates}" == "y" ] ; then
    import_certificates
  fi

  # api post install
  docker-compose up -d ${API_SERVICE}
  docker exec -it ${CONTAINER_PREFIX}${API_SERVICE} composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
  docker exec -it ${CONTAINER_PREFIX}${API_SERVICE} vendor/bin/propel config:convert

  clearapi

  # frontend post install
  docker-compose up -d ${FRONTEND_SERVICE}
  # docker exec -it ${CONTAINER_PREFIX}${FRONTEND_SERVICE} yarn --ignore-scripts --ignore-platform --ignore-engines --ignore-optional
  docker exec -it ${CONTAINER_PREFIX}${FRONTEND_SERVICE} yarn
  docker exec -it ${CONTAINER_PREFIX}${FRONTEND_SERVICE} yarn build
  docker exec -it ${CONTAINER_PREFIX}${FRONTEND_SERVICE} yarn copyoldassets

  docker exec -it ${CONTAINER_PREFIX}${FRONTEND_SERVICE} node_modules/.bin/gettext-compile \
  --output com/xpncms/View/templates/${FRONTEND_TEMPLATE}/src/translations.json \
  com/xpncms/Locale/lv_LV/LC_MESSAGES/lv_LV.po com/xpncms/Locale/en_US/LC_MESSAGES/en_US.po
}

update () {
  docker-compose up -d ${FRONTEND_SERVICE}
  docker exec -it ${CONTAINER_PREFIX}${FRONTEND_SERVICE} yarn upgrade

  docker-compose up -d ${API_SERVICE}
  docker exec -it ${CONTAINER_PREFIX}${API_SERVICE} composer update --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts

  clearapi
}
