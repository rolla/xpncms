#!/bin/bash

fix_permissons () {
  chmod 777 .cache
  chmod 775 node_modules
  chmod 775 public/assets
  chmod 775 public/themes/gradio-material/assets
  chmod 775 vendor

  chown -R ${USER}:${GROUP} ./
}

remove_all () {
  echo ""
  echo "preparing build from zero"
  echo ""

  # important for speed up frontend build
  rm -rf node_modules/**
  rm -rf .cache/**
  rm -rf .node-gyp
  # need to remove only if node_modules folder removed else yarn install --no-lockfile
  rm yarn.lock

  # important files to not go downtime
  rm -rf vendor/**
  rm -rf public/assets/**
  rm -rf public/themes/gradio-material/assets/**

  mkdir node_modules
  mkdir -p public/assets
  mkdir -p public/themes/gradio-material/assets
  mkdir vendor
}

init_docker () {
  echo ""
  echo "####################################"
  echo "init ${APP_NAME} project for docker"
  echo "####################################"
  echo ""

  # remove dangling images eg. with none name
  # docker rmi $(docker images -f dangling=true -q)

  # remove containers
  docker rm -f ${CONTAINERS}

  # clean system
  docker system prune

  rm -rf .yarn
  rm -rf .local
  rm -rf .npm
  rm -rf .config

  rm .yarnrc
  rm .wget-hsts

  # rm package-lock.json
  # rm yarn-error.log
  echo ""
  read -p "do you want start from zero? (delete docker images and vendor, node_modules, cache folders) [y/N]: " startFromZero
  startFromZero=${startFromZero:-N}
  if [ "${startFromZero}" == "y" ] ; then
    # remove images
    docker rmi -f ${IMAGES}

    remove_all
  fi

  fix_permissons

  if [ ! -f .env ]; then
    cp .env.docker .env
  fi

  if [ ! -f config/conf.php ]; then
    cp config/conf.sample.php config/conf.php
  fi

  if [ ! -f config/hybridauth_config.php ]; then
    cp config/hybridauth_config.sample.php config/hybridauth_config.php
  fi

  echo ""
  echo "####################################"
  echo "${APP_NAME} project ready for docker"
  echo "####################################"
  echo ""
}
