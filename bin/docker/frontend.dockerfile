FROM node:14-alpine

ENV NODE_ENV=$NODE_ENV

USER root

RUN apk update && apk add --no-cache \
    bash git openssh wget imagemagick \
    # Prerequisites for pngquant-bin
    build-base libpng-dev lcms2-dev \
    autoconf make g++ automake libtool nasm python3 \
    # to be able to work webpack --watch
    && echo "fs.inotify.max_user_watches=524288" >> /etc/sysctl.conf

RUN yarn global add @vue/cli

USER node

# ENV HOME=/var/www

WORKDIR /var/www

CMD tail -f /dev/null
