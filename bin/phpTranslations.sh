#!/bin/bash

cd "$(dirname "$0")" &&
cd ../ &&

pwd

echo "Find translations in php files!"
find ./com/xpncms/ -iname "*.php" | xargs xgettext --language=PHP --from-code utf-8 -o ./com/xpncms/Locale/code-php.pot;

echo "Find translations in smarty template files!"
php ./vendor/smarty-gettext/smarty-gettext/tsmarty2c.php -o ./com/xpncms/Locale/smarty.pot ./com/xpncms/;

#echo "Find translations in vue and js files!"
#find ./com/xpncms/View/ -iname "*.vue" -o -iname "*.js" | xargs xgettext --language=JavaScript --keyword=npgettext:1c,2,3 --from-#code=utf-8 -o ./com/xpncms/Locale/code-js.pot;

echo "Merge translations!"
msgcat ./com/xpncms/Locale/code-php.pot ./com/xpncms/Locale/smarty.pot -o ./com/xpncms/Locale/template-en.pot;

echo "Cleaning up..."
rm ./com/xpncms/Locale/code-php.pot;
#rm ./com/xpncms/Locale/code-js.pot;
rm ./com/xpncms/Locale/smarty.pot

echo "Translations exported to ./com/xpncms/Locale/template-en.pot"

