#!/bin/bash

# Instant Upgrades and Instant Refactoring of any PHP 5.3+ code
# https://getrector.org https://github.com/rectorphp/rector

cd "$(dirname "$0")" &&
cd ../ &&

pwd

docker run -v $(pwd):/project rector/rector:latest process /project/com/xpncms/Controller --autoload-file ../project/vendor/autoload.php --set php72 --dry-run
