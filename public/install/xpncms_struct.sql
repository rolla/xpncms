CREATE TABLE `xpncms_actions` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `xpncms_actions_categories` (
  `actions_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `actions_category` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`actions_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `xpncms_auths` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'refer to users.id',
  `provider` varchar(100) CHARACTER SET utf8 NOT NULL,
  `provider_uid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 NOT NULL,
  `display_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `profile_url` varchar(300) CHARACTER SET utf8 NOT NULL,
  `website_url` varchar(300) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `full_user_data` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = enabled, 0 - disabled',
  PRIMARY KEY (`id`),
  UNIQUE KEY `provider_uid` (`provider_uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_auths_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'refer to users.id',
  `provider` varchar(100) CHARACTER SET utf8 NOT NULL,
  `provider_uid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 NOT NULL,
  `display_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `profile_url` varchar(300) CHARACTER SET utf8 NOT NULL,
  `website_url` varchar(300) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `full_user_data` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = enabled, 0 - disabled',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_content` text COLLATE utf8_unicode_ci NOT NULL,
  `cat_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_added` datetime NOT NULL,
  `cat_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cat_class` int(11) NOT NULL,
  `cat_parent` int(11) NOT NULL DEFAULT '0',
  `cat_imgs` text COLLATE utf8_unicode_ci NOT NULL,
  `cat_order` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_content` (
  `cont_id` int(11) NOT NULL AUTO_INCREMENT,
  `cont_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cont_content` text COLLATE utf8_unicode_ci NOT NULL,
  `cont_tags` text COLLATE utf8_unicode_ci NOT NULL,
  `cont_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_added` datetime NOT NULL,
  `cont_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cont_class` int(11) NOT NULL,
  `cont_category` int(11) NOT NULL,
  `cont_imgs` text COLLATE utf8_unicode_ci NOT NULL,
  `cont_order` int(11) NOT NULL,
  PRIMARY KEY (`cont_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_friends` (
  `id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `added` datetime NOT NULL,
  `confirmed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `actions_category_id` int(11) NOT NULL,
  `actions_item_id` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `readed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `xpncms_sessions` (
  `id` tinyint(4) NOT NULL,
  `vuser` tinyint(4) NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 NOT NULL,
  `visits` tinyint(4) NOT NULL,
  `currentvisit` datetime NOT NULL,
  `lastvisit` datetime NOT NULL,
  `added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_userclasses` (
  `userclass_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userclass_name` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `userclass_description` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `userclass_editclass` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userclass_parent` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userclass_accum` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `userclass_visibility` smallint(5) NOT NULL DEFAULT '0',
  `userclass_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userclass_icon` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`userclass_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `xpncms_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(10) NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `age` int(10) NOT NULL,
  `adult` int(10) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visits` int(10) NOT NULL,
  `currentvisit` datetime NOT NULL,
  `lastvisit` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `added` datetime NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imgs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imgm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imgl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prefs` text COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(3) NOT NULL DEFAULT '0',
  `perms` text COLLATE utf8_unicode_ci NOT NULL,
  `class` text COLLATE utf8_unicode_ci NOT NULL,
  `sess` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ban` int(10) NOT NULL,
  `dr_created` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dr_deleted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;