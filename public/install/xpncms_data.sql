INSERT INTO `xpncms_userclasses` (`userclass_id`, `userclass_name`, `userclass_description`, `userclass_editclass`, `userclass_parent`, `userclass_accum`, `userclass_visibility`, `userclass_type`, `userclass_icon`) VALUES
(2, 'CONTACT PEOPLE', 'Example contact person class', 254, 0, '0,2', 0, 0, ''),
(253, 'Members', 'Registered and logged in members', 250, 0, '0,253', 253, 0, ''),
(246, 'Search Bots', 'Identified search bots', 250, 0, '0,246', 254, 0, ''),
(254, 'Admin', 'Site Administrators', 250, 249, '254', 253, 0, ''),
(248, 'Forum Moderators', 'Moderators for Forums and other areas', 250, 249, '248', 253, 0, ''),
(249, 'Admins and Mods', 'Anyone able to administer something, moderate forums etc', 250, 250, '249,254,248', 253, 0, ''),
(1, 'PRIVATEMENU', 'Grants access to private menu items', 254, 253, '0,253,1', 0, 0, ''),
(247, 'New Users', 'Recently joined users', 250, 253, '0,253,247', 254, 0, ''),
(250, 'Main Admin', 'Main site Administrators', 250, 255, '250,249,254,248', 253, 0, ''),
(0, 'Everyone (public)', 'Fixed class', 250, 0, '0', 0, 0, ''),
(252, 'Guests', 'Fixed class', 250, 0, '0,252', 0, 0, ''),
(255, 'No One (inactive)', 'Fixed class', 250, 0, '255', 0, 0, ''),
(251, 'Read Only', 'Fixed class', 250, 0, '0,251', 0, 0, ''),
(3, 'FORUM_MODERATOR', 'Moderator of all forums', 254, 255, '3', 254, 0, '');

INSERT INTO `xpncms_actions` (`action_id`, `action`, `added`, `updated`) VALUES
(1, 'liked', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(2, 'disliked', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(3, 'commented', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(4, 'rated', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(5, 'updated', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(6, 'deleted', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(7, 'added', '2015-02-09 18:51:30', '0000-00-00 00:00:00'),
(8, 'send', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(9, 'invite', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(10, 'accept', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(11, 'reject', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(12, 'listen', '2015-02-09 19:15:27', '0000-00-00 00:00:00');

INSERT INTO `xpncms_actions_categories` (`actions_category_id`, `actions_category`, `added`, `updated`) VALUES
(1, 'tune', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(2, 'picture', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(3, 'album', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(4, 'settings', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(5, 'news', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(6, 'category', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(7, 'blog', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(8, 'message', '2015-02-09 19:15:27', '0000-00-00 00:00:00'),
(9, 'notify', '2015-02-09 19:15:27', '0000-00-00 00:00:00');