<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>xpnCMS install</title>

    <!-- Bootstrap -->
    <link href="<?= SITE_URL; ?>/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= SITE_URL; ?>/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid" role="main">
      <div class="row">
        <div class="col-xs-3">
          <a href="<?= SITE_URL; ?>/install"><h1>xpnCMS install</h1></a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-1">
          <a href="<?= SITE_URL; ?>?newinstall=1" class="btn btn-success">To website</a>
        </div>
        <div class="col-xs-2">
          <a href="<?= SITE_URL; ?>/install?createdirs=1" class="btn btn-primary">Create directories</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <th>Item</th>
                <th>App Root <code><i class="fa fa-folder-o" aria-hidden="true"></i> <?= APP_ROOT; ?></code></th>
                <th>Exists</th>
                <th>Writable</th>
                <th>Description</th>
              </thead>
              <tbody>
                <?php foreach ($statusesObj as $key => $val): ?>
                  <tr>
                    <td><?= $val->info; ?></td>
                    <td>
                      <i class="fa <?= $val->directory ? 'fa-folder-o' : 'fa-file-o' ?>" aria-hidden="true"></i>
                      <?= $val->path; ?> <?= $val->folder->humanSize; ?>
                    </td>
                    <td class="<?= $val->type; ?>"><?= $val->status; ?></td>
                    <?php if ($val->needWrite && $val->writable): ?>
                      <td class="success">ok</td>
                    <?php elseif ($val->needWrite && !$val->writable): ?>
                      <td class="danger">fail</td>
                    <?php else: ?>
                     <td class="warning">not needed</td>
                    <?php endif; ?>
                    <td><?= $val->description; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <?= $content; ?>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary btn-submit-edit">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= SITE_URL; ?>/assets/jquery/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= SITE_URL; ?>/assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= SITE_URL; ?>/assets/jquery-ujs/src/rails.js"></script>
    <script src="<?= SITE_URL; ?>/assets/jquery-ui/jquery-ui.min.js"></script>

    <script>

      function calculateTextLength(element, max){
        //var max = 16;
        var elem = $('#editModal #' + element);
        elem.on('keyup', function() {
            var tLength = elem.val().length;
            var remaining = max - tLength;

            $('#' + element + 'CountMessage').html(remaining + ' remaining from ' + max);

            if(remaining < 0 || tLength == 0){
                $('.' + element + '-form-group').addClass('has-error');
                $('.btn-submit-edit').prop('disabled', true);
            } else {
                $('.' + element + '-form-group').removeClass('has-error');
                $('.' + element + '-form-group').addClass('has-success');
                $('.btn-submit-edit').prop('disabled', false);
            }
        });
        elem.keyup();
      }

    </script>
  </body>
</html>
