<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', realpath('..' . DS . '..') . DS);

$configFile = 'config' . DS . 'conf.php';
$vendorDir = 'vendor';
$autoLoadFile = 'vendor' . DS . 'autoload.php';
$cacheDir = 'cache';
$logDir = 'log';
$smartyTemplateCacheDir = 'cache' . DS . 'templates_c';
$installDir = 'public' . DS . 'install';
$publicCacheDir = 'public' . DS . 'cache';
$publicAssetsDir = 'public' . DS . 'assets';
$publicFilesDir = 'public' . DS . 'files';
$sacyCacheDir = 'public' . DS . 'cache' . DS . 'sacy';
$phpthumbCacheDir = 'public' . DS . 'cache' . DS . 'phpthumb';

require_once APP_ROOT . $installDir . DS . 'install.php';
$install = new install();

$statuses = [
    'config' => [
        'info' => 'Configuration file',
        'path' => $configFile,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => 'Not needed',
        'description' => '',
        'load' => true,
        'needWrite' => false,
        'directory' => false,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'vendor' => [
        'info' => 'Composer vendor directory',
        'path' => $vendorDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => 'Not needed',
        'description' => 'All needed PHP packages stays here. Hit <code>cd ' . APP_ROOT . ' && composer install</code> to do the magic :)',
        'load' => false,
        'needWrite' => false,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'autoload' => [
        'info' => 'Composer autoload file',
        'path' => $autoLoadFile,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => 'Not needed',
        'description' => 'Important autoload file, that will create after <code>composer install</code>',
        'load' => false,
        'needWrite' => false,
        'directory' => false,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'cache' => [
        'info' => 'App cache directory',
        'path' => $cacheDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => '',
        'load' => false,
        'needWrite' => true,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'log' => [
        'info' => 'App log directory',
        'path' => $logDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => '',
        'load' => false,
        'needWrite' => false,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'smartyTemplateCache' => [
        'info' => 'Smarty templates cache directory',
        'path' => $smartyTemplateCacheDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => 'Directory where smarty put compiled templates.',
        'load' => false,
        'needWrite' => true,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'install' => [
        'info' => 'Website install directory',
        'path' => $installDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => 'Website instalation check directory',
        'load' => false,
        'needWrite' => false,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'publicCache' => [
        'info' => 'Website cache directory',
        'path' => $publicCacheDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => '',
        'load' => false,
        'needWrite' => false,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'publicAssets' => [
        'info' => 'Website assets directory',
        'path' => $publicAssetsDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => 'Bower components directory. Hit <code>cd ' . APP_ROOT . ' && bower install</code> to do the assmagic :)',
        'load' => false,
        'needWrite' => false,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'publicFiles' => [
        'info' => 'Website files directory',
        'path' => $publicFilesDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => 'User files.',
        'load' => false,
        'needWrite' => true,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'sacy' => [
        'info' => 'Sacy cache directory',
        'path' => $sacyCacheDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => '',
        'load' => false,
        'needWrite' => true,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
    'phpthumb' => [
        'info' => 'phpThumb cache directory',
        'path' => $phpthumbCacheDir,
        'status' => 'fail',
        'type' => 'danger',
        'writable' => false,
        'description' => 'Needs to <code>chmod 777 ' . APP_ROOT . $phpthumbCacheDir . '</code>',
        'load' => false,
        'needWrite' => true,
        'directory' => true,
        'folder' => ['size' => 0, 'howmany' => 0, 'humanSize' => 0],
    ],
];

$statusesObj = json_decode(json_encode($statuses), FALSE);

foreach ($statusesObj as $item => $val) {
    if (file_exists(APP_ROOT . $val->path)) {
        $val->status = 'ok';
        $val->type = 'success';
        $val->writable = is_writable(APP_ROOT . $val->path);
        $val->folder = $install->dirsize(APP_ROOT . $val->path);
    }
    if ($val->load) {
        require_once APP_ROOT . $val->path;
    }
}

$controller = isset($_REQUEST['controller']) && !empty($_REQUEST['controller']) ? $_REQUEST['controller'] : null;
$method = isset($_REQUEST['method']) && !empty($_REQUEST['method']) ? $_REQUEST['method'] : null;
$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? $_REQUEST['id'] : null;
$request = $_REQUEST;

$html;
$html = '<pre>';

if (isset($request['createdirs'])) {
    foreach ($statusesObj as $dir) {
        $html .= $install->checkCreateDir($dir);
    }
}

$html .= '</pre>';

$content = $html;

require_once __DIR__ . DS . 'layout.php';
