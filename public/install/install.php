<?php

class install
{

    public function __construct()
    {

    }

    protected function makedirs($dirpath, $mode = 0775)
    {
        return is_dir($dirpath) || mkdir($dirpath, $mode, true);
    }

    public function dirsize($dir)
    {
        if (is_file($dir)) {
            $result = ['size' => filesize($dir), 'howmany' => 0, 'humanSize' => $this->file_size(filesize($dir))];
            return json_decode(json_encode($result), FALSE);
        }
        if ($dh = opendir($dir)) {
            $size = 0;
            $n = 0;
            while (($file = readdir($dh)) !== false) {
                if ($file == '.' || $file == '..')
                    continue;
                $n++;
                $data = $this->dirsize($dir . '/' . $file);
                $size += $data->size;
                $n += $data->howmany;
            }
            closedir($dh);
            $result = ['size' => $size, 'howmany' => $n, 'humanSize' => $this->file_size($size)];
            return json_decode(json_encode($result), FALSE);
        }
        $result = ['size' => 0, 'howmany' => 0, 'humanSize' => 0];
        return json_decode(json_encode($result), FALSE);
    }

    function file_size($fsizebyte)
    {
        if ($fsizebyte < 1024) {
            $fsize = $fsizebyte . " bytes";
        } elseif (($fsizebyte >= 1024) && ($fsizebyte < 1048576)) {
            $fsize = round(($fsizebyte / 1024), 2);
            $fsize = $fsize . " KB";
        } elseif (($fsizebyte >= 1048576) && ($fsizebyte < 1073741824)) {
            $fsize = round(($fsizebyte / 1048576), 2);
            $fsize = $fsize . " MB";
        } elseif ($fsizebyte >= 1073741824) {
            $fsize = round(($fsizebyte / 1073741824), 2);
            $fsize = $fsize . " GB";
        };
        return $fsize;
    }

    public function checkCreateDir($dir)
    {
        $html = '';
        if ($dir->directory) {
            $html .= $dir->path;
            if (file_exists(APP_ROOT . $dir->path)) {
                $html .= ' Exists OK';
            } else {
                $html .= ' Exists FAIL';
                $make = $this->makedirs(APP_ROOT . $dir->path, 0775);
                if (!$make) {
                    throw new Exception('Cannot make directory ' . APP_ROOT . $dir->path);
                }
                $html .= ' Created OK';
            }
            $html .= '<br>';
        }

        return $html;
    }
}

