<?php

define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', realpath('..' . DS) . DS);

if (php_sapi_name() == 'cli-server') {

}

$bootstrapFile = APP_ROOT . 'app' . DS . 'bootstrap.php';

if (!file_exists($bootstrapFile)) {
    echo 'Ooopss something goes wrong...';
    echo '<br>';
    echo 'You can still listen to our stream';
    echo '<br>';
    echo 'gradio.lv (MP3 @ 320 kbps) <a href="http://stream.gradio.lv:8000/listen.pls?sid=15"><img border="0" title="Listen to Stream" alt="Listen to Stream" style="vertical-align:middle" src="http://stream.gradio.lv:8000/images/listen.png"></a>';
    echo '<br>';
    echo 'gradio.lv (HE-AAC @ 128 kbps) <a href="http://stream.gradio.lv:8000/listen.pls?sid=16"><img border="0" title="Listen to Stream" alt="Listen to Stream" style="vertical-align:middle" src="http://stream.gradio.lv:8000/images/listen.png"></a>';
    exit;
}

require_once($bootstrapFile);
