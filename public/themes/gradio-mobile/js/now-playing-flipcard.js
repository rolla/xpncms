var reloadSecs = 20;
var _wpmejsSettings = {"pluginPath": "\/assets\/gdlr-player\/mediaelement\/"};
var lastImg = '';
$(function () {
  function checkExists(url) {
    $.ajax({
      url: url,
      type: 'HEAD',
      error: function ()
      {
        //file not exists
      },
      success: function ()
      {
        return true;
      }
    });

    return false;
  }

  var headerEl = $('#header');
  headerEl.css('height', $(window).height());

  moment.locale('lv'); //todo need update from variable
  function checkNow() {
    $.getJSON(siteURL + "/gradio/now").success(function (response) {
      var headerEl = $('#full-screen-bg');
      var playerImgEl = $('.gdlr-top-player-thumbnail img');
      var playerNowImgEl = $('.card-container .tune-cover');
      var playerImgAnchor = $('.gdlr-top-player-thumbnail a');
      var playerSongTitle = $('.gdlr-top-player-title');

      /*
      var playerNowSongArtist = $('.header-card-container .card-content .artist');
      var playerNowSongTitle = $('.header-card-container .card-content .title');
      var playerNowSongId = $('.now-playing-card .controls .rate');
      var playerNowAlreadyVoted = $('.now-playing-card .artist-img-container .already-voted');
      var playerDatePlayed = $('.header-card-container .card-content .date-played');
      var playerGenre = $('.header-card-container .card-content .genre');
      var playerPercentsPlayed = $('.header-card-container .card-content .percents-played');
      var playerAlbum = $('.header-card-container .card-content .album');
      var playerYear = $('.header-card-container .card-content .year');
      var playerWeight = $('.header-card-container .card-content .weight');
      var playerBpm = $('.header-card-container .card-content .bpm');
      var playerCountPlayed = $('.header-card-container .card-content .count-played');
      var playerDatePlayed = $('.header-card-container .card-content .date-played');
      var playerExtension = $('.header-card-container .tune-format');
      var playerGenre = $('.header-card-container .card-content .genre');
      var playerLikeId = $('.header-card-container .card-content .tune-like').data('item-id', response.now.song_id);
      var playerDislikeId = $('.header-card-container .card-content .tune-dislike').data('item-id', response.now.song_id);
      var playerDateAdded = $('.header-card-container .card-content .date-added');
      var playerPublisher = $('.header-card-container .card-content .publisher');
      */

      var currentSongUrl = baseURL + 'tune/' + response.now.song_id;
      // var playerSeeMore = $('.header-card-container .card-content .see-more').attr('href', currentSongUrl);

      //playerNowSongId.attr('data-item-id', response.now.song_id);

      playerSongTitle.fadeOut('slow', function () {
        playerSongTitle.html('<a href="' + currentSongUrl + '" class="ajax-nav tune-id" data-tune-id="' + response.now.song_id + '">' + response.now.artist + ' - ' + response.now.title + '</a>');
        playerSongTitle.fadeIn('slow');
      });

      /*
      playerNowAlreadyVoted.fadeOut('slow');

      if (response.now.liked) {
        playerNowAlreadyVoted.fadeOut('slow', function () {
          playerNowAlreadyVoted.removeClass('fa-thumbs-down').addClass('fa-heart')
          playerNowAlreadyVoted.fadeIn('slow');
        });
      }
      if (response.now.disliked) {
        playerNowAlreadyVoted.fadeOut('slow', function () {
          playerNowAlreadyVoted.removeClass('fa-heart').addClass('fa-thumbs-down')
          playerNowAlreadyVoted.fadeIn('slow');
        });
      }

      playerNowSongArtist.fadeOut('slow', function () {
        playerNowSongArtist.html(response.now.artist);
        playerNowSongArtist.fadeIn('slow');
      });

      playerNowSongTitle.fadeOut('slow', function () {
        playerNowSongTitle.html(response.now.title);
        playerNowSongTitle.fadeIn('slow');
      });

      playerDatePlayed.fadeOut('slow', function () {
        playerDatePlayed.html(response.now.date_played);
        playerDatePlayed.fadeIn('slow');
      });

      playerPercentsPlayed.fadeOut('slow', function () {
        playerPercentsPlayed.html(response.now.percents_played + '%');
        playerPercentsPlayed.fadeIn('slow');
      });

      playerAlbum.fadeOut('slow', function () {
        playerAlbum.html(response.now.album);
        playerAlbum.fadeIn('slow');
      });

      playerYear.fadeOut('slow', function () {
        playerYear.html(response.now.year);
        playerYear.fadeIn('slow');
      });

      playerWeight.fadeOut('slow', function () {
        playerWeight.html(response.now.weight);
        playerWeight.fadeIn('slow');
      });

      playerBpm.fadeOut('slow', function () {
        playerBpm.html(response.now.bpm);
        playerBpm.fadeIn('slow');
      });

      playerCountPlayed.fadeOut('slow', function () {
        playerCountPlayed.html(response.now.count_played);
        playerCountPlayed.fadeIn('slow');
      });

      playerDatePlayed.fadeOut('slow', function () {
        playerDatePlayed.html(moment(response.now.date_played, "YYYY-MM-DD HH:mm:ss").fromNow());
        playerDatePlayed.fadeIn('slow');
      });

      playerExtension.fadeOut('slow', function () {
        playerExtension.html(response.now.format, );
        playerExtension.fadeIn('slow');
      });

      playerGenre.fadeOut('slow', function () {
        playerGenre.html(response.now.genre, );
        playerGenre.fadeIn('slow');
      });

      playerDateAdded.fadeOut('slow', function () {
        playerDateAdded.html(moment(response.now.date_added, "YYYY-MM-DD HH:mm:ss").fromNow());
        playerDateAdded.fadeIn('slow');
      });

      playerPublisher.fadeOut('slow', function () {
        playerPublisher.attr('src', baseURL + 'themes/' + frontendTheme + '/img/icons/labels/' + response.now.publisher_svg);
        playerPublisher.fadeIn('slow');
      });
      */

      if ("undefined" !== typeof response.now.picture && lastImg !== response.now.picture) {
        var currentImgUrlEncoded = phpThumbUrl + '?q=100&w=2000&src=' + encodeURIComponent(coversURL + response.now.picture) + '&hash=' + phpThumbCacheHash;

        headerEl.fadeOut('fast', function () {
          headerEl.css('background-image', 'url(' + currentImgUrlEncoded + ')');
          headerEl.addClass('blur-all');
          headerEl.fadeIn('fast');
          lastImg = response.now.picture;
        });

        playerImgEl.fadeOut('slow', function () {
          playerImgEl.attr('src', phpThumbUrl + '?q=100&w=100&src=' + encodeURIComponent(coversURL + response.now.picture) + '&hash=' + phpThumbCacheHash);
          //playerImgAnchor.attr('href', '#header');
          playerImgEl.fadeIn('slow');
        });

        /*
        playerNowImgEl.fadeOut('slow', function () {
          playerNowImgEl.attr('src', phpThumbUrl + '?q=100&w=500&src=' + encodeURIComponent(coversURL + response.now.picture) + '&hash=' + phpThumbCacheHash);
          playerNowImgEl.fadeIn('slow');
        });
        */

      }
      //console.log("first: " + new Date().getTime());
    }).then(function () {
      setTimeout(checkNow, reloadSecs * 1000);
      //console.log("second:" + new Date().getTime());
    });
  }

  checkNow();
});
