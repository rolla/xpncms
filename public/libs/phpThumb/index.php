<?php
//////////////////////////////////////////////////////////////
//   phpThumb() by James Heinrich <info@silisoftware.com>   //
//        available at http://phpthumb.sourceforge.net      //
//         and/or https://github.com/JamesHeinrich/phpThumb //
//////////////////////////////////////////////////////////////
///                                                         //
// See: phpthumb.changelog.txt for recent changes           //
// See: phpthumb.readme.txt for usage instructions          //
//                                                         ///
//////////////////////////////////////////////////////////////

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('magic_quotes_runtime', '0');
if (ini_get('magic_quotes_runtime')) {
	die('"magic_quotes_runtime" is set in php.ini, cannot run phpThumb with this enabled');
}
// Set a default timezone if web server has not done already in php.ini
if (!ini_get('date.timezone') && function_exists('date_default_timezone_set')) { // PHP >= 5.1.0
    date_default_timezone_set('UTC');
}
$starttime = array_sum(explode(' ', microtime())); // could be called as microtime(true) for PHP 5.0.0+


define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', realpath('..' . DS . '..' . DS . '..' . DS) . DS);
define('APP_VENDOR', APP_ROOT . 'vendor' . DS);
define('APP_PHPTHUMB_VENDOR', APP_VENDOR . 'james-heinrich' . DS . 'phpthumb' . DS);
//vendor / james-heinrich / phpthumb /

$auto_load = APP_ROOT . 'vendor' . DS . 'autoload.php';

if (!file_exists($auto_load)) {
    throw new Exception($auto_load . ' cannot load!');
}
$loader = require_once $auto_load;

//include_once APP_PHPTHUMB_VENDOR . 'phpThumb.php';

include_once APP_ROOT . 'config/conf.php';
global $conf;

$errorImage = Core\Includes\Config::get('base.url') . Core\Includes\Config::get('paths.themes') . '/';
$errorImage .= Core\Includes\Config::get('site.template') . '/' . Core\Includes\Config::get('paths.images');
$errorImage .= '/default/not-found.png';

include_once 'phpThumb.php';
